﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strawberry.Common.Plugins
{
    public interface IPlugin
    {
        PluginContext Context
        {
            get;
        }

        string Name
        {
            get;
        }

        string Description
        {
            get;
        }

        string Author
        {
            get;
        }

        Version Version
        {
            get;
        }

        void Initialize();
        void Shutdown();

        void Dispose();
    }
}
