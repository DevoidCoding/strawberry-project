﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using NLog;
using Strawberry.Core.Config;
using Strawberry.Core.Extensions;
using Strawberry.Core.Reflection;

namespace Strawberry.Common.Plugins
{
    // todo : appdomain
    public sealed class PluginManager : Singleton<PluginManager>
    {
        #region Public Delegates

        public delegate void PluginContextHandler(PluginContext pluginContext);

        #endregion Public Delegates

        #region Public Fields

        [Configurable("PluginsFolder")]
        public static string PluginsFolder = "./plugins";

        #endregion Public Fields

        #region Private Constructors

        private PluginManager()
        {
        }

        #endregion Private Constructors

        #region Public Properties

        public ReadOnlyCollection<PluginContext> Plugins => _pluginContexts.AsReadOnly();

        #endregion Public Properties

        #region Private Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly List<PluginContext> _pluginContexts = new List<PluginContext>();

        #endregion Private Fields

        #region Public Events

        public event PluginContextHandler PluginAdded;

        public event PluginContextHandler PluginRemoved;

        #endregion Public Events

        #region Public Methods

        public PluginContext GetPlugin(string name, bool ignoreCase = false)
        {
            var plugins = from entry in _pluginContexts
                where entry.Plugin.Name.Equals(name, ignoreCase
                    ? StringComparison.InvariantCultureIgnoreCase
                    : StringComparison.InvariantCulture)
                select entry;

            return plugins.FirstOrDefault();
        }

        public void LoadAllPlugins()
        {
            if (!Directory.Exists(PluginsFolder))
                Directory.CreateDirectory(PluginsFolder);

            foreach (var file in Directory.EnumerateFiles(PluginsFolder, "*.dll"))
            {
                var pluginContext = LoadPlugin(file);

                if (pluginContext != null)
                    _pluginContexts.Add(pluginContext);
            }
        }

        public PluginContext LoadPlugin(string libPath)
        {
            if (!File.Exists(libPath))
                throw new FileNotFoundException("File doesn't exist", libPath);

            if (_pluginContexts.Any(entry => Path.GetFullPath(entry.AssemblyPath) == Path.GetFullPath(libPath)))
                throw new Exception("Plugin already loaded");

            var asmData = File.ReadAllBytes(libPath);

            return LoadPlugin(libPath, asmData);
        }

        public PluginContext LoadPlugin(string name, byte[] asmData)
        {
            try
            {
                var pluginAssembly = Assembly.Load(asmData);
                var pluginContext = new PluginContext(name, pluginAssembly);

                // search the entry point (the class that implements IPlugin)
                var entryPoint = pluginAssembly.GetTypes().Where(e => e.IsPublic && !e.IsAbstract && e.HasInterface(typeof(IPlugin))).ToList();
                if (entryPoint.Count != 1)
                {
                    if (entryPoint.Count == 0)
                        throw new PluginLoadException($"Plugin {pluginContext.PluginAssembly.GetName().Name} has no IPlugin entry point");
                    if (entryPoint.Count >= 2)
                        throw new PluginLoadException("Found 2 classes that implements IPlugin. A plugin can contains only one");
                }

                pluginContext.Initialize(entryPoint.First());
                RegisterPlugin(pluginContext);

                return pluginContext;
            }
            catch (Exception ex)
            {
                Logger.Warn(ex);
                return null;
            }
        }

        public void UnLoadAllPlugins()
        {
            foreach (var plugin in Plugins.ToArray())
                UnLoadPlugin(plugin);
        }

        public void UnLoadPlugin(string name, bool ignoreCase = false)
        {
            var plugin = from entry in _pluginContexts
                where entry.Plugin.Name.Equals(name, ignoreCase
                    ? StringComparison.InvariantCultureIgnoreCase
                    : StringComparison.InvariantCulture)
                select entry;

            foreach (var pluginContext in plugin)
                UnLoadPlugin(pluginContext);
        }

        public void UnLoadPlugin(PluginContext context)
        {
            // we cannot unload the assembly, sadly :/
            context.Plugin.Shutdown();
            context.Plugin.Dispose();

            UnRegisterPlugin(context);
        }

        #endregion Public Methods

        #region Private Methods

        private void OnPluginAdded(PluginContext pluginContext)
        {
            var handler = PluginAdded;
            handler?.Invoke(pluginContext);
        }

        private void OnPluginRemoved(PluginContext pluginContext)
        {
            var handler = PluginRemoved;
            handler?.Invoke(pluginContext);
        }

        private void RegisterPlugin(PluginContext pluginContext)
        {
            _pluginContexts.Add(pluginContext);

            OnPluginAdded(pluginContext);
        }

        private void UnRegisterPlugin(PluginContext pluginContext)
        {
            _pluginContexts.Remove(pluginContext);

            OnPluginRemoved(pluginContext);
        }

        #endregion Private Methods
    }
}