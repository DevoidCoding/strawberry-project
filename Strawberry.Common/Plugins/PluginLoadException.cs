﻿using System;

namespace Strawberry.Common.Plugins
{
    public class PluginLoadException : Exception
    {
        #region Public Constructors

        public PluginLoadException(string exception)
            : base(exception)
        {
        }

        #endregion Public Constructors
    }
}