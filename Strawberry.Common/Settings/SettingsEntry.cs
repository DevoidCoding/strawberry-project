﻿namespace Strawberry.Common.Settings
{
    public abstract class SettingsEntry
    {
        #region Public Properties

        public abstract string EntryName { get; }

        #endregion Public Properties
    }
}