﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Strawberry.Common.Settings
{
    /// <summary>
    ///     Settings related to a bot instance
    /// </summary>
    public class BotSettings
    {
        #region Public Constructors

        public BotSettings(string path)
        {
            FilePath = path;
        }

        #endregion Public Constructors

        #region Public Properties

        public string FilePath { get; }

        #endregion Public Properties

        #region Private Methods

        private XmlNode GetEntryNode(string name)
        {
            return _loadedNodes.SingleOrDefault(entry => entry.Attributes?[AttributeName].Value == name);
        }

        #endregion Private Methods

        #region Public Fields

        public const string AttributeName = "name";
        public const string NodeName = "Settings";

        #endregion Public Fields

        #region Private Fields

        private readonly List<SettingsEntry> _entries = new List<SettingsEntry>();
        private readonly List<XmlNode> _loadedNodes = new List<XmlNode>();
        private bool _loaded;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        ///     Returns false if the entry already exists
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public bool AddEntry(SettingsEntry entry)
        {
            var type = entry.GetType();
            if (EntryExists(type))
                return false;

            _entries.Add(entry);
            return true;
        }

        public bool AddOrSetEntry(SettingsEntry entry)
        {
            var type = entry.GetType();
            if (EntryExists(type))
                if (!RemoveEntry(type))
                    throw new Exception($"Entry {type} already exists but cannot be removed for an unknow reason");

            _entries.Add(entry);
            return true;
        }

        public bool EntryExists(Type entryType)
        {
            return _entries.Any(entryType.IsInstanceOfType);
        }

        public bool EntryExists<T>()
        {
            return _entries.Any(x => x is T);
        }

        /// <summary>
        ///     Get the settings entry or create it with default constructor if it does not exist
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetOrAddEntry<T>()
            where T : SettingsEntry, new()
        {
            var entries = _entries.OfType<T>().ToArray();

            if (entries.Length > 1)
                throw new Exception($"Found {entries.Length} settings entries of type {typeof(T)}, 1 or 0 expected");

            if (entries.Length == 0)
            {
                var entry = new T();
                var node = GetEntryNode(entry.EntryName);

                if (node != null)
                {
                    var deserializedEntry = (T) new XmlSerializer(typeof(T)).Deserialize(new StringReader(node.InnerXml));

                    _entries.Add(deserializedEntry);
                    _loadedNodes.Remove(node);

                    return deserializedEntry;
                }

                _entries.Add(entry);
                return entry;
            }

            return entries.Single();
        }

        public void Load()
        {
            if (_loaded)
                return;

            if (!File.Exists(FilePath))
            {
                _loaded = true;
                return;
            }

            var document = new XmlDocument();
            document.Load(FilePath);

            var navigator = document.CreateNavigator();
            foreach (XPathNavigator iterator in navigator.Select("//" + NodeName + "[@" + AttributeName + "]"))
            {
                if (!iterator.IsNode)
                    continue;

                var xmlNode = ((IHasXmlNode) iterator).GetNode();

                if (xmlNode.Attributes == null)
                    throw new Exception("xmlNode.Attributes == null");

                if (GetEntryNode(xmlNode.Attributes[AttributeName].Value) != null)
                    throw new Exception($"Found at least 2 entries with name {xmlNode.Attributes[AttributeName].Value}. Expected 1 or 0. File {FilePath} corrupted");

                _loadedNodes.Add(xmlNode);
            }

            _loaded = true;
        }

        public bool RemoveEntry(Type entryType)
        {
            return _entries.RemoveAll(entryType.IsInstanceOfType) > 0;
        }

        public bool RemoveEntry<T>()
        {
            return _entries.RemoveAll(x => x is T) > 0;
        }

        public void Save()
        {
            var directoryName = Path.GetDirectoryName(FilePath);
            if (!Directory.Exists(directoryName))
                if (directoryName != null)
                    Directory.CreateDirectory(directoryName);

            var writer = XmlWriter.Create(FilePath, new XmlWriterSettings
            {
                Indent = true,
                IndentChars = " ",
                Encoding = Encoding.UTF8
            });

            writer.WriteStartDocument();
            writer.WriteStartElement("BotSettings");

            foreach (var entry in _entries)
            {
                writer.WriteStartElement(NodeName);
                writer.WriteAttributeString(AttributeName, entry.EntryName);
                new XmlSerializer(entry.GetType()).Serialize(writer, entry);
                writer.WriteEndElement();
            }

            foreach (var node in _loadedNodes)
                writer.WriteNode(node.CreateNavigator(), true);

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Close();
        }

        #endregion Public Methods
    }
}