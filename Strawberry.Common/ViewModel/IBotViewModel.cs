﻿namespace Strawberry.Common.ViewModel
{
    using System;
    using System.Threading.Tasks;

    using Strawberry.Common.View;

    public interface IBotViewModel
    {
        #region Public Properties

        Bot Bot { get; }

        #endregion

        #region Public Methods and Operators

        Task<ITabViewModel> AddView(IViewModel viewModel, Func<IView> viewFunc);

        #endregion

        Task<bool> RemoveView(IView viewModel);
    }
}