﻿namespace Strawberry.Common.ViewModel
{
    public interface IAccount
    {
        #region Public Properties

        Bot Bot { get; }

        /// <summary>
        ///     Sets and gets the BotViewModel property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        IBotViewModel BotViewModel { get; }

        #endregion
    }
}