﻿namespace Strawberry.Common.ViewModel
{
    using System.Collections.ObjectModel;

    public interface IAccountsViewModel
    {
        #region Public Properties

        ObservableCollection<IAccount> Accounts { get; }

        #endregion
    }
}