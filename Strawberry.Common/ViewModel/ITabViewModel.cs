﻿namespace Strawberry.Common.ViewModel
{
    using Strawberry.Common.View;

    public interface ITabViewModel
    {
        #region Public Properties

        string Header { get; set; }

        bool IsSelected { get; set; }

        /// <summary>
        ///     Sets and gets the View property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        IView View { get; set; }

        #endregion
    }
}