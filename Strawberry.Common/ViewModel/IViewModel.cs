﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Common.View;

namespace Strawberry.Common.ViewModel
{
    public interface IViewModel<T> : INotifyPropertyChanged, IViewModel
        where T : IView
    {
        new T View
        {
            get;
            set;
        }
    }


    public interface IViewModel
    {
        object View
        {
            get;
            set;
        }
    }
}
