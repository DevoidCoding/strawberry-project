﻿// <copyright file="MinimalStats.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;
using NLog;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Stats
{
    /// <summary>
    ///     Stats fields used in fight
    /// </summary>
    public class MinimalStats : IMinimalStats, INotifyPropertyChanged
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MinimalStats(GameFightMinimalStats stats)
        {
            Update(stats);
        }

        public GameActionFightInvisibilityStateEnum InvisibilityState { get; set; }

        public int AirElementReduction { get; set; }

        public int AirResistPercent { get; set; }

        public int CurrentAP { get; set; }

        public int CurrentMP { get; set; }

        public int DodgeAPProbability { get; set; }

        public int DodgeMPProbability { get; set; }

        public int EarthElementReduction { get; set; }

        public int EarthResistPercent { get; set; }

        public int FireElementReduction { get; set; }

        public int FireResistPercent { get; set; }

        public int Health { get; set; }

        /*public bool Summoned
    {
        get;
        set;
    }

    public int Summoner
    {
        get;
        set;
    }*/

        public int Initiative { get; set; }

        public int MaxAP { get; set; }

        public uint MaxHealth { get; set; }

        public uint MaxHealthBase { get; set; }

        public int MaxMP { get; set; }

        public int NeutralElementReduction { get; set; }

        public int NeutralResistPercent { get; set; }

        public int PermanentDamagePercent { get; set; }

        public int Range { get; set; }

        public int TackleBlock { get; set; }

        public int TackleEvade { get; set; }

        public void Update(GameFightMinimalStats stats)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));
            //Summoner = stats.summoner; already processed at MonsterFighter / CharacterFighter level
            //Summoned = stats.summoned;
            Health = (int) stats.LifePoints;
            MaxHealth = stats.MaxLifePoints;
            MaxHealthBase = stats.BaseMaxLifePoints;
            CurrentAP = stats.ActionPoints;
            CurrentMP = stats.MovementPoints;
            MaxAP = stats.MaxActionPoints;
            MaxMP = stats.MaxMovementPoints;
            PermanentDamagePercent = (int) stats.PermanentDamagePercent;
            TackleBlock = stats.TackleBlock;
            TackleEvade = stats.TackleEvade;
            DodgeAPProbability = stats.DodgePALostProbability;
            DodgeMPProbability = stats.DodgePMLostProbability;
            NeutralResistPercent = stats.NeutralElementResistPercent;
            EarthResistPercent = stats.EarthElementResistPercent;
            WaterResistPercent = stats.WaterElementResistPercent;
            AirResistPercent = stats.AirElementResistPercent;
            FireResistPercent = stats.FireElementResistPercent;
            NeutralElementReduction = stats.NeutralElementReduction;
            EarthElementReduction = stats.EarthElementReduction;
            WaterElementReduction = stats.WaterElementReduction;
            AirElementReduction = stats.AirElementReduction;
            FireElementReduction = stats.FireElementReduction;
            //logger.Debug("{0}/{1} AP, {2}/{3} MP, {4}/{5} HP", CurrentAP, MaxAP, CurrentMP, MaxMP, Health, MaxHealth);

            InvisibilityState = (GameActionFightInvisibilityStateEnum) stats.InvisibilityState;
        }

        public int WaterElementReduction { get; set; }

        public int WaterResistPercent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Update(GameFightMinimalStatsPreparation stats)
        {
            Update((GameFightMinimalStats) stats);
            Initiative = (int) stats.Initiative;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        void IMinimalStats.UpdateAP(short delta)
        {
            CurrentAP += delta;
        }

        void IMinimalStats.UpdateHP(int delta)
        {
            Health += delta;
        }

        void IMinimalStats.UpdateMP(short delta)
        {
            CurrentMP += delta;
        }
    }
}