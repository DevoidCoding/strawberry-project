﻿// <copyright file="SpellModification.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Stats
{
    public class SpellModification : INotifyPropertyChanged
    {
        public SpellModification(CharacterSpellModification modification)
        {
            if (modification == null) throw new ArgumentNullException(nameof(modification));
            SpellId = modification.SpellId;
            ModificationType = (CharacterSpellModificationTypeEnum) modification.ModificationType;
            Value = new StatsRow(modification.Value);
        }

        public CharacterSpellModificationTypeEnum ModificationType { get; set; }

        public int SpellId { get; set; }

        public StatsRow Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}