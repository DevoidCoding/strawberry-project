﻿// <copyright file="IMinimalStats.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Stats
{
    public interface IMinimalStats
    {
        int AirElementReduction { get; }

        int AirResistPercent { get; }

        int CurrentAP { get; }

        int CurrentMP { get; }

        int DodgeAPProbability { get; }

        int DodgeMPProbability { get; }

        int EarthElementReduction { get; }

        int EarthResistPercent { get; }

        int FireElementReduction { get; }

        int FireResistPercent { get; }

        int Health { get; }

        int Initiative { get; }

        int MaxAP { get; }

        uint MaxHealth { get; }

        uint MaxHealthBase { get; }

        int MaxMP { get; }

        int NeutralElementReduction { get; }

        int NeutralResistPercent { get; }

        int PermanentDamagePercent { get; }

        int Range { get; }

        int TackleBlock { get; }

        int TackleEvade { get; }

        int WaterElementReduction { get; }

        int WaterResistPercent { get; }

        void Update(GameFightMinimalStats stats);
        void UpdateAP(short deltaAP);
        void UpdateHP(int deltaHP);
        void UpdateMP(short deltaMP);
    }
}