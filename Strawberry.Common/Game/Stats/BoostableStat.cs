﻿// <copyright file="BoostableStat.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Stats
{
    public enum BoostableStat
    {
        Strength = 0xA,
        Vitality,
        Wisdom,
        Chance,
        Agility,
        Intelligence
    }
}