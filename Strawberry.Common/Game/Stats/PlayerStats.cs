﻿// <copyright file="PlayerStats.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using MoreLinq;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Stats
{
    public class PlayerStats : INotifyPropertyChanged, IMinimalStats
    {
        public PlayerStats()
        {
            Fields = new Dictionary<PlayerField, StatsRow>
            {
                {PlayerField.Initiative, new StatsRow(PlayerField.Initiative, x => OnPropertyChanged("Initiative"))},
                {PlayerField.Prospecting, new StatsRow(PlayerField.Prospecting)},
                {
                    PlayerField.AP, new StatsRow(PlayerField.AP, x =>
                    {
                        OnPropertyChanged("MaxAP");
                        OnPropertyChanged("AP");
                    })
                },
                {
                    PlayerField.MP, new StatsRow(PlayerField.MP, x =>
                    {
                        OnPropertyChanged("MaxMP");
                        OnPropertyChanged("MP");
                    })
                },
                {PlayerField.Strength, new StatsRow(PlayerField.Strength, x => OnPropertyChanged("Strength"))},
                {PlayerField.Vitality, new StatsRow(PlayerField.Vitality, x => OnPropertyChanged("Vitality"))},
                {PlayerField.Wisdom, new StatsRow(PlayerField.Wisdom, x => OnPropertyChanged("Wisdom"))},
                {PlayerField.Chance, new StatsRow(PlayerField.Chance, x => OnPropertyChanged("Chance"))},
                {PlayerField.Agility, new StatsRow(PlayerField.Agility, x => OnPropertyChanged("Agility"))},
                {
                    PlayerField.Intelligence,
                    new StatsRow(PlayerField.Intelligence, x => OnPropertyChanged("Intelligence"))
                },
                {PlayerField.Range, new StatsRow(PlayerField.Range, x => OnPropertyChanged("Range"))},
                {PlayerField.SummonLimit, new StatsRow(PlayerField.SummonLimit)},
                {PlayerField.DamageReflection, new StatsRow(PlayerField.DamageReflection)},
                {PlayerField.CriticalHit, new StatsRow(PlayerField.CriticalHit)},
                {PlayerField.CriticalMiss, new StatsRow(PlayerField.CriticalMiss)},
                {PlayerField.HealBonus, new StatsRow(PlayerField.HealBonus)},
                {PlayerField.DamageBonus, new StatsRow(PlayerField.DamageBonus)},
                {PlayerField.WeaponDamageBonusPercent, new StatsRow(PlayerField.WeaponDamageBonusPercent)},
                {PlayerField.DamageBonusPercent, new StatsRow(PlayerField.DamageBonusPercent)},
                {PlayerField.TrapBonus, new StatsRow(PlayerField.TrapBonus)},
                {PlayerField.TrapBonusPercent, new StatsRow(PlayerField.TrapBonusPercent)},
                {
                    PlayerField.PermanentDamagePercent,
                    new StatsRow(PlayerField.PermanentDamagePercent, x => OnPropertyChanged("PermanentDamagePercent"))
                },
                {PlayerField.TackleBlock, new StatsRow(PlayerField.TackleBlock, x => OnPropertyChanged("TackleBlock"))},
                {PlayerField.TackleEvade, new StatsRow(PlayerField.TackleEvade, x => OnPropertyChanged("TackleEvade"))},
                {PlayerField.APAttack, new StatsRow(PlayerField.APAttack)},
                {PlayerField.MPAttack, new StatsRow(PlayerField.MPAttack)},
                {PlayerField.PushDamageBonus, new StatsRow(PlayerField.PushDamageBonus)},
                {PlayerField.CriticalDamageBonus, new StatsRow(PlayerField.CriticalDamageBonus)},
                {PlayerField.NeutralDamageBonus, new StatsRow(PlayerField.NeutralDamageBonus)},
                {PlayerField.EarthDamageBonus, new StatsRow(PlayerField.EarthDamageBonus)},
                {PlayerField.WaterDamageBonus, new StatsRow(PlayerField.WaterDamageBonus)},
                {PlayerField.AirDamageBonus, new StatsRow(PlayerField.AirDamageBonus)},
                {PlayerField.FireDamageBonus, new StatsRow(PlayerField.FireDamageBonus)},
                {
                    PlayerField.DodgeAPProbability,
                    new StatsRow(PlayerField.DodgeAPProbability, x => OnPropertyChanged("DodgeAPProbability"))
                },
                {
                    PlayerField.DodgeMPProbability,
                    new StatsRow(PlayerField.DodgeMPProbability, x => OnPropertyChanged("DodgeMPProbability"))
                },
                {
                    PlayerField.NeutralResistPercent,
                    new StatsRow(PlayerField.NeutralResistPercent, x => OnPropertyChanged("NeutralResistPercent"))
                },
                {
                    PlayerField.EarthResistPercent,
                    new StatsRow(PlayerField.EarthResistPercent, x => OnPropertyChanged("EarthResistPercent"))
                },
                {
                    PlayerField.WaterResistPercent,
                    new StatsRow(PlayerField.WaterResistPercent, x => OnPropertyChanged("WaterResistPercent"))
                },
                {
                    PlayerField.AirResistPercent,
                    new StatsRow(PlayerField.AirResistPercent, x => OnPropertyChanged("AirResistPercent"))
                },
                {
                    PlayerField.FireResistPercent,
                    new StatsRow(PlayerField.FireResistPercent, x => OnPropertyChanged("FireResistPercent"))
                },
                {
                    PlayerField.NeutralElementReduction,
                    new StatsRow(PlayerField.NeutralElementReduction, x => OnPropertyChanged("NeutralElementReduction"))
                },
                {
                    PlayerField.EarthElementReduction,
                    new StatsRow(PlayerField.EarthElementReduction, x => OnPropertyChanged("EarthElementReduction"))
                },
                {
                    PlayerField.WaterElementReduction,
                    new StatsRow(PlayerField.WaterElementReduction, x => OnPropertyChanged("WaterElementReduction"))
                },
                {
                    PlayerField.AirElementReduction,
                    new StatsRow(PlayerField.AirElementReduction, x => OnPropertyChanged("AirElementReduction"))
                },
                {
                    PlayerField.FireElementReduction,
                    new StatsRow(PlayerField.FireElementReduction, x => OnPropertyChanged("FireElementReduction"))
                },
                {PlayerField.PushDamageReduction, new StatsRow(PlayerField.PushDamageReduction)},
                {PlayerField.CriticalDamageReduction, new StatsRow(PlayerField.CriticalDamageReduction)},
                {
                    PlayerField.PvpNeutralResistPercent,
                    new StatsRow(PlayerField.PvpNeutralResistPercent, x => OnPropertyChanged("NeutralResistPercent"))
                },
                {
                    PlayerField.PvpEarthResistPercent,
                    new StatsRow(PlayerField.PvpEarthResistPercent, x => OnPropertyChanged("EarthResistPercent"))
                },
                {
                    PlayerField.PvpWaterResistPercent,
                    new StatsRow(PlayerField.PvpWaterResistPercent, x => OnPropertyChanged("WaterResistPercent"))
                },
                {
                    PlayerField.PvpAirResistPercent,
                    new StatsRow(PlayerField.PvpAirResistPercent, x => OnPropertyChanged("AirResistPercent"))
                },
                {
                    PlayerField.PvpFireResistPercent,
                    new StatsRow(PlayerField.PvpFireResistPercent, x => OnPropertyChanged("FireResistPercent"))
                },
                {
                    PlayerField.PvpNeutralElementReduction,
                    new StatsRow(PlayerField.PvpNeutralElementReduction,
                        x => OnPropertyChanged("NeutralElementReduction"))
                },
                {
                    PlayerField.PvpEarthElementReduction,
                    new StatsRow(PlayerField.PvpEarthElementReduction, x => OnPropertyChanged("EarthElementReduction"))
                },
                {
                    PlayerField.PvpWaterElementReduction,
                    new StatsRow(PlayerField.PvpWaterElementReduction, x => OnPropertyChanged("WaterElementReduction"))
                },
                {
                    PlayerField.PvpAirElementReduction,
                    new StatsRow(PlayerField.PvpAirElementReduction, x => OnPropertyChanged("AirElementReduction"))
                },
                {
                    PlayerField.PvpFireElementReduction,
                    new StatsRow(PlayerField.PvpFireElementReduction, x => OnPropertyChanged("FireElementReduction"))
                }
            };
            SpellsModifications = new ObservableCollectionMT<SpellModification>();
            InvisibilityState = GameActionFightInvisibilityStateEnum.Visible;
        }

        public PlayerStats(PlayedCharacter owner, CharacterCharacteristicsInformations stats)
            : this(owner)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));
            Update(stats);
        }

        public PlayerStats(PlayedCharacter owner)
            : this()
        {
            if (owner == null) throw new ArgumentNullException(nameof(owner));
            Owner = owner;
        }

        public StatsRow Agility => this[PlayerField.Agility];

        public StatsRow AP => this[PlayerField.AP];

        public StatsRow Chance => this[PlayerField.Chance];

        public ushort EnergyPoints { get; set; }

        public double Experience { get; set; }

        public double ExperienceLevelFloor { get; set; }

        public double ExperienceNextLevelFloor { get; set; }

        public Dictionary<PlayerField, StatsRow> Fields { get; set; }

        public StatsRow Intelligence => this[PlayerField.Intelligence];

        public GameActionFightInvisibilityStateEnum InvisibilityState { get; set; }

        [IndexerName("Item")]
        public StatsRow this[PlayerField name]
        {
            get
            {
                StatsRow value;
                return Fields.TryGetValue(name, out value) ? value : null;
            }
            set
            {
                if (!Fields.ContainsKey(name))
                    Fields.Add(name, value);
                else
                    Fields[name] = value;
            }
        }

        public ulong Kamas { get; set; }

        public ushort MaxEnergyPoints { get; set; }

        public StatsRow MP => this[PlayerField.MP];

        public PlayedCharacter Owner { get; set; }

        public bool PvP { get; set; }

        public ObservableCollectionMT<SpellModification> SpellsModifications { get; set; }

        public int SpellsPoints { get; set; }

        public int StatsPoints { get; set; }

        public StatsRow Strength => this[PlayerField.Strength];

        public StatsRow Vitality => this[PlayerField.Vitality];

        public StatsRow Wisdom => this[PlayerField.Wisdom];

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void Update(CharacterCharacteristicsInformations stats)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));
            Experience = stats.Experience;
            ExperienceLevelFloor = stats.ExperienceLevelFloor;
            ExperienceNextLevelFloor = stats.ExperienceNextLevelFloor;

            Kamas = stats.Kamas;
            StatsPoints = stats.StatsPoints;
            SpellsPoints = stats.SpellsPoints;
            Health = (int) stats.LifePoints;
            MaxHealth = stats.MaxLifePoints;

            EnergyPoints = stats.EnergyPoints;
            MaxEnergyPoints = stats.MaxEnergyPoints;
            CurrentAP = stats.ActionPointsCurrent;
            CurrentMP = stats.MovementPointsCurrent;

            this[PlayerField.Initiative].Update(stats.Initiative);
            this[PlayerField.Prospecting].Update(stats.Prospecting);
            this[PlayerField.AP].Update(stats.ActionPoints);
            this[PlayerField.MP].Update(stats.MovementPoints);
            this[PlayerField.Strength].Update(stats.Strength);
            this[PlayerField.Vitality].Update(stats.Vitality);
            this[PlayerField.Wisdom].Update(stats.Wisdom);
            this[PlayerField.Chance].Update(stats.Chance);
            this[PlayerField.Agility].Update(stats.Agility);
            this[PlayerField.Intelligence].Update(stats.Intelligence);
            this[PlayerField.Range].Update(stats.Range);
            this[PlayerField.SummonLimit].Update(stats.SummonableCreaturesBoost);
            this[PlayerField.DamageReflection].Update(stats.Reflect);
            this[PlayerField.CriticalHit].Update(stats.CriticalHit);
            this[PlayerField.CriticalMiss].Update(stats.CriticalMiss);
            this[PlayerField.HealBonus].Update(stats.HealBonus);
            this[PlayerField.DamageBonus].Update(stats.AllDamagesBonus);
            this[PlayerField.WeaponDamageBonusPercent].Update(stats.WeaponDamagesBonusPercent);
            this[PlayerField.DamageBonusPercent].Update(stats.DamagesBonusPercent);
            this[PlayerField.TrapBonus].Update(stats.TrapBonus);
            this[PlayerField.TrapBonusPercent].Update(stats.TrapBonusPercent);
            this[PlayerField.PermanentDamagePercent].Update(stats.PermanentDamagePercent);
            this[PlayerField.TackleBlock].Update(stats.TackleBlock);
            this[PlayerField.TackleEvade].Update(stats.TackleEvade);
            this[PlayerField.APAttack].Update(stats.PAAttack);
            this[PlayerField.MPAttack].Update(stats.PMAttack);
            this[PlayerField.PushDamageBonus].Update(stats.PushDamageBonus);
            this[PlayerField.CriticalDamageBonus].Update(stats.CriticalDamageBonus);
            this[PlayerField.NeutralDamageBonus].Update(stats.NeutralDamageBonus);
            this[PlayerField.EarthDamageBonus].Update(stats.EarthDamageBonus);
            this[PlayerField.WaterDamageBonus].Update(stats.WaterDamageBonus);
            this[PlayerField.AirDamageBonus].Update(stats.AirDamageBonus);
            this[PlayerField.FireDamageBonus].Update(stats.FireDamageBonus);
            this[PlayerField.DodgeAPProbability].Update(stats.DodgePALostProbability);
            this[PlayerField.DodgeMPProbability].Update(stats.DodgePMLostProbability);
            this[PlayerField.NeutralResistPercent].Update(stats.NeutralElementResistPercent);
            this[PlayerField.EarthResistPercent].Update(stats.EarthElementResistPercent);
            this[PlayerField.WaterResistPercent].Update(stats.WaterElementResistPercent);
            this[PlayerField.AirResistPercent].Update(stats.AirElementResistPercent);
            this[PlayerField.FireResistPercent].Update(stats.FireElementResistPercent);
            this[PlayerField.NeutralElementReduction].Update(stats.NeutralElementReduction);
            this[PlayerField.EarthElementReduction].Update(stats.EarthElementReduction);
            this[PlayerField.WaterElementReduction].Update(stats.WaterElementReduction);
            this[PlayerField.AirElementReduction].Update(stats.AirElementReduction);
            this[PlayerField.FireElementReduction].Update(stats.FireElementReduction);
            this[PlayerField.PushDamageReduction].Update(stats.PushDamageReduction);
            this[PlayerField.CriticalDamageReduction].Update(stats.CriticalDamageReduction);
            this[PlayerField.PvpNeutralResistPercent].Update(stats.PvpNeutralElementResistPercent);
            this[PlayerField.PvpEarthResistPercent].Update(stats.PvpEarthElementResistPercent);
            this[PlayerField.PvpWaterResistPercent].Update(stats.PvpWaterElementResistPercent);
            this[PlayerField.PvpAirResistPercent].Update(stats.PvpAirElementResistPercent);
            this[PlayerField.PvpFireResistPercent].Update(stats.PvpFireElementResistPercent);
            this[PlayerField.PvpNeutralElementReduction].Update(stats.PvpNeutralElementReduction);
            this[PlayerField.PvpEarthElementReduction].Update(stats.PvpEarthElementReduction);
            this[PlayerField.PvpWaterElementReduction].Update(stats.PvpWaterElementReduction);
            this[PlayerField.AirElementReduction].Update(stats.PvpAirElementReduction);
            this[PlayerField.PvpFireElementReduction].Update(stats.PvpFireElementReduction);
            // TODO : Shield
            //this[PlayerField.MeleeDamageDonePercent].Update(stats.MeleeDamageDonePercent);
            //this[PlayerField.MeleeDamageReceivedPercent].Update(stats.MeleeDamageReceivedPercent);
            //this[PlayerField.RangedDamageDonePercent].Update(stats.RangedDamageDonePercent);
            //this[PlayerField.RangedDamageReceivedPercent].Update(stats.RangedDamageReceivedPercent);
            //this[PlayerField.WeaponDamageDonePercent].Update(stats.WeaponDamageDonePercent);
            //this[PlayerField.WeaponDamageReceivedPercent].Update(stats.WeaponDamageReceivedPercent);
            //this[PlayerField.SpellDamageDonePercent].Update(stats.SpellDamageDonePercent);
            //this[PlayerField.SpellDamageReceivedPercent].Update(stats.SpellDamageReceivedPercent);

            SpellsModifications.Clear();
            stats.SpellModifications.Select(entry => new SpellModification(entry)).ForEach(SpellsModifications.Add);

            OnPropertyChanged("Item[]");
        }


        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        void IMinimalStats.UpdateAP(short delta)
        {
            CurrentAP += delta;
        }

        void IMinimalStats.UpdateHP(int delta)
        {
            Health += delta;
        }

        void IMinimalStats.UpdateMP(short delta)
        {
            CurrentMP += delta;
        }

        #region IMinimalStats Members

        public int Initiative => this[PlayerField.Initiative].Total;

        public int Health { get; set; }

        public uint MaxHealth { get; set; }

        public uint MaxHealthBase { get; set; }

        public int MaxAP => this[PlayerField.AP].Total;

        public int MaxMP => this[PlayerField.MP].Total;

        public int Range => this[PlayerField.Range].Total;

        public int PermanentDamagePercent => this[PlayerField.PermanentDamagePercent].Total;

        public int TackleBlock => this[PlayerField.TackleBlock].Total;

        public int TackleEvade => this[PlayerField.TackleEvade].Total;

        public int DodgeAPProbability => this[PlayerField.DodgeAPProbability].Total;

        public int DodgeMPProbability => this[PlayerField.DodgeMPProbability].Total;

        public int SummonLimit => this[PlayerField.SummonLimit].Total;

        public int NeutralResistPercent => this[PvP ? PlayerField.PvpNeutralResistPercent : PlayerField.NeutralResistPercent].Total;

        public int EarthResistPercent => this[PvP ? PlayerField.PvpEarthResistPercent : PlayerField.EarthResistPercent].Total;

        public int WaterResistPercent => this[PvP ? PlayerField.PvpWaterResistPercent : PlayerField.WaterResistPercent].Total;

        public int AirResistPercent => this[PvP ? PlayerField.PvpAirResistPercent : PlayerField.AirResistPercent].Total;

        public int FireResistPercent => this[PvP ? PlayerField.PvpFireResistPercent : PlayerField.FireResistPercent].Total;

        public int NeutralElementReduction => this[PvP ? PlayerField.PvpNeutralElementReduction : PlayerField.NeutralElementReduction].Total;

        public int EarthElementReduction => this[PvP ? PlayerField.PvpEarthElementReduction : PlayerField.EarthElementReduction].Total;

        public int WaterElementReduction => this[PvP ? PlayerField.PvpWaterElementReduction : PlayerField.WaterElementReduction].Total;

        public int AirElementReduction => this[PvP ? PlayerField.PvpAirElementReduction : PlayerField.AirElementReduction].Total;

        public int FireElementReduction => this[PvP ? PlayerField.PvpFireElementReduction : PlayerField.FireElementReduction].Total;

        public int CurrentAP { get; set; }

        public int CurrentMP { get; set; }

        public void Update(GameFightMinimalStats stats)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));

            Health = (int) stats.LifePoints;
            MaxHealth = stats.MaxLifePoints;
            MaxHealthBase = stats.BaseMaxLifePoints;
            InvisibilityState = (GameActionFightInvisibilityStateEnum) stats.InvisibilityState;


            this[PlayerField.AP].Update(stats.MaxActionPoints);
            CurrentAP = stats.ActionPoints;
            this[PlayerField.MP].Update(stats.MaxMovementPoints);
            CurrentMP = stats.MovementPoints;

            this[PlayerField.PermanentDamagePercent].Update(stats.PermanentDamagePercent);
            this[PlayerField.TackleBlock].Update(stats.TackleBlock);
            this[PlayerField.TackleEvade].Update(stats.TackleEvade);
            this[PlayerField.DodgeAPProbability].Update(stats.DodgePALostProbability);
            this[PlayerField.DodgeMPProbability].Update(stats.DodgePMLostProbability);
            this[PlayerField.NeutralResistPercent].Update(stats.NeutralElementResistPercent);
            this[PlayerField.EarthResistPercent].Update(stats.EarthElementResistPercent);
            this[PlayerField.WaterResistPercent].Update(stats.WaterElementResistPercent);
            this[PlayerField.AirResistPercent].Update(stats.AirElementResistPercent);
            this[PlayerField.FireResistPercent].Update(stats.FireElementResistPercent);
            this[PlayerField.NeutralElementReduction].Update(stats.NeutralElementReduction);
            this[PlayerField.EarthElementReduction].Update(stats.EarthElementReduction);
            this[PlayerField.WaterElementReduction].Update(stats.WaterElementReduction);
            this[PlayerField.AirElementReduction].Update(stats.AirElementReduction);
            this[PlayerField.FireElementReduction].Update(stats.FireElementReduction);

            OnPropertyChanged("Item[]");
        }

        #endregion
    }
}