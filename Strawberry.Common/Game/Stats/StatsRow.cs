﻿// <copyright file="StatsRow.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Stats
{
    public class StatsRow : INotifyPropertyChanged
    {
        private readonly Action<StatsRow> _onChanged;

        public StatsRow()
        {
        }

        public StatsRow(PlayerField field)
        {
            Field = field;
        }

        internal StatsRow(PlayerField field, Action<StatsRow> onChanged)
            : this(field)
        {
            _onChanged = onChanged;
        }

        public StatsRow(CharacterBaseCharacteristic characteristic)
        {
            if (characteristic == null) throw new ArgumentNullException(nameof(characteristic));
            Base = characteristic.Base;
            Equipements = characteristic.ObjectsAndMountBonus;
            AlignBonus = characteristic.AlignGiftBonus;
            Context = characteristic.ContextModif;
        }

        public StatsRow(CharacterBaseCharacteristic characteristic, PlayerField field)
        {
            if (characteristic == null) throw new ArgumentNullException(nameof(characteristic));
            Base = characteristic.Base;
            Equipements = characteristic.ObjectsAndMountBonus;
            AlignBonus = characteristic.AlignGiftBonus;
            Context = characteristic.ContextModif;
            Field = field;
        }

        internal StatsRow(CharacterBaseCharacteristic characteristic, PlayerField field, Action<StatsRow> onChanged)
        {
            _onChanged = onChanged;
        }

        public int AlignBonus { get; set; }

        public int Base { get; set; }

        public int Context { get; set; }

        public int Equipements { get; set; }

        public PlayerField Field { get; set; }

        public int Total => Base + Equipements + AlignBonus + Context;

        public event PropertyChangedEventHandler PropertyChanged;

        public static int operator +(int i1, StatsRow s1)
        {
            return i1 + s1.Total;
        }

        public static int operator +(StatsRow s1, StatsRow s2)
        {
            return s1.Total + s2.Total;
        }

        public static int operator -(int i1, StatsRow s1)
        {
            return i1 - s1.Total;
        }

        public static int operator -(StatsRow s1, StatsRow s2)
        {
            return s1.Total - s2.Total;
        }

        public void Update(CharacterBaseCharacteristic characteristic)
        {
            Base = characteristic.Base;
            Equipements = characteristic.ObjectsAndMountBonus;
            Context = characteristic.ContextModif;
            AlignBonus = characteristic.AlignGiftBonus;
        }

        public void Update(int @base)
        {
            Base = @base;
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            _onChanged?.Invoke(this);
            
            PropertyChanged?.Invoke(this, e);
        }

        public void Update(uint @base)
        {
            Base = (int)@base;
        }
    }
}