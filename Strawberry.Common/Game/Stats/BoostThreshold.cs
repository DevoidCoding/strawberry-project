﻿// <copyright file="BoostThreshold.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;

namespace Strawberry.Common.Game.Stats
{
    public class BoostThreshold
    {
        public BoostThreshold(uint pointsThreshold, uint pointsPerBoost, uint boostPerPoints = 1)
        {
            PointsThreshold = pointsThreshold;
            PointsPerBoost = pointsPerBoost;
            BoostPerPoints = boostPerPoints;
        }

        public BoostThreshold(List<uint> threshold)
        {
            if (threshold.Count != 3 && threshold.Count != 2)
                throw new ArgumentException("threshold.Count != 3 && threshold.Count != 2");

            PointsThreshold = threshold[0];
            PointsPerBoost = threshold[1];
            BoostPerPoints = threshold.Count > 2 ? threshold[2] : 1;
        }

        public uint BoostPerPoints { get; set; }

        public uint PointsPerBoost { get; set; }

        public uint PointsThreshold { get; set; }
    }
}