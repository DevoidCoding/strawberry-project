﻿// <copyright file="SpellCastHistory.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Core.Collections;

namespace Strawberry.Common.Game.Fights
{
    public class SpellCastHistory
    {
        private readonly ObservableCollectionMT<SpellCast> _casts = new ObservableCollectionMT<SpellCast>();

        public SpellCastHistory(Fighter fighter)
        {
            Fighter = fighter;
            Casts = new ReadOnlyObservableCollectionMT<SpellCast>(_casts);
        }

        public ReadOnlyObservableCollectionMT<SpellCast> Casts { get; }

        public Fighter Fighter { get; private set; }

        public void AddSpellCast(SpellCast cast)
        {
            _casts.Add(cast);
        }
    }
}