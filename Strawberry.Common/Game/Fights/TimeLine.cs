﻿// <copyright file="TimeLine.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Fights
{
    public class TimeLine : INotifyPropertyChanged
    {
        private readonly ObservableCollectionMT<Fighter> _fighters = new ObservableCollectionMT<Fighter>();

        public TimeLine(Fight fight)
        {
            Fight = fight;
            Index = -1;
            Fighters = new ReadOnlyObservableCollectionMT<Fighter>(_fighters);
        }

        public Fighter CurrentPlayer { get; private set; }

        public Fight Fight { get; }

        /// <summary>
        ///     This list is ordered by the fighters turns
        /// </summary>
        public ReadOnlyObservableCollectionMT<Fighter> Fighters { get; }

        public int Index { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public int GetFighterIndex(Fighter fighter)
        {
            return Fighters.IndexOf(fighter);
        }

        public Fighter GetNextPlayer()
        {
            if (Fighters.Count == 0 || Fighters.All(entry => !entry.CanPlay()))
            {
                return null;
            }

            var counter = 0;
            var index = Index + 1 < Fighters.Count ? Index + 1 : 0;
            while (!Fighters[index].CanPlay() && counter < Fighters.Count)
            {
                index = index + 1 < Fighters.Count ? index + 1 : 0;

                counter++;
            }

            if (!Fighters[index].CanPlay()) // no fighter can play
            {
                Index = -1;
                return null;
            }

            return Fighters[index];
        }

        /// <summary>
        ///     Get all fighters who will play before the turn of <paramref name="fighter" />
        /// </summary>
        /// <param name="fighter"></param>
        /// <returns></returns>
        public Fighter[] GetNextPlayers(Fighter fighter)
        {
            var result = new List<Fighter>();
            if (CurrentPlayer == fighter || Fighters.Count == 0 || Fighters.All(entry => !entry.CanPlay()))
            {
                return new Fighter[0];
            }

            var currentFighter = GetNextPlayer();
            var index = Index + 1 < Fighters.Count ? Index + 1 : 0;
            while (currentFighter != fighter)
            {
                if (currentFighter.CanPlay())
                    result.Add(currentFighter);

                index = index + 1 < Fighters.Count ? index + 1 : 0;
                currentFighter = GetNextPlayer();
            }

            return result.ToArray();
        }

        public void InsertFighter(Fighter fighter, int index)
        {
            _fighters.Insert(index, fighter);

            if (Index >= index)
                Index++;
        }

        public void RefreshTimeLine()
        {
            var redFighters = Fight.RedTeam.Fighters.
                                    OrderByDescending(fighter => GetRealInitiative(fighter));
            var blueFighters = Fight.BlueTeam.Fighters.
                                     OrderByDescending(fighter => GetRealInitiative(fighter));

            var redFighterFirst = !(Fight.RedTeam.Fighters.Count == 0 || Fight.BlueTeam.Fighters.Count == 0) &&
                                  GetRealInitiative(redFighters.First()) > GetRealInitiative(blueFighters.First());

            var redEnumerator = redFighters.GetEnumerator();
            var blueEnumerator = blueFighters.GetEnumerator();
            var timeLine = new List<Fighter>();

            bool hasRed;
            var hasBlue = false;
            while ((hasRed = redEnumerator.MoveNext()) || (hasBlue = blueEnumerator.MoveNext()))
            {
                if (redFighterFirst)
                {
                    if (hasRed)
                        timeLine.Add(redEnumerator.Current);

                    if (hasBlue)
                        timeLine.Add(blueEnumerator.Current);
                }
                else
                {
                    if (hasBlue)
                        timeLine.Add(blueEnumerator.Current);

                    if (hasRed)
                        timeLine.Add(redEnumerator.Current);
                }
            }

            _fighters.Clear();
            foreach (var fighter in timeLine)
            {
                _fighters.Add(fighter);
            }
        }

        public void RefreshTimeLine(IEnumerable<double> ids)
        {
            var timeLine = ids.Select(entry => Fight.GetFighter(entry));

            _fighters.Clear();
            foreach (var fighter in timeLine)
            {
                _fighters.Add(fighter);
            }
        }

        public void ResetCurrentPlayer()
        {
            CurrentPlayer = null;
            Index = -1;
        }

        public void SetCurrentPlayer(Fighter fighter)
        {
            CurrentPlayer = fighter;

            var index = GetFighterIndex(fighter);

            if (index == -1)
                throw new Exception($"Something goes wrong, fighter {fighter.Id} not found in the timeline");

            Index = index;
        }

        public void Update(GameFightSynchronizeMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            RefreshTimeLine(msg.Fighters.Select(x => x.ContextualId));
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private long GetRealInitiative(Fighter fighter)
        {
            return fighter.Stats.Initiative*fighter.Stats.Health/fighter.Stats.MaxHealth;
        }
    }
}