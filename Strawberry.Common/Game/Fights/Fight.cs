﻿// <copyright file="Fight.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.Effects;
using Strawberry.Common.Game.Spells;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Fights
{
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Core.Reflection;

    public class Fight : MapContext<Fighter>
    {
        public delegate void ActorAddedRemovedHandler(Fight fight, Fighter fighter);

        public delegate void StateChangedHandler(Fight fight, FightPhase phase);


        public delegate void TurnHandler(Fight fight, Fighter fighter);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private SortedList<Fighter, List<Tuple<AbstractFightDispellableEffect, ushort>>> _effects;


        public Fight(GameFightJoinMessage msg, Map map)
        {
            Map = map;
            StartTime = DateTime.Now - TimeSpan.FromMilliseconds(msg.TimeMaxBeforeFightStart);
            CanCancelFight = msg.CanBeCancelled;
            CanSayReady = msg.CanSayReady;
            Type = (FightTypeEnum) msg.FightType;

            RedTeam = new FightTeam(this, FightTeamColor.Red);
            BlueTeam = new FightTeam(this, FightTeamColor.Blue);

            if (msg.TimeMaxBeforeFightStart > 0)
                Phase = FightPhase.Placement;

            TimeLine = new TimeLine(this);
        }

        public IEnumerable<Fighter> AliveActors
        {
            get { return Actors.Where(actor => actor.IsAlive); }
        }

        public IEnumerable<Cell> AliveActorsCells
        {
            get { return AliveActors.Select(actor => actor.Cell); }
        }

        public FightTeam BlueTeam { get; }

        public bool CanCancelFight { get; private set; }

        public bool CanSayReady { get; private set; }

        public override CellList Cells => Map.Cells;

        public Fighter CurrentPlayer => TimeLine.CurrentPlayer;

        public SortedList<Fighter, List<Tuple<AbstractFightDispellableEffect, ushort>>> Effects => _effects ??
                                                                                                  (_effects =
                                                                                                      new SortedList
                                                                                                          <Fighter,
                                                                                                              List
                                                                                                                  <
                                                                                                                      Tuple
                                                                                                                          <
                                                                                                                              AbstractFightDispellableEffect,
                                                                                                                              ushort
                                                                                                                              >
                                                                                                                      >>
                                                                                                          ());

        public int Id { get; private set; }

        public Map Map { get; }

        public FightPhase Phase { get; private set; }

        public FightTeam RedTeam { get; }

        public uint Round { get; private set; }

        public DateTime StartTime { get; private set; }

        public TimeLine TimeLine { get; }

        public TimeSpan TimeUntilStart
            => Phase == FightPhase.Placement && StartTime > DateTime.Now ? StartTime - DateTime.Now : TimeSpan.Zero;

        public FightTypeEnum Type { get; private set; }

        public override bool UsingNewMovementSystem => Map.UsingNewMovementSystem;
        public event ActorAddedRemovedHandler ActorAdded;

        public event ActorAddedRemovedHandler ActorRemoved;

        public override bool AddActor(Fighter actor)
        {
            if (actor == null) throw new ArgumentNullException(nameof(actor));

            // do it before because an existing fighter can be removed
            var added = base.AddActor(actor);

            actor.Team.AddFighter(actor);

            if (Phase == FightPhase.Placement)
                TimeLine.RefreshTimeLine();

            OnActorAdded(actor);

            return added;
        }

        public void AddActor(GameFightFighterInformations fighter)
        {
            if (fighter == null) throw new ArgumentNullException(nameof(fighter));

            Bot bot = BotManager.Instance.GetCurrentBot();

            var existingFighter = GetFighter(fighter.ContextualId);

            if (existingFighter != null)
            {
                existingFighter.Update(fighter);
            }
            else
            {
                // normally we don't know which is our team before being added to the fight
                if (fighter.ContextualId == bot.Character.Id)
                {
                    bot.Character.Fighter.SetTeam(GetTeam(fighter.TeamId));
                    AddActor(bot.Character.Fighter);
                }
                else
                {
                    AddActor(CreateFighter(fighter));
                }
            }
        }

        public void DispelEffect(int boostUid, double targetId)
        {
            var fighter = GetFighter(targetId);
            foreach (var effectList in Effects)
                foreach (var effectT in effectList.Value.ToArray())
                    if (effectT.Item1.Uid == boostUid && effectT.Item1.TargetId == targetId &&
                        effectT.Item1.Dispelable != 2)
                    {
                        if (fighter is PlayedFighter)
                        {
                            var playedFighter = fighter as PlayedFighter;
                            var spell = new Spell(new SpellItem(effectT.Item1.SpellId, 1));
                            playedFighter.Character.SendInformation(
                                "Effect {0} from spell {1} dispeled (dispelable : {2})",
                                (EffectsEnum) effectT.Item1.TypeID, spell.Name, effectT.Item1.Dispelable);
                        }
                        Effects[effectList.Key].Remove(effectT);
                    }
        }

        public void DispelSpell(double targetId, int spellId)
        {
            var fighter = GetFighter(targetId);
            foreach (var effectList in Effects)
                foreach (var effectT in effectList.Value.ToArray())
                    if (effectT.Item1.TargetId == targetId && effectT.Item1.SpellId == spellId &&
                        effectT.Item1.Dispelable != 2)
                    {
                        if (fighter is PlayedFighter)
                        {
                            var playedFighter = fighter as PlayedFighter;
                            var spell = new Spell(new SpellItem(effectT.Item1.SpellId, 1));
                            playedFighter.Character.SendInformation(
                                "Effect {0} from spell {1} dispeled (dispelable : {2})",
                                (EffectsEnum) effectT.Item1.SpellId, spell.Name, effectT.Item1.Dispelable);
                        }

                        Effects[effectList.Key].Remove(effectT);
                    }
        }

        public void DispelTarget(double targetId)
        {
            var fighter = GetFighter(targetId);
            foreach (var effectList in Effects)
                foreach (var effectT in effectList.Value.ToArray())
                    if (effectT.Item1.TargetId == targetId && effectT.Item1.Dispelable != 2)
                    {
                        if (fighter is PlayedFighter)
                        {
                            var playedFighter = fighter as PlayedFighter;
                            var spell = new Spell(new SpellItem(effectT.Item1.SpellId, 1));
                            playedFighter.Character.SendInformation(
                                "Effect {0} from spell {1} dispeled (dispelable : {2})",
                                (EffectsEnum) effectT.Item1.TypeID, spell.Name, effectT.Item1.Dispelable);
                        }
                        Effects[effectList.Key].Remove(effectT);
                    }
        }

        public void EndFight(GameFightEndMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            SetPhase(FightPhase.Ended);

            // todo : manage the panel
        }

        public void EndSequence(SequenceEndMessage message)
        {
            var fighter = GetFighter(message.AuthorId);

            if (fighter == null)
                throw new InvalidOperationException($"Fighter {message.AuthorId} not found, cannot end sequence");

            SequenceEnded?.Invoke(this, fighter);

            //if ((TimeLine.CurrentPlayer != null) && (message.authorId != TimeLine.CurrentPlayer.Id))
            //    throw new InvalidOperationException(string.Format("EndSequence authorId {0} is not current Player {1}", message.authorId, fighter.Id));
            //if (CurrentPlayer is PlayedFighter)
            //{
            //    (CurrentPlayer as PlayedFighter).Character.SendMessage(String.Format("EndSequence : of fighter {0}, CurrentPlayer {1}, TimeLine.CurrentPlayer {2}", fighter, CurrentPlayer, TimeLine.CurrentPlayer), Color.Gray);
            //}
            if (CurrentPlayer != null && CurrentPlayer.Id == message.AuthorId)
                CurrentPlayer.NotifySequenceEnded();
        }

        public void EndTurn()
        {
            TurnEnded?.Invoke(this, TimeLine.CurrentPlayer);

            TimeLine.CurrentPlayer?.NotifyTurnEnded();

            TimeLine.ResetCurrentPlayer();
        }

        public Fighter GetFighter(double id)
        {
            return Actors.FirstOrDefault(entry => entry.Id == id);
        }

        public T GetFighter<T>(int id) where T : Fighter
        {
            return (T) Actors.FirstOrDefault(entry => entry is T && entry.Id == id);
        }

        public Fighter GetFighter(Cell cell)
        {
            // i assume 2 fighters can be on the same cell (i.g if someone carry someone)
            return Actors.FirstOrDefault(entry => entry.Cell == cell);
        }


        /// <summary>
        ///     To get the fighters sort by Initiative use TimeLine.Fighters
        /// </summary>
        /// <returns></returns>
        public Fighter[] GetFighters()
        {
            return Actors.ToArray();
        }

        public Fighter[] GetFighters(Cell centerCell, int radius)
        {
            return Actors.Where(entry => entry.Cell.IsInRadius(centerCell, radius)).ToArray();
        }

        public Fighter[] GetFighters(Cell centerCell, int minRadius, int radius)
        {
            return Actors.Where(entry => entry.Cell.IsInRadius(centerCell, minRadius, radius)).ToArray();
        }

        public FightTeam GetTeam(sbyte color)
        {
            return GetTeam((FightTeamColor) color);
        }

        public FightTeam GetTeam(FightTeamColor color)
        {
            if (color == FightTeamColor.Red) return RedTeam;
            if (color == FightTeamColor.Blue) return BlueTeam;
            throw new Exception($"Color {color} is not a valid team id !");
        }

        public IEnumerable<short> GetTrappedCellIds()
        {
            return TrappedCells.Values.SelectMany(list => list);
        }

        public bool HasFightStarted()
        {
            return DateTime.Now >= StartTime;
        }

        public override bool IsTrapped(short cellId)
        {
            return TrappedCells.Values.Any(list => list.Contains(cellId));
        }

        public void OnActorAdded(Fighter fighter)
        {
            ActorAdded?.Invoke(this, fighter);
        }

        public void OnActorRemoved(Fighter fighter)
        {
            ActorRemoved?.Invoke(this, fighter);
        }

        public override bool RemoveActor(Fighter actor)
        {
            if (actor.Team.RemoveFighter(actor) && base.RemoveActor(actor))
            {
                OnActorRemoved(actor);
                return true;
            }

            return false;
        }

        public event TurnHandler SequenceEnded;

        public void SetRound(uint round)
        {
            Round = round;

            //evnt
        }

        public override void SetTrap(int noTrap, int cellId, int radius)
        {
            if (TrappedCells.ContainsKey(noTrap))
                UnsetTrap(noTrap);
            var newSet =
                new SortedSet<short>(Cells[cellId].GetAllCellsInRange(0, radius, false, null).Select(cell => cell.Id));
            TrappedCells[noTrap] = newSet;
        }

        public void StartFight()
        {
            if (Phase != FightPhase.Placement)
            {
                Logger.Error($"Cannot start the fight : the fight is not in Placement phase (Phase={Phase})");
                return;
            }

            SetPhase(FightPhase.Fighting);
            StartTime = DateTime.Now;
        }

        public void StartTurn(double playerId)
        {
            var fighter = GetFighter(playerId);

            if (fighter == null)
                throw new InvalidOperationException($"Fighter {playerId} not found, cannot start turn");

            TimeLine.SetCurrentPlayer(fighter);

            var evnt = TurnStarted;
            if (evnt != null)
                evnt(this, TimeLine.CurrentPlayer);

            if (TimeLine.CurrentPlayer != null)
            {
                if (Effects.ContainsKey(TimeLine.CurrentPlayer))
                    foreach (var effectT in Effects[TimeLine.CurrentPlayer].ToArray())
                    {
                        effectT.Item1.TurnDuration--;
                        if (effectT.Item1.TurnDuration == 0) // Effect expired
                            Effects[TimeLine.CurrentPlayer].Remove(effectT);
                    }
                TimeLine.CurrentPlayer.NotifyTurnStarted();
            }
        }

        public event StateChangedHandler StateChanged;


        public override void Tick(int dt)
        {
            if (RedTeam != null)
                foreach (var fighter in RedTeam.Fighters)
                {
                    fighter.Tick(dt);
                }
            if (BlueTeam != null)
                foreach (var fighter in BlueTeam.Fighters)
                {
                    fighter.Tick(dt);
                }
        }

        public event TurnHandler TurnEnded;
        public event TurnHandler TurnStarted;

        public void Update(GameFightPlacementPossiblePositionsMessage msg)
        {
            if (msg == null)
                throw new ArgumentException(nameof(msg));

            RedTeam.Update(msg);
            BlueTeam.Update(msg);

            SetPhase(FightPhase.Placement);
        }

        public void Update(GameEntitiesDispositionMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            foreach (var disposition in msg.Dispositions)
            {
                var fighter = GetFighter(disposition.Id);

                // seems like the client don't cares
                fighter?.Update(disposition);
            }
        }

        public void Update(GameFightUpdateTeamMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Id = msg.FightId;

            GetTeam(msg.Team.TeamId).Update(msg.Team);
        }

        public void Update(GameFightHumanReadyStateMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            var fighter = GetFighter(msg.CharacterId);

            fighter.IsReady = msg.IsReady;
        }

        public void Update(Bot bot, GameFightSynchronizeMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            TimeLine.Update(msg);


            foreach (var info in msg.Fighters)
            {
                var fighter = GetFighter(info.ContextualId);

                if (fighter == null)
                {
                    Logger.Error($"(GameFightSynchronizeMessage) Fighter {info.ContextualId} not found");
                }
                else
                {
                    fighter.Update(info);
                }
            }
        }

        public void Update(GameFightRefreshFighterMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            var fighter = GetFighter(msg.Informations.ContextualId);
            if (fighter == null)
            {
                Logger.Error("(GameFightRefreshFighterMessage) Fighter {0} not found", msg.Informations.ContextualId);
                return;
            }

            fighter.Update(msg.Informations);

            if (Phase == FightPhase.Placement)
                TimeLine.RefreshTimeLine();
        }

        public void Update(GameFightTurnListMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            foreach (var id in msg.Ids.Except(msg.DeadsIds))
            {
                var fighter = GetFighter(id);

                if (fighter == null)
                {
                    Logger.Error($"(GameFightTurnListMessage) Fighter {id} not found");
                }
                else
                {
                    fighter.IsAlive = true;
                }
            }

            foreach (var deadsId in msg.DeadsIds)
            {
                var fighter = GetFighter(deadsId);

                if (fighter == null)
                {
                    Logger.Error($"(GameFightTurnListMessage) Fighter {deadsId} not found");
                }
                else
                {
                    fighter.IsAlive = false;
                }
            }

            TimeLine.RefreshTimeLine(msg.Ids.Except(msg.DeadsIds));
        }

        public void Update(GameFightOptionStateUpdateMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Id = msg.FightId;
            GetTeam(msg.TeamId).Update(msg);
        }

        /// <summary>
        ///     Create a Fighter instance corresponding to the network given type
        /// </summary>
        /// <param name="fighter"></param>
        /// <returns></returns>
        protected Fighter CreateFighter(GameFightFighterInformations fighter)
        {
            if (fighter == null) throw new ArgumentNullException(nameof(fighter));

            return ProtocolConverter.Instance.Convert<Fighter>(fighter, this);
        }

        protected void OnStateChanged(FightPhase phase)
        {
            var handler = StateChanged;
            if (handler != null) handler(this, phase);
        }

        protected void SetPhase(FightPhase phase)
        {
            Phase = phase;
            OnStateChanged(phase);
        }

        internal void AddEffect(AbstractFightDispellableEffect abstractFightDispellableEffect, ushort actionId)
        {
            if (TimeLine.CurrentPlayer != null)
            {
                if (Effects.ContainsKey(TimeLine.CurrentPlayer))
                    Effects[TimeLine.CurrentPlayer].Add(
                        new Tuple<AbstractFightDispellableEffect, ushort>(abstractFightDispellableEffect, actionId));
                else
                    Effects[TimeLine.CurrentPlayer] = new List<Tuple<AbstractFightDispellableEffect, ushort>>
                    {
                        new Tuple<AbstractFightDispellableEffect, ushort>(abstractFightDispellableEffect, actionId)
                    };
            }
        }

        internal void Update(Bot bot, GameActionFightDeathMessage message)
        {
            // Process
            var fighter = GetActor(message.TargetId);
            if (fighter == null)
                throw new InvalidOperationException($"Fighter {message.TargetId} not found, cannot let it die");
            Logger.Debug($"{fighter} is dead");
            fighter.IsAlive = false;
        }
    }
}