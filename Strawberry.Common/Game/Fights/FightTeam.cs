﻿// <copyright file="FightTeam.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Fights
{
    public class FightTeam : INotifyPropertyChanged
    {
        public delegate void FighterAddedOrRemovedHandler(FightTeam team, Fighter fighter);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly List<Fighter> _fighters = new List<Fighter>();
        private double? _unknownLeaderId;

        public FightTeam(Fight fight, FightTeamColor id)
        {
            Fight = fight;
            Id = id;
        }

        public AlignmentSideEnum AlignmentSide { get; private set; }

        public Fight Fight { get; }

        public ReadOnlyCollection<Fighter> Fighters => _fighters.AsReadOnly();

        /// <summary>
        ///     Retreives all fighters that are alive and visible
        /// </summary>
        public Fighter[] FightersAlive
        {
            get { return _fighters.Where(x => x.IsAlive && x.Cell != null).ToArray(); }
        }

        public FightTeamColor Id { get; }

        public bool IsClosed { get; private set; }

        public bool IsHelpRequested { get; private set; }

        public bool IsRestrictedToParty { get; private set; }

        public bool IsSecret { get; private set; }

        public Fighter Leader { get; private set; }

        public Cell[] PlacementCells { get; private set; }

        public TeamTypeEnum TeamType { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public event FighterAddedOrRemovedHandler FighterAdded;
        public event FighterAddedOrRemovedHandler FighterRemoved;

        public Fighter GetFighter(double id)
        {
            return _fighters.FirstOrDefault(entry => entry.Id == id);
        }

        public T GetFighter<T>(int id) where T : Fighter
        {
            return (T) _fighters.FirstOrDefault(entry => entry is T && entry.Id == id);
        }

        public void Update(GameFightPlacementPossiblePositionsMessage msg)
        {
            if (msg == null)
                throw new ArgumentException("msg");

            PlacementCells = (Id == FightTeamColor.Red ? msg.PositionsForChallengers : msg.PositionsForDefenders).
                Select(entry => Fight.Map.Cells[entry]).ToArray();
        }

        public void Update(FightTeamInformations team)
        {
            if (team == null) throw new ArgumentNullException(nameof(team));
            if (team.TeamId != (int) Id)
            {
                Logger.Error("Try to update team {0} but the given object is for team {1}", Id,
                    (FightTeamColor) team.TeamId);
            }

            Leader = GetFighter(team.LeaderId);
            if (Leader == null)
            {
                // if not found we define it later
                _unknownLeaderId = team.LeaderId;
            }
            AlignmentSide = (AlignmentSideEnum) team.TeamSide;
            TeamType = (TeamTypeEnum) team.TeamTypeId;

            // don't care about the figthers infos
        }

        public void Update(GameFightOptionStateUpdateMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            if (msg.FightId != Fight.Id)
            {
                Logger.Warn($"(GameFightOptionStateUpdateMessage) Incorrect fightid {msg.FightId} instead of {Id}");
                return;
            }

            if (msg.TeamId != (int) Id)
            {
                Logger.Warn($"(GameFightOptionStateUpdateMessage) Incorrect teamid {msg.FightId} instead of {Id}");
                return;
            }


            switch ((FightOptionsEnum) msg.Option)
            {
                case FightOptionsEnum.FightOptionSetSecret:
                    IsSecret = msg.State;
                    break;
                case FightOptionsEnum.FightOptionAskForHelp:
                    IsHelpRequested = msg.State;
                    break;
                case FightOptionsEnum.FightOptionSetClosed:
                    IsClosed = msg.State;
                    break;
                case FightOptionsEnum.FightOptionSetToPartyOnly:
                    IsRestrictedToParty = msg.State;
                    break;
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Used by Fight class only
        /// </summary>
        /// <param name="fighter"></param>
        /// <returns></returns>
        internal void AddFighter(Fighter fighter)
        {
            if (Fighters.Any(x => x.Id == fighter.Id))
                throw new Exception($"Fighter with id {fighter.Id} already exists");

            _fighters.Add(fighter);

            if (_unknownLeaderId != null && fighter.Id == _unknownLeaderId)
            {
                Leader = fighter;
                _unknownLeaderId = null;
            }

            var evnt = FighterAdded;
            evnt?.Invoke(this, fighter);
        }

        /// <summary>
        ///     Used by Fight class only
        /// </summary>
        /// <param name="fighter"></param>
        /// <returns></returns>
        internal bool RemoveFighter(Fighter fighter)
        {
            var result = _fighters.Remove(fighter);

            if (!result) return false;

            var evnt = FighterRemoved;
            evnt?.Invoke(this, fighter);

            return true;
        }
    }
}