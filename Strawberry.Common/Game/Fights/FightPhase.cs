﻿// <copyright file="FightPhase.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Fights
{
    public enum FightPhase
    {
        None,
        Placement,
        Fighting,
        Ended
    }
}