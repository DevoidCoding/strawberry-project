﻿// <copyright file="SpellCast.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Fights
{
    public class SpellCast
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public SpellCast()
        {
        }

        public SpellCast(Fight fight, GameActionFightSpellCastMessage msg)
        {
            Caster = fight.GetFighter(msg.SourceId);

            if (Caster == null)
                logger.Error($"Fighter {msg.SourceId} not found as he casted spell {msg.SpellId}");

            Spell = ObjectDataManager.Instance.Get<Spell>(msg.SpellId);
            SpellLevel = ObjectDataManager.Instance.Get<SpellLevel>((int) Spell.SpellLevels[msg.SpellLevel - 1]);
            Target = fight.Map.Cells[msg.DestinationCellId];
            RoundCast = fight.Round;
            Critical = (FightSpellCastCriticalEnum) msg.Critical;
            SilentCast = msg.SilentCast;
            TargetedFighter = fight.GetFighter(msg.TargetId);
        }

        public Fighter Caster { get; set; }

        public FightSpellCastCriticalEnum Critical { get; set; }

        public uint RoundCast { get; set; }

        public bool SilentCast { get; set; }

        public Spell Spell { get; set; }

        public SpellLevel SpellLevel { get; set; }

        public Cell Target { get; set; }

        public Fighter TargetedFighter { get; set; }
    }
}