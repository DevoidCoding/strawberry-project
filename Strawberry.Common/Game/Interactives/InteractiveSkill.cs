﻿// <copyright file="InteractiveSkill.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.ComponentModel;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Interactives
{
    public class InteractiveSkill : INotifyPropertyChanged
    {
        private string _name;
        private string _templateName;

        public InteractiveSkill(InteractiveObject interactive, InteractiveElementSkill skill)
        {
            Id = skill.SkillInstanceUid;
            Interactive = interactive;
            JobSkill = ObjectDataManager.Instance.Get<Skill>(skill.SkillId);

            var namedSkill = skill as InteractiveElementNamedSkill;
            if (namedSkill != null)
                NameId =
                    (int?)
                        ObjectDataManager.Instance.Get<SkillName>(namedSkill.NameId).NameId;
        }

        public int Id { get; private set; }

        public InteractiveObject Interactive { get; }

        public Skill JobSkill { get; }

        public string Name
        {
            get
            {
                if (NameId != null)
                    return _name ?? (_name = I18NDataManager.Instance.ReadText((int) NameId));

                if (JobSkill != null)
                    return _templateName ?? (_templateName = I18NDataManager.Instance.ReadText(JobSkill.NameId));

                return string.Empty;
            }
        }

        public int? NameId { get; }

        public bool IsEnabled()
        {
            return Interactive.EnabledSkills.Contains(this);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}