﻿// <copyright file="InteractiveState.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Interactives
{
    public enum InteractiveState
    {
        None = 0,
        Cut = 1,
        Collecting = 2
    }
}