﻿// <copyright file="InteractiveAction.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Interactives
{
    public enum InteractiveAction
    {
        None = 0,
        Collectable = 1,
        Craftable = 2
    }
}