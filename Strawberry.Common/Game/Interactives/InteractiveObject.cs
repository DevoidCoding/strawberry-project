﻿// <copyright file="InteractiveObject.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.World;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;
using Job = Strawberry.Common.Game.Jobs.Job;

namespace Strawberry.Common.Game.Interactives
{
    public class InteractiveObject : WorldObject
    {
        public delegate void InteractiveUsedHandler(
            InteractiveObject interactive, RolePlayActor user, InteractiveSkill skill, int delay);

        private readonly ObservableCollectionMT<InteractiveSkill> _disabledSkills;
        private readonly ObservableCollectionMT<InteractiveSkill> _enabledSkills;

        private string _name;

        public InteractiveObject(Map map, InteractiveElement interactive)
        {
            if (map == null) throw new ArgumentNullException(nameof(map));
            if (interactive == null) throw new ArgumentNullException(nameof(interactive));
            Id = interactive.ElementId;
            Type = interactive.ElementTypeId > 0
                ? ObjectDataManager.Instance.Get<Interactive>(interactive.ElementTypeId)
                : null;

            Map = map;

            _enabledSkills =
                new ObservableCollectionMT<InteractiveSkill>(
                    interactive.EnabledSkills.Select(x => new InteractiveSkill(this, x)));
            EnabledSkills = new ReadOnlyObservableCollectionMT<InteractiveSkill>(_enabledSkills);

            _disabledSkills =
                new ObservableCollectionMT<InteractiveSkill>(
                    interactive.DisabledSkills.Select(x => new InteractiveSkill(this, x)));
            DisabledSkills = new ReadOnlyObservableCollectionMT<InteractiveSkill>(_disabledSkills);
        }

        public InteractiveAction Action => Type != null ? (InteractiveAction) Type.ActionId : InteractiveAction.None;

        public ReadOnlyObservableCollectionMT<InteractiveSkill> DisabledSkills { get; }

        public ReadOnlyObservableCollectionMT<InteractiveSkill> EnabledSkills { get; }

        public override double Id { get; protected set; }

        public string Name => Type != null ? _name ?? (_name = I18NDataManager.Instance.ReadText(Type.NameId)) : string.Empty;

        public InteractiveState State { get; private set; }

        /// <summary>
        ///     Can be null
        /// </summary>
        public Interactive Type { get; private set; }

        public RolePlayActor UsedBy { get; private set; }

        public IEnumerable<InteractiveSkill> AllSkills(int? jobId = null, int levelMin = 0, int levelMax = 100)
        {
            foreach (var skill in EnabledSkills)
                if (jobId == null || skill.JobSkill.ParentJobId == jobId)
                    if (skill.JobSkill.LevelMin >= levelMin && skill.JobSkill.LevelMin <= levelMax) yield return skill;
            foreach (var skill in DisabledSkills)
                if (jobId == null || skill.JobSkill.ParentJobId == jobId)
                    if (skill.JobSkill.LevelMin >= levelMin && skill.JobSkill.LevelMin <= levelMax) yield return skill;
        }

        public void DefinePosition(Cell cell)
        {
            Cell = cell;
        }

        /// <summary>
        ///     Get adjacent cells where Interactive should be usable from
        /// </summary>
        /// <returns></returns>
        public Cell[] GetAdjacentCells()
        {
            if (IsForJob(Job.Fisher))
            {
                return
                    Cell.GetCellsInDirections(
                        new DirectionsEnum[]
                        {
                            DirectionsEnum.DirectionNorthEast, DirectionsEnum.DirectionNorthWest,
                            DirectionsEnum.DirectionSouthWest, DirectionsEnum.DirectionSouthEast
                        }, 1, 3)
                        .Where(cell => Map.CanStopOnCell(cell) && Map.CanBeSeen(cell, Cell)).ToArray();
            }
            return Cell.GetAdjacentCells(x => Map.CanStopOnCell(x), true).ToArray();
        }

        /// <summary>
        ///     Says if the cell is close enough to use the Interactive
        /// </summary>
        /// <returns></returns>
        public bool IsAdjacentTo(Cell cell)
        {
            if (IsForJob(Job.Fisher))
            {
                return Map.CanStopOnCell(cell) && cell.ManhattanDistanceTo(Cell) < 4 && cell.X == Cell.X &&
                       cell.Y == Cell.Y && Map.CanStopOnCell(cell);
            }
            return cell.IsAdjacentTo(Cell, true);
        }

        // Returns if the ressource is a fish
        public bool IsFish()
        {
            return _disabledSkills.Any(skill => skill.JobSkill.ParentJobId == 36) ||
                   _enabledSkills.Any(skill => skill.JobSkill.ParentJobId == 36);
        }

        public bool IsForJob(int jobId)
        {
            return _disabledSkills.Any(skill => skill.JobSkill.ParentJobId == jobId) ||
                   _enabledSkills.Any(skill => skill.JobSkill.ParentJobId == jobId);
        }

        public void NotifyInteractiveUsed(InteractiveUsedMessage message)
        {
            var actor = Map.GetActor(message.EntityId);
            var skill =
                EnabledSkills.Concat(DisabledSkills)
                             .FirstOrDefault(x => x.JobSkill != null && x.JobSkill.Id == message.SkillId);

            var evnt = Used;
            evnt?.Invoke(this, actor, skill, message.Duration);

            if (actor == null) return;

            actor.NotifyUseInteractive(this, skill, message.Duration);

            if (message.Duration > 0)
                UsedBy = actor;
        }


        public void NotifyInteractiveUseEnded()
        {
            if (UsedBy == null) return;

            UsedBy.NotifyInteractiveUseEnded();
            UsedBy = null;
        }

        public void ResetState()
        {
            State = InteractiveState.None;
        }

        public override string ToString()
        {
            return $"{Name} {State} in {Cell} ";
        }

        public void Update(InteractiveElement interactive)
        {
            if (interactive == null) throw new ArgumentNullException(nameof(interactive));

            Type = interactive.ElementTypeId > 0
                ? ObjectDataManager.Instance.Get<Interactive>(interactive.ElementTypeId)
                : null;
            _enabledSkills.Clear();
            foreach (var skill in interactive.EnabledSkills)
            {
                _enabledSkills.Add(new InteractiveSkill(this, skill));
            }

            _disabledSkills.Clear();
            foreach (var skill in interactive.DisabledSkills)
            {
                _disabledSkills.Add(new InteractiveSkill(this, skill));
            }
        }

        public void Update(StatedElement element)
        {
            if (element == null) throw new ArgumentNullException(nameof(element));
            State = (InteractiveState) element.ElementState;

            DefinePosition(Map.Cells[element.ElementCellId]);
        }

        public event InteractiveUsedHandler Used;
    }
}