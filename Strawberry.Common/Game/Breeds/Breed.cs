﻿// <copyright file="Breed.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Stats;

namespace Strawberry.Common.Game.Breeds
{
    public class Breed : INotifyPropertyChanged
    {
        public Breed(Protocol.Data.Breed breed)
        {
            if (breed == null) throw new ArgumentNullException(nameof(breed));
            Id = breed.Id;
            ShortName = I18NDataManager.Instance.ReadText(breed.ShortNameId);
            LongName = I18NDataManager.Instance.ReadText(breed.LongNameId);
            Description = I18NDataManager.Instance.ReadText(breed.DescriptionId);
            GameplayDescription = I18NDataManager.Instance.ReadText(breed.GameplayDescriptionId);
            MaleLook = breed.MaleLook;
            FemaleLook = breed.FemaleLook;
            CreatureBonesId = breed.CreatureBonesId;
            MaleArtwork = breed.MaleArtwork;
            FemaleArtwork = breed.FemaleArtwork;
            StrengthThreshold = ListsToThresholds(breed.StatsPointsForStrength);
            AgilityThreshold = ListsToThresholds(breed.StatsPointsForAgility);
            VitalityThreshold = ListsToThresholds(breed.StatsPointsForVitality);
            WisdomThreshold = ListsToThresholds(breed.StatsPointsForWisdom);
            ChanceThreshold = ListsToThresholds(breed.StatsPointsForChance);
            IntelligenceThreshold = ListsToThresholds(breed.StatsPointsForIntelligence);
            SpellsId = breed.BreedSpellsId.ToArray();
            MaleColors = breed.MaleColors.ToArray();
            FemaleColors = breed.FemaleColors.ToArray();
        }

        public BoostThreshold[] AgilityThreshold { get; }

        public BoostThreshold[] ChanceThreshold { get; }

        public uint CreatureBonesId { get; private set; }

        public string Description { get; private set; }

        public int FemaleArtwork { get; private set; }

        public uint[] FemaleColors { get; private set; }

        public string FemaleLook { get; private set; }

        public string GameplayDescription { get; private set; }

        public int Id { get; private set; }

        public BoostThreshold[] IntelligenceThreshold { get; }

        public string LongName { get; private set; }

        public int MaleArtwork { get; private set; }

        public uint[] MaleColors { get; private set; }

        public string MaleLook { get; private set; }

        public string ShortName { get; private set; }

        public uint[] SpellsId { get; private set; }

        public BoostThreshold[] StrengthThreshold { get; }

        public BoostThreshold[] VitalityThreshold { get; }

        public BoostThreshold[] WisdomThreshold { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Gives the next threshold or null if not found
        /// </summary>
        /// <param name="actualpoints"></param>
        /// <param name="stat"> </param>
        /// <returns></returns>
        public BoostThreshold GetNextThreshold(int actualpoints, BoostableStat stat)
        {
            return GetNextThreshold(actualpoints, GetThresholds(stat));
        }

        /// <summary>
        ///     Gives the next threshold or null if not found
        /// </summary>
        /// <param name="actualpoints"></param>
        /// <param name="thresholds"></param>
        /// <returns></returns>
        public BoostThreshold GetNextThreshold(int actualpoints, BoostThreshold[] thresholds)
        {
            var index = GetThresholdIndex(actualpoints, thresholds);
            return thresholds.Length > index + 1 ? thresholds[index + 1] : null;
        }

        public BoostThreshold GetThreshold(int actualpoints, BoostableStat stat)
        {
            return GetThreshold(actualpoints, GetThresholds(stat));
        }

        public BoostThreshold GetThreshold(int actualpoints, BoostThreshold[] thresholds)
        {
            return thresholds[GetThresholdIndex(actualpoints, thresholds)];
        }

        public BoostThreshold[] GetThresholds(BoostableStat stat)
        {
            switch (stat)
            {
                case BoostableStat.Agility:
                    return AgilityThreshold;
                case BoostableStat.Strength:
                    return StrengthThreshold;
                case BoostableStat.Intelligence:
                    return IntelligenceThreshold;
                case BoostableStat.Chance:
                    return ChanceThreshold;
                case BoostableStat.Wisdom:
                    return WisdomThreshold;
                case BoostableStat.Vitality:
                    return VitalityThreshold;
                default:
                    throw new ArgumentException(nameof(stat));
            }
        }

        private static int GetThresholdIndex(int actualpoints, IReadOnlyList<BoostThreshold> thresholds)
        {
            for (var i = 0; i < thresholds.Count - 1; i++)
            {
                if (thresholds[i].PointsThreshold <= actualpoints &&
                    thresholds[i + 1].PointsThreshold > actualpoints)
                    return i;
            }

            return thresholds.Count - 1;
        }

        private static BoostThreshold[] ListsToThresholds(IReadOnlyList<List<uint>> lists)
        {
            var thresholds = new BoostThreshold[lists.Count];
            for (var i = 0; i < lists.Count; i++)
            {
                thresholds[i] = new BoostThreshold(lists[i]);
            }

            return thresholds;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}