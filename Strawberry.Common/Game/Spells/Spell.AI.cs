﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.Effects;
using Strawberry.Common.Game.Spells.Shapes;
using Strawberry.Common.Game.Stats;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Enums;
using DamageType = System.Double;

// One can either try as double or uint. Results will sightly differ. 

namespace Strawberry.Common.Game.Spells
{
    public partial class Spell
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private Dictionary<short, int> _efficiencyCache;

        private LOSMap _losMap;

        // Find the best cell to cast a given spell, retrieve damage done and best cell (todo : optimize if needed)
        public SpellTarget FindBestTarget(PlayedFighter pc, Cell source, IEnumerable<Cell> destCells,
            IEnumerable<Fighter> actors, SpellCategory category)
        {
            SpellTarget result = null;
            foreach (var dest in destCells)
            {
                var comment = string.Empty;
                var efficientcy = 0;
                if (AreaDependsOnDirection || !_efficiencyCache.TryGetValue(dest.Id, out efficientcy))
                {
                    efficientcy = GetFullAreaEffect(pc, source, dest, actors, category, ref comment);
                    if (!AreaDependsOnDirection) _efficiencyCache[dest.Id] = efficientcy;
                }

                if (efficientcy <= 0) continue;

                if (result == null)
                    result = new SpellTarget(efficientcy, source, dest, this) {Comment = comment};
                else if (efficientcy > result.Efficiency)
                {
                    result.Efficiency = efficientcy;
                    result.FromCell = source;
                    result.TargetCell = dest;
                    result.Spell = this;
                    result.Comment = comment;
                }
            }
            return result;
        }

        // Find where the PC should come to cast the spell, and the best target there (todo : optimize if needed)
        public SpellTarget FindBestTarget(PlayedFighter pc, IEnumerable<Cell> sourceCells, IEnumerable<Fighter> actors,
            SpellCategory category)
        {
            if (LevelTemplate.StatesForbidden != null)
                if (LevelTemplate.StatesForbidden.Any(pc.HasState))
                {
                    Logger.Debug(
                        $"Spell {this} skipped : statesForbidden {string.Join(",", LevelTemplate.StatesForbidden)}");
                    pc.Character.SendWarning(
                        $"Spell {this} skipped : statesForbidden {string.Join(", ", LevelTemplate.StatesForbidden)}");
                    return null; // Can't cast this : all the required states are not on the caster
                }
            if (LevelTemplate.StatesRequired != null)
                if (!LevelTemplate.StatesRequired.All(pc.HasState))
                {
                    Logger.Debug(
                        $"Spell {this} skipped : statesRequired {string.Join(",", LevelTemplate.StatesRequired)}");
                    pc.Character.SendWarning(
                        $"Spell {this} skipped : statesForbidden {string.Join(", ", LevelTemplate.StatesRequired)}");
                    return null; // Can't cast this : at least one required state is not on the caster
                }
            if (IsMaitrise(null))
                // If the spell is a maitrise, then ignore it if not of the proper type for equipped weapon. 
            {
                var weapon = pc.Character.Inventory.GetEquippedWeapon();
                if (weapon == null) return null;
                if (!IsMaitrise(weapon.TypeId))
                    return null;
            }
            _losMap = new LOSMap(pc.Fight);
            SpellTarget bestResult = null;

            #region optimisations

            IEnumerable<Fighter> enemies = pc.GetOpposedTeam().FightersAlive;
            IEnumerable<Fighter> friends = pc.Team.FightersAlive;

            var goodSpell = (Categories & (SpellCategory.Buff | SpellCategory.Healing)) != 0;
            var badSpell = (Categories & (SpellCategory.Damages | SpellCategory.Curse)) != 0;
            IEnumerable<Fighter> targets = null;
            var targetsCount = 0;
            if (goodSpell && badSpell)
            {
                goodSpell = badSpell = false;
            }
            else
            {
                targets = goodSpell ? friends : enemies;
                targetsCount = targets.Count();
            }
            var surface = GetArea(category);
            _efficiencyCache = null;
            if (!AreaDependsOnDirection) _efficiencyCache = new Dictionary<short, int>();

            #endregion

            if (surface == 1 && LevelTemplate.Range == 0) // Hack fast Cure and protect self
            {
                var res = GetSpellDamages(pc, pc);
                if (res.Damage > 0)
                    bestResult = new SpellTarget(res.Damage, pc.Cell, pc.Cell, this);
            }
            else
                foreach (var source in sourceCells)
                {
                    var destCells = GetCellsAtSpellRange(pc, source, actors);
                    if (goodSpell || badSpell)
                        if (surface <= 1 && LevelTemplate.Range > 0)
                            destCells = destCells.Intersect(targets.Select(fighter => fighter.Cell));
                    // for spells that have an area of effect of 1, just find enemies or friends as targets. No need to scan all the range.                    
                    if (surface >= 560 && destCells.Count() > 1)
                        // For spells that cover the full map, use only the first cell
                        destCells = destCells.Take(1);
                    var newResult = FindBestTarget(pc, source, destCells, actors, category);
                    if (newResult == null || newResult.Efficiency <= 0) continue;
                    if (bestResult == null || bestResult.Efficiency < newResult.Efficiency)
                    {
                        bestResult = newResult;
                        if (surface >= 560)
                            break;
                        // if spell covers all map, and we have some hit, then no need to continue (first source cells are nearest)
                        if (targetsCount == 1 && surface == 1)
                            break; // only one target and 1 cell area spell => no need to loop further
                    }
                }
            if (bestResult != null)
            {
                bestResult.Efficiency *= pc.Stats.CurrentAP/LevelTemplate.ApCost;
                bestResult.Cast = false;
            }

            return bestResult;
        }

        // Fast function that says how big is the area covered by effects from a given category
        public uint GetArea(SpellCategory? category = SpellCategory.Damages)
        {
            return
                GetEffects()
                    .Where(effect => (GetEffectCategories(effect.Id, LevelTemplate.Id) & category) > 0)
                    .Aggregate<EffectDice, uint>(0, (current, effect) => Math.Max(current, effect.Surface));
        }

        // Precise compute efficiency of a spell for a given category (beware to remove pc from friend and friendcells lists before calling this !)
        // Returns -1 if it would hit friends (todo : optimize if needed)
        public int GetFullAreaEffect(PlayedFighter pc, Cell source, Cell dest, IEnumerable<Fighter> actors,
            SpellCategory category, ref string comment)
        {
            var spellImpact = new SpellImpact();

            foreach (var effect in LevelTemplate.Effects)
                if ((GetEffectCategories(effect.EffectId, LevelTemplate.Id) & category) > 0)
                {
                    comment += " Effect " + (EffectsEnum) effect.EffectId + " : ";
                    var effectCl = new EffectDice(effect);
                    var cells = effectCl.GetArea(source, dest);
                    var targetType = (SpellTargetType) effect.TargetId;
                    var nbAffectedTargets = 0;
                    if (EffectBase.CanAffectTarget(effectCl, this, pc, pc) && cells.Contains(source))
                    {
                        // Caster would be affected
                        var efficiency = CumulEffects(effect, ref spellImpact, pc, pc /*, category*/, this);
                        if (efficiency != 0)
                            comment += $"{(decimal) efficiency} on {pc} => {(decimal) spellImpact.Damage}, ";
                        nbAffectedTargets++;
                        if (efficiency < 0) return 0; // The caster would be affected by a bad spell => give up     
                    }

                    foreach (var actor in actors.Where(act => cells.Contains(act.Cell)))
                        // All actors within the area covered by the spell
                    {
                        if (!EffectBase.CanAffectTarget(effectCl, this, pc, actor))
                            continue; // This actor is not affected by the spell
                        var damage = CumulEffects(effect, ref spellImpact, pc, actor /*, category*/, this);
                        if (damage != 0)
                            comment += $" - {(decimal) damage} on {actor} => {(decimal) spellImpact.Damage}";

                        nbAffectedTargets++;
                        //if (damage > 0 && actor.Team == pc.Team) return 0; // Harmful on a friend => give up                        
                    }

                    //if (nbAffectedTargets > 1)
                    //{
                    //    pc.Character.SendWarning("Spell {0} : {1} targets affected for {2} damage - {3}", this, nbAffectedTargets, spellImpact.Damage, comment);
                    //}
                }

            if (Template.Id == 139) // Mot d'altruisme : only use near end of fight or if lot of spellImpact to heal
            {
                var hpLeftOnFoes = actors.Where(actor => actor.Team.Id != pc.Team.Id).Sum(actor => actor.Stats.Health);
                comment +=
                    $" - special \"Mot d'altruisme\" processing : hpLeftOnFoes = {hpLeftOnFoes}, efficiency = {(int) spellImpact.Damage}";
                if (hpLeftOnFoes <= 500) return (int) spellImpact.Damage;
                if (spellImpact.Damage < 300) return 0; // Do not cast it if less than 300 hp of healing
                return (int) spellImpact.Damage/3;
                // Otherwise, far from the end of the fight, divide efficiency by 3                
                // if close to the end of the fight, then returns full result. 
            }

            return (int) spellImpact.Damage;
        }

        public SpellImpact GetSpellDamages(PlayedFighter caster, Fighter target /*, Spell.SpellCategory categories*/)
        {
            SpellImpact damages = null;
            foreach (var effect in LevelTemplate.Effects)
                CumulEffects(effect, ref damages, caster, target /*, categories*/, this);
            return damages;
        }

        public double GetTotalDamageOnAllEnemies(PlayedFighter caster)
        {
            double damages = 0;
            foreach (var enemy in caster.Team == null ? caster.Fight.AliveActors : caster.GetOpposedTeam().FightersAlive
                )
            {
                SpellImpact impact = null;
                foreach (var effect in LevelTemplate.Effects)
                    damages += CumulEffects(effect, ref impact, caster, enemy /*, Spell.SpellCategory.Damages*/, this);
            }
            return damages;
        }

        /// <summary>
        ///     Find all cells where a spell can be cast, from casterCell position
        ///     Todo : takes traps into account
        /// </summary>
        /// <param name="caster"></param>
        /// <param name="casterCell"></param>
        /// <param name="actors"></param>
        /// <returns></returns>
        private IEnumerable<Cell> GetCellsAtSpellRange(PlayedFighter caster, Cell casterCell,
            IEnumerable<Fighter> actors)
        {
            var maxRange = caster.GetRealSpellRange(LevelTemplate);
            if (LevelTemplate.CastTestLos)
                _losMap.UpdateTargetCell(casterCell, true, false);

            Func<Cell, bool> filter = cell =>
            {
                if (LevelTemplate.CastInLine || LevelTemplate.CastInDiagonal)
                    if (
                        !(LevelTemplate.CastInLine && (casterCell.X == cell.X || casterCell.Y == cell.Y) ||
                          LevelTemplate.CastInDiagonal &&
                          (Math.Abs(casterCell.X - cell.X) == Math.Abs(casterCell.Y - cell.Y)))) return false;
                if (LevelTemplate.CastTestLos)
                    if (!_losMap[cell]) return false;
                if (LevelTemplate.NeedFreeCell)
                    if (
                        !(cell.Walkable && !cell.NonWalkableDuringFight && cell != casterCell &&
                          !actors.Any(actor => actor.Cell != null && actor.Cell.Id == cell.Id))) return false;
                if (LevelTemplate.NeedTakenCell)
                    if (!(cell == casterCell || actors.Any(actor => actor.Cell != null && actor.Cell.Id == cell.Id)))
                        return false;
                if (LevelTemplate.NeedFreeTrapCell)
                    return false; // Do not play traps yet   
                var targetId = caster.Fight.GetActorsOnCell(cell).Select(actor => (int?) actor.Id).FirstOrDefault();
                return targetId == null || IsAvailable(targetId);
            };
            return casterCell.GetAllCellsInRange((int) LevelTemplate.MinRange, maxRange, false, filter);
        }

        #region Effects on the fighters

        private double GetDamageReflection(Fighter target)
        {
            double reflect = 0;
            foreach (var effect in target.GetFightTriggeredEffects((short) EffectsEnum.Effect_AddDamageReflection))
                reflect += effect.Arg3 + effect.Arg1*(1 + effect.Arg2)/2;
            return reflect*(1 + target.Level*0.05);
        }

        #endregion

        #region Categories

        [Flags]
        public enum SpellCategory
        {
            Healing = 0x0001,
            Teleport = 0x0002,
            Invocation = 0x0004,
            Buff = 0x0008,
            DamagesWater = 0x0010,
            DamagesEarth = 0x0020,
            DamagesAir = 0x0040,
            DamagesFire = 0x0080,
            DamagesNeutral = 0x0100,
            Curse = 0x0200,
            Damages = DamagesNeutral | DamagesFire | DamagesAir | DamagesEarth | DamagesWater,
            None = 0,
            All = 0x01FF
        }

        private void InitAI()
        {
            Categories = 0;
            AreaDependsOnDirection = false;
            foreach (var eff in LevelTemplate.Effects)
            {
                Categories |= GetEffectCategories(eff.EffectId, LevelTemplate.Id);
                if (eff.ZoneShape == (uint) SpellShapeEnum.L || eff.ZoneShape == (uint) SpellShapeEnum.T ||
                    eff.ZoneShape == (uint) SpellShapeEnum.D || eff.ZoneShape == (uint) SpellShapeEnum.V ||
                    eff.ZoneShape == (uint) SpellShapeEnum.Slash || eff.ZoneShape == (uint) SpellShapeEnum.U)
                    AreaDependsOnDirection = true;
            }
        }

        public SpellCategory Categories { get; private set; }

        public bool AreaDependsOnDirection { get; private set; }

        public bool HasCategory(SpellCategory? category)
        {
            if (category == null) return true;
            return (category & Categories) > 0;
        }

        public bool IsAttack => (Categories &
                                 (SpellCategory.DamagesWater | SpellCategory.DamagesEarth | SpellCategory.DamagesFire |
                                  SpellCategory.DamagesAir | SpellCategory.DamagesNeutral)) > 0;

        public bool IsSelfHealing => (Categories & SpellCategory.Healing) > 0 && LevelTemplate.Range == 0;

        public bool IsFriendHealing => (Categories & SpellCategory.Healing) > 0 && LevelTemplate.Range > 0;

        #endregion Categories

        #region Effects

        public class SpellException
        {
            public SpellException(SpellCategory category)
            {
                Category = category;
            }

            public bool Buf { get; set; }
            public SpellCategory Category { get; set; }
            public bool Curse { get; set; }
            public uint MaxAir { get; set; }
            public uint MaxEarth { get; set; }
            public uint MaxFire { get; set; }
            public uint MaxNeutral { get; set; }
            public uint MaxWater { get; set; }
            public uint MinAir { get; set; }
            public uint MinEarth { get; set; }
            public uint MinFire { get; set; }
            public uint MinNeutral { get; set; }
            public uint MinWater { get; set; }
        }

        private static readonly Dictionary<uint, SpellException> SpellExceptions = new Dictionary<uint, SpellException>
        {
            {46, new SpellException(SpellCategory.DamagesFire) {MinFire = 22, MaxFire = 24}}, // Glyphe enflammée
            {47, new SpellException(SpellCategory.DamagesFire) {MinFire = 24, MaxFire = 26}},
            {48, new SpellException(SpellCategory.DamagesFire) {MinFire = 26, MaxFire = 28}},
            {49, new SpellException(SpellCategory.DamagesFire) {MinFire = 28, MaxFire = 30}},
            {50, new SpellException(SpellCategory.DamagesFire) {MinFire = 30, MaxFire = 32}},
            {10533, new SpellException(SpellCategory.DamagesFire) {MinFire = 30, MaxFire = 32}},
            {81, new SpellException(SpellCategory.DamagesFire) {MinFire = 1, MaxFire = 3}}, //Glyphe agressif
            {82, new SpellException(SpellCategory.DamagesFire) {MinFire = 2, MaxFire = 4}},
            {83, new SpellException(SpellCategory.DamagesFire) {MinFire = 3, MaxFire = 5}},
            {84, new SpellException(SpellCategory.DamagesFire) {MinFire = 5, MaxFire = 7}},
            {85, new SpellException(SpellCategory.DamagesFire) {MinFire = 6, MaxFire = 8}},
            {3093, new SpellException(SpellCategory.DamagesFire) {MinFire = 8, MaxFire = 10}}
        };

        public static SpellCategory GetEffectCategories(uint effectId, uint spellLvId)
        {
            if (SpellExceptions.ContainsKey(spellLvId))
                return SpellExceptions[spellLvId].Category;
            switch ((EffectsEnum) effectId)
            {
                case EffectsEnum.Effect_StealHPAir:
                    return SpellCategory.DamagesAir | SpellCategory.Healing;
                case EffectsEnum.Effect_StealHPWater:
                    return SpellCategory.DamagesWater | SpellCategory.Healing;
                case EffectsEnum.Effect_StealHPFire:
                    return SpellCategory.DamagesFire | SpellCategory.Healing;
                case EffectsEnum.Effect_StealHPEarth:
                    return SpellCategory.DamagesEarth | SpellCategory.Healing;
                case EffectsEnum.Effect_StealHPNeutral:
                    return SpellCategory.DamagesNeutral | SpellCategory.Healing;
                case EffectsEnum.Effect_DamageFire:
                    return SpellCategory.DamagesFire;
                case EffectsEnum.Effect_DamageWater:
                    return SpellCategory.DamagesWater;
                case EffectsEnum.Effect_DamageAir:
                    return SpellCategory.DamagesAir;
                case EffectsEnum.Effect_DamageNeutral:
                case EffectsEnum.Effect_Punishment_Damage:
                    return SpellCategory.DamagesNeutral;
                case EffectsEnum.Effect_DamageEarth:
                    return SpellCategory.DamagesEarth;
                case EffectsEnum.Effect_HealHP_108:
                case EffectsEnum.Effect_HealHP_143:
                case EffectsEnum.Effect_HealHP_81:
                    return SpellCategory.Healing;
                case EffectsEnum.Effect_Summon:
                case EffectsEnum.Effect_Double:
                case EffectsEnum.Effect_185:
                case EffectsEnum.Effect_621:
                case EffectsEnum.Effect_623:
                    return SpellCategory.Invocation;
                case EffectsEnum.Effect_AddArmorDamageReduction:
                case EffectsEnum.Effect_AddAirResistPercent:
                case EffectsEnum.Effect_AddFireResistPercent:
                case EffectsEnum.Effect_AddEarthResistPercent:
                case EffectsEnum.Effect_AddWaterResistPercent:
                case EffectsEnum.Effect_AddNeutralResistPercent:
                case EffectsEnum.Effect_AddAirElementReduction:
                case EffectsEnum.Effect_AddFireElementReduction:
                case EffectsEnum.Effect_AddEarthElementReduction:
                case EffectsEnum.Effect_AddWaterElementReduction:
                case EffectsEnum.Effect_AddNeutralElementReduction:
                case EffectsEnum.Effect_AddAgility:
                case EffectsEnum.Effect_AddStrength:
                case EffectsEnum.Effect_AddIntelligence:
                case EffectsEnum.Effect_AddHealth:
                case EffectsEnum.Effect_AddChance:
                case EffectsEnum.Effect_AddCriticalHit:
                case EffectsEnum.Effect_AddCriticalDamageBonus:
                case EffectsEnum.Effect_AddCriticalDamageReduction:
                case EffectsEnum.Effect_AddDamageBonus:
                case EffectsEnum.Effect_AddDamageBonusPercent:
                case EffectsEnum.Effect_AddDamageBonus_121:
                case EffectsEnum.Effect_AddFireDamageBonus:
                case EffectsEnum.Effect_AddAirDamageBonus:
                case EffectsEnum.Effect_AddWaterDamageBonus:
                case EffectsEnum.Effect_AddEarthDamageBonus:
                case EffectsEnum.Effect_AddNeutralDamageBonus:
                case EffectsEnum.Effect_AddDamageMultiplicator:
                case EffectsEnum.Effect_AddDamageReflection:
                case EffectsEnum.Effect_AddGlobalDamageReduction:
                case EffectsEnum.Effect_AddGlobalDamageReduction_105:
                case EffectsEnum.Effect_AddAP_111:
                case EffectsEnum.Effect_AddHealBonus:
                case EffectsEnum.Effect_AddWisdom:
                case EffectsEnum.Effect_AddProspecting:
                case EffectsEnum.Effect_AddMP:
                case EffectsEnum.Effect_AddMP_128:
                case EffectsEnum.Effect_AddPhysicalDamage_137:
                case EffectsEnum.Effect_AddPhysicalDamage_142:
                case EffectsEnum.Effect_AddPhysicalDamageReduction:
                case EffectsEnum.Effect_AddPushDamageReduction:
                case EffectsEnum.Effect_AddPushDamageBonus:
                case EffectsEnum.Effect_AddRange:
                case EffectsEnum.Effect_AddRange_136:
                case EffectsEnum.Effect_AddSummonLimit:
                case EffectsEnum.Effect_AddVitality:
                case EffectsEnum.Effect_AddVitalityPercent:
                case EffectsEnum.Effect_Dodge:
                case EffectsEnum.Effect_IncreaseAPAvoid:
                case EffectsEnum.Effect_IncreaseMPAvoid:
                case EffectsEnum.Effect_Invisibility:
                case EffectsEnum.Effect_ReflectSpell:
                case EffectsEnum.Effect_RegainAP:
                    return SpellCategory.Buff;
                case EffectsEnum.Effect_Teleport:
                    return SpellCategory.Teleport;
                case EffectsEnum.Effect_PushBack:
                case EffectsEnum.Effect_RemoveAP:
                case EffectsEnum.Effect_LostMP:
                case EffectsEnum.Effect_StealKamas:
                case EffectsEnum.Effect_LoseHPByUsingAP:
                case EffectsEnum.Effect_LosingAP:
                case EffectsEnum.Effect_LosingMP:
                case EffectsEnum.Effect_SubRange_135:
                case EffectsEnum.Effect_SkipTurn:
                case EffectsEnum.Effect_Kill:
                case EffectsEnum.Effect_SubDamageBonus:
                case EffectsEnum.Effect_SubChance:
                case EffectsEnum.Effect_SubVitality:
                case EffectsEnum.Effect_SubAgility:
                case EffectsEnum.Effect_SubIntelligence:
                case EffectsEnum.Effect_SubWisdom:
                case EffectsEnum.Effect_SubStrength:
                case EffectsEnum.Effect_SubDodgeAPProbability:
                case EffectsEnum.Effect_SubDodgeMPProbability:
                case EffectsEnum.Effect_SubAP:
                case EffectsEnum.Effect_SubMP:
                case EffectsEnum.Effect_SubCriticalHit:
                case EffectsEnum.Effect_SubMagicDamageReduction:
                case EffectsEnum.Effect_SubPhysicalDamageReduction:
                case EffectsEnum.Effect_SubInitiative:
                case EffectsEnum.Effect_SubProspecting:
                case EffectsEnum.Effect_SubHealBonus:
                case EffectsEnum.Effect_SubDamageBonusPercent:
                case EffectsEnum.Effect_197:
                case EffectsEnum.Effect_SubEarthResistPercent:
                case EffectsEnum.Effect_SubWaterResistPercent:
                case EffectsEnum.Effect_SubAirResistPercent:
                case EffectsEnum.Effect_SubFireResistPercent:
                case EffectsEnum.Effect_SubNeutralResistPercent:
                case EffectsEnum.Effect_SubEarthElementReduction:
                case EffectsEnum.Effect_SubWaterElementReduction:
                case EffectsEnum.Effect_SubAirElementReduction:
                case EffectsEnum.Effect_SubFireElementReduction:
                case EffectsEnum.Effect_SubNeutralElementReduction:
                case EffectsEnum.Effect_SubPvpEarthResistPercent:
                case EffectsEnum.Effect_SubPvpWaterResistPercent:
                case EffectsEnum.Effect_SubPvpAirResistPercent:
                case EffectsEnum.Effect_SubPvpFireResistPercent:
                case EffectsEnum.Effect_SubPvpNeutralResistPercent:
                case EffectsEnum.Effect_StealChance:
                case EffectsEnum.Effect_StealVitality:
                case EffectsEnum.Effect_StealAgility:
                case EffectsEnum.Effect_StealIntelligence:
                case EffectsEnum.Effect_StealWisdom:
                case EffectsEnum.Effect_StealStrength:
                case EffectsEnum.Effect_275:
                case EffectsEnum.Effect_276:
                case EffectsEnum.Effect_277:
                case EffectsEnum.Effect_278:
                case EffectsEnum.Effect_279:
                case EffectsEnum.Effect_411:
                case EffectsEnum.Effect_413:
                case EffectsEnum.Effect_SubCriticalDamageBonus:
                case EffectsEnum.Effect_SubPushDamageReduction:
                case EffectsEnum.Effect_SubCriticalDamageReduction:
                case EffectsEnum.Effect_SubEarthDamageBonus:
                case EffectsEnum.Effect_SubFireDamageBonus:
                case EffectsEnum.Effect_SubWaterDamageBonus:
                case EffectsEnum.Effect_SubAirDamageBonus:
                case EffectsEnum.Effect_SubNeutralDamageBonus:
                case EffectsEnum.Effect_StealAP_440:
                case EffectsEnum.Effect_StealMP_441:
                    return SpellCategory.Curse;
            }
            return SpellCategory.None;
        }

        public IEnumerable<EffectDice> GetEffects(SpellCategory? category = null) =>
            LevelTemplate.Effects.Where(
                effect => category == null || (GetEffectCategories(effect.EffectId, LevelTemplate.Id) & category) > 0)
                .Select(effect => new EffectDice(effect));

        public class SpellImpact
        {
            public double MinFire,
                MaxFire,
                MinWater,
                MaxWater,
                MinEarth,
                MaxEarth,
                MinAir,
                MaxAir,
                MinNeutral,
                MaxNeutral,
                MinHeal,
                MaxHeal;

            public double Air => (MinAir + MaxAir)/2;

            public double Boost { get; set; }
            public double Curse { get; set; }

            /// <summary>
            ///     Return positive values for bad effects (curses and spellImpact) and négative values for good effects (heals and
            ///     boosts)
            /// </summary>
            public double Damage => (MinDamage + MaxDamage)/2;

            public double Earth => (MinEarth + MaxEarth)/2;

            public double Fire => (MinFire + MaxFire)/2;

            public double Heal => (MinHeal + MaxHeal)/2;

            // Max total damage            
            public double MaxDamage => MaxFire + MaxAir + MaxEarth + MaxWater + MaxNeutral - MinHeal + Curse - Boost;

            //public string Comment { get; set; }
            // Min total damage            
            public double MinDamage => MinFire + MinAir + MinEarth + MinWater + MinNeutral - MaxHeal + Curse - Boost;

            public double Neutral => (MinEarth + MaxEarth)/2;

            public double Water
            {
                get { return (MinWater + MaxWater)/2; }
            }

            public void Add(SpellImpact dmg)
            {
                MinFire += dmg.MinFire;
                MaxFire += dmg.MaxFire;
                MinWater += dmg.MinWater;
                MaxWater += dmg.MaxWater;
                MinEarth += dmg.MinEarth;
                MaxEarth += dmg.MaxEarth;
                MinAir += dmg.MinAir;
                MaxAir += dmg.MaxAir;
                MinNeutral += dmg.MinNeutral;
                MaxNeutral += dmg.MaxNeutral;
                MinHeal += dmg.MinHeal;
                MaxHeal += dmg.MaxHeal;
                Curse += dmg.Curse;
                Boost += dmg.Boost;
            }

            public void Multiply(double ratio)
            {
                MinFire *= ratio;
                MaxFire *= ratio;
                MinWater *= ratio;
                MaxWater *= ratio;
                MinEarth *= ratio;
                MaxEarth *= ratio;
                MinAir *= ratio;
                MaxAir *= ratio;
                MinNeutral *= ratio;
                MaxNeutral *= ratio;
                MinHeal *= ratio;
                MaxHeal *= ratio;
                Curse *= ratio;
                Boost *= ratio;
            }
        }

        private static void AdjustDamage(SpellImpact damages, uint damage1, uint damage2, SpellCategory category,
            double chanceToHappen, int addDamage, int addDamagePercent, int reduceDamage, int reduceDamagePercent,
            bool negativ)
        {
            double minDamage = damage1;
            double maxDamage = damage1 >= damage2 ? damage1 : damage2;
            if (reduceDamagePercent >= 100)
                return; // No damage
            minDamage = (minDamage*(1 + addDamagePercent/100.0) + addDamage - reduceDamage)*
                        (1 - reduceDamagePercent/100.0)*chanceToHappen;
            maxDamage = (maxDamage*(1 + addDamagePercent/100.0) + addDamage - reduceDamage)*
                        (1 - reduceDamagePercent/100.0)*chanceToHappen;

            if (minDamage < 0) minDamage = 0;
            if (maxDamage < 0) maxDamage = 0;


            if (negativ) // or IsFriend
            {
                minDamage *= -1.5; // High penalty for firing on friends
                maxDamage *= -1.5; // High penalty for firing on friends
            }
            switch (category)
            {
                case SpellCategory.DamagesNeutral:
                    damages.MinNeutral += minDamage;
                    damages.MaxNeutral += maxDamage;
                    break;
                case SpellCategory.DamagesFire:
                    damages.MinFire += minDamage;
                    damages.MaxAir += maxDamage;
                    break;
                case SpellCategory.DamagesAir:
                    damages.MinAir += minDamage;
                    damages.MaxAir += maxDamage;
                    break;
                case SpellCategory.DamagesWater:
                    damages.MinWater += minDamage;
                    damages.MaxWater += maxDamage;
                    break;
                case SpellCategory.DamagesEarth:
                    damages.MinEarth += minDamage;
                    damages.MaxEarth += maxDamage;
                    break;
                case SpellCategory.Healing:
                    damages.MinHeal += minDamage;
                    damages.MaxHeal += maxDamage;
                    break;
            }
        }

        private static int GetSafetotal(PlayedFighter caster, PlayerField field)
        {
            var row = caster?.PCStats[field];
            return row?.Total ?? 0;
        }

        public bool IsMaitrise(uint? weaponType)
        {
            if (Template.TypeId == 23) // Maîtrise
                return weaponType == null || LevelTemplate.Effects[0].DiceNum == weaponType;
            return false;
        }

        /// <summary>
        ///     Add spellImpact for a given effect, taking into account caster bonus and target resistance.
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="spellImpact"></param>
        /// <param name="caster"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static double CumulEffects(EffectInstanceDice effect, ref SpellImpact spellImpact, PlayedFighter caster,
            Fighter target /*, Spell.SpellCategory Categories*/, Spell spell)
        {
            if (target == null)
                throw new ArgumentNullException(nameof(target));

            var isFriend = caster.Team.Id == target.Team.Id;
            var result = new SpellImpact();

            var targetType = (SpellTargetType) effect.TargetId;


            //if ((targetType & SpellTargetType.ENEMIES) == 0) return spellImpact; // No enemy can be targeted

            var category = GetEffectCategories(effect.EffectId, spell.LevelTemplate.Id) /* & Categories*/;
            if (category == 0) return 0; // No category selected in this spell

            if (spell.Template.Id == 0) // Weapon => ignore non heal or damage effects
                if ((category & (SpellCategory.Damages | SpellCategory.Healing)) == 0) return 0;

            var chanceToHappen = 1.0; // 

            // When chances to happen is under 100%, then we reduce spellImpact accordingly, for simplicity, but after having apply damage bonus & reduction. 
            // So average damage should remain exact even if Min and Max are not. 
            if (effect.Random > 0)
                chanceToHappen = effect.Random/100.0;

            if (target.Summoned && (caster.Breed.Id != (int) BreedEnum.Osamodas || target.Team.Id != caster.Team.Id))
                chanceToHappen /= 2;
            // It's much better to hit non-summoned foes => effect on summons (except allies summon for Osa) is divided by 2. 

            SpellException spellException = null;
            if (SpellExceptions.ContainsKey(spell.LevelTemplate.Id))
                spellException = SpellExceptions[spell.LevelTemplate.Id];
            if ((category & SpellCategory.DamagesNeutral) > 0)
                AdjustDamage(result, spellException?.MinNeutral ?? effect.DiceNum,
                    spellException?.MaxNeutral ?? effect.DiceSide, SpellCategory.DamagesNeutral,
                    chanceToHappen,
                    GetSafetotal(caster, PlayerField.NeutralDamageBonus) + GetSafetotal(caster, PlayerField.DamageBonus) +
                    GetSafetotal(caster, PlayerField.PhysicalDamage),
                    GetSafetotal(caster, PlayerField.DamageBonusPercent) + GetSafetotal(caster, PlayerField.Strength),
                    target.Stats.NeutralElementReduction,
                    target.Stats.NeutralResistPercent, isFriend);

            if ((category & SpellCategory.DamagesFire) > 0)
                AdjustDamage(result, spellException?.MinFire ?? effect.DiceNum,
                    spellException?.MaxFire ?? effect.DiceSide, SpellCategory.DamagesFire,
                    chanceToHappen,
                    GetSafetotal(caster, PlayerField.FireDamageBonus) + GetSafetotal(caster, PlayerField.DamageBonus) +
                    GetSafetotal(caster, PlayerField.MagicDamage),
                    GetSafetotal(caster, PlayerField.DamageBonusPercent) +
                    GetSafetotal(caster, PlayerField.Intelligence),
                    target.Stats.FireElementReduction,
                    target.Stats.FireResistPercent, isFriend);

            if ((category & SpellCategory.DamagesAir) > 0)
                AdjustDamage(result, spellException?.MinAir ?? effect.DiceNum,
                    spellException?.MaxAir ?? effect.DiceSide, SpellCategory.DamagesAir,
                    chanceToHappen,
                    GetSafetotal(caster, PlayerField.AirDamageBonus) + GetSafetotal(caster, PlayerField.DamageBonus) +
                    GetSafetotal(caster, PlayerField.MagicDamage),
                    GetSafetotal(caster, PlayerField.DamageBonusPercent) + GetSafetotal(caster, PlayerField.Agility),
                    target.Stats.AirElementReduction,
                    target.Stats.AirResistPercent, isFriend);

            if ((category & SpellCategory.DamagesWater) > 0)
                AdjustDamage(result, spellException?.MinWater ?? effect.DiceNum,
                    spellException?.MaxWater ?? effect.DiceSide, SpellCategory.DamagesWater,
                    chanceToHappen,
                    GetSafetotal(caster, PlayerField.WaterDamageBonus) + GetSafetotal(caster, PlayerField.DamageBonus) +
                    GetSafetotal(caster, PlayerField.MagicDamage),
                    GetSafetotal(caster, PlayerField.DamageBonusPercent) + GetSafetotal(caster, PlayerField.Chance),
                    target.Stats.WaterElementReduction,
                    target.Stats.WaterResistPercent, isFriend);

            if ((category & SpellCategory.DamagesEarth) > 0)
                AdjustDamage(result, spellException?.MinEarth ?? effect.DiceNum,
                    spellException?.MaxEarth ?? effect.DiceSide, SpellCategory.DamagesEarth,
                    chanceToHappen,
                    GetSafetotal(caster, PlayerField.EarthDamageBonus) + GetSafetotal(caster, PlayerField.DamageBonus) +
                    GetSafetotal(caster, PlayerField.MagicDamage),
                    GetSafetotal(caster, PlayerField.DamageBonusPercent) + GetSafetotal(caster, PlayerField.Strength),
                    target.Stats.EarthElementReduction,
                    target.Stats.EarthResistPercent, isFriend);

            if ((category & SpellCategory.Healing) > 0)
            {
                var steal = (category & SpellCategory.Damages) > 0;
                if (steal) target = caster; // Probably hp steal
                var hptoHeal = (uint) Math.Max(0, target.Stats.MaxHealth - target.Stats.Health); // Can't heal over max
                if (steal)
                {
                    result.MinHeal = -Math.Min(hptoHeal, Math.Abs(result.MinDamage));
                    result.MaxHeal = -Math.Min(hptoHeal, Math.Abs(result.MaxDamage));
                }
                else
                {
                    var skip = false;
                    if (spell.Template.Id == 140) // Mot de reconstruction => do only use it on purpose
                    {
                        if (hptoHeal < target.Stats.Health || hptoHeal < 400)
                            skip = true; // Only heal targets with under 50% of health and at least 400 hp to heal
                    }
                    if (!skip && hptoHeal > 0)
                    {
                        AdjustDamage(result, Math.Min(effect.DiceNum, hptoHeal), Math.Min(effect.DiceSide, hptoHeal),
                            SpellCategory.Healing, chanceToHappen,
                            GetSafetotal(caster, PlayerField.HealBonus),
                            GetSafetotal(caster, PlayerField.Intelligence),
                            0,
                            0, isFriend);
                        if (result.Heal > hptoHeal)
                            if (isFriend)
                                result.MinHeal = result.MaxHeal = -hptoHeal;
                            else
                                result.MinHeal = result.MaxHeal = hptoHeal;
                    }
                }
            }
            if ((category & SpellCategory.Buff) > 0)
                if (isFriend)
                    result.Boost -= spell.Level*chanceToHappen;
                else
                    result.Boost += spell.Level*chanceToHappen;

            if ((category & SpellCategory.Curse) > 0)
            {
                var ratio = spell.Level*chanceToHappen;

                if (effect.EffectId == (int) EffectsEnum.Effect_SkipTurn)
                    // Let say this effect counts as 2 damage per level of the target
                    ratio = target.Level*2*chanceToHappen;

                if (isFriend)
                    result.Curse -= 2*ratio;
                else
                    result.Curse += ratio;
            }
            if (isFriend)
                result.Add(result); // amplify (double) effects on friends. 


            if (!isFriend && ((category & SpellCategory.Damages) > 0) && result.MinDamage > target.Stats.Health)
                // Enough damage to kill the target => affect an arbitrary 50% of max heal (with at least current health), so strong spells are not favored anymore. 
            {
                var ratio = Math.Max(target.Stats.MaxHealth/2, target.Stats.Health)/result.MinDamage;
                result.Multiply(ratio);
            }

            if (spell.Template.Id == 114) // Rekop
            {
                if (target.Stats.Health < 1000)
                    result.Multiply(0.1);
                else if (target.Stats.Health < 2000)
                    result.Multiply(0.5);
            }

            // Damage reflection
            if (((category & SpellCategory.Damages) > 0) && result.Damage > 0 && !isFriend)
            {
                var reflected = spell.GetDamageReflection(target);
                if (reflected > 0)
                {
                    if (reflected >= spellImpact.Damage) return 0; // Reflect all damages
                    result.MinHeal += reflected*2;
                    result.MaxHeal += reflected*2;
                }
            }

            if (spell.Template.Id == 0 && (category & SpellCategory.Damages) > 0)
                // Weapon => consider effect of "maîtrise"
            {
                var weapon = caster.Character.Inventory.GetEquippedWeapon();
                if (weapon != null)
                    foreach (var boost in caster.GetBoostWeaponDamagesEffects())
                        if (boost.WeaponTypeId == weapon.TypeId)
                            result.Multiply(1.0 + boost.Delta/100.0);
            }

            if (spellImpact != null)
                spellImpact.Add(result);
            else
                spellImpact = result;
            return result.Damage;
        }

        #endregion Effects
    }
}