﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using MoreLinq;
using NLog;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.Effects;
using Strawberry.Core.Collections;
using Strawberry.Core.Reflection;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Spells
{
    public partial class SpellsBook : INotifyPropertyChanged
    {
        private readonly ObservableCollectionMT<Spell> _spells;

        public SpellsBook(PlayedCharacter owner)
        {
            if (owner == null) throw new ArgumentNullException(nameof(owner));
            Character = owner;
            _spells = new ObservableCollectionMT<Spell>();
            Spells = new ReadOnlyObservableCollectionMT<Spell>(_spells);
        }

        public SpellsBook(PlayedCharacter owner, SpellListMessage list)
            : this(owner)
        {
            if (list == null) throw new ArgumentNullException(nameof(list));
            Update(list);
        }

        public PlayedCharacter Character { get; set; }

        public bool SpellPrevisualization { get; set; }

        public ReadOnlyObservableCollectionMT<Spell> Spells { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanUpgradeSpell(Spell spell)
        {
            if (Character.IsFighting()) return false;
            if (spell.Level > Character.Stats.SpellsPoints) return false;
            if (!HasSpell(spell.Template.Id)) return false;
            if (spell.Template.SpellLevels.Count <= spell.Level) return false;
            if (spell.Level == 5 && spell.LevelTemplate.MinPlayerLevel + 100 > Character.Level) return false;
            return true;
        }

        public Spell GetSpell(int id)
        {
            return Spells.FirstOrDefault(x => x.Template.Id == id);
        }

        public bool HasSpell(int id)
        {
            return GetSpell(id) != null;
        }

        // Initializes full SpellsBook
        public void Update(SpellListMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            _spells.Clear();
            foreach (var spell in msg.Spells)
                _spells.Add(new Spell(spell));

            SpellPrevisualization = msg.SpellPrevisualization;

            //FullDump();
        }

        // Learns a new spell
        public void Update(SpellVariantActivationMessage message)
        {
            var newSpell = new Spell(message);
            var known = _spells.FirstOrDefault(spell => spell.Template.Id == newSpell.Template.Id);
            if (known != null)
                _spells[_spells.IndexOf(known)] = newSpell;
            else
                _spells.Add(newSpell);
        }

        public bool UpgradeSpell(Spell spell)
        {
            throw new NotImplementedException();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region debug tool

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void FullDump()
        {
            var dumper = new ObjectDumper(4, true, true, BindingFlags.FlattenHierarchy);
            Logger.Error($"Dump of the spellbook of {Character.Name} : ");
            foreach (var spell in _spells)
            {
                Logger.Error($"   Spell {spell.ToString(true)}");
                foreach (var effectdice in spell.LevelTemplate.Effects)
                {
                    var effect = new EffectBase(effectdice);
                    Logger.Error(
                        $"       Effect {effect.Description} : {(effectdice.DiceNum <= effectdice.DiceSide ? effectdice.DiceNum : effectdice.DiceSide)} - {(effectdice.DiceNum > effectdice.DiceSide ? effectdice.DiceNum : effectdice.DiceSide)} {(effectdice.Random == 0 ? 1.0 : effectdice.Random/100.0):P}");
                }
            }
        }

        public string GetFullDetail()
        {
            return string.Join("\r\n", _spells.Select(spell => spell.GetFullDescription()));
        }

        #endregion debug tool
    }
}