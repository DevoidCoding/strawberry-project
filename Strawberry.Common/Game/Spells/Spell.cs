﻿using System;
using System.ComponentModel;
using System.Linq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Effects;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Spells
{
    public partial class Spell : INotifyPropertyChanged
    {
        public const string Unknown = "<Unknown>";
        protected int _level;

        /// <summary>
        ///     This constructor should only be used by derived classes that override getLevelTemplate.
        /// </summary>
        /// <param name="spellTemplate"></param>
        /// <param name="level"></param>
        protected Spell()
        {
        }

        public Spell(SpellItem spell)
        {
            if (spell == null) throw new ArgumentNullException(nameof(spell));
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Spell>(spell.SpellId);
            Level = spell.SpellLevel;
        }

        // TODO : GetVariantLevel
        public Spell(SpellVariantActivationMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Spell>(message.SpellId);
            Level = 1;
        }

        public string Description
            => Template != null ? I18NDataManager.Instance.ReadText(Template.DescriptionId) : Unknown;

        public int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                // HACK : If the WeaponSpeel call the setter, don't ask for the LevelTemplate, it will do a StackOverflow
                if (!(this is WeaponSpell))
                    LevelTemplate = GetLevelTemplate(_level);
                FillEffectTemplates();
                InitAI();
            }
        }

        public SpellLevel LevelTemplate { get; protected set; }


        public string Name => Template != null ? I18NDataManager.Instance.ReadText(Template.NameId) : Unknown;

        public Protocol.Data.Spell Template { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public string GetFullDescription()
        {
            var result = $"{Name}#{Template.Id} ({Level})#{LevelTemplate.Id} - cat {Categories} - {Description}\r\n";
            return GetEffects().Aggregate(result, (current, effect) => current + ("    > " + effect) + "\n\r");
        }

        public string ToString(bool detailed)
        {
            if (Template != null)
                return detailed ? $"{Name} {Level} : {Description}" : ToString();
            return Unknown;
        }

        public override string ToString()
        {
            return Template != null ? $"{Name} {Level}" : Unknown;
        }

        protected void FillEffect(EffectInstanceDice effect)
        {
            if (string.IsNullOrEmpty(effect.RawZone)) return;
            effect.ZoneShape = effect.RawZone[0];
            if (effect.RawZone.Length == 1) return;
            var options = effect.RawZone.Remove(0, 1);
            var splitted = options.Split(',');
            if (splitted.Length >= 1)
                effect.ZoneSize = splitted[0];
            if (splitted.Length >= 2)
                effect.ZoneMinSize = splitted[1];
        }

        protected void FillEffectTemplates()
        {
            LevelTemplate.Effects.ForEach(FillEffect);
            LevelTemplate.CriticalEffect.ForEach(FillEffect);
        }

        protected virtual SpellLevel GetLevelTemplate(int level)
        {
            if (Template.SpellLevels.Count < Level)
                throw new InvalidOperationException($"Level {Level} doesn't exist in spell {Template.Id}");

            var lv = ObjectDataManager.Instance.Get<SpellLevel>((int) Template.SpellLevels[level - 1]);
            if (Template.Id == 158) // For Iops, Concentration effects are wrong in D2o
            {
                lv.Effects[0].TargetId =
                    (int)
                        (SpellTargetType.AlliesNonSummon | SpellTargetType.EnemiesNonSummon | SpellTargetType.Self);
                lv.Effects[1].TargetId = (int) (SpellTargetType.AlliesSummon | SpellTargetType.EnemiesSummon);
            }
            if (Template.Id == 126) // For Eni, Mot stimulant also affects the enemies (2nd effect)
            {
                lv.Effects[1].TargetId = (int) SpellTargetType.All;
            }

            return lv;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}