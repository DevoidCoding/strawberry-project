﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MoreLinq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Spells
{
    public partial class SpellsBook
    {
        public Spell WeaponSpellLike()
        {
            var weapon = Character.Inventory.GetEquippedWeapon();
            if (weapon == null)
            {
                Character.SendWarning("No weapon avalaible");
                return null;
            }
            if (!Character.CheckCriteria(weapon.Criteria))
            {
                Character.SendWarning($"{Character} do not meet criteria {weapon.Criteria}");
                return null;
            }

            if (weapon.Type == null)
            {
                weapon.Type = ObjectDataManager.Instance.Get<ItemType>(weapon.TypeId);
                if (weapon.Type == null)
                {
                    Character.SendError($"The weapon type is unknown {weapon.TypeId}");
                    return null;
                }
            }
            var rawZone = weapon.Type.RawZone;
            Spell weaponSpellLike = new WeaponSpell(weapon);
            return weaponSpellLike;
        }

        #region Availability management

        /// <summary>
        ///     Notifies the start of a new fight turn to each spell
        /// </summary>
        public void FightStart(GameFightStartingMessage msg)
        {
            _spells.ForEach(spell => spell.StartFight());
        }

        /// <summary>
        ///     Notifies the end of the fighting turn to each spell
        /// </summary>
        public void EndTurn()
        {
            _spells.ForEach(spell => spell.EndTurn());
        }

        /// <summary>
        ///     Notifies the spell that it has been cast on a given target
        /// </summary>
        public void CastAt(GameActionFightSpellCastMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            //TODO : Handle Huppermage rune and effects
            var spell = GetSpell(msg.SpellId);
            if (spell == null)
            {
                return;
                throw new ArgumentException(
                    $"Spell Id {msg.SpellId} do not exists in the SpellsBook of {Character.Name}, with {_spells.Count} entries");
            }
            spell.CastAt(msg.TargetId);
            //Character.SendMessage(string.Format("Spell {0} cast at actor Id {1}. Still available : {2}", spell, msg.targetId, spell.IsAvailable(msg.targetId)));
        }

        public IEnumerable<Spell> GetAvailableSpells(int? targetId = null, Spell.SpellCategory? category = null)
        {
            return _spells.Where(spell => spell.IsAvailable(targetId, category));
        }

        /*public bool IsProperTarget(PlayedFighter caster, Fighter target, Spell spell)
        {
            // SpellTargetType
            if (target == null) return spell.LevelTemplate.needFreeCell && spell.IsAvailable(null, null);

            foreach (var spellEffect in spell.GetEffects())
                if (EffectBase.canAffectTarget(spellEffect, spell, caster, target)) return true;
            return false;
        }*/

        #endregion Availability management

        #region Spell selection

        /*public IEnumerable<Spell> GetOrderListOfSimpleBoostSpells(PlayedFighter caster, Fighter target, Spell.SpellCategory category)
        {
            return _spells.Where(spell => (caster.Stats.CurrentAP >= spell.LevelTemplate.apCost) && spell.IsAvailable(caster.Id, category) && IsProperTarget(caster, target, spell)).OrderByDescending(spell => spell.Level).ThenByDescending(spell => spell.LevelTemplate.minPlayerLevel);
        }

        public IEnumerable<Spell> GetOrderedAttackSpells(PlayedFighter caster, Fighter target, Spell.SpellCategory category = Spell.SpellCategory.Damages)
        {
            Debug.Assert(((category & Spell.SpellCategory.Damages) > 0), "category do not include Damage effects");
            return _spells.Where(spell =>
                (caster.Stats.CurrentAP >= spell.LevelTemplate.apCost) &&
                spell.IsAvailable(target.Id, category) &&
                IsProperTarget(caster, target, spell))
                .OrderByDescending(spell => (int)(spell.GetSpellDamages(caster, target, Spell.SpellCategory.Damages).Damage) * (caster.Stats.CurrentAP / (int)spell.LevelTemplate.apCost));
        }*/


        public SpellTarget FindBestUsage(PlayedFighter pc, List<int> spellIds,
            IEnumerable<Cell> possiblePlacement = null)
        {
            var timer = new Stopwatch();
            timer.Start();
            var pathFinder = new PathFinder(pc.Fight, true);
            var sourceCells = possiblePlacement ?? pc.GetPossibleMoves(true, true, pathFinder);
            var actorsWithoutPC = pc.Fight.AliveActors.Where(actor => actor != pc);
            var spellTarget = new SpellTarget();
            foreach (var spellId in spellIds)
            {
                var spell = _spells.FirstOrDefault(sp => sp.Template.Id == spellId);
                if (spell == null && spellId == 0)
                    spell = WeaponSpellLike();
                if (spell == null) continue;

                if (!spell.IsAvailable(null) || ((spellId == 0 || !pc.CanCastSpells) && (spellId != 0 || !pc.CanFight)) ||
                    spell.LevelTemplate.ApCost > pc.Stats.CurrentAP || !pc.CanCastSpell(spellId)) continue;

                if (!spell.IsAvailable(null) || spell.LevelTemplate.ApCost > pc.Stats.CurrentAP)
                    continue;

                var lastSpellTarget = spell.FindBestTarget(pc, sourceCells, actorsWithoutPC, Spell.SpellCategory.All);
                if (lastSpellTarget == null || !(lastSpellTarget.Efficiency > spellTarget.Efficiency)) continue;

                spellTarget = lastSpellTarget;
                break; // Stop on the first spell with positive efficiency
            }
            timer.Stop();
            spellTarget.TimeSpan = timer.Elapsed;
            return spellTarget;
        }

        private readonly List<int> _ignoredSpells = new List<int> {5 /*Trêve*/, 59 /*Corruption*/};

        public SpellTarget FindBestUsage(PlayedFighter pc, Spell.SpellCategory category, bool withWeapon,
            IEnumerable<Cell> possiblePlacement = null)
        {
            var spellTarget = new SpellTarget();
            if (pc.PCStats.CurrentAP <= 0) return spellTarget;
            var timer = new Stopwatch();
            timer.Start();
            var pathFinder = new PathFinder(pc.Fight, true);
            var sourceCells = possiblePlacement ?? pc.GetPossibleMoves(true, true, pathFinder);
            var actorsWithoutPC = pc.Fight.AliveActors.Except(new[] {pc});
            var spells = _spells.ToList();
            if (withWeapon && ((category & (Spell.SpellCategory.Damages | Spell.SpellCategory.Healing)) != 0))
                spells.Add(WeaponSpellLike());
            //logger.Debug("***FindBestUsage {0}, {1} spells in book. {2} PA. {3}/{4} HP ***", category, spells.Count, pc.PCStats.CurrentAP, pc.PCStats.Health, pc.PCStats.MaxHealth);
            var thisLock = new object();
            //foreach(Spell spell in spells)
            Parallel.ForEach(spells, spell =>
            {
                if (spell != null && !_ignoredSpells.Contains(spell.Template.Id))
                {
                    var spellId = spell.Template.Id;
                    if (spell.IsAvailable(null) && ((spellId != 0 && pc.CanCastSpells) || (spellId == 0 && pc.CanFight)) &&
                        spell.LevelTemplate.ApCost <= pc.Stats.CurrentAP && pc.CanCastSpell(spellId))
                    {
                        if ((spell.Categories & category) > 0)
                        {
                            var lastSpellTarget = spell.FindBestTarget(pc, sourceCells, actorsWithoutPC, category);
                            if (lastSpellTarget != null && lastSpellTarget.Efficiency > spellTarget.Efficiency)
                                //lock (thisLock)
                                spellTarget = lastSpellTarget;
                            //if (lastSpellTarget != null)
                            //    logger.Debug("efficiency {0} = {1} ({2})", lastSpellTarget.Spell, lastSpellTarget.Efficiency, lastSpellTarget.Comment);
                            //lock (thisLock)
//                                    logger.Debug("efficiency {0} = ???", spell); 
//                            else
                            //lock (thisLock)
                            //                                  logger.Debug("efficiency {0} = {1} ({2})", lastSpellTarget.Spell, lastSpellTarget.Efficiency, lastSpellTarget.Comment);
                        }
                    }
                }
            }
                );
            //Debug.Assert(NbSpellsChecked == spells.Count);
            timer.Stop();
            spellTarget.TimeSpan = timer.Elapsed;
            //pc.Character.SendInformation("Spell {0} selected - efficientcy : {1} - comment = {2}", spellTarget.Spell, spellTarget.Efficiency, spellTarget.Comment);
            return spellTarget;
        }

        #endregion Spell selection
    }
}