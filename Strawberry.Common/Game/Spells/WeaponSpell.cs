﻿using System.Linq;
using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.Spells
{
    public class WeaponSpell : Spell
    {
        public WeaponSpell(Weapon weapon)
        {
            Weapon = weapon;
            Template = new Protocol.Data.Spell
            {
                Id = 0,
                DescriptionId = weapon.DescriptionId,
                NameId = weapon.NameId
            };
            LevelTemplate = GetLevelTemplate(1);
        }

        protected Weapon Weapon { get; set; }

        protected sealed override SpellLevel GetLevelTemplate(int level)
        {
            LevelTemplate = new SpellLevel
            {
                Effects = Weapon.PossibleEffects.OfType<EffectInstanceDice>()
                    .Where(
                        effect =>
                            (effect.Duration != 0) ||
                            ((GetEffectCategories(effect.EffectId, 0) &
                              (SpellCategory.Damages | SpellCategory.Healing)) > 0))
                    .ToList()
            };
            // We only take effects with a duration or damage or healing effect into considération. Others are probably constant effects on the caster when holding the weapon. 
            foreach (var effect in LevelTemplate.Effects)
                effect.RawZone = Weapon.Type.RawZone;
            LevelTemplate.CriticalEffect = LevelTemplate.Effects;
            Level = 1;
            LevelTemplate.MinRange = (uint) Weapon.MinRange;
            LevelTemplate.Range = (uint) Weapon.Range;
            LevelTemplate.ApCost = (uint) Weapon.ApCost;
            LevelTemplate.CastInDiagonal = Weapon.CastInDiagonal;
            LevelTemplate.CastInLine = Weapon.CastInLine;
            LevelTemplate.CastTestLos = Weapon.CastTestLos;
            LevelTemplate.CriticalHitProbability = (uint) Weapon.CriticalHitProbability;
            return LevelTemplate;
        }
    }
}