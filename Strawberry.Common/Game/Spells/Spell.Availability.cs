﻿using System.Collections.Generic;

namespace Strawberry.Common.Game.Spells
{
    public partial class Spell
    {
        #region Stuff to control availability of the spell

        private uint _nbCastAllowed;
        private uint _nbTurnToWait;
        private Dictionary<double, int> _targeted;

        public void StartFight()
        {
            _nbTurnToWait = LevelTemplate.InitialCooldown;
            if (_nbTurnToWait == 0)
                _nbCastAllowed = LevelTemplate.MaxCastPerTurn;
            else
                _nbCastAllowed = 0;
            _targeted = null;
        }

        public void EndTurn()
        {
            if (_nbTurnToWait > 0)
                _nbTurnToWait--;
            if (_nbTurnToWait == 0)
                _nbCastAllowed = LevelTemplate.MaxCastPerTurn;
            _targeted = null; // Reset targeted counts
        }

        public void CastAt(double targetId)
        {
            // Count for limited usage per target
            if (LevelTemplate.MaxCastPerTarget > 0)
            {
                var targetCount = 1;
                if (_targeted == null)
                    _targeted = new Dictionary<double, int>();
                if (!_targeted.TryGetValue(targetId, out targetCount))
                    targetCount = 1;
                else
                    targetCount++;
                _targeted[targetId] = targetCount;
                //Debug.Assert(targetCount <= LevelTemplate.maxCastPerTarget);
            }
            //Debug.Assert(_nbCastAllowed > 0 || LevelTemplate.maxCastPerTurn <= 0);
            if (LevelTemplate.MinCastInterval > 0)
                _nbTurnToWait = LevelTemplate.MinCastInterval;

            if (_nbCastAllowed > 0)
                _nbCastAllowed--;
        }

        public string AvailabilityExplainString(double? idTarget)
        {
            var res = string.Empty;
            if (LevelTemplate.MinCastInterval != 0 || _nbTurnToWait != 0)
                res += $"turns to wait : {_nbTurnToWait}/{LevelTemplate.MinCastInterval}, ";
            if (_nbCastAllowed != 0 || LevelTemplate.MaxCastPerTurn != 0)
                res += $"cast allowed : {_nbCastAllowed}/{LevelTemplate.MaxCastPerTurn}, ";
            if (idTarget != null && LevelTemplate.MaxCastPerTarget != 0)
            {
                var targetCount = 0;
                if (_targeted != null && !_targeted.TryGetValue(idTarget.Value, out targetCount))
                    targetCount = 0;
                res += $"cast allowed on target {idTarget}: {targetCount}/{LevelTemplate.MaxCastPerTarget}, ";
            }
            return res;
            //private uint _nbCastAllowed;
            //private uint _nbTurnToWait;
        }

        public bool IsAvailable(double? idTarget, SpellCategory? category = null)
        {
            if (_nbTurnToWait > 0)
                return false;

            // Limit on usage per turn not reached
            if (LevelTemplate.MaxCastPerTurn > 0 && _nbCastAllowed == 0)
                return false;

            if (!HasCategory(category))
                return false;

            // No restriction per target => available
            if (LevelTemplate.MaxCastPerTarget <= 0 || _nbCastAllowed > 0) return true;

            // No target identified
            if (idTarget == null) return true;

            if (_targeted != null)
            {
                var targetCount = 0;
                if (!_targeted.TryGetValue(idTarget.Value, out targetCount))
                    targetCount = 0;
                if (targetCount >= LevelTemplate.MaxCastPerTarget)
                    return false;
            }

            return true;
        }

        #endregion Stuff to control availability of the spell
    }
}