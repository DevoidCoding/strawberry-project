using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Cross : IShape
    {
        public Cross(int minRadius, int radius, DirectionsEnum? direction = null)
        {
            MinRadius = minRadius;
            Radius = radius;

            DisabledDirections = new List<DirectionsEnum>();
            if (direction != null) Direction = direction.Value;
        }

        public bool AllDirections { get; set; }

        public bool Diagonal { get; set; }

        public List<DirectionsEnum> DisabledDirections { get; set; }

        public bool OnlyPerpendicular { get; set; }

        #region IShape Members

        public uint Surface
        {
            get { return (uint) Radius*4 + 1; }
        }

        public int MinRadius { get; set; }

        public DirectionsEnum Direction { get; set; }

        public int Radius { get; set; }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            var result = new List<Cell>();

            if (MinRadius == 0)
                result.Add(centerCell);

            var disabledDirections = DisabledDirections.ToList();
            if (OnlyPerpendicular)
            {
                switch (Direction)
                {
                    case DirectionsEnum.DirectionSouthEast:
                    case DirectionsEnum.DirectionNorthWest:
                    {
                        disabledDirections.Add(DirectionsEnum.DirectionSouthEast);
                        disabledDirections.Add(DirectionsEnum.DirectionNorthWest);
                        break;
                    }
                    case DirectionsEnum.DirectionNorthEast:
                    case DirectionsEnum.DirectionSouthWest:
                    {
                        disabledDirections.Add(DirectionsEnum.DirectionNorthEast);
                        disabledDirections.Add(DirectionsEnum.DirectionSouthWest);
                        break;
                    }
                    case DirectionsEnum.DirectionSouth:
                    case DirectionsEnum.DirectionNorth:
                    {
                        disabledDirections.Add(DirectionsEnum.DirectionSouth);
                        disabledDirections.Add(DirectionsEnum.DirectionNorth);
                        break;
                    }
                    case DirectionsEnum.DirectionEast:
                    case DirectionsEnum.DirectionWest:
                    {
                        disabledDirections.Add(DirectionsEnum.DirectionEast);
                        disabledDirections.Add(DirectionsEnum.DirectionWest);
                        break;
                    }
                }
            }

            for (var i = Radius; i > 0; i--)
            {
                if (i < MinRadius)
                    continue;

                if (!Diagonal)
                {
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionSouthEast))
                        AddCellIfValid(centerCell.X + i, centerCell.Y, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionNorthWest))
                        AddCellIfValid(centerCell.X - i, centerCell.Y, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionNorthEast))
                        AddCellIfValid(centerCell.X, centerCell.Y + i, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionSouthWest))
                        AddCellIfValid(centerCell.X, centerCell.Y - i, map, result);
                }

                if (Diagonal || AllDirections)
                {
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionSouth))
                        AddCellIfValid(centerCell.X + i, centerCell.Y - i, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionNorth))
                        AddCellIfValid(centerCell.X - i, centerCell.Y + i, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionEast))
                        AddCellIfValid(centerCell.X + i, centerCell.Y + i, map, result);
                    if (!disabledDirections.Contains(DirectionsEnum.DirectionWest))
                        AddCellIfValid(centerCell.X - i, centerCell.Y - i, map, result);
                }
            }

            return result.ToArray();
        }

        private static void AddCellIfValid(int x, int y, Map map, IList<Cell> container)
        {
            if (!Cell.IsInMap(x, y))
                return;

            container.Add(map.Cells[x, y]);
        }

        #endregion
    }
}