using System.Diagnostics;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Zone : IShape
    {
        private SpellShapeEnum _shapeType;

        public Zone(string rawZone)
        {
            if (string.IsNullOrWhiteSpace(rawZone)) return;

            var parameters = rawZone.Substring(1).Split(',');
            var zoneShape = (SpellShapeEnum) rawZone[0];
            var hasMinSize = zoneShape == SpellShapeEnum.C || zoneShape == SpellShapeEnum.X ||
                             zoneShape == SpellShapeEnum.Q || zoneShape == SpellShapeEnum.Plus ||
                             zoneShape == SpellShapeEnum.Sharp || zoneShape == SpellShapeEnum.L;

            Radius = int.Parse(parameters[0]);
            if (hasMinSize && parameters.Length >= 2)
                MinRadius = int.Parse(parameters[1]);
        }

        public Zone(SpellShapeEnum shape, int radius)
        {
            Radius = radius;
            ShapeType = shape;
        }

        public Zone(SpellShapeEnum shape, int radius, DirectionsEnum direction)
        {
            Radius = radius;
            Direction = direction;
            ShapeType = shape;
        }

        public IShape Shape { get; private set; }

        public SpellShapeEnum ShapeType
        {
            get { return _shapeType; }
            set
            {
                _shapeType = value;
                InitializeShape();
            }
        }

        private void InitializeShape()
        {
            switch (ShapeType)
            {
                case SpellShapeEnum.X:
                    Shape = new Cross(0, Radius);
                    break;
                case SpellShapeEnum.L:
                    Shape = new Line(Radius);
                    break;
                case SpellShapeEnum.T:
                    Shape = new Cross(0, Radius)
                    {
                        OnlyPerpendicular = true
                    };
                    break;
                case SpellShapeEnum.D:
                    Shape = new Cross(0, Radius);
                    break;
                case SpellShapeEnum.C:
                    Shape = new Lozenge(0, Radius);
                    break;
                case SpellShapeEnum.I:
                    Shape = new Lozenge(Radius, 63);
                    break;
                case SpellShapeEnum.O:
                    Shape = new Cross(1, Radius);
                    break;
                case SpellShapeEnum.Q:
                    Shape = new Cross(1, Radius);
                    break;
                case SpellShapeEnum.V:
                    Shape = new Cone(0, Radius);
                    break;
                case SpellShapeEnum.W:
                    Shape = new Square(0, Radius)
                    {
                        DiagonalFree = true
                    };
                    break;
                case SpellShapeEnum.Plus:
                    Shape = new Cross(0, Radius)
                    {
                        Diagonal = true
                    };
                    break;
                case SpellShapeEnum.Sharp:
                    Shape = new Cross(1, Radius)
                    {
                        Diagonal = true
                    };
                    break;
                case SpellShapeEnum.Star:
                    Shape = new Cross(0, Radius)
                    {
                        AllDirections = true
                    };
                    break;
                case SpellShapeEnum.Minus:
                    Shape = new Cross(0, Radius)
                    {
                        OnlyPerpendicular = true,
                        Diagonal = true
                    };
                    break;
                case SpellShapeEnum.G:
                    Shape = new Square(0, Radius);
                    break;
                case SpellShapeEnum.Slash:
                    Shape = new Line(Radius);
                    break;
                case SpellShapeEnum.U:
                    Shape = new HalfLozenge(0, Radius);
                    break;
                case SpellShapeEnum.A:
                    Shape = new Lozenge(0, 63);
                    break;
                case SpellShapeEnum.P:
                    Shape = new Single();
                    break;
                default:
                    Shape = new Cross(0, 0);
                    break;
            }

            Shape.Direction = Direction;
        }

        #region IShape Members

        public uint Surface
        {
            get
            {
                if (Shape == null) return 0;
                return Shape.Surface;
            }
        }

        public int MinRadius
        {
            get { return Shape.MinRadius; }
            set
            {
                if (Shape != null) Shape.MinRadius = value;
                else Debug.Assert(false);
            }
        }

        public DirectionsEnum Direction
        {
            get { return _direction; }
            set
            {
                _direction = value;
                if (Shape != null)
                    Shape.Direction = value;
            }
        }

        private int _radius;
        private DirectionsEnum _direction;

        public int Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                if (Shape != null)
                    Shape.Radius = value;
            }
        }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            return Shape.GetCells(centerCell, map);
        }

        #endregion
    }
}