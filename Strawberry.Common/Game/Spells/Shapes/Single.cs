﻿using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Single : IShape
    {
        public DirectionsEnum Direction
        {
            get { return DirectionsEnum.DirectionNorth; }
            set { }
        }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            return new[] {centerCell};
        }

        public int MinRadius
        {
            get { return 1; }
            set { }
        }

        public int Radius
        {
            get { return 1; }
            set { }
        }

        public uint Surface => 1;
    }
}