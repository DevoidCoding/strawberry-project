using System;
using System.Collections.Generic;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class HalfLozenge : IShape
    {
        public HalfLozenge(int minRadius, int radius, DirectionsEnum direction = DirectionsEnum.DirectionNorth)
        {
            MinRadius = minRadius;
            Radius = radius;

            Direction = direction;
        }

        public DirectionsEnum Direction { get; set; }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            var result = new List<Cell>();

            if (MinRadius == 0)
                result.Add(centerCell);

            for (var i = 1; i <= Radius; i++)
            {
                switch (Direction)
                {
                    case DirectionsEnum.DirectionNorthWest:
                        AddCellIfValid(centerCell.X + i, centerCell.Y + i, map, result);
                        AddCellIfValid(centerCell.X + i, centerCell.Y - i, map, result);
                        break;

                    case DirectionsEnum.DirectionNorthEast:
                        AddCellIfValid(centerCell.X - i, centerCell.Y - i, map, result);
                        AddCellIfValid(centerCell.X + i, centerCell.Y - i, map, result);
                        break;

                    case DirectionsEnum.DirectionSouthEast:
                        AddCellIfValid(centerCell.X - i, centerCell.Y + i, map, result);
                        AddCellIfValid(centerCell.X - i, centerCell.Y - i, map, result);
                        break;

                    case DirectionsEnum.DirectionSouthWest:
                        AddCellIfValid(centerCell.X - i, centerCell.Y + i, map, result);
                        AddCellIfValid(centerCell.X + i, centerCell.Y + i, map, result);
                        break;
                    case DirectionsEnum.DirectionEast:
                        break;
                    case DirectionsEnum.DirectionSouth:
                        break;
                    case DirectionsEnum.DirectionWest:
                        break;
                    case DirectionsEnum.DirectionNorth:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return result.ToArray();
        }

        public int MinRadius { get; set; }

        public int Radius { get; set; }

        public uint Surface => (uint) Radius*2 + 1;

        private static void AddCellIfValid(int x, int y, Map map, IList<Cell> container)
        {
            if (!Cell.IsInMap(x, y))
                return;

            container.Add(map.Cells[x, y]);
        }
    }
}