using System;
using System.Collections.Generic;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Cone : IShape
    {
        public Cone(int minRadius, int radius, DirectionsEnum direction = DirectionsEnum.DirectionSouthEast)
        {
            MinRadius = minRadius;
            Radius = radius;
            Direction = direction;
        }

        #region IShape Members

        public uint Surface => ((uint) Radius + 1)*((uint) Radius + 1);

        public int MinRadius { get; set; }

        public DirectionsEnum Direction { get; set; }

        public int Radius { get; set; }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            var result = new List<Cell>();

            if (Radius == 0)
            {
                if (MinRadius == 0)
                    result.Add(centerCell);

                return result.ToArray();
            }

            var i = 0;
            var j = 1;
            var y = 0;
            var x = 0;
            switch (Direction)
            {
                case DirectionsEnum.DirectionNorthWest:
                    x = centerCell.X;
                    while (x >= centerCell.X - Radius)
                    {
                        y = -i;
                        while (y <= i)
                        {
                            if (MinRadius == 0 || Math.Abs(centerCell.X - x) + Math.Abs(y) >= MinRadius)
                                AddCellIfValid(x, y + centerCell.Y, map, result);

                            y++;
                        }
                        i = i + j;
                        x--;
                    }
                    break;
                case DirectionsEnum.DirectionSouthWest:
                    y = centerCell.Y;
                    while (y >= centerCell.Y - Radius)
                    {
                        x = -i;
                        while (x <= i)
                        {
                            if (MinRadius == 0 || Math.Abs(x) + Math.Abs(centerCell.Y - y) >= MinRadius)
                                AddCellIfValid(x + centerCell.X, y, map, result);

                            x++;
                        }
                        i = i + j;
                        y--;
                    }
                    break;
                case DirectionsEnum.DirectionSouthEast:
                    x = centerCell.X;
                    while (x <= centerCell.X + Radius)
                    {
                        y = -i;
                        while (y <= i)
                        {
                            if (MinRadius == 0 || Math.Abs(centerCell.X - x) + Math.Abs(y) >= MinRadius)
                                AddCellIfValid(x, y + centerCell.Y, map, result);

                            y++;
                        }
                        i = i + j;
                        x++;
                    }
                    break;
                case DirectionsEnum.DirectionNorthEast:
                    y = centerCell.Y;
                    while (y <= centerCell.Y - Radius)
                    {
                        x = -i;
                        while (x <= i)
                        {
                            if (MinRadius == 0 || Math.Abs(x) + Math.Abs(centerCell.Y - y) >= MinRadius)
                                AddCellIfValid(x + centerCell.X, y, map, result);

                            x++;
                        }
                        i = i + j;
                        y++;
                    }
                    break;
            }

            return result.ToArray();
        }

        private static void AddCellIfValid(int x, int y, Map map, IList<Cell> container)
        {
            if (!Cell.IsInMap(x, y))
                return;

            container.Add(map.Cells[x, y]);
        }

        #endregion
    }
}