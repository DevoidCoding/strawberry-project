using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public interface IShape
    {
        DirectionsEnum Direction { get; set; }

        int MinRadius { get; set; }

        int Radius { get; set; }

        uint Surface { get; }

        Cell[] GetCells(Cell centerCell, Map map);
    }
}