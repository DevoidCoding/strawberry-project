using System;
using System.Collections.Generic;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Lozenge : IShape
    {
        public Lozenge(int minRadius, int radius)
        {
            MinRadius = minRadius;
            Radius = radius;
        }

        #region IShape Members

        public uint Surface => (uint) (((uint) Radius + 1)*((uint) Radius + 1) + Radius*(uint) Radius);

        public int MinRadius { get; set; }

        public DirectionsEnum Direction { get; set; }

        public int Radius { get; set; }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            var result = new List<Cell>();

            if (Radius == 0)
            {
                if (MinRadius == 0)
                    result.Add(centerCell);

                return result.ToArray();
            }

            var x = centerCell.X - Radius;
            var i = 0;
            var j = 1;
            while (x <= centerCell.X + Radius)
            {
                var y = -i;

                while (y <= i)
                {
                    if (MinRadius == 0 || Math.Abs(centerCell.X - x) + Math.Abs(y) >= MinRadius)
                        AddCellIfValid(x, y + centerCell.Y, map, result);

                    y++;
                }

                if (i == Radius)
                {
                    j = -j;
                }

                i = i + j;
                x++;
            }

            return result.ToArray();
        }

        private static void AddCellIfValid(int x, int y, Map map, IList<Cell> container)
        {
            if (!Cell.IsInMap(x, y))
                return;

            container.Add(map.Cells[x, y]);
        }

        #endregion
    }
}