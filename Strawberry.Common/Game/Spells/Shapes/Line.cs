using System;
using System.Collections.Generic;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Spells.Shapes
{
    public class Line : IShape
    {
        public Line(int radius, DirectionsEnum direction = DirectionsEnum.DirectionSouthEast)
        {
            Radius = radius;
            Direction = direction;
        }

        #region IShape Members

        public uint Surface => (uint) Radius + 1;

        public int MinRadius { get; set; }

        public DirectionsEnum Direction { get; set; }

        public int Radius { get; set; }

        public Cell[] GetCells(Cell centerCell, Map map)
        {
            var result = new List<Cell>();

            for (var i = MinRadius; i <= Radius; i++)
            {
                switch (Direction)
                {
                    case DirectionsEnum.DirectionWest:
                        AddCellIfValid(centerCell.X - i, centerCell.Y - i, map, result);
                        break;
                    case DirectionsEnum.DirectionNorth:
                        AddCellIfValid(centerCell.X - i, centerCell.Y + i, map, result);
                        break;
                    case DirectionsEnum.DirectionEast:
                        AddCellIfValid(centerCell.X + i, centerCell.Y + i, map, result);
                        break;
                    case DirectionsEnum.DirectionSouth:
                        AddCellIfValid(centerCell.X + i, centerCell.Y - i, map, result);
                        break;
                    case DirectionsEnum.DirectionNorthWest:
                        AddCellIfValid(centerCell.X - i, centerCell.Y, map, result);
                        break;
                    case DirectionsEnum.DirectionSouthWest:
                        AddCellIfValid(centerCell.X, centerCell.Y - i, map, result);
                        break;
                    case DirectionsEnum.DirectionSouthEast:
                        AddCellIfValid(centerCell.X + i, centerCell.Y, map, result);
                        break;
                    case DirectionsEnum.DirectionNorthEast:
                        AddCellIfValid(centerCell.X, centerCell.Y + i, map, result);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return result.ToArray();
        }

        private static void AddCellIfValid(int x, int y, Map map, IList<Cell> container)
        {
            if (!Cell.IsInMap(x, y))
                return;

            container.Add(map.Cells[x, y]);
        }

        #endregion
    }
}