﻿using System;
using Strawberry.Common.Game.World;

namespace Strawberry.Common.Game.Spells
{
    public class SpellTarget
    {
        public SpellTarget(double efficiency = 0, Cell source = null, Cell target = null, Spell spell = null)
        {
            Efficiency = efficiency;
            FromCell = source;
            TargetCell = target;
            Spell = spell;
            Cast = spell == null;
        }

        public bool Cast { get; set; }
        public string Comment { get; set; }

        public double Efficiency { get; set; }
        public Cell FromCell { get; set; }
        public Spell Spell { get; set; }
        public Cell TargetCell { get; set; }
        public TimeSpan TimeSpan { get; set; }
    }
}