﻿// <copyright file="EffectDate.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectDate : EffectBase
    {
        protected uint m_day;
        protected uint m_hour;
        protected uint m_minute;
        protected uint m_month;
        protected uint m_year;

        public EffectDate()
        {
        }

        public EffectDate(EffectDate copy)
            : this(copy.Id, copy.m_year, copy.m_month, copy.m_day, copy.m_hour, copy.m_minute, copy)
        {
        }

        public EffectDate(ushort id, uint year, uint month, uint day, uint hour, uint minute, EffectBase effect)
            : base(id, effect)
        {
            m_year = year;
            m_month = month;
            m_day = day;
            m_hour = hour;
            m_minute = minute;
            FixDate();
        }

        public EffectDate(ObjectEffectDate effect)
            : base(effect)
        {
            m_year = effect.Year;
            m_month = (uint)effect.Month;
            m_day = (uint)effect.Day;
            m_hour = (uint)effect.Hour;
            m_minute = (uint)effect.Minute;
            FixDate();
        }

        public EffectDate(EffectInstanceDate effect)
            : base(effect)
        {
            m_year =  effect.Year;
            m_month =  effect.Month;
            m_day =  effect.Day;
            m_hour =  effect.Hour;
            m_minute =  effect.Minute;
            FixDate();
        }

        public override int ProtocoleId
        {
            get { return 72; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectDate))
                return false;
            return base.Equals(obj) && GetDate().Equals((obj as EffectDate).GetDate());
        }

        public bool Equals(EffectDate other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_day == m_day && other.m_hour == m_hour && other.m_minute == m_minute &&
                   other.m_month == m_month && other.m_year == m_year;
        }

        public DateTime GetDate()
        {
            return new DateTime((int)m_year, (int)m_month, (int)m_day, (int)m_hour, (int)m_minute, 0);
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceDate
            {
                EffectId = (uint) Id,
                TargetId = (int) Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint) ZoneShape,
                Year = (uint) m_year,
                Month = (uint) m_month,
                Day = (uint) m_day,
                Hour = (uint) m_hour,
                Minute = (uint) m_minute
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = base.GetHashCode();
                result = (result*397) ^ m_day.GetHashCode();
                result = (result*397) ^ m_hour.GetHashCode();
                result = (result*397) ^ m_minute.GetHashCode();
                result = (result*397) ^ m_month.GetHashCode();
                result = (result*397) ^ m_year.GetHashCode();
                return result;
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectDate(Id, (ushort)m_year, (sbyte)m_month, (sbyte)m_day, (sbyte)m_hour, (sbyte)m_minute);
        }

        public override object[] GetValues()
        {
            return new object[]
            {
                m_year.ToString(), m_month.ToString("00") + m_day.ToString("00"),
                m_hour.ToString("00") + m_minute.ToString("00")
            };
        }

        public static bool operator ==(EffectDate a, EffectDate b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectDate a, EffectDate b)
        {
            return !(a == b);
        }


        private void FixDate()
        {
            if (m_year < 1) m_year = 1;
            if (m_year > 9999) m_year = 9999;
            if (m_month < 1) m_month = 1;
            if (m_month > 12) m_month = 12;
            if (m_day < 1) m_day = 1;
            if (m_day > 31) m_day = 31;
        }
    }
}