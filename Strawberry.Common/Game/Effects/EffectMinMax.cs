﻿// <copyright file="EffectMinMax.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectMinMax : EffectBase
    {
        protected uint m_maxvalue;
        protected uint m_minvalue;

        public EffectMinMax()
        {
        }

        public EffectMinMax(EffectMinMax copy)
            : this(copy.Id, copy.ValueMin, copy.ValueMax, copy)
        {
        }

        public EffectMinMax(ushort id, uint valuemin, uint valuemax, EffectBase effect)
            : base(id, effect)
        {
            m_minvalue = valuemin;
            m_maxvalue = valuemax;
        }

        public EffectMinMax(ObjectEffectMinMax effect)
            : base(effect)
        {
            m_maxvalue = effect.Max;
            m_minvalue = effect.Min;
        }

        public EffectMinMax(EffectInstanceMinMax effect)
            : base(effect)
        {
            m_maxvalue = effect.Max;
            m_minvalue = effect.Min;
        }

        public override int ProtocoleId
        {
            get { return 82; }
        }

        public uint ValueMax
        {
            get { return m_maxvalue; }
            set { m_maxvalue = value; }
        }

        public uint ValueMin
        {
            get { return m_minvalue; }
            set { m_minvalue = value; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectMinMax))
                return false;
            var b = obj as EffectMinMax;
            return base.Equals(obj) && m_minvalue == b.m_minvalue && m_maxvalue == b.m_maxvalue;
        }

        public bool Equals(EffectMinMax other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_maxvalue == m_maxvalue && other.m_minvalue == m_minvalue;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceMinMax
            {
                EffectId = (uint)Id,
                TargetId = (int)Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint)ZoneShape,
                Max = (uint) ValueMax,
                Min = (uint) ValueMin
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = base.GetHashCode();
                result = (result*397) ^ m_maxvalue.GetHashCode();
                result = (result*397) ^ m_minvalue.GetHashCode();
                return result;
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectMinMax(Id, ValueMin, ValueMax);
        }

        public override object[] GetValues()
        {
            return new object[] {ValueMin, ValueMax};
        }

        public static bool operator ==(EffectMinMax a, EffectMinMax b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectMinMax a, EffectMinMax b)
        {
            return !(a == b);
        }
    }
}