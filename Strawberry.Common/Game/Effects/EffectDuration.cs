﻿// <copyright file="EffectDuration.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectDuration : EffectBase
    {
        protected uint m_days;
        protected uint m_hours;
        protected uint m_minutes;

        public EffectDuration()
        {
        }

        public EffectDuration(EffectDuration copy)
            : this(copy.Id, copy.m_days, copy.m_hours, copy.m_minutes, copy)
        {
        }

        public EffectDuration(ushort id, uint days, uint hours, uint minutes, EffectBase effect)
            : base(id, effect)
        {
            m_days = days;
            m_hours = hours;
            m_minutes = minutes;
        }

        public EffectDuration(ObjectEffectDuration effect)
            : base(effect)
        {
            m_days = effect.Days;
            m_hours = (uint)effect.Hours;
            m_minutes = (uint)effect.Minutes;
        }

        public EffectDuration(EffectInstanceDuration effect)
            : base(effect)
        {
            m_days = effect.Days;
            m_hours = (uint) effect.Hours;
            m_minutes = (uint) effect.Minutes;
        }

        public override int ProtocoleId
        {
            get { return 75; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectDuration))
                return false;
            return base.Equals(obj) && GetTimeSpan().Equals((obj as EffectDuration).GetTimeSpan());
        }

        public bool Equals(EffectDuration other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_days == m_days && other.m_hours == m_hours &&
                   other.m_minutes == m_minutes;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceDuration
            {
                EffectId = (uint)Id,
                TargetId = (int)Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint)ZoneShape,
                Days = (uint) m_days,
                Hours = (uint) m_hours,
                Minutes = (uint) m_minutes
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = base.GetHashCode();
                result = (result*397) ^ m_days.GetHashCode();
                result = (result*397) ^ m_hours.GetHashCode();
                result = (result*397) ^ m_minutes.GetHashCode();
                return result;
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectDuration(Id, (ushort)m_days, (sbyte)m_hours, (sbyte)m_minutes);
        }

        public TimeSpan GetTimeSpan()
        {
            return new TimeSpan((int)m_days, (int)m_hours, (int)m_minutes, 0);
        }

        public override object[] GetValues()
        {
            return new object[] {m_days, m_hours, m_minutes};
        }

        public static bool operator ==(EffectDuration a, EffectDuration b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectDuration a, EffectDuration b)
        {
            return !(a == b);
        }
    }
}