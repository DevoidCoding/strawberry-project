﻿// <copyright file="EffectCreature.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectCreature : EffectBase
    {
        protected uint m_monsterfamily;

        public EffectCreature()
        {
        }

        public EffectCreature(EffectCreature copy)
            : this(copy.Id, copy.MonsterFamily, copy)
        {
        }

        public EffectCreature(ushort id, uint monsterfamily, EffectBase effectBase)
            : base(id, effectBase)
        {
            m_monsterfamily = monsterfamily;
        }

        public EffectCreature(ObjectEffectCreature effect)
            : base(effect)
        {
            m_monsterfamily =  effect.MonsterFamilyId;
        }

        public EffectCreature(EffectInstanceCreature effect)
            : base(effect)
        {
            m_monsterfamily = (ushort) effect.MonsterFamilyId;
        }

        public uint MonsterFamily
        {
            get { return m_monsterfamily; }
        }

        public override int ProtocoleId
        {
            get { return 71; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectCreature))
                return false;
            return base.Equals(obj) && m_monsterfamily == (obj as EffectCreature).m_monsterfamily;
        }

        public bool Equals(EffectCreature other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_monsterfamily == m_monsterfamily;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceCreature
            {
                EffectId = (uint) Id,
                TargetId = (int) Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint) ZoneShape,
                MonsterFamilyId = (uint) MonsterFamily
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode()*397) ^ m_monsterfamily.GetHashCode();
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectCreature(Id, (ushort) MonsterFamily);
        }

        public override object[] GetValues()
        {
            return new object[] {m_monsterfamily};
        }

        public static bool operator ==(EffectCreature a, EffectCreature b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectCreature a, EffectCreature b)
        {
            return !(a == b);
        }
    }
}