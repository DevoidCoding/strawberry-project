﻿// <copyright file="EffectDice.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectDice : EffectInteger
    {
        protected uint m_diceface;
        protected uint m_dicenum;

        public EffectDice()
        {
        }

        public EffectDice(EffectDice copy)
            : this(copy.Id, copy.Value, copy.DiceNum, copy.DiceFace, copy)
        {
        }

        public EffectDice(ushort id, int value, uint dicenum, uint diceface, EffectBase effect)
            : base(id, value, effect)
        {
            m_dicenum = dicenum;
            m_diceface = diceface;
        }

        public EffectDice(ObjectEffectDice effect)
            : base(effect, effect.DiceConst)
        {
            m_diceface = effect.DiceSide;
            m_dicenum = effect.DiceNum;
        }

        public EffectDice(EffectInstanceDice effect)
            : base(effect)
        {
            m_dicenum = (ushort) effect.DiceNum;
            m_diceface = (ushort) effect.DiceSide;
        }

        public uint DiceFace
        {
            get { return m_diceface; }
            set { m_diceface = value; }
        }

        public uint DiceNum
        {
            get { return m_dicenum; }
            set { m_dicenum = value; }
        }

        public override int ProtocoleId
        {
            get { return 73; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectDice))
                return false;
            var b = obj as EffectDice;
            return base.Equals(obj) && m_diceface == b.m_diceface && m_dicenum == b.m_dicenum;
        }

        public bool Equals(EffectDice other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_diceface == m_diceface && other.m_dicenum == m_dicenum;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceDice
            {
                EffectId = Id,
                TargetId = (int) Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint) ZoneShape,
                Value = Value,
                DiceNum = DiceNum,
                DiceSide = DiceFace
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = base.GetHashCode();
                result = (result*397) ^ (int)m_diceface;
                result = (result*397) ^ (int)m_dicenum;
                return result;
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectDice(Id, (ushort)DiceNum, (ushort)DiceFace, (ushort)Value);
        }

        public override object[] GetValues()
        {
            return new object[] {DiceNum, DiceFace, Value};
        }

        public static bool operator ==(EffectDice a, EffectDice b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectDice a, EffectDice b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return $"{(EffectsEnum)Id} {DiceNum}-{DiceFace}, Targets: {Targets}, Area:{AreaDesc()} - {Description}";
        }
    }
}