﻿// <copyright file="EffectInteger.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectInteger : EffectBase
    {
        protected int m_value;

        public EffectInteger()
        {
        }

        public EffectInteger(EffectInteger copy)
            : this(copy.Id, copy.Value, copy)
        {
        }

        public EffectInteger(ushort id, int value, EffectBase effect)
            : base(id, effect)
        {
            m_value = value;
        }

        public EffectInteger(ObjectEffect effect, int value)
            : base(effect)
        {
            m_value =  value;
        }

        public EffectInteger(ObjectEffectInteger effect)
            : base(effect)
        {
            m_value =  (int)effect.Value;
        }

        public EffectInteger(EffectInstanceInteger effect)
            : base(effect)
        {
            m_value = (ushort)effect.Value;
        }

        public override int ProtocoleId
        {
            get { return 70; }
        }

        public int Value
        {
            get { return m_value; }
            set { m_value = value; }
        }

        public bool Equals(EffectInteger other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_value == m_value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as EffectInteger);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode()*397) ^ m_value.GetHashCode();
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectInteger(Id, (ushort)Value);
        }

        public override object[] GetValues()
        {
            return new object[] {Value};
        }

        public static bool operator ==(EffectInteger left, EffectInteger right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EffectInteger left, EffectInteger right)
        {
            return !Equals(left, right);
        }
    }
}