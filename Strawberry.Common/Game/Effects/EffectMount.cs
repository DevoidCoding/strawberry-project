﻿// <copyright file="EffectMount.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectMount : EffectBase
    {
        protected double m_date;
        protected uint m_modelId;
        protected uint m_mountId;

        public EffectMount()
        {
        }

        public EffectMount(EffectMount copy)
            : this(copy.Id, copy.m_mountId, copy.m_date, copy.m_modelId, copy)
        {
        }

        public EffectMount(ushort id, uint mountid, double date, uint modelid, EffectBase effect)
            : base(id, effect)
        {
            m_mountId = mountid;
            m_date = date;
            m_modelId = modelid;
        }

        public EffectMount(ObjectEffectMount effect)
            : base(effect)
        {
            m_mountId = (uint) effect.MountId;
            m_date = (float) effect.Date;
            m_modelId = effect.ModelId;
        }

        public EffectMount(EffectInstanceMount effect)
            : base(effect)
        {
            m_mountId = effect.MountId;
            m_date = effect.Date;
            m_modelId = effect.ModelId;
        }

        public override int ProtocoleId
        {
            get { return 179; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectMount))
                return false;
            var b = obj as EffectMount;
            return base.Equals(obj) && m_mountId == b.m_mountId && m_date == b.m_date && m_modelId == b.m_modelId;
        }

        public bool Equals(EffectMount other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_date.Equals(m_date) && other.m_modelId == m_modelId &&
                   other.m_mountId == m_mountId;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceMount
            {
                EffectId = (uint)Id,
                TargetId = (int)Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint)ZoneShape,
                ModelId = (uint) m_modelId,
                Date = (float) m_date,
                MountId = (uint) m_mountId
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = base.GetHashCode();
                result = (result*397) ^ m_date.GetHashCode();
                result = (result*397) ^ (int)m_modelId;
                result = (result*397) ^ (int)m_mountId;
                return result;
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectMount(Id, (int)m_mountId, m_date, (ushort)m_modelId);
        }

        public override object[] GetValues()
        {
            return new object[] {m_mountId, m_date, m_modelId};
        }

        public static bool operator ==(EffectMount a, EffectMount b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectMount a, EffectMount b)
        {
            return !(a == b);
        }
    }
}