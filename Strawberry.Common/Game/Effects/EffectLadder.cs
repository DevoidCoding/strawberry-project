﻿// <copyright file="EffectLadder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public class EffectLadder : EffectCreature
    {
        protected uint m_monsterCount;

        public EffectLadder()
        {
        }

        public EffectLadder(EffectLadder copy)
            : this(copy.Id, copy.MonsterFamily, copy.MonsterCount, copy)
        {
        }

        public EffectLadder(ushort id, uint monsterfamily, uint monstercount, EffectBase effect)
            : base(id, monsterfamily, effect)
        {
            m_monsterCount = monstercount;
        }

        public EffectLadder(ObjectEffectLadder effect)
            : base(effect)
        {
            m_monsterCount =  effect.MonsterCount;
        }

        public EffectLadder(EffectInstanceLadder effect)
            : base(effect)
        {
            m_monsterCount =  effect.MonsterCount;
        }

        public uint MonsterCount
        {
            get { return m_monsterCount; }
            set { m_monsterCount = value; }
        }

        public override int ProtocoleId
        {
            get { return 81; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EffectLadder))
                return false;
            return base.Equals(obj) && m_monsterCount == (obj as EffectLadder).m_monsterCount;
        }

        public bool Equals(EffectLadder other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other.m_monsterCount == m_monsterCount;
        }

        public override EffectInstance GetEffectInstance()
        {
            return new EffectInstanceLadder
            {
                EffectId = (uint)Id,
                TargetId = (int)Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint)ZoneShape,
                MonsterCount = (uint) m_monsterCount,
                MonsterFamilyId = (uint) m_monsterfamily
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode()*397) ^ m_monsterCount.GetHashCode();
            }
        }

        public override ObjectEffect GetObjectEffect()
        {
            return new ObjectEffectLadder(Id, (ushort)MonsterFamily, MonsterCount);
        }

        public override object[] GetValues()
        {
            return new object[] {m_monsterCount, m_monsterfamily};
        }

        public static bool operator ==(EffectLadder a, EffectLadder b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (((object) a == null) || ((object) b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EffectLadder a, EffectLadder b)
        {
            return !(a == b);
        }
    }
}