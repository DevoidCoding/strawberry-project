﻿// <copyright file="EffectBase.AI.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.Fighters;
using Strawberry.Common.Game.Spells;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Effects
{
    public partial class EffectBase
    {
        /// <summary>
        ///     Warning : this method says if this affect may affect this target. But NOT if the target can be the target of the
        ///     spell
        ///     (cf épée divine, where you cast it on yourself despite it effects only enemies around you)
        /// </summary>
        /// <param name="spellEffect"></param>
        /// <param name="spell"></param>
        /// <param name="caster"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool CanAffectTarget(EffectDice spellEffect, Spell spell, PlayedFighter caster, Fighter target)
        {
            if (spell.LevelTemplate.SpellBreed == (uint) BreedEnum.Eniripsa &&
                spell.Categories == Spell.SpellCategory.Healing && caster.HasState(76)) return false;
            //if (!spell.IsAvailable(target == null ? null : (int?)target.Id)) return false;
            //Strawberry.Common.Game.Spells.Spell.SpellCategory categories = 0;
            var surface = spellEffect.Surface;
            //categories = Strawberry.Common.Game.Spells.Spell.GetEffectCategories((uint)spellEffect.Id, spell.LevelTemplate.id);
            if (spellEffect.Targets == SpellTargetType.None) spellEffect.Targets = SpellTargetType.All;
            //if (target == null) return !spell.LevelTemplate.needTakenCell;

            if (caster == target) // Self
                return (spellEffect.Targets & (SpellTargetType.OnlySelf | SpellTargetType.Self)) != 0;


            if (caster.Team == target.Team) // Ally
                if (target.Summoned)
                    return (spellEffect.Targets & SpellTargetType.AlliesSummon) != 0;
                else
                    return (spellEffect.Targets & SpellTargetType.AlliesNonSummon) != 0;

            if (target.Summoned) // Enemies
                return (spellEffect.Targets & SpellTargetType.EnemiesSummon) != 0;
            return (spellEffect.Targets & SpellTargetType.EnemiesNonSummon) != 0;
        }
    }
}