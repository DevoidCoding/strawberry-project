// <copyright file="SpellTargetType.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;

namespace Strawberry.Common.Game.Effects
{
    [Flags]
    public enum SpellTargetType
    {
        None = 0,
        Self = 0x1,
        Ally1 = 0x2,
        Ally2 = 0x4,
        AllySummons = 0x8,
        AllyStaticSummons = 0x10,
        Ally3 = 0x20,
        Ally4 = 0x40,
        Ally5 = 0x80,
        Enemy1 = 0x100,
        Enemy2 = 0x200,
        EnemySummons = 0x400,
        EnemyStaticSummons = 0x800,
        Enemy3 = 0x1000,
        Enemy4 = 0x2000,
        Enemy5 = 0x4000,
        AlliesSummon = AllySummons | AllyStaticSummons,
        EnemiesSummon = EnemySummons | EnemyStaticSummons,
        AlliesNonSummon = Ally1 | Ally2 | Ally3 | Ally4 | Ally5,
        EnemiesNonSummon = Enemy1 | Enemy2 | Enemy3 | Enemy4 | Enemy5,
        Allies = AlliesSummon | AlliesNonSummon,
        Enemies = EnemiesSummon | EnemiesNonSummon,
        All = 0x7FFF,
        OnlySelf = 0x8000
    }
}