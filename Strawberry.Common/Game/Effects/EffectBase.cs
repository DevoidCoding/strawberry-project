﻿// <copyright file="EffectBase.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Spells.Shapes;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Effects
{
    [Serializable]
    public partial class EffectBase : ICloneable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _description;

        [NonSerialized] protected Effect _template;

        private bool _initialized = false;
        private string _rawZone = string.Empty;

        public EffectBase()
        {
        }

        public EffectBase(EffectBase effect)
        {
            Id = effect.Id;
            _template = ObjectDataManager.Instance.Get<Effect>(effect.Id);
            Targets = effect.Targets;
            Delay = effect.Delay;
            Duration = effect.Duration;
            Group = effect.Group;
            Random = effect.Random;
            Modificator = effect.Modificator;
            Trigger = effect.Trigger;
            Hidden = effect.Hidden;
            ZoneSize = effect.ZoneSize;
            ZoneMinSize = effect.ZoneMinSize;
            ZoneShape = effect.ZoneShape;
            _rawZone = effect._rawZone;

            if (!_initialized) Initialize();
        }

        public EffectBase(ushort id, EffectBase effect)
        {
            Id = id;
            _template = ObjectDataManager.Instance.Get<Effect>(id);
            Targets = effect.Targets;
            Delay = effect.Delay;
            Duration = effect.Duration;
            Group = effect.Group;
            Random = effect.Random;
            Modificator = effect.Modificator;
            Trigger = effect.Trigger;
            Hidden = effect.Hidden;
            ZoneSize = effect.ZoneSize;
            ZoneMinSize = effect.ZoneMinSize;
            ZoneShape = effect.ZoneShape;
            _rawZone = effect._rawZone;

            if (!_initialized) Initialize();
        }

        public EffectBase(ushort id, int targetId, int duration, int delay, int random, int @group, int modificator, bool trigger, bool hidden, byte zoneSize, uint zoneShape, byte zoneMinSize, string rawZone)
        {
            Id = id;
            _template = ObjectDataManager.Instance.Get<Effect>(id);
            Targets = (SpellTargetType) targetId;
            Delay = delay;
            Duration = duration;
            Group = group;
            Random = random;
            Modificator = modificator;
            Trigger = trigger;
            Hidden = hidden;
            ZoneSize = zoneSize;
            ZoneMinSize = zoneMinSize;
            ZoneMinSize = zoneMinSize;
            ZoneSize = zoneSize;
            ZoneShape = (SpellShapeEnum) zoneShape;
            _rawZone = rawZone;

            if (!_initialized) Initialize();
        }

        public EffectBase(ObjectEffect effect)
        {
            Id = effect.ActionId;
            try
            {
                _template = ObjectDataManager.Instance.Get<Effect>(Id, true);
                if (_template == null) _template = new Effect();
            }
            catch (Exception ex)
            {
                Logger.Debug($"Can't find effect Id {Id} : {ex.Message}");
                _template = new Effect();
            }
        }

        public EffectBase(EffectInstance effect)
        {
            Id = (ushort) effect.EffectId;
            _template = ObjectDataManager.Instance.Get<Effect>(effect.EffectId);
            Targets = (SpellTargetType) effect.TargetId;
            Delay = effect.Delay;
            Duration = effect.Duration;
            Group = effect.Group;
            Random = effect.Random;
            Modificator = effect.Modificator;
            Trigger = effect.Trigger;
            Hidden = effect.VisibleInFightLog; // CHANGE : Hidden effect
            ZoneShape = (SpellShapeEnum) effect.ZoneShape;
            _rawZone = effect.RawZone;

            if (!_initialized) Initialize();
        }

        public int Delay { get; protected set; }

        public string Description
        {
            get
            {
                if (_description != null)
                    return _description;

                var pattern = I18NDataManager.Instance.ReadText(Template.DescriptionId);

                var decoder = new StringPatternDecoder(pattern, GetValues());

                int? index;
                if ((index = decoder.CheckValidity(false)) != null)
                    return $"Error in pattern '{pattern}' at index {index}";

                return _description = decoder.Decode();
            }
        }

        public int Duration { get; protected set; }

        public int Group { get; protected set; }

        public bool Hidden { get; protected set; }

        public ushort Id { get; protected set; }

        public int Modificator { get; protected set; }

        public string Operator
        {
            get { return Template.Operator; }
        }

        public uint Priority
        {
            get { return Template.EffectPriority; }
        }

        public virtual int ProtocoleId => 76;

        public int Random { get; protected set; }

        public uint Surface => new Zone(ZoneShape, ZoneSize).Surface;

        public SpellTargetType Targets { get; protected set; }

        public Effect Template
        {
            get { return _template; }
            protected set { _template = value; }
        }

        public bool Trigger { get; protected set; }

        public int ZoneMinSize { get;
            protected set;
        }

        public SpellShapeEnum ZoneShape { get; protected set; }

        public int ZoneSize
        {
            get; protected set; }

        #region ICloneable Members

        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion

        public string AreaDesc()
        {
            var zone = new Zone(ZoneShape, ZoneSize);
            zone.MinRadius = ZoneMinSize;
            return $"{zone.Shape.GetType().Name} ({zone.Surface} cells)";
        }

      //  private function parseZone() : void
      //{
      //   var _loc1_:Array = null;
      //   var _loc2_:* = false;
      //   if(this.rawZone && this.rawZone.length)
      //   {
      //      this.zoneShape = this.rawZone.charCodeAt(0);
      //      _loc1_ = this.rawZone.substr(1).split(",");
      //      switch(this.zoneShape)
      //      {
      //         case SpellShapeEnum.l:
      //            this.zoneMinSize = parseInt(_loc1_[0]);
      //            this.zoneSize = parseInt(_loc1_[1]);
      //            if(_loc1_.length > 2)
      //            {
      //               this.zoneEfficiencyPercent = parseInt(_loc1_[2]);
      //               this.zoneMaxEfficiency = parseInt(_loc1_[3]);
      //            }
      //            if(_loc1_.length == 5)
      //            {
      //               this.zoneStopAtTarget = parseInt(_loc1_[4]);
      //            }
      //            this._rawZoneInit = true;
      //            break;
      //      }
      //      if(this._rawZoneInit)
      //      {
      //         return;
      //      }
      //      _loc2_ = this.zoneShape == SpellShapeEnum.C || this.zoneShape == SpellShapeEnum.X || this.zoneShape == SpellShapeEnum.Q || this.zoneShape == SpellShapeEnum.plus || this.zoneShape == SpellShapeEnum.sharp;
      //      switch(_loc1_.length)
      //      {
      //         case 1:
      //            this.zoneSize = parseInt(_loc1_[0]);
      //            break;
      //         case 2:
      //            this.zoneSize = parseInt(_loc1_[0]);
      //            if(_loc2_)
      //            {
      //               this.zoneMinSize = parseInt(_loc1_[1]);
      //            }
      //            else
      //            {
      //               this.zoneEfficiencyPercent = parseInt(_loc1_[1]);
      //            }
      //            break;
      //         case 3:
      //            this.zoneSize = parseInt(_loc1_[0]);
      //            if(_loc2_)
      //            {
      //               this.zoneMinSize = parseInt(_loc1_[1]);
      //               this.zoneEfficiencyPercent = parseInt(_loc1_[2]);
      //            }
      //            else
      //            {
      //               this.zoneEfficiencyPercent = parseInt(_loc1_[1]);
      //               this.zoneMaxEfficiency = parseInt(_loc1_[2]);
      //            }
      //            break;
      //         case 4:
      //            this.zoneSize = parseInt(_loc1_[0]);
      //            this.zoneMinSize = parseInt(_loc1_[1]);
      //            this.zoneEfficiencyPercent = parseInt(_loc1_[2]);
      //            this.zoneMaxEfficiency = parseInt(_loc1_[3]);
      //            break;
      //      }
      //      this._rawZoneInit = true;
      //   }
      //   else
      //   {
      //      _log.error("Zone incorrect (" + this.rawZone + ")");
      //   }
      //}
        private void Initialize()
        {
            if (string.IsNullOrWhiteSpace(_rawZone)) return;

            var parameters = _rawZone.Substring(1).Split(',');
            var zoneShape = (SpellShapeEnum)_rawZone[0];
            if (ZoneShape != zoneShape)
                Logger.Error($"Effect initializer : ZoneShapes are different : object ({ZoneShape}) - raw ({zoneShape})");

            var hasMinSize = zoneShape == SpellShapeEnum.C || zoneShape == SpellShapeEnum.X ||
                             zoneShape == SpellShapeEnum.Q || zoneShape == SpellShapeEnum.Plus ||
                             zoneShape == SpellShapeEnum.Sharp || zoneShape == SpellShapeEnum.L;
            if (parameters[0] == string.Empty) parameters[0] = "1";

            ZoneSize = int.Parse(parameters[0]);
            if (hasMinSize && parameters.Length >= 2)
                ZoneMinSize = int.Parse(parameters[1]);
            _initialized = true;
        }

        public static EffectBase CreateInstance(ObjectEffect effect)
        {
            if (effect is ObjectEffectLadder)
                return new EffectLadder(effect as ObjectEffectLadder);
            if (effect is ObjectEffectCreature)
                return new EffectCreature(effect as ObjectEffectCreature);
            if (effect is ObjectEffectDate)
                return new EffectDate(effect as ObjectEffectDate);
            if (effect is ObjectEffectDice)
                return new EffectDice(effect as ObjectEffectDice);
            if (effect is ObjectEffectDuration)
                return new EffectDuration(effect as ObjectEffectDuration);
            if (effect is ObjectEffectMinMax)
                return new EffectMinMax(effect as ObjectEffectMinMax);
            if (effect is ObjectEffectMount)
                return new EffectMount(effect as ObjectEffectMount);
            if (effect is ObjectEffectString)
                return new EffectString(effect as ObjectEffectString);
            if (effect is ObjectEffectInteger)
                return new EffectInteger(effect as ObjectEffectInteger);

            return new EffectBase(effect);
        }

        public static EffectBase CreateInstance(EffectInstance effect)
        {
            if (effect is EffectInstanceLadder)
                return new EffectLadder(effect as EffectInstanceLadder);
            if (effect is EffectInstanceCreature)
                return new EffectCreature(effect as EffectInstanceCreature);
            if (effect is EffectInstanceDate)
                return new EffectDate(effect as EffectInstanceDate);
            if (effect is EffectInstanceDice)
                return new EffectDice(effect as EffectInstanceDice);
            if (effect is EffectInstanceDuration)
                return new EffectDuration(effect as EffectInstanceDuration);
            if (effect is EffectInstanceMinMax)
                return new EffectMinMax(effect as EffectInstanceMinMax);
            if (effect is EffectInstanceMount)
                return new EffectMount(effect as EffectInstanceMount);
            if (effect is EffectInstanceString)
                return new EffectString(effect as EffectInstanceString);
            if (effect is EffectInstanceInteger)
                return new EffectInteger(effect as EffectInstanceInteger);

            return new EffectBase(effect);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(EffectBase)) return false;
            return Equals((EffectBase) obj);
        }

        public bool Equals(EffectBase other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == Id;
        }

        public IEnumerable<Cell> GetArea(Cell origin, Cell dest)
        {
            var direction = origin.OrientationTo(dest, true);
            var zone = new Zone(ZoneShape, ZoneSize, direction);
            zone.MinRadius = ZoneMinSize;
            return zone.GetCells(dest, dest.Map);
        }

        public virtual EffectInstance GetEffectInstance()
        {
            return new EffectInstance
            {
                EffectId = (uint) Id,
                TargetId = (int) Targets,
                Delay = Delay,
                Duration = Duration,
                Group = Group,
                Random = Random,
                Modificator = Modificator,
                Trigger = Trigger,
                VisibleInFightLog = Hidden,
                ZoneMinSize = ZoneMinSize,
                ZoneSize = ZoneSize,
                ZoneShape = (uint) ZoneShape
            };
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public virtual ObjectEffect GetObjectEffect()
        {
            return new ObjectEffect(Id);
        }

        public virtual object[] GetValues()
        {
            return new object[0];
        }

        public static bool operator ==(EffectBase left, EffectBase right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EffectBase left, EffectBase right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return $"{(EffectsEnum) Id} Targets: {Targets}, Area:{AreaDesc()} - {Description}";
        }

        protected void OnDescriptionChanged()
        {
            _description = null;
        }
    }
}