﻿// <copyright file="PlayedFighter.AI.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Stats;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
using Strawberry.Core.Config;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Messages;
using Spell = Strawberry.Common.Game.Spells.Spell;

namespace Strawberry.Common.Game.Actors.Fighters
{
    public partial class PlayedFighter
    {
        [Configurable("DefaultRecordOnTheFly", "If true, the sniffer will record on the fly by default at start.")] public static bool DefaultRecordOnTheFly = true;

        public IEnumerable<Cell> AvailablePlacementCells
        {
            get { return Team.PlacementCells.Except(FriendCells); }
        }

        public bool CanCastSpells { get; private set; }

        public bool CanFight { get; private set; }

        public IEnumerable<Cell> EnemiesCells
        {
            get { return GetOpposedTeam().FightersAlive.Select(fighter => fighter.Cell); }
        }

        /// <summary>
        ///     All cells where friends stand, not including his own cell
        /// </summary>
        public IEnumerable<Cell> FriendCells
        {
            get { return Team.FightersAlive.Where(fighter => fighter.Id != Id).Select(fighter => fighter.Cell); }
        }

        public int SummonedCount
        {
            get
            {
                return Fight.Actors.Count(fighter => fighter.IsAlive && fighter.Summoned && fighter.Summoner == this);
            }
        }

        /// <summary>
        ///     Says if no state prevents from casting spells
        ///     Warning: a bit time consuming
        /// </summary>
        /// <returns></returns>
        public bool CanCastSpell(int spellId)
        {
            return !GetSpellImmunityEffects().Any(effect => effect.ImmuneSpellId == spellId);
        }

        public bool CanSummon()
        {
            return (Stats as PlayerStats).SummonLimit > SummonedCount;
        }

        public Spell FindMostEfficientAttackSpell()
        {
            Spell bestSpell = null;
            double betterDamage = 0;

            var spells = Character.SpellsBook.Spells.ToList();
            spells.Add(Character.SpellsBook.WeaponSpellLike());

            foreach (var spell in spells)
                if (spell.IsAvailable(null, Spell.SpellCategory.Damages))
                {
                    var dmg = spell.GetTotalDamageOnAllEnemies(this)/spell.LevelTemplate.ApCost;
                    if (dmg > betterDamage)
                    {
                        betterDamage = dmg;
                        bestSpell = spell;
                    }
                }
            if (bestSpell == null)
                return
                    Character.SpellsBook.Spells.Where(
                        spell =>
                            ((spell.Categories & Spell.SpellCategory.Damages) != 0) && spell.IsAvailable(null, null))
                             .OrderByDescending(spell => spell.Level)
                             .ThenByDescending(spell => spell.LevelTemplate.MinPlayerLevel)
                             .FirstOrDefault();
            return bestSpell;
        }

        /*public IEnumerable<Spells.Spell> GetZoneAttackSpells()
        {
            return Character.SpellsBook.GetZoneSpells(this, Spells.Spell.SpellCategory.Damages);
        }

        public IEnumerable<Spells.Spell> GetOrderListOfSimpleAttackSpells(Fighter target, bool NoRangeCheck = false)
        {
            return Character.SpellsBook.GetOrderedAttackSpells(this, target, Spells.Spell.SpellCategory.Damages).
                Where(spell => CanCastSpell(spell, target, NoRangeCheck) && !spell.LevelTemplate.needFreeCell);
        }

        public IEnumerable<Spells.Spell> GetOrderListOfSimpleBoostSpells()
        {
            return Character.SpellsBook.GetOrderListOfSimpleBoostSpells(this, this, Spell.SpellCategory.Buff).
                Where(spell => CanCastSpell(spell, this, true));
        }*/

        public IEnumerable<Spell> GetOrderListOfInvocationSpells()
        {
            IEnumerable<Spell> spells =
                Character.SpellsBook.Spells.Where(
                    spell =>
                        (Stats.CurrentAP >= spell.LevelTemplate.ApCost) &&
                        spell.IsAvailable(null, Spell.SpellCategory.Invocation) &&
                        CanCastSpell(spell, (Cell) null, true) && spell.LevelTemplate.NeedFreeCell)
                         .OrderByDescending(spell => spell.Level)
                         .ThenByDescending(spell => spell.LevelTemplate.MinPlayerLevel);
            //Character.SendDebug("Sorted invocs : {0}", String.Join(",", spells));
            return spells;
        }

        /// <summary>
        ///     Gives walkable cells where the character may walk within the turn.
        ///     Note : it's now supposed to be reliable, using PathFinder
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Cell> GetPossibleMoves(bool cautious, bool ForceIncludeStartingCell = true,
            PathFinder pathFinder = null)
        {
            if (pathFinder == null)
                pathFinder = new PathFinder(Fight, true);
            var cells = pathFinder.FindConnectedCells(
                Cell, true, cautious,
                cell => cell.DistanceSteps <= Stats.CurrentMP, null, Stats.CurrentMP).ToList();
            if (ForceIncludeStartingCell && !cells.Contains(Cell))
                cells.Add(Cell);
            return cells;
        }

        internal void Update(GameMapNoMovementMessage message)
        {
            if (IsMoving())
                NotifyStopMoving(true, true);
            NotifyAck(true);
        }

        internal void Update(GameActionFightNoSpellCastMessage message)
        {
            NotifyAck(true);
        }

        /// <summary>
        ///     Says if no state prevents from casting spells
        ///     Warning: a bit time consuming
        /// </summary>
        /// <returns></returns>
        private bool canCastSpells()
        {
            return !GetStates().Any(state => state.PreventsSpellCast);
        }

        /// <summary>
        ///     Says if no state prevents from casting spells
        ///     Warning: a bit time consuming
        /// </summary>
        /// <returns></returns>
        private bool canFight()
        {
            return !GetStates().Any(state => state.PreventsFight);
        }

        /// <summary>
        ///     Retrieves current states in effect on the fighter
        ///     Warning: a bit time consuming
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SpellState> GetStates()
        {
            return GetBoostStateEffects().Select(effect => ObjectDataManager.Instance.Get<SpellState>(effect.StateId));
        }

        #region Action acknowledgement

        public delegate void AckHandler(bool failed);

        public event AckHandler Acknowledge;

        internal void NotifyAck(bool failed)
        {
            if (Acknowledge != null) Acknowledge(failed);
        }

        internal void Update(GameActionAcknowledgementMessage message)
        {
            if (IsMoving())
                NotifyStopMoving(false, false);
            NotifyAck(!message.Valid);
        }

        #endregion Action acknowledgement
    }
}