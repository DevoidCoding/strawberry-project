﻿// <copyright file="PlayedFighter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using System.Drawing;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.Alignement;
using Strawberry.Common.Game.Breeds;
using Strawberry.Common.Game.Fights;
using Strawberry.Common.Game.Spells;
using Strawberry.Common.Game.Stats;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.Fighters
{
    public partial class PlayedFighter : CharacterFighter
    {
        public PlayedFighter(PlayedCharacter character, Fight fight)
            : base(fight)
        {
            Character = character;
            Map = character.Map;
            Cell = character.Cell;
            Direction = character.Direction;
            CanFight = true;
            CanCastSpells = true;
        }

        public override AlignmentInformations Alignment
        {
            get { return Character.Alignement; }
            protected set { }
        }

        public override Breed Breed
        {
            get { return Character.Breed; }
            protected set { }
        }

        public PlayedCharacter Character { get; }

        public override double Id
        {
            get { return Character.Id; }
            protected set { }
        }

        public override bool IsAlive
        {
            get { return Character.Stats.Health > 0; }
            set { }
        }

        public override int Level
        {
            get { return Character.Level; }
            protected set { }
        }

        public override EntityLook Look
        {
            get { return Character.Look; }
            protected set { }
        }

        public override string Name
        {
            get { return Character.Name; }
            protected set { }
        }

        public PlayerStats PCStats => Character.Stats;

        public override IMinimalStats Stats => Character.Stats;


        public override bool Summoned
        {
            get { return false; }
            protected set { }
        }

        public override Fighter Summoner
        {
            get { return null; }
            protected set { }
        }

        /// <summary>
        ///     Check if the player can cast a spell to the targeted fighter
        /// </summary>
        /// <param name="spell">Casted spell</param>
        /// <param name="fighter">Targeted fighter</param>
        /// <param name="noRangeCheck">
        ///     if true, then skip all checking related with caster position,
        ///     (preparatory stuff, before fight)
        /// </param>
        /// <returns>False if cannot cast the spell</returns>
        public bool CanCastSpell(Spell spell, Fighter fighter, bool noRangeCheck = false)
        {
            return CanCastSpell(spell, fighter.Cell, noRangeCheck);
        }

        /// <summary>
        ///     Check if the player can cast a spell to the targeted cell
        /// </summary>
        /// <param name="spell">Casted spell</param>
        /// <param name="cell">Targeted cell</param>
        /// <param name="noRangeCheck">
        ///     if true, then skip all checking related with caster position,
        ///     (preparatory stuff, before fight)
        /// </param>
        /// <returns>False if cannot cast the spell</returns>
        public bool CanCastSpell(Spell spell, Cell cell, bool noRangeCheck = false)
        {
            // todo spells modifications
            // todo states
            if (cell == null) noRangeCheck = true;

            if (!noRangeCheck && !IsPlaying())
                return false;

            if (spell.LevelTemplate.ApCost > Stats.CurrentAP)
                return false;

            if (!noRangeCheck && !IsInSpellRange(cell, spell.LevelTemplate))
                return false;

            // test the LoS
            if (!noRangeCheck && spell.LevelTemplate.CastTestLos && !Fight.CanBeSeen(Cell, cell, false))
                return false;

            return true;
        }

        /// <summary>
        ///     Try to cast a spell to a targeted cell
        /// </summary>
        /// <param name="spell">Spell to cast</param>
        /// <param name="cell">Targeted cell</param>
        /// <returns>False if cannot cast the spell</returns>
        public bool CastSpell(Spell spell, Cell cell)
        {
            if (!CanCastSpell(spell, cell))
                return false;

            Character.Bot.SendToServer(new GameActionFightCastRequestMessage((ushort) spell.Template.Id, cell.Id));

            return true;
        }

        /// <summary>
        ///     Try to change the cell during the placement phase
        /// </summary>
        /// <param name="cell">Targeted cell</param>
        /// <returns>Returns false if cannot change the placement cell</returns>
        public bool ChangePrePlacement(Cell cell)
        {
            if (Fight.Phase != FightPhase.Placement)
            {
                Logger.Warn($"Call ChangePrePlacement({cell}) but the fight is not in placement phase");
                return false;
            }

            if (Array.IndexOf(Team.PlacementCells, cell) == -1)
            {
                Logger.Error("Placement {0} isn't valid", cell);
                return false;
            }

            Character.Bot.SendToServer(new GameFightPlacementPositionRequestMessage((ushort) cell.Id));
            return true;
        }

        /// <summary>
        ///     Try to move to the targeted Cell (truncate the path if the player hasn't enough MP)
        /// </summary>
        /// <param name="cell">Targeted cell</param>
        /// <param name="pathFinder">The path finder.</param>
        /// <param name="minDistance">The minimum distance.</param>
        /// <param name="cautious">The cautious.</param>
        /// <returns>System.Boolean.</returns>
        public bool Move(Cell cell, IAdvancedPathFinder pathFinder = null, int minDistance = 0, bool cautious = true)
        {
            return Move(cell, Stats.CurrentMP, pathFinder, minDistance, cautious);
        }

        /// <summary>
        ///     Try to move to the targeted Cell (truncate the path if the player hasn't enough MP)
        /// </summary>
        /// <param name="cell">Targeted cell</param>
        /// <param name="mp">MP to use</param>
        /// <param name="pathFinder">The path finder.</param>
        /// <param name="minDistance">The minimum distance.</param>
        /// <param name="cautious">The cautious.</param>
        /// <returns>False if cannot move</returns>
        public bool Move(Cell cell, int mp, IAdvancedPathFinder pathFinder = null, int minDistance = 0,
            bool cautious = true)
        {
            if (!IsPlaying())
                return false;
            if (mp < 1)
            {
                Character.SendMessage($"Can't move with {mp} MP", Color.Red);
                return false;
            }

            if (pathFinder == null)
                pathFinder = new PathFinder(Fight, true);
            Path path = null;
            path = pathFinder.FindPath(Cell, cell, false, Stats.CurrentMP < mp ? Stats.CurrentMP : mp, minDistance, true);
            // Try in cautious way first
            if (path == null || path.IsEmpty())
            {
                path = pathFinder.FindPath(Cell, cell, false, Stats.CurrentMP < mp ? Stats.CurrentMP : mp, minDistance,
                    false); // If failed, then uncautious
                if (path != null && cautious)
                {
                    Character.SendWarning("Couldn't find a cautious path from {0} to {1}, so go the unsafe way", Cell,
                        cell);
                }
            }
            return Move(path);
        }

        public bool Move(Path path)
        {
            if (!IsPlaying())
                return false;

            if (Stats.CurrentMP < 1)
            {
                Character.SendMessage($"Can't move with {Stats.CurrentMP} MP", Color.Red);
                return false;
            }

            // DEBUG
            //Character.SendMessage(String.Format("Move {0} => {1} ({3} PM): {2}", Cell, cell, String.Join<Cell>(",", path.Cells), mp));
            //Character.ResetCellsHighlight();
            //Character.HighlightCells(path.Cells, Color.YellowGreen);
            if (path == null || path.IsEmpty())
            {
                Character.SendMessage("Empty path skipped", Color.Red);
                return false;
            }
            if (path.Start.Id != Cell.Id)
            {
                Character.SendMessage($"Path starts with {path.Start} instead of {Cell}", Color.Red);
                return false;
            }

            if (NotifyStartMoving(path))
                Character.Bot.SendToServer(new GameMapMovementRequestMessage(path.GetClientPathKeys(), Map.Id));

            return true;
        }

        // Do not call NotifyStopMoving(false), wait for confirmation message
        public override void OnTimedPathExpired()
        {
        }

        /// <summary>
        ///     Pass the turn
        /// </summary>
        /// <returns>False if cannot pass the turn</returns>
        public bool PassTurn()
        {
            if (IsPlaying())
            {
                Character.Bot.CallDelayed(200, () => Character.Bot.SendToServer(new GameFightTurnFinishMessage()));
                return true;
            }

            return false;
        }


        /// <summary>
        ///     Define the fighter team. Throws Exception if already defined.
        /// </summary>
        /// <param name="team">Fighter team</param>
        public void SetTeam(FightTeam team)
        {
            if (Team != null && Team != team)
                throw new Exception("Team already defined !");

            Team = team;
        }

        internal override void NotifyTurnStarted()
        {
            CanFight = canFight();
            CanCastSpells = canCastSpells();
            base.NotifyTurnStarted();
        }
    }
}