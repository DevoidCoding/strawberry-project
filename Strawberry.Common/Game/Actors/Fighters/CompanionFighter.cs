﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strawberry.Common.Game.Actors.Fighters
{
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Game.Actors.Interfaces;
    using Strawberry.Common.Game.Alignement;
    using Strawberry.Common.Game.Fights;
    using Strawberry.Protocol.Types;

    public class CompanionFighter : Fighter, IPlayer
    {
        public CompanionFighter(GameFightCompanionInformations msg, Fight fight) : base(msg, fight)
        {
            CompanionGenericId = msg.CompanionGenericId;
            MasterId = msg.MasterId;
        }

        public double MasterId { get; set; }

        public sbyte CompanionGenericId { get; set; }
    }
}
