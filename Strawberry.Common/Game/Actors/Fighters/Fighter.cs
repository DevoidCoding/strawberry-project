﻿// <copyright file="Fighter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actions;
using Strawberry.Common.Game.Actors.Interfaces;
using Strawberry.Common.Game.Fights;
using Strawberry.Common.Game.Stats;
using Strawberry.Common.Game.World;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;
using Spell = Strawberry.Common.Game.Spells.Spell;

namespace Strawberry.Common.Game.Actors.Fighters
{
    public abstract class Fighter : ContextActor, INamed, IComparable
    {
        public delegate void SpellCastHandler(Fighter fighter, SpellCast cast);

        public delegate void TurnHandler(Fighter fighter);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ObservableCollectionMT<Fighter> _summons = new ObservableCollectionMT<Fighter>();

        private bool _isReady;

        protected Fighter()
        {
            CastsHistory = new SpellCastHistory(this);
            Summons = new ReadOnlyObservableCollectionMT<Fighter>(_summons);
        }

        public Fighter(GameFightFighterInformations msg, Fight fight) : this()
        {
            Id = msg.ContextualId;
            Fight = fight;
            Look = msg.Look;
            Map = fight.Map;
            Update(msg.Disposition);
            Team = fight.GetTeam((FightTeamColor) msg.TeamId);
            IsAlive = msg.Alive;
            Stats = new MinimalStats(msg.Stats);
            Summoned = msg.Stats.Summoned;
            if (Summoned)
            {
                Summoner = Fight.GetActor(msg.Stats.Summoner);

                if (Summoner == null)
                    Logger.Error($"Summoner {msg.Stats.Summoner} of fighter {this} not found");
            }
        }

        public SpellCastHistory CastsHistory { get; protected set; }

        public override IMapContext Context
        {
            get { return Fight; }
            protected set { }
        }

        public Fight Fight { get; protected set; }

        public override double Id { get; protected set; }

        public override bool IsAlive { get; set; }

        public bool IsReady
        {
            get { return _isReady; }
            set
            {
                _isReady = value;
                var evnt = ReadyStateChanged;

                evnt?.Invoke(this, value);
            }
        }

        public virtual int Level { get; protected set; }

        public override Map Map
        {
            get { return Fight.Map; }
            protected set { }
        }

        public virtual IMinimalStats Stats { get; protected set; }

        public virtual bool Summoned { get; protected set; }

        public virtual Fighter Summoner { get; protected set; }

        public ReadOnlyObservableCollectionMT<Fighter> Summons { get; }

        public FightTeam Team { get; protected set; }

        #region IComparable

        public int CompareTo(object obj)
        {
            if (!(obj is WorldObject)) return 0;
            return Id.CompareTo((obj as WorldObject).Id);
        }

        #endregion

        public virtual string Name { get; protected set; }

        public bool AddSummon(Fighter fighter)
        {
            if (!fighter.Summoned || fighter.Summoner != this)
                return false;

            _summons.Add(fighter);
            return true;
        }

        /// <summary>
        ///     Returns true whenever the fighter is able to play (i.g not dead)
        /// </summary>
        /// <returns></returns>
        public virtual bool CanPlay()
        {
            return IsAlive;
        }

        public IEnumerable<FightTemporaryBoostEffect> GetAllBoostEffects(short? actionId = null)
        {
            // AbstractFightDispellableEffect + delta
            return
                GetAllEffects(actionId)
                    .OfType<FightTemporaryBoostEffect>();
        }

        public IEnumerable<AbstractFightDispellableEffect> GetAllEffects(short? actionId = null)
        {
            return
                Fight.Effects.Values.SelectMany(
                    effectList =>
                        effectList.Where(
                            effectT => effectT.Item1.TargetId == Id && (actionId == null || effectT.Item2 == actionId))
                                  .Select(effectT => effectT.Item1));
        }

        public IEnumerable<FightTemporaryBoostStateEffect> GetBoostStateEffects(short? actionId = null)
        {
            // FightTemporaryBoostEffect + stateId        
            return
                GetAllEffects(actionId)
                    .OfType<FightTemporaryBoostStateEffect>();
        }

        public IEnumerable<FightTemporaryBoostWeaponDamagesEffect> GetBoostWeaponDamagesEffects(short? actionId = null)
        {
            // FightTemporaryBoostEffect + weaponTypeId
            return
                GetAllEffects(actionId)
                    .OfType<FightTemporaryBoostWeaponDamagesEffect>();
        }

        public IEnumerable<FightTriggeredEffect> GetFightTriggeredEffects(short? actionId = null)
        {
            // AbstractFightDispellableEffect + arg1, arg2, arg3, delay
            return
                GetAllEffects(actionId)
                    .OfType<FightTriggeredEffect>();
        }

        public FightTeam GetOpposedTeam()
        {
            return Fight.GetTeam(Team.Id == FightTeamColor.Blue ? FightTeamColor.Red : FightTeamColor.Blue);
        }

        public int GetRealSpellRange(SpellLevel spell)
        {
            var range = (int) (spell.RangeCanBeBoosted ? Stats.Range + spell.Range : spell.Range);

            if (range < spell.MinRange)
                return (int) spell.MinRange;

            return range;
        }

        public IEnumerable<FightTemporarySpellBoostEffect> GetSpellBoostEffects(short? actionId = null)
        {
            // FightTemporaryBoostEffect + boostedSpellId
            return
                GetAllEffects(actionId)
                    .OfType<FightTemporarySpellBoostEffect>();
        }

        public IEnumerable<FightTemporarySpellImmunityEffect> GetSpellImmunityEffects(short? actionId = null)
        {
            // AbstractFightDispellableEffect + immuneSpellId
            return
                GetAllEffects(actionId)
                    .OfType<FightTemporarySpellImmunityEffect>();
        }

        /// <summary>
        ///     Says if a given effect is in effect
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool HasState(int state)
        {
            return GetBoostStateEffects().Any(eff => eff.StateId == state);
        }

        public bool IsImmune(Spell spell)
        {
            if (spell.Categories == Spell.SpellCategory.Healing && HasState(76)) return true;
            return GetSpellImmunityEffects().Any(effect => effect.ImmuneSpellId == spell.Template.Id);
        }

        public bool IsInSpellRange(Cell cell, SpellLevel spell)
        {
            var range = GetRealSpellRange(spell);
            var dist = Cell.ManhattanDistanceTo(cell);

            if (!(dist >= spell.MinRange && dist <= range)) return false;
            return IsInLineIfNeeded(cell, spell);
        }

        /// <summary>
        ///     Returns true whenever it's fighter's turn
        /// </summary>
        /// <returns></returns>
        public virtual bool IsPlaying()
        {
            return this == Fight.TimeLine.CurrentPlayer;
        }

        public event Action<Fighter, bool> ReadyStateChanged;

        public bool RemoveSummon(Fighter fighter)
        {
            return _summons.Remove(fighter);
        }

        public event TurnHandler SequenceEnded;
        public event SpellCastHandler SpellCasted;

        public override string ToString()
        {
            return $"{Name} (lv {Level}) at {Cell}";
        }

        public event TurnHandler TurnEnded;


        public event TurnHandler TurnStarted;

        public virtual void Update(GameContextActorInformations actorInformation)
        {
            var fighterInformations = actorInformation as GameFightFighterInformations;
            if (fighterInformations != null)
            {
                IsAlive = fighterInformations.Alive;
                Stats.Update(fighterInformations.Stats);
            }
            Id = actorInformation.ContextualId;
            Look = actorInformation.Look;
            Map = Fight.Map;

            Update(actorInformation.Disposition);
        }

        internal void NotifySequenceEnded()
        {
            SequenceEnded?.Invoke(this);
        }

        internal void NotifySpellCasted(SpellCast cast)
        {
            CastsHistory.AddSpellCast(cast);

            var handler = SpellCasted;
            handler?.Invoke(this, cast);
        }

        internal virtual void NotifyTurnEnded()
        {
            if (IsMoving())
                NotifyStopMoving(false);

            var handler = TurnEnded;
            handler?.Invoke(this);
        }

        internal virtual void NotifyTurnStarted()
        {
            TurnStarted?.Invoke(this);
        }

        internal void Update(GameActionFightPointsVariationMessage message)
        {
            switch ((ActionIdEnum) message.ActionId)
            {
                case ActionIdEnum.ActionCharacterActionPointsLostCaster:
                    //if (this is PlayedFighter)
                    //  (this as PlayedFighter).Character.SendMessage(String.Format("{3} => AP of {0} : {1} => {2}", Name, Stats.CurrentAP, Stats.CurrentAP + message.delta, (ActionIdEnum)(message.actionId)));
                    Stats.UpdateAP(message.Delta);
                    break;
                case ActionIdEnum.ActionCharacterActionPointsLost:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;
                case ActionIdEnum.ActionCharacterDeboostActionPoints:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;
                case ActionIdEnum.ActionCharacterActionPointsSteal:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;
                case ActionIdEnum.ActionCharacterBoostActionPoints:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;
                case ActionIdEnum.ActionCharacterActionPointsUse:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;
                case ActionIdEnum.ActionCharacterActionPointsWin:
                    goto case ActionIdEnum.ActionCharacterActionPointsLostCaster;

                case ActionIdEnum.ActionCharacterMovementPointsLost:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterMovementPointsSteal:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterMovementPointsWin:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterDeboostMovementPoints:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterBoostMovementPoints:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterMovementPointsUse:
                    goto case ActionIdEnum.ActionCharacterMovememtPointsLostCaster;
                case ActionIdEnum.ActionCharacterMovememtPointsLostCaster:
                    //if (this is PlayedFighter)
                    //  (this as PlayedFighter).Character.SendMessage(String.Format("{3} => MP of {0} : {1} => {2}", Name, Stats.CurrentMP, Stats.CurrentMP + message.delta, (ActionIdEnum)(message.actionId)));
                    Stats.UpdateMP(message.Delta);
                    break;
            }
        }

        internal void UpdateHP(GameActionFightLifePointsLostMessage message)
        {
            //if (this is PlayedFighter)
            //  Logger.Debug("HP of {0} : {1} => {2}", Name, Stats.Health, Stats.Health - message.loss);
            Stats.UpdateHP((int) -message.Loss);
        }

        internal void UpdateHP(GameActionFightLifePointsGainMessage message)
        {
            //if (this is PlayedFighter)
            //  Logger.Debug("HP of {0} : {1} => {2}", Name, Stats.Health, Stats.Health + message.delta);
            Stats.UpdateHP((int) message.Delta);
        }

        private bool IsInLineIfNeeded(Cell cell, SpellLevel spell)
        {
            if (!spell.CastInLine) return true;
            return Cell.X == cell.X || Cell.Y == cell.Y;
        }
    }
}