﻿// <copyright file="MonsterFighter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Game.Fights;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.Fighters
{
    public class MonsterFighter : Fighter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string m_name;

        public MonsterFighter(GameFightMonsterInformations msg, Fight fight) : base(msg, fight)
        {
            MonsterTemplate = ObjectDataManager.Instance.Get<Monster>(msg.CreatureGenericId);
            MonsterGrade = MonsterTemplate.Grades[msg.CreatureGrade - 1];
            Level = (int) MonsterGrade.Level;
        }

        public MonsterGrade MonsterGrade { get; protected set; }

        public Monster MonsterTemplate { get; protected set; }

        public override string Name
        {
            get
            {
                if (m_name == null && MonsterTemplate == null)
                    return "???";
                return m_name ?? (m_name = I18NDataManager.Instance.ReadText(MonsterTemplate.NameId));
            }
            protected set { }
        }

        public void Update(GameFightMonsterInformations msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            //Id = msg.contextualId;
            //Look = msg.look;
            //Update(msg.disposition);
            //IsAlive = msg.alive;
            MonsterTemplate = ObjectDataManager.Instance.Get<Monster>(msg.CreatureGenericId);
            MonsterGrade = MonsterTemplate.Grades[msg.CreatureGrade - 1];
        }

        public override void Update(GameContextActorInformations informations)
        {
            if (informations == null) throw new ArgumentNullException(nameof(informations));
            base.Update(informations);
            if (informations is GameFightMonsterInformations)
            {
                Update(informations as GameFightMonsterInformations);
            }
        }
    }
}