﻿// <copyright file="CharacterFighter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Actors.Interfaces;
using Strawberry.Common.Game.Alignement;
using Strawberry.Common.Game.Breeds;
using Strawberry.Common.Game.Fights;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.Fighters
{
    public class CharacterFighter : Fighter, IPlayer
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public CharacterFighter(Fight fight)
        {
            Fight = fight;
        }

        public CharacterFighter(GameFightCharacterInformations msg, Fight fight) : base(msg, fight)
        {
            Alignment = new AlignmentInformations(msg.AlignmentInfos);
            Breed = new Breed(ObjectDataManager.Instance.Get<Protocol.Data.Breed>(msg.Breed, true));
        }

        public virtual AlignmentInformations Alignment { get; protected set; }

        public virtual Breed Breed { get; protected set; }

        public override string ToString()
        {
            return $"{Name} (lvl {Level}) at {Cell}";
        }

        public override void Update(GameContextActorInformations informations)
        {
            if (informations == null) throw new ArgumentNullException(nameof(informations));
            base.Update(informations);
            if (informations is GameFightCharacterInformations)
            {
                Update(informations as GameFightCharacterInformations);
            }
            else
            {
                Logger.Error($"Cannot update a {GetType()} with a {informations.GetType()} instance");
            }
        }

        private void Update(GameFightCharacterInformations msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Alignment = new AlignmentInformations(msg.AlignmentInfos);
            Name = msg.Name;
            Level = msg.Level;
            if (Breed == null || Breed.Id != msg.Breed)
                Breed = new Breed(ObjectDataManager.Instance.Get<Protocol.Data.Breed>(msg.Breed, true));

            Stats.Update(msg.Stats);
        }
    }
}