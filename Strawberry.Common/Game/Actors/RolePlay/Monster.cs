﻿// <copyright file="Monster.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public class Monster
    {
        public Monster(MonsterInGroupInformations informations)
            : this(informations, informations.Look)
        {
        }

        public Monster(MonsterInGroupLightInformations informations, EntityLook look)
        {
            Look = look;
            MonsterTemplate = ObjectDataManager.Instance.Get<Protocol.Data.Monster>(informations.CreatureGenericId);
            Grade = MonsterTemplate.Grades[informations.Grade - 1];
            Race = ObjectDataManager.Instance.Get<MonsterRace>(MonsterTemplate.Race);
            SuperRace = ObjectDataManager.Instance.Get<MonsterSuperRace>(Race.SuperRaceId);
        }

        public MonsterGrade Grade { get; }

        public int Id
        {
            get { return MonsterTemplate.Id; }
        }

        public bool IsArchMonster
        {
            get { return Race.Id == 78 && SuperRace.Id == 20; }
        }

        public bool IsBoss
        {
            get { return MonsterTemplate.IsBoss; }
        }

        public EntityLook Look { get; private set; }

        public Protocol.Data.Monster MonsterTemplate { get; }

        public string Name
        {
            get { return I18NDataManager.Instance.ReadText(MonsterTemplate.NameId); }
        }

        public MonsterRace Race { get; }

        public string RaceName
        {
            get { return I18NDataManager.Instance.ReadText(Race.NameId); }
        }

        public MonsterSuperRace SuperRace { get; }

        public string SuperRaceName
        {
            get { return I18NDataManager.Instance.ReadText(SuperRace.NameId); }
        }
    }
}