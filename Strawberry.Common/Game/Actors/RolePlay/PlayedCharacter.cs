﻿namespace Strawberry.Common.Game.Actors.RolePlay
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Text.RegularExpressions;

    using NLog;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Game.Actors.Fighters;
    using Strawberry.Common.Game.Alignement;
    using Strawberry.Common.Game.Fights;
    using Strawberry.Common.Game.Guilds;
    using Strawberry.Common.Game.Interactives;
    using Strawberry.Common.Game.Items;
    using Strawberry.Common.Game.Shortcuts;
    using Strawberry.Common.Game.Spells;
    using Strawberry.Common.Game.Stats;
    using Strawberry.Common.Game.TreasureHunt;
    using Strawberry.Common.Game.World;
    using Strawberry.Common.Game.World.Pathfinding;
    using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
    using Strawberry.Common.Handlers.Context;
    using Strawberry.Core.Collections;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;
    using Strawberry.Protocol.Types;

    using Breed = Strawberry.Common.Game.Breeds.Breed;
    using Job = Strawberry.Common.Game.Jobs.Job;
    using Spell = Strawberry.Common.Game.Spells.Spell;

    public class PlayedCharacter : Character
    {
        #region Static Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly ObservableCollectionMT<Emoticon> m_emotes;

        private readonly ObservableCollectionMT<Job> m_jobs;

        private InteractiveSkill m_useAfterMove;

        #endregion

        #region Constructors and Destructors

        public PlayedCharacter(Bot bot, CharacterBaseInformations informations)
        {
            InformationLevel = MessageLevel.All; // MessageLevel.Warning | MessageLevel.Error;

            if (informations == null) throw new ArgumentNullException(nameof(informations));

            Bot = bot;

            Id = informations.Id;
            Level = informations.Level;
            Name = informations.Name;
            Breed = new Breed(ObjectDataManager.Instance.Get<Protocol.Data.Breed>(informations.Breed));
            Look = informations.EntityLook;
            Sex = informations.Sex;

            Inventory = new Inventory(this);
            Stats = new PlayerStats(this);
            SpellsBook = new SpellsBook(this);
            SpellShortcuts = new SpellShortcutBar(this);
            GeneralShortcuts = new GeneralShortcutBar(this);
            TreasureHunt = new TreasureHunt(this);

            m_jobs = new ObservableCollectionMT<Job>();
            Jobs = new ReadOnlyObservableCollectionMT<Job>(m_jobs);
            m_emotes = new ObservableCollectionMT<Emoticon>();
            Emotes = new ReadOnlyObservableCollectionMT<Emoticon>(m_emotes);

            WorldPathFinder = new World.Utils.WorldPathFinder(this);
        }

        #endregion

        #region Delegates

        public delegate void FightJoinedHandler(PlayedCharacter character, Fight fight);

        public delegate void FightLeftHandler(PlayedCharacter character, Fight fight);

        public delegate void MapJoinedHandler(PlayedCharacter character, Map map);

        #endregion

        #region Public Events

        public event FightJoinedHandler FightJoined;

        public event FightLeftHandler FightLeft;

        public event MapJoinedHandler MapJoined;

        #endregion

        #region Public Properties

        public Bot Bot { get; }

        public Breed Breed { get; }

        /// <summary>
        ///     Not recommanded to use this
        /// </summary>
        public GameContextEnum ContextType { get; private set; }

        public ReadOnlyObservableCollectionMT<Emoticon> Emotes { get; }
        public World.Utils.WorldPathFinder WorldPathFinder { get; }

        public Fight Fight => Fighter != null ? Fighter.Fight : null;

        public PlayedFighter Fighter { get; private set; }

        public GeneralShortcutBar GeneralShortcuts { get; }

        public Guild Guild { get; private set; }

        public Inventory Inventory { get; }

        public ReadOnlyObservableCollectionMT<Job> Jobs { get; }

        public int Level { get; }

        public int? NextMap
        {
            get;
            private set;
        }

        public int? PreviousMap { get; private set; }

        public byte RegenRate { get; set; }

        public DateTime? RegenStartTime { get; set; }

        /// <summary>
        ///     True = female, False = male
        /// </summary>
        public bool Sex { get; }

        public SpellsBook SpellsBook { get; }

        public SpellShortcutBar SpellShortcuts { get; }

        public PlayerStats Stats { get; }

        public TreasureHunt TreasureHunt { get; }

        #endregion

        #region Public Methods and Operators

        public bool CheckCriteria(string p)
        {
            var pattern = @"(P|C)(C|I|S|V|W|A|M)\&(g|l)t\;(\d+)";
            var matches = Regex.Matches(p, pattern);

            foreach (Match match in matches)
            {
                Debug.Assert(match.Captures.Count == 4);
                var greater = match.Captures[2].Value == "g";
                var Value = 0;

                if (!int.TryParse(match.Captures[3].Value, out Value))
                {
                    logger.Error("Weapon criteria : {0} is not an int", match.Captures[3].Value);
                    continue;
                }

                if (match.Captures[0].Value == "C")
                    switch (match.Captures[1].Value)
                    {
                        case "C":
                            if (!SubCheck(greater, Stats.Chance, Value)) return false;
                            break;
                        case "W":
                            if (!SubCheck(greater, Stats.Wisdom, Value)) return false;
                            break;
                        case "S":
                            if (!SubCheck(greater, Stats.Strength, Value)) return false;
                            break;
                        case "A":
                            if (!SubCheck(greater, Stats.Agility, Value)) return false;
                            break;
                        case "V":
                            if (!SubCheck(greater, Stats.Vitality, Value)) return false;
                            break;
                        case "M":
                            if (!SubCheck(greater, Stats.MP, Value)) return false;
                            break;
                        case "I":
                            if (!SubCheck(greater, Stats.Intelligence, Value)) return false;
                            break;
                    }
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="TargetId"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public IEnumerable<Spell> GetAvailableSpells(int? TargetId, Spell.SpellCategory category)
        {
            foreach (var spell in SpellsBook.GetAvailableSpells(TargetId, category))
                if (spell.LevelTemplate.ApCost <= Stats.CurrentAP)
                    yield return spell;
        }

        #region Jobs

        public Job GetJob(int id)
            => m_jobs.FirstOrDefault(x => x.JobTemplate.Id == id);

        #endregion

        public override void Tick(int dt)
        {
            base.Tick(dt);
            UpdateRegen();
        }

        public void Update(GameFightPlacementPossiblePositionsMessage msg)
        {
            if (msg == null) throw new ArgumentException("msg");

            if (Fighter.Team == null)
            {
                // it's a bit tricky ...
                Fighter.SetTeam(Fight.GetTeam((FightTeamColor)msg.TeamNumber));
                Fight.AddActor(Fighter);
            }

            Fight.Update(msg);
        }

        public void Update(LifePointsRegenEndMessage message)
            => Stats.Health = (int)message.LifePoints;

        /// <summary>
        ///     Call this periodically, to update Health according to current regen rate
        /// </summary>
        public void UpdateRegen()
        {
            if (IsFighting()) return; // No regen in fights ? Well... anyway it would be turn-based

            if (RegenRate > 0 && RegenStartTime != null)
            {
                var elapsedSeconds = (DateTime.Now - RegenStartTime).Value.TotalSeconds;
                if (elapsedSeconds < 3.0) return; // Avoids significative errors when UpdateRegen is called too often.
                var regainedLife = (int)Math.Floor(elapsedSeconds / (RegenRate / 10.0f));

                if (regainedLife <= 0)
                    return; // Avoids significative errors when UpdateRegen is called too often when regenRate is low.

                if (Stats.Health + regainedLife > Stats.MaxHealth)
                {
                    Stats.Health = (int)Stats.MaxHealth;
                    RegenRate = 0;
                    RegenStartTime = null;
                }
                else
                {
                    Stats.Health += regainedLife;
                    RegenStartTime = DateTime.Now;
                }
            }
        }

        #region Interactives

        public bool UseInteractiveObject(InteractiveSkill skill)
        {
            m_useAfterMove = null;

            if (!Map.Interactives.Contains(skill.Interactive) || !skill.IsEnabled())
                return false;

            if (skill.Interactive.Cell != null && !skill.Interactive.Cell.IsAdjacentTo(Cell))
            {
                var cell = skill.Interactive.Cell.GetAdjacentCells(x => Map.CanStopOnCell(x)).OrderBy(x => x.ManhattanDistanceTo(Cell)).FirstOrDefault();

                if (cell == null)
                    return false;

                if (Move(cell))
                {
                    m_useAfterMove = skill;
                    return true;
                }
            }
            else
            {
                Bot.SendToServer(new InteractiveUseRequestMessage((uint)skill.Interactive.Id, (uint)skill.Id));
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Methods

        internal void Update(GameMapNoMovementMessage message)
        {
            if (IsMoving())
                NotifyStopMoving(true, true);
        }

        internal void Update(SpellVariantActivationMessage message)
            => SpellsBook.Update(message);

        private void OnFightJoined(Fight fight)
            => FightJoined?.Invoke(this, fight);

        private void OnFightLeft(Fight fight)
            => FightLeft?.Invoke(this, fight);

        internal void OnMapJoined(Map map)
            => MapJoined?.Invoke(this, map);

        private bool SubCheck(bool greater, StatsRow stat, int limit)
        {
            if (greater && stat.Total < limit) return false;
            if (!greater && stat.Total > limit) return false;
            return true;
        }

        #endregion

        #region Movements

        public bool CanMove()
            => Map != null && !IsFighting();

        private Action _actionAfterMove;

        public void MoveIfNeededThenAction(short cellId, Action actionAfterMove, int delayBeforeMove = 0, int minDistance = 0, bool cautious = false, bool cancelMove = true)
        {
            if (cellId != Cell.Id && CanMove())
            {
                if (actionAfterMove != null)
                    _actionAfterMove = actionAfterMove;

                Bot.CallDelayed(delayBeforeMove, () => Move(Map.Cells[cellId], null, minDistance, cautious, cancelMove));
            }
            else
            {
                if (actionAfterMove != null)
                    Bot.CallDelayed(delayBeforeMove, actionAfterMove);
            }
        }

        public bool Move(ushort cellId, ISimplePathFinder pathFinder = null, int minDistance = 0, bool cautious = false, bool cancelMove = true)
            => CanMove() && Move(Map.Cells[cellId], pathFinder, minDistance, cautious, cancelMove);

        public bool Move(Path path, Cell dest, bool cancelMove = true)
        {
            if (IsMoving())
                if (cancelMove)
                {
                    CancelMove(false);
                }
                else
                {
                    if (_actionAfterMove != null) return false; // Can't remember this move
                    _actionAfterMove = () => Move(path, dest, cancelMove);
                    return true;
                }

            // DEBUG
            SendMessage($"Move {Cell} => {dest} : {string.Join<Cell>(",", path.Cells)}");
            ResetCellsHighlight();
            HighlightCells(path.Cells, Color.YellowGreen);

            if (path.IsEmpty())
            {
                SendMessage("Empty path skipped", Color.Gray);
                return false;
            }

            if (path.Start.Id != Cell.Id)
            {
                SendMessage($"Path start with {path.Start} instead of {Cell}", Color.Red);
                return false;
            }

            Bot.SendToServer(new GameMapMovementRequestMessage(path.GetClientPathKeys(), Map.Id));
            _actionAfterMove = () => Bot.SendToServer(new GameMapMovementConfirmMessage());
            return true;
        }

        public bool Move(Cell cell, ISimplePathFinder pathFinder = null, int minDistance = 0, bool cautious = false, bool cancelMove = true)
        {
            if (cell == null) throw new ArgumentNullException("cell");

            if (!CanMove())
                return false;

            if (pathFinder == null)
                pathFinder = new Pathfinder(Map, Map, false);

            Path path = null;

            if (pathFinder is IAdvancedPathFinder advancedPathFinder)
                path = advancedPathFinder.FindPath(Cell, cell, true, -1, minDistance, cautious);
            else
                path = pathFinder.FindPath(Cell, cell, true, -1);

            return Move(path, cell);
        }

        /// <summary>
        /// </summary>
        /// <param name="refused">true if path were rejected (not even started)</param>
        public void CancelMove(bool refused)
        {
            if (!IsMoving())
                return;

            NotifyStopMoving(true, refused);

            Bot.SendToServer(new GameMapMovementCancelMessage((ushort)Cell.Id));
        }

        public override void NotifyStopMoving(bool canceled, bool refused)
        {
            base.NotifyStopMoving(canceled, refused);

            if (m_useAfterMove != null)
            {
                if (!canceled)
                {
                    var skill = m_useAfterMove;
                    Bot.AddMessage(() => UseInteractiveObject(skill)); // call next tick
                }

                m_useAfterMove = null;
            }

            if (_actionAfterMove != null)
            {
                Bot.AddMessage(_actionAfterMove);
                _actionAfterMove = null;
            }

            if (NextMap != null)
            {
                if (!canceled && !refused)
                {
                    // NOTE : Can they detect if we use the worldpathfinding ?
                    var id = NextMap.Value;
                    PreviousMap = Map.Id;
                    Bot.AddMessage(() => Bot.SendToServer(new ChangeMapMessage(id, false)));
                }

                NextMap = null;
            }
        }

        private bool _changingMap;

        private int _srcLeaderMap;

        private int _dstLeaderMap;

        private ushort _pivotLeaderCell;

        private int _nbTryLeft;

        /// <summary>
        ///     Try to reach the same map as the leader.
        ///     Returns false if it fails (not on the proper map...)
        /// </summary>
        /// <param name="SrcMap"></param>
        /// <param name="destCell"></param>
        /// <param name="dstMap"></param>
        /// <param name="nbTryLeft"></param>
        /// <returns></returns>
        public bool ChangeMap(int SrcMap, short destCell, int dstMap, int nbTryLeft)
        {
            _srcLeaderMap = SrcMap;
            _dstLeaderMap = dstMap;
            _pivotLeaderCell = (ushort)destCell;
            _nbTryLeft = nbTryLeft;

            if (!_changingMap)
            {
                _changingMap = true;
                InternalChangeMap();
            }

            return _changingMap;
        }

        private void InternalChangeMap()
        {
            _changingMap = true;

            if (IsFighting())
            {
                SendWarning("InternalChangeMap : I'm fighting => stop it !");
                _changingMap = false;
                return;
            }

            if (_srcLeaderMap != Map.Id)
            {
                SendWarning("I'm not on the proper map to follow leader on the next map");
                _changingMap = false;
                return;
            }

            if (_pivotLeaderCell != Cell.Id)
            {
                var pathFinder = new Pathfinder(Map, Map, false);

                if (!Move(_pivotLeaderCell, pathFinder, 0, true))
                {
                    if (_nbTryLeft > 0)
                    {
                        SendWarning("InternalChangeMap : Can't join the leader yet, try later");
                        _nbTryLeft--;
                        Bot.CallDelayed(6000, InternalChangeMap);
                    }
                    else if (_nbTryLeft > -6)
                    {
                        var neighbour = Map.GetDirectionOfTransitionCell(Map.Cells[_pivotLeaderCell]);
                        var destCell = pathFinder.FindConnectedCells(Cell, false, true, cell => (cell.MapChangeData & Map.MapChangeDatas[neighbour]) != 0).GetRandom();

                        if (destCell == null)
                        {
                            SendWarning("InternalChangeMap  : Can't join the leader, no try left. Can't even find any alternate path to go {0}", neighbour);
                        }
                        else
                        {
                            SendWarning("InternalChangeMap : Can't join the leader, no try left. Trying alternative path to go {0} : cell {1}", neighbour, destCell);
                            _pivotLeaderCell = (ushort)destCell.Id;
                        }

                        _nbTryLeft--;
                        Bot.CallDelayed(2000, InternalChangeMap);
                    }
                    else
                    {
                        SendError("InternalChangeMap : Can't find any path to join the leader. Trying again later. ");
                        _changingMap = false;
                    }

                    return;
                }
                // TODO : Corner cell are double directions
                var neighbourAlt = Map.GetDirectionOfTransitionCell(Map.Cells[_pivotLeaderCell]);
                SendWarning("InternalChangeMap : Move from {0} to {1} succeeded. When move is complete, should go from map {2} to map {3}. ", Cell, Map.Cells[_pivotLeaderCell], Map.ToString(), _dstLeaderMap);
                _changingMap = false;
                NextMap = neighbourAlt != MapNeighbour.None ? GetNeighbourId(neighbourAlt) : _dstLeaderMap;
                return;
            }

            SendError("I'm already on the good Cell, just try to jump to the other map.");
            Bot.CallDelayed(400, () => Bot.AddMessage(() => Bot.SendToServer(new ChangeMapMessage(_dstLeaderMap, false))));
            PreviousMap = Map.Id;
            _changingMap = false;
        }

        public MapNeighbour ChangeMap(MapNeighbour neighbour = MapNeighbour.Any)
        {
            NextMap = null;
            var pathFinder = new Pathfinder(Map, Map, false);

            // Select a random cell in the set of all reachable cells that allow map change in the right direction. 
            var destCell = pathFinder.FindConnectedCells(Cell, false, true, cell => cell.MapChangeData == Map.MapChangeDatas[neighbour]).GetRandom();
            if (destCell == null) return MapNeighbour.None;

            // neighbour = Map.GetDirectionOfTransitionCell(destCell);
            if (neighbour == MapNeighbour.None)
            {
                SendMessage($"Can't find any linked map from {this}", Color.Red);
                return MapNeighbour.None;
            }

            if (destCell.Id != Cell.Id)
            {
                if (Move(destCell, pathFinder, 0, true))
                {
                    NextMap = GetNeighbourId(neighbour);
                    SendMessage(string.Format("Moving at the {2} of map {0} to map {1}", this, NextMap, neighbour), Color.Pink);
                    return neighbour;
                }

                return MapNeighbour.None;
            }

            Bot.AddMessage(() => Bot.SendToServer(new ChangeMapMessage(GetNeighbourId(neighbour), false)));
            PreviousMap = Map.Id;
            SendMessage(string.Format("Moving at the {2} of map {0} to map {1} (direct)", this, NextMap, neighbour), Color.Pink);
            return neighbour;
        }

        public bool ChangeMap(MapNeighbour neighbour, Predicate<Cell> cellSelector)
        {
            try
            {
                NextMap = null;
                var pathFinder = new Pathfinder(Map, Map, false);

                // Select a random cell in the set of all reachable cells that allow map change in the right direction. 
                var destCell = pathFinder.FindConnectedCells(Cell, false, true, cell => (cell.MapChangeData & Map.MapChangeDatas[neighbour]) != 0 && cellSelector(cell)).GetRandom();
                if (destCell == null) return false;
                neighbour = Map.GetDirectionOfTransitionCell(destCell);
                if (neighbour == MapNeighbour.None) return false;

                if (Move(destCell, pathFinder, 0, true))
                {
                    NextMap = GetNeighbourId(neighbour);
                    PreviousMap = Map.Id;
                    return true;
                }

                return false;
            }
            finally
            {
                if (NextMap == null)
                    SendMessage($"Can't find any linked map from {this}", Color.Red);
                else
                    SendMessage(string.Format("Moving at the {2} of map {0} to map {1}", this, NextMap, neighbour), Color.Pink);
            }
        }

        // Do not call NotifyStopMoving(false), wait for confirmation message
        public override void OnTimedPathExpired()
            => NotifyStopMoving(false, false);

        public bool ChangeMap(MapNeighbour neighbour, ISimplePathFinder pathfinder = null)
        {
            var mapChangeData = Map.MapChangeDatas[neighbour];
            Path path = null;

            if (pathfinder != null && pathfinder is IAdvancedPathFinder)
            {
                path = (pathfinder as IAdvancedPathFinder).FindPath(Cell, Map.Cells.Where(cell => cell != Cell && (cell.MapChangeData & mapChangeData) != 0 && Map.IsCellWalkable(cell)), true);
            }
            else
            {
                var cells = Map.Cells.Where(x => x != Cell && (x.MapChangeData & mapChangeData) != 0 && Map.IsCellWalkable(x)).OrderBy(x => x.DistanceTo(Cell));

                if (pathfinder == null)
                    pathfinder = new Pathfinder(Map, Map, true, true);

                foreach (var cell in cells)
                    if ((path = pathfinder.FindPath(Cell, cell, true)) != null && !path.IsEmpty())
                        break;
            }

            if (path != null && !path.IsEmpty())
            {
                int? neighbourId = null;

                if (neighbour == null)
                    foreach (MapNeighbour neighbourFound in Enum.GetValues(typeof(MapNeighbour)))
                        if ((Map.MapChangeDatas[neighbourFound] & path.End.MapChangeData) > 0)
                            neighbour = neighbourFound;

                neighbourId = GetNeighbourId(neighbour);

                if (Move(path, path.End))
                {
                    NextMap = neighbourId;
                    return true;
                }

                return false;
            }

            return false;
        }

        private int GetNeighbourId(MapNeighbour neighbour)
        {
            switch (neighbour)
            {
                case MapNeighbour.Top:
                    return Map.TopNeighbourId;
                case MapNeighbour.Bottom:
                    return Map.BottomNeighbourId;
                case MapNeighbour.Right:
                    return Map.RightNeighbourId;
                case MapNeighbour.Left:
                    return Map.LeftNeighbourId;
                default:
                    return -1;
            }
        }

        public int GetMapLinkedToCell(short cellId)
        {
            var cell = Map.Cells[cellId];
            if (cell == null) return -1;
            return GetMapLinkedToCell(cell);
        }

        public int GetMapLinkedToCell(Cell cell)
        {
            var neighbour = Map.GetDirectionOfTransitionCell(cell);
            return GetNeighbourId(neighbour);
        }

        #endregion

        #region Chat

        public void Say(string message)
            => Say(message, ChatActivableChannelsEnum.ChannelGlobal);

        public void Say(string message, ChatActivableChannelsEnum channel)
            => Bot.SendToServer(new ChatClientMultiMessage(message, (sbyte)channel));

        public void SayTo(string message, string receiverName)
            => Bot.SendToServer(new ChatClientPrivateMessage(message, receiverName));

        public void SendTextInformation(TextInformationTypeEnum type, ushort id, params object[] parameters)
            => Bot.SendToClient(new TextInformationMessage((sbyte)type, id, parameters.Select(entry => entry.ToString()).ToArray()));

        #region Messages

        public MessageLevel InformationLevel { get; set; }

        [Flags]
        public enum MessageLevel : byte
        {
            Information = 1,

            Debug = 2,

            Warning = 4,

            Error = 8,

            All = 15,

            None = 0
        }

        public void SendInformation(string message, params object[] pars)
        {
            if ((InformationLevel & MessageLevel.Information) > 0) SendMessage(string.Format(message, pars), Color.Green);
        }

        public void SendDebug(string message, params object[] pars)
        {
            if ((InformationLevel & MessageLevel.Debug) > 0) SendMessage(string.Format(message, pars), Color.Gray);
        }

        public void SendWarning(string message, params object[] pars)
        {
            if ((InformationLevel & MessageLevel.Warning) > 0) SendMessage(string.Format(message, pars), Color.Orange);
        }

        public void SendError(string message, params object[] pars)
        {
            if ((InformationLevel & MessageLevel.Error) > 0) SendMessage(string.Format(message, pars), Color.Red);
        }

        /// <summary>
        ///     Send a message to the client's chat
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(string message)
            => SendTextInformation(TextInformationTypeEnum.TextInformationMessage, 0, message);

        /// <summary>
        ///     Send a message to the client's chat
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(string message, Color color)
            => SendMessage($"<font color=\"#{color.ToArgb().ToString("X")}\">{message}</font>");

        #endregion

        public void OpenPopup(string message)
            => OpenPopup(message, "Strawberry", 0);

        public void OpenPopup(string message, string sender)
            => Bot.SendToClient(new PopupWarningMessage(sender, message));

        public void OpenPopup(string message, string sender, byte lockDuration)
            => OpenPopup(message, sender);

        #endregion

        #region Stats

        public void SpendStatsPoints(BoostableStat stat, ushort amount)
        {
            if (amount > Stats.SpellsPoints)
                amount = (ushort)Stats.SpellsPoints;

            Bot.SendToServer(new StatsUpgradeRequestMessage(false, (sbyte)stat, amount));
        }

        public ushort GetPointsForBoostAmount(BoostableStat stat, short amount)
        {
            var currentPoints = GetBoostableStatPoints(stat);
            var threshold = Breed.GetThreshold(currentPoints, stat);
            var nextThreshold = Breed.GetNextThreshold(currentPoints, stat);
            short pointsToSpend = 0;
            var totalBoost = 0;

            while (totalBoost < amount)
            {
                short boost = 0;

                if (nextThreshold != null && currentPoints >= nextThreshold.PointsThreshold)
                {
                    threshold = Breed.GetThreshold(currentPoints, stat);
                    nextThreshold = Breed.GetNextThreshold(currentPoints, stat);
                }

                // must fill the current threshold first
                if (nextThreshold != null && amount > nextThreshold.PointsThreshold - currentPoints)
                {
                    boost = (short)(nextThreshold.PointsThreshold - pointsToSpend);
                    pointsToSpend += (short)(boost * threshold.PointsPerBoost);

                    boost = (short)(boost * threshold.BoostPerPoints);
                }
                else
                {
                    pointsToSpend += (short)(amount * threshold.PointsPerBoost);

                    boost = (short)(amount * threshold.BoostPerPoints);
                }

                amount -= boost;
                currentPoints += boost;
                totalBoost += boost;
            }

            return (ushort)pointsToSpend;
        }

        /// <summary>
        ///     Gives the amount of boost of a given stat with the given points
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="pointsToSpend"></param>
        /// <returns></returns>
        public int GetBoostAmountWithPoints(BoostableStat stat, int pointsToSpend)
        {
            var currentPoints = GetBoostableStatPoints(stat);
            var threshold = Breed.GetThreshold(currentPoints, stat);
            var nextThreshold = Breed.GetNextThreshold(currentPoints, stat);

            if (pointsToSpend < threshold.PointsPerBoost)
                return 0;

            var totalBoost = 0;

            while (pointsToSpend >= threshold.PointsPerBoost)
            {
                var boost = 0;

                if (nextThreshold != null && currentPoints >= nextThreshold.PointsThreshold)
                {
                    threshold = Breed.GetThreshold(currentPoints, stat);
                    nextThreshold = Breed.GetNextThreshold(currentPoints, stat);
                }

                if (pointsToSpend - threshold.PointsPerBoost < 0)
                    break;

                if (nextThreshold != null && pointsToSpend / (double)threshold.PointsPerBoost > nextThreshold.PointsThreshold - currentPoints)
                {
                    boost = (short)(threshold.PointsThreshold - pointsToSpend);
                    pointsToSpend -= (short)(boost * threshold.PointsPerBoost);

                    boost = (short)(boost * threshold.BoostPerPoints);
                }
                else
                {
                    boost = (short)Math.Floor(pointsToSpend / (double)threshold.PointsPerBoost);
                    pointsToSpend -= (short)(boost * threshold.PointsPerBoost);

                    boost = (short)(boost * threshold.BoostPerPoints);
                }

                currentPoints += boost;
                totalBoost += boost;
            }

            return totalBoost;
        }

        private int GetBoostableStatPoints(BoostableStat stat)
        {
            switch (stat)
            {
                case BoostableStat.Agility:
                    return Stats.Agility.Base;
                case BoostableStat.Strength:
                    return Stats.Strength.Base;
                case BoostableStat.Intelligence:
                    return Stats.Intelligence.Base;
                case BoostableStat.Chance:
                    return Stats.Chance.Base;
                case BoostableStat.Wisdom:
                    return Stats.Wisdom.Base;
                case BoostableStat.Vitality:
                    return Stats.Vitality.Base;
                default:
                    throw new ArgumentException("stat");
            }
        }

        #endregion

        #region Shortcuts

        #endregion

        #region Cells Highlighting

        public void HighlightCell(Cell cell, Color color)
            => Bot.SendToClient(new DebugHighlightCellsMessage(color.ToArgb(), new[] { (ushort)cell.Id }));

        public void HighlightCell(ushort cell, Color color)
            => Bot.SendToClient(new DebugHighlightCellsMessage(color.ToArgb(), new[] { cell }));

        public void HighlightCells(IEnumerable<Cell> cells, Color color)
            => Bot.SendToClient(new DebugHighlightCellsMessage(color.ToArgb(), cells.Select(entry => (ushort)entry.Id).ToArray()));

        public void HighlightCells(IEnumerable<ushort> cells, Color color)
            => Bot.SendToClient(new DebugHighlightCellsMessage(color.ToArgb(), cells.ToArray()));

        public void ResetCellsHighlight()
            => Bot.SendToClient(new DebugClearHighlightCellsMessage());

        #endregion

        #region Contexts

        public void EnterMap(Map map)
        {
            Map = map;
            Context = map;

            Bot.AddFrame(new RolePlayHandler(Bot));
        }

        // We don't really need to handle the contexts

        public bool IsInContext()
            => (int)ContextType != 0;

        public void ChangeContext(GameContextEnum context)
            => ContextType = context;

        public void LeaveContext()
        {
            var lastContext = ContextType;
            ContextType = 0;

            if (lastContext == GameContextEnum.Fight && IsFighting())
                LeaveFight();

            Bot.RemoveFrame<RolePlayHandler>();
            Bot.RemoveFrame<FightHandler>();
        }

        #endregion

        #region Fights

        public bool TryStartFightWith(GroupMonster monster, ISimplePathFinder pathFinder = null)
            => Move(monster.Cell, pathFinder);

        public void EnterFight(GameFightJoinMessage message)
        {
            if (IsFighting())
                throw new Exception("Player already fighting !");

            var fight = new Fight(message, Map);
            Fighter = new PlayedFighter(this, fight);

            Context = Fight;
            Bot.AddFrame(new FightHandler(Bot));
            OnFightJoined(Fight);
        }

        public void LeaveFight()
        {
            if (!IsFighting())
            {
                logger.Error("Cannot leave the fight : the character is not in fight");
                return;
            }

            if (Fight.Phase != FightPhase.Ended)
            {
                // todo : have to leave fight
            }

            Context = Map;
            Bot.RemoveFrame<FightHandler>();
            OnFightLeft(Fight);
            Fighter = null;
        }

        public bool IsFighting()
            => Fighter != null;

        #endregion

        #region Update Method

        public void Update(ShortcutBarContentMessage msg)
        {
            if ((ShortcutBarEnum)msg.BarType == ShortcutBarEnum.GeneralShortcutBar)
                GeneralShortcuts.Update(msg);
            else
                SpellShortcuts.Update(msg);
        }

        public void Update(EmoteListMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            m_emotes.Clear();
            foreach (var emoteId in msg.EmoteIds) m_emotes.Add(ObjectDataManager.Instance.Get<Emoticon>(emoteId));
        }

        public void Update(JobDescriptionMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            m_jobs.Clear();
            foreach (var job in msg.JobsDescription) m_jobs.Add(new Job(this, job));
        }

        internal void Update(JobLevelUpMessage message)
        {
            var job = GetJob(message.JobsDescription.JobId);

            if (job == null)
                logger.Warn($"Cannot update job {message.JobsDescription.JobId} level because it's not found");
            else
                job.SetJob(message.JobsDescription, message.NewLevel);
        }

        public void Update(SetCharacterRestrictionsMessage msg)
            => Restrictions = msg != null ? msg.Restrictions : throw new ArgumentNullException(nameof(msg));

        public void Update(CharacterStatsListMessage msg)
            => Stats.Update(msg != null ? msg.Stats : throw new ArgumentNullException(nameof(msg)));

        public void Update(SpellListMessage msg)
            => SpellsBook.Update(msg ?? throw new ArgumentNullException(nameof(msg)));

        public void Update(GameRolePlayCharacterInformations msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Update(msg.Disposition);
            Update(msg.HumanoidInfo);

            Name = msg.Name;
            Look = msg.Look;

            if (Alignement == null)
                Alignement = new AlignmentInformations(msg.AlignmentInfos);
            else
                Alignement.Update(msg.AlignmentInfos);
        }

        #endregion
    }
}