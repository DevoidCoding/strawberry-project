﻿// <copyright file="Merchant.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Common.Game.World;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public class Merchant : RolePlayActor
    {
        public Merchant(GameRolePlayMerchantInformations informations, Map map)
        {
            Id = informations.ContextualId;
            Look = informations.Look;
            Map = map;
            Update(informations.Disposition);
        }
    }
}