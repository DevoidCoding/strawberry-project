﻿// <copyright file="Npc.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Common.Game.World;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public class Npc : RolePlayActor
    {
        public Npc(GameRolePlayNpcWithQuestInformations informations, Map map)
            : this((GameRolePlayNpcInformations) informations, map)
        {
        }

        public Npc(GameRolePlayNpcInformations informations, Map map)
        {
            Id = informations.ContextualId;
            Look = informations.Look;
            Map = map;
            Update(informations.Disposition);
        }

        public Npc(GameRolePlayTreasureHintInformations informations, Map map)
        {
            Id = informations.NpcId;
            Look = informations.Look;
            Map = map;
            Update(informations.Disposition);
        }
    }
}