﻿// <copyright file="Character.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using Strawberry.Common.Game.Actors.Interfaces;
using Strawberry.Common.Game.Alignement;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public class Character : Humanoid, IAlignedActor, IPlayer
    {
        public Character()
        {
        }

        public Character(GameRolePlayCharacterInformations characterInformations, Map map)
            : base(characterInformations.HumanoidInfo)
        {
            if (characterInformations == null) throw new ArgumentNullException(nameof(characterInformations));
            if (map == null) throw new ArgumentNullException(nameof(map));

            // do not care about this warnings, this ctor is never called by his inheriter
            Id = characterInformations.ContextualId;
            Look = characterInformations.Look;
            Map = map;
            Update(characterInformations.Disposition);
            Name = characterInformations.Name;
            Alignement = new AlignmentInformations(characterInformations.AlignmentInfos);
        }

        public AlignmentInformations Alignement { get; protected set; }

        public override string ToString()
        {
            return $"{Name} [{Cell}]";
        }
    }
}