﻿namespace Strawberry.Common.Game.Actors.RolePlay
{
    using Strawberry.Common.Game.World;
    using Strawberry.Protocol.Types;

    public class PortalActor : RolePlayActor
    {
        #region Constructors and Destructors

        public PortalActor(GameRolePlayPortalInformations informations, Map map)
        {
            Map = map;
            Id = informations.Portal.PortalId;
        }

        #endregion
    }
}