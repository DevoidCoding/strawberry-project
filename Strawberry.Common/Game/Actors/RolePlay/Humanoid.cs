﻿// <copyright file="Humanoid.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using MoreLinq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Movements;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Core.Collections;
using Strawberry.Core.Extensions;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;
using GuildInformations = Strawberry.Common.Game.Guilds.GuildInformations;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public abstract class Humanoid : NamedActor
    {
        protected Humanoid()
        {
        }

        protected Humanoid(HumanInformations human)
        {
            if (human == null) throw new ArgumentNullException(nameof(human));

            Update(human);
        }

        public Emoticon Emote { get; protected set; }

        public DateTime? EmoteStartTime { get; protected set; }

        public ObservableCollectionMT<IndexedEntityLook> FollowingCharactersLook { get; private set; } =
            new ObservableCollectionMT<IndexedEntityLook>();

        public GuildInformations GuildInformations { get; protected set; }

        public ActorRestrictionsInformations Restrictions { get; protected set; }

        public Title Title { get; protected set; }

        public string TitleParam { get; protected set; }

        public override void Dispose()
        {
            FollowingCharactersLook.Clear();
            FollowingCharactersLook = null;

            base.Dispose();
        }

        public override VelocityConfiguration GetAdaptedVelocity(Path path)
        {
            if (Restrictions.CantRun)
                return MovementBehavior.WalkingMovementBehavior;

            return path.MPCost <= 3
                ? MovementBehavior.WalkingMovementBehavior
                : MovementBehavior.RunningMovementBehavior;
        }

        public void Update(HumanInformations human)
        {
            human.Options.ForEach(HandleOption);
            Restrictions = human.Restrictions;
        }

        private void HandleOption(HumanOption option)
        {
            var optionType = option.GetType();
            if (optionType == typeof(HumanOptionEmote))
            {
                var emote = (HumanOptionEmote) option;
                Emote = emote.EmoteId > 0 ? ObjectDataManager.Instance.Get<Emoticon>(emote.EmoteId) : null;
                EmoteStartTime = emote.EmoteStartTime > 0 ? emote.EmoteStartTime.UnixTimestampToDateTime() : (DateTime?) null;
            }
            else if (optionType == typeof(HumanOptionFollowers))
            {
                FollowingCharactersLook =
                    new ObservableCollectionMT<IndexedEntityLook>(
                        ((HumanOptionFollowers) option).FollowingCharactersLook);
            }
            else if (optionType == typeof(HumanOptionGuild))
            {
                // todo : guild
            }
            else if (optionType == typeof(HumanOptionOrnament))
            {
                // todo
            }
            else if (optionType == typeof(HumanOptionTitle))
            {
                // todo
            }
            else
            {
                // TODO
                //throw new Exception($"Unattempt HumanOption type {option.GetType()}");
            }
        }
    }
}