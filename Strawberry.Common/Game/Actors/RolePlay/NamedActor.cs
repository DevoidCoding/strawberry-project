﻿// <copyright file="NamedActor.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Common.Game.Actors.Interfaces;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public abstract class NamedActor : RolePlayActor, INamed
    {
        public virtual string Name { get; protected set; }
    }
}