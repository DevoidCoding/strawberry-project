﻿// <copyright file="StatusEnum.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public enum StatusEnum
    {
        ALIVE = 0,
        DEAD = 1,
        GHOST = 2
    }
}