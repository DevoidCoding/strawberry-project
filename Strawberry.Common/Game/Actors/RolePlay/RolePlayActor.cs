﻿// <copyright file="RolePlayActor.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using NLog;
using Strawberry.Common.Game.Interactives;
using Strawberry.Common.Game.World;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public abstract class RolePlayActor : ContextActor
    {
        public delegate void InteractiveUsageEndedHandler(
            RolePlayActor actor, InteractiveObject interactive, InteractiveSkill skill);

        public delegate void UseInteractiveHandler(
            RolePlayActor actor, InteractiveObject interactive, InteractiveSkill skill, DateTime? usageEndTime);

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public override IMapContext Context { get; protected set; }

        public override double Id { get; protected set; }

        public override bool IsAlive { get; set; }

        public DateTime? UsageEndTime { get; private set; }

        public InteractiveObject UsingInteractive { get; private set; }

        public InteractiveSkill UsingSkill { get; private set; }

        public override void Dispose()
        {
            base.Dispose();
        }

        public bool IsUsingInteractive()
        {
            return UsingInteractive != null;
        }

        public virtual void NotifyInteractiveUseEnded()
        {
            var skill = UsingSkill;
            var interactive = UsingInteractive;
            UsageEndTime = null;
            UsingSkill = null;
            UsingInteractive = null;

            var handler = StopUsingInteractive;
            handler?.Invoke(this, interactive, skill);
        }

        public virtual void NotifyUseInteractive(InteractiveObject interactive, InteractiveSkill skill, int duration)
        {
            if (duration > 0)
            {
                UsingInteractive = interactive;
                UsingSkill = skill;
                UsageEndTime = DateTime.Now + TimeSpan.FromMilliseconds(duration);
            }

            var handler = StartUsingInteractive;
            handler?.Invoke(this, interactive, skill, UsageEndTime);
        }

        public event UseInteractiveHandler StartUsingInteractive;
        public event InteractiveUsageEndedHandler StopUsingInteractive;

        public override void Tick(int dt)
        {
            base.Tick(dt);
        }

        public override string ToString()
        {
            return $"RP#{Id} at {Cell}";
        }
    }
}