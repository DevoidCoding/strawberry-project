// <copyright file="GroupMonster.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.RolePlay
{
    public class GroupMonster : RolePlayActor
    {
        public GroupMonster(GameRolePlayGroupMonsterInformations informations, Map map)
        {
            Id = informations.ContextualId;
            Look = informations.Look;
            Map = map;
            Update(informations.Disposition);
            AgeBonus = informations.AgeBonusRate;
            LootShare = informations.LootShare;
            AlignmentSide = informations.AlignmentSide;
            KeyRingBonus = informations.KeyRingBonus;

            // Gets monsters infos.
            var monsters = new List<Monster>();
            // Main monster, his look correspond to the group monster look
            monsters.Add(Leader = new Monster(informations.StaticInfos.MainCreatureLightInfos, informations.Look));
            // Other monsters of the group.
            monsters.AddRange(informations.StaticInfos.Underlings.Select(entry => new Monster(entry)));
            Monsters = monsters.ToArray();
        }

        public int AgeBonus { get; set; }

        public sbyte AlignmentSide { get; set; }

        public bool KeyRingBonus { get; set; }

        public Monster Leader { get; private set; }

        public long Level
        {
            get { return Monsters.Sum(x => x.Grade.Level); }
        }

        public sbyte LootShare { get; set; }

        public Monster[] Monsters { get; }

        public override string ToString()
        {
            return $"RP#{Id} at {Cell}";
        }
    }
}