﻿// <copyright file="IPlayer.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors.Interfaces
{
    public interface IPlayer
    {
        double Id { get; }

        EntityLook Look { get; }

        string Name { get; }
    }
}