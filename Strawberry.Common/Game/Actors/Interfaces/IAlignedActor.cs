﻿// <copyright file="IAlignedActor.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using Strawberry.Common.Game.Alignement;

namespace Strawberry.Common.Game.Actors.Interfaces
{
    public interface IAlignedActor
    {
        AlignmentInformations Alignement { get; }
    }
}