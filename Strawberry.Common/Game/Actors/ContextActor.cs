﻿// <copyright file="ContextActor.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:35</date>

using System;
using NLog;
using Strawberry.Common.Game.Chat;
using Strawberry.Common.Game.Movements;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Actors
{
    using System.Threading;
    using System.Threading.Tasks;

    using Strawberry.Protocol.Messages;

    public abstract class ContextActor : WorldObject
    {
        public delegate void MoveStartHandler(ContextActor actor, MovementBehavior movement);

        public delegate void MoveStopHandler(ContextActor actor, MovementBehavior movement, bool canceled, bool refused);

        public delegate void SpeakHandler(ContextActor actor, BotChatMessage message);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public virtual IMapContext Context { get; protected set; }

        public abstract bool IsAlive { get; set; }

        public virtual EntityLook Look { get; protected set; }

        public MovementBehavior Movement { get; protected set; }

        public Path MovementPath => Movement.MovementPath;

        public DateTime? MovementStartTime => Movement.StartTime;

        public override void Dispose()
        {
            StartMoving = null;
            StopMoving = null;

            base.Dispose();
        }

        public virtual VelocityConfiguration GetAdaptedVelocity(Path path)
        {
            return MovementBehavior.WalkingMovementBehavior;
        }

        public bool IsMoving()
        {
            return Movement != null;
        }

        public virtual void NotifySpeak(BotChatMessage message)
        {
            // TODO : NotImplementedException
            var handler = Speak;
            if (handler != null) handler(this, message);
        }

        public virtual bool NotifyStartMoving(Path path)
        {
            if (path.IsEmpty())
            {
                Logger.Warn("Try to start moving with an empty path");
                return false;
            }

            Movement = new MovementBehavior(path, GetAdaptedVelocity(path));
            Movement.Start();


            return NotifyStartMoving(Movement);
        }

        public virtual bool NotifyStartMoving(MovementBehavior movement)
        {
            Movement = movement;

            if (Movement.StartCell != Cell)
            {
                Logger.Warn("Actor start cell incorrect for this moving path Position={0}, StartPath={1}, Path={2}",
                    Cell, Movement.StartCell, string.Join<Cell>(",", Movement.MovementPath.Cells));
                Cell = Movement.StartCell;
            }

            var handler = StartMoving;
            if (handler != null) handler(this, Movement);

            return true;
        }

        public virtual void NotifyStopMoving(bool canceled, bool refused = false)
        {
            if (Movement == null)
            {
                Logger.Warn("Try to stop moving while the entity is not actually moving");
                return;
            }
            if (refused)
            {
                Movement.Cancel();

                //var element = Movement.StartCell;
                Cell = Movement.StartCell;
            }
            else if (canceled)
            {
                Movement.Cancel();

                var element = Movement.TimedPath.GetCurrentElement();
                Cell = element.CurrentCell;
                Direction = element.Direction;
            }
            else
            {
                Cell = Movement.EndCell;
                Direction = Movement.EndDirection;
            }

            var movement = Movement;

            Movement = null;

            var handler = StopMoving;
            if (handler != null) handler(this, movement, canceled, refused);
        }

        public virtual void OnTimedPathExpired()
        {
            NotifyStopMoving(false);
        }

        /// <summary>
        /// </summary>
        /// <remarks>I consider that any actor can speak</remarks>
        public event SpeakHandler Speak;

        public event MoveStartHandler StartMoving;

        public event MoveStopHandler StopMoving;

        public override void Tick(int dt)
        {
            base.Tick(dt);

            if (Movement != null && Movement.IsEnded())
                OnTimedPathExpired();
        }

        public void UpdatePosition(int cell)
        {
            UpdatePosition(Map.Cells[cell]);
        }

        public virtual void UpdatePosition(Cell cell)
        {
            Cell = cell;
        }
    }
}