﻿namespace Strawberry.Common.Game.Jobs.Skills
{
    using Strawberry.Common.Game.Items;
    using Strawberry.Protocol.Types;

    public class SkillCollect : Skill
    {
        #region Fields

        private readonly int gatheredRessourceId;

        private ItemBase gatheredRessource;

        #endregion

        #region Constructors and Destructors

        public SkillCollect(SkillActionDescriptionCollect skill)
            : base(skill)
        {
            Max = skill.Max;
            Min = skill.Min;
            Time = skill.Time;

            gatheredRessourceId = SkillTemplate.GatheredRessourceItem;
        }

        #endregion

        #region Public Properties

        public ItemBase GatheredRessource => gatheredRessource ?? (gatheredRessource = new ItemBase(gatheredRessourceId));

        public ushort Max { get; set; }

        public ushort Min { get; set; }

        public byte Time { get; set; }

        #endregion
    }
}