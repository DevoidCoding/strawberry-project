﻿namespace Strawberry.Common.Game.Jobs.Skills
{
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Game.Items;
    using Strawberry.Protocol.Types;

    public class SkillCraft : Skill
    {
        #region Fields

        private readonly IEnumerable<int> craftableItemIds;

        private IEnumerable<ItemBase> craftableItems;

        #endregion

        #region Constructors and Destructors

        public SkillCraft(SkillActionDescriptionCraft skill)
            : base(skill)
        {
            Probability = skill.Probability;
            craftableItemIds = SkillTemplate.CraftableItemIds;
        }

        #endregion

        #region Public Properties

        public IEnumerable<ItemBase> CraftableItems => craftableItems ?? (craftableItems = craftableItemIds.Select(e => new ItemBase(e)));

        public sbyte Probability { get; set; }

        #endregion
    }
}