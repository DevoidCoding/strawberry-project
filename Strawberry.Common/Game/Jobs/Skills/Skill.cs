﻿namespace Strawberry.Common.Game.Jobs.Skills
{
    using System;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Types;

    public class Skill
    {
        #region Fields

        private readonly int interactiveId;

        private readonly uint nameId;

        private readonly int parentJobId;

        private Job job;

        private string name;

        #endregion

        #region Constructors and Destructors

        public Skill(Protocol.Data.Skill skill)
        {
            if (skill == null)
                throw new ArgumentNullException(nameof(skill));

            nameId = skill.NameId;
            interactiveId = skill.InteractiveId;
            parentJobId = skill.ParentJobId;

            SkillTemplate = skill;
        }

        public Skill(int id)
            : this(ObjectDataManager.Instance.Get<Protocol.Data.Skill>(id))
        {
        }

        public Skill(SkillActionDescription skill)
            : this(skill.SkillId)
        {
        }

        #endregion

        #region Public Properties

        public Interactive Interactive => ObjectDataManager.Instance.Get<Interactive>(interactiveId);

        public Job JobTemplate => job ?? (job = ObjectDataManager.Instance.Get<Job>(parentJobId));

        public string Name => name ?? (name = I18NDataManager.Instance.ReadText(nameId));

        public Protocol.Data.Skill SkillTemplate { get; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
            => Name;

        #endregion
    }
}