﻿// <copyright file="Job.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Jobs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.Jobs.Skills;
    using Strawberry.Core.Reflection;
    using Strawberry.Protocol.Types;

    public class Job
    {
        #region Constants

        public const int Fisher = 36;

        public const int Hunter = 41;

        #endregion

        #region Fields

        private IEnumerable<SkillCollect> _collectSkills;

        private IEnumerable<SkillCraft> _craftSkills;

        private string _name;

        #endregion

        #region Constructors and Destructors

        public Job(Job job)
        {
            Owner = job.Owner;
            Skills = job.Skills;
            JobTemplate = job.JobTemplate;
            _craftSkills = null;
            _collectSkills = null;
            Level = job.Level;
            Experience = job.Experience;
        }

        public Job(PlayedCharacter owner, JobDescription job)
        {
            Owner = owner;
            SetJob(job);
        }

        #endregion

        #region Public Properties

        public IEnumerable<SkillCollect> CollectSkills => _collectSkills ?? (_collectSkills = Skills.OfType<SkillCollect>().ToArray());

        public IEnumerable<SkillCraft> CraftSkills => _craftSkills ?? (_craftSkills = Skills.OfType<SkillCraft>().ToArray());

        public double Experience { get; private set; }

        public double ExperienceLevelFloor { get; private set; }

        public double ExperienceNextLevelFloor { get; private set; }

        public Protocol.Data.Job JobTemplate { get; private set; }

        public int Level { get; private set; }

        public string Name => _name ?? (_name = I18NDataManager.Instance.ReadText(JobTemplate.NameId));

        public PlayedCharacter Owner { get; }

        public IEnumerable<Skill> Skills { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void SetJob(JobDescription job, int? level = null)
        {
            JobTemplate = ObjectDataManager.Instance.Get<Protocol.Data.Job>(job.JobId);
            Skills = ProtocolConverter.Instance.Convert<Skill>(job.Skills);
            _craftSkills = null;
            _collectSkills = null;

            if (level != null)
                Level = level.Value;
        }

        public void Update(JobExperience experience)
        {
            if (experience == null) throw new ArgumentNullException(nameof(experience));
            Level = experience.JobLevel;
            Experience = experience.JobXP;
            ExperienceLevelFloor = experience.JobXpLevelFloor;
            ExperienceNextLevelFloor = experience.JobXpNextLevelFloor;
        }

        #endregion
    }
}