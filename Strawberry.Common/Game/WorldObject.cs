﻿// <copyright file="WorldObject.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;
using NLog;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.MapTraveling;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game
{
    public abstract class WorldObject : INotifyPropertyChanged, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private Cell _cell;

        public virtual Cell Cell
        {
            get { return _cell; }
            protected set
            {
                _cell = value;
                SubMap = Map.GetSubMap(value);
            }
        }

        public virtual DirectionsEnum Direction { get; protected set; }

        public abstract double Id { get; protected set; }

        public virtual Map Map { get; protected set; }

        public virtual SubMap SubMap { get; protected set; }

        public virtual void Dispose()
        {
            PropertyChanged = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetPos(int cellId)
        {
            if (Map == null)
                Logger.Error($"Cannot define position of {this} with new cellId because Map is null");
            else
            {
                if (Cell != null && Cell.Id == cellId) return;

                OnPropertyChanged("Cell");
                Cell = Map.Cells[cellId];
            }
        }


        public virtual void Tick(int dt)
        {
        }


        public void Update(EntityDispositionInformations informations)
        {
            Direction = (DirectionsEnum) informations.Direction;
            if (Map == null)
                Logger.Error($"Cannot define position of {this} with EntityDispositionInformations because Map is null");
            else if (Cell == null || Cell.Id != informations.CellId)
            {
                OnPropertyChanged("Cell");
                Cell = Map.Cells[informations.CellId];
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}