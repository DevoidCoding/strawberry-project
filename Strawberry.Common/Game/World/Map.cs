﻿// <copyright file="Map.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Documents;

    using NLog;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Common.Data.Maps;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.Interactives;
    using Strawberry.Common.Game.World.Data;
    using Strawberry.Common.Game.World.MapTraveling;
    using Strawberry.Core.Collections;
    using Strawberry.Core.Config;
    using Strawberry.Core.Reflection;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Messages;
    using Strawberry.Protocol.Tools.Dlm;
    using Strawberry.Protocol.Types;

    using Npc = Strawberry.Common.Game.Actors.RolePlay.Npc;
    using SubArea = Strawberry.Common.Game.World.Areas.SubArea;
    using Area = Strawberry.Common.Game.World.Areas.Area;

    public class Map : MapContext<RolePlayActor>, IMap
    {
        #region Static Fields

        public static readonly Dictionary<MapNeighbour, int> MapCellChangement = new Dictionary<MapNeighbour, int> { { MapNeighbour.Right, -13 }, { MapNeighbour.Left, 13 }, { MapNeighbour.Top, 532 }, { MapNeighbour.Bottom, -532 } };

        public static readonly Dictionary<MapNeighbour, int> MapChangeDatas =
            new Dictionary<MapNeighbour, int> { { MapNeighbour.None, 0 }, { MapNeighbour.Right, 1 }, { MapNeighbour.Bottom, 4 }, { MapNeighbour.Left, 16 }, { MapNeighbour.Top, 64 }, { MapNeighbour.Any, 1 | 2 | 4 | 8 } };

        [Configurable("MapDecryptionKey", "The decryption key used by default")]
        public static string GenericDecryptionKey = "649ae451ca33ec53bbcbcc33becf15f4";

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly CellList _cells;

        private readonly List<(uint, Cell, DlmGraphicalElement)> _elements = new List<(uint, Cell, DlmGraphicalElement)>();

        private readonly ObservableCollectionMT<InteractiveObject> _interactives;

        // not used
        /*public static readonly Dictionary<MapNeighbour, int[]> MapChangeDatas = new Dictionary<MapNeighbour, int[]>
        {
            {MapNeighbour.Right, new [] {1, 2, 128}},
            {MapNeighbour.Left, new [] {8, 16, 32}},
            {MapNeighbour.Top, new [] {32, 64, 128}},
            {MapNeighbour.Bottom, new [] {2, 4, 8}},
        };*/
        private readonly DlmMap _map;

        private readonly MapPosition _position;

        private string _name;

        private int? _x;

        private int? _y;

        #endregion

        #region Constructors and Destructors

        #region Private Constructors

        private Map(MapData map)
        {
            _position = ObjectDataManager.Instance.GetOrDefault<MapPosition>(map.Id);
            var cells = map.Cells.Select(entry => new Cell(this, entry));
            _cells = new CellList(cells.ToArray());
        }

        #endregion Private Constructors

        #endregion

        #region Public Properties

        public DlmFixture[] BackgroudFixtures => _map.BackgroudFixtures;

        public Color BackgroundColor => _map.BackgroundColor;

        public int BottomNeighbourId => _map.BottomNeighbourId;

        public List<int> BottomArrowCellIds => _map.BottomArrowCellIds;
        public List<int> TopArrowCellIds => _map.TopArrowCellIds;
        public List<int> LeftArrowCellIds => _map.LeftArrowCellIds;
        public List<int> RightArrowCellIds => _map.RightArrowCellIds;

        public int Capabilities => _position?.Capabilities ?? 0;

        public override CellList Cells => _cells;

        public bool Encrypted => _map.Encrypted;

        public byte EncryptionVersion => _map.EncryptionVersion;

        public DlmFixture[] ForegroundFixtures => _map.ForegroundFixtures;

        public Color GridColor => _map.GridColor;

        // ReSharper disable once InconsistentNaming
        public int GroundCRC => _map.GroundCRC;

        public bool HasProprityOnWorldmap => _position?.HasPriorityOnWorldmap ?? false;

        public int Id => (int)_map.Id;

        public ReadOnlyObservableCollectionMT<InteractiveObject> Interactives { get; }

        public DlmLayer[] Layers => _map.Layers;

        public int LeftNeighbourId => _map.LeftNeighbourId;

        public byte MapType => _map.MapType;

        public string Name => _position == null ? string.Empty : _name ?? (_name = I18NDataManager.Instance.ReadText(_position.NameId));

        public MapObstacle[] Obstacles { get; private set; }

        public bool Outdoor => _position != null && _position.Outdoor;

        public int PresetId => _map.PresetId;

        public uint RelativeId => _map.RelativeId;

        public int RightNeighbourId => _map.RightNeighbourId;

        public uint ShadowBonusOnEntities => _map.ShadowBonusOnEntities;

        public SubArea SubArea { get; private set; }

        public int SubAreaId => _map.SubAreaId;

        public SubMap[] SubMaps { get; private set; }

        public int TopNeighbourId => _map.TopNeighbourId;

        public bool UseLowPassFilter => _map.UseLowPassFilter;

        public bool UseReverb => _map.UseReverb;

        public override bool UsingNewMovementSystem => _map.UsingNewMovementSystem;

        public byte Version => _map.Version;

        public int WorldMap => _position?.WorldMap ?? Id & (0x3FFC0000 >> 18);

        public int X
        {
            get
            {
                if (_position != null)
                    return _position.PosX;

                if (_x != null)
                    return _x.Value;

                _x = (Id & 0x3FE00) >> 9; // 9 higher bits

                if ((_x & 0x100) == 0x100) // 9th bit is the sign. 1 means it's minus
                    _x = -(X & 0xFF); // just take the 8 first bits and take the opposite number

                return _x.Value;
            }
        }

        public int Y
        {
            get
            {
                if (_position != null)
                    return _position.PosY;

                if (_y != null)
                    return _y.Value;

                _y = Id & 0x01FF; // 9 lower bits

                if ((_y & 0x100) == 0x100) // 9th bit is the sign. 1 means it's minus
                    _y = -(X & 0xFF); // just take the 8 first bits and take the opposite number

                return _y.Value;
            }
        }

        public short ZoomOffsetX => _map.ZoomOffsetX;

        public short ZoomOffsetY => _map.ZoomOffsetY;

        public ushort ZoomScale => _map.ZoomScale;

        #endregion

        #region Explicit Interface Properties

        ICellList<ICell> IMap.Cells => _cells;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Create a Map instance only used to store datas (cells, properties ...)
        /// </summary>
        public static Map CreateDataMapInstance(MapData map)
            => new Map(map);

        public static MapNeighbour GetDirectionOfTransitionCell(Cell cell)
        {
            foreach (MapNeighbour neighbourFound in Enum.GetValues(typeof(MapNeighbour)))
                if (neighbourFound != MapNeighbour.Any && (MapChangeDatas[neighbourFound] & cell.MapChangeData) > 0)
                    return neighbourFound;

            return MapNeighbour.None;
        }

        public static bool operator ==(Map first, Map second)
            => Equals(first, second);

        public static bool operator !=(Map first, Map second)
            => !(first == second);

        public void AddActor(Bot bot, GameRolePlayActorInformations actor)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (actor.ContextualId == bot.Character.Id)
            {
                bot.Character.Update(actor as GameRolePlayCharacterInformations);
                AddActor(bot.Character);
            }
            else
            {
                AddActor(CreateRolePlayActor(actor));
            }
        }

        public void AddInteractive(InteractiveObject interactive)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            var element = _elements.FirstOrDefault(x => x.Item1 == interactive.Id);

            if (element.Equals(default((uint, Cell, DlmGraphicalElement))))
                return;

            if (!GeometricSearch.Instance.MapContainsElement(element.Item3, SearchAlgorithmTypeEnum.Origin))
                return;

            _interactives.Add(interactive);
            interactive.DefinePosition(element.Item2);
        }

        public bool CanStopOnCell(Cell cell)
            => IsCellWalkable(cell) && Interactives.All(x => x.Cell != cell);

        public RolePlayActor CreateRolePlayActor(GameRolePlayActorInformations actor)
        {
            if (actor == null) throw new ArgumentNullException(nameof(actor));

            return ProtocolConverter.Instance.Convert<RolePlayActor>(actor, this);
        }

        public override bool Equals(object obj)
            => ((Map)obj)?.Id == Id;

        public InteractiveObject[] GetFilteredInteractives(InteractiveAction interactiveCategory, InteractiveState? state = InteractiveState.None, bool? activeSkills = null, short[] skills = null)
            => _interactives.Where(
                interactive => interactive.Action == interactiveCategory && (state == null || interactive.State == state)
                                                                         && (skills == null || activeSkills != true && interactive.DisabledSkills.Select(skill => (short)skill.JobSkill.Id).Intersect(skills).Any()
                                                                                            || activeSkills != false && interactive.EnabledSkills.Select(skill => (short)skill.JobSkill.Id).Intersect(skills).Any())).ToArray();

        public InteractiveObject[] GetFilteredInteractives(int jobId, InteractiveState? state = null)
            => _interactives.Where(
                    interactive => interactive.DisabledSkills.Any(skill => skill.JobSkill.ParentJobId == jobId) || interactive.EnabledSkills.Any(skill => skill.JobSkill.ParentJobId == jobId) && (state == null || interactive.State == state))
                .ToArray();

        public override int GetHashCode()
            => Id.GetHashCode();

        public InteractiveObject GetInteractive(uint id)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            var interactives = _interactives.Where(entry => entry.Id == id).ToArray();

            if (interactives.Length > 1)
                Logger.Error($"{interactives.Length} interactives objects found with the same id {id} !");

            return interactives.FirstOrDefault();
        }

        public SubMap GetSubMap(Cell cell)
            => SubMaps.FirstOrDefault(x => x.Cells.Contains(cell));

        public bool IsNeighbour(int mapId)
            => RightNeighbourId == mapId || LeftNeighbourId == mapId || BottomNeighbourId == mapId || TopNeighbourId == mapId;

        public override void Tick(int dt)
        {
            foreach (var actor in Actors)
                actor.Tick(dt);
        }

        public override string ToString()
            => $"#{Id} [{X},{Y}] {Name}";

        public void Update(Bot bot, MapComplementaryInformationsDataMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            SubArea = new SubArea(message.SubAreaId); // TODO : Alliance {AlignmentSide = (AlignmentSideEnum) message.subareaAlignmentSide};

            ClearActors();

            foreach (var actor in message.Actors)
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (actor.ContextualId == bot.Character.Id)
                {
                    bot.Character.Update(actor as GameRolePlayCharacterInformations);
                    AddActor(bot.Character);
                }
                else
                {
                    AddActor(bot, actor);
                }

            _interactives.Clear();

            foreach (var element in message.InteractiveElements)
                AddInteractive(new InteractiveObject(this, element));

            foreach (var element in message.StatedElements)
                Update(element);

            Obstacles = message.Obstacles;
            bot.Character.OnMapJoined(bot.Character.Map);
        }

        public void Update(InteractiveElementUpdatedMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var element = GetInteractive((uint)message.InteractiveElement.ElementId);

            if (element != null)
                element.Update(message.InteractiveElement);
            else
                AddInteractive(new InteractiveObject(this, message.InteractiveElement));
        }

        public void Update(InteractiveMapUpdateMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            foreach (var element in message.InteractiveElements)
            {
                var interactive = GetInteractive((uint)element.ElementId);

                if (interactive == null)
                    AddInteractive(new InteractiveObject(this, element));
                else
                    interactive.Update(element);
            }
        }

        public void Update(StatedElement element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            var interactive = GetInteractive((uint)element.ElementId);

            if (interactive != null)
                interactive.Update(element);
            else
                Logger.Warn($"Cannot find Interactive {element.ElementId} associated to the StatedElement");
        }

        public void Update(StatedElementUpdatedMessage message)
            => Update(message.StatedElement);

        public void Update(StatedMapUpdateMessage message)
        {
            foreach (var interactive in _interactives)
                interactive.ResetState();

            foreach (var element in message.StatedElements)
                Update(element);
        }

        #endregion

        #region Methods

        private void InitializeElements()
        {
            foreach (var layer in _map.Layers)
            foreach (var cell in layer.Cells)
            foreach (var element in cell.Elements.OfType<DlmGraphicalElement>())
                _elements.Add((element.Identifier, Cells[cell.Id], element));
        }

        private void LoadSubMaps()
        {
            var builder = new SubMapBuilder();
            SubMaps = builder.GenerateSubMaps(this);
        }

        #endregion

        #region Public Constructors

        public Map(long id)
            : this(id, GenericDecryptionKey)
        {
        }

        // ReSharper disable once UnusedParameter.Local
        public Map(long id, string decryptionKey)
        {
            // decryption key not used ? oO
            _map = MapsManager.Instance.GetDlmMap(id, GenericDecryptionKey);
            _position = ObjectDataManager.Instance.GetOrDefault<MapPosition>((int)id);
            var cells = _map.Cells.Select(entry => new Cell(this, entry));
            _cells = new CellList(cells.ToArray());

            InitializeElements();
            LoadSubMaps();

            _interactives = new ObservableCollectionMT<InteractiveObject>();
            Interactives = new ReadOnlyObservableCollectionMT<InteractiveObject>(_interactives);
        }

        #endregion Public Constructors

        //private int? m_worldId;
    }
}