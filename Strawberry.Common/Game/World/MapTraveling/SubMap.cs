﻿// <copyright file="SubMap.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Strawberry.Common.Game.World.MapTraveling
{
    public class SubMap
    {
        private readonly bool m_isBound;
        private readonly List<SubMapNeighbour> m_neighbours;

        public SubMap(Map map, Cell[] cells, byte submapId)
        {
            Map = map;
            Cells = cells;
            SubMapId = submapId;
            m_isBound = false;
        }

        public SubMap(Map map, Cell[] cells, SubMapBinder binder)
        {
            Map = map;
            Cells = cells;
            SubMapId = binder.SubMapId;
            m_neighbours = binder.Neighbours;
        }


        public Cell[] Cells { get; private set; }

        public long GlobalId
        {
            get { return (long) Map.Id << 8 | SubMapId; }
        }

        public Map Map { get; }

        public byte SubMapId { get; }

        public int X
        {
            get { return Map.X; }
        }

        public int Y
        {
            get { return Map.Y; }
        }

        public ReadOnlyCollection<SubMapNeighbour> GetNeighbours()
        {
            return !m_isBound ? new List<SubMapNeighbour>().AsReadOnly() : m_neighbours.AsReadOnly();
        }

        public bool IsBound()
        {
            return m_isBound;
        }
    }
}