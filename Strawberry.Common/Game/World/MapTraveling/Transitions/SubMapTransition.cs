﻿// <copyright file="SubMapTransition.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.IO;
using Strawberry.Common.Game.Actors.RolePlay;

namespace Strawberry.Common.Game.World.MapTraveling.Transitions
{
    public abstract class SubMapTransition
    {
        #region Delegates

        public delegate void TransitionEndedHandler(
            SubMapTransition transition, SubMap from, SubMapBinder to, bool success);

        #endregion

        public abstract bool BeginTransition(SubMap from, SubMapBinder to, PlayedCharacter character);
        public abstract void Deserialize(BinaryReader reader);

        public virtual void OnTransitionEnded(SubMap from, SubMapBinder to, bool success)
        {
            TransitionEnded?.Invoke(this, @from, to, success);
        }

        public abstract void Serialize(BinaryWriter writer);

        public event TransitionEndedHandler TransitionEnded;
    }
}