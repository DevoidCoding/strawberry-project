﻿// <copyright file="TransitionAttribute.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;

namespace Strawberry.Common.Game.World.MapTraveling.Transitions
{
    public class TransitionAttribute : Attribute
    {
        public int Identifier { get; set; }
    }
}