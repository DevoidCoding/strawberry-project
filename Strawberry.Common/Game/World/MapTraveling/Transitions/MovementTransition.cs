﻿// <copyright file="MovementTransition.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.IO;
using NLog;
using Strawberry.Common.Game.Actors.RolePlay;

namespace Strawberry.Common.Game.World.MapTraveling.Transitions
{
    public class MovementTransition : SubMapTransition
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public MovementTransition(MapNeighbour neighbour, short[] cells)
        {
            MapNeighbour = neighbour;
            Cells = cells;
        }

        public short[] Cells { get; set; }

        public MapNeighbour MapNeighbour { get; set; }

        public override bool BeginTransition(SubMap from, SubMapBinder to, PlayedCharacter character)
        {
            if (
                !character.ChangeMap(MapNeighbour,
                    cell => Cells == null || Cells.Length == 0 || Array.IndexOf(Cells, cell.Id) != -1))
            {
                logger.Error(
                    $"Cannot proceed transition : cannot reach {MapNeighbour} map from {character.Map.Id} (submap:{@from.GlobalId} to {to.GlobalId})");

                return false;
            }

            return true;
        }

        public override void Deserialize(BinaryReader reader)
        {
            MapNeighbour = (MapNeighbour) reader.ReadInt32();
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write((int) MapNeighbour);
        }
    }
}