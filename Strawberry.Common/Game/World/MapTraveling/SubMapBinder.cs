﻿// <copyright file="SubMapBinder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;

namespace Strawberry.Common.Game.World.MapTraveling
{
    /// <summary>
    ///     Represent the binding between a submap and his neighbours submaps
    /// </summary>
    public class SubMapBinder
    {
        public SubMapBinder()
        {
            Neighbours = new List<SubMapNeighbour>();
        }

        public SubMapBinder(int mapId, byte subMapId, int x, int y, List<SubMapNeighbour> neighbours)
        {
            MapId = mapId;
            SubMapId = subMapId;
            X = x;
            Y = y;
            Neighbours = neighbours;
        }

        public long GlobalId
        {
            get { return (long) MapId << 8 | SubMapId; }
        }

        public int MapId { get; set; }

        /// <summary>
        ///     Reachable sub maps
        /// </summary>
        public List<SubMapNeighbour> Neighbours { get; protected set; }

        public byte SubMapId { get; set; }

        public int X { get; set; }

        public int Y { get; set; }
    }
}