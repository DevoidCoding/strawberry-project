﻿// <copyright file="MapTravel.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.World.MapTraveling.Transitions;

namespace Strawberry.Common.Game.World.MapTraveling
{
    public class MapTravel
    {
        #region Delegates

        public delegate void TransitionEndedHandler(MapTravel travel, SubMapTransition transition, bool success);

        #endregion

        public MapTravel(SubMapTransition[] transitions, SubMapBinder[] subMaps)
        {
            Transitions = transitions;
            SubMaps = subMaps;

            if (Transitions.Length != SubMaps.Length)
                throw new ArgumentException("Transitions.Length != SubMaps.Length");
        }

        public SubMapBinder CurrentSubMap
        {
            get { return Index < 0 || Index >= SubMaps.Length ? null : SubMaps[Index]; }
        }

        public SubMapTransition CurrentTransition
        {
            get { return Index < 0 || Index >= Transitions.Length ? null : Transitions[Index]; }
        }

        public int Index { get; set; }

        public SubMapBinder[] SubMaps { get; }

        public SubMapTransition[] Transitions { get; }

        public bool BeginTransition(PlayedCharacter character)
        {
            if (IsEnded())
                return true;

            CurrentTransition.TransitionEnded += CurrentTransitionOnTransitionEnded;
            return CurrentTransition.BeginTransition(character.SubMap, SubMaps[Index + 1], character);
        }

        public bool HasFailed()
        {
            return Index == -1;
        }

        public bool IsEnded()
        {
            return Index >= Transitions.Length - 1;
        }

        public event TransitionEndedHandler TransitionEnded;

        private void CurrentTransitionOnTransitionEnded(SubMapTransition transition, SubMap from, SubMapBinder to,
            bool success)
        {
            CurrentTransition.TransitionEnded -= CurrentTransitionOnTransitionEnded;

            if (!success)
                Index = -1;
            else
            {
                Index++;
            }

            OnTransitionEnded(transition, success);
        }

        private void OnTransitionEnded(SubMapTransition transition, bool success)
        {
            TransitionEnded?.Invoke(this, transition, success);
        }
    }
}