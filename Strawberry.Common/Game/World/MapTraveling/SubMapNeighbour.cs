﻿// <copyright file="SubMapNeighbour.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using Strawberry.Common.Game.World.MapTraveling.Transitions;

namespace Strawberry.Common.Game.World.MapTraveling
{
    public class SubMapNeighbour
    {
        public SubMapNeighbour()
        {
        }

        public SubMapNeighbour(long globalId, SubMapTransition transition)
        {
            GlobalId = globalId;
            Transition = transition;
        }

        public long GlobalId { get; set; }

        public SubMapTransition Transition { get; set; }
    }
}