﻿// <copyright file="AdjacentSubMap.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using Strawberry.Common.Game.World.Data;

namespace Strawberry.Common.Game.World.MapTraveling
{
    public class AdjacentSubMap
    {
        public AdjacentSubMap(SubMapBinder subMap, ICell[] changeCells)
        {
            SubMap = subMap;
            ChangeCells = changeCells;
        }

        public ICell[] ChangeCells { get; set; }

        public SubMapBinder SubMap { get; set; }
    }
}