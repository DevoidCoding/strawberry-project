// <copyright file="ICellList.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;
using Strawberry.Common.Game.World.Data;

namespace Strawberry.Common.Game.World
{
    public interface ICellList<out T> : IEnumerable<T>
        where T : ICell
    {
        /// <summary>
        ///     Returns null if not found
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T this[int id] { get; }
    }
}