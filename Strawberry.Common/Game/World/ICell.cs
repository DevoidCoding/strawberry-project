// <copyright file="ICell.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using ProtoBuf;

namespace Strawberry.Common.Game.World.Data
{
    public interface ICell
    {
        [ProtoMember(2)]
        int Floor { get; }

        [ProtoMember(1)]
        short Id { get; }

        [ProtoMember(4)]
        byte MapChangeData { get; }

        [ProtoMember(5)]
        byte MoveZone { get; }

        [ProtoMember(3)]
        sbyte Speed { get; }

        // ReSharper disable once InconsistentNaming
        [ProtoMember(8)]
        bool NonWalkableDuringRP { get; }

        [ProtoMember(10)]
        bool HavenbagCell { get; }

        [ProtoMember(12)]
        bool FarmCell { get; }

        [ProtoMember(11)]
        bool Visible { get; }

        [ProtoMember(13)]
        bool Red { get; }

        [ProtoMember(14)]
        bool Blue { get; }

        [ProtoMember(6)]
        bool Los { get; }

        [ProtoMember(9)]
        bool NonWalkableDuringFight { get; }

        [ProtoMember(7)]
        bool Mov { get; }

        // ReSharper disable once InconsistentNaming
        bool Walkable { get; }

        bool LineOfSight { get; }
    }
}