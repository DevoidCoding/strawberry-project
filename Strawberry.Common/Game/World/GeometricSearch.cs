﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Strawberry.Core.Reflection;
using Strawberry.Protocol.Tools.Dlm;
using Strawberry.Protocol.Tools.Ele;
using Strawberry.Protocol.Tools.Ele.Datas;

namespace Strawberry.Common.Game.World
{
    public class GeometricSearch : Singleton<GeometricSearch>
    {
        #region Public Properties

        public Rect MapRectangle { get; set; } = new Rect(0, 0, 1247, 881.5);

        #endregion Public Properties

        #region Private Methods

        private static void InitializeStaticGrid()
        {
            var cellCount = 0;

            for (var y = 0; y < Map.Height; y++)
            {
                for (var x = 0; x < Map.Width; x++)
                    OrthogonalGridReference[cellCount++] = new Point(x, y);

                for (var x = 0; x < Map.Width; x++)
                    OrthogonalGridReference[cellCount++] = new Point(x + 0.5, y + 0.5);
            }

            _initialized = true;
        }

        #endregion Private Methods

        #region Private Fields

        private static readonly Point[] OrthogonalGridReference = new Point[Map.MapSize];
        private static bool _initialized;

        #endregion Private Fields

        #region Public Methods

        public Point GetCenterPointFromCellId(short cellId) => GetCenterPointFromCellPoint(GetPointFromCell(cellId));

        public Point GetCenterPointFromCellPoint(Point cell)
        {
            return new Point(cell.X * 86 + 43, cell.Y * 43 + 21.5);
        }

        public Point GetPointFromCell(short id)
        {
            if (!_initialized)
                InitializeStaticGrid();

            if (id < 0 || id > Map.MapSize)
                throw new IndexOutOfRangeException("Cell identifier out of bounds (" + id + ").");

            var point = OrthogonalGridReference[id];

            return point;
        }

        public bool MapContainsElement(DlmGraphicalElement dlmGraphicalElement, SearchAlgorithmTypeEnum searchAlgorithmTypeEnum = SearchAlgorithmTypeEnum.Origin)
        {
            return searchAlgorithmTypeEnum == SearchAlgorithmTypeEnum.Origin ? SearchByOrigin(dlmGraphicalElement) : SearchBySize(dlmGraphicalElement);
        }

        public bool SearchByOrigin(DlmGraphicalElement dlmGraphicalElement)
        {
            var normalGraphicalElementData = EleReader.Instance.EleInstance.GetElementData((int)dlmGraphicalElement.ElementId) as NormalGraphicalElementData;
            var cellCenter = GetCenterPointFromCellId((short)dlmGraphicalElement.Cell.Id);
            cellCenter.Offset(dlmGraphicalElement.PixelOffset.X, dlmGraphicalElement.PixelOffset.Y);
            return MapRectangle.Contains(cellCenter.X, cellCenter.Y);
        } 

        public bool SearchBySize(DlmGraphicalElement dlmGraphicalElement)
        {
            var normalGraphicalElementData = EleReader.Instance.EleInstance.GetElementData((int)dlmGraphicalElement.ElementId) as NormalGraphicalElementData;
            var cellCenter = GetCenterPointFromCellId((short)dlmGraphicalElement.Cell.Id);
            if (normalGraphicalElementData == null)
                return false;

            var elementRect = new Rect(cellCenter.X - normalGraphicalElementData.Origin.X, cellCenter.Y - normalGraphicalElementData.Origin.Y, normalGraphicalElementData.Size.X, normalGraphicalElementData.Size.Y);

            elementRect.Offset(dlmGraphicalElement.PixelOffset.X, dlmGraphicalElement.PixelOffset.Y);
            return MapRectangle.IntersectsWith(elementRect);
        }

        #endregion Public Methods
    }
}
