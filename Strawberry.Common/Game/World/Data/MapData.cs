﻿// <copyright file="MapData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Linq;
using ProtoBuf;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Tools.Dlm;

namespace Strawberry.Common.Game.World.Data
{
    [ProtoContract]
    public class MapData : IMap
    {
        public MapData()
        {
        }

        public MapData(DlmMap map, MapPosition position = null)
        {
            Id = (int)map.Id;
            Version = map.Version;
            Encrypted = map.Encrypted;
            EncryptionVersion = map.EncryptionVersion;
            RelativeId = map.RelativeId;
            MapType = map.MapType;
            SubAreaId = map.SubAreaId;
            BottomNeighbourId = map.BottomNeighbourId;
            LeftNeighbourId = map.LeftNeighbourId;
            RightNeighbourId = map.RightNeighbourId;
            TopNeighbourId = map.TopNeighbourId;
            Cells = new CellDataList(map.Cells.Select(x => new CellData(x)).ToArray());
            if (position != null)
            {
                X = position.PosX;
                Y = position.PosY;
                WorldMap = position.WorldMap;
                Outdoor = position.Outdoor;
            }
            else
            {
                X = ((int)Id & 0x3FE00) >> 9; // 9 higher bits
                Y = (int)Id & 0x01FF; // 9 lower bits
                WorldMap = (int)Id & 0x3FFC0000 >> 18;

                if ((X & 0x100) == 0x100) // 9th bit is the sign. 1 means it's minus
                {
                    X = -(X & 0xFF); // just take the 8 first bits and take the opposite number
                }
                if ((Y & 0x100) == 0x100) // 9th bit is the sign. 1 means it's minus
                {
                    Y = -(Y & 0xFF); // just take the 8 first bits and take the opposite number
                }
            }

            TactictalModeTemplateId = map.TactictalModeTemplateId;
        }

        [ProtoMember(19)]
        public int TactictalModeTemplateId { get; set; }

        [ProtoMember(14)]
        public CellDataList Cells { get; set; }

        [ProtoMember(8)]
        public int BottomNeighbourId { get; set; }

        ICellList<ICell> IMap.Cells => Cells;

        [ProtoMember(3)]
        public bool Encrypted { get; set; }

        [ProtoMember(4)]
        public byte EncryptionVersion { get; set; }

        [ProtoMember(1)]
        public int Id { get; set; }

        [ProtoMember(10)]
        public int LeftNeighbourId { get; set; }

        [ProtoMember(6)]
        public byte MapType { get; set; }

        [ProtoMember(18)]
        public bool Outdoor { get; set; }

        [ProtoMember(5)]
        public uint RelativeId { get; set; }

        [ProtoMember(11)]
        public int RightNeighbourId { get; set; }

        [ProtoMember(7)]
        public int SubAreaId { get; set; }

        [ProtoMember(9)]
        public int TopNeighbourId { get; set; }

        [ProtoMember(12)]
        public bool UsingNewMovementSystem { get; set; }

        [ProtoMember(2)]
        public byte Version { get; set; }

        [ProtoMember(17)]
        public int WorldMap { get; set; }

        [ProtoMember(15)]
        public int X { get; set; }

        [ProtoMember(16)]
        public int Y { get; set; }

        public override string ToString()
        {
            return $"#{Id} [{X},{Y}]";
        }
    }
}