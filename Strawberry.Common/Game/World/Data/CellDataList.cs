﻿// <copyright file="CellDataList.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;

namespace Strawberry.Common.Game.World.Data
{
    [ProtoContract(IgnoreListHandling = true)]
    public class CellDataList : ICellList<CellData>
    {
        public CellDataList()
        {
        }

        public CellDataList(CellData[] array)
        {
            UnderlyingList = array;
        }

        [ProtoMember(1)]
        public CellData[] UnderlyingList { get; set; }

        public CellData this[int id] => UnderlyingList[id];

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<CellData> GetEnumerator()
        {
            return UnderlyingList.AsEnumerable().GetEnumerator();
        }
    }
}