﻿// <copyright file="MapPositionData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World.Data
{
    public class MapPositionData
    {
        public int? AreaId { get; set; }

        public int? BottomNeighbourId { get; set; }

        public int? LeftNeighbourId { get; set; }

        public int MapId { get; set; }

        public int? RightNeighbourId { get; set; }

        public int? SubAreaId { get; set; }

        public int? SuperAreaId { get; set; }

        public int? TopNeighbourId { get; set; }

        public int? WorldMapId { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int? GetNeighbourId(MapNeighbour neighbour)
        {
            switch (neighbour)
            {
                case MapNeighbour.Right:
                    return RightNeighbourId;
                case MapNeighbour.Left:
                    return LeftNeighbourId;
                case MapNeighbour.Top:
                    return TopNeighbourId;
                case MapNeighbour.Bottom:
                    return BottomNeighbourId;
                default:
                    return null;
            }
        }
    }
}