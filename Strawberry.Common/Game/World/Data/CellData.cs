﻿// <copyright file="CellData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using ProtoBuf;
using Strawberry.Protocol.Tools.Dlm;

namespace Strawberry.Common.Game.World.Data
{
    [ProtoContract]
    public class CellData : ICell
    {
        public CellData()
        {
        }

        public CellData(DlmCellData cell)
        {
            Id = cell.Id;
            Floor = cell.Floor;
            Speed = cell.Speed;
            MapChangeData = (byte) cell.MapChangeData;
            MoveZone = cell.MoveZone;
            Los = cell.Los;
            Mov = cell.Mov;
            NonWalkableDuringRP = cell.NonWalkableDuringRP;
            NonWalkableDuringFight = cell.NonWalkableDuringFight;
            HavenbagCell = cell.HavenbagCell;
            Visible = cell.Visible;
            FarmCell = cell.FarmCell;
            Red = cell.Red;
            Blue = cell.Blue;
            LinkedZoneRP = cell.LinkedZoneRP;
            LinkedZoneFight = cell.LinkedZoneFight;
        }

        [ProtoMember(16)]
        public int LinkedZoneFight { get; set; }

        [ProtoMember(15)]
        public int LinkedZoneRP { get; set; }

        [ProtoMember(14)]
        public bool Blue { get; set; }

        [ProtoMember(12)]
        public bool FarmCell { get; set; }

        [ProtoMember(2)]
        public int Floor { get; set; }

        [ProtoMember(10)]
        public bool HavenbagCell { get; set; }

        [ProtoMember(1)]
        public short Id { get; set; }

        public bool LineOfSight => Los;

        [ProtoMember(6)]
        public bool Los { get; set; }

        [ProtoMember(4)]
        public byte MapChangeData { get; set; }

        [ProtoMember(7)]
        public bool Mov { get; set; }

        [ProtoMember(5)]
        public byte MoveZone { get; set; }

        [ProtoMember(9)]
        public bool NonWalkableDuringFight { get; set; }

        // ReSharper disable once InconsistentNaming
        [ProtoMember(8)]
        public bool NonWalkableDuringRP { get; set; }

        [ProtoMember(13)]
        public bool Red { get; set; }

        [ProtoMember(3)]
        public sbyte Speed { get; set; }

        [ProtoMember(11)]
        public bool Visible { get; set; }

        public bool Walkable => Mov;
    }
}