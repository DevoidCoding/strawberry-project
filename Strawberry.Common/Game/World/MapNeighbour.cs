﻿// <copyright file="MapNeighbour.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World
{
    public enum MapNeighbour
    {
        None = 0,
        Right = 1,
        Top = 2,
        Left = 3,
        Bottom = 4,
        Any = 5
    }
}