﻿// <copyright file="SuperArea.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;

namespace Strawberry.Common.Game.World.Areas
{
    public class SuperArea
    {
        private readonly Protocol.Data.SuperArea _superArea;

        public SuperArea(int id)
        {
            _superArea = ObjectDataManager.Instance.Get<Protocol.Data.SuperArea>(id);
        }

        public int Id => _superArea.Id;

        public string Name => I18NDataManager.Instance.ReadText(NameId);

        public uint NameId => _superArea.NameId;

        public uint WorldmapId => _superArea.WorldmapId;
    }
}