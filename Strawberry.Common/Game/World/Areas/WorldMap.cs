﻿// <copyright file="WorldMap.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.ComponentModel;

namespace Strawberry.Common.Game.World.Areas
{
    public class WorldMap : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public Area GetArea(int id)
        {
            throw new NotImplementedException();
        }

        public Map GetMap(int id)
        {
            throw new NotImplementedException();
        }

        public SubArea GetSubArea(int id)
        {
            throw new NotImplementedException();
        }

        public SuperArea GetSuperArea(int id)
        {
            throw new NotImplementedException();
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}