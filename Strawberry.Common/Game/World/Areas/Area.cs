﻿using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.World.Areas
{
    public class Area
    {
        private readonly Protocol.Data.Area _area;

        public Area(int id)
        {
            _area = ObjectDataManager.Instance.Get<Protocol.Data.Area>(id);
            SuperArea = new SuperArea(SuperAreaId);
        }

        public Rectangle Bounds => _area.Bounds;

        public bool ContainHouses => _area.ContainHouses;

        public bool ContainPaddocks => _area.ContainPaddocks;

        public int Id => _area.Id;

        public string Name => I18NDataManager.Instance.ReadText(NameId);

        public uint NameId => _area.NameId;

        public SuperArea SuperArea { get; private set; }

        public int SuperAreaId => _area.SuperAreaId;

        public override string ToString() => Name;
    }
}