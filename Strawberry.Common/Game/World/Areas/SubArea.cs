﻿// <copyright file="SubArea.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World.Areas
{
    using System.Collections.Generic;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;

    public class SubArea
    {
        #region Fields

        private readonly Protocol.Data.SubArea subArea;

        private Area area;

        #endregion

        #region Constructors and Destructors

        public SubArea(int id)
            => subArea = ObjectDataManager.Instance.Get<Protocol.Data.SubArea>(id);

        #endregion

        #region Public Properties

        public AlignmentSideEnum AlignmentSide { get; set; }

        public IEnumerable<AmbientSound> AmbientSounds => subArea.AmbientSounds;

        public Area Area => area ?? (area = new Area(subArea.AreaId));

        public int AreaId => subArea.AreaId;

        public Rectangle Bounds => subArea.Bounds;

        public IEnumerable<uint> CustomWorldMap => subArea.CustomWorldMap;

        public IEnumerable<double> EntranceMapIds => subArea.EntranceMapIds;

        public IEnumerable<double> ExitMapIds => subArea.ExitMapIds;

        public IEnumerable<int> Harvestables => subArea.Harvestables;

        public int Id => subArea.Id;

        public IEnumerable<double> MapIds => subArea.MapIds;

        public string Name => I18NDataManager.Instance.ReadText(NameId);

        public uint NameId => subArea.NameId;

        public int PackId => subArea.PackId;

        public IEnumerable<int> Shape => subArea.Shape;

        #endregion

        #region Public Methods and Operators

        public override string ToString()
            => Name;

        #endregion
    }
}