﻿// <copyright file="Cell.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;
using System.Drawing;
using Strawberry.Common.Game.World.Data;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Tools.Dlm;

namespace Strawberry.Common.Game.World
{
    public class Cell : ICell, IComparable
    {
        public const int StructSize = 2 + 2 + 1 + 1 + 1 + 1;
        private static readonly Point[] OrthogonalGridReference = new Point[Map.MapSize];
        private static readonly Point VectorDown = new Point(1, -1);
        private static readonly Point VectorDownLeft = new Point(0, -1);
        private static readonly Point VectorDownRight = new Point(1, 0);
        private static readonly Point VectorLeft = new Point(-1, -1);
        private static readonly Point VectorRight = new Point(1, 1);
        private static readonly Point VectorUp = new Point(-1, 1);
        private static readonly Point VectorUpLeft = new Point(-1, 0);
        private static readonly Point VectorUpRight = new Point(0, 1);
        private static bool _initialized;
        private Point? _point;

        public Cell(Map map, DlmCellData cell)
        {
            Map = map;
            Id = cell.Id;
            Floor = cell.Floor;
            Speed = cell.Speed;
            MapChangeData = (byte) cell.MapChangeData;
            MoveZone = cell.MoveZone;
            Los = cell.Los;
            Mov = cell.Mov;
            NonWalkableDuringRP = cell.NonWalkableDuringRP;
            NonWalkableDuringFight = cell.NonWalkableDuringFight;
            HavenbagCell = cell.HavenbagCell;
            Visible = cell.Visible;
            FarmCell = cell.FarmCell;
            Red = cell.Red;
            Blue = cell.Blue;
            LinkedZoneRP = cell.LinkedZoneRP;
            LinkedZoneFight = cell.LinkedZoneFight;
        }

        public Cell(Map map, CellData cell)
        {
            Map = map;
            Id = cell.Id;
            Floor = cell.Floor;
            Speed = cell.Speed;
            MapChangeData = cell.MapChangeData;
            MoveZone = cell.MoveZone;
            Los = cell.Los;
            Mov = cell.Mov;
            NonWalkableDuringRP = cell.NonWalkableDuringRP;
            NonWalkableDuringFight = cell.NonWalkableDuringFight;
            HavenbagCell = cell.HavenbagCell;
            Visible = cell.Visible;
            FarmCell = cell.FarmCell;
            Red = cell.Red;
            Blue = cell.Blue;
            LinkedZoneRP = cell.LinkedZoneRP;
            LinkedZoneFight = cell.LinkedZoneFight;
        }

        public Map Map { get; }
        public Point Point => _point ?? (_point = GetPointFromCell(Id)).Value;
        public int X => Point.X;
        public int Y => Point.Y;
        public bool LineOfSight => Los;
        public bool Walkable => Mov;

        #region IComparable

        public int CompareTo(object obj)
        {
            var cell = obj as Cell;
            if (cell != null) return Id.CompareTo(cell.Id);
            if (obj is short) return Id.CompareTo(obj as short?);
            throw new InvalidOperationException($"Can't compare a Cell and a {obj.GetType().Name}");
        }

        #endregion IComparable

        public override string ToString()
            => $"{Id}[{X},{Y}]";

        #region Point-Cell

        public static short? GetCellFromPoint(Point point)
            => GetCellFromPoint(point.X, point.Y);

        public static short? GetCellFromPoint(int x, int y)
        {
            if (!_initialized)
                InitializeStaticGrid();

            var lowPart = y + (x - y) / 2;
            var highPart = x - y;

            if (lowPart < 0 || lowPart >= Map.Width)
                return null;

            if (highPart < 0 || highPart > 39)
                return null;

            var result = (int) (highPart * Map.Width + lowPart);

            if (result >= Map.MapSize || result < 0)
                return null;

            return (short) result;
        }

        public static Point GetPointFromCell(short id)
        {
            if (!_initialized)
                InitializeStaticGrid();

            if (id < 0 || id > Map.MapSize)
                throw new IndexOutOfRangeException("Cell identifier out of bounds (" + id + ").");

            var point = OrthogonalGridReference[id];

            return point;
        }

        /// <summary>
        ///     Initialize a static 2D plan that is used as reference to convert a cell to a (X,Y) point
        /// </summary>
        private static void InitializeStaticGrid()
        {
            var posX = 0;
            var posY = 0;
            var cellCount = 0;

            for (var x = 0; x < Map.Height; x++)
            {
                for (var y = 0; y < Map.Width; y++)
                    OrthogonalGridReference[cellCount++] = new Point(posX + y, posY + y);

                posX++;

                for (var y = 0; y < Map.Width; y++)
                    OrthogonalGridReference[cellCount++] = new Point(posX + y, posY + y);

                posY--;
            }

            _initialized = true;
        }

        #endregion

        #region Serialization

        //public byte[] Serialize()
        //{
        //    var bytes = new byte[StructSize];

        //    bytes[0] = (byte) (Id >> 8);
        //    bytes[1] = (byte) (Id & 0xFF);

        //    bytes[2] = (byte) (Floor >> 8);
        //    bytes[3] = (byte) (Floor & 0xFF);

        //    bytes[4] = LosMov;
        //    bytes[5] = MapChangeData;
        //    bytes[6] = Speed;

        //    bytes[7] = MoveZone;

        //    return bytes;
        //}

        //public void Deserialize(byte[] data, int index = 0)
        //{
        //    Id = (short) ((data[index + 0] << 8) | data[index + 1]);

        //    Floor = (short) ((data[index + 2] << 8) | data[index + 3]);

        //    LosMov = data[index + 4];
        //    MapChangeData = data[index + 5];
        //    Speed = data[index + 6];

        //    MoveZone = data[index + 7];
        //}

        #endregion

        #region Geometry

        public uint DistanceTo(Cell cell)
            => (uint) Math.Sqrt((cell.X - Point.X) * (cell.X - Point.X) + (cell.Y - Point.Y) * (cell.Y - Point.Y));

        public uint ManhattanDistanceTo(Cell cell)
        {
            if (cell == null) return 255;
            return (uint) (Math.Abs(Point.X - cell.X) + Math.Abs(Point.Y - cell.Y));
        }

        public bool IsInRadius(Cell cell, int radius)
            => ManhattanDistanceTo(cell) <= radius;

        public bool IsInRadius(Cell cell, int minRadius, int radius)
        {
            var dist = ManhattanDistanceTo(cell);
            return dist >= minRadius && dist <= radius;
        }

        public bool IsAdjacentTo(Cell cell, bool diagonal = true)
        {
            var dist = diagonal ? DistanceTo(cell) : ManhattanDistanceTo(cell);

            return dist == 1;
        }

        public static bool IsInMap(Cell cell)
            => IsInMap(cell.X, cell.Y);

        public static bool IsInMap(int x, int y)
            => x + y >= 0 && x - y >= 0 && x - y < Map.Height * 2 && x + y < Map.Width * 2;

        public DirectionsEnum OrientationTo(Cell cell, bool diagonal = true)
        {
            var dx = cell.X - X;
            var dy = Y - cell.Y;

            var distance = Math.Sqrt(dx * dx + dy * dy);
            var angleInRadians = Math.Acos(dx / distance);

            var angleInDegrees = angleInRadians * 180 / Math.PI;
            var transformedAngle = angleInDegrees * (cell.Y > Y ? -1 : 1);

            var orientation =
                !diagonal ? Math.Round(transformedAngle / 90) * 2 + 1 : Math.Round(transformedAngle / 45) + 1;

            if (orientation < 0) orientation = orientation + 8;

            return (DirectionsEnum) (uint) orientation;
        }

        public DirectionsEnum OrientationToAdjacent(Cell cell)
        {
            var vector = new Point
            {
                X = cell.X > Point.X ? 1 : (cell.X < Point.X ? -1 : 0),
                Y = cell.Y > Point.Y ? 1 : (cell.Y < Point.Y ? -1 : 0)
            };

            if (vector == VectorRight) return DirectionsEnum.DirectionEast;

            if (vector == VectorDownRight) return DirectionsEnum.DirectionSouthEast;

            if (vector == VectorDown) return DirectionsEnum.DirectionSouth;

            if (vector == VectorDownLeft) return DirectionsEnum.DirectionSouthWest;

            if (vector == VectorLeft) return DirectionsEnum.DirectionWest;

            if (vector == VectorUpLeft) return DirectionsEnum.DirectionNorthWest;

            if (vector == VectorUp) return DirectionsEnum.DirectionNorth;

            if (vector == VectorUpRight) return DirectionsEnum.DirectionNorthEast;

            return DirectionsEnum.DirectionEast;
        }

        /// <summary>
        ///     Returns null if the cell is not in the map
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public Cell GetCellInDirection(DirectionsEnum direction, short step)
        {
            Point mapPoint;

            switch (direction)
            {
                case DirectionsEnum.DirectionEast:

                {
                    mapPoint = new Point(Point.X + step, Point.Y + step);
                    break;
                }
                case DirectionsEnum.DirectionSouthEast:

                {
                    mapPoint = new Point(Point.X + step, Point.Y);
                    break;
                }
                case DirectionsEnum.DirectionSouth:

                {
                    mapPoint = new Point(Point.X + step, Point.Y - step);
                    break;
                }
                case DirectionsEnum.DirectionSouthWest:

                {
                    mapPoint = new Point(Point.X, Point.Y - step);
                    break;
                }
                case DirectionsEnum.DirectionWest:

                {
                    mapPoint = new Point(Point.X - step, Point.Y - step);
                    break;
                }
                case DirectionsEnum.DirectionNorthWest:

                {
                    mapPoint = new Point(Point.X - step, Point.Y);
                    break;
                }
                case DirectionsEnum.DirectionNorth:

                {
                    mapPoint = new Point(Point.X - step, Point.Y + step);
                    break;
                }
                case DirectionsEnum.DirectionNorthEast:

                {
                    mapPoint = new Point(Point.X, Point.Y + step);
                    break;
                }
                default:
                    throw new Exception("Unknown direction : " + direction);
            }

            return IsInMap(mapPoint.X, mapPoint.Y) ? Map.Cells[mapPoint] : null;
        }

        public IEnumerable<Cell> GetCellsInDirection(DirectionsEnum direction, short minDistance, short maxDistance)
        {
            for (var distance = minDistance; distance <= maxDistance; distance++)
            {
                var cell = GetCellInDirection(direction, distance);

                if (cell != null)
                    yield return cell;
            }
        }

        public IEnumerable<Cell> GetCellsInDirections(IEnumerable<DirectionsEnum> directions, short minDistance,
            short maxDistance)
        {
            foreach (var direction in directions)
            foreach (var cell in GetCellsInDirection(direction, minDistance, maxDistance))
                yield return cell;
        }

        public Cell GetNearestCellInDirection(DirectionsEnum direction)
            => GetCellInDirection(direction, 1);

        public IEnumerable<Cell> GetAdjacentCells(bool diagonals = false)
            => GetAdjacentCells(IsInMap, diagonals);

        public IEnumerable<Cell> GetAdjacentCells(Func<Cell, bool> predicate, bool diagonal = false)
        {
            var northEast = Map.Cells[Point.X, Point.Y + 1];

            if (northEast != null && IsInMap(northEast.X, northEast.Y) && predicate(northEast))
                yield return northEast;

            var northWest = Map.Cells[Point.X - 1, Point.Y];

            if (northWest != null && IsInMap(northWest.X, northWest.Y) && predicate(northWest))
                yield return northWest;

            var southEast = Map.Cells[Point.X + 1, Point.Y];

            if (southEast != null && IsInMap(southEast.X, southEast.Y) && predicate(southEast))
                yield return southEast;

            var southWest = Map.Cells[Point.X, Point.Y - 1];

            if (southWest != null && IsInMap(southWest.X, southWest.Y) && predicate(southWest))
                yield return southWest;

            if (diagonal)
            {
                var north = Map.Cells[Point.X - 1, Point.Y + 1];

                if (north != null && IsInMap(north.X, north.Y) && predicate(north))
                    yield return north;

                var east = Map.Cells[Point.X + 1, Point.Y + 1];

                if (east != null && IsInMap(east.X, east.Y) && predicate(east))
                    yield return east;

                var south = Map.Cells[Point.X + 1, Point.Y - 1];

                if (south != null && IsInMap(south.X, south.Y) && predicate(south))
                    yield return south;

                var west = Map.Cells[Point.X - 1, Point.Y - 1];

                if (west != null && IsInMap(west.X, west.Y) && predicate(west))
                    yield return west;
            }
        }

        public IEnumerable<Cell> GetAllCellsInRange(int minRange, int maxRange, bool ignoreThis,
            Func<Cell, bool> predicate)
        {
            for (var x = X - maxRange; x <= X + maxRange; x++)
            for (var y = Y - maxRange; y <= Y + maxRange; y++)
                if (!ignoreThis || x != X || y != Y)
                {
                    var distance = Math.Abs(x - X) + Math.Abs(y - Y);

                    if (IsInMap(x, y) && distance <= maxRange && distance >= minRange)
                    {
                        var cell = Map.Cells[x, y];
                        if (cell != null && (predicate == null || predicate(cell))) yield return cell;
                    }
                }
        }

        public IEnumerable<Cell> GetAllCellsInRectangle(Cell oppositeCell, bool skipStartAndEndCells,
            Func<Cell, bool> predicate)
        {
            int x1 = Math.Min(oppositeCell.X, X),
                y1 = Math.Min(oppositeCell.Y, Y),
                x2 = Math.Max(oppositeCell.X, X),
                y2 = Math.Max(oppositeCell.Y, Y);

            for (var x = x1; x <= x2; x++)
            for (var y = y1; y <= y2; y++)
                if (!skipStartAndEndCells || !(x == X && y == Y) && !(x == oppositeCell.X && y == oppositeCell.Y))
                {
                    var cell = Map.Cells[x, y];
                    if (cell != null && (predicate == null || predicate(cell))) yield return cell;
                }
        }

        public Cell[] GetCellsBetween(Cell cell, bool includeVertex = true)
        {
            var dx = cell.X - X;
            var dy = cell.Y - Y;

            var distance = Math.Sqrt(dx * dx + dy * dy);
            var vx = dx / distance;
            var vy = dy / distance;
            var roundedDistance = (int) distance;

            var result = new Cell[includeVertex ? roundedDistance + 1 : roundedDistance - 1];
            var i = 0;

            if (includeVertex)
                result[i++] = this;

            var x = X + vx;
            var y = Y + vx;

            while (i < roundedDistance)
            {
                x += vx;
                y += vy;
                result[i++] = Map.Cells[(int) x, (int) y];
            }

            if (includeVertex)
                result[i] = cell;

            return result;
        }

        public IEnumerable<Cell> GetCellsInLine(Cell destination)
        {
            // http://playtechs.blogspot.fr/2007/03/raytracing-on-grid.html

            var dx = Math.Abs(destination.X - X);
            var dy = Math.Abs(destination.Y - Y);
            var x = X;
            var y = Y;
            var n = 1 + dx + dy;
            var vectorX = destination.X > X ? 1 : -1;
            var vectorY = destination.Y > Y ? 1 : -1;
            var error = dx - dy;
            dx *= 2;
            dy *= 2;

            for (; n > 0; --n)
            {
                yield return Map.Cells[x, y];

                if (error > 0)
                {
                    x += vectorX;
                    error -= dy;
                }
                else if (error == 0)
                {
                    x += vectorX;
                    y += vectorY;
                    error -= dy;
                    error += dx;
                    n--;
                }
                else
                {
                    y += vectorY;
                    error += dx;
                }
            }
        }

        public bool IsChangeZone(Cell cell)
            => MoveZone != cell.MoveZone && Math.Abs(Floor) == Math.Abs(cell.Floor);

        #endregion

        #region Map Changement

        #endregion

        #region ICell Members

        public int Floor { get; }
        public short Id { get; }
        public byte MapChangeData { get; }
        public byte MoveZone { get; }
        public sbyte Speed { get; }

        // ReSharper disable once InconsistentNaming
        public bool NonWalkableDuringRP { get; }
        public bool HavenbagCell { get; }
        public bool FarmCell { get; }
        public bool Visible { get; }
        public bool Red { get; }
        public bool Blue { get; }
        public bool Los { get; }
        public bool NonWalkableDuringFight { get; }
        public bool Mov { get; }
        public int LinkedZoneFight { get; set; }
        public int LinkedZoneRP { get; set; }

        #endregion
    }
}