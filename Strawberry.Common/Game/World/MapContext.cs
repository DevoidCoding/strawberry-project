﻿// <copyright file="MapContext.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actors;
using Strawberry.Core.Collections;

namespace Strawberry.Common.Game.World
{
    public interface IMapContext
    {
        IEnumerable<ContextActor> Actors { get; }

        CellList Cells { get; }

        /// <summary>
        ///     Says if Cell1 can see Cell2
        ///     If not sure, then returns false.
        /// </summary>
        /// <returns></returns>
        bool CanBeSeen(Cell cell1, Cell cell2, bool throughEntities = true);

        ContextActor GetActor(double id);
        ContextActor[] GetActors();
        ContextActor[] GetActors(Func<ContextActor, bool> predicate);
        ContextActor[] GetActorsOnCell(int cellId);
        ContextActor[] GetActorsOnCell(Cell cell);
        bool IsActor(double id);
        bool IsActorOnCell(int cellId);
        bool IsActorOnCell(Cell cell);

        bool IsCellWalkable(Cell cell, bool throughEntities = true, Cell previousCell = null);
        bool IsTrapped(short cellId);
        bool RemoveActor(double id);

        void SetTrap(int noTrap, int cellId, int radius);
        void Tick(int dt);
        void UnsetTrap(int noTrap);
        event Action<ContextActor> ActorJoined;
    }

    public abstract class MapContext<T> : IMapContext, INotifyPropertyChanged where T : ContextActor
    {
        public const int ElevationTolerance = 11;
        public const uint Height = 20;
        public const uint MapSize = Width*Height*2;
        public const uint Width = 14;

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<double, T> _actors = new Dictionary<double, T>();
        private readonly ObservableCollectionMT<T> _collectionActors;
        private readonly ReadOnlyObservableCollectionMT<T> _readOnlyActors;

        protected readonly SortedList<int, SortedSet<short>> TrappedCells;

        public MapContext()
        {
            _collectionActors = new ObservableCollectionMT<T>();
            _readOnlyActors = new ReadOnlyObservableCollectionMT<T>(_collectionActors);
            TrappedCells = new SortedList<int, SortedSet<short>>();
        }

        public ReadOnlyObservableCollection<T> Actors => _readOnlyActors;

        IEnumerable<ContextActor> IMapContext.Actors => Actors;


        public abstract CellList Cells { get; }

        ContextActor IMapContext.GetActor(double id)
        {
            return GetActor(id);
        }

        ContextActor[] IMapContext.GetActors()
        {
            return GetActors();
        }

        public ContextActor[] GetActors(Func<ContextActor, bool> predicate)
        {
            return GetActors(predicate as Func<T, bool>);
        }


        ContextActor[] IMapContext.GetActorsOnCell(Cell cell)
        {
            return GetActorsOnCell(cell);
        }

        ContextActor[] IMapContext.GetActorsOnCell(int cellId)
        {
            return GetActorsOnCell(cellId);
        }

        public bool IsActor(double id)
        {
            return _actors.ContainsKey(id);
        }

        // Only if alive
        public bool IsActorOnCell(int cellId)
        {
            return _actors.Values.Any(actor => actor.Cell != null && actor.Cell.Id == cellId && actor.IsAlive);
        }

        public bool IsActorOnCell(Cell cell)
        {
            return IsActorOnCell(cell.Id);
        }

        public virtual bool IsTrapped(short cellId)
        {
            return false;
        }

        public bool RemoveActor(double id)
        {
            T actor;
            if (_actors.TryGetValue(id, out actor))
                return RemoveActor(actor);

            return false;
        }

        public virtual void SetTrap(int noTrap, int cellId, int radius)
        {
        }

        public abstract void Tick(int dt);

        public virtual void UnsetTrap(int noTrap)
        {
            if (TrappedCells.ContainsKey(noTrap))
                TrappedCells.Remove(noTrap);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Add an actor. Returns false is existing actor has been replaced
        /// </summary>
        /// <param name="actor"></param>
        /// <returns></returns>
        public virtual bool AddActor(T actor)
        {
            if (_actors.ContainsKey(actor.Id))
            {
                RemoveActor(actor.Id);

                _actors.Add(actor.Id, actor);
                _collectionActors.Add(actor);

                ActorJoined?.Invoke(actor);
                return false;
            }

            _actors.Add(actor.Id, actor);
            _collectionActors.Add(actor);
            ActorJoined?.Invoke(actor);

            return true;
        }

        public event Action<ContextActor> ActorJoined;

        public virtual void ClearActors()
        {
            _actors.Clear();
            _collectionActors.Clear();
        }

        public T GetActor(double id)
        {
            return Actors.FirstOrDefault(x => x.Id == id);
        }

        public TActor GetActor<TActor>(int id)
            where TActor : T
        {
            return Actors.FirstOrDefault(x => x.Id == id && x is TActor) as TActor;
        }

        public TActor GetActor<TActor>(Func<TActor, bool> predicate)
        {
            return Actors.OfType<TActor>().FirstOrDefault(predicate);
        }

        public T[] GetActors()
        {
            return Actors.ToArray();
        }

        public TActor[] GetActors<TActor>()
        {
            return Actors.OfType<TActor>().ToArray();
        }

        public T[] GetActors(Func<T, bool> predicate)
        {
            return Actors.Where(predicate).ToArray();
        }

        public TActor[] GetActors<TActor>(Func<TActor, bool> predicate)
        {
            return Actors.OfType<TActor>().Where(predicate).ToArray();
        }

        // Even dead one
        public T[] GetActorsOnCell(int cellId)
        {
            return _actors.Values.Where(actor => actor.Cell != null && actor.Cell.Id == cellId).ToArray();
        }

        public T[] GetActorsOnCell(Cell cell)
        {
            return GetActorsOnCell(cell.Id);
        }

        public virtual bool RemoveActor(T actor)
        {
            return _actors.Remove(actor.Id) && _collectionActors.Remove(actor);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region LoS

        /// <summary>
        ///     Check if distance from C to the segment [AB] is less or very close to sqrt(2)/2 "units"
        ///     and the projection of C on the line (AB) is inside the segment [AB].
        ///     This should give a conservative way to compute LOS. In very close cases
        ///     (where the exact implementation of the LOS algorithm could make the difference), then
        ///     we consider that the LOS is blocked. The safe way.
        /// </summary>
        private bool TooCloseFromSegment(int cx, int cy, int ax, int ay, int bx, int by)
        {
            const double MIN_DISTANCE_SQUARED = 0.5001;

            // Distance computing is inspired by Philip Nicoletti algorithm - http://forums.codeguru.com/printthread.php?t=194400&pp=15&page=2     
            var numerator = (cx - ax)*(bx - ax) + (cy - ay)*(by - ay);
            var denomenator = (bx - ax)*(bx - ax) + (by - ay)*(by - ay);

            if (numerator > denomenator || numerator < 0)
                return false;
                    //The projection of the point on the line is outside the segment, so it doesn't block the LOS

            double Base = (ay - cy)*(bx - ax) - (ax - cx)*(by - ay);
            var distanceLineSquared = Base*Base/denomenator;
            return distanceLineSquared <= MIN_DISTANCE_SQUARED;
                // if distance to line is frankly over sqrt(2)/2, it won't block LOS. 
        }


        /// <summary>
        ///     Says if Cell1 can see Cell2
        ///     If not sure, then returns false.
        ///     Warning : cell1 and cell2 are ignored !
        /// </summary>
        /// <remarks>Same version as the client</remarks>
        /// <returns></returns>
        public bool CanBeSeen(Cell from, Cell to, bool throughEntities = false)
        {
            if (from == null || to == null) return false;
            if (from == to) return true;

            var occupiedCells = new short[0];
            if (!throughEntities)
                occupiedCells = _actors.Values.Where(e => e.Cell != null).Select(x => x.Cell.Id).ToArray();

            var line = from.GetCellsInLine(to);
            foreach (var cell in line.Skip(1)) // skip first cell
            {
                if (cell == null)
                    continue;

                if (to.Id == cell.Id)
                    continue;

                if (!throughEntities && Array.IndexOf(occupiedCells, cell.Id) != -1)
                    return false;
            }

            return true;
        }

        #endregion

        #region Pathfinding

        public bool IsCellWalkable(Cell cell, bool throughEntities = false, Cell previousCell = null)
        {
            if (!cell.Walkable)
                return false;

            if (cell.NonWalkableDuringRP)
                return false;

            // compare the floors
            if (UsingNewMovementSystem && previousCell != null)
            {
                var floorDiff = Math.Abs(cell.Floor) - Math.Abs(previousCell.Floor);

                if (cell.MoveZone != previousCell.MoveZone ||
                    cell.MoveZone == previousCell.MoveZone && cell.MoveZone == 0 && floorDiff > Map.ElevationTolerance)
                    return false;
            }

            if (!throughEntities && IsActorOnCell(cell))
                return false;

            // todo : LoS => Sure ? LoS may stop a walk ?!

            return true;
        }

        public abstract bool UsingNewMovementSystem { get; }

        #endregion
    }
}