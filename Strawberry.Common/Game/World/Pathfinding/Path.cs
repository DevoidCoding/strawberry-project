// <copyright file="Path.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.World.Pathfinding
{
    public class Path
    {
        private PathElement[] m_compressedPath;

        /// <summary>
        ///     Constructs the Path instance from the complete path as a list of cell
        /// </summary>
        /// <param name="map">Current map</param>
        /// <param name="path">Complete path</param>
        public Path(IMapContext map, IEnumerable<Cell> path)
        {
            Map = map;
            Cells = path.ToArray();
        }

        /// <summary>
        ///     Constructs the Path instance from the compressed path as a list of PathElement
        /// </summary>
        /// <param name="map">Current map</param>
        /// <param name="compressedPath">Compressed Path</param>
        private Path(IMapContext map, IEnumerable<PathElement> compressedPath)
        {
            Map = map;
            m_compressedPath = compressedPath.ToArray();
            Cells = BuildCompletePath();
        }

        public Cell[] Cells { get; private set; }

        public Cell End
        {
            get
            {
                if (Cells != null && Cells.Length > 0) return Cells[Cells.Length - 1];
                return null;
            }
        }

        public IMapContext Map { get; }

        // Warning : this is wrong
        public int MPCost
        {
            get { return (int) Start.ManhattanDistanceTo(End); }
        }

        public Cell Start
        {
            get
            {
                if (Cells != null && Cells.Length > 0) return Cells[0];
                return null;
            }
        }

        /// <summary>
        ///     Build a Path instance from the keys sent by the client
        /// </summary>
        /// <param name="map"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static Path BuildFromClientCompressedPath(IMapContext map, IEnumerable<short> keys)
        {
            var path = from key in keys
                let cellId = key & 4095
                let direction = (DirectionsEnum) ((key >> 12) & 7)
                select new PathElement(map.Cells[cellId], direction);

            return new Path(map, path);
        }

        /// <summary>
        ///     Build a Path instance from the keys sent by the server
        /// </summary>
        /// <returns></returns>
        public static Path BuildFromServerCompressedPath(Map map, IEnumerable<short> keys)
        {
            var cells = keys.Select(entry => map.Cells[entry]).ToArray();
            var compressedPath = new List<PathElement>();

            for (var i = 0; i < cells.Length - 1; i++)
            {
                compressedPath.Add(new PathElement(cells[i], cells[i].OrientationTo(cells[i + 1])));
            }

            compressedPath.Add(new PathElement(cells[cells.Length - 1], DirectionsEnum.DirectionEast));

            return new Path(map, compressedPath);
        }

        public bool Contains(short cellId)
        {
            return Cells.Any(entry => entry.Id == cellId);
        }

        public void CutPath(int index)
        {
            if (index > Cells.Length - 1)
                return;

            Cells = Cells.Take(index).ToArray();
        }

        /// <summary>
        ///     Build an array of keys that represents the path like a path sent by the client
        /// </summary>
        /// <returns></returns>
        public short[] GetClientPathKeys()
        {
            var compressedPath = GetCompressedPath();

            return
                compressedPath.Select(entry => (short) ((ushort) entry.Cell.Id | ((ushort) entry.Direction << 12)))
                              .ToArray();
        }

        public PathElement[] GetCompressedPath()
        {
            return m_compressedPath ?? (m_compressedPath = BuildCompressedPath());
        }

        /// <summary>
        ///     Get an empty path with the current cell
        /// </summary>
        /// <param name="map"></param>
        /// <param name="startCell"></param>
        /// <returns></returns>
        public static Path GetEmptyPath(IMapContext map, Cell startCell)
        {
            return new Path(map, new[] {startCell});
        }

        public DirectionsEnum GetEndCellDirection()
        {
            if (Cells.Length <= 1)
                return DirectionsEnum.DirectionEast;

            if (m_compressedPath != null)
                return m_compressedPath.Last().Direction;

            return Cells[Cells.Length - 2].OrientationToAdjacent(Cells[Cells.Length - 1]);
        }

        /// <summary>
        ///     Build an array of keys that represents the path like a path sent by the server
        /// </summary>
        /// <returns></returns>
        public short[] GetServerPathKeys()
        {
            var compressedPath = GetCompressedPath();

            return compressedPath.Select(entry => entry.Cell.Id).ToArray();
        }

        public bool IsEmpty()
        {
            return Cells == null || Cells.Length <= 1; // if end == start the path is also empty
        }

        public override string ToString()
        {
            return string.Join<Cell>("-", Cells);
        }

        private Cell[] BuildCompletePath()
        {
            var completePath = new List<Cell>();

            for (var i = 0; i < m_compressedPath.Length - 1; i++)
            {
                completePath.Add(m_compressedPath[i].Cell);

                var l = 0;
                var nextPoint = m_compressedPath[i].Cell;
                while ((nextPoint = nextPoint.GetNearestCellInDirection(m_compressedPath[i].Direction)) != null &&
                       nextPoint.Id != m_compressedPath[i + 1].Cell.Id)
                {
                    if (l > World.Map.Height*2 + World.Map.Width)
                        throw new Exception("Path too long. Maybe an orientation problem ?");

                    completePath.Add(Map.Cells[nextPoint.Id]);

                    l++;
                }
            }

            completePath.Add(m_compressedPath[m_compressedPath.Length - 1].Cell);

            return completePath.ToArray();
        }

        private PathElement[] BuildCompressedPath()
        {
            if (Cells.Length <= 0)
                return new PathElement[0];

            // only one cell
            if (Cells.Length <= 1)
                return new[] {new PathElement(Cells[0], DirectionsEnum.DirectionEast)};

            // build the path
            var path = new List<PathElement>();
            for (var i = 1; i < Cells.Length; i++)
            {
                path.Add(new PathElement(Cells[i - 1], Cells[i - 1].OrientationToAdjacent(Cells[i])));
            }

            path.Add(new PathElement(Cells[Cells.Length - 1], path[path.Count - 1].Direction));

            // compress it
            if (path.Count > 0)
            {
                var i = path.Count - 2; // we don't touch to the last vector
                while (i > 0)
                {
                    if (path[i].Direction == path[i - 1].Direction)
                        path.RemoveAt(i);
                    i--;
                }
            }

            return path.ToArray();
        }
    }
}