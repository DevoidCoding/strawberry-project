﻿// <copyright file="IOpenList.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World.Pathfinding
{
    public interface IOpenList
    {
        int Count { get; }

        Cell Pop();

        void Push(Cell cell);
    }
}