﻿// <copyright file="IAdvancedPathFinder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;

namespace Strawberry.Common.Game.World.Pathfinding
{
    public interface IAdvancedPathFinder : ISimplePathFinder
    {
        Path FindPath(IEnumerable<Cell> startCells, IEnumerable<Cell> endCells, bool outsideFight, int mp = -1,
            int minDistance = 0, bool cautiousMode = false);

        Path FindPath(Cell startCell, IEnumerable<Cell> endCells, bool outsideFight, int mp = -1, int minDistance = 0,
            bool cautiousMode = false);

        Path FindPath(Cell startCell, Cell endCell, bool outsideFight, int mp = -1, int MinDistance = 0,
            bool cautiousMode = false);
    }
}