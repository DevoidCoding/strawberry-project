﻿// <copyright file="PathElement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.World.Pathfinding
{
    public class PathElement
    {
        public PathElement(Cell cell, DirectionsEnum direction)
        {
            Cell = cell;
            Direction = direction;
        }

        public Cell Cell { get; set; }
        public DirectionsEnum Direction { get; set; }
    }
}