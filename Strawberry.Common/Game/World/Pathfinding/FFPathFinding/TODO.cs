﻿// <copyright file="TODO.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;

/// <summary>
/// Here are grouped all stuff that still need to be done for proper integration in Strawberry
/// </summary>

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    /// <summary>
    ///     MapDataManager provides easy access to Map data.
    ///     It provides fast reading of headers, and only load complete map data as needed.
    /// </summary>
    public class MapDataManager
    {
        public MapDataManager(string pathToMapsData, bool StoredInMap)
        {
            throw new NotImplementedException();
        }

        public Dictionary<object, Map> Maps { get; private set; }

        public int NbMapsInNativeFiles { get; set; }
    }

    public class MapParser
    {
    }

    public class PakFile
    {
        internal bool ExistsFile(int mapId)
        {
            throw new NotImplementedException();
        }
    }
}