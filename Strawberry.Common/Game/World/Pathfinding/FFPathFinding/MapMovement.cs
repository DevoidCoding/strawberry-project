﻿// <copyright file="MapMovement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;
using System.Diagnostics;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public class MapMovement
    {
        private readonly CellInfo[] cells;

        public MapMovement(CellInfo[] _cells)
        {
            cells = _cells;
        }

        /// <summary>
        ///     Give each cell (included the starting cell) of the path as input, and compute packed cell array for
        ///     GameMapMovementRequestMessage.
        /// </summary>
        /// <param name="path">Minimum size = 2 (source and dest cells)</param>
        /// <returns></returns>
        public short[] PackPath(short[] path)
        {
            Debug.Assert(path.Length > 1); // At least source and dest cells          
            var PackedPath = new List<short>();
            if (path.Length < 2) return PackedPath.ToArray();
            DirectionsEnum PreviousOrientation = DirectionsEnum.DirectionEast;
            DirectionsEnum Orientation = DirectionsEnum.DirectionEast;
            var PreviousCellId = path[0];
            for (short NoCell = 1; NoCell < path.Length; NoCell++)
            {
                var cellid = path[NoCell];
                Debug.Assert(cellid >= 0 && cellid < CellInfo.NB_CELL);

                Orientation = GetOrientation(PreviousCellId, cellid);
                if (NoCell == 1 || (Orientation != PreviousOrientation) || NoCell == path.Length - 1)
                    // Odd, but first step is always packed
                {
                    PackedPath.Add((short) ((ushort) cellid | (ushort) Orientation << 12));
                    PreviousOrientation = Orientation;
                }

                PreviousCellId = cellid;
            }
            return PackedPath.ToArray();
        }

        private DirectionsEnum GetOrientation(short cellStart, short cellDest)
        {
            var dx = cells[cellDest].X - cells[cellStart].X;
            var dy = cells[cellDest].Y - cells[cellStart].Y;
            if (dx == 0)
                if (dy == 0)
                {
                    Debug.Assert(false);
                    return DirectionsEnum.DirectionEast;
                } // 0,0 - no mouvement :p
                else if (dy < 0) return DirectionsEnum.DirectionSouthWest; // 0,-1
                else
                    return DirectionsEnum.DirectionNorthEast; // 0,1
            if (dx < 0)
                if (dy == 0) return DirectionsEnum.DirectionNorthWest; // -1,0
                else if (dy < 0) return DirectionsEnum.DirectionWest; // -1,-1
                else
                    return DirectionsEnum.DirectionNorth; // -1,1
            if (dy == 0) return DirectionsEnum.DirectionSouthEast; // 1,0
            if (dy < 0) return DirectionsEnum.DirectionSouth; // 1,-1
            return DirectionsEnum.DirectionEast; // 1,1
        }
    }
}