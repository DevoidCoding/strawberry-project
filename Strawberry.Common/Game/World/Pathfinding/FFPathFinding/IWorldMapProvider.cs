﻿// <copyright file="IWorldMapProvider.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;
using Strawberry.Common.Game.World.Data;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public interface IWorldMapProvider
    {
        SortedSet<int> KnownMapIds { get; }
        IMap LoadMap(int mapId);
    }
}