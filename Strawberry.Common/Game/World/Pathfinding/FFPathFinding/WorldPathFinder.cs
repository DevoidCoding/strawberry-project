﻿// <copyright file="WorldPathFinder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public class WorldPathFinder
    {
        private const uint DEFAULT_DISTANCE = 0xFFFFFFFF;
        private readonly Random rnd = new Random();

        // Ctor : provides cells array and mode (combat or not)
        public WorldPathFinder(short[][] MapLinks, uint[] Penalties)
        {
            cells = cells;
            links = MapLinks;
            mapPenalties = Penalties;
        }

        // Cells stores information about each square.
        public WorldCellInfo[] cells { get; private set; }
        public int ExitCell { get; private set; }
        private short[][] links { get; }
        private uint[] mapPenalties { get; }

        public List<int> PathResult { get; private set; }

        //public Dictionary<int, CellInfo> startingCells;
        //public Dictionary<int, CellInfo> exitCells;
        public int StartingCell { get; private set; }

        /// <summary>
        ///     Reset old PathFinding path from the cells.
        /// </summary>
        public void ClearLogic()
        {
            // Reset some information about the cells.
            if (cells == null)
                cells = new WorldCellInfo[links.Length];
            for (var i = 0; i < cells.Length; i++)
            {
                if (cells[i] == null)
                {
                    cells[i] = null;
                }

                cells[i].distanceSteps = DEFAULT_DISTANCE;
                cells[i].isInPath = false;
            }

            if (PathResult == null)
                PathResult = new List<int>();
            else
                PathResult.Clear();
        }

        /// <summary>
        ///     Returns the last path.
        /// </summary>
        /// <param name="MinDistance"></param>
        /// <returns></returns>
        public int[] GetLastPath()
        {
            return PathResult.ToArray();
        }


        /// <summary>
        ///     Compute the exact length of the last path.
        /// </summary>
        /// <returns></returns>
        public int GetLengthOfLastPath()
        {
            return PathResult.Count - 1;
        }

        //Int32 posRandom;

        public class WorldCellInfo
        {
            public uint distanceSteps = DEFAULT_DISTANCE;
            public bool isInPath;
        }

        #region FindPath algorithm itself

        /// <summary>
        ///     Entry point for PathFinding algorithm, with one starting cell, and one exit
        /// </summary>
        public bool FindPath(int StartingSubMapId, int ExitSubMapId)
        {
            int[] ExitStartingSubMapIds = {ExitSubMapId};
            return FindPath(StartingSubMapId, ExitStartingSubMapIds);
        }

        /// <summary>
        ///     Entry point for PathFinding algorithm, with the new SubMapId of the Destination (usually the leader' position)
        /// </summary>
        public bool ProcessNewDistancesFromDestination(int destinationSubMapId)
        {
            return FindPath(destinationSubMapId, null, true);
        }

        /// <summary>
        ///     Find where the team member should move from its current position to reach the leader
        ///     Return -1 if no path can be found
        ///     If several possible, then get a random one
        /// </summary>
        public int GetNextSubMapIdToReachTheDestination(int MemberCurrentSubMapId, bool RandomPath = true)
        {
            var BestNextCell = -1;
            if (cells == null) throw new Exception("Cells are not initialized");
            var BestChoiceDistance = DEFAULT_DISTANCE;
            foreach (int NextSubMap in links[MemberCurrentSubMapId])
                if (cells[NextSubMap].distanceSteps < BestChoiceDistance)
                {
                    BestNextCell = NextSubMap;
                    BestChoiceDistance = cells[NextSubMap].distanceSteps;
                }
                else if ((cells[NextSubMap].distanceSteps == BestChoiceDistance) && RandomPath && (rnd.Next(2) == 0))
                    // If 2 possible cells have same value, choose randomly
                {
                    BestNextCell = NextSubMap;
                }
            return BestNextCell;
        }


        /// <summary>
        ///     PathFinding main method
        /// </summary>
        /// <param name="startingCells"></param>
        /// <param name="exitCells"></param>
        /// <param name="selectFartherCells"></param>
        /// <param name="firstStepOnly"></param>
        /// <returns></returns>
        public bool FindPath(int StartingSubMapId, int[] ExitSubMapIds, bool FirstStepOnly = false)
        {
            var rnd = new Random();
            ClearLogic();
            if (cells == null) throw new Exception("Cells are not initialized");
            if (StartingSubMapId < 0 || StartingSubMapId >= cells.Length)
                return false; // We need at least one starting cell
            if (!FirstStepOnly && (ExitSubMapIds == null || ExitSubMapIds.Length == 0))
                return false; // We need at least one exit cell for step 2
            // PC starts at distance of 0. Set 0 to all possible starting cells
            cells[StartingSubMapId].distanceSteps = 0;

            //    cells[StartingCell].distanceSteps = 0;
            var NbMainLoop = 0;
            while (true)
            {
                NbMainLoop++;
                var madeProgress = false;

                // Look at each square on the board.
                for (var CurrentSubMap = 0; CurrentSubMap < cells.Length; CurrentSubMap++)
                {
                    var LocalLinks = links[CurrentSubMap];
                    uint penalty = 0;

                    var nexMapDistance = cells[CurrentSubMap].distanceSteps + 1;
                    foreach (var Link in LocalLinks)
                    {
                        if (mapPenalties != null)
                            penalty = mapPenalties[Link];
                        if (cells[Link].distanceSteps > nexMapDistance + penalty)
                        {
                            cells[Link].distanceSteps = nexMapDistance + penalty;
                            madeProgress = true;
                        }
                    }
                }
                if (!madeProgress)
                {
                    break;
                }
            }

            if (FirstStepOnly)
                return true;
            // Step 2
            // Mark the path from Exit to Starting position.
            // if several Exit cells, then get the lowest distance one = the closest from one starting cell
            // (or the highest distance one if selectFartherCells)
            ExitCell = ExitSubMapIds[0];
            var MinDist = cells[ExitCell].distanceSteps;
            foreach (var cell in ExitSubMapIds)
                if (cells[cell].distanceSteps < MinDist)
                {
                    ExitCell = cell;
                    MinDist = cells[cell].distanceSteps;
                }
            var CurrentCell = ExitCell;
            PathResult.Add(ExitCell);
            cells[ExitCell].isInPath = true;
            // Look through each MapNeighbour and find the square
            // with the lowest number of steps marked.
            int lowestPoint;
            uint lowest;
            var LowestPoints = new List<int>(10);
            while (true)
            {
                // Look through each MapNeighbour and find the square
                // with the lowest number of steps marked.
                lowestPoint = CellInfo.CELL_ERROR;
                lowest = DEFAULT_DISTANCE;

                foreach (int NewSubMapId in links[CurrentCell])
                {
                    var count = cells[NewSubMapId].distanceSteps;
                    if (count < lowest)
                    {
                        LowestPoints.Clear();
                        lowest = count;
                        lowestPoint = NewSubMapId;
                    }
                    else if (count == lowest)
                        // If more than one point, then push it in the list, for random determination
                    {
                        if (LowestPoints.Count == 0)
                            LowestPoints.Add(lowestPoint);
                        LowestPoints.Add(NewSubMapId);
                    }
                }
                if (lowest == DEFAULT_DISTANCE) break; // Can't find a valid way :(
                if (LowestPoints.Count > 1) // Several points with same distance =>> randomly select one of them
                    lowestPoint = LowestPoints[rnd.Next(LowestPoints.Count)];

                // Mark the subMap as part of the path if it is the lowest
                // number. Set the current position as the subMap with
                // that number of steps.
                PathResult.Add(lowestPoint);
                cells[lowestPoint].isInPath = true;
                CurrentCell = lowestPoint;

                if (cells[CurrentCell].distanceSteps == 0) // Exit reached            
                {
                    StartingCell = CurrentCell;
                    // We went from closest Exit to a Starting position, so we're finished.
                    break;
                }
            }
            PathResult.Reverse(); // Reorder the path from starting position to target
            return CurrentCell == StartingCell;
        }

        #endregion FindPath algorithm itself
    }
}