﻿// <copyright file="WorldPoint.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public class WorldPoint
    {
        public static WorldPoint EmptyPoint = new WorldPoint();
        public int WorldId;
        public int X;
        public int Y;

        /// <summary>
        ///     Set point Coordinates from mapId
        /// </summary>
        /// <param name="mapId"></param>
        public WorldPoint(int mapId)
        {
            WorldId = ((int)mapId & 0x3FFC0000) >> 18;
            X = mapId >> 9 & 511;
            Y = mapId & 511;
            if ((X & 0x00000100) == 0x00000100)
            {
                X = -(X & 0x000000FF);
            }
            if ((Y & 0x00000100) == 0x00000100)
            {
                Y = -(Y & 0x000000FF);
            }
        }

        public WorldPoint(int mapId, MapNeighbour MapNeighbour)
            : this(mapId)
        {
            Move(MapNeighbour);
        }

        public WorldPoint(WorldPoint point)
        {
            X = point.X;
            Y = point.Y;
            WorldId = point.WorldId;
        }

        public WorldPoint(WorldPoint point, MapNeighbour MapNeighbour) : this(point)
        {
            Move(MapNeighbour);
        }

        public WorldPoint(int x, int y, int worldId)
        {
            X = x;
            Y = y;
            WorldId = worldId;
        }

        public WorldPoint(int x, int y)
        {
            X = x;
            Y = y;
            WorldId = 0;
        }

        public WorldPoint()
        {
            X = int.MinValue;
            Y = int.MinValue;
            WorldId = 0;
        }

        /// <summary>
        ///     Compute mapId from WorldPoint coordinates
        /// </summary>
        /// <returns></returns>
        public int MapID
        {
            get
            {
                const int MAP_COORDS_MAX = 0x00000200;
                const int WORLD_ID_MAX = 0x00002000;

                if ((X > MAP_COORDS_MAX) || (Y > MAP_COORDS_MAX) || (WorldId > WORLD_ID_MAX))
                {
                    throw new ArgumentException("Coordinates or world identifier out of range.");
                }

                var _X = Math.Abs(X) & 255;
                var _Y = Math.Abs(Y) & 255;
                var _WorldId = WorldId & 4095;
                if (X < 0)
                {
                    _X = _X | 0x00000100;
                }
                if (Y < 0)
                {
                    _Y = _Y | 0x00000100;
                }
                var _mapId =(_WorldId << 18 | (_X << 9) | _Y);
                return _mapId;
            }
        }

        public WorldPoint GetNeighbourPosition(MapNeighbour MapNeighbour)
        {
            return new WorldPoint(this, MapNeighbour);
        }

        public bool isEmpty()
        {
            return this == null || (X == EmptyPoint.X && Y == EmptyPoint.Y);
        }

        public void Move(MapNeighbour MapNeighbour)
        {
            switch (MapNeighbour)
            {
                case MapNeighbour.Bottom:
                    Y += 1;
                    break;
                case MapNeighbour.Top:
                    Y -= 1;
                    break;
                case MapNeighbour.Left:
                    X -= 1;
                    break;
                case MapNeighbour.Right:
                    X += 1;
                    break;
            }
        }
    }
}