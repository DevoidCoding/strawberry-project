﻿// <copyright file="MapExtensions.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Game.World.Data;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public static class MapExtensions
    {
        private static readonly Random rnd = new Random();

        // Select a random transition for another map. If set, limit the search in a given MapNeighbour. 
        public static Cell GetClosestTransitionCell(this Map map, MapNeighbour MapNeighbour, Cell startingCell)
        {
            var MapChangeMask = GetMapChangeMask(MapNeighbour);
            return
                map.Cells.Where(cell => (cell.MapChangeData & MapChangeMask) != 0)
                   .OrderBy(cell => cell.DistanceTo(startingCell))
                   .FirstOrDefault();
        }

        public static int GetNeighbourMapId(this IMap map, MapNeighbour mapNeighbour, bool GetStoredVersion)
        {
            if (GetStoredVersion)
                switch (mapNeighbour)
                {
                    case MapNeighbour.Bottom:
                        return (int)map.BottomNeighbourId;
                    case MapNeighbour.Top:
                        return (int)map.TopNeighbourId;
                    case MapNeighbour.Left:
                        return (int)map.LeftNeighbourId;
                    case MapNeighbour.Right:
                        return (int)map.RightNeighbourId;
                    default:
                        throw new ArgumentOutOfRangeException("MapNeighbour");
                }

            var MapChangeMask = GetMapChangeMask(mapNeighbour);

            // Check if at least one cell allow a transition to the supposed-to-be neighbour
            var ChangeMapFound = map.Cells.Any(cell => (cell.MapChangeData & MapChangeMask) != 0);
            if (ChangeMapFound)
                return new WorldPoint((int) map.Id, mapNeighbour).MapID;
            return -1;
        }

        /// <summary>
        ///     Select a random item within the elements of an IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputSet"></param>
        /// <returns></returns>
        public static T GetRandom<T>(this IEnumerable<T> inputSet)
        {
            return GetRandom(inputSet.ToArray());
        }

        /// <summary>
        ///     Select a random item within the elements of an array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputSet"></param>
        /// <returns></returns>
        public static T GetRandom<T>(this T[] inputSet)
        {
            if (inputSet.Length == 0) return default(T);
            return inputSet[rnd.Next(inputSet.Length)];
        }


        // Select a random transition for another map. If set, limit the search in a given MapNeighbour. 
        public static Cell GetRandomTransitionCell(this Map map, MapNeighbour? MapNeighbour)
        {
            var MapChangeMask = GetMapChangeMask(MapNeighbour);
            return map.Cells.Where(cell => (cell.MapChangeData & MapChangeMask) != 0).GetRandom();
        }

        public static ICell GetTransitionCell(this IMap map, MapNeighbour mapNeighbour)
        {
            var MapChangeMask = GetMapChangeMask(mapNeighbour);

            // Check if at least one cell allow a transition to the supposed-to-be neighbour
            return map.Cells.FirstOrDefault(cell => (cell.MapChangeData & MapChangeMask) != 0);
        }

        // Returns all cells that allow a transition to the map in the given MapNeighbour
        // If MapNeighbour = null, then any MapNeighbour applies
        public static IEnumerable<Cell> GetTransitionCells(this Map map, MapNeighbour? MapNeighbour)
        {
            var MapChangeMask = GetMapChangeMask(MapNeighbour);
            return map.Cells.Where(cell => (cell.MapChangeData & MapChangeMask) != 0);
        }

        // Returns all cells that allow a transition to the map in the given MapNeighbour
        // If MapNeighbour = null, then any MapNeighbour applies
        public static IEnumerable<Cell> GetTransitionCells(IEnumerable<Cell> inputCells, MapNeighbour? MapNeighbour)
        {
            var MapChangeMask = GetMapChangeMask(MapNeighbour);
            return inputCells.Where(cell => (cell.MapChangeData & MapChangeMask) != 0);
        }

        private static int GetMapChangeMask(MapNeighbour? mapNeighbour)
        {
            switch (mapNeighbour)
            {
                case MapNeighbour.Bottom:
                    return 4;
                case MapNeighbour.Top:
                    return 64;
                case MapNeighbour.Left:
                    return 16;
                case MapNeighbour.Right:
                    return 1;
                case null:
                default:
                    return 1 | 4 | 16 | 64;
            }
        }
    }
}