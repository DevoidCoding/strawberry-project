﻿// <copyright file="CellInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public class CellInfo
    {
        public enum FieldNames
        {
            Nothing,
            Label,
            isInPath,
            isWalkable,
            isCombatWalkable,
            allowLOS, /*color, */
            speed,
            coordinates,
            PathFindingInformations,
            mapLink,
            cellID /*, gfxCount, firstGfx */
        }

        public const short CELL_ERROR = -1;
        public const int DEFAULT_DISTANCE = 999;
        public const int DefaultWeight = 5;
        public const int MAP_SIZE = 14;
        public const int MaxWeight = 50;
        public const int NB_CELL = 560;

        private short _cellid;

        //public int gfxCount { get; set; }
        //public uint firstGfx { get; set; }
        private int _speed;

        public CellInfo()
        {
            X = -1;
            Y = -1;
            CellId = -1;
            //color = Color.Empty;
            IsInPath = false;
            //isInPath2 = false;
            IsWalkable = false;
            IsCombatWalkable = false;
            AllowLOS = false;
            Speed = 1;
            DistanceSteps = DEFAULT_DISTANCE;
            SubMapId = 0;
        }

        public CellInfo(Cell _cell)
        {
            Cell = _cell;
            X = _cell.X;
            Y = _cell.Y;
            _cellid = _cell.Id;
            //color = DefaultColor;
            IsInPath = false;
            //isInPath2 = false;
            IsWalkable = _cell.Walkable;
            IsCombatWalkable = !_cell.NonWalkableDuringFight && IsWalkable;
            AllowLOS = _cell.LineOfSight;
            Speed = _cell.Speed;
            DistanceSteps = DEFAULT_DISTANCE;
            SubMapId = 0;
            MapLink = _cell.MapChangeData;
        }

        public bool AllowLOS { get; set; }
        public Cell Cell { get; set; }

        public short CellId
        {
            get { return _cellid; }
            set
            {
                _cellid = value;
                //(cellId Mod 14) + cellId / 28
                if (value < 0) return;

                //x = (value % 14) + (int)((double)value  / 14.0); // Faux
                Y = value%14 - (value - value%28)/28; // OK
                X = (short) (0.5 + value/14.5 + Y*13.5/14.5); // OK.... even if I don't know why

                //Debug.Assert(_cellid == CellIdFromPos(x, y));
                //if (_cellid != CellIdFromPos(x, y))
                //  Debug.Print("CellId:{0} => [{1},{2}] => {3}", _cellid, x, y, CellIdFromPos(x, y));
            }
        }

        // Tmp data, used dirung Path Finding
        public int DistanceSteps { get; set; }
        //public bool isInPath2 { get; set; }
        public bool IsCloseToEnemy { get; set; }
        public bool IsCombatWalkable { get; set; }
        public bool IsDiagonal { get; set; } // Used to put an higher price on diagonals in PathFinding algorithm 
        //public string label { get; set; }
        public bool IsInPath { get; set; }
        public bool? IsOpen { get; set; }
        public bool IsWalkable { get; set; }
        //public Color color { get; set; }
        public byte MapLink { get; set; }

        public short[] Neighbours { get; set; }

        public int Speed
        {
            get { return _speed; }
            set
            {
                _speed = value;
                if (value == 0)
                {
                    Weight = DefaultWeight;
                    //color = DefaultColor;
                }
                else
                {
                    if (Speed < 0)
                    {
                        Weight = DefaultWeight*(1 - Speed);
                        //color = ForestColor;
                        if (Weight > MaxWeight)
                            Weight = MaxWeight;
                    }
                    else if (Speed > 0)
                    {
                        //color = PathColor;
                        Weight = DefaultWeight/(1 + Speed);
                        if (Weight < 1) Weight = 1;
                    }
                }
            }
        }

        public byte SubMapId { get; set; } // 0 = undefined

        //public static readonly Color DefaultColor = Color.Green;
        //public static readonly Color PathColor = Color.LightGreen;
        //public static readonly Color ForestColor = Color.Brown;
        public int Weight { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
        //public CellInfo(short cellId, bool isWalkable = true, bool isCombatWalkable = true, bool allowLOS = true, bool drawable = true, int speed = 1)
        //{
        //  this.X = X;
        //  this.Y = Y;
        //  this.CellId = cellId;
        //  //this.color = DefaultColor;
        //  this.isInPath1 = false;
        //  //this.isInPath2 = false;
        //  this.isWalkable = isWalkable;
        //  this.isCombatWalkable = isCombatWalkable;
        //  this.allowLOS = allowLOS;
        //  this.speed = speed;
        //}


        public static short CellIdFromPos(int x, int y)
        {
            var HighPart = x - y;
            if (HighPart < 0 || HighPart > 39) return CELL_ERROR;
            var LowPart = y + HighPart/2;
            if (LowPart < 0 || LowPart >= MAP_SIZE) return CELL_ERROR;
            var result = HighPart*MAP_SIZE + LowPart;
            if (result >= NB_CELL || result < 0) return CELL_ERROR;
            return (short) result;
        }

        public static Array FillCombo()
        {
            return Enum.GetValues(typeof(FieldNames));
        }


        public short GetNeighbourCell(int dx, int dy)
        {
            return CellIdFromPos(X + dx, Y + dy);
        }

        public string getValue(FieldNames whichElement)
        {
            switch (whichElement)
            {
                case FieldNames.Nothing:
                    return "";
                //case FieldNames.Label:
                //  return label;
                case FieldNames.isInPath:
                    return IsInPath.ToString();
                case FieldNames.isWalkable:
                    return IsWalkable.ToString();
                case FieldNames.isCombatWalkable:
                    return IsCombatWalkable.ToString();
                case FieldNames.allowLOS:
                    return AllowLOS.ToString();
                //case FieldNames.color:
                //  return color.ToString();
                case FieldNames.speed:
                    return Speed.ToString();
                case FieldNames.coordinates:
                    return $"{X},{Y}";
                case FieldNames.PathFindingInformations:
                    return DistanceSteps.ToString();
                case FieldNames.mapLink:
                    return MapLink.ToString();
                case FieldNames.cellID:
                    return CellId.ToString();
                //case FieldNames.firstGfx:
                //  return this.firstGfx.ToString();
                //case FieldNames.gfxCount:
                //  return this.gfxCount.ToString();
                default:
                    return "???";
            }
        }

        public override string ToString()
        {
            if (Cell != null) return Cell.ToString();
            return "<null>";
        }

        public static short XFromId(short cellId)
        {
            return (short) (0.5 + cellId/14.5 + YFromId(cellId)*13.5/14.5);
        }

        public static short YFromId(short cellId)
        {
            return (short) (cellId%14 - (cellId - cellId%28)/28); // OK            
        }
    }
}