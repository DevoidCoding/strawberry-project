﻿// <copyright file="WorldMap.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Strawberry.Common.Game.World.Data;

namespace Strawberry.Common.Game.World.Pathfinding.FFPathFinding
{
    public class WorldMap
    {
        private static WorldMap _instance;

        private static IWorldMapProvider _mapDataManager;
        private int[] InternalToMapId;

        private Dictionary<int, int> MapIdToInternal;

        //private SortedSet<int> _mapsInTheWorld;

        private WorldMap(IWorldMapProvider mapDataManager)
        {
            if (mapDataManager == null)
                throw new ArgumentNullException(nameof(mapDataManager),
                    "You need to provide the instance of an object to retrieve map data");
            var _mapDataManager = mapDataManager;
            WorldMapConnectionsStored = GetWorldMapConnections(_mapDataManager, true);
            WorldMapConnectionsComputed = GetWorldMapConnections(_mapDataManager, false);
        }

        public static IEnumerable<MapNeighbour> EnumDirections
        {
            get { return (IEnumerable<MapNeighbour>) Enum.GetValues(typeof(MapNeighbour)); }
        }

        public static WorldMap Instance
        {
            get
            {
                if (_instance == null) _instance = new WorldMap(_mapDataManager);
                return _instance;
            }
        }

        public TimeSpan ProcessAllMapsTimer { get; private set; }
        public Dictionary<int, List<int>> WorldMapConnectionsComputed { get; }

        public Dictionary<int, List<int>> WorldMapConnectionsStored { get; }

        public Dictionary<int, List<int>> GetWorldMapConnections(bool StoredInMap)
        {
            if (StoredInMap)
                return WorldMapConnectionsStored;
            return WorldMapConnectionsComputed;
        }

        private List<int> GetConnectedMaps(IMap map, bool GetStoredData)
        {
            var list = new List<int>();
            foreach (var dir in EnumDirections)
            {
                var mapId = map.GetNeighbourMapId(dir, GetStoredData);
                if (mapId != -1)
                    if (_mapDataManager.KnownMapIds.Contains(mapId))
                        list.Add(mapId);
            }
            return list;
        }

        private Dictionary<int, List<int>> GetWorldMapConnections(IWorldMapProvider mapDataManager, bool StoredInMap)
        {
            _mapDataManager = mapDataManager;
            var st = new Stopwatch();
            st.Start();
            //MapDataManager mapDataManager = new MapDataManager(pathToMapsData, StoredInMap); // Gets only Headers, except if checking cell content is needed
            var dico = new Dictionary<int, List<int>>(mapDataManager.KnownMapIds.Count);
            foreach (var mapId in mapDataManager.KnownMapIds)
            {
                var mapData = mapDataManager.LoadMap(mapId);
                dico.Add(mapData.Id, GetConnectedMaps(mapData, StoredInMap));
            }
            st.Stop();
            ProcessAllMapsTimer = st.Elapsed;

            // To do : detect submaps
            // To do : find connections between submaps 

            // Convert into 0 based internal index (todo : split sub maps)
            MapIdToInternal = new Dictionary<int, int>();
            var i = 0;
            var tmpInternaltoMapId = new List<int>();
            foreach (var mapId in dico.Keys) // 
            {
                MapIdToInternal[mapId] = i++;
                tmpInternaltoMapId.Add(mapId);
            }
            InternalToMapId = tmpInternaltoMapId.ToArray();
            // Fill converted int[][] to feed the PathFinder
            var mainList = new List<int[]>();
            List<int> connectionList;
            List<int> ConvertedConnectionList;
            var newDico = new Dictionary<int, List<int>>(dico.Count);
            foreach (var map in dico)
            {
                var mapID = map.Key;
                connectionList = map.Value;
                ConvertedConnectionList = new List<int>();
                foreach (var mapId in connectionList)
                    ConvertedConnectionList.Add(MapIdToInternal[mapId]);
                newDico[mapID] = ConvertedConnectionList;
            }
            return newDico;
        }
    }
}