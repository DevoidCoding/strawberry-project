﻿// <copyright file="ISimplePathFinder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

namespace Strawberry.Common.Game.World.Pathfinding
{
    public interface ISimplePathFinder
    {
        Path FindPath(Cell startCell, Cell endCell, bool outsideFight, int mp = -1);
    }
}