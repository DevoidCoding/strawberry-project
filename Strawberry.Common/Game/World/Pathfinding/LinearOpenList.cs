﻿// <copyright file="LinearOpenList.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System;
using System.Collections.Generic;

namespace Strawberry.Common.Game.World.Pathfinding
{
    public class LinearOpenList : IOpenList
    {
        private readonly List<Cell> m_cells = new List<Cell>();
        private readonly IComparer<Cell> m_comparer;

        public LinearOpenList(IComparer<Cell> comparer)
        {
            m_comparer = comparer;
        }

        #region IOpenList Members

        public void Push(Cell cell)
        {
            m_cells.Add(cell);
        }

        public Cell Pop()
        {
            if (m_cells.Count == 0)
                throw new InvalidOperationException("LinearOpenList is empty");

            var bestCell = m_cells[0];
            for (var i = 1; i < m_cells.Count; i++)
            {
                // bestCell has a greater cost than the other cell
                if (m_comparer.Compare(bestCell, m_cells[i]) >= 0)
                    bestCell = m_cells[i];
            }

            m_cells.Remove(bestCell);

            return bestCell;
        }

        public int Count
        {
            get { return m_cells.Count; }
        }

        #endregion
    }
}