﻿// <copyright file="LogOpenList.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 06:57</date>

using System.Collections.Generic;
using Strawberry.Core.Collections;

namespace Strawberry.Common.Game.World.Pathfinding
{
    public class LogOpenList : PriorityQueueB<Cell>, IOpenList
    {
        public LogOpenList(IComparer<Cell> comparer)
            : base(comparer)
        {
        }

        #region IOpenList Members

        void IOpenList.Push(Cell cell)
        {
            Push(cell);
        }

        #endregion
    }
}