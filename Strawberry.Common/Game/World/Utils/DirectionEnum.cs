﻿namespace Strawberry.Common.Game.World.Utils
{
    public enum DirectionEnum
    {
        Invalid = -1,
        East = 0,
        SouthEast = 1,
        South = 2,
        SouthWest = 3,
        West = 4,
        NorthWest = 5,
        North = 6,
        NorthEast = 7
    }

    public static class DirectionsExtensions
    {
        public static bool IsValidDirection(this DirectionEnum direction)
            => direction != DirectionEnum.Invalid;
    }
}