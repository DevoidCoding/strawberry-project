﻿namespace Strawberry.Common.Game.World.Utils
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Strawberry.Core.IO;
    using Strawberry.Core.Reflection;

    public class WorldGraph : Singleton<WorldGraph>
    {
        #region Public Properties

        public Dictionary<string, Edge> Edges { get; } = new Dictionary<string, Edge>();

        public Dictionary<Vertex, List<Edge>> OutgoingEdges { get; } = new Dictionary<Vertex, List<Edge>>();

        public Dictionary<string, Vertex> Vertices { get; } = new Dictionary<string, Vertex>();

        #endregion

        #region Public Methods and Operators

        public void Initialize(string filepath)
        {
            var reader = new BigEndianReader(File.ReadAllBytes(filepath));

            var edgeCount = reader.ReadInt();

            for (var i = 0; i < edgeCount; i++)
            {
                var from = AddVertex(reader.ReadDouble(), reader.ReadInt());
                var to = AddVertex(reader.ReadDouble(), reader.ReadInt());
                var edge = AddEdge(from, to);

                var transitionCount = reader.ReadInt();

                for (var j = 0; j < transitionCount; j++)
                {
                    var type = reader.ReadSByte();
                    edge.AddTransition((DirectionEnum)type, (TransitionTypeEnum)reader.ReadSByte(), reader.ReadInt(), reader.ReadUTFBytes(reader.ReadInt()), reader.ReadDouble(), reader.ReadInt(), reader.ReadDouble());
                }
            }

            var t = Edges.Values.SelectMany(e => e.Transitions).Where(e => e.Type.ToString().Contains(","));
            var f = Edges.Values.Where(e => e.Transitions.Any(a => a.Type == TransitionTypeEnum.Unspecified));
        }

        #endregion

        #region Methods

        private Edge AddEdge(Vertex from, Vertex to)
        {
            if (Edges.TryGetValue(Edge.GetInternalId(from, to), out var edge)) return edge;
            if (!Vertices.ContainsValue(from) | !Vertices.ContainsValue(to)) return null;

            edge = new Edge(from, to);
            Edges.Add(edge.InternalId, edge);

            if (!OutgoingEdges.ContainsKey(from))
                OutgoingEdges.Add(from, new List<Edge> { edge });

            OutgoingEdges[from].Add(edge);
            return edge;
        }

        private Vertex AddVertex(double mapId, int zoneId)
        {
            var key = Vertex.GetUid(mapId, zoneId);
            if (Vertices.TryGetValue(key, out var vertex)) return vertex;

            vertex = new Vertex(mapId, zoneId);
            Vertices.Add(key, vertex);
            return vertex;
        }

        #endregion
    }
}