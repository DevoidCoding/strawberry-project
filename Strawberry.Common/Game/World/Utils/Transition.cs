﻿namespace Strawberry.Common.Game.World.Utils
{
    public class Transition
    {
        public Transition(TransitionTypeEnum type, DirectionEnum direction, int skillId, string criterion,
            double transitionMapId, int cell, double id)
        {
            Type = type;
            Direction = direction;
            SkillId = skillId;
            Criterion = criterion;
            TransitionMapId = transitionMapId;
            Cell = cell;
            Id = id;
        }

        public int Cell { get; }
        public string Criterion { get; }
        public DirectionEnum Direction { get; }
        public double Id { get; }
        public int SkillId { get; }
        public double TransitionMapId { get; }
        public TransitionTypeEnum Type { get; }

        public override string ToString()
            => $"Tansition ({Id}, {Type}): Direction = {Direction}, SkillId = {SkillId}, Criterion = {Criterion}, MapId = {TransitionMapId}, Cell = {Cell}";
    }
}