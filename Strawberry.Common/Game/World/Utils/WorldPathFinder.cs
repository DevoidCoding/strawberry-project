﻿namespace Strawberry.Common.Game.World.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    public class WorldPathFinder
    {
        #region Static Fields

        private static bool isInitialized;

        #endregion

        #region Fields

        private Vertex from;

        private int linkedZone;

        private int to;

        private Bot bot;

        private PlayedCharacter character;

        #endregion

        #region Constructors and Destructors

        public WorldPathFinder(PlayedCharacter character)
        {
            bot = character.Bot;
            this.character = character;
            character.MapJoined += CharacterOnMapJoined;
            AStar.Instance.Completed += OnAStarCompleted;
        }

        private void CharacterOnMapJoined(PlayedCharacter character, Map map)
        {
            if (!IsTravelling) return;

            NextStage();
        }

        #endregion

        #region Delegates

        public delegate void PathEndedHandler();

        public delegate void PathFoundHandler(IEnumerable<Edge> nodes);

        #endregion

        #region Public Events

        public event PathEndedHandler PathEnded;

        public event PathFoundHandler PathFound;

        #endregion

        #region Public Properties

        public static bool IsTravelling { get; set; }

        public static IList<Edge> Nodes { get; set; }

        public static int StageIndex { get; set; }

        #endregion

        #region Public Methods and Operators

        public static void Initialize(string filepath)
        {
            if (isInitialized) return;

            WorldGraph.Instance.Initialize(filepath);
            isInitialized = true;
        }

        public void Move(int mapId)
        {
            var zoneId = bot.Character.Cell.LinkedZoneRP;

            if (!WorldGraph.Instance.Vertices.TryGetValue(Vertex.GetUid(bot.Character.Map.Id, zoneId), out from))
            {
                OnPathFound(null);
                return;
            }

            linkedZone = 1;
            to = mapId;
            Next();
        }

        public void Stop()
            => IsTravelling = false;

        #endregion

        #region Methods

        protected virtual void OnPathEnded()
            => PathEnded?.Invoke();

        protected virtual void OnPathFound(IEnumerable<Edge> nodes)
        {
            if (nodes == null)
                return;

            PathFound?.Invoke(nodes);

            var enumerable = nodes.ToList();

            if (!enumerable.Any())
            {
                IsTravelling = false;
                return;
            }
            
            Nodes = enumerable.ToList();
            StageIndex = 0;
            IsTravelling = true;
            NextStage();
        }

        private void Next()
        {
            if (!WorldGraph.Instance.Vertices.TryGetValue(Vertex.GetUid(to, linkedZone++), out var dest))
            {
                OnPathFound(null);
                return;
            }

            AStar.Instance.Search(from, dest);
        }

        private void NextStage()
        {
            if (StageIndex >= Nodes.Count)
            {
                IsTravelling = false;
                OnPathEnded();
                return;
            }

            var node = Nodes[StageIndex++];
            var transition = node.Transitions.First();

            switch (transition.Type)
            {
                case TransitionTypeEnum.Scroll:
                case TransitionTypeEnum.ScrollAction:
                    bot.CallDelayed(300, () => character.ChangeMap(character.Map.Id, (short)transition.Cell, (int)transition.TransitionMapId, 1));
                    break;
                case TransitionTypeEnum.MapEvent:
                    break;
                case TransitionTypeEnum.MapAction:
                    break;
                case TransitionTypeEnum.MapObstacle:
                    break;
                case TransitionTypeEnum.Interactive:
                    break;
                case TransitionTypeEnum.NpcAction:
                    break;
                case TransitionTypeEnum.Unspecified:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnAStarCompleted(IEnumerable<Edge> path)
        {
            if (path == null)
                Next();
            else OnPathFound(path);
        }

        #endregion
    }
}