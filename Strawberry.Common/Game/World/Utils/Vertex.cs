﻿namespace Strawberry.Common.Game.World.Utils
{
    public class Vertex
    {
        public Vertex(double mapId, int zoneId)
        {
            MapId = mapId;
            ZoneId = zoneId;
        }

        public double MapId { get; }
        public string Uid => GetUid(MapId, ZoneId);
        public int ZoneId { get; }

        public static double GetMapId(string vertexUid)
            => int.Parse(vertexUid.Split('|')[0]);

        public static string GetUid(double mapId, int zoneId)
            => $"{mapId}|{zoneId}";

        public static int GetZoneId(string vertexUid)
            => int.Parse(vertexUid.Split('|')[1]);

        public override string ToString()
            => $"Vertex ({MapId}, {ZoneId})";
    }
}