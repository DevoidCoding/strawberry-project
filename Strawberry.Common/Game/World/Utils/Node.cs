﻿using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.World.Utils
{
    public class Node
    {
        public Node(Vertex vertex, MapPosition map, int cost = 0, int heuristic = 0, Node parent = null)
        {
            Vertex = vertex;
            Map = map;
            Cost = cost;
            Heuristic = heuristic;
            Parent = parent;
        }

        public int Cost { get; }
        public int Heuristic { get; }
        public MapPosition Map { get; }
        public Node Parent { get; }
        public Vertex Vertex { get; }
    }
}