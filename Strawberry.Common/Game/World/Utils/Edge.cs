﻿using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.World.Utils
{
    public class Edge
    {
        public Edge(Vertex from, Vertex to)
        {
            From = from;
            To = to;
        }

        public Vertex From { get; }
        public Vertex To { get; }
        public string InternalId => GetInternalId(From, To);

        public static string GetInternalId(Vertex from, Vertex to)
            => $"{from.Uid}|{to.Uid}";

        public IList<Transition> Transitions { get; } = new List<Transition>();

        // Note : Direction & Type are inversed
        public void AddTransition(DirectionEnum direction, TransitionTypeEnum type, int skillId, string criterion,
            double transitionMapId, int cell, double id)
            => AddTransition(new Transition(type, direction, skillId, criterion, transitionMapId, cell, skillId));

        public void AddTransition(Transition transition)
            => Transitions.Add(transition);

        public IEnumerable<Transition> GetTransitionWithValidDirections()
            => Transitions.Where(e => e.Direction != DirectionEnum.Invalid);

        public override string ToString()
            => $"Edge: From : {From}" +
               $"      To   : {To}" +
               $"      Transitions : {string.Join(", ", Transitions)}";

        public bool HasValidTransition()
        {
            // TODO : Criterions
            return true;
        }
    }
}