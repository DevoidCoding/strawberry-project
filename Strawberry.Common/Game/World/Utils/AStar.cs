﻿namespace Strawberry.Common.Game.World.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Data.D2O;
    using Strawberry.Core.Reflection;
    using Strawberry.Protocol.Data;

    public class AStar : Singleton<AStar>
    {
        #region Constants

        private const int HeuristicScale = 1;

        private const int IndoorWeight = 0;

        private const int MaxIteration = 10000;

        #endregion

        #region Fields

        private Dictionary<Vertex, Node> _closedDictionary;

        private MapPosition _dest;

        private int _iterations;

        private Dictionary<Vertex, Node> _openDictionary;

        private Queue<Node> _openList;

        private Vertex _to;

        #endregion

        #region Delegates

        public delegate void CompletedHandler(IEnumerable<Edge> path);

        #endregion

        #region Public Events

        public event CompletedHandler Completed;

        #endregion

        #region Public Methods and Operators

        public void Search(Vertex from, Vertex to)
        {
            if (from == to)
                return;

            _to = to;
            _dest = ObjectDataManager.Instance.Get<MapPosition>((uint)to.MapId);
            _closedDictionary = new Dictionary<Vertex, Node>();
            _openList = new Queue<Node>();
            _openDictionary = new Dictionary<Vertex, Node>();
            _iterations = 0;
            _openList.Enqueue(new Node(from, ObjectDataManager.Instance.Get<MapPosition>((uint)from.MapId)));
            Compute();
        }

        #endregion

        #region Methods

        private static IEnumerable<Edge> BuildPath(Node node)
        {
            var result = new List<Edge>();

            while (node.Parent != null)
            {
                result.Add(WorldGraph.Instance.Edges[Edge.GetInternalId(node.Parent.Vertex, node.Vertex)]);
                node = node.Parent;
            }

            result.Reverse();
            return result;
        }

        private static int OrderNodes(Node x, Node y)
            => x.Heuristic == y.Heuristic ? 0 : x.Heuristic > y.Heuristic ? 1 : -1;

        private void Compute()
        {
            for (var start = DateTime.Now; _openList.Count > 0;)
            {
                if (_iterations++ > MaxIteration) return;

                var current = _openList.Dequeue();
                _openDictionary.Remove(current.Vertex);

                if (current.Vertex == _to)
                {
                    RaiseCompleted(BuildPath(current));
                    return;
                }

                var edges = WorldGraph.Instance.OutgoingEdges[current.Vertex];
                var oldLenght = _openList.Count;
                var cost = current.Cost + 1;

                foreach (var edge in edges)
                {
                    if (!edge.HasValidTransition()) continue;
                    // TODO : Check is SubArea is blacklisted

                    if (_closedDictionary.TryGetValue(edge.To, out var existing) && cost >= existing.Cost) continue;
                    if (_openDictionary.TryGetValue(edge.To, out existing) && cost >= existing.Cost) continue;

                    var map = ObjectDataManager.Instance.Get<MapPosition>((uint)edge.To.MapId);
                    if (map == null) continue;

                    var manhattanDistance = Math.Abs(map.PosX - _dest.PosX) + Math.Abs(map.PosY - _dest.PosY);

                    var node = new Node(edge.To, map, cost, cost + HeuristicScale * manhattanDistance + (current.Map.Outdoor && !map.Outdoor ? IndoorWeight : 0), current);

                    _openList.Enqueue(node);
                    _openDictionary[node.Vertex] = node;
                }

                _closedDictionary[current.Vertex] = current;

                if (oldLenght >= _openList.Count) continue;

                var list = _openList.ToList();
                list.Sort(OrderNodes);
                _openList = new Queue<Node>(list);
            }
        }

        private void RaiseCompleted(IEnumerable<Edge> edges)
            => Completed?.Invoke(edges);

        #endregion
    }
}