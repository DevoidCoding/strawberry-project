﻿namespace Strawberry.Common.Game.Sequences
{
    using System;

    using Strawberry.Protocol.Messages;

    public sealed class Sequence
    {
        #region Fields

        private readonly Bot bot;

        private ushort sequence;

        #endregion

        #region Constructors and Destructors

        public Sequence(Bot bot)
            => this.bot = bot ?? throw new ArgumentNullException(nameof(bot));

        #endregion

        #region Delegates

        public delegate void SequenceRequestedHandler(Bot bot);

        #endregion

        #region Public Events

        public event SequenceRequestedHandler SequenceRequested;

        #endregion

        #region Public Methods and Operators

        public void Update(SequenceNumberRequestMessage message)
        {
            sequence++;
            OnSequenceRequested();
        }

        #endregion

        #region Methods

        private void OnSequenceRequested()
        {
            SequenceRequested?.Invoke(bot);
            bot.SendToServer(new SequenceNumberMessage(sequence));
        }

        #endregion
    }
}