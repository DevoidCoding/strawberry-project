﻿namespace Strawberry.Common.Game.TreasureHunt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.TreasureHunt.Steps;
    using Strawberry.Common.Game.World;
    using Strawberry.Core.Reflection;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;

    public class TreasureHunt
    {
        #region Fields

        private readonly PlayedCharacter character;

        private bool isStarted;

        #endregion

        #region Constructors and Destructors

        public TreasureHunt(PlayedCharacter character)
            => this.character = character;

        #endregion

        #region Delegates

        public delegate void FinishedEventHandler(TreasureHunt hunt, Bot bot);

        public delegate void FlaggedEventHandler(TreasureHunt hunt, Bot bot);

        public delegate void StartedEventHandler(TreasureHunt hunt, Bot bot);

        public delegate void StartHuntingEventHandler(TreasureHunt hunt, Bot bot);

        public delegate void UpdatedEventHandler(TreasureHunt hunt, Bot bot);

        #endregion

        #region Public Events

        public event FinishedEventHandler Finished;

        public event FlaggedEventHandler Flagged;

        public event StartedEventHandler Started;

        public event StartHuntingEventHandler StartHunting;

        public event UpdatedEventHandler Updated;

        #endregion

        #region Public Properties

        public Step ActualStep => Steps?.LastOrDefault();

        public IEnumerable<Flag> Flags { get; private set; }

        public bool IsHunting { get; private set; }

        public TreasureHuntTypeEnum QuestType { get; private set; }

        public Map StartMap { get; private set; }

        public IEnumerable<Step> Steps { get; private set; }

        public sbyte TotalStep { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Dig()
            => character.Bot.SendToServer(new TreasureHuntDigRequestMessage((sbyte)QuestType));

        public void Flag()
        {
            if (Flags.Count() == TotalStep) throw new Exception("No more steps to flag");
            character.Bot.SendToServer(new TreasureHuntFlagRequestMessage((sbyte)QuestType, (sbyte)Flags.Count()));
            OnFlagged();
        }

        public void Update(MapComplementaryInformationsDataMessage message)
        {
            if (!isStarted) return;
            if (message.MapId == StartMap.Id) OnStartHunting();
        }

        #endregion

        #region Methods

        internal void Update(TreasureHuntRequestAnswerMessage message)
        {
            QuestType = (TreasureHuntTypeEnum)message.QuestType;

            if ((TreasureHuntRequestEnum)message.Result == TreasureHuntRequestEnum.TreasureHuntOk)
                OnStarted();
        }

        internal void Update(TreasureHuntFinishedMessage message)
            => OnStopped();

        internal void Update(TreasureHuntMessage message)
        {
            StartMap = new Map((long)message.StartMapId);
            QuestType = (TreasureHuntTypeEnum)message.QuestType;
            Steps = ProtocolConverter.Instance.Convert<Step>(message.KnownStepsList);
            Flags = ProtocolConverter.Instance.Convert<Flag>(message.Flags);
            TotalStep = message.TotalStepCount;

            if (!isStarted)
                OnStarted();

            if (Steps.Count() > 1 || message.CheckPointCurrent > 1)
                IsHunting = true;

            OnUpdated();
        }

        protected virtual void OnFlagged()
            => Flagged?.Invoke(this, character.Bot);

        protected virtual void OnStarted()
        {
            isStarted = true;
            Started?.Invoke(this, character.Bot);
        }

        protected virtual void OnStartHunting()
        {
            IsHunting = true;
            StartHunting?.Invoke(this, character.Bot);
        }

        protected virtual void OnStopped()
        {
            isStarted = false;
            IsHunting = false;
            Finished?.Invoke(this, character.Bot);
        }

        protected virtual void OnUpdated()
            => Updated?.Invoke(this, character.Bot);

        #endregion
    }
}