﻿namespace Strawberry.Common.Game.TreasureHunt.Steps
{
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Types;

    public class FollowDirectionStep : Step
    {
        #region Constructors and Destructors

        public FollowDirectionStep(TreasureHuntStepFollowDirection step)
        {
            Direction = (DirectionsEnum)step.Direction;
            MapCount = step.MapCount;
        }

        #endregion

        #region Public Properties

        public DirectionsEnum Direction { get; }

        public ushort MapCount { get; }

        #endregion
    }
}