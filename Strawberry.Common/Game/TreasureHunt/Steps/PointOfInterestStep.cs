﻿namespace Strawberry.Common.Game.TreasureHunt.Steps
{
    using Strawberry.Common.Data.D2O;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Types;

    public class PointOfInterestStep : Step
    {
        #region Constructors and Destructors

        public PointOfInterestStep(TreasureHuntStepFollowDirectionToPOI step)
        {
            Direction = (DirectionsEnum)step.Direction;
            PointOfInterest = ObjectDataManager.Instance.Get<PointOfInterest>(step.PoiLabelId);
        }

        #endregion

        #region Public Properties

        public DirectionsEnum Direction { get; }

        public PointOfInterest PointOfInterest { get; }

        #endregion
    }
}