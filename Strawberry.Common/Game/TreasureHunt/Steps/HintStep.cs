﻿namespace Strawberry.Common.Game.TreasureHunt.Steps
{
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Types;

    public class HintStep : Step
    {
        #region Constructors and Destructors

        public HintStep(TreasureHuntStepFollowDirectionToHint step)
        {
            Direction = (DirectionsEnum)step.Direction;
            NpcId = step.NpcId;
        }

        #endregion

        #region Public Properties

        public DirectionsEnum Direction { get; }

        public ushort NpcId { get; }

        #endregion
    }
}