﻿namespace Strawberry.Common.Game.TreasureHunt
{
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Types;

    public class Flag
    {
        #region Constructors and Destructors

        public Flag(TreasureHuntFlag flag)
        {
            MapId = flag.MapId;
            State = (TreasureHuntFlagStateEnum)flag.State;
        }

        #endregion

        #region Public Properties

        public double MapId { get; }

        public TreasureHuntFlagStateEnum State { get; }

        #endregion
    }
}