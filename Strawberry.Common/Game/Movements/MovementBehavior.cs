﻿using System;
using System.ComponentModel;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Movements
{
    public class MovementBehavior : INotifyPropertyChanged
    {
        public static VelocityConfiguration BenchmarkMovementBehavior = new VelocityConfiguration(0.00392157, 0.00470588,
            0.00588235);

        /*public delegate void PathCompletedHandler(MovementBehavior path, ContextActor actor);

        public event PathCompletedHandler Completed;

        public void NotifyCompleted(ContextActor actor)
        {
            PathCompletedHandler handler = Completed;
            if (handler != null) handler(this, actor);
        }*/


        public static VelocityConfiguration FantomMovementBehavior = new VelocityConfiguration(0.000980392, 0.00117647,
            0.000147059);

        public static VelocityConfiguration MountedMovementBehavior = new VelocityConfiguration(0.005, 0.00833333,
            00740741);

        public static VelocityConfiguration ParableMovementBehavior = new VelocityConfiguration(0.002, 0.00222222,
            0.0025);

        public static VelocityConfiguration RunningMovementBehavior = new VelocityConfiguration(0.00392157, 0.00666667,
            0.00588235);

        public static VelocityConfiguration SlideMovementBehavior = new VelocityConfiguration(0.0117647, 0.02, 0.0176471);

        public static VelocityConfiguration WalkingMovementBehavior = new VelocityConfiguration(0.00196078, 0.00235294,
            0.00208333);

        public MovementBehavior(Path path, VelocityConfiguration velocityConfiguration)
        {
            MovementPath = path;
            VelocityConfiguration = velocityConfiguration;
        }

        public bool Canceled { get; private set; }

        public double CurrentVelocity => TimedPath.GetCurrentVelocity();

        public Cell EndCell => MovementPath.End;


        public DirectionsEnum EndDirection => MovementPath.GetEndCellDirection();

        public DateTime EndTime => TimedPath.Elements[TimedPath.Elements.Count - 1].EndTime;

        public Path MovementPath { get; }

        public Cell StartCell => MovementPath.Start;

        public DateTime StartTime { get; private set; }

        public TimedPath TimedPath { get; private set; }

        public VelocityConfiguration VelocityConfiguration { get; }

        public void Cancel()
        {
            Canceled = true;
        }

        public bool IsEnded()
        {
            return DateTime.Now >= EndTime || Canceled;
        }

        public void Start()
        {
            Start(DateTime.Now);
        }

        public void Start(DateTime startTime)
        {
            StartTime = startTime;
            TimedPath = TimedPath.Create(MovementPath,
                VelocityConfiguration.HorizontalVelocity,
                VelocityConfiguration.VerticalVelocity,
                VelocityConfiguration.LinearVelocity,
                StartTime);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}