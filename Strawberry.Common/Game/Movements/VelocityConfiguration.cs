﻿// <copyright file="VelocityConfiguration.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Movements
{
    public class VelocityConfiguration
    {
        public VelocityConfiguration(double horizontalVelocity, double verticalVelocity, double linearVelocity)
        {
            HorizontalVelocity = horizontalVelocity;
            VerticalVelocity = verticalVelocity;
            LinearVelocity = linearVelocity;
        }

        public double HorizontalVelocity { get; private set; }

        public double LinearVelocity { get; private set; }

        public double VerticalVelocity { get; private set; }
    }
}