﻿// <copyright file="TimedPath.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Movements
{
    public class TimedPath
    {
        private readonly TimedPathElement[] _path;

        public TimedPath(TimedPathElement[] path)
        {
            _path = path;
            CheckValidity();
        }

        public ReadOnlyCollection<TimedPathElement> Elements => Array.AsReadOnly(_path);

        public static TimedPath Create(Path path, double hVelocity, double vVelocity, double lVelocity,
            DateTime referenceDate)
        {
            var cells = path.Cells;
            var result = new List<TimedPathElement>();

            var lastCellTime = referenceDate;
            for (var i = 0; i < cells.Length; i++)
            {
                int direction;

                if (i + 1 < cells.Length)
                    direction = (int) cells[i].OrientationToAdjacent(cells[i + 1]);
                else
                    direction = (int) cells[i - 1].OrientationToAdjacent(cells[i]);

                double velocity;
                if (direction%4 == 0)
                    velocity = hVelocity;
                else if (direction%2 == 0)
                    velocity = vVelocity;
                else
                    velocity = lVelocity;

                var end = lastCellTime + TimeSpan.FromMilliseconds(1/velocity);

                result.Add(new TimedPathElement(cells[i], i + 1 < cells.Length ? cells[i + 1] : null, lastCellTime, end,
                    velocity, (DirectionsEnum) direction));

                lastCellTime = end;
            }

            return new TimedPath(result.ToArray());
        }

        public double GetBaryCenter(DateTime now)
        {
            var elem = GetCurrentElement();

            return elem.Velocity*(now - elem.StartTime).TotalMilliseconds;
        }

        public Cell GetCurrentCell()
        {
            return GetCurrentElement().CurrentCell;
        }

        public TimedPathElement GetCurrentElement()
        {
            for (var i = 0; i < _path.Length; i++)
            {
                if (_path[i].EndTime <= DateTime.Now) continue;

                return i == 0 ? _path[0] : _path[i];
            }

            return _path[_path.Length - 1];
        }

        public double GetCurrentVelocity()
        {
            return GetCurrentElement().Velocity;
        }

        public override string ToString()
        {
            return string.Join(", ",
                _path.Select(entry => entry.CurrentCell.Id + "@" + entry.StartTime.ToString("mm:ss.FFFF")));
        }

        private void CheckValidity()
        {
            if (_path.Where((t, i) => i > 0 && _path[i - 1].EndTime > t.EndTime).Any())
            {
                throw new Exception("Incorrect timed path, the dates are not correctly sorted");
            }
        }
    }
}