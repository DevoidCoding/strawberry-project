﻿// <copyright file="TimedPathElement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Movements
{
    public class TimedPathElement
    {
        public TimedPathElement(Cell currentCell, Cell nextCell, DateTime startTime, DateTime endTime, double velocity,
            DirectionsEnum direction)
        {
            CurrentCell = currentCell;
            NextCell = nextCell;
            StartTime = startTime;
            EndTime = endTime;
            Velocity = velocity;
            Direction = direction;
        }

        public Cell CurrentCell { get; private set; }

        public DirectionsEnum Direction { get; private set; }

        public DateTime EndTime { get; private set; }

        public Cell NextCell { get; private set; }

        public DateTime StartTime { get; private set; }

        public double Velocity { get; private set; }
    }
}