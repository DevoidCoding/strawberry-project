﻿// <copyright file="BotChatMessage.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.ComponentModel;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Chat
{
    public abstract class BotChatMessage : Message, INotifyPropertyChanged
    {
        public ChatActivableChannelsEnum Channel { get; set; }

        // note : I have to encapsulate this protocol part because ChatAbstractServerMessage and ChatAbstractClientMessage
        // are not bound, and this is not good

        public string Content { get; set; }

        // todo
        public object Items { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}