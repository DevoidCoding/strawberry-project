﻿// <copyright file="BotChatMessageServer.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Linq;
using Strawberry.Common.Game.Actors;
using Strawberry.Common.Game.Actors.Interfaces;
using Strawberry.Common.Game.World;
using Strawberry.Core.Extensions;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Chat
{
    public class BotChatMessageServer : BotChatMessage
    {
        public BotChatMessageServer()
        {
        }

        public BotChatMessageServer(ChatServerMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Content = message.Content;
            Channel = (ChatActivableChannelsEnum) message.Channel;
            SentTime = message.Timestamp.UnixTimestampToDateTime();
            FingerPrint = message.Fingerprint;
            SenderId = message.SenderId;
            SenderName = message.SenderName;
            SenderAccountId = message.SenderAccountId;
        }

        public BotChatMessageServer(ChatServerCopyMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Content = message.Content;
            Channel = (ChatActivableChannelsEnum) message.Channel;
            SentTime = message.Timestamp.UnixTimestampToDateTime();
            ReceiverId = message.ReceiverId;
            ReceiverName = message.ReceiverName;
            FingerPrint = message.Fingerprint;
            SenderId = ReceiverId;
            SenderName = ReceiverName;
            Copy = true;
        }

        public BotChatMessageServer(ChatAdminServerMessage message)
            : this((ChatServerMessage) message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Admin = true;
        }

        public bool Admin { get; set; }

        public bool Copy { get; set; }

        public string FingerPrint { get; set; }

        public double ReceiverId { get; set; }

        public string ReceiverName { get; set; }

        public int SenderAccountId { get; set; }

        public double SenderId { get; set; }

        public string SenderName { get; set; }

        public DateTime SentTime { get; set; }

        public ContextActor TryGetSender(IMapContext context)
        {
            return
                context.Actors.FirstOrDefault(
                    entry => (entry is INamed && ((INamed) entry).Name == SenderName) || entry.Id == SenderId);
        }
    }
}