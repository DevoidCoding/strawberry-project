﻿// <copyright file="BotChatMessageClient.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Chat
{
    public class BotChatMessageClient : BotChatMessage
    {
        public BotChatMessageClient()
        {
        }

        public BotChatMessageClient(ChatClientPrivateMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Content = message.Content;
            ReceiverName = message.Receiver;
            Channel = ChatActivableChannelsEnum.PseudoChannelPrivate;
        }

        public BotChatMessageClient(ChatClientMultiMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Content = message.Content;
            Channel = (ChatActivableChannelsEnum) message.Channel;
        }

        public string ReceiverName { get; set; }
    }
}