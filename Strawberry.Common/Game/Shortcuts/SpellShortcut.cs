﻿// <copyright file="SpellShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.Spells;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class SpellShortcut : Shortcut
    {
        public SpellShortcut(PlayedCharacter character, ShortcutSpell shortcut)
            : base(character, shortcut.Slot)
        {
            Character = character;
            SpellId = shortcut.SpellId;
        }

        public int SpellId { get; private set; }

        public Spell GetSpell()
        {
            return Character.SpellsBook.GetSpell(SpellId);
        }

        public void Update(Protocol.Types.Shortcut shortcut)
        {
            Slot = shortcut.Slot;
            var spell = shortcut as ShortcutSpell;
            if (spell == null) return;

            SpellId = spell.SpellId;
        }
    }
}