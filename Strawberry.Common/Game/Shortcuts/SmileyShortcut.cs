﻿// <copyright file="SmileyShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class SmileyShortcut : GeneralShortcut
    {
        public SmileyShortcut(PlayedCharacter character, ShortcutSmiley smiley)
            : base(character, smiley.Slot)
        {
            SmileyId = smiley.SmileyId;
        }

        public int SmileyId { get; private set; }

        public override void Update(Protocol.Types.Shortcut shortcut)
        {
            base.Update(shortcut);

            var smiley = shortcut as ShortcutSmiley;
            if (smiley == null) return;

            SmileyId = smiley.SmileyId;
        }
    }
}