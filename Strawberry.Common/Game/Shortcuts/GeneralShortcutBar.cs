﻿// <copyright file="GeneralShortcutBar.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using MoreLinq;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class GeneralShortcutBar : ShortcutBar<GeneralShortcut>
    {
        public GeneralShortcutBar(PlayedCharacter character)
            : base(character)
        {
        }


        public override ShortcutBarEnum BarType => ShortcutBarEnum.GeneralShortcutBar;

        public void Add(ShortcutObjectItem item)
        {
            Add(new ItemShortcut(Character, item));
        }

        public void Add(ShortcutObjectPreset shortcut)
        {
            Add(new ItemPresetShortcut(Character, shortcut));
        }

        public void Add(ShortcutEmote shortcut)
        {
            Add(new EmoteShortcut(Character, shortcut));
        }

        public void Add(ShortcutSmiley shortcut)
        {
            Add(new SmileyShortcut(Character, shortcut));
        }

        public void Add(Protocol.Types.Shortcut shortcut)
        {
            if (shortcut == null)
                throw new ArgumentNullException(nameof(shortcut));
            if (shortcut is ShortcutObjectItem)
                Add(shortcut as ShortcutObjectItem);
            else if (shortcut is ShortcutObjectPreset)
                Add(shortcut as ShortcutObjectPreset);
            else if (shortcut is ShortcutEmote)
                Add(shortcut as ShortcutEmote);
            else if (shortcut is ShortcutSmiley)
                Add(shortcut as ShortcutSmiley);
        }

        public override void Update(ShortcutBarContentMessage content)
        {
            if (content == null) throw new ArgumentNullException(nameof(content));
            Clear();
            Shortcuts.ForEach(Add);
        }

        public override void Update(ShortcutBarRefreshMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            if ((ShortcutBarEnum) message.BarType != BarType)
                return;

            var shortcut = Get(message.Shortcut.Slot);
            if (shortcut != null)
                shortcut.Update(message.Shortcut);
            else
                Add(message.Shortcut);
        }
    }
}