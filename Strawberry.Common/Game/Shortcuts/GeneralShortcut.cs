﻿// <copyright file="GeneralShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;

namespace Strawberry.Common.Game.Shortcuts
{
    public abstract class GeneralShortcut : Shortcut
    {
        public GeneralShortcut(PlayedCharacter character, int slot)
            : base(character, slot)
        {
        }

        public bool CanUse()
        {
            return false;
        }

        public virtual void Update(Protocol.Types.Shortcut shortcut)
        {
            Slot = shortcut.Slot;
        }

        public bool Use()
        {
            // todo
            return CanUse();
        }
    }
}