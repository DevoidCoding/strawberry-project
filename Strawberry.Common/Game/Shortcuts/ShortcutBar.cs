﻿// <copyright file="ShortcutBar.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Linq;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;

namespace Strawberry.Common.Game.Shortcuts
{
    public abstract class ShortcutBar<T>
        where T : Shortcut
    {
        private readonly ObservableCollectionMT<T> _shortcuts;

        public ShortcutBar(PlayedCharacter character)
        {
            if (character == null) throw new ArgumentNullException(nameof(character));
            Character = character;
            _shortcuts = new ObservableCollectionMT<T>();
            Shortcuts = new ReadOnlyObservableCollectionMT<T>(_shortcuts);
        }

        public abstract ShortcutBarEnum BarType { get; }

        public PlayedCharacter Character { get; protected set; }

        public ReadOnlyObservableCollectionMT<T> Shortcuts { get; }

        public void Add(T shortcut)
        {
            _shortcuts.Add(shortcut);
        }

        public void Clear()
        {
            _shortcuts.Clear();
        }

        public T Get(int slot)
        {
            return Shortcuts.FirstOrDefault(x => x.Slot == slot);
        }

        public bool Remove(T shortcut)
        {
            return _shortcuts.Remove(shortcut);
        }

        public bool Remove(int slot)
        {
            var item = Get(slot);

            return item != null && _shortcuts.Remove(item);
        }

        public abstract void Update(ShortcutBarContentMessage content);
        public abstract void Update(ShortcutBarRefreshMessage message);
    }
}