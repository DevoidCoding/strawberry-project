﻿// <copyright file="EmoteShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class EmoteShortcut : GeneralShortcut
    {
        public EmoteShortcut(PlayedCharacter character, ShortcutEmote shortcut)
            : base(character, shortcut.Slot)
        {
            Emote = ObjectDataManager.Instance.Get<Emoticon>(shortcut.EmoteId);
        }

        public Emoticon Emote { get; private set; }

        public override void Update(Protocol.Types.Shortcut shortcut)
        {
            base.Update(shortcut);

            var emote = shortcut as ShortcutEmote;
            if (emote != null)
                Emote = ObjectDataManager.Instance.Get<Emoticon>(emote.EmoteId);
        }
    }
}