﻿// <copyright file="ItemShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Types;
using Item = Strawberry.Common.Game.Items.Item;

namespace Strawberry.Common.Game.Shortcuts
{
    public class ItemShortcut : GeneralShortcut
    {
        public ItemShortcut(PlayedCharacter character, ShortcutObjectItem shortcut)
            : base(character, shortcut.Slot)
        {
            Item = character.Inventory.GetItem(shortcut);
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Item>(shortcut.ItemGID);
        }

        public Item Item { get; private set; }

        public Protocol.Data.Item Template { get; private set; }

        public override void Update(Protocol.Types.Shortcut shortcut)
        {
            base.Update(shortcut);

            var item = shortcut as ShortcutObjectItem;
            if (item == null) return;

            Item = Character.Inventory.GetItem(item);
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Item>(item.ItemGID);
        }
    }
}