﻿// <copyright file="SpellShortcutBar.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Linq;
using MoreLinq;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class SpellShortcutBar : ShortcutBar<SpellShortcut>
    {
        public SpellShortcutBar(PlayedCharacter character)
            : base(character)
        {
        }

        public override ShortcutBarEnum BarType => ShortcutBarEnum.SpellShortcutBar;

        public void Add(ShortcutSpell shortcut)
        {
            Add(new SpellShortcut(Character, shortcut));
        }

        public override void Update(ShortcutBarContentMessage content)
        {
            if (content == null) throw new ArgumentNullException(nameof(content));
            Clear();
            content.Shortcuts.OfType<ShortcutSpell>().ForEach(Add);
        }

        public override void Update(ShortcutBarRefreshMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            if ((ShortcutBarEnum) message.BarType != BarType)
                return;

            var shortcut = Get(message.Shortcut.Slot);
            if (shortcut != null)
                shortcut.Update(message.Shortcut);
            else
            {
                var spell = message.Shortcut as ShortcutSpell;
                if (spell != null)
                    Add(spell);
            }
        }
    }
}