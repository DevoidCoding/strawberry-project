﻿// <copyright file="Shortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;

namespace Strawberry.Common.Game.Shortcuts
{
    public abstract class Shortcut
    {
        public Shortcut(PlayedCharacter character, int slot)
        {
            Character = character;
            Slot = slot;
        }

        public PlayedCharacter Character { get; protected set; }

        public int Slot { get; protected set; }
    }
}