﻿// <copyright file="ItemPresetShortcut.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Shortcuts
{
    public class ItemPresetShortcut : GeneralShortcut
    {
        public ItemPresetShortcut(PlayedCharacter character, ShortcutObjectPreset shortcut)
            : base(character, shortcut.Slot)
        {
            PresetId = shortcut.PresetId;
        }

        public int PresetId { get; private set; }

        public override void Update(Protocol.Types.Shortcut shortcut)
        {
            base.Update(shortcut);

            var preset = shortcut as ShortcutObjectPreset;
            if (preset == null) return;
            PresetId = preset.PresetId;
        }
    }
}