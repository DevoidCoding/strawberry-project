﻿// <copyright file="AlignmentInformations.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Alignement
{
    public class AlignmentInformations : INotifyPropertyChanged
    {
        public AlignmentInformations()
        {
        }

        public AlignmentInformations(ActorAlignmentInformations informations)
        {
            if (informations == null) throw new ArgumentNullException(nameof(informations));
            AlignmentSide = informations.AlignmentSide;
            AlignmentValue = informations.AlignmentValue;
            AlignmentGrade = informations.AlignmentGrade;
            // CHANGE : Dishonor = informations.dishonor;
            CharacterPower = informations.CharacterPower;
        }

        public AlignmentInformations(ActorExtendedAlignmentInformations informations)
            : this((ActorAlignmentInformations) informations)
        {
            if (informations == null) throw new ArgumentNullException(nameof(informations));
            Extended = true;
            Honor = informations.Honor;
            HonorGradeFloor = informations.HonorGradeFloor;
            HonorNextGradeFloor = informations.HonorNextGradeFloor;
            // CHANGE : PvpEnabled = informations.pvpEnabled;
        }

        public sbyte AlignmentGrade { get; set; }

        public sbyte AlignmentSide { get; set; }

        public sbyte AlignmentValue { get; set; }

        public double CharacterPower { get; set; }

        public ushort Dishonor { get; set; }

        public bool Extended { get; set; }

        public ushort Honor { get; set; }

        public ushort HonorGradeFloor { get; set; }

        public ushort HonorNextGradeFloor { get; set; }

        public bool PvpEnabled { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void TogglePvpEnabled()
        {
            throw new NotImplementedException();
        }


        public void Update(ActorAlignmentInformations informations)
        {
            if (informations == null) throw new ArgumentNullException(nameof(informations));
            AlignmentSide = informations.AlignmentSide;
            AlignmentValue = informations.AlignmentValue;
            AlignmentGrade = informations.AlignmentGrade;
            // CHANGE : Dishonor = informations.dishonor;
            CharacterPower = informations.CharacterPower;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}