﻿// <copyright file="ItemToSellInNpcShop.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.Collections.ObjectModel;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public class ItemToSellInNpcShop : ItemBase
    {
        public ItemToSellInNpcShop(ObjectItemToSellInNpcShop item)
            : base(item.ObjectGID)
        {
            Effects = new ObservableCollectionMT<ObjectEffect>(item.Effects);
            ObjectPrice = item.ObjectPrice;
            BuyCriterion = item.BuyCriterion;
        }

        public string BuyCriterion { get; set; }

        public ObservableCollection<ObjectEffect> Effects { get; set; }

        public ulong ObjectPrice { get; set; }
    }
}