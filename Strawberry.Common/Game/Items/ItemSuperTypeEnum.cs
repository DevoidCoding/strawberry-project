﻿// <copyright file="ItemSuperTypeEnum.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

namespace Strawberry.Common.Game.Items
{
    public enum ItemSuperTypeEnum
    {
        SupertypeUnknown0,
        SupertypeAmulet,
        SupertypeWeapon,
        SupertypeRing,
        SupertypeBelt,
        SupertypeBoots,
        SupertypeUnknown5,
        SupertypeShield,
        SupertypeWeapon7,
        SupertypeUnknown8,
        SupertypeHat,
        SupertypeCape,
        SupertypePet,
        SupertypeDofus,
        SupertypeUnknown13,
        SupertypeQuest,
        SupertypeUnknown15,
        SupertypeUnknown16,
        SupertypeUnknown17,
        SupertypeUnknown18,
        SupertypeUnknown19,
        SupertypeUnknown20,
        SupertypeUnknown21,
        SupertypeUnknown22
    }
}