﻿// <copyright file="ItemToSell.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.Collections.ObjectModel;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public class ItemToSell : ItemBase
    {
        public ItemToSell(ObjectItemToSell item)
            : base(item.ObjectGID)
        {
            Guid = item.ObjectUID;
            Effects = new ObservableCollectionMT<ObjectEffect>(item.Effects);
            Quantity = item.Quantity;
            ObjectPrice = item.ObjectPrice;
        }


        public ObservableCollection<ObjectEffect> Effects { get; set; }

        public uint Guid { get; protected set; }

        public ulong ObjectPrice { get; set; }

        public uint Quantity { get; set; }
    }
}