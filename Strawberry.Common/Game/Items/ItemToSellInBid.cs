﻿// <copyright file="ItemToSellInBid.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public class ItemToSellInBid : ItemToSell
    {
        public ItemToSellInBid(ObjectItemToSellInBid item)
            : base(item)
        {
            UnsoldDelay = item.UnsoldDelay;
        }

        public int UnsoldDelay { get; set; }
    }
}