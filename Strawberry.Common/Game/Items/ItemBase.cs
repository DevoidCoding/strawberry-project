﻿// <copyright file="ItemBase.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.ComponentModel;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Data.Icons;
using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.Items
{
    public class ItemBase : INotifyPropertyChanged
    {
        private string _description;
        private Icon _icon;
        private string _name;
        private ItemType _type;

        internal ItemBase(int templateId)
        {
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Item>(templateId);
        }

        public string Description => _description ?? (_description = I18NDataManager.Instance.ReadText(Template.DescriptionId));

        public Icon Icon => _icon ?? (_icon = IconsManager.Instance.GetIcon(Template.IconId));

        public uint Level => Template.Level;

        public string Name => _name ?? (_name = I18NDataManager.Instance.ReadText(Template.NameId));

        public ItemSuperTypeEnum SuperType => (ItemSuperTypeEnum) Type.SuperTypeId;


        public Protocol.Data.Item Template { get; protected set; }

        public ItemType Type => _type ?? (_type = ObjectDataManager.Instance.Get<ItemType>(Template.TypeId));

        public uint UnityWeight => Template.RealWeight;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}