﻿// <copyright file="Inventory.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NLog;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.Fights;
using Strawberry.Common.Game.Shortcuts;
using Strawberry.Common.Game.World;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public partial class Inventory : INotifyPropertyChanged
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<ItemSuperTypeEnum, CharacterInventoryPositionEnum[]> _itemsPositioningRules
            = new Dictionary<ItemSuperTypeEnum, CharacterInventoryPositionEnum[]>
            {
                {ItemSuperTypeEnum.SupertypeAmulet, new[] {CharacterInventoryPositionEnum.AccessoryPositionAmulet}},
                {ItemSuperTypeEnum.SupertypeWeapon, new[] {CharacterInventoryPositionEnum.AccessoryPositionWeapon}},
                {ItemSuperTypeEnum.SupertypeWeapon7, new[] {CharacterInventoryPositionEnum.AccessoryPositionWeapon}},
                {ItemSuperTypeEnum.SupertypeCape, new[] {CharacterInventoryPositionEnum.AccessoryPositionCape}},
                {ItemSuperTypeEnum.SupertypeHat, new[] {CharacterInventoryPositionEnum.AccessoryPositionHat}},
                {
                    ItemSuperTypeEnum.SupertypeRing,
                    new[]
                    {
                        CharacterInventoryPositionEnum.InventoryPositionRingLeft,
                        CharacterInventoryPositionEnum.InventoryPositionRingRight
                    }
                },
                {ItemSuperTypeEnum.SupertypeBoots, new[] {CharacterInventoryPositionEnum.AccessoryPositionBoots}},
                {ItemSuperTypeEnum.SupertypeBelt, new[] {CharacterInventoryPositionEnum.AccessoryPositionBelt}},
                {ItemSuperTypeEnum.SupertypePet, new[] {CharacterInventoryPositionEnum.AccessoryPositionPets}},
                {
                    ItemSuperTypeEnum.SupertypeDofus,
                    new[]
                    {
                        CharacterInventoryPositionEnum.InventoryPositionDofus1,
                        CharacterInventoryPositionEnum.InventoryPositionDofus2,
                        CharacterInventoryPositionEnum.InventoryPositionDofus3,
                        CharacterInventoryPositionEnum.InventoryPositionDofus4,
                        CharacterInventoryPositionEnum.InventoryPositionDofus5,
                        CharacterInventoryPositionEnum.InventoryPositionDofus6
                    }
                },
                {ItemSuperTypeEnum.SupertypeShield, new[] {CharacterInventoryPositionEnum.AccessoryPositionShield}}
            };

        private Item _lastAddedItem;
        private uint _lastQuantity;
        private readonly ObservableCollectionMT<Item> _items;

        public Inventory(PlayedCharacter owner)
        {
            if (owner == null) throw new ArgumentNullException(nameof(owner));
            Owner = owner;
            _items = new ObservableCollectionMT<Item>();
            Items = new ReadOnlyObservableCollectionMT<Item>(_items);
            AutomaticallyDestroyItemsOnOverload = false;
        }

        public Inventory(PlayedCharacter owner, InventoryContentMessage inventory)
            : this(owner)
        {
            if (inventory == null) throw new ArgumentNullException(nameof(inventory));
            Kamas = inventory.Kamas;

            Update(inventory);
        }

        public ReadOnlyObservableCollectionMT<Item> Items { get; }

        public ulong Kamas { get; private set; }

        public PlayedCharacter Owner { get; set; }

        public uint Weight { get; private set; }

        public uint WeightMax { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanDelete(Item item)
        {
            if (Owner.IsFighting() && Owner.Fight.Phase != FightPhase.Placement)
                return false;

            if (!HasItem(item))
                return false;

            return true;
        }

        public bool CanDrop(Item item)
        {
            if (!(Owner.Context is Map))
                return false;

            if (!HasItem(item))
                return false;

            return true;
        }

        public bool CanEquip(Item item)
        {
            if (!_itemsPositioningRules.ContainsKey(item.SuperType))
                return false;

            return CanMove(item, _itemsPositioningRules[item.SuperType].First(), 1);
        }

        public bool CanMove(Item item, CharacterInventoryPositionEnum position, uint quantity)
        {
            if (Owner.IsFighting() && Owner.Fight.Phase != FightPhase.Placement)
                return false;

            if (!HasItem(item))
            {
                //logger.Error("Cannot move item {0} because the item is not own", item.Name);
                return false;
            }

            if (quantity > item.Quantity)
            {
                //logger.Error("Cannot move item {0} because the moved quantity ({1}) is greater than the actual item quantity", item.Name, quantity, item.Quantity);
                return false;
            }

            if (position != CharacterInventoryPositionEnum.InventoryPositionNotEquiped && quantity != 1)
            {
                //logger.Error("Cannot equip item {0} because the moved quantity > 1", item.Name);
                return false;
            }

            if (position != CharacterInventoryPositionEnum.InventoryPositionNotEquiped &&
                _itemsPositioningRules.ContainsKey(item.SuperType) &&
                !_itemsPositioningRules[item.SuperType].Contains(position))
            {
                //logger.Error("Cannot equip item {0} to {1} because the excepted position is {2}", item.Name, position, _itemsPositioningRules[item.SuperType][0]);
                return false;
            }

            return true;
        }

        public bool CanUse(Item item)
        {
            if (!item.IsUsable)
                return false;

            if (Owner.IsFighting() && Owner.Fight.Phase != FightPhase.Placement)
                return false;

            if (!HasItem(item))
                return false;

            return true;
        }

        public bool Delete(Item item)
        {
            return Delete(item, item.Quantity);
        }

        public bool Delete(Item item, uint quantity)
        {
            if (!CanDelete(item))
                return false;

            Owner.Bot.SendToServer(new ObjectDeleteMessage(item.Guid, quantity));
            return true;
        }

        public bool Drop(Item item)
        {
            return Drop(item, item.Quantity);
        }

        public bool Drop(Item item, uint quantity)
        {
            if (!CanDrop(item))
                return false;

            // todo : check if near cells are free

            Owner.Bot.SendToServer(new ObjectDropMessage(item.Guid, quantity));
            return true;
        }

        public bool Equip(Item item)
        {
            if (!_itemsPositioningRules.ContainsKey(item.SuperType))
            {
                Logger.Error(
                    $"Cannot equip item {item.Name} because the super type {item.SuperType} has no position associated");
                return false;
            }

            var availablePositions = _itemsPositioningRules[item.SuperType].Where(x => !HasItem(x)).ToArray();
            var position = availablePositions.Length == 0
                ? _itemsPositioningRules[item.SuperType].First()
                : availablePositions.First();

            return Move(item, position, 1);
        }

        public Item[] GetEquipedItems()
        {
            return
                Items.Where(x => x.Position != CharacterInventoryPositionEnum.InventoryPositionNotEquiped).ToArray();
        }

        public Item GetEquippedItem(CharacterInventoryPositionEnum position)
        {
            return Items.FirstOrDefault(x => x.Position == position);
        }

        public Weapon GetEquippedWeapon()
        {
            var item = Items.FirstOrDefault(x => x.Position == CharacterInventoryPositionEnum.AccessoryPositionWeapon);
            return item?.Template as Weapon;
        }

        public Item GetItem(uint guid)
        {
            return Items.FirstOrDefault(x => x.Guid == guid);
        }

        public Item GetItem(ShortcutObjectItem shortcut)
        {
            return Items.FirstOrDefault(x => x.Guid == shortcut.ItemUID);
        }

        public Item GetItem(CharacterInventoryPositionEnum position)
        {
            return Items.FirstOrDefault(x => x.Position == position);
        }

        public Item GetItemByTemplate(int templateId)
        {
            return Items.FirstOrDefault(x => x.Template.Id == templateId);
        }

        public IEnumerable<Item> GetItemByTemplateIds(int[] templateIds)
        {
            return _items.Where(item => templateIds.Contains(item.Template.Id));
        }

        public IEnumerable<Item> GetItems(CharacterInventoryPositionEnum position)
        {
            return _items.Where(item => item.Position == position);
        }

        public IEnumerable<Item> GetItems(ItemSuperTypeEnum superType)
        {
            return _items.Where(item => item.SuperType == superType);
        }

        public bool HasItem(uint guid)
        {
            return Items.Any(x => x.Guid == guid);
        }

        public bool HasItem(Item item)
        {
            return HasItem(item.Guid);
        }


        public bool HasItem(CharacterInventoryPositionEnum position)
        {
            return Items.Any(x => x.Position == position);
        }

        public bool Move(Item item, CharacterInventoryPositionEnum position)
        {
            return Move(item, position, item.Quantity);
        }

        public bool Move(Item item, CharacterInventoryPositionEnum position, uint quantity)
        {
            if (!CanMove(item, position, quantity))
                return false;

            Owner.Bot.SendToServer(new ObjectSetPositionMessage(item.Guid, (byte) position, quantity));
            return true;
        }

        public void Update(InventoryContentMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            _items.Clear();

            foreach (var item in msg.Objects)
            {
                _items.Add(new Item(item));
            }
        }


        public void Update(InventoryWeightMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Weight = msg.Weight;
            WeightMax = msg.WeightMax;
        }

        public void Update(ObjectAddedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            AddItem(new Item(msg.Object));
        }

        public void Update(ObjectDeletedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            RemoveItem(msg.ObjectUID);
        }

        public void Update(ObjectModifiedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            var item = GetItem(msg.Object.ObjectUID);

            if (item == null)
                Logger.Warn($"Try to update item {msg.Object.ObjectUID} but item not found !");
            else
                item.Update(msg.Object);
        }

        public void Update(ObjectMovementMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            var item = GetItem(msg.ObjectUID);

            if (item == null)
                Logger.Warn($"Try to update item {msg.ObjectUID} but item not found !");
            else
                item.Update(msg);
        }

        public void Update(ObjectQuantityMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            var item = GetItem(msg.ObjectUID);

            if (item == null)
                Logger.Warn($"Try to update item {msg.ObjectUID} but item not found !");
            else
            {
                _lastAddedItem = item;
                _lastQuantity = msg.Quantity;
                item.Update(msg);
            }
        }

        public void Update(ObjectsAddedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            foreach (var item in msg.Object)
            {
                AddItem(new Item(item));
            }
        }

        public void Update(ObjectsDeletedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            foreach (var item in msg.ObjectUID)
            {
                RemoveItem(item);
            }
        }

        public void Update(ObjectsQuantityMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            foreach (var obj in msg.ObjectsUIDAndQty)
            {
                var item = GetItem(obj.ObjectUID);

                if (item == null)
                    Logger.Warn("Try to update item {0} but item not found !", obj.ObjectUID);
                else
                    item.Update(obj);
            }
        }

        public void Update(ObjectUseMessage msg)
        {
        }

        public void Update(ObjectUseMultipleMessage msg)
        {
        }

        public void Update(ObjectUseOnCellMessage msg)
        {
        }

        public void Update(ObjectUseOnCharacterMessage msg)
        {
        }

        public void Update(SetUpdateMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
        }

        public bool Use(Item item)
        {
            if (!CanUse(item))
                return false;

            Owner.Bot.SendToServer(new ObjectUseMessage(item.Guid));
            return true;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        internal void AddItem(Item item)
        {
            _items.Add(item);
            _lastAddedItem = item;
            _lastQuantity = item.Quantity;
        }

        internal bool RemoveItem(uint guid)
        {
            var item = GetItem(guid);

            return item != null && _items.Remove(item);
        }
    }
}