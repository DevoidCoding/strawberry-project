﻿// <copyright file="Inventory.AI.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.Linq;
using Strawberry.Protocol.Enums;

namespace Strawberry.Common.Game.Items
{
    public partial class Inventory
    {
        public bool AutomaticallyDestroyItemsOnOverload { get; set; }

        /// <summary>
        ///     Returns true if a weapon (and NOT a tool) is already equipped.
        ///     Otherwise, looks for the "best" weapon available, and equips it.
        /// </summary>
        /// <returns></returns>
        public bool EquipBestWeaponIfNeeded()
        {
            var equipped = GetEquippedItem(CharacterInventoryPositionEnum.AccessoryPositionWeapon);
            if (equipped != null && equipped.IsWeapon) return true; // A weapon is allready equipped => No change

            return (GetItems(ItemSuperTypeEnum.SupertypeWeapon)
                .Union(GetItems(ItemSuperTypeEnum.SupertypeWeapon7))
                .OrderByDescending(item => item.Level)
                .Where(item => item.IsWeapon)
                .Select(item => new {item, wpn = item.Template})
                .Where(@t => !@t.wpn.Cursed && !@t.wpn.Etheral)
                .Where(@t => @t.item.CheckCriteria(Owner))
                .Where(@t => Owner.Level >= @t.wpn.Level)
                .Select(@t => @t.item)).Any(Equip);
        }

        public void FixInventoryOverloadIfNeeded()
        {
            var weight = Weight;
            if (weight <= WeightMax || !AutomaticallyDestroyItemsOnOverload) return;

            foreach (
                var item in
                    Items.Where(item => item.IsAutomaticallyDeletable)
                        .OrderBy(item => item.Template.Price/item.UnityWeight))
            {
                if (weight <= WeightMax) return; // Inventory fixed
                var tooMuch = weight - WeightMax;
                if (item.TotalWeight < tooMuch) // Completely delete this item stack
                {
                    Delete(item);
                    weight -= item.TotalWeight;
                }
                else
                {
                    // Only partially delete this stack
                    var nbItemsToDelete = (uint) ((tooMuch + (int) item.UnityWeight - 1)/(int) item.UnityWeight);
                    if (nbItemsToDelete > item.Quantity)
                        nbItemsToDelete = item.Quantity;
                    Delete(item, nbItemsToDelete);
                    weight -= item.UnityWeight*nbItemsToDelete;
                }
            }
        }
    }
}