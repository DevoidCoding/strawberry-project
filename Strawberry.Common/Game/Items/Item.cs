﻿// <copyright file="Item.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.Linq;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.Effects;
using Strawberry.Core.Collections;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public partial class Item : ItemBase
    {
        private readonly ObservableCollectionMT<EffectBase> _effects;


        public Item(ObjectItem item)
            : base(item.ObjectGID)
        {
            Guid = item.ObjectUID;
            _effects = new ObservableCollectionMT<EffectBase>(item.Effects.Select(EffectBase.CreateInstance));
            Effects = new ReadOnlyObservableCollectionMT<EffectBase>(_effects);
            Quantity = item.Quantity;
            Position = (CharacterInventoryPositionEnum) item.Position;
        }

        public ReadOnlyObservableCollectionMT<EffectBase> Effects { get; }


        public uint Guid { get; protected set; }

        public bool IsEquipped => Position != CharacterInventoryPositionEnum.InventoryPositionNotEquiped;

        public bool IsUsable => Template.Usable;

        public CharacterInventoryPositionEnum Position { get; private set; }

        public uint Quantity { get; private set; }

        public uint TotalWeight => UnityWeight*Quantity;

        public void Update(ObjectItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            Guid = item.ObjectUID;
            Template = ObjectDataManager.Instance.Get<Protocol.Data.Item>(item.ObjectGID);

            _effects.Clear();
            foreach (var x in item.Effects.Select(EffectBase.CreateInstance))
            {
                _effects.Add(x);
            }

            Quantity = item.Quantity;
            Position = (CharacterInventoryPositionEnum) item.Position;
        }

        public void Update(ObjectMovementMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Position = (CharacterInventoryPositionEnum) msg.Position;
        }

        public void Update(ObjectQuantityMessage msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Quantity = msg.Quantity;
        }

        public void Update(ObjectItemQuantity msg)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));
            Quantity = msg.Quantity;
        }
    }
}