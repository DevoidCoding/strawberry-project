﻿// <copyright file="Item.AI.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Protocol.Data;

namespace Strawberry.Common.Game.Items
{
    public partial class Item
    {
        public bool IsAutomaticallyDeletable => !IsEquipped &&
                                                !IsUsable &&
                                                SuperType != ItemSuperTypeEnum.SupertypeQuest &&
                                                SuperType != ItemSuperTypeEnum.SupertypeDofus &&
                                                Quantity > 1 &&
                                                Template.Price > 0;

        public bool IsWeapon
        {
            get
            {
                if (SuperType != ItemSuperTypeEnum.SupertypeWeapon && SuperType != ItemSuperTypeEnum.SupertypeWeapon7)
                    return false;
                if (Template.TypeId == 19 || Template.TypeId == 20 || Template.TypeId == 21 || Template.TypeId == 22 ||
                    // Tool
                    Template.TypeId == 99 || // filet de capture
                    Template.TypeId == 83 // pierre d'âme
                    ) return false;
                return Template is Weapon;
            }
        }

        public bool CheckCriteria(PlayedCharacter pc)
        {
            return string.IsNullOrEmpty(Template.Criteria) || pc.CheckCriteria(Template.Criteria);
        }
    }
}