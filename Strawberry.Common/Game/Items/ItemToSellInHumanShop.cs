﻿// <copyright file="ItemToSellInHumanShop.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.Collections.ObjectModel;
using Strawberry.Protocol.Types;

namespace Strawberry.Common.Game.Items
{
    public class ItemToSellInHumanShop : ItemBase
    {
        public ItemToSellInHumanShop(ObjectItemToSellInHumanVendorShop item)
            : base(item.ObjectGID)
        {
            Guid = item.ObjectUID;
            Effects = new ObservableCollection<ObjectEffect>(item.Effects);
            Quantity = item.Quantity;
            ObjectPrice = item.ObjectPrice;
            PublicPrice = item.PublicPrice;
        }

        public ObservableCollection<ObjectEffect> Effects { get; set; }

        public uint Guid { get; protected set; }

        public ulong ObjectPrice { get; set; }

        public ulong PublicPrice { get; set; }

        public uint Quantity { get; set; }
    }
}