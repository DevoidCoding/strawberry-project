﻿// <copyright file="GuildEmblem.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;

namespace Strawberry.Common.Game.Guilds
{
    public class GuildEmblem : INotifyPropertyChanged
    {
        public GuildEmblem()
        {
        }

        public GuildEmblem(Protocol.Types.GuildEmblem emblem)
        {
            if (emblem == null) throw new ArgumentNullException(nameof(emblem));

            SymbolShape = emblem.SymbolShape;
            SymbolColor = emblem.SymbolColor;
            BackgroundShape = emblem.BackgroundShape;
            BackgroundColor = emblem.BackgroundColor;
        }

        public int BackgroundColor { get; private set; }

        public short BackgroundShape { get; private set; }

        public int SymbolColor { get; private set; }

        public ushort SymbolShape { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}