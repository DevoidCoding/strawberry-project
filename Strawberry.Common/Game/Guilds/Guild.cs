﻿// <copyright file="Guild.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System.ComponentModel;

namespace Strawberry.Common.Game.Guilds
{
    public class Guild : INotifyPropertyChanged
    {
        // todo : members, role, methods and so on.

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}