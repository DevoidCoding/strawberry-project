﻿// <copyright file="GuildInformations.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/25/2016 20:40</date>

using System;
using System.ComponentModel;

namespace Strawberry.Common.Game.Guilds
{
    public class GuildInformations : INotifyPropertyChanged
    {
        public GuildInformations()
        {
        }

        public GuildInformations(Protocol.Types.GuildInformations guild)
        {
            if (guild == null) throw new ArgumentNullException(nameof(guild));

            Id = guild.GuildId;
            Name = guild.GuildName;
            Emblem = new GuildEmblem(guild.GuildEmblem);
        }

        public GuildEmblem Emblem { get; private set; }

        public uint Id { get; private set; }

        public string Name { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}