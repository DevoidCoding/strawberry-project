﻿namespace Strawberry.Common.Game.Basic
{
    using System;

    using Strawberry.Core.Messages;
    using Strawberry.Core.Network;
    using Strawberry.Protocol.Messages;

    public class BasicLatency
    {
        #region Constants

        private const int MaxSample = 50;

        #endregion

        #region Fields

        private readonly Bot bot;

        private int messageSent;

        #endregion

        #region Constructors and Destructors

        public BasicLatency(Bot bot)
        {
            this.bot = bot;
            bot.Dispatcher.MessageDispatched += OnMessageDispatched;
        }

        #endregion

        #region Public Methods and Operators

        public void Respond()
        {
            var msg = new BasicLatencyStatsMessage((ushort)bot.DelayFromLastMessage, (ushort)Math.Min(messageSent, MaxSample), MaxSample);
            bot.SendToServer(msg);
        }

        #endregion

        #region Methods

        private void OnMessageDispatched(MessageDispatcher messageDispatcher, Message message)
        {
            if (message.Canceled || !(message is NetworkMessage networkMessage) || networkMessage is BasicLatencyStatsMessage || !networkMessage.Destinations.HasFlag(ListenerEntry.Server)) return;
            messageSent++;
        }

        #endregion
    }
}