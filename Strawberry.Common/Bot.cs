﻿namespace Strawberry.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using GalaSoft.MvvmLight.Ioc;

    using NLog;

    using Strawberry.Common.Authentification;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.Basic;
    using Strawberry.Common.Game.Sequences;
    using Strawberry.Common.Settings;
    using Strawberry.Common.View;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Config;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Network;
    using Strawberry.Core.Threading;

    public class Bot : SelfRunningTaskQueue, IDisposable
    {
        #region Static Fields

        [Configurable("DefaultBotTick", "The interval (ms) between two message dispatching")]
        private static readonly int DefaultBotTick = 100;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly ObservableCollection<IFrame> frames = new ObservableCollection<IFrame>();

        #endregion

        #region Constructors and Destructors

        public Bot(MessageDispatcher messageDispatcher)
            : base(DefaultBotTick)
        {
            Id = -1;
            Dispatcher = messageDispatcher ?? throw new ArgumentNullException(nameof(messageDispatcher));
            ConnectionType = ClientConnectionType.Disconnected;
            ClientInformations = new ClientInformations();
            Display = DisplayState.None;
            Sequence = new Sequence(this);

            Dispatcher.MessageDispatched += OnMessageDispatched;
        }

        #endregion

        #region Delegates

        public delegate void CharacterIdentificationHandler(Bot bot, bool succeeded);

        public delegate void CharacterSelectedHandler(Bot bot, PlayedCharacter character);

        public delegate void LogHandler(Bot bot, LogLevel level, string caller, string message);

        #endregion

        #region Public Events

        public event CharacterIdentificationHandler CharacterIdentified;

        public event CharacterSelectedHandler CharacterSelected;

        public event LogHandler LogNotified;

        #endregion

        #region Public Properties

        public BasicLatency BasicLatency { get; set; }

        public PlayedCharacter Character { get; private set; }

        public ClientInformations ClientInformations { get; set; }

        public ClientConnectionType ConnectionType { get; set; }

        /// <summary>
        ///     Says how many milliseconds elapsed since last message.
        /// </summary>
        public long DelayFromLastMessage => Dispatcher.DelayFromLastMessage;

        public MessageDispatcher Dispatcher { get; }

        public DisplayState Display { get; set; }

        public bool Disposed { get; private set; }

        public ReadOnlyObservableCollection<IFrame> Frames => new ReadOnlyObservableCollection<IFrame>(frames);

        public int Id { get; internal set; }

        public uint InstanceId { get; set; }

        public override string Name
        {
            get => ToString();
            set
            {
            }
        }

        public Sequence Sequence { get; set; }

        public BotSettings Settings { get; private set; }

        #endregion

        #region Public Methods and Operators

        public bool AddFrame(IFrame frame)
        {
            if (HasFrame(frame.GetType()))
                return false;

            frames.Add(frame);
            frame.OnAttached();

            return true;
        }

        public virtual void Dispose()
        {
            if (Disposed)
                return;

            Disposed = true;
            Id = -1;
            Stop();
            foreach (var frame in frames) frame.OnDetached();
            frames.Clear();
            Dispatcher?.Dispose();
            BotManager.Instance.RemoveBot(this);
            Logger.Debug("Bot removed");
        }

        public IFrame GetFrame(Type type)
        {
            var framesArray = frames.Where(entry => entry.GetType().IsAssignableFrom(type)).ToArray();

            if (framesArray.Length > 1)
                Logger.Warn("Found {0} frames of type {1} (1 or 0 expected)", framesArray.Length, type);

            return framesArray.FirstOrDefault();
        }

        public T GetFrame<T>()
        {
            var framesArray = frames.OfType<T>().ToArray();

            if (framesArray.Length > 1)
                Logger.Warn("Found {0} frames of type {1} (1 or 0 expected)", framesArray.Length, typeof(T));

            return framesArray.FirstOrDefault();
        }

        public string GetSkin(string mode, int orientation, int width, int height, int zoom)
        {
            const string Url = "https://static.ankama.com/dofus/renderer/look/";
            var breed = Character.Sex ? Character.Breed.FemaleLook : Character.Breed.MaleLook;
            var sub = string.Join(",", Character.Look.Skins);
            var colors = string.Empty;

            for (var i = 0; i < Character.Look.IndexedColors.Length; i++)
            {
                colors += $"{i + 1}={Character.Look.IndexedColors[i]}";

                if (i == Character.Look.IndexedColors.Length - 1)
                    break;

                colors += ",";
            }

            var s = breed.Split('|')[0] + "|" + sub + "|" + colors + "|" + breed.Split('|')[3];

            var hex = s.ToCharArray().Select(Convert.ToInt32).Aggregate(string.Empty, (current, value) => current + $"{value:X}");

            return $"{Url}{hex}/{mode}/{orientation}/{width}_{height}-{zoom}.png";
        }

        public IBotViewModel GetViewModel()
        {
            var viewmodel = SimpleIoc.Default.GetInstance<IAccountsViewModel>();
            return viewmodel.Accounts.FirstOrDefault(e => e.Bot == this)?.BotViewModel ?? throw new Exception($"{this} ViewModel not found");
        }

        public bool HasFrame(Type type)
            => frames.Any(x => x.GetType().IsAssignableFrom(type));

        public bool HasFrame<T>()
            => HasFrame(typeof(T));

        public void LoadSettings(string path)
        {
            if (Settings != null)
                throw new Exception("Settings already loaded");

            Settings = new BotSettings(path);
            Settings.Load();
        }

        public void NotifyMessageLog(LogLevel level, string caller, string message)
            => LogNotified?.Invoke(this, level, caller, message);

        public void OnCharacterIdentified(bool succeeded)
            => CharacterIdentified?.Invoke(this, succeeded);

        public bool RemoveFrame(IFrame frame)
        {
            if (frame is IViewModel viewModel && viewModel.View is IView view)
                GetViewModel().RemoveView(view);
            if (!frames.Remove(frame)) return false;
            frame.OnDetached();
            return true;
        }

        public bool RemoveFrame<T>()
            => RemoveFrame(typeof(T));

        public bool RemoveFrame(Type type)
        {
            var framesArray = frames.Where(entry => entry.GetType().IsAssignableFrom(type)).ToArray();

            if (framesArray.Length > 1)
                Logger.Warn("Found {0} frames of type {1} (1 or 0 expected)", framesArray.Length, type);

            return framesArray.All(RemoveFrame);
        }

        public void SaveSettings()
            => Settings?.Save();

        public void Send(Message message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Dispatcher.Enqueue(message, this);
        }

        public void Send(NetworkMessage message, ListenerEntry dest)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            message.Destinations = dest;
            message.From = ListenerEntry.Local;

            Dispatcher.Enqueue(message, this);

            if (Disposed)
                Logger.Error("Error the message {0} wont be dispatched because the bot {1} is disposed !", message, this);
            else if (!Running)
                Logger.Warn("Warning, enqueue {0} but the bot is stopped, the message will be processed once the bot {1} restart", message, this);
        }

        public void SendLocal(Message message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            Dispatcher.Enqueue(message, this);

            if (Disposed)
                Logger.Error("Error the message {0} wont be dispatched because the bot {1} is disposed !", message, this);
            else if (!Running)
                Logger.Warn("Warning, enqueue {0} but the bot is stopped, the message will be processed once the bot {1} restart", message, this);
        }

        public void SendToClient(NetworkMessage message, int delay)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            CallDelayed(delay, () => SendToClient(message));
        }

        /// <summary>
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direct">If true it doesn't reprocess the message internally</param>
        public void SendToClient(NetworkMessage message, bool direct = false)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            if (direct) Send(message, ListenerEntry.Client);
            else Send(message, ListenerEntry.Client | ListenerEntry.Local);
        }

        public void SendToServer(NetworkMessage message, int delay)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            CallDelayed(delay, () => SendToServer(message));
        }

        /// <summary>
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direct">If true it doesn't reprocess the message internally</param>
        public void SendToServer(NetworkMessage message, bool direct = false)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            if (direct) Send(message, ListenerEntry.Server);
            else Send(message, ListenerEntry.Server | ListenerEntry.Local);
        }

        public void SetPlayedCharacter(PlayedCharacter character)
        {
            if (Disposed)
                throw new Exception("Bot instance is disposed");

            if (Character != null)
                throw new Exception("Character already selected");

            Character = character ?? throw new ArgumentNullException(nameof(character));
            OnCharacterSelected(character);
            OnPropertyChanged(nameof(Name));
        }

        public override void Start()
        {
            if (Running)
                return;

            if (Disposed)
                throw new Exception("Cannot start a disposed bot instance");

            if (Dispatcher.Stopped) Dispatcher.Resume();

            base.Start();

            Logger.Debug("Bot started");
        }

        public override void Stop()
        {
            if (!Running)
                return;

            Dispatcher?.Stop();

            base.Stop();

            Logger.Debug("Bot stopped");
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Character?.Name))
                return Character.Name;

            if (!string.IsNullOrEmpty(ClientInformations?.Nickname))
                return ClientInformations.Nickname;

            return "Bot #" + Id;
        }

        #endregion

        #region Methods

        protected void OnCharacterSelected(PlayedCharacter character)
            => CharacterSelected?.Invoke(this, character);

        protected override void OnTick()
        {
            try
            {
                Dispatcher.ProcessDispatching(this);

                // note : not the correct way for the moment
                Character?.Context?.Tick(0);
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);

                Stop();
            }
        }

        private void OnMessageDispatched(MessageDispatcher messageDispatcher, Message message)
        {
            if (!(message is NetworkMessage networkMessage) || networkMessage.Canceled || !networkMessage.Destinations.HasFlag(ListenerEntry.Client | ListenerEntry.Server)) return;
            InstanceId++;
        }

        #endregion
    }
}