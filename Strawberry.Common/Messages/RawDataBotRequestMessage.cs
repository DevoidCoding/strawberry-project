﻿namespace Strawberry.Common.Messages
{
    using Strawberry.Core.Messages;

    public class RawDataBotRequestMessage : Message
    {
        #region Constructors and Destructors

        public RawDataBotRequestMessage(byte[] content)
            => Content = content;

        #endregion

        #region Public Properties

        public byte[] Content { get; set; }

        #endregion
    }
}