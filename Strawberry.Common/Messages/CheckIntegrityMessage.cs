﻿namespace Strawberry.Common.Messages
{
    using Strawberry.Core.Messages;

    public class CheckIntegrityMessage : Message
    {
        #region Constructors and Destructors

        public CheckIntegrityMessage(byte[] content)
            => Content = content;

        #endregion

        #region Public Properties

        public byte[] Content { get; set; }

        #endregion
    }
}