﻿namespace Strawberry.Common.Handlers.Chat
{
    using JetBrains.Annotations;

    using Strawberry.Common.Game.Chat;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class ChatHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(ChatAdminServerMessage))]
        public static void HandleChatAdminServerMessage(Bot bot, ChatAdminServerMessage message)
            => bot.SendLocal(new BotChatMessageServer(message));

        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatClientMultiMessage(Bot bot, ChatClientMultiMessage message)
            => bot.SendLocal(new BotChatMessageClient(message));

        [MessageHandler(typeof(ChatClientPrivateMessage))]
        public static void HandleChatClientPrivateMessage(Bot bot, ChatClientPrivateMessage message)
            => bot.SendLocal(new BotChatMessageClient(message));

        [MessageHandler(typeof(ChatServerCopyMessage))]
        public static void HandleChatServerCopyMessage(Bot bot, ChatServerCopyMessage message)
            => bot.SendLocal(new BotChatMessageServer(message));

        [MessageHandler(typeof(ChatServerMessage))]
        public static void HandleChatServerMessage(Bot bot, ChatServerMessage message)
            => bot.SendLocal(new BotChatMessageServer(message));

        #endregion
    }
}