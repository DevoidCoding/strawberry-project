﻿namespace Strawberry.Common.Handlers.Character
{
    using JetBrains.Annotations;

    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class CharacterHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.SetPlayedCharacter(new PlayedCharacter(bot, message.Infos));

        [MessageHandler(typeof(CharactersListMessage))]
        public static void HandleCharactersListMessage(Bot bot, CharactersListMessage message)
        {
            bot.ClientInformations.Update(message);
            bot.Display = DisplayState.CharacterSelection;
        }

        [MessageHandler(typeof(CharacterStatsListMessage))]
        public static void HandleCharacterStatsListMessage(Bot bot, CharacterStatsListMessage message)
            => bot.Character?.Update(message);

        [MessageHandler(typeof(GameMapNoMovementMessage))]
        public static void HandleGameMapNoMovementMessage(Bot bot, GameMapNoMovementMessage message)
        {
            if (bot.Character == null)
                return;

            if (bot.Character.IsFighting())
                bot.Character.Fighter.Update(message);
            else
                bot.Character.Update(message);
        }

        [MessageHandler(typeof(SetCharacterRestrictionsMessage))]
        public static void HandleSetCharacterRestrictionsMessage(Bot bot, SetCharacterRestrictionsMessage message)
            => bot.Character?.Update(message);

        [MessageHandler(typeof(EmoteListMessage))]
        public static void OnEmoteListMessage(Bot bot, EmoteListMessage message)
            => bot.Character?.Update(message);

        #endregion
    }
}