﻿namespace Strawberry.Common.Handlers.Connection
{
    using JetBrains.Annotations;

    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class ConnectionHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(AccountCapabilitiesMessage))]
        public static void HandleAccountCapabilitiesMessage(Bot bot, AccountCapabilitiesMessage message)
            => bot.ClientInformations.Update(message);

        [MessageHandler(typeof(HelloConnectMessage))]
        public static void HandleHelloConnectMessage(Bot bot, HelloConnectMessage message)
            => bot.ClientInformations.Update(message);

        [MessageHandler(typeof(IdentificationFailedBannedMessage))]
        public static void HandleIdentificationFailedBannedMessage(Bot bot, IdentificationFailedBannedMessage message)
        {
            bot.ClientInformations.Update(message);
            bot.OnCharacterIdentified(false);
        }

        [MessageHandler(typeof(IdentificationFailedForBadVersionMessage))]
        public static void HandleIdentificationFailedForBadVersionMessage(Bot bot, IdentificationFailedForBadVersionMessage message)
        {
            bot.ClientInformations.Update(message);
            bot.OnCharacterIdentified(false);
        }

        [MessageHandler(typeof(IdentificationFailedMessage))]
        public static void HandleIdentificationFailedMessage(Bot bot, IdentificationFailedMessage message)
        {
            bot.ClientInformations.Update(message);
            bot.OnCharacterIdentified(false);
        }

        [MessageHandler(typeof(IdentificationMessage))]
        public static void HandleIdentificationMessage(Bot bot, IdentificationMessage message)
            => bot.ClientInformations.Update(message);

        [MessageHandler(typeof(IdentificationSuccessMessage))]
        public static void HandleIdentificationSuccessMessage(Bot bot, IdentificationSuccessMessage message)
        {
            bot.ClientInformations.Update(message);
            message.HasRights = true; // allow to open the console
            bot.OnCharacterIdentified(true);
        }

        [MessageHandler(typeof(SelectedServerDataMessage))]
        public static void HandleSelectedServerDataMessage(Bot bot, SelectedServerDataMessage message)
            => bot.ClientInformations.Update(message);

        [MessageHandler(typeof(ServersListMessage))]
        public static void HandleServersListMessage(Bot bot, ServersListMessage message)
        {
            bot.ClientInformations.Update(message);
            bot.Display = DisplayState.ServerSelection;
        }

        #endregion
    }
}