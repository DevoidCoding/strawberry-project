﻿namespace Strawberry.Common.Handlers.Job
{
    using JetBrains.Annotations;

    using NLog;

    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class JobHandler
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(JobDescriptionMessage))]
        public static void HandleJobDescriptionMessage(Bot bot, JobDescriptionMessage message)
            => bot.Character.Update(message);

        [MessageHandler(typeof(JobExperienceMultiUpdateMessage))]
        public static void HandleJobExperienceMultiUpdateMessage(Bot bot, JobExperienceMultiUpdateMessage message)
        {
            foreach (var update in message.ExperiencesUpdate)
            {
                var job = bot.Character.GetJob(update.JobId);

                if (job == null)
                    Logger.Warn("Cannot update job {0} experience because it's not found", update.JobId);
                else
                    job.Update(update);
            }
        }

        [MessageHandler(typeof(JobExperienceUpdateMessage))]
        public static void HandleJobExperienceUpdateMessage(Bot bot, JobExperienceUpdateMessage message)
        {
            var job = bot.Character.GetJob(message.ExperiencesUpdate.JobId);

            if (job == null)
                Logger.Warn("Cannot update job {0} experience because it's not found", message.ExperiencesUpdate.JobId);
            else
                job.Update(message.ExperiencesUpdate);

            bot.Character.Inventory.FixInventoryOverloadIfNeeded();
        }

        [MessageHandler(typeof(JobLevelUpMessage))]
        public static void HandleJobLevelUpMessage(Bot bot, JobLevelUpMessage message)
            => bot.Character.Update(message);

        #endregion
    }
}