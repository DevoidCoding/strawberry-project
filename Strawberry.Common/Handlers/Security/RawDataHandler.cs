﻿namespace Strawberry.Common.Handlers.Security
{
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common.Messages;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    using CheckIntegrityMessage = Strawberry.Common.Messages.CheckIntegrityMessage;

    internal class RawDataHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(RawDataMessage))]
        public static void OnRawDataMessage(Bot bot, RawDataMessage message)
        {
            if (bot.ConnectionType == ClientConnectionType.Authentification) return;
            message.BlockProgression();
            bot.SendLocal(new RawDataBotRequestMessage(message.Content));
        }

        [MessageHandler(typeof(CheckIntegrityMessage))]
        public static void OnCheckIntegrityMessage(Bot bot, CheckIntegrityMessage message)
        {
            bot.SendToServer(new Protocol.Messages.CheckIntegrityMessage(message.Content.Select(e => (sbyte)e).ToArray()));
        }
        #endregion
    }
}