﻿namespace Strawberry.Common.Handlers.Basic
{
    using System;

    using JetBrains.Annotations;

    using Strawberry.Common.Game.Basic;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class BasicHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(BasicLatencyStatsRequestMessage))]
        public static void OnBasicLatencyStatsRequestMessage(Bot bot, BasicLatencyStatsRequestMessage message)
        {
            if (bot.BasicLatency == null)
                throw new NullReferenceException(nameof(bot.BasicLatency));

            bot.BasicLatency.Respond();
        }

        [MessageHandler(typeof(BasicTimeMessage))]
        public static void OnBasicTimeMessage(Bot bot, BasicTimeMessage message)
            => bot.ClientInformations.Update(message);

        [MessageHandler(typeof(ClientKeyMessage))]
        public static void OnClientKeyMessage(Bot bot, ClientKeyMessage message)
        {
            if (bot.ConnectionType != ClientConnectionType.GameConnection)
                return;

            if (bot.BasicLatency != null)
                return;

            bot.BasicLatency = new BasicLatency(bot);
        }

        #endregion
    }
}