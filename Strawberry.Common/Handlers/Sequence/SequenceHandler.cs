﻿namespace Strawberry.Common.Handlers.Sequence
{
    using JetBrains.Annotations;

    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class SequenceHandler
    {
        #region Methods

        [MessageHandler(typeof(SequenceNumberRequestMessage))]
        private static void OnSequenceNumberRequestMessage(Bot bot, SequenceNumberRequestMessage message)
        {
            bot.Sequence.Update(message);
            message.BlockProgression();
        }

        #endregion
    }
}