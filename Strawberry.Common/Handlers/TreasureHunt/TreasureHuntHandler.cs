﻿namespace Strawberry.Common.Handlers.TreasureHunt
{
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    public class TreasureHuntHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(MapComplementaryInformationsDataMessage))]
        public static void OnMapInformationsRequestMessage(Bot bot, MapComplementaryInformationsDataMessage message)
            => bot.Character?.TreasureHunt?.Update(message);

        [MessageHandler(typeof(TreasureHuntFinishedMessage))]
        public static void OnTreasureHuntFinishedMessage(Bot bot, TreasureHuntFinishedMessage message)
            => bot.Character?.TreasureHunt?.Update(message);

        [MessageHandler(typeof(TreasureHuntRequestAnswerMessage))]
        public static void OnTreasureHuntMessage(Bot bot, TreasureHuntRequestAnswerMessage message)
            => bot.Character?.TreasureHunt?.Update(message);

        [MessageHandler(typeof(TreasureHuntMessage))]
        public static void OnTreasureHuntMessage(Bot bot, TreasureHuntMessage message)
            => bot.Character?.TreasureHunt?.Update(message);

        #endregion
    }
}