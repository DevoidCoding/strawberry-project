﻿namespace Strawberry.Common.Handlers.Spell
{
    using JetBrains.Annotations;

    using NLog;

    using Strawberry.Common.Game.Fights;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class SpellHandler
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(GameActionFightNoSpellCastMessage))]
        public static void HandleGameActionFightNoSpellCastMessage(Bot bot, GameActionFightNoSpellCastMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fighter;

            fighter?.Update(message);
        }

        [MessageHandler(typeof(GameActionFightSpellCastMessage))]
        public static void HandleGameActionFightSpellCastMessage(Bot bot, GameActionFightSpellCastMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fight.GetFighter(message.SourceId);

            if (fighter == null)
            {
                Logger.Error("Fighter {0} cast a spell but doesn't exist", message.SourceId);
            }
            else
            {
                fighter.NotifySpellCasted(new SpellCast(bot.Character.Fight, message));

                if (bot.Character.Fighter != null && bot.Character.Fighter.Id == message.SourceId)
                    bot.Character.SpellsBook.CastAt(message);
            }
        }

        [MessageHandler(typeof(GameActionFightSpellCooldownVariationMessage))]
        public static void HandleGameActionFightSpellCooldownVariationMessage(Bot bot, GameActionFightSpellCooldownVariationMessage message)
        {
        }

        [MessageHandler(typeof(SpellListMessage))]
        public static void HandleSpellListMessage(Bot bot, SpellListMessage message)
            => bot.Character.Update(message);

        [MessageHandler(typeof(SpellVariantActivationMessage))]
        public static void HandleSpellUpgradeSuccessMessage(Bot bot, SpellVariantActivationMessage message)
            => bot.Character.Update(message);

        #endregion
    }
}