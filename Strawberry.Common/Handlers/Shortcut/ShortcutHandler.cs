﻿namespace Strawberry.Common.Handlers.Shortcut
{
    using JetBrains.Annotations;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class ShortcutHandler
    {
        // [MessageHandler(typeof(ShortcutBarContentMessage))]
        // public static void HandleShortcutBarContentMessage(Bot bot, ShortcutBarContentMessage message)
        // {
        //     bot.Character.Update(message);
        // }

        // [MessageHandler(typeof(ShortcutBarRefreshMessage))]
        // public static void HandleShortcutBarRefreshMessage(Bot bot, ShortcutBarRefreshMessage message)
        // {
        //     if (message.BarType == (int)bot.Character.GeneralShortcuts.BarType)
        //         bot.Character.GeneralShortcuts.Update(message);
        //     else
        //         bot.Character.SpellShortcuts.Update(message);
        // }

        // [MessageHandler(typeof(ShortcutBarRemovedMessage))]
        // public static void HandleShortcutBarRemovedMessage(Bot bot, ShortcutBarRemovedMessage message)
        // {
        //     if (message.BarType == (int)bot.Character.GeneralShortcuts.BarType)
        //         bot.Character.GeneralShortcuts.Remove(message.Slot);
        //     else
        //         bot.Character.SpellShortcuts.Remove(message.Slot);
        // }
    }
}