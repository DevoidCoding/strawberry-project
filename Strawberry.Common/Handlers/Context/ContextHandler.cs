﻿namespace Strawberry.Common.Handlers.Context
{
    using JetBrains.Annotations;

    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class ContextHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(GameContextCreateMessage))]
        public static void HandleGameContextCreateMessage(Bot bot, GameContextCreateMessage message)
        {
            if (bot.Display != DisplayState.InGame)
                bot.Display = DisplayState.InGame;

            bot.Character.ChangeContext((GameContextEnum)message.Context);
        }

        [MessageHandler(typeof(GameContextDestroyMessage))]
        public static void HandleGameContextDestroyMessage(Bot bot, GameContextDestroyMessage message)
            => bot.Character.LeaveContext();

        [MessageHandler(typeof(GameContextRemoveElementMessage))]
        public static void HandleGameContextRemoveElementMessage(Bot bot, GameContextRemoveElementMessage message)
            => bot.Character.Context.RemoveActor(message.Id);

        #endregion
    }
}