﻿namespace Strawberry.Common.Handlers.Context
{
    using JetBrains.Annotations;

    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.World;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class RolePlayHandler : Frame<RolePlayHandler>
    {
        #region Constructors and Destructors

        public RolePlayHandler(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(CurrentMapMessage))]
        public static void HandleCurrentMapMessage(Bot bot, CurrentMapMessage message)
            => bot.Character.EnterMap(new Map((int)message.MapId, message.MapKey));

        [MessageHandler(typeof(GameRolePlayShowActorMessage))]
        public void HandleGameRolePlayShowActorMessage(Bot bot, GameRolePlayShowActorMessage message)
            => bot.Character.Map.AddActor(bot, message.Informations);

        [MessageHandler(typeof(InteractiveElementUpdatedMessage))]
        public void HandleInteractiveElementUpdatedMessage(Bot bot, InteractiveElementUpdatedMessage message)
            => bot.Character.Map.Update(message);

        [MessageHandler(typeof(InteractiveMapUpdateMessage))]
        public void HandleInteractiveMapUpdateMessage(Bot bot, InteractiveMapUpdateMessage message)
            => bot.Character.Map.Update(message);

        [MessageHandler(typeof(InteractiveUsedMessage))]
        public void HandleInteractiveUsedMessage(Bot bot, InteractiveUsedMessage message)
        {
            var interactive = bot.Character.Map.GetInteractive(message.ElemId);

            interactive?.NotifyInteractiveUsed(message);
        }

        [MessageHandler(typeof(InteractiveUseEndedMessage))]
        public void HandleInteractiveUseEndedMessage(Bot bot, InteractiveUseEndedMessage message)
        {
            var interactive = bot.Character.Map.GetInteractive(message.ElemId);

            interactive?.NotifyInteractiveUseEnded();
        }

        [MessageHandler(typeof(MapComplementaryInformationsDataMessage))]
        public void HandleMapComplementaryInformationsDataMessage(Bot bot, MapComplementaryInformationsDataMessage message)
            => bot.Character.Map.Update(bot, message);

        [MessageHandler(typeof(StatedElementUpdatedMessage))]
        public void HandleStatedElementUpdatedMessage(Bot bot, StatedElementUpdatedMessage message)
        {
            var interactive = bot.Character.Map.GetInteractive((uint)message.StatedElement.ElementId);
            if (interactive == null) return;
            var previousState = interactive.ToString();
            bot.Character.Map.Update(message);
            bot.Character.SendInformation("StatedElementUpdatedMessage : {0} => {1}", previousState, interactive);
        }

        [MessageHandler(typeof(StatedMapUpdateMessage))]
        public void HandleStatedMapUpdateMessage(Bot bot, StatedMapUpdateMessage message)
            => bot.Character.Map.Update(message);

        #endregion
    }
}