﻿namespace Strawberry.Common.Handlers.Context
{
    using JetBrains.Annotations;

    using MoreLinq;

    using NLog;

    using Strawberry.Common.Frames;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class FightHandler : Frame<FightHandler>
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors and Destructors

        public FightHandler(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(GameFightJoinMessage))]
        public static void HandleGameFightJoinMessage(Bot bot, GameFightJoinMessage message)
        {
            if (bot?.Character == null)
            {
                Logger.Error("Character is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.EnterFight(message);
        }

        [MessageHandler(typeof(GameFightStartingMessage))]
        public static void HandleGameFightStartingMessage(Bot bot, GameFightStartingMessage message)
            => bot.Character.SpellsBook.FightStart(message);

        [MessageHandler(typeof(FighterStatsListMessage))]
        public void HandleFighterStatsListMessage(Bot bot, FighterStatsListMessage message)
            => bot.Character.Stats.Update(message.Stats);

        /// <summary>
        ///     New effect
        /// </summary>
        /// <param name="bot"></param>
        /// <param name="message"></param>
        [MessageHandler(typeof(GameActionFightDispellableEffectMessage))]
        public void HandleGameActionFightDispellableEffectMessage(Bot bot, GameActionFightDispellableEffectMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.AddEffect(message.Effect, message.ActionId);
        }

        /// <summary>
        ///     Effect dispelled
        /// </summary>
        /// <param name="bot"></param>
        /// <param name="message"></param>
        [MessageHandler(typeof(GameActionFightDispellEffectMessage))]
        public void HandleGameActionFightDispellEffectMessage(Bot bot, GameActionFightDispellEffectMessage message)
            => bot.Character.Fight.DispelEffect(message.BoostUID, message.TargetId);

        [MessageHandler(typeof(GameActionFightDispellMessage))]
        public void HandleGameActionFightDispellMessage(Bot bot, GameActionFightDispellMessage message)
            => bot.Character.Fight.DispelTarget(message.TargetId);

        [MessageHandler(typeof(GameActionFightDispellSpellMessage))]
        public void HandleGameActionFightDispellSpellMessage(Bot bot, GameActionFightDispellSpellMessage message)
            => bot.Character.Fight.DispelSpell(message.TargetId, message.SpellId);

        [MessageHandler(typeof(GameActionFightMarkCellsMessage))]
        public void HandleGameActionFightMarkCellsMessage(Bot bot, GameActionFightMarkCellsMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            foreach (var cellSet in message.Mark.Cells)
                bot.Character.Fight.SetTrap(message.Mark.MarkId, cellSet.CellId, cellSet.ZoneSize);
        }

        [MessageHandler(typeof(GameActionFightSummonMessage))]
        public void HandleGameActionFightSummonMessage(Bot bot, GameActionFightSummonMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            message.Summons.ForEach(bot.Character.Fight.AddActor);
        }

        [MessageHandler(typeof(GameActionFightTriggerGlyphTrapMessage))]
        public void HandleGameActionFightTriggerGlyphTrapMessage(Bot bot, GameActionFightTriggerGlyphTrapMessage message)
        {
        }

        [MessageHandler(typeof(GameActionFightUnmarkCellsMessage))]
        public void HandleGameActionFightUnmarkCellsMessage(Bot bot, GameActionFightUnmarkCellsMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.UnsetTrap(message.MarkId);
        }

        [MessageHandler(typeof(GameEntitiesDispositionMessage))]
        public void HandleGameEntitiesDispositionMessage(Bot bot, GameEntitiesDispositionMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(GameFightEndMessage))]
        public void HandleGameFightEndMessage(Bot bot, GameFightEndMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.EndFight(message);
            bot.Character.LeaveFight();
        }

        [MessageHandler(typeof(GameFightHumanReadyStateMessage))]
        public void HandleGameFightHumanReadyStateMessage(Bot bot, GameFightHumanReadyStateMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(GameFightLeaveMessage))]
        public void HandleGameFightLeaveMessage(Bot bot, GameFightLeaveMessage message)
        {
        }

        [MessageHandler(typeof(GameFightNewRoundMessage))]
        public void HandleGameFightNewRoundMessage(Bot bot, GameFightNewRoundMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.SetRound(message.RoundNumber);
        }

        [MessageHandler(typeof(GameFightOptionStateUpdateMessage))]
        public void HandleGameFightOptionStateUpdateMessage(Bot bot, GameFightOptionStateUpdateMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(GameFightPlacementPossiblePositionsMessage))]
        public void HandleGameFightPlacementPossiblePositionsMessage(Bot bot, GameFightPlacementPossiblePositionsMessage message)
        {
            if (bot?.Character == null)
            {
                Logger.Error("Character is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Update(message);
        }

        [MessageHandler(typeof(GameFightRefreshFighterMessage))]
        public void HandleGameFightRefreshFighterMessage(Bot bot, GameFightRefreshFighterMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(GameFightRemoveTeamMemberMessage))]
        public void HandleGameFightRemoveTeamMemberMessage(Bot bot, GameFightRemoveTeamMemberMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            if (bot.Character.Fight.Id == message.FightId)
                bot.Character.Fight.RemoveActor(message.CharId);
        }

        [MessageHandler(typeof(GameFightShowFighterMessage))]
        public void HandleGameFightShowFighterMessage(Bot bot, GameFightShowFighterMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.AddActor(message.Informations);
        }

        [MessageHandler(typeof(GameFightStartMessage))]
        public void HandleGameFightStartMessage(Bot bot, GameFightStartMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.StartFight();
        }

        [MessageHandler(typeof(GameFightSynchronizeMessage))]
        public void HandleGameFightSynchronizeMessage(Bot bot, GameFightSynchronizeMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(bot, message);
        }

        [MessageHandler(typeof(GameFightTurnEndMessage))]
        public void HandleGameFightTurnEndMessage(Bot bot, GameFightTurnEndMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.EndTurn();

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (message.Id == bot.Character.Fighter.Id)
                bot.Character.SpellsBook.EndTurn();
        }

        [MessageHandler(typeof(GameFightTurnListMessage))]
        public void HandleGameFightTurnListMessage(Bot bot, GameFightTurnListMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(GameFightTurnStartMessage))]
        public void HandleGameFightTurnStartMessage(Bot bot, GameFightTurnStartMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.StartTurn(message.Id);
        }

        [MessageHandler(typeof(GameFightUpdateTeamMessage))]
        public void HandleGameFightUpdateTeamMessage(Bot bot, GameFightUpdateTeamMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            if (bot.Character.Fight.Id == message.FightId)
                bot.Character.Fight.Update(message);
        }

        [MessageHandler(typeof(SequenceEndMessage))]
        public void HandleSequenceEndMessage(Bot bot, SequenceEndMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.EndSequence(message);
        }

        #endregion

        #region stats update

        [MessageHandler(typeof(GameActionFightLifePointsLostMessage))]
        public void HandleGameActionFightLifePointsLostMessage(Bot bot, GameActionFightLifePointsLostMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fight.GetFighter(message.TargetId);

            if (fighter == null)
                Logger.Error("Fighter {0} has lost {2} HP cast but doesn't exist, or is it {1} ?", message.TargetId, message.SourceId, message.Loss);
            else fighter.UpdateHP(message);
        }

        [MessageHandler(typeof(GameActionFightLifeAndShieldPointsLostMessage))]
        public void HandleGameActionFightLifeAndShieldPointsLostMessage(Bot bot, GameActionFightLifeAndShieldPointsLostMessage message)
            => HandleGameActionFightLifePointsLostMessage(bot, message);

        [MessageHandler(typeof(GameActionFightLifePointsGainMessage))]
        public void HandleGameActionFightLifePointsGainMessage(Bot bot, GameActionFightLifePointsGainMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fight.GetFighter(message.TargetId);

            if (fighter == null)
                Logger.Error("Fighter {0} has gain {2} HP cast but doesn't exist, or is it {1} ?", message.TargetId, message.SourceId, message.Delta);
            else fighter.UpdateHP(message);
        }

        [MessageHandler(typeof(GameActionFightPointsVariationMessage))]
        public void HandleGameActionFightPointsVariationMessage(Bot bot, GameActionFightPointsVariationMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fight.GetFighter(message.TargetId);

            if (fighter == null)
                Logger.Error("Fighter {0} has lost {2} ?P points but doesn't exist, or is it {1} ?", message.SourceId, message.TargetId, -message.Delta);
            else fighter.Update(message);
        }

        [MessageHandler(typeof(GameActionFightTackledMessage))]
        public void HandleGameActionFightTackledMessage(Bot bot, GameActionFightTackledMessage message)
        {
        }

        [MessageHandler(typeof(GameActionAcknowledgementMessage))]
        public void HandleGameActionAcknowledgementMessage(Bot bot, GameActionAcknowledgementMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            var fighter = bot.Character.Fighter;

            fighter?.Update(message);
        }

        [MessageHandler(typeof(GameActionFightDeathMessage))]
        public void HandleGameActionFightDeathMessage(Bot bot, GameActionFightDeathMessage message)
        {
            if (bot?.Character?.Fight == null)
            {
                Logger.Error("Fight is not properly initialized.");
                return; // Can't handle the message
            }

            bot.Character.Fight.Update(bot, message);
        }

        #endregion stats update
    }
}