﻿namespace Strawberry.Common.Handlers.Context
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using JetBrains.Annotations;

    using NLog;

    using Strawberry.Common.Game.Actors;
    using Strawberry.Common.Game.Movements;
    using Strawberry.Common.Game.World.Pathfinding;
    using Strawberry.Core.Config;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class MapMovementHandler
    {
        #region Static Fields

        [Configurable("EstimatedMovementLag", "Refer to the estimated time elapsed until the client start moving (in ms)")]
        public static int EstimatedMovementLag = 160;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(GameActionFightSlideMessage))]
        public static void HandleGameActionFightSlideMessage(Bot bot, GameActionFightSlideMessage message)
        {
            if (!bot.Character.IsFighting() || bot.Character.Fight == null)
            {
                Logger.Error("GameActionFightSlideMessage has no sense out of a fight");
                return;
            }

            var actor = bot.Character.Fight.GetActor(message.TargetId);

            if (actor == null)
            {
                Logger.Error("Actor {0} is not known.", message.TargetId);
                return;
            }

            actor.SetPos(message.EndCellId);
        }

        [MessageHandler(typeof(GameMapMovementCancelMessage))]
        public static void HandleGameMapMovementCancelMessage(Bot bot, GameMapMovementCancelMessage message)
        {
            // always check, the client can send bad things :)
            if (!bot.Character.IsMoving())
                return;

            var attemptElement = bot.Character.Movement.TimedPath.GetCurrentElement();

            if (attemptElement.CurrentCell.Id != message.CellId)
            {
                var clientCell = bot.Character.Movement.TimedPath.Elements.FirstOrDefault(entry => entry.CurrentCell.Id == message.CellId);
                if (clientCell == null) return;

                // the difference is the time elapsed until the client analyse the path and start moving (~160ms) it depends also on computer hardware
                Logger.Warn(
                    "Warning the client has canceled the movement but the given cell ({0}) is not the attempted one ({1})." + "Estimated difference : {2}ms",
                    message.CellId,
                    attemptElement.CurrentCell.Id,
                    (attemptElement.EndTime - clientCell.EndTime).TotalMilliseconds);
            }

            bot.Character.NotifyStopMoving(true, false);
            bot.Character.SetPos(message.CellId);
        }

        [MessageHandler(typeof(GameMapMovementConfirmMessage))]
        public static void HandleGameMapMovementConfirmMessage(Bot bot, GameMapMovementConfirmMessage message)
        {
            if (bot.Character.IsMoving())
                bot.Character.NotifyStopMoving(false, false);
        }

        [MessageHandler(typeof(GameMapMovementMessage))]
        public static void HandleGameMapMovementMessage(Bot bot, GameMapMovementMessage message)
        {
            if (bot.Character?.Context == null)
            {
                Logger.Error("Context is null as processing movement");
                return;
            }

            ContextActor actor = null;
            var fightActor = false;

            if (bot.Character.IsFighting())
                actor = bot.Character.Fight.GetActor(message.ActorId);

            if (actor == null)
                actor = bot.Character.Context.GetActor(message.ActorId);
            else
                fightActor = true;

            if (actor == null)
            {
                Logger.Error("Actor {0} not found (known : {1})", message.ActorId, string.Join(",", fightActor ? bot.Character.Fight.Actors : bot.Character.Context.Actors)); // only a log for the moment until context are fully handled
                return;
            }

            // just to update the position. If in fight, better update immediately to be sure that the next action take the mouvement into account
            if (message.KeyMovements.Length == 1)
            {
                actor.UpdatePosition(message.KeyMovements[0]);
            }
            else
            {
                var path = Path.BuildFromServerCompressedPath(bot.Character.Map, message.KeyMovements);

                if (path.IsEmpty())
                {
                    Logger.Warn("Try to start moving with an empty path");
                    return;
                }

                var movement = new MovementBehavior(path, actor.GetAdaptedVelocity(path));
                movement.Start(DateTime.Now + TimeSpan.FromMilliseconds(EstimatedMovementLag));

                actor.NotifyStartMoving(movement);
            }
        }

        [MessageHandler(typeof(GameMapNoMovementMessage))]
        public static void HandleGameMapNoMovementMessage(Bot bot, GameMapNoMovementMessage message)
        {
            // always check, the client can send bad things :)
            if (!bot.Character.IsMoving())
                return;

            bot.Character.NotifyStopMoving(true, true);
        }

        #endregion
    }
}