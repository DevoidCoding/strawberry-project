﻿using Strawberry.Core.Reflection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Common.Data.Icons;
using Strawberry.Core.Extensions;
using Strawberry.Core.UI;
using Strawberry.Protocol.Tools.D2P;

namespace Strawberry.Common.Data.GFXs
{
    // ReSharper disable once InconsistentNaming
    public class GFXsManager : Singleton<GFXsManager>
    {
        // ReSharper disable once InconsistentNaming
        public D2PFile _d2pFile;
        public void Initialize(string path)
        {
            _d2pFile = new D2PFile(path);
        }

        public Icon GetIcon(int id)
        {
            var isPng = _d2pFile.RootDirectories.First(e => e.Name == "png").Entries.Any(e => e.FileName.Split('.')[0] == id.ToString());
            var isJpg = _d2pFile.RootDirectories.First(e => e.Name == "jpg").Entries.Any(e => e.FileName.Split('.')[0] == id.ToString());
            if (!isPng && !isJpg)
                throw new ArgumentException($"Item icon {id} not found");

            var ext = isPng ? ".png" : ".jpg";
            var data = _d2pFile.ReadFile($"{ext.Substring(1)}/{id}{ext}");

            return new Icon(id, id + ext, data);
        }

        public IEnumerable<Icon> EnumerateIcons()
        {
            foreach (var entry in _d2pFile.RootDirectories.First(e => e.Name == "png" ||e.Name == "jpg").Entries)
            {
                if (entry.FullFileName.StartsWith("empty") || entry.FullFileName.StartsWith("error"))
                    continue;

                var extension = entry.FileName.Split('.').Last();
                var data = _d2pFile.ReadFile(entry);
                var id = int.Parse(entry.FileName.Replace($".{extension}", ""));

                yield return new Icon(id, entry.FileName, data);
            }
        }

        public IEnumerable<(string name, byte[] data)> EnumerateSwfs()
        {
            var d2PEntries = _d2pFile.RootDirectories.FirstOrDefault(e => e.Name == "swf")?.Entries;
            if (d2PEntries == null)
                throw new NullReferenceException(nameof(d2PEntries));
            foreach (var entry in d2PEntries)
            {
                var data = _d2pFile.ReadFile(entry);
                var name = entry.FileName;

                yield return (name, data);
            }
        }

        public ProgressionCounter ExtractGfxs(string directory)
        {
            var gfxs = EnumerateIcons();
            var progression = new ProgressionCounter(gfxs.Count());
            Task.Factory.StartNew(() => ExtractGfxs(gfxs, directory, progression));
            return progression;
        }

        private static void ExtractGfxs(IEnumerable<Icon> gfxs, string directory, ProgressionCounter progression)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            var i = 0;
            foreach (var gfx in gfxs)
            {
                var path = Path.Combine($@"{directory}\", gfx.Name);
                gfx.Image.Save(path);
                progression.UpdateValue(i);
                i++;
            }

            progression.NotifyEnded();
        }
    }
}
