﻿#region License GNU GPL
// MapsPositionManager.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.World.Data;
using Strawberry.Core.Reflection;
using Strawberry.Core.UI;
using Strawberry.Protocol.Data;
using Point = System.Drawing.Point;

namespace Strawberry.Common.Data.Maps
{
    public class MapsPositionManager : Singleton<MapsPositionManager>
    {
        internal class RegionContainerCollection
        {
            private readonly Dictionary<int, List<int>> _subRegions = new Dictionary<int, List<int>>();
            private readonly Dictionary<int, Dictionary<Point, List<MapWithPriority>>> _mapsByPoint = new Dictionary<int, Dictionary<Point, List<MapWithPriority>>>();

            public void AddRegion(int containerId, int regionId)
            {
                if (!_subRegions.ContainsKey(containerId))
                    _subRegions.Add(containerId, new List<int>());

                _subRegions[containerId].Add(regionId);
            }

            public bool ContainsRegion(int containerId, int regionId)
            {
                return _subRegions.ContainsKey(containerId) && _subRegions[containerId].Contains(regionId);
            }

            public void AddMap(int containerId, Point mapPos, MapWithPriority mapId)
            {
                if (!_mapsByPoint.ContainsKey(containerId))
                    _mapsByPoint.Add(containerId, new Dictionary<Point, List<MapWithPriority>>());

                if (!_mapsByPoint[containerId].ContainsKey(mapPos))
                    _mapsByPoint[containerId].Add(mapPos, new List<MapWithPriority>());

                _mapsByPoint[containerId][mapPos].Add(mapId);
            }

            public int? GetBestMap(int containerId, int x, int y)
            {
                var pos = new Point(x, y);
                if (!_mapsByPoint[containerId].TryGetValue(pos, out List<MapWithPriority> maps))
                    return null;

                return maps.OrderByDescending(map => map.CellsCount).First().MapId;
            }

            public void Dispose()
            {
                _subRegions.Clear();
                _mapsByPoint.Clear();
            }
        }

        internal class MapWithPriority
        {
            public MapWithPriority(MapData map)
            {
                MapId = map.Id;
                CellsCount = map.Cells.Count(x => x.Walkable);
            }

            public int MapId;
            public int CellsCount;
        }

        public const int Version = 1;

        public const string RedisVersion = "MAPS_POSITIONS_VERSION";
        public const string RedisKey = "MAPS_POSITIONS";

        private bool _initialized;

        private readonly RegionContainerCollection _subAreaMaps = new RegionContainerCollection();
        private readonly RegionContainerCollection _areaChildrens = new RegionContainerCollection();
        private readonly RegionContainerCollection _superAreaChildrens = new RegionContainerCollection();
        private readonly RegionContainerCollection _worldMapsChildrens = new RegionContainerCollection();

        private Dictionary<int, SubArea> _subAreas = new Dictionary<int, SubArea>();
        private Dictionary<int, Area> _areas = new Dictionary<int, Area>();
        private Dictionary<int, SuperArea> _superAreas = new Dictionary<int, SuperArea>();

        public ProgressionCounter Initialize()
        {
            _initialized = true;
            return BeginGeneration();
        }

        public MapPositionData GetMapPosition(int mapId, bool @throw = true)
        {
            AssertInitialized();
            if (_mapsPosition.All(e => e.Value.MapId != mapId) && @throw)
                throw new Exception($"Map {mapId} not found");

            return _mapsPosition[mapId];
        }

        public IEnumerable<MapPositionData> EnumerateAllMaps()
        {
            AssertInitialized();

            return _mapsPosition.Values.ToList();
        }

        private void AssertInitialized()
        {
            if (!_initialized)
                throw new Exception("Cannot call this method because the class isn't initialized. Call Initialize() before");
        }

        private readonly Dictionary<int, MapPositionData> _mapsPosition = new Dictionary<int, MapPositionData>();
        public ProgressionCounter BeginGeneration()
        {
            var progression = new ProgressionCounter();
            var maps = MapsManager.Instance.EnumerateMaps();
            Task.Factory.StartNew(() =>
                {
                    // step 1 : load areas stuff
                    progression.UpdateValue(0, "(1/3) Getting areas ...");
                    _subAreas = ObjectDataManager.Instance.EnumerateObjects<SubArea>().ToDictionary(x => x.Id);
                    progression.UpdateValue(33);
                    _areas = ObjectDataManager.Instance.EnumerateObjects<Area>().ToDictionary(x => x.Id);
                    progression.UpdateValue(66);
                    _superAreas = ObjectDataManager.Instance.EnumerateObjects<SuperArea>().ToDictionary(x => x.Id);
                    progression.UpdateValue(100);

                    // step 2 : bind to each map his parents areas
                    progression.UpdateValue(0, "(2/3) Getting maps ...");
                    int counter = 0;
                    progression.Total = MapsManager.Instance.MapsCount;
                    foreach (var map in maps)
                    {
                        var pos = new Point(map.X, map.Y);
                        var subArea = _subAreas.ContainsKey(map.SubAreaId) ? _subAreas[map.SubAreaId] : null;
                        var area = subArea != null && _areas.ContainsKey(subArea.AreaId) ? _areas[subArea.AreaId] : null;
                        var superArea = area != null && _subAreas.ContainsKey(area.SuperAreaId) ? _superAreas[area.SuperAreaId] : null;

                        var mapWithPrority = new MapWithPriority(map);

                        if (subArea != null)
                        {
                            _subAreaMaps.AddRegion(subArea.Id, map.Id);
                            _subAreaMaps.AddMap(subArea.Id, pos, mapWithPrority);
                        }

                        if (area != null)
                        {
                            if (!_areaChildrens.ContainsRegion(area.Id, subArea.Id))
                                _areaChildrens.AddRegion(area.Id, subArea.Id);
                            _areaChildrens.AddMap(area.Id, pos, mapWithPrority);
                        }

                        if (superArea != null)
                        {
                            if (!_superAreaChildrens.ContainsRegion(superArea.Id, area.Id))
                                _superAreaChildrens.AddRegion(superArea.Id, area.Id);
                            _superAreaChildrens.AddMap(superArea.Id, pos, mapWithPrority);
                        }

                        int? worldmapId = superArea != null ? (int?)superArea.WorldmapId : null;
                        if (superArea != null)
                        {
                            if (!_worldMapsChildrens.ContainsRegion(worldmapId.Value, superArea.Id))
                                _worldMapsChildrens.AddRegion(worldmapId.Value, superArea.Id);
                            _worldMapsChildrens.AddMap(worldmapId.Value, pos, mapWithPrority);
                        }

                        _mapsPosition.Add(map.Id, new MapPositionData
                        {
                            MapId = map.Id,
                            SubAreaId = subArea?.Id,
                            AreaId = area?.Id,
                            SuperAreaId = superArea?.Id,
                            WorldMapId = worldmapId,
                            X = map.X,
                            Y = map.Y
                        });
                        progression.UpdateValue(counter++);
                    }

                    progression.UpdateValue(0, "(3/3) Finding neighbours ...");
                    progression.Total = _mapsPosition.Count;
                    // step 3 : found for each map his neighbours
                    foreach (var map in _mapsPosition.Values)
                    {
                        var enumerator = FindMapNeighbours(map).GetEnumerator();
                        enumerator.MoveNext();
                        map.RightNeighbourId = enumerator.Current;
                        enumerator.MoveNext();
                        map.TopNeighbourId = enumerator.Current;
                        enumerator.MoveNext();
                        map.LeftNeighbourId = enumerator.Current;
                        enumerator.MoveNext();
                        map.BottomNeighbourId = enumerator.Current;

                        Debug.Assert(!enumerator.MoveNext());
                        progression.Value++;
                    }

                    // hack : Don't dispose
                    //_subAreaMaps.Dispose();
                    //_areaChildrens.Dispose();
                    //_superAreaChildrens.Dispose();
                    //_worldMapsChildrens.Dispose();

                    //_subAreas.Clear();
                    //_areas.Clear();
                    //_superAreas.Clear();

                    progression.NotifyEnded();
                });
            return progression;
        }

        private IEnumerable<int?> FindMapNeighbours(MapPositionData map)
        {
            // right, top, left, bottom
            yield return FindMapNeighbour(map, 1, 0);
            yield return FindMapNeighbour(map, 0, -1);
            yield return FindMapNeighbour(map, -1, 0);
            yield return FindMapNeighbour(map, 0, 1);
        }

        private int? FindMapNeighbour(MapPositionData map, int deltaX, int deltaY)
        {
            int? bySubArea = map.SubAreaId != null ? _subAreaMaps.GetBestMap(map.SubAreaId.Value, map.X + deltaX, map.Y + deltaY) : null;
            if (bySubArea != null)
                return bySubArea;

            int? byArea = map.AreaId != null ? _areaChildrens.GetBestMap(map.AreaId.Value, map.X + deltaX, map.Y + deltaY) : null;
            if (byArea != null)
                return byArea;

            int? bySuperArea = map.SuperAreaId != null ? _superAreaChildrens.GetBestMap(map.SuperAreaId.Value, map.X + deltaX, map.Y + deltaY) : null;
            if (bySuperArea != null)
                return bySuperArea;

            int? byWorldMap = map.WorldMapId != null ? _worldMapsChildrens.GetBestMap(map.WorldMapId.Value, map.X + deltaX, map.Y + deltaY) : null;
            if (byWorldMap != null)
                return byWorldMap;

            return null;
        }
    }
}