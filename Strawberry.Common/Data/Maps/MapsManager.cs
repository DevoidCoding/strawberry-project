﻿#region License GNU GPL
// MapsManager.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Data;
using Strawberry.Core.Config;
using Strawberry.Core.Reflection;
using Strawberry.Core.UI;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Tools.D2P;
using Strawberry.Protocol.Tools.Dlm;
using NLog;
using ProtoBuf;

namespace Strawberry.Common.Data.Maps
{
    public class MapsManager : Singleton<MapsManager>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Dictionary<double, D2PEntry> _entriesLinks = new Dictionary<double, D2PEntry>();
        private D2PFile _reader;

        #region Map Data
        private const int HeaderSize = 14;
        public const ushort CurrentMapsFileVersion = 2;

        [Configurable("MapsDataFile")]
        public static string MapsDataFile = "./maps.dat";

        private readonly ConcurrentDictionary<long, int> _offsetsTable = new ConcurrentDictionary<long, int>();
        private ProgressionCounter _progression;
        private BinaryWriter _writer;

        private ushort _fileVersion;
        private uint _tableOffset;
        private uint _length;

        #endregion

        public int MapsCount => _reader.Entries.Count();

        public DlmMap GetDlmMap(double id, string decryptionKey)
        {
            // retrieve the bound entry to the mainLock or find it in the d2p file
            D2PEntry entry;
            lock (_entriesLinks) 
                if (!_entriesLinks.TryGetValue(id, out entry))
                {
                    var idStr = id.ToString();
                    entry = _reader.Entries.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x.FileName) == idStr);

                    if (entry == null)
                        throw new ArgumentException($"Map id {id} not found");

                    _entriesLinks.Add(id, entry);
                }

            // then retrieve the data source bound to the entry or create it
            var dlmReader = new DlmReader(_reader.ReadFile(entry)) {DecryptionKey = decryptionKey};

            return dlmReader.ReadMap();
        }

        public MapData GetMapData(long id)
        {
            if (!_offsetsTable.ContainsKey(id))
                throw new Exception($"Map id {id} not found");

            var stream = File.OpenRead(MapsDataFile);
            stream.Seek(_offsetsTable[id], SeekOrigin.Begin);

            return Serializer.DeserializeWithLengthPrefix<MapData>(stream, PrefixStyle.Fixed32);
        }

        public IEnumerable<MapData> EnumerateMaps()
        {
            var stream = File.OpenRead(MapsDataFile);
            stream.Seek(HeaderSize, SeekOrigin.Begin);

            while (stream.Position < _length + HeaderSize)
            {
                yield return Serializer.DeserializeWithLengthPrefix<MapData>(stream, PrefixStyle.Fixed32);
            }

            stream.Dispose();
        }


        public IEnumerable<DlmMap> EnumerateClientMaps(string key)
        {

            foreach (var entry in _reader.Entries)
            {
                // ReSharper disable once UnusedVariable
                if (entry.FileName != null)
                {
                    var id = int.Parse(Path.GetFileNameWithoutExtension(entry.FileName));
                }

                // then retrieve the data source bound to the entry or create it
                var dlmReader = new DlmReader(_reader.ReadFile(entry)) {DecryptionKey = key};

                yield return dlmReader.ReadMap();
            }
        }

        public IEnumerable<DlmMap> EnumerateClientMaps(DlmReader.KeyProvider decryptionKeyProvider)
        {

            foreach (var entry in _reader.Entries)
            {
                if (entry.FileName != null)
                {
                    // ReSharper disable once UnusedVariable
                    var id = int.Parse(Path.GetFileNameWithoutExtension(entry.FileName));
                }

                // then retrieve the data source bound to the entry or create it
                var dlmReader = new DlmReader(_reader.ReadFile(entry)) {DecryptionKeyProvider = decryptionKeyProvider};

                yield return dlmReader.ReadMap();
            }
        }

        // ReSharper disable once InconsistentNaming
        public ProgressionCounter Initialize(string d2pFile)
        {
            _reader = new D2PFile(d2pFile);

            if (!File.Exists(MapsDataFile))
                return BeginFileCreation();

            var stream = File.OpenRead(MapsDataFile);
            var reader = new BinaryReader(stream);

            if (new string(reader.ReadChars(4)) != "MAPS")
            {
                throw new Exception($"File {MapsDataFile} corrupted, delete it manually");
            }

            _fileVersion = reader.ReadUInt16();

            if (_fileVersion != CurrentMapsFileVersion)
            {
                Logger.Info("{0} outdated (file version :{1}, expected version {2})", MapsDataFile, _fileVersion, CurrentMapsFileVersion);
                reader.Dispose();
                return BeginFileCreation();
            }

            _tableOffset = reader.ReadUInt32();
            _length = reader.ReadUInt32();

            reader.BaseStream.Seek(_tableOffset, SeekOrigin.Begin);

            while (stream.Position < stream.Length)
            {
                _offsetsTable.TryAdd(reader.ReadInt64(), reader.ReadInt32());
            }

            reader.Dispose();
            return null;
        }

        public ProgressionCounter BeginFileCreation()
        {
            _writer = new BinaryWriter(File.Create(MapsDataFile));

            _writer.Write("MAPS".ToCharArray());
            _writer.Write(CurrentMapsFileVersion); // version
            _writer.Write(0); // table offset
            _writer.Write(0); // total length

            _progression = new ProgressionCounter(MapsCount);
            IEnumerable<DlmMap> maps = EnumerateClientMaps(Map.GenericDecryptionKey);
            int counter = 0;
            Task.Factory.StartNew(() =>
            {
                foreach (DlmMap map in maps)
                {
                    var position = ObjectDataManager.Instance.GetOrDefault<MapPosition>(map.Id);
                    _offsetsTable.TryAdd((int)map.Id, (int)_writer.BaseStream.Position);
                    Serializer.SerializeWithLengthPrefix(_writer.BaseStream, new MapData(map, position), PrefixStyle.Fixed32);

                    _progression.UpdateValue(counter++);
                }
            }).ContinueWith((task) => EndFileCreation());

            return _progression;
        }

        private void EndFileCreation()
        {
            _tableOffset = (uint)_writer.BaseStream.Position;
            _length = (uint)(_writer.BaseStream.Position - HeaderSize); // substracts the header

            foreach (var entry in _offsetsTable)
            {
                _writer.Write(entry.Key);
                _writer.Write(entry.Value);
            }

            _writer.Seek(6, SeekOrigin.Begin);
            _writer.Write(_tableOffset);
            _writer.Write(_length);

            _writer.Flush();
            _writer.Close();
            _writer.Dispose();
            _writer = null;

            _progression.NotifyEnded();
        }
    }
}