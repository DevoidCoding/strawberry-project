﻿#region License GNU GPL

// BotManager.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace Strawberry.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using JetBrains.Annotations;

    using NLog;

    using Strawberry.Common.Messages;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Reflection;

    public class BotManager : Singleton<BotManager>
    {
        #region Fields

        private readonly List<Bot> bots = new List<Bot>();

        #endregion

        #region Public Events

        public event Action<BotManager, Bot> BotAdded;

        public event Action<BotManager, Bot> BotRemoved;

        #endregion

        #region Public Properties

        public ReadOnlyCollection<Bot> Bots
        {
            get
            {
                lock (bots)
                {
                    return bots.AsReadOnly();
                }
            }
        }

        public DispatcherTask DispatcherTask { get; private set; }

        public MessageDispatcher InterBotDispatcher { get; private set; }

        #endregion

        #region Public Methods and Operators

        [UsedImplicitly]
        public static void LogNotified(string level, string caller, string message)
        {
            var bot = Instance.GetCurrentBot();

            bot?.NotifyMessageLog(LogLevel.FromString(level), caller, message);
        }

        public Bot GetCurrentBot()
        {
            var bot = Bots.FirstOrDefault(entry => entry.IsInContext);

            return bot;
        }

        public void Initialize()
        {
            InterBotDispatcher = new MessageDispatcher();
            DispatcherTask = new DispatcherTask(InterBotDispatcher, this);
            DispatcherTask.Start();
        }

        public void RegisterBot(Bot bot)
        {
            lock (bots)
            {
                var index = bots.Count;
                bots.Add(bot);
                bot.Id = index;
            }

            OnBotAdded(bot);
        }

        public void RemoveAll()
        {
            lock (bots)
            {
                foreach (var bot in bots.ToArray()) bot?.Dispose();
            }
        }

        #endregion

        #region Methods

        // DO NOT CALL THIS, CALL bot.Dispose() INSTEAD !!
        internal void RemoveBot(Bot bot)
        {
            bool removed;

            lock (bots)
            {
                if (!bot.Disposed)
                    bot.Dispose();

                removed = bots.Remove(bot);
            }

            if (removed) OnBotRemoved(bot);
        }

        private void OnBotAdded(Bot bot)
        {
            BotAdded?.Invoke(this, bot);
            InterBotDispatcher.Enqueue(new BotAddedMessage(bot));
        }

        private void OnBotRemoved(Bot bot)
        {
            BotRemoved?.Invoke(this, bot);
            InterBotDispatcher.Enqueue(new BotRemovedMessage(bot));
        }

        #endregion
    }
}