﻿using System;
using System.ComponentModel;
using System.Linq;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Core.Extensions;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Enums;
using Strawberry.Protocol.Messages;
using Strawberry.Protocol.Types;
using Version = Strawberry.Protocol.Types.Version;

namespace Strawberry.Common.Authentification
{
    public class ClientInformations : INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private DateTime m_referenceTimeChange;

        private DateTime? m_serverTimeReference;

        public DateTime ServerTime
        {
            get { return ServerTimeReference + (DateTime.Now - m_referenceTimeChange); }
        }

        public DateTime AccountCreation { get; set; }

        public int AccountId { get; set; }

        public PlayableBreedEnum[] AvailableBreeds { get; set; }

        public DateTime? BanEndDate { get; set; }

        public bool Banned { get; set; }

        public bool CanCreateNewCharacter { get; set; }

        public CharactersList CharactersList { get; set; }

        public sbyte CommunityId { get; set; }

        public sbyte[] ConnectionTicket { get; set; }

        public sbyte[] Credentials { get; set; }

        public bool HasAdminRights { get; set; }

        public IdentificationFailureReasonEnum? IdentificationFailureReason { get; set; }

        public bool IsHangedUp { get; set; }

        public string Lang { get; set; }

        public string Login { get; set; }

        public string Nickname { get; set; }

        public sbyte[] PublicRSAKey { get; set; }

        public bool Reconnect { get; set; }

        public Version RequieredVersion { get; set; }

        public string Salt { get; set; }

        public string SecretQuestion { get; set; }

        public byte[] HashKey { get; set; }

        public Server SelectedServer { get; set; }

        public string ServerAddress { get; set; }

        public ushort ServerPort { get; set; }

        public ServersList ServersList { get; set; }

        public TimeSpan ServerTimeOffset { get; set; }

        public DateTime ServerTimeReference
        {
            get { return m_serverTimeReference ?? DateTime.Now; }
            set
            {
                m_serverTimeReference = value;
                m_referenceTimeChange = DateTime.Now;
            }
        }

        public DateTime? SubscriptionEndDate { get; set; }

        public VersionExtended Version { get; set; }

        public PlayableBreedEnum[] VisibleBreeds { get; set; }

        public string Ticket { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanLogin()
        {
            return IsClientUpToDate() && !IsBanned();
        }

        public bool IsBanned()
        {
            return Banned && (BanEndDate > DateTime.Now);
        }

        public bool IsClientUpToDate()
        {
            // if RequieredVersion is null then it means that the server accept the client version
            return (RequieredVersion == null) || ((Version != null) &&
                                                  (RequieredVersion.Major == Version.Major) &&
                                                  (RequieredVersion.Minor == Version.Minor) &&
                                                  (RequieredVersion.Release == Version.Release) &&
                                                  (RequieredVersion.Revision == Version.Revision) &&
                                                  (RequieredVersion.Patch == Version.Patch));
        }

        public bool IsSubscribed()
        {
            return SubscriptionEndDate.HasValue && (SubscriptionEndDate > DateTime.Now);
        }

        public void Update(HelloConnectMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            Salt = msg.Salt;
            PublicRSAKey = msg.Key.ToArray();
            IsHangedUp = false;
        }

        public void Update(IdentificationMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            Version = msg.Version;
            Lang = msg.Lang;
            Credentials = msg.Credentials.ToArray();
            IsHangedUp = false;
        }

        public void Update(IdentificationFailedBannedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            BanEndDate = msg.BanEndDate.UnixTimestampToDateTime();
            Banned = true;

            logger.Warn("F*** I'm banned :( for {0}", BanEndDate - DateTime.Now);
        }

        public void Update(IdentificationFailedMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            IdentificationFailureReason = (IdentificationFailureReasonEnum) msg.Reason;

            if (IdentificationFailureReason == IdentificationFailureReasonEnum.Banned)
                Banned = true;
        }

        public void Update(IdentificationSuccessMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            HasAdminRights = msg.HasRights;
            Login = msg.Login;
            Nickname = msg.Nickname;
            AccountId = msg.AccountId;
            CommunityId = msg.CommunityId;
            SecretQuestion = msg.SecretQuestion;
            if (msg.SubscriptionEndDate > 0)
                SubscriptionEndDate = msg.SubscriptionEndDate.UnixTimestampToDateTime();

            AccountCreation = msg.SubscriptionEndDate.UnixTimestampToDateTime();
            IsHangedUp = false;
            OnPropertyChanged(nameof(Nickname));
        }

        public void Update(IdentificationFailedForBadVersionMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            RequieredVersion = msg.RequiredVersion;
        }

        public void Update(SelectedServerDataMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            SelectedServer = ObjectDataManager.Instance.Get<Server>(msg.ServerId);
            ConnectionTicket = msg.Ticket;
            ServerAddress = msg.Address;
            ServerPort = msg.Port;
            CanCreateNewCharacter = msg.CanCreateNewCharacter;
        }

        public void Update(ServersListMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            ServersList = new ServersList(msg);
        }

        public void Update(AccountCapabilitiesMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            VisibleBreeds = Enum.GetValues(typeof(PlayableBreedEnum)).
                Cast<PlayableBreedEnum>().Where(entry => ((msg.BreedsVisible >> ((int) entry - 1)) & 1) == 1).ToArray();
            AvailableBreeds = Enum.GetValues(typeof(PlayableBreedEnum)).
                Cast<PlayableBreedEnum>()
                .Where(entry => ((msg.BreedsAvailable >> ((int) entry - 1)) & 1) == 1)
                .ToArray();
        }

        public void Update(BasicTimeMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            ServerTimeOffset = new TimeSpan(0, 0, msg.TimezoneOffset, 0);
            ServerTimeReference = msg.Timestamp.UnixTimestampToDateTime();
        }

        public void Update(CharactersListMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            CharactersList = new CharactersList(msg);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        internal void Update(SystemMessageDisplayMessage message)
        {
            if (message.HangUp)
                IsHangedUp = message.HangUp;
        }

        internal void Update(CharacterSelectedForceMessage message)
        {
            Reconnect = true;
        }
    }
}