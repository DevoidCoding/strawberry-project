using System;
using System.Reflection;
using Strawberry.Core.Messages;
using Strawberry.Core.Network;
using Strawberry.Core.Threading;
using MessageDispatcher = Strawberry.Sniffer.Messages.MessageDispatcher;

namespace Strawberry.Sniffer.Models.Account.Network
{
    public class Network : SelfRunningTaskQueue, IDisposable
    {
        // Constructor
        public Network(Account account, IClient connection, MessageDispatcher dispatcher) : base(100)
        {
            Account = account;
            Connection = connection;
            Dispatcher = dispatcher;

            RegisterPacket<NetworkMessage>(HandleNetworkMessage, MessagePriority.VeryHigh);
        }

        // Properties
        public Account Account { get; set; }
        public IClient Connection { get; set; }

        public MessageDispatcher Dispatcher { get; set; }
        public bool ExpectedDisconnection { get; set; }

        public void Dispose()
        {
            if (!Running)
                return;

            if (Connection != null)
                Connection.Disconnect();

            if (Dispatcher != null)
                Dispatcher.Stop();

            Stop();
        }

        private void HandleNetworkMessage(Account account, NetworkMessage message)
        {
            if (message.From != ListenerEntry.Server)
            {
                if (MessageSent != null)
                    MessageSent(account, message);
            }
            else if (message.From == ListenerEntry.Server)
            {
                if (MessageReceived != null)
                    MessageReceived(account, message);
            }
        }

        public event Action<Account, NetworkMessage> MessageReceived;

        public event Action<Account, NetworkMessage> MessageSent;

        public void RegisterPacket<T>(Action<Account, T> handler, MessagePriority priority) where T : Message
        {
            Dispatcher.RegisterMessageHandler(Assembly.GetCallingAssembly(), typeof(T), handler, priority);
        }

        // Methods
        public void Send(Message message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            Dispatcher.Enqueue(message, this);
        }

        public void Send(NetworkMessage message, ListenerEntry dest)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            message.Destinations = dest;
            message.From = ListenerEntry.Local;

            Dispatcher.Enqueue(message, Account);
        }

        public void SendLocal(Message message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Dispatcher.Enqueue(message, this);
        }

        public void SendToClient(NetworkMessage message, int delay)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            CallDelayed(delay, () => SendToClient(message, false));
        }

        public void SendToClient(NetworkMessage message, bool direct = false)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (direct)
                Send(message, ListenerEntry.Client);
            else
                Send(message, ListenerEntry.Client | ListenerEntry.Local);
        }

        public void SendToServer(NetworkMessage message, int delay)
        {
            if (message == null) throw new ArgumentNullException("message");
            CallDelayed(delay, () => SendToServer(message, false));
        }


        public void SendToServer(NetworkMessage message, bool direct = false)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (direct)
                Send(message, ListenerEntry.Server);
            else
                Send(message, ListenerEntry.Server | ListenerEntry.Local);
        }

        // SelfRunningTaskQueue Overrides
        public override void Start()
        {
            if (Running)
                return;

            if (Dispatcher.Stopped)
                Dispatcher.Resume();

            base.Start();
        }

        public override void Stop()
        {
            if (!Running)
                return;

            if (Connection != null)
                Connection.Disconnect();

            if (Dispatcher != null)
                Dispatcher.Stop();

            base.Stop();
        }

        protected override void OnTick()
        {
            try
            {
                Dispatcher.ProcessDispatching(this);
            }
            catch (Exception ex)
            {
                // Note : Did we really need to Disconnect ?
                // Stop();
            }
        }
    }
}