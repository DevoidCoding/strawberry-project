﻿using Strawberry.Core.Network;
using Strawberry.Sniffer.Messages;
using Strawberry.Sniffer.ViewModels;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.Models.Account
{
    public class Account : ViewModelBase
    {
        private string _name;

        public Account(string name)
        {
            Name = name;
        }

        public Account(IClient connection, MessageDispatcher dispatcher)
        {
            Network = new Network.Network(this, connection, dispatcher);
        }

        public Network.Network Network { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
                OnPropertyChanged(nameof(AccountViewModel.Header));
            }
        }

        public string GameServerTicket { get; set; }
    }
}