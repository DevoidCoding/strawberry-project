﻿using System;
using Strawberry.Core.Network;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.Models.Messages
{
    public class MessageItem : ViewModelBase
    {
        public MessageItem(NetworkMessage message)
        {
            Message = message;
            DateTime = DateTime.Now;
        }

        public uint Id => Message.MessageID;
        public string Name => Message.GetType().Name;
        public ListenerEntry From => Message.From;
        public Type Type => Message.GetType();
        public NetworkMessage Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}