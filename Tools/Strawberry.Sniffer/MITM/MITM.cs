﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using NLog;
using Strawberry.Core.Extensions;
using Strawberry.Core.Messages;
using Strawberry.Core.Network;
using Strawberry.Protocol.Messages;
using Strawberry.Sniffer.Models.Account;
using Strawberry.Sniffer.ViewModels;

namespace Strawberry.Sniffer.MITM
{
    public class MITM
    {
        public static int ServerConnectionTimeout = 4000;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly MITMConfiguration m_configuration;

        private readonly List<Tuple<Account, SelectedServerDataMessage>> m_tickets =
            new List<Tuple<Account, SelectedServerDataMessage>>();

        public MITM(MITMConfiguration configuration, MessageReceiver messageReceiver, SnifferViewModel sniffer)
        {
            Sniffer = sniffer;
            m_configuration = configuration;
            AuthConnections = new ClientManager<ConnectionMITM>(
                DnsExtensions.GetIPEndPointFromHostName(m_configuration.FakeAuthHost, m_configuration.FakeAuthPort,
                    AddressFamily.InterNetwork), CreateAuthClient);
            WorldConnections = new ClientManager<ConnectionMITM>(
                DnsExtensions.GetIPEndPointFromHostName(m_configuration.FakeWorldHost, m_configuration.FakeWorldPort,
                    AddressFamily.InterNetwork), CreateWorldClient);

            AuthConnections.ClientConnected += OnAuthClientConnected;
            AuthConnections.ClientDisconnected += OnAuthClientDisconnected;
            WorldConnections.ClientConnected += OnWorldClientConnected;
            WorldConnections.ClientDisconnected += OnWorldClientDisconnected;

            MessageBuilder = messageReceiver;
        }

        public MessageReceiver MessageBuilder { get; set; }

        public ClientManager<ConnectionMITM> AuthConnections { get; }

        public ClientManager<ConnectionMITM> WorldConnections { get; }

        public SnifferViewModel Sniffer { get; set; }

        public void Start()
        {
            try
            {
                AuthConnections.Start();
                WorldConnections.Start();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void Stop()
        {
            AuthConnections.Stop();
            WorldConnections.Stop();
        }

        private ConnectionMITM CreateAuthClient(Socket socket)
        {
            if (socket == null)
                throw new ArgumentNullException("socket");
            var client = new ConnectionMITM(socket, MessageBuilder);
            client.MessageReceived += OnAuthClientMessageReceived;

            var dispatcher = new NetworkMessageDispatcher {Client = client, Server = client.Server};

            client.Account = new Account(client, dispatcher);


            client.Account.Network.RegisterPacket<SelectedServerDataMessage>(HandleSelectedServerDataMessage,
                MessagePriority.VeryHigh);
            client.Account.Network.RegisterPacket<SelectedServerDataExtendedMessage>(
                HandleSelectedServerDataExtendedMessage, MessagePriority.VeryHigh);

            return client;
        }

        private ConnectionMITM CreateWorldClient(Socket socket)
        {
            if (socket == null)
                throw new ArgumentNullException("socket");
            var client = new ConnectionMITM(socket, MessageBuilder);
            client.MessageReceived += OnWorldClientMessageReceived;

            return client;
        }

        private void OnAuthClientConnected(ConnectionMITM client)
        {
            client.Account.Network.Start();
            Sniffer.AddAccount(client.Account);

            try
            {
                client.BindToServer(m_configuration.RealAuthHost, m_configuration.RealAuthPort);
            }
            catch (Exception ex)
            {
                client.Account.Network.Stop();
                return;
            }
        }

        private void OnAuthClientDisconnected(ConnectionMITM client)
        {
            client.Account.Network.AddMessage(() =>
            {
                if (client.Account.Network.ExpectedDisconnection)
                {
                    client.Account.Network.ExpectedDisconnection = false;
                    var ticket = m_tickets.First();
                    //client.Account.Network.Stop();
                }
                else
                {
                    client.Account.Network.Dispose();
                }
            });
        }

        private void OnWorldClientConnected(ConnectionMITM client)
        {
            // todo : config
            client.Send(new ProtocolRequired(1818, 1820));
            client.Send(new HelloGameMessage());
        }

        private void OnWorldClientDisconnected(ConnectionMITM client)
        {
            if (client.Account.Network != null)
            {
                //Sniffer.RemoveAccount(client.Account);
                client.Account.Network.AddMessage(client.Account.Network.Dispose);
            }
        }

        private void OnAuthClientMessageReceived(Client client, NetworkMessage message)
        {
            if (!(client is ConnectionMITM))
                throw new ArgumentException("client is not of type ConnectionMITM");

            var mitm = client as ConnectionMITM;

            if (message is SelectedServerDataMessage)
            {
                var msg = (SelectedServerDataMessage) message;
                m_tickets.Add(Tuple.Create(mitm.Account,
                    new SelectedServerDataMessage(msg.ServerId, msg.Address, msg.Port, msg.CanCreateNewCharacter,
                        msg.Ticket)));
            }

            if (message is IdentificationSuccessMessage)
                HandleIdentificationSuccessMessage(mitm, message as IdentificationSuccessMessage);

            if (mitm.Account.Network == null)
                throw new NullReferenceException("mitm.Bot");

            mitm.Account.Network.Dispatcher.Enqueue(message, mitm.Account);
        }

        private void OnWorldClientMessageReceived(Client client, NetworkMessage message)
        {
            if (!(client is ConnectionMITM))
                throw new ArgumentException("client is not of type ConnectionMITM");

            var mitm = client as ConnectionMITM;

            if (message is HelloGameMessage)
            {
                var timer = ((ConnectionMITM) mitm.Account.Network.Connection).TimeOutTimer;
                if (timer != null)
                    timer.Dispose();
            }

            if (message is AuthenticationTicketMessage && mitm.Account == null)
            {
                // special handling to connect and retrieve the bot instance
                HandleAuthenticationTicketMessage(mitm, (AuthenticationTicketMessage) message);
            }
            else
            {
                if (mitm.Account.Network == null)
                    throw new NullReferenceException("mitm.Bot");

                if (mitm.Account.Network.Dispatcher.Stopped)
                    throw new Exception("Enqueue a message but the dispatcher is stopped !");

                mitm.Account.Network.Dispatcher.Enqueue(message, mitm.Account);
            }
        }

        private void HandleAuthenticationTicketMessage(ConnectionMITM client, AuthenticationTicketMessage message)
        {
            var tuple = m_tickets.First();

            m_tickets.Remove(tuple);

            client.Account = tuple.Item1;
            client.Account.Network.Connection = client;
            client.Account.Network.CancelAllMessages(); // avoid to handle message from the auth client.
            client.Account.Network.Start();

            ((NetworkMessageDispatcher) client.Account.Network.Dispatcher).Client = client;
            ((NetworkMessageDispatcher) client.Account.Network.Dispatcher).Server = client.Server;

            try
            {
                client.BindToServer(tuple.Item2.Address, tuple.Item2.Port);
                client.Account.GameServerTicket = message.Ticket;
            }
            catch (Exception)
            {
                client.Account.Network.Stop();
                return;
            }

            //client.TimeOutTimer = client.Account.Network.CallDelayed(ServerConnectionTimeout * 1000, () => OnServerConnectionTimedOut(client));
        }

        private void HandleIdentificationSuccessMessage(ConnectionMITM client, IdentificationSuccessMessage message)
        {
            client.Account.Name = message.Login;
            // TODO : Init client.Account.InitializeConfig();
        }

        private void OnServerConnectionTimedOut(ConnectionMITM connection)
        {
            if (connection.Server.IsConnected)
            {
                // unblock the connection (fix #14)
                connection.SendToServer(new BasicPingMessage());
            }
        }

        private void HandleSelectedServerDataMessage(Account bot, SelectedServerDataMessage message)
        {
            if (message.From == ListenerEntry.Server)
            {
                message.Address = m_configuration.FakeWorldHost;
                message.Port = (ushort) m_configuration.FakeWorldPort;

                var tuple = m_tickets.First();
                bot.Network.ExpectedDisconnection = true;
                //tuple.Item1.GameServerTicket = message.Ticket;
            }
        }

        private void HandleSelectedServerDataExtendedMessage(Account bot, SelectedServerDataExtendedMessage message)
        {
            if (message.From == ListenerEntry.Server)
            {
                message.Address = m_configuration.FakeWorldHost;
                message.Port = (ushort) m_configuration.FakeWorldPort;

                var tuple = m_tickets.First();
                tuple.Item1.Network.ExpectedDisconnection = true;
                //tuple.Item1.GameServerTicket = message.ticket;
            }
        }
    }
}