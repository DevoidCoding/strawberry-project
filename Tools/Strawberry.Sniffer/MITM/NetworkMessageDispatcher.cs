﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Strawberry.Core.Messages;
using Strawberry.Core.Network;
using Strawberry.Sniffer.Models.Account;
using MessageDispatcher = Strawberry.Sniffer.Messages.MessageDispatcher;

namespace Strawberry.Sniffer.MITM
{
    public class NetworkMessageDispatcher : MessageDispatcher
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly List<Message> m_logs = new List<Message>();

        public IClient Client { get; set; }

        public IClient Server { get; set; }

        protected override void Dispatch(Message message, object token)
        {
            if (message == null) throw new ArgumentNullException("message");
            if (message is NetworkMessage)
                Dispatch(message as NetworkMessage, token);
            else
                base.Dispatch(message, token);
        }

        protected void Dispatch(NetworkMessage message, object token)
        {
            if (message == null) throw new ArgumentNullException("message");

            if (message.Destinations.HasFlag(ListenerEntry.Local))
            {
                try
                {
                    InternalDispatch(message, token);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }

            if (Client != null && message.Destinations.HasFlag(ListenerEntry.Client))
            {
                Client.Send(message);
            }

            if (Server != null && message.Destinations.HasFlag(ListenerEntry.Server))
            {
                Server.Send(message);
            }

            message.OnDispatched();
            OnMessageDispatched(message);
        }

        private void InternalDispatch(NetworkMessage message, object token)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            var handlers = GetHandlers(message.GetType(), token).ToArray();
                // have to transform it into a collection if we want to add/remove handler


            foreach (var handler in handlers)
            {
                handler.Handler((Account) token, message);

                if (message.Canceled)
                    break;
            }
        }
    }
}