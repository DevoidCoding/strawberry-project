﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Core.Threading;

namespace Strawberry.Sniffer.MITM
{
    public class Injector : SelfRunningTaskQueue
    {
        public Injector(int updateInterval) : base(updateInterval)
        {
        }

        protected override void OnTick()
        {
        }
    }
}
