﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Strawberry.Core.Network;
using Strawberry.Protocol.Messages;
using Strawberry.Sniffer.Models.Messages;

namespace Strawberry.Sniffer.Styles
{
    public class MessageFromStyle : StyleSelector
    {
        private readonly Type[] _hash =
        {
            typeof(GameActionFightCastRequestMessage),
            typeof(BasicLatencyStatsMessage),
            typeof(ChatClientMultiMessage),
            typeof(ChatClientMultiWithObjectMessage),
            typeof(ChatClientPrivateMessage),
            typeof(ChatClientPrivateWithObjectMessage),
            typeof(GameCautiousMapMovementRequestMessage),
            typeof(GameMapMovementRequestMessage),
            typeof(GameRolePlayPlayerFightRequestMessage),
            typeof(GameRolePlayArenaSwitchToFightServerMessage),
            typeof(NpcGenericActionRequestMessage),
            typeof(InteractiveUseRequestMessage),
            typeof(InteractiveUseWithParamRequestMessage),
            typeof(ExchangePlayerMultiCraftRequestMessage),
            typeof(ExchangePlayerRequestMessage),
            typeof(ClientKeyMessage)
        };

        public Style ServerStyle { get; set; }
        public Style ClientStyle { get; set; }
        public Style HashStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            var messageItem = item as MessageItem;
            if (messageItem == null) return null;

            if (_hash.Contains(messageItem.Type)) return HashStyle;
            return messageItem.From == ListenerEntry.Server ? ServerStyle : ClientStyle;
        }
    }
}