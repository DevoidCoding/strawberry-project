﻿using System.Windows;
using System.Windows.Controls;
using Strawberry.Sniffer.ViewModels;

namespace Strawberry.Sniffer.Views
{
    /// <summary>
    ///     Logique d'interaction pour AccountView.xaml
    /// </summary>
    public partial class AccountView : UserControl, IView<AccountViewModel>
    {
        public AccountView()
        {
            Loaded += OnLoaded;
            InitializeComponent();
        }

        public AccountViewModel ViewModel { get; set; }

        object IView.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (AccountViewModel) value; }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (DataContext != null && ViewModel == null)
            {
                ViewModel = (AccountViewModel) DataContext;
                ViewModel.View = this;
            }
        }
    }
}