﻿using Strawberry.Sniffer.ViewModels;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.Views
{
    /// <summary>
    ///     Interaction logic for SnifferView.xaml
    /// </summary>
    public partial class SnifferView : RadWindow, IView<SnifferViewModel>
    {
        public SnifferView()
        {
            VisualStudio2013Palette.LoadPreset(VisualStudio2013Palette.ColorVariation.Dark);
            DataContext = ViewModel = new SnifferViewModel();
            InitializeComponent();
            ViewModel.View = this;
        }

        object IView.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (SnifferViewModel) value; }
        }

        public SnifferViewModel ViewModel { get; set; }
    }
}