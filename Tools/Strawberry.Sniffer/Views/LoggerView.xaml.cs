﻿using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using NLog;
using Strawberry.Sniffer.ViewModels;

namespace Strawberry.Sniffer.Views
{
    /// <summary>
    ///     Logique d'interaction pour LoggerView.xaml
    /// </summary>
    public partial class LoggerView : UserControl, IView<LoggerViewModel>
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        public LoggerView()
        {
            DataContext = ViewModel = LoggerViewModel.Instance;
            InitializeComponent();
            ViewModel.View = this;
        }

        object IView.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (LoggerViewModel) value; }
        }

        public LoggerViewModel ViewModel { get; set; }

        public void AppendText(string text)
        {
            var tr = new TextRange(Console.Document.ContentEnd, Console.Document.ContentEnd)
            {
                Text = text
            };
        }

        public void AppendText(string text, Color foreColor)
        {
            var tr = new TextRange(Console.Document.ContentEnd, Console.Document.ContentEnd)
            {
                Text = text
            };

            tr.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(foreColor));
        }

        public void AppendText(string text, Color foreColor, Color backColor)
        {
            var tr = new TextRange(Console.Document.ContentEnd, Console.Document.ContentEnd)
            {
                Text = text
            };

            tr.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(foreColor));
            tr.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(backColor));
        }
    }
}