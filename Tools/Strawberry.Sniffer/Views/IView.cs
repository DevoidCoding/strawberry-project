﻿using System.Windows.Threading;
using Strawberry.Sniffer.ViewModels;

namespace Strawberry.Sniffer.Views
{
    public interface IView<T> : IView
        where T : IViewModel
    {
        new T ViewModel { get; set; }
    }

    public interface IView
    {
        object ViewModel { get; set; }

        Dispatcher Dispatcher { get; }
    }
}