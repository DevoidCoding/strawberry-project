﻿using System;
using System.Windows;
using Strawberry.Sniffer.Views;

namespace Strawberry.Sniffer
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            new SnifferView().Show();
            base.OnStartup(e);
            AppDomain.CurrentDomain.UnhandledException += Dispatcher_UnhandledException;
        }

        private void Dispatcher_UnhandledException(object sender, object e)
        {
            throw new System.NotImplementedException();
        }
    }
}