﻿namespace Strawberry.Sniffer
{
    public class Constants
    {
        public const string BotAuthHost = "127.0.0.1";
        public const int BotAuthPort = 5555;
        public const string BotWorldHost = "127.0.0.1";
        public const int BotWorldPort = 5556;
        public const string RealAuthHost = "213.248.126.41";
        public const int RealAuthPort = 443;
    }
}