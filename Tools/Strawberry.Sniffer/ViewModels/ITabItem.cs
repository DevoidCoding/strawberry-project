﻿namespace Strawberry.Sniffer.ViewModels
{
    public interface ITabItem
    {
        string Header { get; set; }
    }
}