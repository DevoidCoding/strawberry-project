﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Strawberry.Core.Threading;
using Strawberry.Protocol.Messages;
using Strawberry.Sniffer.MITM;
using Strawberry.Sniffer.Models.Account;
using Strawberry.Sniffer.Views;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.ViewModels
{
    public class SnifferViewModel : ViewModelBase, IViewModel<SnifferView>
    {
        private List<int> _dofusInstances;
        private MessageReceiver _messageReceiver;
        private MITM.MITM _mitm;

        private int _selectedAccountIndex;
        private SelfRunningTaskQueue _timer;

        public SnifferViewModel()
        {
            TabItems = new ObservableCollection<ITabItem>();

            InitializeMessageReceiver();
            InitializeMitm();
            InitializeDofusInjection();

            InitializeLogger();
        }

        public ObservableCollection<ITabItem> TabItems { get; set; }

        public int SelectedAccountIndex
        {
            get { return _selectedAccountIndex; }
            set
            {
                _selectedAccountIndex = value;
                OnPropertyChanged("SelectedAccountIndex");
            }
        }

        object IViewModel.View
        {
            get { return View; }
            set { View = (SnifferView) value; }
        }

        public SnifferView View { get; set; }

        private void InitializeLogger()
        {
            TabItems.Add(LoggerViewModel.Instance);
        }

        private void InitializeDofusInjection()
        {
            _dofusInstances = new List<int>();
            _timer = new Injector(500);
            _timer.CallPeriodically(1000, DofusInject);
            _timer.Start();
        }

        private void DofusInject()
        {
            var processLst = Process.GetProcessesByName("Dofus").Where(e => !_dofusInstances.Contains(e.Id)).ToList();
            var dllInjector = new DllInjector();
            foreach (var prc in processLst)
            {
                if (_dofusInstances.Contains(prc.Id)) continue;
                dllInjector.Inject((uint) prc.Id, Path.Combine(Environment.CurrentDirectory, @"No.Ankama.dll"));
                _dofusInstances.Add(prc.Id);
            }
        }

        private void InitializeMessageReceiver()
        {
            _messageReceiver = new MessageReceiver();
            _messageReceiver.Initialize();
        }

        private void InitializeMitm()
        {
            var mitmConfiguration = new MITMConfiguration
            {
                FakeAuthHost = Constants.BotAuthHost,
                FakeAuthPort = Constants.BotAuthPort,
                FakeWorldHost = Constants.BotWorldHost,
                FakeWorldPort = Constants.BotWorldPort,
                RealAuthHost = Constants.RealAuthHost,
                RealAuthPort = Constants.RealAuthPort
            };
            _mitm = new MITM.MITM(mitmConfiguration, _messageReceiver, this);
            _mitm.Start();
        }

        public void RemoveAccount(Account account)
        {
            View.Dispatcher.BeginInvoke(new Action(() =>
            {
                TabItems.Remove(
                    TabItems.FirstOrDefault(e => e is AccountViewModel && ((AccountViewModel) e).Account == account));
                SelectedAccountIndex = TabItems.Count - 1;
            }));
        }

        public void AddAccount(Account account)
        {
            View.Dispatcher.BeginInvoke(new Action(() =>
            {
                TabItems.Add(new AccountViewModel(account));
                SelectedAccountIndex = TabItems.Count - 1;
            }));
        }
    }
}