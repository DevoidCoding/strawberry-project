﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Media;
using NLog;
using Strawberry.Sniffer.Views;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.ViewModels
{
    public class LoggerViewModel : ViewModelBase, IViewModel<LoggerView>, ITabItem
    {
        private static readonly Dictionary<LogLevel, Color> ColorsRules = new Dictionary<LogLevel, Color>
        {
            {LogLevel.Debug, Colors.Gray},
            {LogLevel.Trace, Colors.DarkGray},
            {LogLevel.Info, Colors.Black},
            {LogLevel.Warn, Colors.Orange},
            {LogLevel.Error, Colors.Red},
            {LogLevel.Fatal, Colors.DarkRed}
        };

        private LoggerViewModel()
        {
            Header = "Logger";
            MyClass = new MyClass();
            MyClass.Names = new List<string> {"Lol", "Lel"};
            MyClass.MyClasses = new List<MyClass>
            {
                new MyClass
                {
                    Names = new List<string> {"Lol", "Lel"},
                    MyClasses = new List<MyClass> {new MyClass(), new MyClass()}
                },
                new MyClass()
            };
        }

        public static LoggerViewModel Instance
        {
            get { return SingletonAllocator.instance; }
            protected set { SingletonAllocator.instance = value; }
        }

        public MyClass MyClass { get; set; }
        public string Header { get; set; }

        object IViewModel.View
        {
            get { return View; }
            set { View = (LoggerView) value; }
        }

        public LoggerView View { get; set; }

        public static void LogNotified(string level, string caller, string message)
        {
            Instance.Log(level, caller, message);
        }

        private void Log(string level, string caller, string message)
        {
            var color = ColorsRules[LogLevel.FromString(level)];

            View.Dispatcher.BeginInvoke(
                new Action(
                    () =>
                        View.AppendText(
                            $"[{DateTime.Now:hh:mm:ss}] {caller} : {message}\n",
                            color)));
        }

        #region Nested type: SingletonAllocator

        internal static class SingletonAllocator
        {
            internal static LoggerViewModel instance;

            static SingletonAllocator()
            {
                CreateInstance(typeof(LoggerViewModel));
            }

            public static LoggerViewModel CreateInstance(Type type)
            {
                var ctorsPublic = type.GetConstructors(
                    BindingFlags.Instance | BindingFlags.Public);

                if (ctorsPublic.Length > 0)
                    return instance = (LoggerViewModel) Activator.CreateInstance(type);

                var ctorNonPublic = type.GetConstructor(
                    BindingFlags.Instance | BindingFlags.NonPublic, null, Type.EmptyTypes, new ParameterModifier[0]);

                if (ctorNonPublic == null)
                {
                    throw new Exception(
                        type.FullName +
                        " doesn't have a private/protected constructor so the property cannot be enforced.");
                }

                try
                {
                    return instance = (LoggerViewModel) ctorNonPublic.Invoke(new object[0]);
                }
                catch (Exception e)
                {
                    throw new Exception(
                        "The Singleton couldnt be constructed, check if " + type.FullName + " has a default constructor",
                        e);
                }
            }
        }

        #endregion
    }

    public class MyClass
    {
        public IEnumerable<string> Names { get; set; }
        public IEnumerable<MyClass> MyClasses { get; set; }
    }
}