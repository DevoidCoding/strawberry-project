﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using NLog;
using Strawberry.Core.Collections;
using Strawberry.Core.Messages;
using Strawberry.Core.Network;
using Strawberry.Protocol.Messages;
using Strawberry.Sniffer.Models.Account;
using Strawberry.Sniffer.Models.Messages;
using Strawberry.Sniffer.Views;
using Telerik.Windows.Controls;

namespace Strawberry.Sniffer.ViewModels
{
    public class AccountViewModel : ViewModelBase, IViewModel<AccountView>, ITabItem
    {
        private string _header = "";
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public AccountViewModel(Account account)
        {
            Messages = new AsyncObservableCollection<MessageItem>();

            Account = account;
            Account.Network.RegisterPacket<NetworkMessage>(HandleNetworkMessage, MessagePriority.Normal);
            Account.Network.RegisterPacket<IdentificationSuccessMessage>(OnIndentificationSuccesMessage,
                MessagePriority.VeryHigh);
            Account.Network.RegisterPacket<CharacterSelectedSuccessMessage>(OnCharacterSelectedSuccessMessage,
                MessagePriority.VeryHigh);
            Account.Network.RegisterPacket<HaapiApiKeyMessage>(OnHaapiApiKeyMessage, MessagePriority.VeryHigh);
            Account.Network.RegisterPacket<HaapiApiKeyRequestMessage>(OnHaapiApiKeyRequestMessage,
                MessagePriority.VeryHigh);
            Account.Network.RegisterPacket<GameMapMovementRequestMessage>(OnGameMapMovementRequestMessage,
                MessagePriority.VeryHigh);
            Account.Network.RegisterPacket<ClientKeyMessage>(OnCKMHandler, MessagePriority.VeryHigh);
        }

        private void OnCKMHandler(Account arg1, ClientKeyMessage arg2)
        {

        }

        private void OnGameMapMovementRequestMessage(Account arg1, GameMapMovementRequestMessage message)
        {

        }

        public ObservableCollection<MessageItem> Messages { get; set; }

        public Account Account { get; set; }

        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged(nameof(Header));
            }
        }

        object IViewModel.View
        {
            get { return View; }
            set { View = (AccountView) value; }
        }

        public AccountView View { get; set; }

        private void OnHaapiApiKeyMessage(Account arg1, HaapiApiKeyMessage arg2)
        {
            _logger.Info($"Token : {arg2.Token}, blocked");
            //arg2.BlockProgression();
        }

        private void OnHaapiApiKeyRequestMessage(Account arg1, HaapiApiKeyRequestMessage arg2)
        {
            _logger.Info("Token Requested");
        }

        private void OnCharacterSelectedSuccessMessage(Account account,
            CharacterSelectedSuccessMessage characterSelectedSuccessMessage)
        {
            Header = $"{characterSelectedSuccessMessage.Infos.Name} ({Account.Name})";
        }

        private void OnIndentificationSuccesMessage(Account account,
            IdentificationSuccessMessage identificationSuccessMessage)
        {
            Header = identificationSuccessMessage.Nickname;
        }

        private void HandleNetworkMessage(Account account, NetworkMessage networkMessage)
        {
            Messages.Add(new MessageItem(networkMessage));
        }

        private RelayCommand _simulateCommand;

        /// <summary>
        /// Gets the SimulateCommand.
        /// </summary>
        public RelayCommand SimulateCommand => _simulateCommand
                                               ?? (_simulateCommand = new RelayCommand(ExecuteSimulateCommand));

        private void ExecuteSimulateCommand()
        {
            Account.Network.SendToClient(new CheckFileRequestMessage("DofusInvoker.swf", 0));
            Account.Network.SendToClient(new CheckFileRequestMessage("DofusInvoker.swf", 1));
        }
    }
}