﻿using Strawberry.Sniffer.Views;

namespace Strawberry.Sniffer.ViewModels
{
    public interface IViewModel<T> : IViewModel
        where T : IView
    {
        new T View { get; set; }
    }

    public interface IViewModel
    {
        object View { get; set; }
    }
}