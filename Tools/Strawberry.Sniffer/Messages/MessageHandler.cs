﻿using System;
using Strawberry.Core.Messages;
using Strawberry.Sniffer.Models.Account;

namespace Strawberry.Sniffer.Messages
{
    public class MessageHandler
    {
        // Constructors
        public MessageHandler(Type messageType, Action<Account, object> handler, string pluginName, Type tokenType,
            MessagePriority priority)
        {
            MessageType = messageType;
            Handler = handler;
            PluginName = pluginName;
            TokenType = tokenType;
            Priority = priority;
        }

        public Type MessageType { get; set; }
        public Action<Account, object> Handler { get; set; }
        public string PluginName { get; set; }
        public Type TokenType { get; set; }
        public MessagePriority Priority { get; set; }
    }
}