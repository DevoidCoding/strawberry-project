﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using Strawberry.Core.Messages;
using Strawberry.Core.Network;
using Strawberry.Sniffer.Models.Account;

namespace Strawberry.Sniffer.Messages
{
    /// <summary>
    ///     Classic message dispatcher
    /// </summary>
    /// <typeparam name="T">Messsage dispatcher type</typeparam>
    public class MessageDispatcher
    {
        private Stopwatch _spy;

        private int currentThreadId;
        private bool dispatching;

        private readonly Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>> handlers =
            new Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>>();

        private readonly ManualResetEventSlim messageEnqueuedEvent = new ManualResetEventSlim(false);

        private readonly SortedDictionary<MessagePriority, Queue<Tuple<Message, object>>> messagesToDispatch =
            new SortedDictionary<MessagePriority, Queue<Tuple<Message, object>>>();

        private readonly ManualResetEventSlim resumeEvent = new ManualResetEventSlim(true);

        public MessageDispatcher()
        {
            foreach (var value in Enum.GetValues(typeof(MessagePriority)))
            {
                messagesToDispatch.Add((MessagePriority) value, new Queue<Tuple<Message, object>>());
            }
        }

        public object CurrentProcessor { get; private set; }

        public int CurrentThreadId
        {
            get { return currentThreadId; }
        }

        public bool Stopped { get; private set; }

        /// <summary>
        ///     Says how many milliseconds elapsed since last message.
        /// </summary>
        public long DelayFromLastMessage
        {
            get
            {
                if (_spy == null) _spy = Stopwatch.StartNew();
                return _spy.ElapsedMilliseconds;
            }
        }

        public event Action<MessageDispatcher, Message> MessageDispatched;

        protected void OnMessageDispatched(Message message)
        {
            var evnt = MessageDispatched;
            if (evnt != null)
                MessageDispatched(this, message);
        }

        public void Enqueue(Message message, bool executeIfCan = true)
        {
            Enqueue(message, null, executeIfCan);
        }

        public virtual void Enqueue(Message message, object token, bool executeIfCan = true)
        {
            if (executeIfCan && IsInDispatchingContext())
            {
                Dispatch(message, token);
            }
            else
            {
                lock (messageEnqueuedEvent)
                {
                    messagesToDispatch[message.Priority].Enqueue(Tuple.Create(message, token));

                    if (!dispatching)
                        messageEnqueuedEvent.Set();
                }
            }
        }

        public bool IsInDispatchingContext()
        {
            return Thread.CurrentThread.ManagedThreadId == currentThreadId &&
                   CurrentProcessor != null;
        }

        public void RegisterMessageHandler<T>(Assembly assembly, Type t, Action<Account, T> handler,
            MessagePriority priority) where T : Message
        {
            if (!handlers.ContainsKey(assembly))
                handlers.Add(assembly, new Dictionary<Type, List<MessageHandler>>());

            if (!handlers[assembly].ContainsKey(t))
                handlers[assembly].Add(t, new List<MessageHandler>());

            handlers[assembly][t].Add(new MessageHandler(t, Convert(handler), assembly.GetName().Name,
                handler.Method.GetParameters()[0].ParameterType, priority));
        }

        public Action<Account, object> Convert<T>(Action<Account, T> myActionT)
        {
            if (myActionT == null) return null;
            return (a, o) => myActionT(a, (T) o);
        }

        public void ProcessDispatching(object processor)
        {
            if (Stopped)
                return;

            if (Interlocked.CompareExchange(ref currentThreadId, Thread.CurrentThread.ManagedThreadId, 0) == 0)
            {
                CurrentProcessor = processor;
                dispatching = true;

                var copy = messagesToDispatch.ToArray();
                foreach (var keyPair in copy)
                {
                    if (Stopped)
                        break;

                    while (keyPair.Value.Count != 0)
                    {
                        if (Stopped)
                            break;

                        var message = keyPair.Value.Dequeue();

                        if (message != null)
                            Dispatch(message.Item1, message.Item2);
                    }
                }

                CurrentProcessor = null;
                dispatching = false;
                Interlocked.Exchange(ref currentThreadId, 0);
            }

            lock (messagesToDispatch)
            {
                if (messagesToDispatch.Sum(x => x.Value.Count) > 0)
                    messageEnqueuedEvent.Set();
                else
                    messageEnqueuedEvent.Reset();
            }
        }

        protected virtual void Dispatch(Message message, object token)
        {
            try
            {
                foreach (var handler in GetHandlers(message.GetType(), token).ToArray())
                    // have to transform it into a collection if we want to add/remove handler
                {
                    handler.Handler((Account) token, message);

                    if (message.Canceled)
                        break;
                }

                message.OnDispatched();
                OnMessageDispatched(message);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot dispatch {message}", ex);
            }
        }

        protected IEnumerable<MessageHandler> GetHandlers(Type messageType, object token)
        {
            var handlersEnumerable = new List<MessageHandler>();

            foreach (var list in handlers.Values.ToList())
                // ToArray : to avoid error if handler are added in the same time
            {
                List<MessageHandler> handlersList;

                if (list.TryGetValue(typeof(NetworkMessage), out handlersList))
                    foreach (var handler in handlersList)
                        if (token == null || handler.TokenType.IsInstanceOfType(token))
                            handlersEnumerable.Add(handler);

                if (list.TryGetValue(messageType, out handlersList))
                    foreach (var handler in handlersList)
                        if (token == null || handler.TokenType.IsInstanceOfType(token))
                            handlersEnumerable.Add(handler);
            }

            return handlersEnumerable;

            // note : disabled yet.

            // recursivity to handle message from base class
            //if (messageType.BaseType != null && messageType.BaseType.IsSubclassOf(typeof(Message)))
            //    foreach (var handler in GetHandlers(messageType.BaseType, token))
            //    {
            //        if (handler.Attribute.HandleChildMessages)
            //            yield return handler;
            //    }
        }

        /// <summary>
        ///     Block the current thread until a message is enqueued
        /// </summary>
        public void Wait()
        {
            if (Stopped)
                resumeEvent.Wait();

            if (messagesToDispatch.Sum(x => x.Value.Count) > 0)
                return;

            messageEnqueuedEvent.Wait();
        }

        public void Resume()
        {
            if (!Stopped)
                return;

            Stopped = false;
            resumeEvent.Set();
        }

        public void Stop()
        {
            if (Stopped)
                return;

            Stopped = true;
            resumeEvent.Reset();
        }

        public void Dispose()
        {
            Stop();

            foreach (var messages in messagesToDispatch)
            {
                messages.Value.Clear();
            }
        }

        /// <summary>
        ///     Reset timer for last received message
        /// </summary>
        protected void ActivityUpdate()
        {
            if (_spy == null)
                _spy = Stopwatch.StartNew();
            else
                _spy.Restart();
        }
    }
}