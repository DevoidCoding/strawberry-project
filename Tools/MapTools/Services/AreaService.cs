﻿using System.Collections.Generic;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Protocol.Data;

namespace MapTools.Services
{
    public class AreaService : IAreaService
    {
        #region Private Fields

        private readonly List<AreaModel> _areaModels = new List<AreaModel>();

        #endregion Private Fields

        #region Public Methods

        public AreaModel Add(Area area, SuperAreaModel superAreaModel)
        {
            var areaModel = new AreaModel(area, superAreaModel);
            _areaModels.Add(areaModel);
            superAreaModel.AreaModels.Add(areaModel);
            return areaModel;
        }

        public AreaModel Get(int id) => _areaModels.Find(e => e.Id == id);

        public IList<AreaModel> GetAreaModels() => _areaModels.AsReadOnly();

        public bool Remove(int id)
        {
            var areaModel = Get(id);
            return areaModel.SuperAreaModel.AreaModels.Remove(areaModel) && _areaModels.Remove(areaModel);
        }

        #endregion Public Methods
    }
}