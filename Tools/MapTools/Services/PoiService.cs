﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Newtonsoft.Json;

namespace MapTools.Services
{
    public class PoiService : IPoiService
    {
        #region Public Constructors

        public PoiService()
        {
            _gfxService = SimpleIoc.Default.GetInstance<IGfxService>();
        }

        #endregion Public Constructors

        #region Private Fields

        private readonly IGfxService _gfxService;
        private Dictionary<PoiModel, List<GfxModel>> _pois = new Dictionary<PoiModel, List<GfxModel>>();

        #endregion Private Fields

        #region Public Methods

        public void Add(PoiModel poi)
        {
            if (!_pois.ContainsKey(poi))
                _pois[poi] = new List<GfxModel>();
        }

        public void Add(PoiModel poi, GfxModel gfx)
        {
            Add(poi);
            if (_pois[poi] == null)
                _pois[poi] = new List<GfxModel>();
            _pois[poi].Add(gfx);
        }

        public void Add(int poiId, GfxModel gfx) => Add(Get(poiId), gfx);

        public void Add(int poiId, int gfxId) => Add(Get(poiId), _gfxService.Get(gfxId));

        public bool Deserialize()
        {
            const string path = "pois.json";
            if (!File.Exists(path))
                return false;
            var json = File.ReadAllText(path);
            _pois = JsonConvert.DeserializeObject<KeyValuePair<PoiModel, List<GfxModel>>[]>(json).ToDictionary(e => e.Key, e => e.Value);
            foreach (var keyValuePair in _pois)
                keyValuePair.Key.GfxModels = keyValuePair.Value;
            return true;
        }

        public PoiModel Get(int poiId) => _pois.FirstOrDefault(e => e.Key.Id == poiId).Key;

        public IEnumerable<GfxModel> GetGfxs(PoiModel poi) => !_pois.ContainsKey(poi) ? new List<GfxModel>() : _pois[poi].AsEnumerable();

        public IEnumerable<GfxModel> GetGfxs(int poiId) => _pois[Get(poiId)].AsEnumerable();

        public IEnumerable<PoiModel> GetPois() => _pois.Keys.AsEnumerable();

        public bool Remove(PoiModel poi) => _pois.Remove(poi);

        public void Serialize() => File.WriteAllText("pois.json", JsonConvert.SerializeObject(_pois.ToArray(), Formatting.Indented));

        public void Update(Dictionary<PoiModel, List<GfxModel>> dictionary) => _pois = new Dictionary<PoiModel, List<GfxModel>>(dictionary);

        #endregion Public Methods
    }
}