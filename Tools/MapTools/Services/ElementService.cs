﻿using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Protocol.Tools.Dlm;
using Strawberry.Protocol.Tools.Ele.Datas;

namespace MapTools.Services
{
    public class ElementService : IElementService
    {
        #region Public Constructors

        public ElementService()
        {
            _mapService = SimpleIoc.Default.GetInstance<IMapService>();
        }

        #endregion Public Constructors

        #region Private Fields

        private readonly Dictionary<MapModel, List<GraphicalElementModel>> _elements = new Dictionary<MapModel, List<GraphicalElementModel>>();
        private readonly IMapService _mapService;

        #endregion Private Fields

        #region Public Methods

        public void Add(int elementId, int mapId)
        {
            _mapService.Get(mapId);
        }

        public void Add(DlmGraphicalElement dlmGraphicalElement, MapModel map, NormalGraphicalElementData elementData)
        {
            var element = new GraphicalElementModel(dlmGraphicalElement, map, elementData);
            Add(element);
        }

        public void Add(GraphicalElementModel element)
        {
            if (!_elements.ContainsKey(element.MapModel))
                _elements[element.MapModel] = new List<GraphicalElementModel>();
            _elements[element.MapModel].Add(element);
        }

        public GraphicalElementModel Get(int elementId, int mapId)
        {
            var mapModel = _mapService.Get(mapId);
            return _elements[mapModel].FirstOrDefault(e => e.ElementId == elementId);
        }

        public IEnumerable<GraphicalElementModel> GetAll()
        {
            return _elements.SelectMany(e => e.Value);
        }

        public IEnumerable<GraphicalElementModel> GetElementByGfxId(int gfxId)
        {
            return _elements.SelectMany(e => e.Value).Where(e => e.GfxId == gfxId);
        }

        public IEnumerable<GraphicalElementModel> GetElementsByMapId(int mapId)
        {
            if (!_elements.TryGetValue(_mapService.Get(mapId), out var element)) return Enumerable.Empty<GraphicalElementModel>();
            return element;
        }

        public bool Remove(GraphicalElementModel element)
        {
            return _elements[element.MapModel].Remove(element);
        }

        public bool Remove(int elementId, int mapId)
        {
            var element = Get(elementId, mapId);
            return _elements[element.MapModel].Remove(element);
        }

        #endregion Public Methods
    }
}