﻿using System.Collections.Generic;
using System.Linq;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Common.Game.World.Data;

namespace MapTools.Services
{
    public class MapService : IMapService
    {
        #region Private Fields

        private readonly List<MapModel> _mapModels = new List<MapModel>();

        #endregion Private Fields

        #region Public Methods

        public MapModel Add(MapData mapData, SubAreaModel subAreaModel)
        {
            var mapModel = new MapModel(mapData, subAreaModel);
            _mapModels.Add(mapModel);
            subAreaModel.MapModels.Add(mapModel);
            return mapModel;
        }

        public MapModel Get(int id) => _mapModels.FirstOrDefault(e => e.Id == id);

        public IList<MapModel> GetAll() => _mapModels.AsReadOnly();

        public bool Remove(int id)
        {
            var mapModel = Get(id);
            return mapModel.SubAreaModel.MapModels.Remove(mapModel) && _mapModels.Remove(mapModel);
        }

        #endregion Public Methods
    }
}