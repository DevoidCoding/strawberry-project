﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapTools.Model;
using MapTools.Services.Interfaces;

namespace MapTools.Services
{
    public class GfxService : IGfxService
    {
        #region Private Fields

        private Dictionary<GfxModel, List<GraphicalElementModel>> _gfxs = new Dictionary<GfxModel, List<GraphicalElementModel>>();

        #endregion Private Fields

        #region Public Events

        public event EventHandler Updated;

        #endregion Public Events

        #region Public Methods

        public void Add(GfxModel gfx)
        {
            if (!_gfxs.ContainsKey(gfx))
                _gfxs[gfx] = new List<GraphicalElementModel>();
        }

        public void Add(int gfxId, IEnumerable<GraphicalElementModel> elements)
        {
            var model = Get(gfxId);
            if (model == null)
            {
                model = new GfxModel((uint) gfxId);
                Add(model);
            }
            _gfxs[model] = elements.ToList();
            Updated?.Invoke(this, EventArgs.Empty);
        }

        public GfxModel Get(int gfxId) => _gfxs.FirstOrDefault(e => e.Key.Id == gfxId).Key;

        public Dictionary<GfxModel, List<GraphicalElementModel>> GetAll() => new Dictionary<GfxModel, List<GraphicalElementModel>>(_gfxs);

        public IEnumerable<GraphicalElementModel> GetElementsByGfx(GfxModel model) => _gfxs[model];

        public IEnumerable<GraphicalElementModel> GetElementsByGfxId(int gfxId) => _gfxs.FirstOrDefault(e => e.Key.Id == gfxId).Value;

        public GfxModel GetGfxByElementId(uint elementId) => (from gfx in _gfxs where gfx.Value.Any(e => e.ElementId == elementId) select gfx.Key).FirstOrDefault();

        public void Update(Dictionary<GfxModel, List<GraphicalElementModel>> elementsByGfx)
        {
            _gfxs = new Dictionary<GfxModel, List<GraphicalElementModel>>(elementsByGfx);
            Updated?.Invoke(this, EventArgs.Empty);
        }

        #endregion Public Methods
    }
}