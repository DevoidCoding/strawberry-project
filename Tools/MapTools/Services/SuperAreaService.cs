﻿using System.Collections.Generic;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Protocol.Data;

namespace MapTools.Services
{
    public class SuperAreaService : ISuperAreaService
    {
        #region Private Fields

        private readonly List<SuperAreaModel> _superAreaModels = new List<SuperAreaModel>();

        #endregion Private Fields

        #region Public Methods

        public SuperAreaModel Add(SuperArea superArea)
        {
            var superAreaModel = new SuperAreaModel(superArea);
            _superAreaModels.Add(superAreaModel);
            return superAreaModel;
        }

        public SuperAreaModel Get(int id) => _superAreaModels.Find(e => e.Id == id);

        public IList<SuperAreaModel> GetSuperAreaModels() => _superAreaModels.AsReadOnly();

        public bool Remove(int id) => _superAreaModels.Remove(Get(id));

        #endregion Public Methods
    }
}