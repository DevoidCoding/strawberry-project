﻿using System.Collections.Generic;
using MapTools.Model;
using Strawberry.Protocol.Data;

namespace MapTools.Services.Interfaces
{
    public interface ISubAreaService
    {
        #region Public Methods

        SubAreaModel Add(SubArea subArea, AreaModel areaModel);

        SubAreaModel Get(int id);

        IList<SubAreaModel> GetSubAreaModels();

        bool Remove(int id);

        #endregion Public Methods
    }
}