﻿using System.Collections.Generic;
using MapTools.Model;
using Strawberry.Common.Game.World.Data;

namespace MapTools.Services.Interfaces
{
    public interface IMapService
    {
        #region Public Methods

        MapModel Add(MapData mapData, SubAreaModel subAreaModel);

        MapModel Get(int id);

        IList<MapModel> GetAll();

        bool Remove(int id);

        #endregion Public Methods
    }
}