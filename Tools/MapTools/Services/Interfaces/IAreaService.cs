﻿using System.Collections.Generic;
using MapTools.Model;
using Strawberry.Protocol.Data;

namespace MapTools.Services.Interfaces
{
    public interface IAreaService
    {
        #region Public Methods

        AreaModel Add(Area area, SuperAreaModel superAreaModel);

        AreaModel Get(int id);

        IList<AreaModel> GetAreaModels();

        bool Remove(int id);

        #endregion Public Methods
    }
}