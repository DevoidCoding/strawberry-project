using System.Collections.Generic;
using MapTools.Model;
using Strawberry.Protocol.Tools.Dlm;
using Strawberry.Protocol.Tools.Ele.Datas;

namespace MapTools.Services.Interfaces
{
    public interface IElementService
    {
        #region Public Methods

        void Add(GraphicalElementModel element);

        void Add(DlmGraphicalElement dlmGraphicalElement, MapModel map, NormalGraphicalElementData elementData);

        GraphicalElementModel Get(int elementId, int mapId);

        IEnumerable<GraphicalElementModel> GetAll();

        IEnumerable<GraphicalElementModel> GetElementByGfxId(int gfxId);

        IEnumerable<GraphicalElementModel> GetElementsByMapId(int mapId);

        bool Remove(GraphicalElementModel element);

        bool Remove(int elementId, int mapId);

        #endregion Public Methods
    }
}