﻿using System;
using System.Collections.Generic;
using MapTools.Model;

namespace MapTools.Services.Interfaces
{
    public interface IGfxService
    {
        #region Public Events

        event EventHandler Updated;

        #endregion Public Events

        #region Public Methods

        void Add(GfxModel gfx);

        void Add(int gfxId, IEnumerable<GraphicalElementModel> elements);

        GfxModel Get(int gfxId);

        Dictionary<GfxModel, List<GraphicalElementModel>> GetAll();

        IEnumerable<GraphicalElementModel> GetElementsByGfx(GfxModel model);

        IEnumerable<GraphicalElementModel> GetElementsByGfxId(int gfxId);

        GfxModel GetGfxByElementId(uint elementId);

        void Update(Dictionary<GfxModel, List<GraphicalElementModel>> elementsByGfx);

        #endregion Public Methods
    }
}