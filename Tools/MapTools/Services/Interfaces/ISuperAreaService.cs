﻿using System.Collections.Generic;
using MapTools.Model;
using Strawberry.Protocol.Data;

namespace MapTools.Services.Interfaces
{
    public interface ISuperAreaService
    {
        #region Public Methods

        SuperAreaModel Add(SuperArea superArea);

        SuperAreaModel Get(int id);

        IList<SuperAreaModel> GetSuperAreaModels();

        bool Remove(int id);

        #endregion Public Methods
    }
}