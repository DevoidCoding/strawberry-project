﻿using System.Collections.Generic;
using MapTools.Model;

namespace MapTools.Services.Interfaces
{
    public interface IPoiService
    {
        #region Public Methods

        void Add(PoiModel poi);

        void Add(PoiModel poi, GfxModel gfx);

        void Add(int poiId, GfxModel gfx);

        void Add(int poiId, int gfxId);

        bool Deserialize();

        PoiModel Get(int poiId);

        IEnumerable<GfxModel> GetGfxs(PoiModel poi);

        IEnumerable<GfxModel> GetGfxs(int poiId);

        IEnumerable<PoiModel> GetPois();

        bool Remove(PoiModel poi);

        void Serialize();

        void Update(Dictionary<PoiModel, List<GfxModel>> dictionary);

        #endregion Public Methods
    }
}