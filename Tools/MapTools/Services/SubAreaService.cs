﻿using System.Collections.Generic;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Protocol.Data;

namespace MapTools.Services
{
    public class SubAreaService : ISubAreaService
    {
        #region Private Fields

        private readonly List<SubAreaModel> _subAreaModels = new List<SubAreaModel>();

        #endregion Private Fields

        #region Public Methods

        public SubAreaModel Add(SubArea subArea, AreaModel areaModel)
        {
            var subAreaModel = new SubAreaModel(subArea, areaModel);
            _subAreaModels.Add(subAreaModel);
            areaModel.SubAreaModels.Add(subAreaModel);
            return subAreaModel;
        }

        public SubAreaModel Get(int id) => _subAreaModels.Find(e => e.Id == id);

        public IList<SubAreaModel> GetSubAreaModels() => _subAreaModels.AsReadOnly();

        public bool Remove(int id)
        {
            var subAreaModel = Get(id);
            return subAreaModel.AreaModel.SubAreaModels.Remove(subAreaModel) && _subAreaModels.Remove(subAreaModel);
        }

        #endregion Public Methods
    }
}