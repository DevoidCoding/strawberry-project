﻿using System.Windows;
using System.Windows.Controls;
using MapTools.Model;

namespace MapTools.Templates
{
    public class MapDetailsTemplateSelector : DataTemplateSelector
    {
        #region Public Properties

        public DataTemplate MapDetailsTemplate { get; set; }

        #endregion Public Properties

        #region Public Methods

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            base.SelectTemplate(item, container);
            if (item is MapModel)
                return MapDetailsTemplate;
            return null;
        }

        #endregion Public Methods
    }
}