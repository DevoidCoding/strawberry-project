﻿using System.Windows;
using GalaSoft.MvvmLight.Threading;
using MapTools.View;

namespace MapTools
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Public Constructors

        public App()
        {
            DispatcherHelper.Initialize();
        }

        #endregion Public Constructors

        #region Protected Methods

        protected override void OnStartup(StartupEventArgs e)
        {
            new MainWindow().Show();
            base.OnStartup(e);
        }

        #endregion Protected Methods
    }
}