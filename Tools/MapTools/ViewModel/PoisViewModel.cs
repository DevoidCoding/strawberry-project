﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Threading;
using HtmlAgilityPack;
using MapTools.Core;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Common.Data.D2O;
using Strawberry.Protocol.Data;

namespace MapTools.ViewModel
{
    public class PoisViewModel : ViewModelBase
    {
        #region Public Constructors

        public PoisViewModel(IPoiService poiService, IGfxService gfxService)
        {
            _poiService = poiService;
            _gfxService = gfxService;

            FilteredPois = new CollectionViewSource {Source = Pois};
            FilteredPois.Filter += FilteredPoisOnFilter;

            SimpleIoc.Default.GetInstance<MapsViewModel>().Initialized += HostOnInitialized;

            var geometricSearch = GeometricSearch.Instance;
        }

        #endregion Public Constructors

        #region Public Events

        public event EventHandler Initialized;

        #endregion Public Events

        #region Private Fields

        private readonly IGfxService _gfxService;

        private readonly IPoiService _poiService;

        private string _filterText = string.Empty;

        private ObservableCollection<PoiModel> _pois = new ObservableCollection<PoiModel>();

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the FilteredGfxs property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public CollectionViewSource FilteredPois { get; set; }

        /// <summary>
        ///     Sets and gets the FilterText property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string FilterText
        {
            get { return _filterText; }

            set
            {
                if (_filterText == value)
                    return;

                _filterText = value;
                RaisePropertyChanged(() => FilterText);
                FilteredPois.View.Refresh();
            }
        }

        /// <summary>
        ///     Sets and gets the Pois property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<PoiModel> Pois
        {
            get { return _pois; }

            set
            {
                if (_pois == value)
                    return;

                _pois = value;
                RaisePropertyChanged(() => Pois);
            }
        }

        #endregion Public Properties

        #region Private Methods

        private async Task AddPointOfInterest(PoiModel model) => await DispatcherHelper.RunAsync(() => Pois.Add(model));

        private void FilteredPoisOnFilter(object sender, FilterEventArgs filterEventArgs)
        {
            filterEventArgs.Accepted = FilteredPoisOnFilter(filterEventArgs.Item);
        }

        private bool FilteredPoisOnFilter(object item)
        {
            return item is PoiModel poi && poi.Name.Contains(FilterText);
        }

        private void GeneratePoisOfDofusGo()
        {
            foreach (var poiModel in Pois)
            {
                if (poiModel.GfxModels == null)
                    poiModel.GfxModels = new List<GfxModel>();

                var nameId = poiModel.NameId;

                string url = $"https://dofusgo.com/app/clues/{nameId}";
                var htmlWeb = new HtmlWeb();
                var document = htmlWeb.Load(url);

                var divs = document.DocumentNode.Descendants("div");
                var clues = divs.FirstOrDefault(e => e.HasAttributes && e.Attributes.Any(a => a.Name == "class") && e.Attributes["class"].Value == "clues");
                if (clues == null)
                    continue;
                
                foreach (var clue in clues.ChildNodes)
                {
                    var img = clue.FirstChild?.FirstChild;
                    if (img == null)
                        continue;

                    int.TryParse(img.Attributes["data-original"].Value.Split('/').Last().Split('.').First(), out int gfxId);
                    var gfx = _gfxService.Get(gfxId);
                    if (gfx == null)
                        continue;

                    var gfxModel = _gfxService.Get(gfxId);
                    if (!poiModel.GfxModels.Contains(gfxModel))
                        poiModel.GfxModels.Add(gfxModel);

                    if (gfxId != 16062) continue;

                    gfxModel = _gfxService.Get(16064);
                    if (!poiModel.GfxModels.Contains(gfxModel))
                        poiModel.GfxModels.Add(gfxModel);
                }

                Thread.Sleep(50);
            }
        }

        private void HostOnInitialized(object sender, EventArgs eventArgs)
        {
            Task.Factory.StartNew(Initialize);
        }

        private async void Initialize()
        {
            var pointOfInterests = _poiService.Deserialize() ? _poiService.GetPois().OrderBy(e => e.Name).ToList() : ObjectDataManager.Instance.EnumerateObjects<PointOfInterest>().Select(e => new PoiModel(e)).OrderBy(e => e.Name).ToList();
            foreach (var pointOfInterest in pointOfInterests)
                await AddPointOfInterest(pointOfInterest);
            if (pointOfInterests.All(e => e.GfxModels == null || e.GfxModels.Count == 0))
            {
                GeneratePoisOfDofusGo();
                UpdateAndSerialize();
            }
            Initialized?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateAndSerialize()
        {
            _poiService.Update(Pois.ToDictionary(e => e, e => e.GfxModels));
            _poiService.Serialize();
        }

        #endregion Private Methods
    }
}