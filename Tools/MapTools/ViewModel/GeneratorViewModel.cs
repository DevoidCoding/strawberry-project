﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Threading;
using MapTools.Converters.Json;
using MapTools.Core;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Newtonsoft.Json;

namespace MapTools.ViewModel
{
    public class GeneratorViewModel : ViewModelBase
    {
        #region Public Constructors

        public GeneratorViewModel(IMapService mapService, IPoiService poiService, IElementService elementService)
        {
            _mapService = mapService;
            _poiService = poiService;
            _elementService = elementService;
            _mainViewModel = SimpleIoc.Default.GetInstance<MainViewModel>();

            _gfxsByMapByPoi = new Dictionary<int, Dictionary<int, List<int>>>();
            Pois = new ObservableCollection<PoiModel>();
            SelectedPois = new ObservableCollection<PoiModel>();

            SimpleIoc.Default.GetInstance<PoisViewModel>().Initialized += OnPoisViewModelInitialized;
            SelectedPois.CollectionChanged += SelectedPoisOnCollectionChanged;
        }

        #endregion Public Constructors

        #region Private Fields

        private readonly IElementService _elementService;
        private readonly Dictionary<int, Dictionary<int, List<int>>> _gfxsByMapByPoi;
        private readonly MainViewModel _mainViewModel;
        private readonly IMapService _mapService;
        private readonly IPoiService _poiService;
        private RelayCommand _generateAllCommand;

        private RelayCommand _generateSelectedCommand;

        private bool _isSelectedGenerator;

        private ObservableCollection<PoiModel> _pois;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Gets the GenerateAllCommand.
        /// </summary>
        public RelayCommand GenerateAllCommand => _generateAllCommand ?? (_generateAllCommand = new RelayCommand(ExecuteGenerateAllCommand, CanExecuteGenerateAllCommand));

        /// <summary>
        ///     Gets the GenerateSelectedCommand.
        /// </summary>
        public RelayCommand GenerateSelectedCommand => _generateSelectedCommand ?? (_generateSelectedCommand = new RelayCommand(ExecuteGenerateSelectedCommand, CanExecuteGenerateSelectedCommand));

        /// <summary>
        ///     Sets and gets the Pois property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<PoiModel> Pois
        {
            get { return _pois; }

            set
            {
                if (_pois == value)
                    return;

                _pois = value;
                RaisePropertyChanged(() => Pois);
            }
        }

        /// <summary>
        ///     Sets and gets the SelectedPois property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<PoiModel> SelectedPois { get; set; }

        #endregion Public Properties

        #region Private Methods

        private bool CanExecuteGenerateAllCommand() => Pois.Any();

        private bool CanExecuteGenerateSelectedCommand() => SelectedPois.Any();

        private void ExecuteGenerateAllCommand()
        {
            _isSelectedGenerator = false;
            Generate();
        }

        private void ExecuteGenerateSelectedCommand()
        {
            _isSelectedGenerator = true;
            Generate();
        }

        private void Generate()
        {
            _gfxsByMapByPoi.Clear();
            var maps = _mapService.GetAll();

            _mainViewModel.SetBusy(true, "Searching in maps...");

            Task.Factory.StartNew(() =>
            {
                var pois = (_isSelectedGenerator ? SelectedPois : _pois).Where(e => e.GfxModels != null && e.GfxModels.Any()).ToList();
                var poiElements = pois.SelectMany(e => e.GfxModels).ToList();
                var elements = _elementService.GetAll().Where(e => poiElements.Exists(a => a.Id == e.GfxId) && GeometricSearch.Instance.MapContainsElement(e)).ToList();
                var elementsByPoi = elements.Select(ge => new {pois = pois.Where(e => e.GfxModels.Exists(a => a.Id == ge.GfxId)), element = ge});
                foreach (var x1 in elementsByPoi)
                foreach (var poiModel in x1.pois)
                {
                    var poiModelId = poiModel.Id;
                    var mapId = x1.element.MapModel.Id;
                    var gfxId = x1.element.GfxId;

                    if (!_gfxsByMapByPoi.ContainsKey(poiModelId))
                        _gfxsByMapByPoi[poiModelId] = new Dictionary<int, List<int>>();
                    if (!_gfxsByMapByPoi[poiModelId].ContainsKey(mapId))
                        _gfxsByMapByPoi[poiModelId][mapId] = new List<int>();

                    if (!_gfxsByMapByPoi[poiModelId][mapId].Contains(gfxId))
                        _gfxsByMapByPoi[poiModelId][mapId].Add(gfxId);
                }
            }).ContinueWith(task => SerializePois()).ContinueWith(task => _mainViewModel.SetBusy(false));
        }

        private void OnPoisViewModelInitialized(object sender, EventArgs eventArgs)
        {
            Pois = new ObservableCollection<PoiModel>(_poiService.GetPois());
            DispatcherHelper.CheckBeginInvokeOnUI(() => GenerateAllCommand.RaiseCanExecuteChanged());
        }

        private void SelectedPoisOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            GenerateSelectedCommand.RaiseCanExecuteChanged();
        }

        private void SerializePois()
        {
            if (!Directory.Exists("pois"))
                Directory.CreateDirectory("pois");

            _mainViewModel.SetBusyMessage("Generating json...");

            foreach (var kvp in _gfxsByMapByPoi)
            {
                var json = JsonConvert.SerializeObject(kvp, Formatting.Indented, new PointOfInterestJsonConverter());
                File.WriteAllText($@"pois\{kvp.Key}.json", json);
            }
        }

        #endregion Private Methods
    }
}