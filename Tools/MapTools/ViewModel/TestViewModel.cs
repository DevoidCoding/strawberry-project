﻿using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MapTools.Core;
using MapTools.Model;
using MapTools.Services.Interfaces;

namespace MapTools.ViewModel
{
    public class TestViewModel : ViewModelBase
    {
        #region Public Constructors

        public TestViewModel(IMapService mapService)
        {
            _mapService = mapService;
        }

        #endregion Public Constructors

        #region Private Fields

        private readonly IMapService _mapService;

        private int _gfxId;

        private ObservableCollection<(int cellId, GfxModel gfx)> _insideElements = new ObservableCollection<(int, GfxModel)>();

        private int _mapId;

        private ObservableCollection<(int cellId, GfxModel gfx)> _outsideElements = new ObservableCollection<(int, GfxModel)>();

        private RelayCommand _searchCommand;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the GfxId property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public int GfxId
        {
            get { return _gfxId; }

            set
            {
                if (_gfxId == value)
                    return;

                _gfxId = value;
                RaisePropertyChanged(() => GfxId);
                SearchCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        ///     Sets and gets the InsideElements property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<(int cellId, GfxModel gfx)> InsideElements
        {
            get { return _insideElements; }

            set
            {
                if (_insideElements == value)
                    return;

                _insideElements = value;
                RaisePropertyChanged(() => InsideElements);
            }
        }

        /// <summary>
        ///     Sets and gets the MapId property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public int MapId
        {
            get { return _mapId; }

            set
            {
                if (_mapId == value)
                    return;

                _mapId = value;
                RaisePropertyChanged(() => MapId);
                SearchCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        ///     Sets and gets the OutsideElements property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<(int cellId, GfxModel gfx)> OutsideElements
        {
            get { return _outsideElements; }

            set
            {
                if (_outsideElements == value)
                    return;

                _outsideElements = value;
                RaisePropertyChanged(() => OutsideElements);
            }
        }

        /// <summary>
        ///     Gets the SearchCommand.
        /// </summary>
        public RelayCommand SearchCommand => _searchCommand ?? (_searchCommand = new RelayCommand(ExecuteCommandName, CanExecuteCommandName));

        #endregion Public Properties

        #region Private Methods

        private bool CanExecuteCommandName()
        {
            return MapId != 0 && GfxId != 0;
        }

        private void ExecuteCommandName()
        {
            InsideElements.Clear();
            OutsideElements.Clear();

            var elements = _mapService.Get(_mapId).Elements.Where(e => e.GfxId == _gfxId);
            foreach (var graphicalElementModel in elements)
            {
                var isInMap = GeometricSearch.Instance.MapContainsElement(graphicalElementModel, SearchAlgorithmTypeEnum.Size);
                var list = isInMap ? InsideElements : OutsideElements;
                list.Add((graphicalElementModel.CellId, graphicalElementModel.GfxModel));
            }
        }

        #endregion Private Methods
    }
}