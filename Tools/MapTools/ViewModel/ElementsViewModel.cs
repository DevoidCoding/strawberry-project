﻿using System;
using System.Linq;
using System.Windows.Data;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using MapTools.Model;
using MapTools.Services.Interfaces;

namespace MapTools.ViewModel
{
    public class ElementsViewModel : ViewModelBase
    {
        #region Public Constructors

        public ElementsViewModel(IElementService elementService, IGfxService gfxService)
        {
            _elementService = elementService;
            _gfxService = gfxService;

            _gfxService.Updated += GfxServiceOnUpdated;

            FilteredGfxs = new CollectionViewSource();
            FilteredGfxs.Filter += FilteredGfxsOnFilter;

            _filterText = string.Empty;
        }

        #endregion Public Constructors

        #region Private Fields

        private readonly IElementService _elementService;

        private readonly IGfxService _gfxService;

        private string _filterText;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the FilteredGfxs property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public CollectionViewSource FilteredGfxs { get; set; }

        /// <summary>
        ///     Sets and gets the FilterText property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string FilterText
        {
            get { return _filterText; }

            set
            {
                if (_filterText == value)
                    return;

                _filterText = value ?? string.Empty;
                RaisePropertyChanged(() => FilterText);
                FilteredGfxs.View.Refresh();
            }
        }

        #endregion Public Properties

        #region Private Methods

        private void FilteredGfxsOnFilter(object sender, FilterEventArgs filterEventArgs)
        {
            filterEventArgs.Accepted = FilteredGfxsOnFilter(filterEventArgs.Item);
        }

        private bool FilteredGfxsOnFilter(object item)
        {
            var gfx = item as GfxModel;
            return gfx != null && gfx.Id.ToString().Contains(FilterText);
        }

        private void GfxServiceOnUpdated(object sender, EventArgs eventArgs)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() => { FilteredGfxs.Source = _gfxService.GetAll().Select(e => e.Key).OrderBy(e => e.Id).ToList(); });
        }

        #endregion Private Methods
    }
}