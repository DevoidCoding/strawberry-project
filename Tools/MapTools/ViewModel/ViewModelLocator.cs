/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:MapTools"
                           x:Key="Locator" />
  </Application.Resources>

  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using MapTools.Services;
using MapTools.Services.Design;
using MapTools.Services.Interfaces;

namespace MapTools.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IMapService, DesignMapService>();
            }
            else
            {
                SimpleIoc.Default.Register<ISuperAreaService, SuperAreaService>();
                SimpleIoc.Default.Register<IAreaService, AreaService>();
                SimpleIoc.Default.Register<ISubAreaService, SubAreaService>();
                SimpleIoc.Default.Register<IMapService, MapService>();
                SimpleIoc.Default.Register<IElementService, ElementService>();
                SimpleIoc.Default.Register<IGfxService, GfxService>();
                SimpleIoc.Default.Register<IPoiService, PoiService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<MapsViewModel>();
            SimpleIoc.Default.Register<ElementsViewModel>();
            SimpleIoc.Default.Register<PoisViewModel>();
            SimpleIoc.Default.Register<TestViewModel>();
            SimpleIoc.Default.Register<GeneratorViewModel>();
        }

        #endregion Public Constructors

        #region Public Properties

        public ElementsViewModel Elements => SimpleIoc.Default.GetInstance<ElementsViewModel>();
        public GeneratorViewModel Generator => SimpleIoc.Default.GetInstance<GeneratorViewModel>();
        public MainViewModel Main => SimpleIoc.Default.GetInstance<MainViewModel>();
        public MapsViewModel Maps => SimpleIoc.Default.GetInstance<MapsViewModel>();
        public PoisViewModel Pois => SimpleIoc.Default.GetInstance<PoisViewModel>();
        public TestViewModel Test => SimpleIoc.Default.GetInstance<TestViewModel>();

        #endregion Public Properties

        #region Public Methods

        public static void Cleanup()
        {
        }

        #endregion Public Methods
    }
}