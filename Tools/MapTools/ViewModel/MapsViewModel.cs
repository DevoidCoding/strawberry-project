﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using MapTools.Model;
using MapTools.Services.Interfaces;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.Maps;
using Strawberry.Common.Game.World;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Tools.Dlm;
using Strawberry.Protocol.Tools.Ele;
using Strawberry.Protocol.Tools.Ele.Datas;
using Telerik.Windows.Controls;
using ViewModelBase = GalaSoft.MvvmLight.ViewModelBase;

namespace MapTools.ViewModel
{
    public class MapsViewModel : ViewModelBase
    {
        #region Public Constructors

        public MapsViewModel(ISuperAreaService superAreaService, IAreaService areaService, ISubAreaService subAreaService, IMapService mapService, IElementService elementService, IGfxService gfxService)
        {
            _superAreaService = superAreaService;
            _areaService = areaService;
            _subAreaService = subAreaService;
            _mapService = mapService;
            _elementService = elementService;
            _gfxService = gfxService;

            _filterText = string.Empty;

            Host.Initialized += InstanceOnInitialized;
        }

        #endregion Public Constructors

        #region Public Events

        public event EventHandler Initialized;

        #endregion Public Events

        #region Private Fields

        private readonly IAreaService _areaService;

        private readonly IElementService _elementService;

        private readonly IGfxService _gfxService;

        private readonly IMapService _mapService;

        private readonly int _maxProgress = 0;

        private readonly ISubAreaService _subAreaService;

        private readonly ISuperAreaService _superAreaService;

        private double _busyCounterPercent;

        private string _busyMessage;

        private string _filterText;

        private RelayCommand<RadTreeView> _filterTextEnterCommand;

        private bool _isBusy;

        private bool _isBusyCounterDisplayed;

        private ReadOnlyObservableCollection<SuperAreaModel> _superAreaModels;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the BusyCounterPercent property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public double BusyCounterPercent
        {
            get { return _busyCounterPercent; }

            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_busyCounterPercent == value)
                    return;

                _busyCounterPercent = value;
                RaisePropertyChanged(() => BusyCounterPercent);
            }
        }

        /// <summary>
        ///     Sets and gets the BusyMessage property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string BusyMessage
        {
            get { return _busyMessage; }

            set
            {
                if (_busyMessage == value)
                    return;

                _busyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }

        /// <summary>
        ///     Sets and gets the FilterText property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string FilterText
        {
            get { return _filterText; }

            set
            {
                if (_filterText == value)
                    return;

                _filterText = value;
                RaisePropertyChanged(() => FilterText);
            }
        }

        /// <summary>
        ///     Gets the FilterTextEnterCommand.
        /// </summary>
        public RelayCommand<RadTreeView> FilterTextEnterCommand => _filterTextEnterCommand ?? (_filterTextEnterCommand = new RelayCommand<RadTreeView>(ExecuteFilterEnterCommand));

        /// <summary>
        ///     Sets and gets the IsBusy property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }

            set
            {
                if (_isBusy == value)
                    return;

                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusyCounterDisplayed property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusyCounterDisplayed
        {
            get { return _isBusyCounterDisplayed; }

            set
            {
                if (_isBusyCounterDisplayed == value)
                    return;

                _isBusyCounterDisplayed = value;
                RaisePropertyChanged(() => IsBusyCounterDisplayed);
            }
        }

        /// <summary>
        ///     Sets and gets the SuperAreaModels property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ReadOnlyObservableCollection<SuperAreaModel> SuperAreaModels
        {
            get { return _superAreaModels; }

            set
            {
                if (_superAreaModels == value)
                    return;

                _superAreaModels = value;
                RaisePropertyChanged(() => SuperAreaModels);
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void SetBusy(bool toogle)
        {
            IsBusy = toogle;
        }

        public void SetBusy(bool toogle, string message)
        {
            SetBusyMessage(message);
            SetBusy(toogle);
        }

        public void SetBusyMessage(string message)
        {
            BusyMessage = message;
        }

        public void SetBusyProgress(double current, double max)
        {
            IsBusyCounterDisplayed = Math.Abs(current - max) > 0.1;
            BusyCounterPercent = (int) current / max * 100;
        }

        #endregion Public Methods

        #region Private Methods

        private void ExecuteFilterEnterCommand(RadTreeView radTreeView)
        {
            if (!int.TryParse(_filterText, out int id))
                return;

            Task.Factory.StartNew(() =>
            {
                var mapModel = _mapService.GetAll().FirstOrDefault(e => e.Id == id);
                if (mapModel == null)
                    return;

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    var item = radTreeView.GetItemByPath($@"{mapModel.SubAreaModel.AreaModel.SuperAreaModel}\{mapModel.SubAreaModel.AreaModel}\{mapModel.SubAreaModel}\{mapModel}");
                    if (item == null)
                        return;

                    item.BringIntoView();
                    item.IsSelected = true;
                });
            });
        }

        private void GenerateMaps()
        {
            SetBusy(true, $"Generating map models...");
            var mapProgress = new Progress<int>();
            mapProgress.ProgressChanged += MapProgressOnProgressChanged;

            var superAres = ObjectDataManager.Instance.EnumerateObjects<SuperArea>().ToList();
            var areas = ObjectDataManager.Instance.EnumerateObjects<Area>().ToList();
            var subAreas = ObjectDataManager.Instance.EnumerateObjects<SubArea>().ToList();
            var maps = MapsManager.Instance.EnumerateMaps().ToList();

            foreach (var superArea in superAres)
            {
                var superAreaModel = _superAreaService.Add(superArea);
                foreach (var area in areas.Where(e => e.SuperAreaId == superArea.Id))
                {
                    var areaModel = _areaService.Add(area, superAreaModel);
                    foreach (var subArea in subAreas.Where(e => e.AreaId == area.Id))
                    {
                        var subAreaModel = _subAreaService.Add(subArea, areaModel);
                        foreach (var mapData in maps.Where(e => e.SubAreaId == subArea.Id))
                        {
                            var msg = new StringBuilder($"{superAreaModel.Name}\n" +
                                                        $"-> {areaModel.Name}\n" +
                                                        $"--> {subAreaModel.Name}\n" +
                                                        $"---> {mapData}");
                            SetBusyMessage(msg.ToString());
                            _mapService.Add(mapData, subAreaModel);
                        }
                    }
                }
            }
            var superAreaModels = _superAreaService.GetSuperAreaModels();
            SuperAreaModels = new ReadOnlyObservableCollection<SuperAreaModel>(new ObservableCollection<SuperAreaModel>(superAreaModels));

            SetBusyMessage($"Generating Elements...");
            var i = 0;
            var mapModels = _mapService.GetAll().ToList();
            var count = mapModels.Count;
            foreach (var mapModel in mapModels)
            {
                SetBusyProgress(i, count);
                foreach (var dlmGraphicalElement in MapsManager.Instance.GetDlmMap(mapModel.Id, "649ae451ca33ec53bbcbcc33becf15f4").Layers.SelectMany(e => e.Cells).SelectMany(e => e.Elements).OfType<DlmGraphicalElement>().Where(e => e.ElementType == DlmBasicElement.ElementTypesEnum.Graphical).ToList())
                {
                    var elementData = EleReader.Instance.EleInstance.GetElementData((int) dlmGraphicalElement.ElementId);
                    if (!(elementData is NormalGraphicalElementData graphicalElement))
                        continue;
                    _elementService.Add(dlmGraphicalElement, mapModel, graphicalElement);
                }
                i++;
            }

            SetBusyMessage($"Binding GFX...");
            var elementsByGfx = _elementService.GetAll().GroupBy(e => e.GfxId).ToDictionary(e => new GfxModel((uint) e.Key), e => e.ToList());
            var whea = elementsByGfx.Where(e => e.Value.Any(a => a.GfxId == 42)).Select(e => e.Value);
            _gfxService.Update(elementsByGfx);
            SetBusy(false);
        }

        private void InstanceOnInitialized(object sender, EventArgs eventArgs)
        {
            Task.Factory.StartNew(GenerateMaps).ContinueWith(task => Initialized?.Invoke(this, EventArgs.Empty));
        }

        private void MapProgressOnProgressChanged(object sender, int i)
        {
            SetBusyProgress(i, _maxProgress);
        }

        #endregion Private Methods
    }
}