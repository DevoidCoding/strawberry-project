using System;
using GalaSoft.MvvmLight;

namespace MapTools.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Private Fields

        private double _busyCounterPercent;

        private string _busyMessage;

        private bool _isBusy;

        private bool _isBusyCounterDisplayed;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the BusyCounterPercent property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public double BusyCounterPercent
        {
            get { return _busyCounterPercent; }

            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_busyCounterPercent == value)
                    return;

                _busyCounterPercent = value;
                RaisePropertyChanged(() => BusyCounterPercent);
            }
        }

        /// <summary>
        ///     Sets and gets the BusyMessage property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string BusyMessage
        {
            get { return _busyMessage; }

            set
            {
                if (_busyMessage == value)
                    return;

                _busyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusy property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }

            set
            {
                if (_isBusy == value)
                    return;

                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusyCounterDisplayed property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusyCounterDisplayed
        {
            get { return _isBusyCounterDisplayed; }

            set
            {
                if (_isBusyCounterDisplayed == value)
                    return;

                _isBusyCounterDisplayed = value;
                RaisePropertyChanged(() => IsBusyCounterDisplayed);
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void SetBusy(bool toogle)
        {
            IsBusy = toogle;
        }

        public void SetBusy(bool toogle, string message)
        {
            SetBusyMessage(message);
            SetBusy(toogle);
        }

        public void SetBusyMessage(string message)
        {
            BusyMessage = message;
        }

        public void SetBusyProgress(double current, double max)
        {
            IsBusyCounterDisplayed = Math.Abs(current - max) > 0.1;
            BusyCounterPercent = (int)current / max * 100;
        }

        #endregion Public Methods
    }
}