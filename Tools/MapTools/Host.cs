﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using GalaSoft.MvvmLight.Ioc;
using MapTools.ViewModel;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.GFXs;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Data.Icons;
using Strawberry.Common.Data.Maps;
using Strawberry.Core.I18N;
using Strawberry.Core.Machine;
using Strawberry.Core.UI;
using Strawberry.Protocol.Tools.Ele;

namespace MapTools
{
    public class Host
    {
        #region Public Constructors

        static Host()
        {
            MainViewModel = SimpleIoc.Default.GetInstance<MainViewModel>();
        }

        #endregion Public Constructors

        #region Public Properties

        public static bool IsInitialized { get; private set; }

        #endregion Public Properties

        #region Public Fields

        public static string DofusBasePath = Path.Combine(OSInfo.GetLocalAppData(), @"\Ankama\Dofus 2\");

        public static string DofusDataPath = @"app\data\common";

        public static string DofusElements = @"app\content\maps\elements.ele";

        public static string DofusExe = @"app\Dofus.exe";

        public static string DofusI18NPath = @"app\data\i18n";

        public static string DofusItemIconPath = @"app\content\gfx\items\bitmap0.d2p";

        public static string DofusMapsD2P = @"app\content\maps\maps0.d2p";

        public static bool FindDofusPathAuto = true;

        #endregion Public Fields

        #region Private Fields

        // ReSharper disable once UnusedMember.Local
        private const string ConfigPath = "./config.xml";

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly MainViewModel MainViewModel;

        private static string _dofusPath;

        #endregion Private Fields

        #region Public Events

        public static event EventHandler Initialized;

        public static event UnhandledExceptionEventHandler UnhandledException;

        #endregion Public Events

        #region Public Methods

        public static string GetDofusPath()
        {
            if (_dofusPath != null)
                return _dofusPath;

            if (!FindDofusPathAuto)
                return _dofusPath = DofusBasePath;

            var localappdata = OSInfo.GetLocalAppData();

            if (Directory.Exists(Path.Combine(localappdata, @"Ankama\Dofus")))
            {
                _dofusPath = Path.Combine(localappdata, @"Ankama\Dofus");
            }
            else if (Directory.Exists(Path.Combine(localappdata, @"Ankama\Dofus 2")))
            {
                _dofusPath = Path.Combine(localappdata, @"Ankama\Dofus 2");
            }
            else
            {
                var programFiles = OSInfo.GetProgramFiles();

                if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus")))
                {
                    _dofusPath = Path.Combine(programFiles, @"Ankama\Dofus");
                }
                else if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus 2")))
                {
                    _dofusPath = Path.Combine(programFiles, @"Ankama\Dofus 2");
                }
                else
                {
                    programFiles = OSInfo.GetProgramFilesX86();

                    if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus")))
                        _dofusPath = Path.Combine(programFiles, @"Ankama\Dofus");
                    else if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus 2")))
                        _dofusPath = Path.Combine(programFiles, @"Ankama\Dofus 2");
                }
            }

            return _dofusPath;
        }

        public static void Initialize()
        {
            if (IsInitialized)
                return;

            MainViewModel.SetBusy(true);
            try
            {
                if (!Debugger.IsAttached) // the debugger handle the unhandled exceptions
                    AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

                AppDomain.CurrentDomain.ProcessExit += OnProcessExit;

                //UIManager.Instance.SetBusyMessage("Load config ...");
                //Config = new Config(ConfigPath);
                //Config.Load();

                //Logger.Info("{0} loaded", Path.GetFileName(Config.FilePath));

                MainViewModel.SetBusyMessage("Loading D2O files ...");
                ObjectDataManager.Instance.AddReaders(Path.Combine(GetDofusPath(), DofusDataPath));

                I18NDataManager.Instance.DefaultLanguage = Languages.French;
                I18NDataManager.Instance.AddReaders(Path.Combine(GetDofusPath(), DofusI18NPath));

                IconsManager.Instance.Initialize(Path.Combine(GetDofusPath(), DofusItemIconPath));

                MainViewModel.SetBusyMessage($"Loading {MapsManager.MapsDataFile}...");
                Logger.Info("Loading {0}...", MapsManager.MapsDataFile);
                var progression = MapsManager.Instance.Initialize(Path.Combine(GetDofusPath(), DofusMapsD2P));
                if (progression != null)
                    ExecuteProgress(progression);

                MainViewModel.SetBusyMessage("Loading maps positions ...");
                Logger.Info("Loading maps positions ...");
                progression = MapsPositionManager.Instance.Initialize();
                if (progression != null)
                    ExecuteProgress(progression);

                //MainViewModel.SetBusyMessage("Loading submaps ...");
                //Logger.Info("Loading submaps ...");
                //progression = SubMapsManager.Instance.Initialize();
                //if (progression != null)
                //    ExecuteProgress(progression);

                MainViewModel.SetBusyMessage("Loading Elements ...");
                Logger.Info("Loading Elements ...");
                EleReader.Instance.Initialize(@"C:\Users\Devoid\AppData\Local\Ankama\Dofus\app\content\maps\elements.ele");

                MainViewModel.SetBusyMessage("Loading GFXs ...");
                Logger.Info("Loading GFXs ...");
                GFXsManager.Instance.Initialize(@"C:\Users\Devoid\AppData\Local\Ankama\Dofus\app\content\gfx\world\gfx0.d2p");

                if (!Directory.Exists("GFX"))
                {
                    MainViewModel.SetBusyMessage("Extracting GFXs ...");
                    Logger.Info("Extracting GFXs ...");
                    progression = GFXsManager.Instance.ExtractGfxs("GFX");
                    if (progression != null)
                        ExecuteProgress(progression);
                }
            }
            finally
            {
                MainViewModel.SetBusy(false);
            }

            IsInitialized = true;
            OnInitialized();
        }

        #endregion Public Methods

        #region Private Methods

        private static void ExecuteProgress(ProgressionCounter progression, int refreshRate = 100)
        {
            var lastValue = progression.Value;
            while (!progression.IsEnded)
            {
                Thread.Sleep(refreshRate);
                if (Math.Abs(lastValue - progression.Value) > 0.1)
                {
                    Logger.Debug("{0} : {1:0.0}/{2} Mem:{3}MB", progression.Text, progression.Value, progression.Total, GC.GetTotalMemory(false) / (1024 * 1024));
                    MainViewModel.SetBusyProgress(progression.Value, progression.Total);
                }

                lastValue = progression.Value;
                if (!string.IsNullOrWhiteSpace(progression.Text))
                    MainViewModel.SetBusyMessage(progression.Text);
            }
            // Report last time to attend 100%
            MainViewModel.SetBusyProgress(100, 100);

            GC.Collect();
        }

        private static void LogUnhandledException(Exception ex)
        {
            Logger.Fatal(ex, "Unhandled exception : ");

            if (ex.InnerException != null)
                LogUnhandledException(ex.InnerException);
        }

        private static void OnInitialized()
        {
            Initialized?.Invoke(null, EventArgs.Empty);
        }

        private static void OnProcessExit(object sender, EventArgs e)
        {
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogUnhandledException((Exception) e.ExceptionObject);

            try
            {
            }
            finally
            {
                if (UnhandledException != null)
                {
                    UnhandledException(sender, e);
                }
                else
                {
                    Console.WriteLine(@"Press enter to exit");
                    Console.Read();
                }

                Environment.Exit(-1);
            }
        }

        #endregion Private Methods
    }
}