﻿using System.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for GeneratorUserControl.xaml
    /// </summary>
    public partial class GeneratorUserControl : UserControl
    {
        #region Public Constructors

        public GeneratorUserControl()
        {
            InitializeComponent();
        }

        #endregion Public Constructors
    }
}