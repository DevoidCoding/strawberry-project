﻿using System.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for PoisUserControl.xaml
    /// </summary>
    public partial class PoisUserControl : UserControl
    {
        #region Public Constructors

        public PoisUserControl()
        {
            InitializeComponent();
        }

        #endregion Public Constructors
    }
}