﻿using System.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for TestUserControl.xaml
    /// </summary>
    public partial class TestUserControl : UserControl
    {
        #region Public Constructors

        public TestUserControl()
        {
            InitializeComponent();
        }

        #endregion Public Constructors
    }
}