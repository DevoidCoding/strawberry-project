﻿using System.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for MapsUserControl.xaml
    /// </summary>
    public partial class MapsUserControl : UserControl
    {
        #region Public Constructors

        public MapsUserControl()
        {
            InitializeComponent();
        }

        #endregion Public Constructors
    }
}