﻿using System;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Public Constructors

        public MainWindow()
        {
            VisualStudio2013Palette.LoadPreset(VisualStudio2013Palette.ColorVariation.Dark);
            Initialized += OnInitialized;
            InitializeComponent();
        }

        #endregion Public Constructors

        #region Private Methods

        private static void OnInitialized(object sender, EventArgs eventArgs)
        {
            Task.Factory.StartNew(Host.Initialize);
        }

        #endregion Private Methods
    }
}