﻿using System.Windows.Controls;

namespace MapTools.View
{
    /// <summary>
    ///     Interaction logic for ElementsUserControl.xaml
    /// </summary>
    public partial class ElementsUserControl : UserControl
    {
        #region Public Constructors

        public ElementsUserControl()
        {
            InitializeComponent();
        }

        #endregion Public Constructors
    }
}