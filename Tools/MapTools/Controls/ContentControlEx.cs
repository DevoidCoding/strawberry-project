﻿using System.Windows;
using System.Windows.Controls;

namespace MapTools.Controls
{
    public class ContentControlEx : ContentControl
    {
        #region Public Fields

        public static readonly DependencyProperty TemplateSelectorProperty =
            DependencyProperty.Register("TemplateSelector", typeof(DataTemplateSelector), typeof(ContentControlEx), new PropertyMetadata(null));

        #endregion Public Fields

        #region Public Properties

        public DataTemplateSelector TemplateSelector
        {
            get { return (DataTemplateSelector) GetValue(TemplateSelectorProperty); }
            set { SetValue(TemplateSelectorProperty, value); }
        }

        #endregion Public Properties

        #region Protected Methods

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);
            if (TemplateSelector != null)
                ContentTemplate = TemplateSelector.SelectTemplate(newContent, this);
        }

        #endregion Protected Methods
    }
}