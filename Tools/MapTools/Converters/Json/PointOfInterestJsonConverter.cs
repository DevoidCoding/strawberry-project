﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MapTools.Converters.Json
{
    public class PointOfInterestJsonConverter : JsonConverter
    {
        #region Public Methods

        public override bool CanConvert(Type objectType) => objectType == typeof(KeyValuePair<int, Dictionary<int, List<int>>>);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var obj = (KeyValuePair<int, Dictionary<int, List<int>>>) value;
            writer.WriteStartObject();
            writer.WritePropertyName("id");
            writer.WriteValue(obj.Key);
            writer.WritePropertyName("maps");
            writer.WriteStartArray();
            foreach (var gfxsByMap in obj.Value)
            {
                writer.WriteStartObject();
                writer.WritePropertyName("mapId");
                writer.WriteValue(gfxsByMap.Key);
                writer.WritePropertyName("gfxs");
                writer.WriteStartArray();
                foreach (var gfx in gfxsByMap.Value)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("gfxId");
                    writer.WriteValue(gfx);
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }

        #endregion Public Methods
    }
}