﻿using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;

namespace MapTools.Model
{
    public class SubAreaModel : RadTreeViewItemModel
    {
        #region Private Fields

        private readonly SubArea _subArea;

        #endregion Private Fields

        #region Public Constructors

        public SubAreaModel(SubArea subArea, AreaModel areaModel)
        {
            _subArea = subArea;
            AreaModel = areaModel;
        }

        #endregion Public Constructors

        #region Public Methods

        public override string ToString() => Name;

        #endregion Public Methods

        #region Public Properties

        public AreaModel AreaModel { get; }

        public IEnumerable<GraphicalElementModel> Elements => MapModels.SelectMany(e => e.Elements);

        public int Id => _subArea.Id;

        public List<MapModel> MapModels { get; } = new List<MapModel>();

        public string Name => I18NDataManager.Instance.ReadText(_subArea.NameId);

        #endregion Public Properties
    }
}