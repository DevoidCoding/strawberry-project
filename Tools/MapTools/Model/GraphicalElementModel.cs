﻿using System.Drawing;
using GalaSoft.MvvmLight.Ioc;
using MapTools.Services.Interfaces;
using Strawberry.Protocol.Tools.Dlm;
using Strawberry.Protocol.Tools.Ele.Datas;

namespace MapTools.Model
{
    public class GraphicalElementModel
    {
        #region Public Constructors

        public GraphicalElementModel()
        {
        }

        public GraphicalElementModel(DlmGraphicalElement element, MapModel mapModel, NormalGraphicalElementData elementData)
        {
            MapModel = mapModel;

            ElementId = element.ElementId;
            CellId = element.Cell.Id;
            Offset = element.Offset;
            PixelOffset = element.PixelOffset;

            GfxId = elementData.Gfx;
            Origin = elementData.Origin;
            Size = elementData.Size;
        }

        #endregion Public Constructors

        #region Public Properties

        public int CellId { get; set; }
        public uint ElementId { get; set; }
        public int GfxId { get; set; }

        public GfxModel GfxModel => SimpleIoc.Default.GetInstance<IGfxService>().Get(GfxId);
        public MapModel MapModel { get; set; }
        public Point Offset { get; set; }
        public Point Origin { get; set; }
        public Point PixelOffset { get; set; }
        public Point Size { get; set; }

        #endregion Public Properties
    }
}