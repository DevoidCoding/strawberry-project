﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;

namespace MapTools.Model
{
    public class GfxModel
    {
        #region Public Constructors

        public GfxModel(uint gfxId)
        {
            Id = gfxId;
        }

        #endregion Public Constructors

        #region Public Methods

        public override string ToString() => Id.ToString();

        #endregion Public Methods

        #region Public Properties

        public uint Id { get; set; }

        [JsonIgnore]
        public BitmapImage Image
        {
            get
            {
                if (ImagePath == null)
                    return null;
                var image = CreateBitmap(ImagePath.ToString().Replace("%20", " "));
                double width = image.PixelWidth;
                double height = image.PixelHeight;

                if (width <= 100 && height <= 100)
                    return image;

                var max = Math.Max(width, height);
                var newWidth = width / max * 100;
                var newHeight = height / max * 100;

                // Scale to with of 100px
                var ratio = 1 / Math.Min(width / newWidth, height / newHeight);
                var scaledImage = ScaleImage(image, ratio);
                return scaledImage;
            }
        }

        [JsonIgnore]
        public Uri ImagePath
        {
            get
            {
                if (File.Exists($@"GFX\{Id}.png"))
                    return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GFX", $"{Id}.png"));
                if (File.Exists($@"GFX\{Id}.jpg"))
                    return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GFX", $"{Id}.jpg"));
                return null;
            }
        }

        #endregion Public Properties

        #region Private Methods

        private static BitmapImage CreateBitmap(string path)
        {
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(path);
            bi.EndInit();
            return bi;
        }

        private BitmapImage BitmapSourceToBitmap(BitmapSource source)
        {
            var encoder = new PngBitmapEncoder();
            var memoryStream = new MemoryStream();
            var image = new BitmapImage();

            encoder.Frames.Add(BitmapFrame.Create(source));
            encoder.Save(memoryStream);

            image.BeginInit();
            image.StreamSource = new MemoryStream(memoryStream.ToArray());
            image.EndInit();
            memoryStream.Close();
            return image;
        }

        private BitmapImage ScaleImage(BitmapImage original, double scale)
        {
            var scaledBitmapSource = new TransformedBitmap();
            scaledBitmapSource.BeginInit();
            scaledBitmapSource.Source = original;
            scaledBitmapSource.Transform = new ScaleTransform(scale, scale);
            scaledBitmapSource.EndInit();
            return BitmapSourceToBitmap(scaledBitmapSource);
        }

        #endregion Private Methods
    }
}