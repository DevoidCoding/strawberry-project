﻿using GalaSoft.MvvmLight;

namespace MapTools.Model
{
    public class RadTreeViewItemModel : ViewModelBase
    {
        #region Private Fields

        private bool _isExpandable;
        private bool _isSelected;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     Sets and gets the IsExpandable property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsExpandable
        {
            get { return _isExpandable; }

            set
            {
                if (_isExpandable == value)
                    return;

                _isExpandable = value;
                RaisePropertyChanged(() => IsExpandable);
            }
        }

        /// <summary>
        ///     Sets and gets the IsSelected property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }

            set
            {
                if (_isSelected == value)
                    return;

                _isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        #endregion Public Properties
    }
}