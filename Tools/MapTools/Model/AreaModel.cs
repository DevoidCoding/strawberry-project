﻿using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;

namespace MapTools.Model
{
    public class AreaModel : RadTreeViewItemModel
    {
        #region Private Fields

        private readonly Area _area;

        #endregion Private Fields

        #region Public Constructors

        public AreaModel(Area area, SuperAreaModel superAreaModel)
        {
            _area = area;
            SuperAreaModel = superAreaModel;
        }

        #endregion Public Constructors

        #region Public Methods

        public override string ToString() => Name;

        #endregion Public Methods

        #region Public Properties

        public IEnumerable<GraphicalElementModel> Elements => SubAreaModels.SelectMany(e => e.Elements);

        public int Id => _area.Id;

        public string Name => I18NDataManager.Instance.ReadText(_area.NameId);

        public List<SubAreaModel> SubAreaModels { get; } = new List<SubAreaModel>();

        public SuperAreaModel SuperAreaModel { get; }

        #endregion Public Properties
    }
}