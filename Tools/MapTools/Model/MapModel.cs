﻿using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using MapTools.Services.Interfaces;
using Strawberry.Common.Game.World.Data;

namespace MapTools.Model
{
    public class MapModel
    {
        #region Private Fields

        private IList<GraphicalElementModel> _elements;

        #endregion Private Fields

        #region Public Constructors

        public MapModel(MapData map, SubAreaModel subAreaModel)
        {
            Map = map;
            SubAreaModel = subAreaModel;
        }

        #endregion Public Constructors

        #region Public Methods

        public override string ToString() => Map.ToString();

        #endregion Public Methods

        #region Public Properties

        public IList<GraphicalElementModel> Elements => _elements ?? (_elements = SimpleIoc.Default.GetInstance<IElementService>().GetElementsByMapId(Id).ToList());

        public int Id => Map.Id;

        //_elements ?? (_elements = new Map(Id).Layers.SelectMany(e => e.Cells).SelectMany(e => e.Elements).OfType<DlmGraphicalElement>().Select(e =>
        //{
        //    var elementData = EleReader.Instance.EleInstance.GetElementData((int)e.GfxId);
        //    return elementData is NormalGraphicalElementData ? new GraphicalElementModel(e) : null;
        //}).Where(e => e != null).ToList());
        public MapData Map { get; }

        public SubAreaModel SubAreaModel { get; }

        #endregion Public Properties
    }
}