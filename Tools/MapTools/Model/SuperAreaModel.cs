﻿using System.Collections.Generic;
using System.Linq;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;

namespace MapTools.Model
{
    public class SuperAreaModel : RadTreeViewItemModel
    {
        #region Private Fields

        private readonly SuperArea _superArea;

        #endregion Private Fields

        #region Public Constructors

        public SuperAreaModel(SuperArea superArea)
        {
            _superArea = superArea;
        }

        #endregion Public Constructors

        #region Public Methods

        public override string ToString() => Name;

        #endregion Public Methods

        #region Public Properties

        public List<AreaModel> AreaModels { get; } = new List<AreaModel>();

        public IEnumerable<GraphicalElementModel> Elements => AreaModels.SelectMany(e => e.Elements);

        public int Id => _superArea.Id;

        public string Name => I18NDataManager.Instance.ReadText(_superArea.NameId);

        public int WolrdId => (int)_superArea.WorldmapId;

        #endregion Public Properties
    }
}