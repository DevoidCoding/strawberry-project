﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Strawberry.Common.Data.I18N;
using Strawberry.Protocol.Data;

namespace MapTools.Model
{
    [DataContract]
    public class PoiModel
    {
        #region Public Constructors

        public PoiModel()
        {
        }

        public PoiModel(PointOfInterest poi)
        {
            Id = (int) poi.Id;
            NameId = (int) poi.NameId;
            GfxModels = new List<GfxModel>();
        }

        #endregion Public Constructors

        #region Public Properties

        public List<GfxModel> GfxModels { get; set; }

        [DataMember]
        public int Id { get; set; }

        [JsonIgnore]
        public string Name => I18NDataManager.Instance.ReadText(NameId);

        [DataMember]
        public int NameId { get; set; }

        #endregion Public Properties
    }
}