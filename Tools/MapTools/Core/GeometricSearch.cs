﻿using System;
using System.Windows;
using MapTools.Model;
using Strawberry.Common.Game.World;
using Strawberry.Core.Reflection;

namespace MapTools.Core
{
    public class GeometricSearch : Singleton<GeometricSearch>
    {
        #region Public Properties

        public Rect MapRectangle { get; set; } = new Rect(0, 0, 1247, 881.5);

        #endregion Public Properties

        #region Private Methods

        private static void InitializeStaticGrid()
        {
            var cellCount = 0;

            for (var y = 0; y < Map.Height; y++)
            {
                for (var x = 0; x < Map.Width; x++)
                    OrthogonalGridReference[cellCount++] = new Point(x, y);

                for (var x = 0; x < Map.Width; x++)
                    OrthogonalGridReference[cellCount++] = new Point(x + 0.5, y + 0.5);
            }

            _initialized = true;
        }

        #endregion Private Methods

        #region Private Fields

        private static readonly Point[] OrthogonalGridReference = new Point[Map.MapSize];
        private static bool _initialized;

        #endregion Private Fields

        #region Public Methods

        public Point GetCenterPointFromCellId(short cellId) => GetCenterPointFromCellPoint(GetPointFromCell(cellId));

        public Point GetCenterPointFromCellPoint(Point cell)
        {
            return new Point(cell.X * 86 + 43, cell.Y * 43 + 21.5);
        }

        public Point GetPointFromCell(short id)
        {
            if (!_initialized)
                InitializeStaticGrid();

            if (id < 0 || id > Map.MapSize)
                throw new IndexOutOfRangeException("Cell identifier out of bounds (" + id + ").");

            var point = OrthogonalGridReference[id];

            return point;
        }

        public bool MapContainsElement(GraphicalElementModel graphicalElementModel, SearchAlgorithmTypeEnum searchAlgorithmTypeEnum = SearchAlgorithmTypeEnum.Origin)
        {
            return searchAlgorithmTypeEnum == SearchAlgorithmTypeEnum.Origin ? SearchByOrigin(graphicalElementModel) : SearchBySize(graphicalElementModel);
        }

        public bool SearchByOrigin(GraphicalElementModel graphicalElementModel) => MapRectangle.Contains(graphicalElementModel.Origin.X, graphicalElementModel.Origin.Y);

        public bool SearchBySize(GraphicalElementModel graphicalElementModel)
        {
            var cellCenter = GetCenterPointFromCellId((short) graphicalElementModel.CellId);
            var elementRect = new Rect(cellCenter.X - graphicalElementModel.Origin.X, cellCenter.Y - graphicalElementModel.Origin.Y, graphicalElementModel.Size.X, graphicalElementModel.Size.Y);

            elementRect.Offset(graphicalElementModel.PixelOffset.X, graphicalElementModel.PixelOffset.Y);
            return MapRectangle.IntersectsWith(elementRect);
        }

        #endregion Public Methods
    }
}