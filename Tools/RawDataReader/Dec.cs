﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RawDataReader
{
    public class Dec
    {
        public static string GetString(string s1, string s2)
        {
            var b1 = Convert.FromBase64String(s1);
            var b2 = Convert.FromBase64String(s2);

            for (int i = 0; i < b1.Length; i++)
                b1[i] ^= b2[i % b2.Length];

            return Encoding.UTF8.GetString(b1);
        }
    }
}
