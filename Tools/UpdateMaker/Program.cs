﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace UpdateMaker
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("You need to specify the folder. i.e : UpdateMaker.exe C:\\");
                return;
            }

            var path = args[0].Replace("/", @"\");
            if (!Directory.Exists(path))
            {
                Console.WriteLine($"Directory {path} doesn't exist");
                return;
            }

            var filesChecksum = DirectoryExtensions.GetFiles(path, "*.exe|*.dll", SearchOption.AllDirectories)
                .Select(e => new FileWithChecksum(e))
                .Where(e => !e.FileInfo.Directory.Name.Equals("Plugins", StringComparison.InvariantCultureIgnoreCase) ||
                             e.FileInfo.Directory.Name.Equals("Plugins", StringComparison.InvariantCultureIgnoreCase) && e.FileInfo.Name.ToLower().Contains("plugin")).ToList();

            XmlGenerator.Write(path, filesChecksum);
        }
    }

    internal static class XmlGenerator
    {
        public static bool Write(string path, IEnumerable<FileWithChecksum> files)
        {
            try
            {
                var document = new XmlDocument();
                var rootNode = document.CreateElement("files");
                document.AppendChild(rootNode);
                foreach (var file in files)
                {
                    var element = document.CreateElement("file");
                    var filenameAttribute = document.CreateAttribute("filename");
                    filenameAttribute.Value = file.FileInfo.FullName.Replace(path, "").Substring(1);
                    element.Attributes.Append(filenameAttribute);
                    element.InnerText = file.MD5;

                    rootNode.AppendChild(element);
                }
                document.Save(Path.Combine(path, "update.xml"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }
    }

    internal static class Checksum
    {
        public static string GetHash<T>(Stream stream) where T : HashAlgorithm
        {
            var sb = new StringBuilder();

            var create = typeof(T).GetMethod("Create", new Type[] { });
            using (var crypt = (T) create.Invoke(null, null))
            {
                var hashBytes = crypt.ComputeHash(stream);
                foreach (var bt in hashBytes)
                {
                    sb.Append(bt.ToString("x2"));
                }
            }
            return sb.ToString();
        }
    }

    internal class FileWithChecksum
    {
        public FileInfo FileInfo { get; }

        // ReSharper disable once InconsistentNaming
        public string MD5 => Checksum.GetHash<MD5>(FileInfo.OpenRead());

        public FileWithChecksum(FileInfo fileInfo) => FileInfo = fileInfo;

        public FileWithChecksum(string fileName) => FileInfo = new FileInfo(fileName);
    }

    internal static class DirectoryExtensions
    {
        public static IEnumerable<string> GetFiles(string path, string searchPattern, SearchOption searchOption)
        {
            var patterns = searchPattern.Split('|');
            foreach (var pattern in patterns)
            {
                foreach (var file in Directory.GetFiles(path, pattern, searchOption))
                {
                    yield return file;
                }
            }
        }
    }
}