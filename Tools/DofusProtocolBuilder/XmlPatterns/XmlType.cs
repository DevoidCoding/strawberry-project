// <copyright file="XmlType.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System.Xml.Serialization;

namespace DofusProtocolBuilder.XmlPatterns
{
    public class XmlType
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute]
        public string Heritage { get; set; }

        [XmlAttribute]
        public string Namespace { get; set; }

        public XmlField[] Fields { get; set; }
    }
}