// <copyright file="XmlField.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System.Xml.Serialization;

namespace DofusProtocolBuilder.XmlPatterns
{
    public class XmlField
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Type { get; set; }

        [XmlAttribute]
        public string Limit { get; set; }

        [XmlAttribute]
        public string Condition { get; set; }

        [XmlAttribute]
        public string LimitType { get; set; }
    }
}