// <copyright file="XmlMessage.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System.Xml.Serialization;

namespace DofusProtocolBuilder.XmlPatterns
{
    public class XmlMessage
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute]
        public string Heritage { get; set; }

        [XmlAttribute]
        public string Namespace { get; set; }

        public XmlField[] Fields { get; set; }

        [XmlAttribute]
        public bool IsHashed { get; set; }
    }
}