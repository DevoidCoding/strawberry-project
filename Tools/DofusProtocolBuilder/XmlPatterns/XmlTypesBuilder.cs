// <copyright file="XmlTypesBuilder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using DofusProtocolBuilder.Parsing;
using DofusProtocolBuilder.Parsing.Elements;

namespace DofusProtocolBuilder.XmlPatterns
{
    public class XmlTypesBuilder : XmlPatternBuilder
    {
        public XmlTypesBuilder(Parser parser)
            : base(parser)
        {
        }

        public override void WriteToXml(XmlWriter writer)
        {
            var xmlType = new XmlType
            {
                Name = Parser.Class.Name,
                Id = Parser.Fields.Find(entry => entry.Name == "protocolId").Value,
                Heritage = Parser.Class.Heritage
            };
            var xmlFields = new List<XmlField>();

            var deserializeAsMethod = Parser.Methods.Find(entry => entry.Name.Contains("deserializeAs"));
            string type = null;
            var limit = 0;

            // HACK : FuncTree statements
            for (var i = 0; i < deserializeAsMethod.Statements.Count; i++)
            {
                if (deserializeAsMethod.Statements[i] is InvokeExpression && (((InvokeExpression)deserializeAsMethod.Statements[i]).Name.EndsWith("Func") || ((InvokeExpression)deserializeAsMethod.Statements[i]).Name == "deserializeByteBoxes"))
                {
                    var statement = (InvokeExpression)deserializeAsMethod.Statements[i];
                    var funcMethod = Parser.Methods.Find(entry => entry.Name == statement.Name);
                    if (funcMethod == null)
                        throw new Exception("funcMethod doesn't exists");
                    foreach (AssignationStatement assignStatement in funcMethod.Statements.Where(e => e is AssignationStatement))
                    {
                        if (assignStatement.Name.StartsWith("loc") && assignStatement.Name != "locked")
                        {
                            assignStatement.Name = assignStatement.Name.Insert(3, funcMethod.Name);
                        }
                    }
                    deserializeAsMethod.Statements.InsertRange(i + 1, funcMethod.Statements);
                    deserializeAsMethod.Statements.Remove(statement);
                    i--;
                }
            }

            for (var i = 0; i < deserializeAsMethod.Statements.Count; i++)
            {
                if (deserializeAsMethod.Statements[i] is AssignationStatement &&
                    ((AssignationStatement) deserializeAsMethod.Statements[i]).Value.Contains("Read"))
                {
                    var statement = (AssignationStatement) deserializeAsMethod.Statements[i];
                    type = Regex.Match(statement.Value, @"Read([\w\d_]+)\(").Groups[1].Value.ToLower();
                    var name = statement.Name;

                    if (type == "bytes" || type == "bytearray")
                        type = "byte[]";

                    var match = Regex.Match(name, @"^([\w\d]+)\[.+\]$");
                    if (match.Success)
                    {
                        var limitLinq = from entry in Parser.Constructors[0].Statements
                            where
                                entry is AssignationStatement &&
                                ((AssignationStatement) entry).Name == match.Groups[1].Value
                            let entryMatch =
                                Regex.Match(((AssignationStatement) entry).Value,
                                    @"new List<[\d\w\._]+>\(([\d]+)(,(true|false))?\)")
                            where entryMatch.Success
                            select entryMatch.Groups[1].Value;

                        if (limitLinq.Count() == 1)
                            limit = int.Parse(limitLinq.Single());

                        type += "[]";
                        name = name.Split('[')[0];
                    }

                    var field = Parser.Fields.Find(entry => entry.Name == name);

                    if (field != null)
                    {
                        string condition = null;

                        if (i + 1 < deserializeAsMethod.Statements.Count &&
                            deserializeAsMethod.Statements[i + 1] is ControlStatement &&
                            ((ControlStatement) deserializeAsMethod.Statements[i + 1]).ControlType == ControlType.If)
                            condition = ((ControlStatement) deserializeAsMethod.Statements[i + 1]).Condition;

                        xmlFields.Add(new XmlField
                        {
                            Name = field.Name,
                            Type = type,
                            Limit = limit > 0 ? limit.ToString() : null,
                            Condition = condition,
                            LimitType = type.EndsWith("[]") ? GetLimitType(deserializeAsMethod, i) : null
                        });

                        limit = 0;
                        type = null;
                    }
                }

                if (deserializeAsMethod.Statements[i] is InvokeExpression &&
                    ((InvokeExpression) deserializeAsMethod.Statements[i]).Name == "deserialize")
                {
                    var statement = (InvokeExpression) deserializeAsMethod.Statements[i];
                    var field = Parser.Fields.Find(entry => entry.Name == statement.Target);

                    if (field != null && xmlFields.Count(entry => entry.Name == field.Name) <= 0)
                    {
                        type = "Types." + field.Type;

                        string condition = null;

                        if (i + 1 < deserializeAsMethod.Statements.Count &&
                            deserializeAsMethod.Statements[i + 1] is ControlStatement &&
                            ((ControlStatement) deserializeAsMethod.Statements[i + 1]).ControlType == ControlType.If)
                            condition = ((ControlStatement) deserializeAsMethod.Statements[i + 1]).Condition;

                        xmlFields.Add(new XmlField
                        {
                            Name = field.Name,
                            Type = type,
                            Limit = limit > 0 ? limit.ToString() : null,
                            Condition = condition,
                            LimitType = type.EndsWith("[]") ? GetLimitType(deserializeAsMethod, i) : null
                        });

                        limit = 0;
                        type = null;
                    }
                    else if (i > 0 &&
                             deserializeAsMethod.Statements[i - 1] is AssignationStatement)
                    {
                        var substatement = (AssignationStatement) deserializeAsMethod.Statements[i - 1];
                        var name = substatement.Name;
                        var match = Regex.Match(substatement.Value, @"new ([\d\w]+)");

                        if (match.Success)
                        {
                            var temp = match.Groups[1].Value;
                            if (temp == "com")
                                temp = substatement.Value.Split('.').Last().Replace("()", "");

                            type = "Types." + temp;

                            var arrayMatch = Regex.Match(name, @"^([\w\d]+)\[.+\]$");
                            if (arrayMatch.Success)
                            {
                                var limitLinq = from entry in Parser.Constructors[0].Statements
                                    where
                                        entry is AssignationStatement &&
                                        ((AssignationStatement) entry).Name == arrayMatch.Groups[1].Value
                                    let entryMatch =
                                        Regex.Match(((AssignationStatement) entry).Value,
                                            @"new List<[\d\w\._]+>\(([\d]+)(,(true|false))?\)")
                                    where entryMatch.Success
                                    select entryMatch.Groups[1].Value;

                                if (limitLinq.Count() == 1)
                                    limit = int.Parse(limitLinq.Single());

                                type += "[]";
                                name = name.Split('[')[0];
                            }
                        }

                        field = Parser.Fields.Find(entry => entry.Name == name);

                        if (field != null && xmlFields.Count(entry => entry.Name == field.Name) <= 0)
                        {
                            string condition = null;

                            if (i + 1 < deserializeAsMethod.Statements.Count &&
                                deserializeAsMethod.Statements[i + 1] is ControlStatement &&
                                ((ControlStatement) deserializeAsMethod.Statements[i + 1]).ControlType == ControlType.If)
                                condition = ((ControlStatement) deserializeAsMethod.Statements[i + 1]).Condition;

                            xmlFields.Add(new XmlField
                            {
                                Name = field.Name,
                                Type = type,
                                Limit = limit > 0 ? limit.ToString() : null,
                                Condition = condition,
                                LimitType = type.EndsWith("[]") ? GetLimitType(deserializeAsMethod, i) : null
                            });

                            limit = 0;
                            type = null;
                        }
                    }
                }

                if (deserializeAsMethod.Statements[i] is AssignationStatement &&
                    ((AssignationStatement) deserializeAsMethod.Statements[i]).Value.Contains("getFlag"))
                {
                    var statement = (AssignationStatement) deserializeAsMethod.Statements[i];
                    var field = Parser.Fields.Find(entry => entry.Name == statement.Name);

                    var match = Regex.Match(statement.Value, @"getFlag\([\w\d]+, ?(\d+)\)");

                    if (match.Success)
                    {
                        type = "flag(" + match.Groups[1].Value + ")";

                        if (field != null)
                        {
                            xmlFields.Add(new XmlField
                            {
                                Name = field.Name,
                                Type = type
                            });

                            type = null;
                        }
                    }
                }

                if (deserializeAsMethod.Statements[i] is AssignationStatement &&
                    ((AssignationStatement) deserializeAsMethod.Statements[i]).Value.Contains("getInstance"))
                {
                    var statement = (AssignationStatement) deserializeAsMethod.Statements[i];
                    var field = Parser.Fields.Find(entry => entry.Name == statement.Name);

                    // HACK : JPEXS 6.1.1 com.ankamagames.*
                    if (Regex.IsMatch(statement.Value, @"[a-zA-Z\.]+\.([A-Za-z]+)"))
                        statement.Value = Regex.Replace(statement.Value, @"[a-zA-Z\.]+\.([A-Za-z]+)", "$1");

                    type = "instance of Types." +
                           Regex.Match(statement.Value, @"getInstance\(([\w\d_\.]+),").Groups[1].Value;

                    if (field != null)
                    {
                        xmlFields.Add(new XmlField
                        {
                            Name = field.Name,
                            Type = type
                        });

                        type = null;
                    }
                }

                if (deserializeAsMethod.Statements[i] is InvokeExpression &&
                    ((InvokeExpression) deserializeAsMethod.Statements[i]).Name == "Add" &&
                    type != null)
                {
                    var statement = (InvokeExpression) deserializeAsMethod.Statements[i];

                    var field = Parser.Fields.Find(entry => entry.Name == statement.Target);

                    string condition = null;

                    if (i + 1 < deserializeAsMethod.Statements.Count &&
                        deserializeAsMethod.Statements[i + 1] is ControlStatement &&
                        ((ControlStatement) deserializeAsMethod.Statements[i + 1]).ControlType == ControlType.If)
                        condition = ((ControlStatement) deserializeAsMethod.Statements[i + 1]).Condition;

                    var limitType = GetLimitType(deserializeAsMethod, i);

                    xmlFields.Add(new XmlField
                    {
                        Name = field.Name,
                        Type = type + "[]",
                        Limit = limit > 0 ? limit.ToString() : null,
                        Condition = condition,
                        LimitType = limitType
                    });

                    limit = 0;
                    type = null;
                }
            }

            xmlType.Fields = xmlFields.ToArray();

            var serializer = new XmlSerializer(typeof (XmlType));
            serializer.Serialize(writer, xmlType);
        }

        private string GetLimitType(MethodInfo deserializeAsMethod, int index)
        {
            string limitType = null;
            var tmp =
                (ControlStatement)
                    deserializeAsMethod.Statements.Take(index)
                                       .LastOrDefault(
                                           e =>
                                               e is ControlStatement &&
                                               ((ControlStatement) e).ControlType == ControlType.While);
            if (tmp != null)
            {
                var match = Regex.Matches(tmp.Condition, @"(loc\d|locdeserializeByteBoxes\d|loc.*Func\d)");

                if (match.Count > 0)
                {
                    var lastMatch = match[match.Count - 1];
                    var limitStatement =
                        (AssignationStatement)
                            deserializeAsMethod.Statements.FirstOrDefault(
                                e => e is AssignationStatement && ((AssignationStatement) e).Name == lastMatch.Value);
                    if (limitStatement != null)
                    {
                        lastMatch = Regex.Match(limitStatement.Value, @"arg1\.Read([\w]+)\(\)");
                        if (lastMatch.Success)
                            limitType = lastMatch.Groups[1].Value;
                    }
                }
            }

            return limitType;
        }
    }
}