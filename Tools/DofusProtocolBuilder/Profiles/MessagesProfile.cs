// <copyright file="MessagesProfile.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System;
using System.CodeDom.Compiler;
using System.IO;
using DofusProtocolBuilder.Parsing;
using DofusProtocolBuilder.Templates;
using Microsoft.VisualStudio.TextTemplating;

namespace DofusProtocolBuilder.Profiles
{
    public class MessagesProfile : ParsingProfile
    {
        public override void ExecuteProfile(Parser parser)
        {
            var file = Path.Combine(Program.Configuration.Output, OutPutPath, GetRelativePath(parser.Filename),
                Path.GetFileNameWithoutExtension(parser.Filename));
            var xmlMessage =
                Program.Configuration.XmlMessagesProfile.SearchXmlPattern(
                    Path.GetFileNameWithoutExtension(parser.Filename));

            if (xmlMessage == null)
                Program.Shutdown($"File {file} not found");

            var engine = new Engine();
            var host = new TemplateHost(TemplatePath);
            host.Session["Message"] = xmlMessage;
            host.Session["Profile"] = this;
            var output = engine.ProcessTemplate(File.ReadAllText(TemplatePath), host);

            foreach (CompilerError error in host.Errors)
            {
                Console.WriteLine(error.ErrorText);
            }

            if (host.Errors.Count > 0)
                Program.Shutdown();

            File.WriteAllText(file + host.FileExtension, output);

            //Console.WriteLine("Wrote {0}", file);
        }
    }
}