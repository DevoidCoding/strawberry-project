// <copyright file="PropertyInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing
{
    public class PropertyInfo
    {
        public AccessModifiers AccessModifier { get; set; }

        public MethodInfo.MethodModifiers MethodModifier { get; set; }

        public string Name { get; set; }

        public string PropertyType { get; set; }

        public MethodInfo MethodGet { get; set; }

        public MethodInfo MethodSet { get; set; }
    }
}