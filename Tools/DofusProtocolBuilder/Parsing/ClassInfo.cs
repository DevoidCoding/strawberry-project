﻿// <copyright file="ClassInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System.Collections.Generic;

namespace DofusProtocolBuilder.Parsing
{
    public class ClassInfo
    {
        #region ClassModifiers enum

        public enum ClassModifiers
        {
            None,
            Abstract
        }

        #endregion

        public ClassInfo()
        {
            Implementations = new List<string>();
            CustomAttribute = new List<string>();
        }

        public AccessModifiers AccessModifier { get; set; }

        public ClassModifiers ClassModifier { get; set; }

        public string Namespace { get; set; }

        public string Name { get; set; }

        public string Heritage { get; set; }

        public List<string> Implementations { get; set; }

        public List<string> CustomAttribute { get; set; }
    }
}