// <copyright file="EnumInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

using System.Collections.Generic;

namespace DofusProtocolBuilder.Parsing
{
    public class EnumElement
    {
        public string Key;
        public string Value;

        public EnumElement(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }

    public class EnumInfo
    {
        public EnumInfo()
        {
            Elements = new List<EnumElement>();
        }

        public AccessModifiers AccessModifier { get; set; }

        public string Namespace { get; set; }

        public string Name { get; set; }

        public List<EnumElement> Elements { get; set; }

        public string CustomAttribute { get; set; }
    }
}