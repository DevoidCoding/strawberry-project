// <copyright file="ForStatement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing.Elements
{
    public class ForStatement : IStatement
    {
        public string Iterated { get; set; }
        public string Condition { get; set; }

        public string Iterator { get; set; }
    }
}