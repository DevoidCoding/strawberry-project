// <copyright file="ControlType.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing.Elements
{
    public enum ControlType
    {
        If,
        Else,
        Elseif,
        While,
        Break,
        Return
    }
}