// <copyright file="ForEachStatement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing.Elements
{
    public class ForEachStatement : IStatement
    {
        public string Iterated { get; set; }

        public string Iterator { get; set; }
    }
}