﻿// <copyright file="AccessModifiers.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing
{
    public enum AccessModifiers
    {
        Public,
        Protected,
        Internal,
        Private
    }
}