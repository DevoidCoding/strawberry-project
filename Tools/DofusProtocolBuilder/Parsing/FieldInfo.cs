﻿// <copyright file="FieldInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>10/31/2015 07:39</date>

namespace DofusProtocolBuilder.Parsing
{
    public class FieldInfo
    {
        public AccessModifiers Modifiers { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }

        public bool IsStatic { get; set; }

        public bool IsConst { get; set; }

        public bool IsVar { get; set; }
    }
}