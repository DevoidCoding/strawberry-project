﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Coffee.IO;

namespace RawDataUtils
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            Console.ReadLine();
            new Program();
        }

        public Program()
        {
        }

        public byte[] D2PFileDlm(string d2PFilePath)
        {
            var d2PFileStream = new FileStream(d2PFilePath, FileMode.Open, FileAccess.Read);
            var reader = new BigEndianReader(d2PFileStream);
            byte[] ret = {};
            byte num = Convert.ToByte((reader.ReadByte() + reader.ReadByte()));
            if ((num == 3))
            {
                d2PFileStream.Position = (d2PFileStream.Length - 24);
                int num2 = Convert.ToInt32(reader.ReadUInt32());
                reader.ReadUInt32();
                int num3 = Convert.ToInt32(reader.ReadUInt32());
                int num4 = Convert.ToInt32(reader.ReadUInt32());
                int num1 = Convert.ToInt32(reader.ReadUInt32());
                int num9 = Convert.ToInt32(reader.ReadUInt32());
                d2PFileStream.Position = num3;
                int num5 = num4;
                int i = 1;

                    string key = reader.ReadString();
                    int num7 = (reader.ReadInt32() + num2);
                int num8 = reader.ReadInt32();

                d2PFileStream.Position = num7;
                ret = reader.ReadBytes(num8);
            }

            return ret;
        }

        //internal byte[] ReadFile(string fileName)
        //{
        //    lock (this.CheckLock)
        //    {
        //        int[] numArray = this.FilenameDataDictionnary[fileName];
        //        this.D2pFileStream.Position = numArray[0];
        //        return this.Reader.ReadBytes(numArray[1]);
        //    }
        //}

        private static Signature GetSignedData(LittleEndianReader reader, bool reset = false)
        {
            int loc9 = 0, loc10 = 0, loc11 = 0;
            var signature = new Signature();
            var loc13 = "";

            var loc3 = reader.ReadUTFBytes(3);
            var loc4 = reader.ReadByte();
            var loc5 = reader.ReadUInt();
            var loc6 = reader.ReadByte() >> 3;

            reader.Seek((int)Math.Ceiling((decimal)loc6 * 4 / 8), SeekOrigin.Current);
            var loc7 = reader.ReadByte() /255 + reader.ReadSByte();
            var loc8 = reader.ReadUShort();
            while (reader.BytesAvailable != 0)
            {
                loc9 = reader.ReadUShort();
                loc10 = loc9 >> 6;
                loc11 = (loc9 & ((1 << 6) - 1));
                if (loc11 == 63)
                    loc11 = reader.ReadInt();
                if (loc10 == 77)
                {
                    loc13 = reader.ReadUTFBytes((ushort)loc11);
                    if (loc13.Substring(0, 4) == "SMD%")
                    {
                        return signature;
                    }
                }
                else
                {
                    reader.Seek(loc11, SeekOrigin.Current);
                }
                if (loc10 == 0 || loc10 == 82)
                    return null;
            }

            return null;
        }

        private static byte[] HashWithAesKey(byte[] data, sbyte[] key)
        {
            var writer = new BigEndianWriter();
            byte[] hash = MD5.Create().ComputeHash(data);
            writer.WriteBytes(hash);
            writer.Seek(0, SeekOrigin.Begin);
            using (Aes encryptor = Aes.Create())
            {
                encryptor.Mode = CipherMode.CBC;
                encryptor.Padding = PaddingMode.PKCS7;
                encryptor.Key = key.Select(e => (byte)e).ToArray();
                encryptor.GenerateIV();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(hash, 0, hash.Length);
                        cs.Close();
                    }
                    writer.Clear();
                    writer.WriteBytes(encryptor.IV.Select(e => (byte)e).ToArray());
                    writer.WriteBytes(ms.ToArray());
                    writer.WriteBytes(data);
                    ms.Close();
                }
            }
            return writer.Data;
        }

        //private function hash(param1:ByteArray, encrypted:ByteArray) : void
		//{
		//	var _loc2_:ByteArray = new ByteArray();
		//	var md5:MD5 = new MD5();
		//	_loc2_["writeBytes"](md5.hash(param1));
		//	_loc2_["position"] = 0;
		//	var aes:AESKey =new AESKey(hashKey);
		//	var pad:PKCS5 = new PKCS5();
		//	var cbc:CBCMode = new CBCMode(aes,pad);
		//	var mode:SimpleIVMode = new SimpleIVMode(cbc);
		//	var iv:ByteArray = new ByteArray();
			
		//	// read iv
		//	encrypted.position = 0;
		//	encrypted.readBytes(iv, 0, aes.getBlockSize());
			
		//	encrypted.position = 0;
		//	cbc.IV = iv;
			
		//	pad["setBlockSize"](mode["getBlockSize"]());
		//	mode["encrypt"](_loc2_);
		//	param1["position"] = param1["length"];
		//	param1["writeBytes"](_loc2_);
		//}
    }
}
