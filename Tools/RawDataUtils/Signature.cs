﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coffee.IO;

namespace RawDataUtils
{
    public class Signature
    {
        public BigEndianWriter Sign { get; set; }
        public BigEndianWriter SignedData { get; set; }
    }
}
