﻿// <copyright file="Program.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>11/09/2015 11:22</date>

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace RawDataPatcher
{
    internal class Program
    {
        public const string AbcExport = "abcexport.exe";
        public const string AbcExtension = ".abc";
        public const string AbcReplace = "abcreplace.exe";
        public const string RabcAsm = "rabcasm.exe";
        public const string RabcDasm = "rabcdasm.exe";
        public const string RabcDasmDirectory = @"RABCDasm";
        public const string RawDataMessageAbc = "RawDataMessage-1.abc";
        public const string RawDataMessageAbcDirectory = "RawDataMessage-1";
        public const string RawDataMessageMainAsam = "RawDataMessage-1.main.asasm";
        public const string RawDataMessageSwf = "RawDataMessage.swf";
        private readonly string _abcExportExec = Path.Combine(RabcDasmDirectory, AbcExport);
        private readonly string _rabcDasmExec = Path.Combine(RabcDasmDirectory, RabcDasm);

        public Program(IReadOnlyList<string> args)
        {
            var patcher = new Patcher($"raw-{args[0]}.swf", $"{args[0]}.swf");
            patcher.Patch();
        }

        public Process Process { get; set; }

        private static void Main(string[] args)
            => new Program(args);
    }
}