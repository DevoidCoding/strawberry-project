﻿// <copyright file="Parser.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>11/09/2015 12:18</date>

using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace RawDataPatcher
{
    public class Parser
    {
        private readonly string _codeTogglePattern = @"^ *(code|end ?; ?code) *$";

        private readonly string _traitSlotPattern =
            @"trait slot QName\(PackageInternalNs\("".*""\), ?""(?<name>.*)""\) slotid (?<id>[\d]+) (type .*)? ?end";

        private readonly string file;

        private string _fileText;

        public Parser(string file)
        {
            this.file = file;
            CodeLines = new List<string>();
            TraitSlots = new Dictionary<string, int>();
        }

        public List<string> CodeLines { get; set; }
        public string[] Lines { get; private set; }

        public Dictionary<string, int> TraitSlots { get; set; }

        public bool Parse()
        {
            if (!File.Exists(file))
                return false;

            Lines = File.ReadAllLines(file);
            _fileText = string.Join("\r\n", Lines);

            ParseCode();
            ParseTraitSlots();

            return true;
        }

        private void ParseCode()
        {
            var i = 1;
            var inCode = false;
            foreach (var line in Lines)
            {
                if (Regex.IsMatch(line, _codeTogglePattern))
                {
                    if (inCode) break;
                    inCode = true;
                }

                if (inCode && !Regex.IsMatch(line, _codeTogglePattern))
                    CodeLines.Add(line);

                i++;
            }
        }

        private void ParseTraitSlots()
        {
            var match = Regex.Match(_fileText, _traitSlotPattern);

            while (match.Success)
            {
                TraitSlots.Add(match.Groups["name"].Value, int.Parse(match.Groups["id"].Value));
                match = match.NextMatch();
            }
        }
    }
}