﻿// <copyright file="Patcher.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>11/09/2015 13:56</date>

namespace RawDataPatcher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class Patcher
    {
        #region Constants

        public const string AbcExport = "abcexport.exe";

        public const string AbcExtension = ".abc";

        public const string AbcReplace = "abcreplace.exe";

        public const string ArchivesDirectory = "Archives";

        public const string EmulatorDirectory = @"..\Emulator";

        public const string RabcAsm = "rabcasm.exe";

        public const string RabcDasm = "rabcdasm.exe";

        public const string RabcDasmDirectory = @"RABCDasm";

        #endregion

        #region Fields

        public string RawDataMessageAbc;

        public string RawDataMessageAbcDirectory;

        public string RawDataMessageMainAbc;

        public string RawDataMessageMainAsam;

        private readonly string _abcExportExec = Path.Combine(RabcDasmDirectory, AbcExport);

        private readonly string _abcReplaceExec = Path.Combine(RabcDasmDirectory, AbcReplace);

        private readonly string _asasmFile;

        private readonly string _asasmHumanFile;

        private readonly string _codePattern = @"^ *code *$";

        private readonly string _endCodePattern = @"^ *end ?; ?code *$";

        private readonly string _getSlotDofusInstancePattern = @"getslot +{0}";

        private readonly string _input;

        private readonly string _output;

        private readonly string _pushStringDofusInstancePattern = @"pushstring +""(root|stage|loaderInfo|bytes)""";

        private readonly string _rabcAsmExec = Path.Combine(RabcDasmDirectory, RabcAsm);

        private readonly string _rabcDasmExec = Path.Combine(RabcDasmDirectory, RabcDasm);

        private readonly string _rawDataMessageAbc;

        private readonly string _rawDataMessageMainAsam;

        private readonly string _youShallNotPassPattern = @"You shall not pass";

        #endregion

        #region Constructors and Destructors

        public Patcher(string input, string output)
        {
            _input = input;

            _output = output;

            RawDataMessageAbc = $"{Path.GetFileNameWithoutExtension(_input)}-1.abc";
            RawDataMessageMainAsam = $"{Path.GetFileNameWithoutExtension(_input)}-1.main.asasm";
            RawDataMessageMainAbc = $"{Path.GetFileNameWithoutExtension(_input)}-1.main.abc";
            RawDataMessageAbcDirectory = $"{Path.GetFileNameWithoutExtension(_input)}-1";

            _rawDataMessageMainAsam = Path.Combine(RawDataMessageAbcDirectory, RawDataMessageMainAsam);

            _rawDataMessageAbc = Path.Combine(RawDataMessageAbcDirectory, RawDataMessageMainAbc);
            _asasmHumanFile = Path.Combine(RawDataMessageAbcDirectory, @"HumanCheck.class.asasm");

            YsnpParser = new Parser(_asasmHumanFile);
        }

        #endregion

        #region Public Properties

        public Parser Parser { get; set; }

        public Process Process { get; set; }

        public Parser YsnpParser { get; set; }

        #endregion

        #region Public Methods and Operators

        public void Patch()
        {
            InitializeRabc();

            // Export & disassemble Abc
            Console.WriteLine(ExportAbc() ? "Export success" : "Export failed !");
            Console.WriteLine(DisassembleRabc() ? "Disassemble success" : "Disassemble failed !");

            var asasm = FindDofusInstanceAsasm();
            Parser = new Parser(asasm);

            // Parse Abc
            Console.WriteLine(Parser.Parse() ? "Parse sucess" : "Parse failed !");
            Console.WriteLine(YsnpParser.Parse() ? "Parse sucess" : "Parse failed !");

            // Patch abc
            Console.WriteLine("Patch...");
            PatchYouShallNotPass();

            PatchDofusBytes();
            PatchDofusHashKey();

            PatchAbcFile(Parser, asasm);
            PatchAbcFile(YsnpParser, _asasmHumanFile);

            // Assemble & replace Abc
            AssembleAbc();
            ReplaceAbc();

            Clean();
        }

        #endregion

        #region Methods

        private void AssembleAbc()
        {
            Process.StartInfo.FileName = _rabcAsmExec;
            Process.StartInfo.Arguments = _rawDataMessageMainAsam;
            Process.Start();
            Process.WaitForExit();
        }

        private void Clean()
        {
            foreach (var abcFile in Directory.GetFiles(Environment.CurrentDirectory).Where(IsAbcFile))
                File.Delete(abcFile);

            if (Directory.Exists(RawDataMessageAbcDirectory))
                Directory.Delete(RawDataMessageAbcDirectory, true);

            if (!Directory.Exists(ArchivesDirectory))
                Directory.CreateDirectory(ArchivesDirectory);

            if (File.Exists(_input)) MoveFileOverwrite(_input, Path.Combine(ArchivesDirectory, _input));

            MoveFileOverwrite(_output, Path.Combine(EmulatorDirectory, _output));
        }

        private bool DisassembleRabc()
        {
            if (!File.Exists(RawDataMessageAbc))
                return false;

            Process.StartInfo.FileName = _rabcDasmExec;
            Process.StartInfo.Arguments = RawDataMessageAbc;

            Process.Start();
            Process.WaitForExit();

            return Directory.Exists(RawDataMessageAbcDirectory);
        }

        private bool ExportAbc()
        {
            Process.StartInfo.FileName = _abcExportExec;
            Process.StartInfo.Arguments = _input;
            Process.Start();
            Process.WaitForExit();

            return Directory.GetFiles(Environment.CurrentDirectory).Count(IsAbcFile) == 2;
        }

        private string FindDofusInstanceAsasm()
        {
            foreach (var file in Directory.GetFiles(Path.Combine($"{RawDataMessageAbcDirectory}", @"HumanCheck.instance.init")))
            {
                if (!File.ReadAllText(file).Contains("dofusInstance")) continue;

                return Path.Combine(RawDataMessageAbcDirectory, $@"HumanCheck.instance.init\{Path.GetFileName(file)}");
            }

            return string.Empty;
        }

        private void InitializeRabc()
        {
            Process = new Process { StartInfo = { UseShellExecute = false, RedirectStandardOutput = true } };

            //Clean();

            if (File.Exists(_output))
                File.Delete(_output);

            File.Copy(_input, _output, true);
        }

        private bool IsAbcFile(string file)
        {
            var extension = Path.GetExtension(file);
            return extension != null && extension.Equals(AbcExtension, StringComparison.InvariantCultureIgnoreCase);
        }

        private void MoveFileOverwrite(string input, string output)
        {
            if (File.Exists(output))
                File.Delete(output);

            File.Move(input, output);
        }

        private void PatchAbcFile(Parser parser, string file)
        {
            var linesBeforeCodeCount = parser.Lines.TakeWhile(e => !Regex.IsMatch(e, _codePattern)).Count() + 1;
            var linesBeforeCode = parser.Lines.Take(linesBeforeCodeCount);
            var linesToSkip = parser.Lines.TakeWhile(e => !Regex.IsMatch(e, _endCodePattern)).Count();
            var linesAfterCode = parser.Lines.Skip(linesToSkip);
            var lines = new List<string>();

            lines.AddRange(linesBeforeCode);
            lines.AddRange(parser.CodeLines);
            lines.AddRange(linesAfterCode);

            File.WriteAllLines(file, lines);
        }

        private bool PatchDofusBytes()
        {
            if (!Parser.TraitSlots.ContainsKey("dofusInstance"))
                return false;

            var dofusInstaceSlot = Parser.TraitSlots["dofusInstance"];
            var linesToRemove = new List<int>();
            var linesToEdit = new List<int>();

            var index = 0;

            foreach (var codeLine in Parser.CodeLines)
            {
                if (Regex.IsMatch(codeLine, string.Format(_getSlotDofusInstancePattern, dofusInstaceSlot)))
                {
                    var broken = false;

                    for (var i = 1; i <= 5; i += 2)
                    {
                        var key = index + i;

                        var line = Parser.CodeLines[key];

                        if (!Regex.IsMatch(line, _pushStringDofusInstancePattern))
                        {
                            broken = true;
                            break;
                        }
                    }

                    if (!broken)
                    {
                        linesToRemove.AddRange(Enumerable.Range(index + 1, 4));
                        linesToEdit.Add(index + 1);
                    }
                }

                index++;
            }

            foreach (var i in linesToRemove.OrderByDescending(e => e).ToList())
                Parser.CodeLines.RemoveAt(i);

            foreach (var i in linesToEdit)
                Parser.CodeLines[i] = Parser.CodeLines[i].Replace("bytes", "dofusBytes");

            return true;
        }

        private void PatchDofusHashKey()
        {
            if (!Parser.TraitSlots.ContainsKey("dofusInstance"))
                return;

            var dofusInstaceSlot = Parser.TraitSlots["dofusInstance"];
            var returnVoid = Parser.CodeLines.Last();

            if (!returnVoid.Contains("returnvoid"))
                return;

            Parser.CodeLines.RemoveAt(Parser.CodeLines.Count - 1);
            Parser.CodeLines.Add("");

            void addLines(string traitSlot)
                => Parser.CodeLines.AddRange(
                    new[]
                        {
                            $"   getscopeobject 0", $"   getslot {dofusInstaceSlot}", $"   pushstring \"{traitSlot}\"", $"   getscopeobject 0", $"   getslot {Parser.TraitSlots[traitSlot]}",
                            $"   setproperty MultinameL([PackageNamespace(\"\"), PackageInternalNs(\"\"), PrivateNamespace(null), Namespace(\"http://adobe.com/AS3/2006/builtin\"), ProtectedNamespace(\"HumanCheck\"), StaticProtectedNs(\"Emulated\"), StaticProtectedNs(\"flash.display:Sprite\"), StaticProtectedNs(\"flash.display:DisplayObjectContainer\"), StaticProtectedNs(\"flash.display:InteractiveObject\"), StaticProtectedNs(\"flash.display:DisplayObject\"), StaticProtectedNs(\"flash.events:EventDispatcher\")])"
                        });

            var traitsSlots = new[] { "key2", "publicModulo", "sigOk" };

            foreach (var traitsSlot in traitsSlots)
                addLines(traitsSlot);

            Parser.CodeLines.Add(returnVoid);
        }

        private bool PatchYouShallNotPass()
        {
            var linesToRemove = new List<int>();

            var index = 0;

            foreach (var codeLine in YsnpParser.CodeLines)
            {
                if (Regex.IsMatch(codeLine, _youShallNotPassPattern))
                    linesToRemove.AddRange(Enumerable.Range(index - 1, 4));

                index++;
            }

            foreach (var i in linesToRemove.OrderByDescending(e => e).ToList())
                YsnpParser.CodeLines.RemoveAt(i);

            return true;
        }

        private void ReplaceAbc()
        {
            Process.StartInfo.FileName = _abcReplaceExec;
            Process.StartInfo.Arguments = $"{_output} 1 {_rawDataMessageAbc}";
            Process.Start();
            Process.WaitForExit();
        }

        #endregion
    }
}