using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using NLog;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Core.I18N;
using Strawberry.Protocol;
using Strawberry.Protocol.Data;
using Point = Strawberry.Protocol.Data.Point;

namespace DofusDataExtractor.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public static string DofusDataPath = @"data\common";
        public static string DofusI18NPath = @"data\i18n";
        public static string DofusDataDirectory = @"datas";
        public static string DofusI18NDirectory = @"i18n";
        private RelayCommand _extractCommand;
        public static Logger Logger;

        private string _extractPath;

        private RelayCommand _extractPathCommand;

        private string _sourcePath;

        private RelayCommand _sourcePathCommand;

        public RelayCommand ExtractCommand => _extractCommand ??
                                              (_extractCommand =
                                                  new RelayCommand(ExecuteExtractCommand, CanExecuteExtractCommand));

        public string ExtractPath
        {
            get => _extractPath;

            set
            {
                if (_extractPath == value)
                    return;

                _extractPath = value;
                RaisePropertyChanged(() => ExtractPath);
                ExtractCommand.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand ExtractPathCommand => _extractPathCommand ??
                                                  (_extractPathCommand = new RelayCommand(ExecuteExtractPathCommand));

        public string SourcePath
        {
            get => _sourcePath;

            set
            {
                if (_sourcePath == value)
                    return;

                _sourcePath = value;
                RaisePropertyChanged(() => SourcePath);
                ExtractCommand.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand SourcePathCommand => _sourcePathCommand ??
                                                 (_sourcePathCommand = new RelayCommand(ExecuteSourcePathCommand));

        private bool CanExecuteExtractCommand()
        {
            return !string.IsNullOrWhiteSpace(SourcePath) && !string.IsNullOrWhiteSpace(ExtractPath);
        }

        private void ExecuteExtractCommand()
        {
            Initialize();
            Extract();
        }

        private void ExecuteExtractPathCommand()
        {
            OpenFolderBrowserDialog(() => ExtractPath);
        }

        private void ExecuteSourcePathCommand()
        {
            OpenFolderBrowserDialog(() => SourcePath);
        }

        private void Initialize()
        {
            SetBusy(true);
            SetBusyMessage("Loading D2O files ...");
            ObjectDataManager.Instance.AddReaders(Path.Combine(SourcePath, DofusDataPath));

            SetBusyMessage("Loading I18N files ...");
            I18NDataManager.Instance.DefaultLanguage = Languages.French;
            I18NDataManager.Instance.AddReaders(Path.Combine(SourcePath, DofusI18NPath));
            SetBusy(false);
        }

        private void Extract()
        {
            //Logger = LogManager.GetCurrentClassLogger();
            Task.Factory.StartNew(() => {
                Directory.CreateDirectory(Path.Combine(ExtractPath, BuildInfo.ToString()));
                SetBusy(true, "Extracting D2O files ...");
                ExtractD2Os();
                SetBusyMessage("Extraction I18N files ...");
                ExtractI18N();
                SetBusy(false);
            });
        }

        private void ExtractI18N()
        {
            Directory.CreateDirectory(Path.Combine(ExtractPath, BuildInfo.ToString(), DofusI18NDirectory));
        }

        private void ExtractD2Os()
        {
            Directory.CreateDirectory(Path.Combine(ExtractPath, BuildInfo.ToString(), DofusDataDirectory));
            var types = ObjectDataManager.Instance.GetAllTypes().ToList();
            for (var i = 0; i < types.Count; i++)
            {
                try
                {
                    var type = types[i];
                    SetBusyProgress(i, types.Count);
                    if (type == typeof(Point) || type == typeof(TransformData))
                        continue;
                    Directory.CreateDirectory(Path.Combine(ExtractPath, BuildInfo.ToString(), DofusDataDirectory, type.Name));
                    foreach (var obj in ObjectDataManager.Instance.EnumerateObjectsWithKey(type))
                    {
                        var id = obj.key;
                        var filePath = Path.Combine(ExtractPath, BuildInfo.ToString(), DofusDataDirectory, type.Name, $"{id}.json");
                        if (File.Exists(filePath))
                            throw new FileAlreadyExistException($"File \"{filePath}\" of type {type} already exist.");
                        var json = JsonConvert.SerializeObject(obj.obj, Formatting.Indented);
                        File.WriteAllText(filePath, json);
                    }
                }
                catch (Exception e)
                {
                    Logger.Fatal(e);
                }
            }
        }

        private void OpenFolderBrowserDialog<T>(Expression<Func<T>> propertyExpression)
        {
            OpenFolderBrowserDialog(GetPropertyName(propertyExpression));
        }

        private void OpenFolderBrowserDialog(string propertyName)
        {
            var fbd = new FolderBrowserDialog();
            fbd.SelectedPath = @"D:\Documents\Amakna\Dofus 2\DofusInvoker";
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            GetType()
                .InvokeMember(propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                    Type.DefaultBinder, this, new object[] {fbd.SelectedPath});
        }

        #region Busy

        private double _busyCounterPercent;
        private string _busyMessage;
        private bool _isBusy;
        private bool _isBusyCounterDisplayed;

        /// <summary>
        ///     Sets and gets the BusyCounterPercent property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public double BusyCounterPercent
        {
            get => _busyCounterPercent;

            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_busyCounterPercent == value)
                    return;

                _busyCounterPercent = value;
                RaisePropertyChanged(() => BusyCounterPercent);
            }
        }

        /// <summary>
        ///     Sets and gets the BusyMessage property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string BusyMessage
        {
            get => _busyMessage;

            set
            {
                if (_busyMessage == value)
                    return;

                _busyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusy property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusy
        {
            get => _isBusy;

            set
            {
                if (_isBusy == value)
                    return;

                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusyCounterDisplayed property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusyCounterDisplayed
        {
            get => _isBusyCounterDisplayed;

            set
            {
                if (_isBusyCounterDisplayed == value)
                    return;

                _isBusyCounterDisplayed = value;
                RaisePropertyChanged(() => IsBusyCounterDisplayed);
            }
        }

        public void SetBusy(bool toogle)
        {
            IsBusy = toogle;
        }

        public void SetBusy(bool toogle, string message)
        {
            SetBusyMessage(message);
            SetBusy(toogle);
        }

        public void SetBusyMessage(string message)
        {
            BusyMessage = message;
        }

        public void SetBusyProgress(double current, double max)
        {
            IsBusyCounterDisplayed = Math.Abs(current - max) > 0.1;
            BusyCounterPercent = (int) current / max * 100;
        }

        #endregion Busy

        public static void ViewLoaded(object sender, RoutedEventArgs e)
        {
            Logger = LogManager.GetCurrentClassLogger();
        }
    }

    internal class FileAlreadyExistException : Exception
    {
        public FileAlreadyExistException()
        {
        }

        public FileAlreadyExistException(string message) : base(message)
        {
        }

        public FileAlreadyExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FileAlreadyExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}