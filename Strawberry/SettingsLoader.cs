﻿namespace Strawberry
{
    using System;

    using Strawberry.Common;
    using Strawberry.Core.Config;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    public static class SettingsLoader
    {
        #region Constants

        public const string AccountPlaceHolder = "?account?";

        public const string CharacterPlaceHolder = "?character?";

        #endregion

        #region Static Fields

        [Configurable("SettingsFile", "Path of settings files, " + AccountPlaceHolder + " is replaced by the account name and " + CharacterPlaceHolder + " by the character name. You must use them !")]
        public static readonly string SettingsFile = "./settings/" + AccountPlaceHolder + "/" + CharacterPlaceHolder + ".xml";

        #endregion

        #region Constructors and Destructors

        static SettingsLoader()
            => BotManager.Instance.BotRemoved += OnBotRemoved;

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
        {
            if (!SettingsFile.Contains(AccountPlaceHolder) || !SettingsFile.Contains(CharacterPlaceHolder))
                throw new Exception($"Configurable entry 'SettingsFile' must contains {AccountPlaceHolder} and {CharacterPlaceHolder} to differentiate bot settings");

            var settingsPath = SettingsFile.Replace(AccountPlaceHolder, bot.ClientInformations.Nickname).Replace(CharacterPlaceHolder, bot.Character.Name);

            bot.LoadSettings(settingsPath);
        }

        public static void OnBotRemoved(BotManager botManager, Bot bot)
            => bot.SaveSettings();

        #endregion
    }
}