﻿namespace Strawberry.Server
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using System.Windows;

    using Newtonsoft.Json;

    using NLog;

    using Strawberry.Annotations;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Reflection;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Protocol.Messages;
    using Strawberry.UI;

    using WebSocketSharp;

    using Logger = NLog.Logger;

    public class FarmServer : Singleton<FarmServer>, IDisposable
    {
        #region Static Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly WebSocketDispatcher dispatcher;

        private readonly MessageReceiver messageReceiver;

        private WebSocket webSocket;

        private bool excpectingClose;

        #endregion

        #region Constructors and Destructors

        [UsedImplicitly]
        public FarmServer()
        {
            messageReceiver = new MessageReceiver();
            dispatcher = new WebSocketDispatcher();
            messageReceiver.Initialize();
        }

        #endregion

        #region Public Methods and Operators

        public void Close()
        {
            excpectingClose = true;
            webSocket.Close();
        }

        public void Connect()
        {
            if (webSocket == null)
            {
                webSocket = new WebSocket($"wss://{Host.FarmHost}:{Host.FarmPort}") { SslConfiguration = { ClientCertificates = new X509Certificate2Collection() } };

                var cert = Convert.FromBase64String(Application.Current.FindResource("certificate")?.ToString() ?? throw new InvalidOperationException());
                webSocket.SslConfiguration.ClientCertificates.Add(new X509Certificate(cert));

                webSocket.SslConfiguration.ServerCertificateValidationCallback += (sender, certificate, chain, errors) =>
                    {
                        var serverCert = new X509Certificate2(certificate);
                        var clientCert = new X509Certificate2(webSocket.SslConfiguration.ClientCertificates[0]);

                        return serverCert.Thumbprint == clientCert.Thumbprint;
                    };

                webSocket.SslConfiguration.CheckCertificateRevocation = true;
            }

            if (webSocket.ReadyState == WebSocketState.Open) return;

            webSocket.OnMessage += WebSocketOnOnMessage;
            webSocket.OnClose += WebSocketOnOnClose;
            webSocket.Connect();
        }

        public void Dispose()
        {
            webSocket.Close();
            ((IDisposable)webSocket)?.Dispose();
        }

        public void Send(JsonMessage message)
            => webSocket.Send(JsonConvert.SerializeObject(message, Formatting.None, new JsonSerializerSettings()));

        #endregion

        #region Methods

        private void WebSocketOnOnClose(object o, CloseEventArgs closeEventArgs)
        {
            if (excpectingClose)
                return;

            if (Host.Initialized)
                UIManager.Instance.SetBusy(true, "Reconnecting...");

            while (webSocket.ReadyState == WebSocketState.Closed)
                webSocket.Connect();

            if (Host.Initialized)
                UIManager.Instance.SetBusy(false);
        }

        private void WebSocketOnOnMessage(object sender, MessageEventArgs eventArgs)
        {
            try
            {
                var message = messageReceiver.BuildMessage(eventArgs.Data);

                if (message != null) dispatcher.Dispatch(message, dispatcher);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        #endregion
    }
}