﻿namespace Strawberry.UI.Helpers
{
    using System;
    using System.IO;
    using System.Reflection;

    public static class ApplicationInfo
    {
        #region Static Fields

        private static string applicationPath;

        private static bool applicationPathCached;

        private static string company;

        private static bool companyCached;

        private static string copyright;

        private static bool copyrightCached;

        private static string productName;

        private static bool productNameCached;

        private static string version;

        private static bool versionCached;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the path for the executable file that started the application, not including the executable name.
        /// </summary>
        public static string ApplicationPath
        {
            get
            {
                if (applicationPathCached) return applicationPath;
                var entryAssembly = Assembly.GetEntryAssembly();

                applicationPath = entryAssembly != null ? Path.GetDirectoryName(entryAssembly.Location) : string.Empty;

                applicationPathCached = true;

                return applicationPath;
            }
        }

        /// <summary>
        ///     Gets the company of the application.
        /// </summary>
        public static string Company
        {
            get
            {
                if (companyCached) return company;
                var entryAssembly = Assembly.GetEntryAssembly();

                if (entryAssembly != null)
                {
                    var attribute = (AssemblyCompanyAttribute)Attribute.GetCustomAttribute(entryAssembly, typeof(AssemblyCompanyAttribute));
                    company = attribute != null ? attribute.Company : string.Empty;
                }
                else
                {
                    company = string.Empty;
                }

                companyCached = true;

                return company;
            }
        }

        /// <summary>
        ///     Gets the copyright information of the application.
        /// </summary>
        public static string Copyright
        {
            get
            {
                if (copyrightCached) return copyright;
                var entryAssembly = Assembly.GetEntryAssembly();

                if (entryAssembly != null)
                {
                    var attribute = (AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(entryAssembly, typeof(AssemblyCopyrightAttribute));
                    copyright = attribute != null ? attribute.Copyright : string.Empty;
                }
                else
                {
                    copyright = string.Empty;
                }

                copyrightCached = true;

                return copyright;
            }
        }

        /// <summary>
        ///     Gets the product name of the application.
        /// </summary>
        public static string ProductName
        {
            get
            {
                if (productNameCached) return productName;
                var entryAssembly = Assembly.GetEntryAssembly();

                if (entryAssembly != null)
                {
                    var attribute = (AssemblyProductAttribute)Attribute.GetCustomAttribute(entryAssembly, typeof(AssemblyProductAttribute));
                    productName = attribute != null ? attribute.Product : string.Empty;
                }
                else
                {
                    productName = string.Empty;
                }

                productNameCached = true;

                return productName;
            }
        }

        /// <summary>
        ///     Gets the version number of the application.
        /// </summary>
        public static string Version
        {
            get
            {
                if (versionCached) return version;
                var entryAssembly = Assembly.GetEntryAssembly();

                version = entryAssembly != null ? entryAssembly.GetName().Version.ToString() : string.Empty;

                versionCached = true;

                return version;
            }
        }

        #endregion
    }
}