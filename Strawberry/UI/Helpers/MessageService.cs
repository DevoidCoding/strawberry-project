﻿namespace Strawberry.UI.Helpers
{
    using System.Globalization;
    using System.Windows;

    public static class MessageService
    {
        #region Properties

        private static MessageBoxOptions MessageBoxOptions => CultureInfo.CurrentUICulture.TextInfo.IsRightToLeft ? MessageBoxOptions.RtlReading : MessageBoxOptions.None;

        private static MessageBoxResult MessageBoxResult => MessageBoxResult.None;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Shows the message as error.
        /// </summary>
        /// <param name="owner">The window that owns this Message Window.</param>
        /// <param name="message">The message.</param>
        public static void ShowError(object owner, string message)
        {
            if (owner is Window ownerWindow)
                MessageBox.Show(ownerWindow, message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult, MessageBoxOptions);
            else
                MessageBox.Show(message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult, MessageBoxOptions);
        }

        /// <summary>
        ///     Shows the message.
        /// </summary>
        /// <param name="owner">The window that owns this Message Window.</param>
        /// <param name="message">The message.</param>
        public static void ShowMessage(object owner, string message)
        {
            if (owner is Window ownerWindow)
                MessageBox.Show(ownerWindow, message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult, MessageBoxOptions);
            else
                MessageBox.Show(message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult, MessageBoxOptions);
        }

        /// <summary>
        ///     Shows the specified question.
        /// </summary>
        /// <param name="owner">The window that owns this Message Window.</param>
        /// <param name="message">The question.</param>
        /// <returns><c>true</c> for yes, <c>false</c> for no and <c>null</c> for cancel.</returns>
        public static bool? ShowQuestion(object owner, string message)
        {
            var result = owner is Window ownerWindow
                             ? MessageBox.Show(ownerWindow, message, ApplicationInfo.ProductName, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel, MessageBoxOptions)
                             : MessageBox.Show(message, ApplicationInfo.ProductName, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel, MessageBoxOptions);

            switch (result)
            {
                case MessageBoxResult.Yes:
                    return true;
                case MessageBoxResult.No:
                    return false;
                case MessageBoxResult.None:
                    return false;
                case MessageBoxResult.OK:
                    return true;
                case MessageBoxResult.Cancel:
                    return false;
                default:
                    return false;
            }
        }

        /// <summary>
        ///     Shows the message as warning.
        /// </summary>
        /// <param name="owner">The window that owns this Message Window.</param>
        /// <param name="message">The message.</param>
        public static void ShowWarning(object owner, string message)
        {
            if (owner is Window ownerWindow)
                MessageBox.Show(ownerWindow, message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult, MessageBoxOptions);
            else
                MessageBox.Show(message, ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult, MessageBoxOptions);
        }

        /// <summary>
        ///     Shows the specified yes/no question.
        /// </summary>
        /// <param name="owner">The window that owns this Message Window.</param>
        /// <param name="message">The question.</param>
        /// <returns><c>true</c> for yes and <c>false</c> for no.</returns>
        public static bool ShowYesNoQuestion(object owner, string message)
        {
            var result = owner is Window ownerWindow
                             ? MessageBox.Show(ownerWindow, message, ApplicationInfo.ProductName, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No, MessageBoxOptions)
                             : MessageBox.Show(message, ApplicationInfo.ProductName, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No, MessageBoxOptions);

            return result == MessageBoxResult.Yes;
        }

        #endregion
    }
}