﻿namespace Strawberry.UI.Helpers
{
    using System.Windows;

    public class BindingProxy : Freezable
    {
        #region Static Fields

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new UIPropertyMetadata(null));

        #endregion

        #region Public Properties

        public object Data
        {
            get => GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }

        #endregion

        #region Methods

        #region Overrides of Freezable

        protected override Freezable CreateInstanceCore()
            => new BindingProxy();

        #endregion

        #endregion
    }
}