﻿namespace Strawberry.UI.View
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for LogsUserControl.xaml
    /// </summary>
    public partial class LogsUserControl : UserControl
    {
        #region Constructors and Destructors

        public LogsUserControl()
            => InitializeComponent();

        #endregion
    }
}