﻿namespace Strawberry.UI.View.Partial
{
    using System.Windows.Controls;

    using Strawberry.Common.View;
    using Strawberry.UI.ViewModel;

    /// <summary>
    ///     Interaction logic for BotPartialView.xaml
    /// </summary>
    public partial class BotPartialView : UserControl, IView<BotViewModel>
    {
        #region Constructors and Destructors

        public BotPartialView(BotViewModel botViewModel)
        {
            InitializeComponent();
            DataContext = ViewModel = botViewModel;
        }

        #endregion

        #region Public Properties

        public BotViewModel ViewModel { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (BotViewModel)value;
        }

        #endregion
    }
}