﻿// <copyright file="MainWindow.xaml.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/13/2016 11:18</date>

namespace Strawberry.UI.View
{
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;

    using Strawberry.Common.View;
    using Strawberry.UI.ViewModel;

    using Telerik.Windows.Controls;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadWindow, IView<UIManager>
    {
        #region Constructors and Destructors

        public MainWindow()
        {
            //VisualStudio2013Palette.LoadPreset(VisualStudio2013Palette.ColorVariation.Dark);
            VisualStudio2013Palette.Palette.AccentMainColor = Color.FromRgb(0xD1, 0x30, 0x3C);
            VisualStudio2013Palette.Palette.AccentDarkColor = Color.FromRgb(0xAA, 0x27, 0x31);
            VisualStudio2013Palette.Palette.AccentColor = Color.FromRgb(0xAA, 0x27, 0x31);
            VisualStudio2013Palette.Palette.HeaderColor = Color.FromRgb(0xF0, 0xF0, 0xF0);
            VisualStudio2013Palette.Palette.MouseOverColor = Color.FromRgb(0xD1, 0x30, 0x3C);
            VisualStudio2013Palette.Palette.FontFamily = (FontFamily)Application.Current.Resources["OpenSansLight"];

            Closed += (s, e) => ViewModelLocator.Cleanup();
            ViewModel = UIManager.Instance;
            ViewModel.View = this;
            Loaded += UIManager.Instance.OnInitialized;

            InitializeComponent();
        }

        #endregion

        #region Public Properties

        public UIManager ViewModel { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (UIManager)value;
        }

        #endregion

        #region Methods

        private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) this.ParentOfType<Window>().DragMove();
        }

        #endregion
    }
}