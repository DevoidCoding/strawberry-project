﻿namespace Strawberry.UI.View
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for AccountsUserControl.xaml
    /// </summary>
    public partial class AccountsUserControl : UserControl
    {
        #region Constructors and Destructors

        public AccountsUserControl()
            => InitializeComponent();

        #endregion
    }
}