﻿namespace Strawberry.UI.ViewModel
{
    using GalaSoft.MvvmLight;

    using Strawberry.Common.View;
    using Strawberry.Common.ViewModel;

    public class TabViewModel : ViewModelBase, ITabViewModel
    {
        #region Fields

        private bool _isSelected;

        private IView _view;

        #endregion

        #region Constructors and Destructors

        #region Public Constructors

        public TabViewModel(string header, IView view)
        {
            Header = header;
            View = view;
        }

        #endregion Public Constructors

        #endregion

        #region Public Properties

        public string Header { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value)
                    return;

                _isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        /// <summary>
        ///     Sets and gets the View property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public IView View
        {
            get => _view;
            set
            {
                if (Equals(_view, value))
                    return;

                _view = value;
                RaisePropertyChanged(() => View);
            }
        }

        #endregion
    }
}