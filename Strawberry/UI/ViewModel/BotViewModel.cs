﻿namespace Strawberry.UI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Controls;

    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Threading;

    using Strawberry.Common;
    using Strawberry.Common.View;
    using Strawberry.Common.ViewModel;
    using Strawberry.UI.View.Partial;

    public class BotViewModel : ViewModelBase, IViewModel<BotPartialView>, IBotViewModel
    {
        #region Fields

        private readonly ObservableCollection<TabViewModel> views = new ObservableCollection<TabViewModel>();

        #endregion

        #region Constructors and Destructors

        public BotViewModel(Bot bot)
            => Bot = bot;

        #endregion

        #region Public Properties

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public Bot Bot { get; }

        public BotPartialView View { get; set; }

        /// <summary>
        ///     Sets and gets the Views property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        public ReadOnlyObservableCollection<TabViewModel> Views => new ReadOnlyObservableCollection<TabViewModel>(views);

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (BotPartialView)value;
        }

        #endregion

        #region Public Methods and Operators

        public async Task<ITabViewModel> AddView(IViewModel viewModel, Func<IView> viewFunc)
        {
            TabViewModel tabViewModel = null;
            await DispatcherHelper.RunAsync(() => { DispatcherHelper.CheckBeginInvokeOnUI(() => tabViewModel = AddViewInternal(viewModel, viewFunc)); });
            RaisePropertyChanged(() => Views);
            return tabViewModel;
        }

        #endregion

        #region Methods

        internal TabViewModel AddViewInternal(IViewModel viewModel, Func<IView> viewFunc)
        {
            var view = viewFunc();
            var tabViewModel = new TabViewModel(string.Empty, view);

            view.ViewModel = viewModel;
            viewModel.View = view;

            ((UserControl)viewModel.View).DataContext = viewModel;

            views.Add(tabViewModel);
            RaisePropertyChanged(() => Views);
            return tabViewModel;
        }

        public async Task<bool> RemoveView(IView view)
        {
            var isRemoved = false;
            var views = this.views.Where(e => e.View == view).ToArray();
            await DispatcherHelper.RunAsync(() => DispatcherHelper.CheckBeginInvokeOnUI(() => isRemoved = views.All(this.views.Remove)));
            return isRemoved;
        }

        #endregion
    }
}