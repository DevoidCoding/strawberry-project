﻿// <copyright file="ViewModelLocator.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/13/2016 11:17</date>

namespace Strawberry.UI.ViewModel
{
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Ioc;

    using Strawberry.Common.ViewModel;
    using Strawberry.Server;
    using Strawberry.Services;
    using Strawberry.Services.Design;
    using Strawberry.Services.Interfaces;

    public class ViewModelLocator
    {
        #region Constructors and Destructors

        static ViewModelLocator()
        {
            if (ViewModelBase.IsInDesignModeStatic) SimpleIoc.Default.Register<IAccountService, DesignAccountService>();
            else SimpleIoc.Default.Register<IAccountService, AccountService>();

            SimpleIoc.Default.Register(() => UIManager.Instance);
            SimpleIoc.Default.Register<AccountsViewModel>();
            SimpleIoc.Default.Register<LogsViewModel>();

            SimpleIoc.Default.Register<IAccountsViewModel>(() => SimpleIoc.Default.GetInstance<AccountsViewModel>(), true);
        }

        #endregion

        #region Public Properties

        public AccountsViewModel Accounts => SimpleIoc.Default.GetInstance<AccountsViewModel>();

        public LogsViewModel Logs => SimpleIoc.Default.GetInstance<LogsViewModel>();

        public UIManager Main => SimpleIoc.Default.GetInstance<UIManager>();

        #endregion

        #region Public Methods and Operators

        public static void Cleanup()
            => FarmServer.Instance.Close();

        #endregion
    }
}