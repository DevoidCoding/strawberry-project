﻿namespace Strawberry.UI.ViewModel
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using Strawberry.Common;
    using Strawberry.Common.ViewModel;
    using Strawberry.Model;
    using Strawberry.Services.Interfaces;

    using Telerik.Windows.Controls;

    // ReSharper disable once ClassNeverInstantiated.Global
    public class AccountsViewModel : ViewModelBase, IAccountsViewModel
    {
        #region Fields

        private readonly IAccountService accountService;

        #endregion

        #region Constructors and Destructors

        public AccountsViewModel(IAccountService accountService)
            => this.accountService = accountService;

        #endregion

        #region Public Properties

        public ObservableCollection<Account> Accounts => new ObservableCollection<Account>(accountService.GetAccounts());

        #endregion

        #region Explicit Interface Properties

        ObservableCollection<IAccount> IAccountsViewModel.Accounts => new ObservableCollection<IAccount>(Accounts);

        #endregion

        #region Public Methods and Operators

        public Account AddAccount(Bot bot)
        {
            Account account = null;

            InvokeOnUIThread(
                () =>
                    {
                        account = new Account(bot);
                        accountService.AddAccount(account, null);
                        OnPropertyChanged(() => Accounts);
                    });

            return account;
        }

        public Account RemoveAccount(Bot bot)
        {
            // TODO : Remove bot and fck off.
            Account account = null;

            InvokeOnUIThread(
                () =>
                    {
                        account = Accounts.FirstOrDefault(e => e.Bot.Id == bot.Id);
                        accountService.RemoveAccount(account, null);
                        OnPropertyChanged(() => Accounts);
                    });

            return account;
        }

        #endregion
    }
}