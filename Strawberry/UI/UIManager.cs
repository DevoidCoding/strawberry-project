﻿namespace Strawberry.UI
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Ioc;

    using Strawberry.Common;
    using Strawberry.Common.ViewModel;
    using Strawberry.Model;
    using Strawberry.UI.View;
    using Strawberry.UI.ViewModel;

    // ReSharper disable once InconsistentNaming
    public class UIManager : ViewModelBase, IViewModel<MainWindow>
    {
        #region Static Fields

        private static UIManager instance;

        #endregion

        #region Fields

        private double busyCounterPercent;

        private string busyMessage;

        private bool isBusy;

        private bool isBusyCounterDisplayed;

        #endregion

        #region Public Properties

        public static UIManager Instance => instance ?? (instance = new UIManager());

        /// <summary>
        ///     Sets and gets the BusyCounterPercent property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public double BusyCounterPercent
        {
            get => busyCounterPercent;
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (busyCounterPercent == value) return;

                busyCounterPercent = value;
                RaisePropertyChanged(() => BusyCounterPercent);
            }
        }

        /// <summary>
        ///     Sets and gets the BusyMessage property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public string BusyMessage
        {
            get => busyMessage;
            set
            {
                if (busyMessage == value) return;

                busyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusy property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy == value) return;

                isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        /// <summary>
        ///     Sets and gets the IsBusyCounterDisplayed property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsBusyCounterDisplayed
        {
            get => isBusyCounterDisplayed;
            set
            {
                if (isBusyCounterDisplayed == value) return;

                isBusyCounterDisplayed = value;
                RaisePropertyChanged(() => IsBusyCounterDisplayed);
            }
        }

        public MainWindow View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (MainWindow)value;
        }

        #endregion

        #region Public Methods and Operators

        public Account AddAccount(Bot bot)
        {
            var viewmodel = SimpleIoc.Default.GetInstance<AccountsViewModel>();
            return viewmodel.AddAccount(bot);
        }

        public BotViewModel GetBotViewModel(Bot bot)
        {
            var viewmodel = SimpleIoc.Default.GetInstance<AccountsViewModel>();
            return viewmodel.Accounts.FirstOrDefault(e => e.Bot == bot)?.BotViewModel ?? throw new Exception($"{bot} ViewModel not found");
        }

        public void OnInitialized(object sender, EventArgs e)
            => Task.Factory.StartNew(
                () =>
                    {
                        Host.Initialize();
                        Host.Start();
                    });

        public void RemoveAccount(Bot bot)
        {
            var viewmodel = SimpleIoc.Default.GetInstance<AccountsViewModel>();
            var account = viewmodel.RemoveAccount(bot);
            account?.Bot?.Dispose();
            account?.Dispose();
        }

        public void SetBusy(bool toogle)
            => IsBusy = toogle;

        public void SetBusy(bool toogle, string message)
        {
            SetBusyMessage(message);
            SetBusy(toogle);
        }

        public void SetBusyMessage(string message)
            => BusyMessage = message;

        public void SetBusyProgress(double current, double max)
        {
            IsBusyCounterDisplayed = Math.Abs(current - max) > 0.1;
            BusyCounterPercent = (int)current / max * 100;
        }

        #endregion
    }
}