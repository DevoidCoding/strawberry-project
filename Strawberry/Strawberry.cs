﻿namespace Strawberry
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading;

    using NLog;

    using Strawberry.Common;
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Common.Data.Icons;
    using Strawberry.Common.Data.Maps;
    using Strawberry.Common.Game.World.Utils;
    using Strawberry.Common.Messages;
    using Strawberry.Common.Plugins;
    using Strawberry.Core.Config;
    using Strawberry.Core.I18N;
    using Strawberry.Core.Machine;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Reflection;
    using Strawberry.Core.Server;
    using Strawberry.Core.UI;
    using Strawberry.MITM;
    using Strawberry.MITM.Injection;
    using Strawberry.Protocol.Tools.Ele;
    using Strawberry.Server;
    using Strawberry.UI;

    public class Host
    {
        #region Constants

#if DEBUG
        public const string FarmHost = "localhost";
#else
        public const string FarmHost = "164.132.58.249";
#endif

        public const int FarmPort = 3000;

        private const string ConfigPath = "./config.xml";

        #endregion

        #region Static Fields

        private static readonly List<Assembly> Hierarchy = new List<Assembly>
                                                               {
                                                                   Assembly.Load("Strawberry.Core"),
                                                                   Assembly.Load("Strawberry.Protocol"),
                                                                   Assembly.Load("Strawberry.Common"),
                                                                   Assembly.Load("Strawberry.MITM"),
                                                                   Assembly.Load("Strawberry")
                                                               };

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static string dofusPath;

        #endregion

        #region Public Events

        public static event UnhandledExceptionEventHandler UnhandledException;

        #endregion

        #region Public Properties

        [Configurable("AutoMITMInject")]
        public static bool AutoMITMInject { get; set; } = true;

        [Configurable("BotAuthHost")]
        public static string BotAuthHost { get; set; } = "localhost";

        [Configurable("BotAuthPort")]
        public static int BotAuthPort { get; set; } = 5555;

        [Configurable("BotWorldHost")]
        public static string BotWorldHost { get; set; } = "localhost";

        [Configurable("BotWorldPort")]
        public static int BotWorldPort { get; set; } = 5556;

        public static Config Config { get; private set; }

        public static DispatcherTask DispatcherTask { get; private set; }

        [Configurable("DofusBasePath")]
        public static string DofusBasePath { get; set; } = Path.Combine(OSInfo.GetLocalAppData(), @"\Ankama\Dofus 2\");

        [Configurable("DofusDataPath")]
        public static string DofusDataPath { get; set; } = @"app\data\common";

        [Configurable("DofusElementsPath")]
        public static string DofusElementsPath { get; set; } = @"app\content\maps\elements.ele";

        [Configurable("DofusExe")]
        public static string DofusExe { get; set; } = @"app\Dofus.exe";

        [Configurable("DofusI18NPath")]
        public static string DofusI18NPath { get; set; } = @"app\data\i18n";

        [Configurable("DofusItemIconPath")]
        public static string DofusItemIconPath { get; set; } = @"app\content\gfx\items\bitmap0.d2p";

        [Configurable("DofusMapsD2P")]
        public static string DofusMapsD2P { get; set; } = @"app\content\maps\maps0.d2p";

        [Configurable("DofusWorldGraph")]
        public static string DofusWorldGraph { get; set; } = @"app\content\maps\worldgraph.bin";

        [Configurable("FindDofusPathAuto", "When true the dofus path will be find automatically")]
        public static bool FindDofusPathAuto { get; set; } = true;

        public static bool Initialized { get; private set; }

        public static MITM.MITM MITM { get; private set; }

        [Configurable("RealAuthHost")]
        public static string RealAuthHost { get; set; } = "213.248.126.39";

        [Configurable("RealAuthPort")]
        public static int RealAuthPort { get; set; } = 5555;

        public static bool Running { get; private set; }

        #endregion

        #region Public Methods and Operators

        public static string GetDofusPath()
        {
            if (dofusPath != null)
                return dofusPath;

            if (!FindDofusPathAuto)
                return dofusPath = DofusBasePath;

            var localappdata = OSInfo.GetLocalAppData();

            if (Directory.Exists(Path.Combine(localappdata, @"Ankama\Dofus")))
            {
                dofusPath = Path.Combine(localappdata, @"Ankama\Dofus");
            }
            else if (Directory.Exists(Path.Combine(localappdata, @"Ankama\Dofus 2")))
            {
                dofusPath = Path.Combine(localappdata, @"Ankama\Dofus 2");
            }
            else
            {
                var programFiles = OSInfo.GetProgramFiles();

                if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus")))
                {
                    dofusPath = Path.Combine(programFiles, @"Ankama\Dofus");
                }
                else if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus 2")))
                {
                    dofusPath = Path.Combine(programFiles, @"Ankama\Dofus 2");
                }
                else
                {
                    programFiles = OSInfo.GetProgramFilesX86();

                    if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus")))
                        dofusPath = Path.Combine(programFiles, @"Ankama\Dofus");
                    else if (Directory.Exists(Path.Combine(programFiles, @"Ankama\Dofus 2")))
                        dofusPath = Path.Combine(programFiles, @"Ankama\Dofus 2");
                }
            }

            return dofusPath;
        }

        public static void Initialize()
        {
            if (Initialized)
                return;

            UIManager.Instance.SetBusy(true, "Connecting...");
            FarmServer.Instance.Connect();
            try
            {
                if (!Debugger.IsAttached) // the debugger handle the unhandled exceptions
                    AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

                // Register Handler for server without plugins
                WebSocketDispatcher.DefineHierarchy(Hierarchy);
                Hierarchy.ForEach(WebSocketDispatcher.RegisterSharedAssembly);

                AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
                UIManager.Instance.SetBusyMessage("Load config ...");
                Config = new Config(ConfigPath);

                foreach (var assembly in Hierarchy)
                {
                    Config.BindAssembly(assembly);
                    Config.RegisterAttributes(assembly);
                }

                Config.Load();
                Logger.Info("{0} loaded", Path.GetFileName(Config.FilePath));
                UIManager.Instance.SetBusyMessage("Loading D2O files ...");
                ObjectDataManager.Instance.AddReaders(Path.Combine(GetDofusPath(), DofusDataPath));
                I18NDataManager.Instance.DefaultLanguage = Languages.French;
                I18NDataManager.Instance.AddReaders(Path.Combine(GetDofusPath(), DofusI18NPath));
                IconsManager.Instance.Initialize(Path.Combine(GetDofusPath(), DofusItemIconPath));
                ProtocolConverter.Instance.RegisterTypes(Hierarchy.First(e => e.GetName().Name.Equals("Strawberry.Protocol")), Hierarchy.First(e => e.GetName().Name.Equals("Strawberry.Common")));

                // UIManager.Instance.BusyMessage = "Starting redis server ...";
                // logger.Info("Starting redis server ...");
                // RedisServerHost.Instance.ExecutablePath = RedisServerExe;
                // RedisServerHost.Instance.StartOrFindProcess();
                UIManager.Instance.SetBusyMessage($"Loading {MapsManager.MapsDataFile}...");
                Logger.Info("Loading {0}...", MapsManager.MapsDataFile);
                var progression = MapsManager.Instance.Initialize(Path.Combine(GetDofusPath(), DofusMapsD2P));

                if (progression != null)
                    ExecuteProgress(progression);

                UIManager.Instance.SetBusyMessage("Loading maps positions ...");
                Logger.Info("Loading maps positions ...");
                progression = MapsPositionManager.Instance.Initialize();

                if (progression != null)
                    ExecuteProgress(progression);

                // UIManager.Instance.SetBusyMessage("Loading submaps ...");
                // Logger.Info("Loading submaps ...");
                // progression = SubMapsManager.Instance.Initialize();

                // if (progression != null)
                // ExecuteProgress(progression);
                UIManager.Instance.SetBusyMessage("Loading elements ...");
                Logger.Info("Loading elements ...");
                EleReader.Instance.Initialize(Path.Combine(GetDofusPath(), DofusElementsPath));

                UIManager.Instance.SetBusyMessage("Loading world pathfinder ...");
                Logger.Info("Loading world pathfinder ...");
                WorldPathFinder.Initialize(Path.Combine(GetDofusPath(), DofusWorldGraph));

                MITM = new MITM.MITM(new MITMConfiguration { FakeAuthHost = BotAuthHost, FakeAuthPort = BotAuthPort, FakeWorldHost = BotWorldHost, FakeWorldPort = BotWorldPort, RealAuthHost = RealAuthHost, RealAuthPort = RealAuthPort });

                MessageDispatcher.DefineHierarchy(Hierarchy);

                foreach (var assembly in Hierarchy)
                    MessageDispatcher.RegisterSharedAssembly(assembly);

                UIManager.Instance.SetBusyMessage("Loading plugins ...");
                PluginManager.Instance.LoadAllPlugins();
                DispatcherTask = new DispatcherTask(new MessageDispatcher(), MITM);
                DispatcherTask.Start(); // we have to start it now to dispatch the initialization msg

                BotManager.Instance.Initialize();
                var msg = new HostInitializationMessage();
                DispatcherTask.Dispatcher.Enqueue(msg, MITM);
                msg.Wait();
            }
            finally
            {
                UIManager.Instance.SetBusy(false);
            }

            Initialized = true;
        }

        public static void Start()
        {
            if (Running)
                return;

            Running = true;
            MITM.Start();
            Injector.Instance.Start();
        }

        #endregion

        #region Methods

        private static void ExecuteProgress(ProgressionCounter progression, int refreshRate = 100)
        {
            var lastValue = progression.Value;

            while (!progression.IsEnded)
            {
                Thread.Sleep(refreshRate);

                if (Math.Abs(lastValue - progression.Value) > 0.1)
                {
                    Logger.Debug("{0} : {1:0.0}/{2} Mem:{3}MB", progression.Text, progression.Value, progression.Total, GC.GetTotalMemory(false) / (1024 * 1024));

                    UIManager.Instance.SetBusyProgress(progression.Value, progression.Total);
                }

                lastValue = progression.Value;
            }

            // Report last time to attend 100%
            UIManager.Instance.SetBusyProgress(progression.Value, progression.Total);
            GC.Collect();
        }

        private static void OnProcessExit(object sender, EventArgs e)
            => Stop();

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Fatal((Exception)e.ExceptionObject, "Unhandled exception : ");

            try
            {
                Stop();
            }
            finally
            {
                if (UnhandledException != null)
                {
                    UnhandledException(sender, e);
                }
                else
                {
                    Console.WriteLine(@"Press enter to exit");
                    Console.Read();
                }

                Environment.Exit(-1);
            }
        }

        private static void Stop()
        {
            if (!Running)
                return;

            Running = false;
            BotManager.Instance.RemoveAll();
            Config?.Save();
            MITM?.Stop();
            Injector.Instance.Stop();
            DispatcherTask?.Stop();

            // PluginManager.Instance.UnLoadAllPlugins();
            // RedisServerHost.Instance.Shutdown();
        }

        #endregion
    }
}