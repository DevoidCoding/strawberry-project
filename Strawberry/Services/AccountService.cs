﻿// <copyright file="AccountService.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/15/2016 16:41</date>

namespace Strawberry.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Strawberry.Model;
    using Strawberry.Services.Interfaces;

    public class AccountService : IAccountService
    {
        #region Fields

        private readonly ObservableCollection<Account> _accounts = new ObservableCollection<Account>();

        #endregion

        #region Public Methods and Operators

        public void AddAccount(Account account, Action<Account, Exception> callback)
        {
            Exception exception = null;

            if (account == null)
                exception = new ArgumentNullException(nameof(account));

            if (exception != null && callback != null)
                callback(null, exception);
            else if (exception != null)
                throw exception;

            _accounts.Add(account);
            callback?.Invoke(account, exception);
        }

        public IEnumerable<Account> GetAccounts()
            => _accounts;

        public void RemoveAccount(Account account, Action<Account, Exception> callback)
        {
            Exception exception = null;

            if (account == null)
                exception = new ArgumentNullException(nameof(account));

            if (exception != null && callback != null)
                callback(null, exception);
            else if (exception != null)
                throw exception;

            _accounts.Remove(account);
            callback?.Invoke(account, exception);
        }

        #endregion
    }
}