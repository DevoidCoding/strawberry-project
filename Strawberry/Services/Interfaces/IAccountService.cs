﻿// <copyright file="IAccountService.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/15/2016 16:40</date>

namespace Strawberry.Services.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Strawberry.Model;

    public interface IAccountService
    {
        #region Public Methods and Operators

        void AddAccount(Account account, Action<Account, Exception> callback);

        IEnumerable<Account> GetAccounts();

        void RemoveAccount(Account account, Action<Account, Exception> callback);

        #endregion
    }
}