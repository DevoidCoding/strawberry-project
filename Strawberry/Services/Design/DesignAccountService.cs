﻿// <copyright file="DesignAccountService.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/15/2016 16:46</date>

namespace Strawberry.Services.Design
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Strawberry.Model;
    using Strawberry.Services.Interfaces;

    // ReSharper disable once ClassNeverInstantiated.Global
    public class DesignAccountService : IAccountService
    {
        #region Public Methods and Operators

        public void AddAccount(Account account, Action<Account, Exception> callback)
            => throw new NotImplementedException();

        public IEnumerable<Account> GetAccounts()
            => new ObservableCollection<Account>();

        public void RemoveAccount(Account account, Action<Account, Exception> callback)
            => throw new NotImplementedException();

        #endregion
    }
}