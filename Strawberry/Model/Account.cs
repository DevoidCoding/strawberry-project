﻿// <copyright file="Account.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/15/2016 16:42</date>

namespace Strawberry.Model
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Strawberry.Annotations;
    using Strawberry.Common;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.ViewModel;
    using Strawberry.UI.View.Partial;
    using Strawberry.UI.ViewModel;

    public sealed class Account : INotifyPropertyChanged, IDisposable, IAccount
    {
        #region Fields

        private BotViewModel botViewModel;

        #endregion

        #region Constructors and Destructors

        public Account(Bot bot)
        {
            Bot = bot;

            BotViewModel = new BotViewModel(bot);
            BotViewModel.View = new BotPartialView(BotViewModel);

            Header = bot.Name;
            Icon = "/Images/user.png";
            bot.CharacterSelected += BotOnCharacterSelected;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public Bot Bot { get; }

        /// <summary>
        ///     Sets and gets the BotViewModel property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public BotViewModel BotViewModel
        {
            get => botViewModel;
            private set
            {
                if (botViewModel == value) return;

                botViewModel = value;
                OnPropertyChanged(nameof(BotViewModel));
            }
        }

        public string Header { get; set; }

        public string Icon { get; private set; }

        #endregion

        #region Explicit Interface Properties

        IBotViewModel IAccount.BotViewModel => BotViewModel;

        #endregion

        #region Public Methods and Operators

        public void Dispose()
        {
        }

        #endregion

        #region Methods

        private void BotOnCharacterSelected(Bot bot, PlayedCharacter character)
        {
            Icon = bot.GetSkin("face", 1, 48, 48, 0);
            OnPropertyChanged(nameof(Icon));
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        #endregion
    }
}