﻿// <copyright file="App.xaml.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/13/2016 11:17</date>

namespace Strawberry
{
    using System.Windows;

    using GalaSoft.MvvmLight.Threading;

    using Strawberry.UI.View;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constructors and Destructors

        static App()
            => DispatcherHelper.Initialize();

        #endregion

        #region Methods

        protected override void OnStartup(StartupEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            base.OnStartup(e);
        }

        #endregion
    }
}