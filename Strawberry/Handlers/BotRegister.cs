﻿namespace Strawberry.Handlers
{
    using Strawberry.Common.Messages;
    using Strawberry.Core.Messages;
    using Strawberry.UI;

    public class BotRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(BotAddedMessage))]
        public static void HandleBotAddedMessage(object dummy, BotAddedMessage message)
            => UIManager.Instance.AddAccount(message.Bot);

        [MessageHandler(typeof(BotRemovedMessage))]
        public static void HandleBotRemovedMessage(object dummy, BotRemovedMessage message)
            => UIManager.Instance.RemoveAccount(message.Bot);

        #endregion
    }
}