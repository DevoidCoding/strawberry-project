﻿namespace Strawberry.Handlers.Server
{
    using Strawberry.Common.Messages;
    using Strawberry.Common.Plugins;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Protocol.Messages.Plugin;
    using Strawberry.Server;

    public class PluginHandler
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(HostInitializationMessage))]
        public static void OnHostInitializationMessage(object bot, HostInitializationMessage message)
            => FarmServer.Instance.Send(new PluginsRequestMessage());

        #endregion

        #region Methods

        [JsonMessageHandler(typeof(PluginLoadMessage))]
        private static void OnPluginLoadMessage(WebSocketDispatcher dispatcher, PluginLoadMessage message)
            => PluginManager.Instance.LoadPlugin(message.Assembly.Filename, message.Assembly.Content);

        #endregion
    }
}