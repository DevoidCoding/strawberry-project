﻿namespace Strawberry.Handlers.Server
{
    using System.Collections.Generic;
    using System.Linq;

    using Strawberry.Common;
    using Strawberry.Common.Messages;
    using Strawberry.Core.Cryptography;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Protocol.Messages.Security;
    using Strawberry.Farm.Protocol.Types;
    using Strawberry.Server;

    public class RawDataHandler
    {
        #region Static Fields

        private static readonly Dictionary<string, byte[]> RawDataPerTicket = new Dictionary<string, byte[]>();

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(RawDataBotRequestMessage))]
        public static void OnRawDataBotRequestMessage(Bot bot, RawDataBotRequestMessage message)
        {
            RawDataPerTicket.Add(bot.ClientInformations.Ticket, message.Content);
            FarmServer.Instance.Send(new RawDataRequestMessage { GameServerTicket = bot.ClientInformations.Ticket, MD5 = Cryptography.GetMD5Hash(message.Content) });
        }

        #endregion

        #region Methods

        [JsonMessageHandler(typeof(RawDataBinaryRequestMessage))]
        private static void OnRawDataBinaryRequestMessage(WebSocketDispatcher dispatcher, RawDataBinaryRequestMessage message)
        {
            if (message is RawDataRespondMessage rawDataRespondMessage)
            {
                var bot = BotManager.Instance.Bots.FirstOrDefault(e => e.ClientInformations.Ticket == message.GameServerTicket);
                bot.ClientInformations.HashKey = rawDataRespondMessage.HashKey;
                bot.SendLocal(new CheckIntegrityMessage(rawDataRespondMessage.CheckIntegrityContent));
            }
            else
            {
                var rdm = RawDataPerTicket[message.GameServerTicket];
                var md5 = Cryptography.GetMD5Hash(rdm);
                var msg = new RawDataBinaryMessage { Assembly = new Assembly(Cryptography.GetMD5Hash(rdm), rdm), GameServerTicket = message.GameServerTicket, MD5 = md5 };
                FarmServer.Instance.Send(msg);
            }
        }

        #endregion
    }
}