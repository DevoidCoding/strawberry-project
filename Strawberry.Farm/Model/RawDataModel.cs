﻿namespace Strawberry.Farm.Model
{
    internal class RawDataModel
    {
        #region Public Properties

        public byte[] Key { get; set; }

        public byte[] Modulo { get; set; }

        #endregion
    }
}