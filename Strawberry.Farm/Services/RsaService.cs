﻿namespace Strawberry.Farm.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Org.BouncyCastle.Crypto.Engines;
    using Org.BouncyCastle.Crypto.Parameters;
    using Org.BouncyCastle.Math;
    using Org.BouncyCastle.Security;

    internal static class RsaService
    {
        #region Public Methods and Operators

        public static byte[] Hash(byte[] data, byte[] modulus)
        {
            var key = new RsaKeyParameters(false, new BigInteger(modulus), new BigInteger("65537"));
            var bigInteger = new BigInteger(modulus);
            var cipher = new RsaEngine();
            cipher.Init(true, key);
            var pad = Pkcs1Pad(data, data.Length, cipher.GetOutputBlockSize());
            return cipher.ProcessBlock(pad, 0, pad.Length);
        }

        public static byte[] Pkcs1Pad(IReadOnlyList<byte> src, int end, int n)
        {
            var ret = new byte[n];
            var p = 0;
            end = Math.Min(end, Math.Min(src.Count, p + n - 11));
            var i = end - 1;

            while (i >= p && n > 11)
                ret[--n] = src[i--];

            ret[--n] = 0;
            var rng = new SecureRandom();

            while (n > 2)
            {
                ret[--n] = (byte)rng.NextInt();
                Trace.WriteLine(ret[n]);
            }

            ret[--n] = 0x02;
            ret[--n] = 0;
            return ret;
        }

        #endregion
    }
}