﻿namespace Strawberry.Farm.Services
{
    using System;
    using System.IO;
    using System.Linq;

    using Strawberry.Core.Reflection;
    using Strawberry.Farm.Protocol.Types;

    public class PluginService : Singleton<PluginService>
    {
        #region Constructors and Destructors

        public PluginService()
            => Initialize();

        #endregion

        #region Public Methods and Operators

        public Assembly Get(string pluginName)
        {
            var file = Directory.GetFiles("plugins", "*.dll").FirstOrDefault(e => Path.GetFileNameWithoutExtension(e).Equals(pluginName, StringComparison.InvariantCultureIgnoreCase));
            return string.IsNullOrWhiteSpace(file) ? null : new Assembly(pluginName, File.ReadAllBytes(file));
        }

        public void Initialize()
        {
            if (!Directory.Exists("plugins")) return;

            Console.WriteLine($"{Directory.GetFiles("plugins", "*.dll")} plugin(s) loaded");
        }

        #endregion
    }
}