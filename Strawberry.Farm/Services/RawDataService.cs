﻿namespace Strawberry.Farm.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using JetBrains.Annotations;

    using Newtonsoft.Json;

    using Strawberry.Core.Cryptography;
    using Strawberry.Core.IO;
    using Strawberry.Core.Reflection;
    using Strawberry.Farm.Model;
    using Strawberry.Farm.Protocol.Messages.Security;

    internal class RawDataService : Singleton<RawDataService>, IDisposable
    {
        #region Constants

        private const string EmulatorExe = @"Emulator.exe";

        private const string PatcherExe = @"RawDataPatcher.exe";

        private const string RawDataModelsJson = "RawDataModels.json";

        #endregion

        #region Fields

        private readonly Dictionary<string, SemaphoreSlim> decryptingRawData = new Dictionary<string, SemaphoreSlim>();

        private readonly JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();

        private readonly Process process = new Process();

        private CancellationTokenSource cancellation = new CancellationTokenSource();

        private Dictionary<string, RawDataModel> rawDataModels = new Dictionary<string, RawDataModel>();

        #endregion

        #region Constructors and Destructors

        [UsedImplicitly]
        public RawDataService()
            => LoadRawDataModels();

        #endregion

        #region Public Methods and Operators

        public void AddRawData(RawDataInformationsMessage message)
        {
            if (!rawDataModels.ContainsKey(message.MD5))
                rawDataModels.Add(message.MD5, new RawDataModel { Key = message.Key.Select(e => (byte)e).ToArray(), Modulo = message.Modulo.Select(e => (byte)e).ToArray() });

            if (!decryptingRawData.TryGetValue(message.MD5, out var semaphore)) return;
            semaphore.Release(int.MaxValue);
            decryptingRawData.Remove(message.MD5);
        }

        public void Dispose()
        {
            cancellation.Dispose();
            process.Dispose();
        }

        public async Task<RawDataBinaryRequestMessage> GetRawDataResponse(RawDataRequestMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            if (!rawDataModels.ContainsKey(message.MD5) && !(message is RawDataBinaryMessage))
                return new RawDataBinaryRequestMessage { GameServerTicket = message.GameServerTicket };

            if (message is RawDataBinaryMessage binaryMessage && !rawDataModels.ContainsKey(binaryMessage.MD5))
                if (!decryptingRawData.ContainsKey(message.MD5))
                {
                    decryptingRawData.Add(message.MD5, new SemaphoreSlim(0));
                    await PatchRawDataMessage(binaryMessage);
                    SaveRawDataModels();
                }
                else
                    await decryptingRawData[message.MD5].WaitAsync(cancellation.Token);
            else if (decryptingRawData.TryGetValue(message.MD5, out var semaphore))
                await semaphore.WaitAsync(cancellation.Token);

            return rawDataModels.ContainsKey(message.MD5) ? GenerateRawDataBinaryRequestMessage(message) : new RawDataErrorMessage { GameServerTicket = message.GameServerTicket };
        }

        #endregion

        #region Methods

        private static byte[] RandomHashKey()
        {
            var ret = new byte[16];
            new Random().NextBytes(ret);
            return ret;
        }

        private static byte[] XorData(IList<byte> data, IReadOnlyList<byte> xorKey)
        {
            var xored = data.ToArray();

            for (var i = 0; i < data.Count; i++)
                xored[i] = (byte)(xored[i] ^ xorKey[i % xorKey.Count]);

            return xored;
        }

        private RawDataBinaryRequestMessage GenerateRawDataBinaryRequestMessage(RawDataRequestMessage message)
        {
            var rawDataModel = rawDataModels[message.MD5];
            var hashKey = RandomHashKey();
            var gameServerTicket = message.GameServerTicket;
            var writer = new BigEndianWriter();
            writer.WriteUTF(gameServerTicket);
            writer.WriteShort((short)hashKey.Length);
            writer.WriteBytes(hashKey);

            // Need to xor data before encrypting them
            var data = writer.Data;
            var xored = XorData(data, rawDataModel.Key);
            var encrypted = RsaService.Hash(xored, rawDataModel.Modulo);
            return new RawDataRespondMessage { CheckIntegrityContent = encrypted, HashKey = hashKey, GameServerTicket = message.GameServerTicket };
        }

        private void LoadRawDataModels()
        {
            if (!File.Exists(RawDataModelsJson)) return;

            var reader = new JsonTextReader(new StreamReader(File.OpenRead(RawDataModelsJson)));
            var serializer = JsonSerializer.Create(jsonSerializerSettings);
            serializer.Formatting = Formatting.Indented;

            rawDataModels = serializer.Deserialize<Dictionary<string, RawDataModel>>(reader) ?? new Dictionary<string, RawDataModel>();

            reader.Close();
        }

        private async Task PatchRawDataMessage(RawDataBinaryMessage binaryMessage)
        {
            Console.WriteLine($"Patching {binaryMessage.MD5}");

            try
            {
                var md5 = Cryptography.GetMD5Hash(binaryMessage.Assembly.Content);

                File.WriteAllBytes($@"Patcher\raw-{md5}.swf", binaryMessage.Assembly.Content);

                process.StartInfo.FileName = PatcherExe;
                process.StartInfo.Arguments = $"{md5}";
                process.StartInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Patcher");
                process.Start();
                process.WaitForExit();

                process.StartInfo.FileName = EmulatorExe;
                process.StartInfo.Arguments = $"{md5}";
                process.StartInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Emulator");
                process.Start();

                await decryptingRawData[binaryMessage.MD5].WaitAsync(cancellation.Token);
            }
            catch (TaskCanceledException)
            {
                cancellation = new CancellationTokenSource();
            }
        }

        private void SaveRawDataModels()
        {
            var writer = new JsonTextWriter(new StreamWriter(File.OpenWrite(RawDataModelsJson)));
            var serializer = JsonSerializer.Create(jsonSerializerSettings);
            serializer.Formatting = Formatting.Indented;
            serializer.Serialize(writer, rawDataModels);
            writer.Close();
        }

        #endregion
    }
}