﻿namespace Strawberry.Farm.Extensions
{
    using Newtonsoft.Json;

    using Strawberry.Core.Messages;

    using WebSocketSharp;

    public static class WebSocketExtensions
    {
        #region Public Methods and Operators

        public static void Send(this WebSocket webSocket, JsonMessage message)
            => webSocket.Send(JsonConvert.SerializeObject(message, Formatting.None, new JsonSerializerSettings())); // {ContractResolver = new CamelCasePropertyNamesContractResolver()}));

        #endregion
    }
}