﻿namespace Strawberry.Farm
{
    using System;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;

    using JetBrains.Annotations;

    using Strawberry.Core.Reflection;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Protocol.Messages;
    using Strawberry.Farm.Services;
    using Strawberry.Farm.WebSocketServices;

    using WebSocketSharp.Server;

    // ReSharper disable LocalizableElement
    public class Server : Singleton<Server>, IDisposable
    {
        #region Fields

        private readonly MessageReceiver messageReceiver = new MessageReceiver();

        private readonly WebSocketDispatcher webSocketDispatcher = new WebSocketDispatcher();

        private readonly WebSocketServer webSocketEmulatorServer = new WebSocketServer(3001);

        private readonly WebSocketServer webSocketServer = new WebSocketServer(3000, true);

        #endregion

        #region Constructors and Destructors

        [UsedImplicitly]
        public Server()
            => Initialize();

        #endregion

        #region Public Methods and Operators

        public void Dispose()
        {
            Stop();
            RawDataService.Instance.Dispose();
        }

        public void Start()
        {
            webSocketServer.Start();
            webSocketEmulatorServer.Start();
            Console.WriteLine($"Server started.");
        }

        public void Stop()
        {
            webSocketServer.Stop();
            webSocketEmulatorServer.Stop();
            Console.WriteLine($"Server stoped.");
        }

        #endregion

        #region Methods

        private void Initialize()
        {
            messageReceiver.Initialize();
            InitializeWebSocket();
        }

        private void InitializeWebSocket()
        {
            webSocketServer.Realm = "Strawberry Farm";
            webSocketServer.SslConfiguration.ClientCertificateRequired = true;
            webSocketServer.SslConfiguration.CheckCertificateRevocation = true;
            webSocketServer.SslConfiguration.ServerCertificate = new X509Certificate2("certificate.pfx", "123456789");
            webSocketServer.SslConfiguration.EnabledSslProtocols = SslProtocols.Default;

            webSocketServer.AddWebSocketService("/", () => new BasicWebSocketService(webSocketDispatcher, messageReceiver));

            webSocketEmulatorServer.Realm = "Strawberry Emulator";

            webSocketEmulatorServer.AddWebSocketService("/", () => new BasicWebSocketService(webSocketDispatcher, messageReceiver));
        }

        #endregion
    }
}