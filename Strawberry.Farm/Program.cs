﻿namespace Strawberry.Farm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    using Strawberry.Core.IO;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Services;

    internal class Program
    {
        #region Static Fields

        private static readonly List<Assembly> Hierarchy = new List<Assembly> { Assembly.Load("Strawberry.Farm") };

        #endregion

        #region Constructors and Destructors

        private Program()
        {
            Initialized += OnInitialized;
            Initialize();
        }

        #endregion

        #region Public Events

        public event EventHandler Initialized;

        #endregion

        #region Methods

        private static void Main()
        {
            // ReSharper disable once ObjectCreationAsStatement
            Task.Run(() => { new Program(); });

            while (Console.ReadLine() != "exit")
            {
            }
        }

        private static void OnInitialized(object sender, EventArgs eventArgs)
        {
            Server.Instance.Start();

            // var ws = new WebSocket(@"wss:\\127.0.0.1:3000");
            // var content = File.ReadAllBytes(@"D:\Projets\Strawberry-Project\strawberry-project\bin\Client\Debug\RDM\Merkator\rdm.game.2018-02-10T11-03-26.swf");
            // var md5 = Cryptography.GetMD5Hash(content);
            // ws.OnOpen += (o, args) => ws.Send(new RawDataBinaryMessage() { Assembly = new Protocol.Types.Assembly(md5, content), GameServerTicket = "42", MD5 = md5 });
            // ws.Connect();
            var hashKey = new byte[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var writer = new BigEndianWriter();
            writer.WriteUTF("42");
            writer.WriteShort((short)hashKey.Length);
            writer.WriteBytes(hashKey);

            var key = Convert.FromBase64String("HmppSTBJPhIljzWmgUK9OA==");
            var data = writer.Data.ToArray();
            for (var i = 0; i < data.Length; i++) data[i] = (byte)(data[i] ^ key[i % key.Length]);

            var publicModulo = Convert.FromBase64String(
                "AJMwkOWlxqc6+Q+NtkP9R4i1A3sSg65+WM6AXa6AilW8Wzbl/PIL7E+dvDeHuUKa2Fi4FinoGMia4Ze7azxLKxjGwRs8k8e88UIocrE+iiF7H9uElxtgS2G9hKx2ImhJJ5zpY4sY0K++8arA6UHJbGakO4g/SkXdOPbSJ3gOAwTAPbJYEWSGplDEKL4p5XN/Lwa3zCEQuqDEk2PhbNdALloMxp20/Xrc4f47fEzQTjlzWZjxVqr7L4oz/kQb/uQOtg2d3mOG36Vu4CBI46CMwKCpnoayE2R9P+Zg9DKYkotr/QS9hB2x0++gOswpoch3cNvhnUp0l26MniD6vlpWejU=");

            var rsaEncrypted = RsaService.Hash(data, publicModulo);

            var sbytes = rsaEncrypted.Select(e => (sbyte)e).ToArray();
        }

        private void Initialize()
        {
            WebSocketDispatcher.DefineHierarchy(Hierarchy);
            Hierarchy.ForEach(WebSocketDispatcher.RegisterSharedAssembly);

            OnInitialized();
        }

        private void OnInitialized()
            => Initialized?.Invoke(this, EventArgs.Empty);

        #endregion
    }
}