﻿namespace Strawberry.Farm.Handlers.RawData
{
    using System;

    using JetBrains.Annotations;

    using Strawberry.Core.Messages;
    using Strawberry.Farm.Protocol.Messages.Security;
    using Strawberry.Farm.Services;
    using Strawberry.Farm.WebSocketServices;

    [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class RawDataHandler
    {
        #region Methods

        [JsonMessageHandler(typeof(RawDataRequestMessage))]
        private static async void OnRawDataBinaryMessage(BasicWebSocketService dispatcher, RawDataRequestMessage message)
        {
            message.MD5 = message.MD5.ToUpperInvariant();
            var rawDataBinaryRequestMessage = await RawDataService.Instance.GetRawDataResponse(message);
            Console.WriteLine($"Sending {message} for {message.MD5}");
            dispatcher.Send(rawDataBinaryRequestMessage);
        }

        [JsonMessageHandler(typeof(RawDataInformationsMessage))]
        private static void OnRawDataInformationsMessage(BasicWebSocketService dispatcher, RawDataInformationsMessage message)
            => RawDataService.Instance.AddRawData(message);

        #endregion
    }
}