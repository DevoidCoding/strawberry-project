﻿namespace Strawberry.Farm.Handlers.Plugins
{
    using Strawberry.Core.Messages;
    using Strawberry.Farm.Protocol.Messages.Plugin;
    using Strawberry.Farm.Services;
    using Strawberry.Farm.WebSocketServices;

    public class PluginsHandler
    {
        #region Methods

        [JsonMessageHandler(typeof(PluginsRequestMessage))]
        private static void OnPluginsRequestMessage(BasicWebSocketService dispatcher, PluginsRequestMessage message)
        {
            var assembly = PluginService.Instance.Get("TreasureHuntPlugin");
            if (assembly == null) return;
            dispatcher.Send(new PluginLoadMessage { Assembly = assembly, HasServerHandler = true });
        }

        #endregion
    }
}