﻿namespace Strawberry.Farm.WebSocketServices
{
    using System;

    using Newtonsoft.Json;

    using Strawberry.Core.Messages;
    using Strawberry.Core.Server;
    using Strawberry.Farm.Protocol.Messages;

    using WebSocketSharp;
    using WebSocketSharp.Server;

    public class BasicWebSocketService : WebSocketBehavior
    {
        #region Fields

        private readonly WebSocketDispatcher dispatcher;

        private readonly MessageReceiver messageReceiver;

        #endregion

        #region Constructors and Destructors

        public BasicWebSocketService(WebSocketDispatcher dispatcher, MessageReceiver messageReceiver)
        {
            this.dispatcher = dispatcher;
            this.messageReceiver = messageReceiver;
        }

        #endregion

        #region Public Methods and Operators

        public void Send(JsonMessage message)
            => Send(JsonConvert.SerializeObject(message));

        #endregion

        #region Methods

        protected override void OnMessage(MessageEventArgs eventArgs)
        {
            try
            {
                var message = messageReceiver.BuildMessage(eventArgs.Data);

                if (message != null) dispatcher.Dispatch(message, this);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        #endregion
    }
}