﻿namespace SimplePlugin.Handlers
{
    using System.Diagnostics;
    using System.Drawing;
    using System.Threading;
    using System.Threading.Tasks;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Actors;
    using Strawberry.Common.Game.Movements;
    using Strawberry.Common.Game.World.Pathfinding;
    using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Network;
    using Strawberry.Protocol.Messages;

    public class PathfindersComparerRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatClientMultiMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (!message.Content.StartsWith(".compare pathfinder"))
                return;

            message.BlockNetworkSend();

            if (bot.HasFrame<PathfindersComparer>())
                bot.RemoveFrame<PathfindersComparer>();
            else
                bot.AddFrame(new PathfindersComparer(bot));
        }

        #endregion
    }

    public class PathfindersComparer : Frame<PathfindersComparer>
    {
        #region Constructors and Destructors

        public PathfindersComparer(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.Character.StartMoving += OnStartMoving;

        [MessageHandler(typeof(GameMapMovementCancelMessage))]
        public void HandleGameMapMovementCancelMessage(Bot bot, GameMapMovementCancelMessage message)
            => bot.SendToClient(new DebugHighlightCellsMessage(Color.Violet.ToArgb(), new[] { message.CellId }));

        [MessageHandler(typeof(GameMapMovementRequestMessage), FromFilter = ListenerEntry.Client)]
        public void HandleGameMapMovementRequestMessage(Bot bot, GameMapMovementRequestMessage message)
        {
            bot.SendToClient(new DebugClearHighlightCellsMessage());

            var clientPath = Path.BuildFromClientCompressedPath(bot.Character.Map, message.KeyMovements);

            var pathfinder = new Pathfinder(bot.Character.Map, bot.Character.Map);
            var ffpathfinder = new PathFinder(bot.Character.Map, false);
            var stopwatch = new Stopwatch();

            Path botPath1 = null, botPath2 = null;
            stopwatch.Start();

            for (var i = 0; i < 100; i++)
                botPath1 = pathfinder.FindPath(bot.Character.Cell, clientPath.End, true);

            stopwatch.Stop();

            bot.Character.SendWarning("Dofus-like PathFinder x 100 = {0}", stopwatch.Elapsed.ToString("ss\\.fff"));

            stopwatch.Reset();
            stopwatch.Start();

            for (var i = 0; i < 100; i++)
                botPath2 = ((ISimplePathFinder)ffpathfinder).FindPath(bot.Character.Cell, clientPath.End, true);

            stopwatch.Stop();

            bot.Character.SendWarning("FF PathFinder x 100 = {0}", stopwatch.Elapsed.ToString("ss\\.fff"));
            //botPath2 = new Path(bot.Character.Map, FFpathfinder.GetLastPathUnpacked(0).Select(id => bot.Character.Map.Cells[id]));
            // if you see red cells it means the pathfinder is wrong and don't get the same path as the client

            bot.Character.HighlightCells(botPath1?.Cells, Color.Red);
            bot.Character.HighlightCells(clientPath.Cells, Color.Blue);
            bot.Character.HighlightCells(botPath2?.Cells, Color.Green);

            message.KeyMovements = botPath1?.GetClientPathKeys();
        }

        #endregion

        #region Methods

        private void OnStartMoving(ContextActor actor, MovementBehavior movement)
        {
            var bot = BotManager.Instance.GetCurrentBot();

            Task.Factory.StartNew(
                () =>
                    {
                        var element = movement.TimedPath.GetCurrentElement();

                        bot.Character.HighlightCell(element.CurrentCell, Color.Green);

                        while (!movement.IsEnded())
                        {
                            var newElement = movement.TimedPath.GetCurrentElement();

                            if (element != newElement)
                            {
                                element = newElement;

                                bot.Character.ResetCellsHighlight();
                                bot.Character.HighlightCell(element.CurrentCell, Color.Green);
                            }

                            Thread.Sleep(30);
                        }
                    });
        }

        #endregion
    }
}