﻿using System;
using System.Linq;
using Strawberry.Common;
using Strawberry.Common.Frames;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.Interactives;
using Strawberry.Core.Messages;
using Strawberry.Core.Threading;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public class GatherTestRegister
    {
        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (message.Content == ".gather on")
            {
                message.BlockNetworkSend(); // do not send this message to the server

                if (bot.Character.Jobs.All(x => x.JobTemplate.Id != GatherTest.FarmerJob))
                {
                    bot.Character.SendMessage("You haven't the farmer job");
                }
                else
                {
                    bot.AddFrame(new GatherTest(bot));
                    bot.Character.SendMessage("Gathering ...");
                }
            }
            else if (message.Content == ".gather off")
            {
                message.BlockNetworkSend(); // do not send this message to the server


                bot.RemoveFrame<GatherTest>();
                bot.Character.SendMessage("Stop gathering ...");
            }
        }
    }

    public class GatherTest : Frame<GatherTest>
    {
        public const int GatherTickTime = 5000;

        public const int WheathInteractive = 38;
        public const int FarmerJob = 28;

        private SimplerTimer _timeoutGatherTimer;
        private double _lastTriedInteractive;

        public bool Stopped;

        public GatherTest(Bot bot)
            : base(bot)
        {

        }

        public override void OnAttached()
        {
            Bot.Character.StopUsingInteractive += OnStopUsingInteractive;
            Bot.Character.StartUsingInteractive += OnStartUsingInteractive;
            CheckRessources();
        }

        public override void OnDetached()
        {
            Stopped = true;
            Bot.Character.StopUsingInteractive -= OnStopUsingInteractive;
            Bot.Character.StartUsingInteractive -= OnStartUsingInteractive;
        }

        private void CheckRessources()
        {
            if (Stopped)
                return;

            DisposeTimer();

            if (Bot.Character.IsFighting() || Bot.Character.IsUsingInteractive() || Bot.Character.IsMoving())
            {
                Bot.CallDelayed(GatherTickTime, CheckRessources);
                return;
            }

            var ressource = Bot.Character.Map.Interactives.Where(x => x.Type != null && x.Type.Id == WheathInteractive && x.Cell != null &&
                x.EnabledSkills.Count > 0 &&
                x.State == InteractiveState.None && x.Id != _lastTriedInteractive).
                OrderBy(x => x.Cell.ManhattanDistanceTo(Bot.Character.Cell)).FirstOrDefault();

            if (ressource == null)
            {
                // should change map here
                Bot.CallDelayed(GatherTickTime, CheckRessources);
                return;
            }

            // avoid being stucked
            _lastTriedInteractive = ressource.Id;


            var skill = ressource.EnabledSkills.FirstOrDefault();

            if (skill == null)
                return;

            if (!Bot.Character.UseInteractiveObject(skill))
                Bot.CallDelayed(GatherTickTime, CheckRessources);
            else
                _timeoutGatherTimer = Bot.CallDelayed(4000, InteractiveUseTimeout);
        }

        private void OnStartUsingInteractive(RolePlayActor actor, InteractiveObject interactive, InteractiveSkill skill, DateTime? usageEndTime)
        {
            DisposeTimer();
        }

        private void OnStopUsingInteractive(RolePlayActor actor, InteractiveObject interactive, InteractiveSkill skill)
        {
            CheckRessources();
        }

        private void InteractiveUseTimeout()
        {
            // timeout
            if (!Bot.Character.IsUsingInteractive())
                CheckRessources();
        }

        private void DisposeTimer() => _timeoutGatherTimer?.Dispose();
    }
}