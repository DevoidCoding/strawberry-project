﻿using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Common;
using Strawberry.Core.Config;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public static class GameCommands
    {
        private static Bot _bot;
        private static readonly List<TchatCommand> Commands = new List<TchatCommand>();

        [Configurable("CharCommand")] private const char CharCommand = '#';

        internal static void OnHelpCommand(string[] parameters, Bot bot)
        {
            foreach (var line in GetHelpCommands())
                bot.Character.SendMessage(line, System.Drawing.Color.MidnightBlue);
        }

        public static void CreateTchatCommand(string name, string help, Action<string[], Bot> action)
        {
            var commandName = name.ToLowerInvariant();
            if (Commands.Count(entry => entry.CommandName == commandName) == 1)
                throw new InvalidOperationException($"Command {name} already exists.");
            Commands.Add(new TchatCommand(name, help, action));
        }

        public static string[] GetHelpCommands()
        {
            var sb = new List<string> {"<b><u>Help commands :</u></b>"};
            sb.AddRange(Commands.Select(cmd => GetHelpCommand(cmd.CommandName)));

            return sb.ToArray();
        }

        public static string GetHelpCommand(string commandName)
        {
            var cmd = Commands.FirstOrDefault(entry => entry.CommandName == commandName);
            return cmd == null ? $"Command <b>{commandName}</b> doesn't exist." : $"<b>{cmd.Name}</b> : {cmd.Help}";
        }


        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (_bot == null)
            {
                _bot = bot;
                CreateTchatCommand("Help", "help", OnHelpCommand);
            }

            if (message.Content[0] == CharCommand)
                message.BlockNetworkSend();
            else
                return;

            var parts = message.Content.Substring(1).Split(' ');
            var commandName = parts[0].ToLowerInvariant();
            var parameters = new List<string>(parts.Length - 1);
            for (var i = 1; i < parts.Length; i++)
            {
                if (parts[i].StartsWith("\""))
                {

                    var paramInQuote = parts[i].Substring(1);
                    i++;
                    while (!parts[i].EndsWith("\""))
                    {
                        paramInQuote += " " + parts[i];
                        i++;
                    }
                    paramInQuote += " " + parts[i].Substring(0, parts[i].Length - 1);
                    parameters.Add(paramInQuote);
                }
                else
                    parameters.Add(parts[i]);
            }

            if (Commands.Count(entry => entry.CommandName == commandName) == 1)
                Commands.First(entry => entry.CommandName == commandName).Action(parameters.ToArray(), bot);
            else
                bot.Character.SendMessage($"The tchat command <b>{commandName}</b> doesn't exist.", System.Drawing.Color.Red);
        }
    }
}