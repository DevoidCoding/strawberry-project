﻿using System;
using Strawberry.Common;
using Strawberry.Common.Game.World;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public class ChangeMapTester
    {
        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (message.Content.StartsWith(".move"))
            {
                message.BlockNetworkSend();// do not send this message to the server

                var splits = message.Content.Split(' ');
                if (splits.Length != 2)
                {
                    bot.Character.SendMessage("syntax : .move [Right/Left/Top/Bottom/Any]");
                    return;
                }

                MapNeighbour neighbour;
                try
                {
                    neighbour = (MapNeighbour)Enum.Parse(typeof(MapNeighbour), splits[1]);
                }
                catch (Exception)
                {
                    bot.Character.SendMessage("syntax : .move [Right/Left/Top/Bottom/Any]");
                    return;
                }


                if (bot.Character.ChangeMap(neighbour) == MapNeighbour.None)
                {
                    bot.Character.SendMessage($"Cannot move to {neighbour} !");
                }
            }
        }
    }
}