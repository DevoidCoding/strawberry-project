﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using Strawberry.Common;
using Strawberry.Common.Data.D2O;
using Strawberry.Common.Data.I18N;
using Strawberry.Common.Data.Maps;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.MapTraveling.Transitions;
using Strawberry.Common.Game.World.Utils;
using Strawberry.Core.Messages;
using Strawberry.Core.UI;
using Strawberry.Protocol.Data;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public class AlgoTester
    {
        public static bool IsLosActivated { get; set; }

        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (message.Content.StartsWith(".algo submap"))
            {
                bot.Character.ResetCellsHighlight();

                var sw = Stopwatch.StartNew();
                var submaps = SubMapsManager.Instance.GetMapSubMapsBinder(bot.Character.Map.Id);
                sw.Stop();
                bot.Character.SendMessage($"{sw.ElapsedMilliseconds}ms");

                foreach (var subMap in submaps)
                {
                    var random = new Random();
                    var hue = random.Next(0, 361);
                    bot.Character.SendMessage($"[{subMap.GlobalId} mapid:{subMap.MapId} submap:{subMap.SubMapId}]");

                    var step = 1d / subMap.Neighbours.Count;

                    for (var i = 0; i < subMap.Neighbours.Count; i++)
                    {
                        var neighbour = subMap.Neighbours[i];
                        var color = HsvColorConverter.ColorFromHsv(hue, step * (i + 1), 1);

                        bot.Character.SendMessage(
                            $"[{subMap.GlobalId}] to [{neighbour.GlobalId}] ({(neighbour.Transition as MovementTransition)?.MapNeighbour ?? MapNeighbour.None})");

                        if (neighbour.Transition is MovementTransition transition)
                            bot.Character.HighlightCells(transition.Cells.Select(e => (ushort) e), color);
                    }
                }

                message.BlockNetworkSend();
            }
            else if (message.Content.StartsWith(".algo los"))
            {
                if (message.Content.Split(' ').Length == 3)
                {
                    IsLosActivated = message.Content.Split(' ').Last() == "on";
                    return;
                }

                var canBeSee = new List<Cell>();
                var cannotBeSee = new List<Cell>();

                Cell currentCell;

                if (bot.Character.IsFighting())
                    currentCell = bot.Character.Fighter.Cell;
                else
                    currentCell = bot.Character.Cell;

                foreach (var cell in bot.Character.Map.Cells.Where(x => x.Walkable))
                    if (bot.Character.Context.CanBeSeen(currentCell, cell, !bot.Character.IsFighting()))
                        canBeSee.Add(cell);
                    else
                        cannotBeSee.Add(cell);

                bot.Character.ResetCellsHighlight();

                bot.Character.HighlightCells(canBeSee, Color.Green);
                bot.Character.HighlightCells(cannotBeSee, Color.Red);

                message.BlockNetworkSend();
            }
            else if (message.Content.StartsWith(".algo rune"))
            {
                var spell = ObjectDataManager.Instance.EnumerateObjects<Spell>().FirstOrDefault(e
                    => string.Equals(I18NDataManager.Instance.ReadText(e.NameId), "Rune Terre",
                        StringComparison.CurrentCultureIgnoreCase));

                bot.SendToServer(new GameActionFightCastRequestMessage((ushort) spell?.Id,
                    bot.Character.Fight.AliveActorsCells.First().Id));
            }
            else if (message.Content.StartsWith(".pf"))
            {
                message.BlockNetworkSend();
                var args = message.Content.Split(' ');
                if (args.Length != 2) return;
                if (!int.TryParse(args[1], out var mapId)) return;

                bot.Character.WorldPathFinder.Move(mapId);
            }
        }
    }
}