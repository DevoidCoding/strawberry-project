﻿using Strawberry.Common;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public static class ChatCommands
    {
        [MessageHandler(typeof(ChatClientMultiMessage))]
        public static void HandleChatMessage(Bot bot, ChatClientMultiMessage message)
        {
            // if the client sends ".hello" in the chat
            if (message.Content == ".hello")
            {
                message.BlockNetworkSend();// do not send this message to the server

                bot.Character.SendMessage($"Hello {bot.Character.Name} you are on sub area {bot.Character.Map.SubArea.Name}");
            }
        }
    }
}