﻿/*using Strawberry.Common;
using Strawberry.Common.Data.Maps;
using Strawberry.Common.Frames;
using Strawberry.Common.Game.Actors;
using Strawberry.Common.Game.Movements;
using Strawberry.Common.Game.World;
using Strawberry.Common.Game.World.Data;
using Strawberry.Common.Game.World.Pathfinding;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public static class NeighboursCheckerRegister
    {
        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
        {
            bot.AddFrame(new NeighboursChecker(bot));
        }
    }

    public class NeighboursChecker : Frame<NeighboursChecker>
    {
        private MapPositionData _lastMapPos;
        private Path _lastPath;

        public NeighboursChecker(Bot bot)
            : base(bot)
        {
            bot.Character.StopMoving += OnStopMoving;
        }

        private void OnStopMoving(ContextActor actor, MovementBehavior movement, bool canceled, bool refused)
        {
            _lastPath = movement.MovementPath;
        }

        [MessageHandler(typeof(ChangeMapMessage))]
        public void HandleChangeMapMessage(Bot bot, ChangeMapMessage message)
        {
            var mapPosition = MapsPositionManager.Instance.GetMapPosition((int)message.MapId);
            if (_lastPath != null && _lastMapPos != null)
            {
                var direction = Map.GetDirectionOfTransitionCell(_lastPath.End);
                var neighbour = _lastMapPos.GetNeighbourId(direction);

                if (neighbour == null)
                    bot.Character.SendWarning("The actual map ({0}) is not the {1} neighbour of the previous map {2} (NOT FOUND)", message.MapId, direction, _lastMapPos.MapId);
                else if (neighbour != message.MapId)
                    bot.Character.SendWarning("The actual map ({0}) is not the {1} neighbour of the previous map {2}" +
                                              "(MISMATCH attempt : {3})", message.MapId, direction, _lastMapPos.MapId, neighbour);
                else
                {
                    bot.Character.SendDebug("You came from {0}", direction);
                }
            }

            _lastMapPos = mapPosition;
        }


    }
}*/