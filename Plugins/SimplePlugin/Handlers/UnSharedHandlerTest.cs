﻿using Strawberry.Common;
using Strawberry.Common.Frames;
using Strawberry.Common.Messages;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    internal class UnSharedHandlerTest
    {
        [MessageHandler(typeof(BotAddedMessage))]
        public static void OnBotAdded(object sender, BotAddedMessage message)
        {
            message.Bot.AddFrame(new HandlerClass(message.Bot));
        }
    }

    public class HandlerClass : Frame<HandlerClass>
    {
        public HandlerClass(Bot bot)
            : base(bot)
        {
        }

        [MessageHandler(typeof(ChatClientMultiMessage))]
        public void HandleChatClientMultiMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (message.Content == ".test")
            {
                bot.Character.OpenPopup("Yes Man !");
                message.BlockNetworkSend();
            }
            else if (message.Content == ".nop")
            {
                bot.RemoveFrame(this);
                message.BlockNetworkSend();
            }
        }

    }
}