﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Strawberry.Common;
using Strawberry.Common.Messages;
using Strawberry.Core.Config;
using Strawberry.Core.Messages;
using Strawberry.Core.Reflection;

namespace SimplePlugin.Handlers
{
    public static class PacketsLogger
    {
        [Configurable("AllowLogging", "If true, all messsages for this char will be dumped in a file")]
        public static bool AllowLogging;

        private static readonly ObjectDumper Dumper = new ObjectDumper(2, true, false, BindingFlags.Public | BindingFlags.Instance |
            BindingFlags.GetField | BindingFlags.FlattenHierarchy);


        [MessageHandler(typeof(BotAddedMessage))]
        public static void OnBotAdded(object sender, BotAddedMessage message)
        {
            if (AllowLogging)
                message.Bot.Dispatcher.MessageDispatched += OnMessageDispatched;
        }

        private static void OnMessageDispatched(MessageDispatcher dispatcher, Message message)
        {
            var bot = (Bot)dispatcher.CurrentProcessor;
            var file = $"{SimplePlugin.CurrentPlugin.GetPluginDirectory()}/log {bot} {Process.GetCurrentProcess().StartTime:dd MMM HH-mm-ss}.log";

            // this is thread safe
            try
            {
                File.AppendAllText(file, Dumper.Dump(message) + Environment.NewLine);
            }
            catch (Exception ex)
            {
                bot.Character.SendError("The log file for {0} can't be written in {1} : {2}", bot.Character, file, ex.Message);
                AllowLogging = false;
            }
        }
    }
}