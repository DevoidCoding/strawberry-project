﻿using System;
using Strawberry.Common;

namespace SimplePlugin.Handlers
{
    public class TchatCommand
    {
        public Action<string[], Bot> Action { get; }

        public string Name { get; }

        public string Help { get; }

        public string CommandName { get; }

        public TchatCommand(string name, string help, Action<string[], Bot> action)
        {
            Name = name;
            CommandName = name.ToLowerInvariant();
            Help = help;
            Action = action;
        }
    }
}