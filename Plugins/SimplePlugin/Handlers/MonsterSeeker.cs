﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Strawberry.Common;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Core.Config;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    public class MonsterSeeker
    {
        [Configurable("AllowSeeker")]
        public static bool AllowSeeker = false;

        public static bool SeekArchiMonster = true;

        private static readonly List<string> Seeked = new List<string>();

        [MessageHandler(typeof(MapComplementaryInformationsDataMessage))]
        public static void HandleMapMessage(Bot bot, MapComplementaryInformationsDataMessage message)
        {
            if (!AllowSeeker) return;

            var groups = bot.Character.Map.Actors.OfType<GroupMonster>().ToArray();
            var alreadySignaled = new List<string>();
            foreach (var group in groups)
            {
                var monster = SeekedInGroup(group);
                if (!string.IsNullOrEmpty(monster))
                {
                    if (!alreadySignaled.Contains(monster))
                    {
                        alreadySignaled.Add(monster);

                        bot.Character.SendMessage($"Le monstre <b>'{monster}'</b> a été trouvé sur cette carte.", System.Drawing.Color.Green);
                    }
                }
                var archi = SeekedArchiMonsterInGroup(group);
                if (!string.IsNullOrEmpty(archi))
                {
                    bot.Character.SendMessage($"L'archimonstre <b>'{archi}'</b> a été trouvé sur cette carte.", System.Drawing.Color.Red);
                }
            }
        }

        private static string SeekedInGroup(GroupMonster group)
        {
            if (group == null) return null;

            var monsters = group.Monsters.Where(entry => IsSeeking(entry.Name.ToLowerInvariant())).Select(entry => entry.Name).ToArray();
            return monsters.Length == 0 ? null : monsters.FirstOrDefault();
        }

        private static string SeekedArchiMonsterInGroup(GroupMonster group)
        {
            if (group == null) return null;
            if (!SeekArchiMonster) return null;

            var monsters = group.Monsters.Where(entry => entry.IsArchMonster).Select(entry => entry.Name).ToArray();
            return monsters.Length == 0 ? null : monsters.Aggregate((a, b) => a + ", " + b);
        }

        internal static ReadOnlyCollection<string> SeekedMonsters => Seeked.AsReadOnly();

        internal static bool AddMonster(string name)
        {
            if (Seeked.Contains(name))
                return false;
            Seeked.Add(name);
            return true;
        }

        internal static bool RemoveMonster(string name)
        {
            if (!Seeked.Contains(name))
                return false;
            Seeked.Remove(name);
            return true;
        }

        internal static bool IsSeeking(string name)
        {
            return Seeked.Contains(name);
        }

        internal static void HandleSeekCommand(string[] parameters, Bot bot)
        {
            if (parameters.Length != 1)
            {
                bot.Character.SendMessage("Invalid use. Use help command for more informations.", System.Drawing.Color.Firebrick);
                return;
            }
            if (AddMonster(parameters[0].ToLowerInvariant()))
                bot.Character.SendMessage($"Le monstre '{parameters[0]}' est désormais recherché.", System.Drawing.Color.Green);
            else
                bot.Character.SendMessage($"Le monstre '{parameters[0]}' est déjà recherché.", System.Drawing.Color.DarkSlateGray);
        }

        internal static void HandleStopCommand(string[] parameters, Bot bot)
        {
            if (parameters.Length != 1)
            {
                bot.Character.SendMessage("Invalid use. Use help command for more informations.", System.Drawing.Color.Firebrick);
                return;
            }
            if (RemoveMonster(parameters[0].ToLowerInvariant()))
                bot.Character.SendMessage($"Le monstre '{parameters[0]}' n'est désormais plus recherché.", System.Drawing.Color.Green);
            else
                bot.Character.SendMessage($"Le monstre '{parameters[0]}' n'était pas recherché.", System.Drawing.Color.DarkSlateGray);
        }

        internal static void HandleListCommand(string[] parameters, Bot bot)
        {
            bot.Character.SendMessage(SeekedMonsters.Count > 0 ? $"Les monstres suivants sont recherchés :\n{SeekedMonsters.Aggregate((a, b) => a + ", " + b)}" : "Aucun monstre n'est recherché.", System.Drawing.Color.DarkSlateGray);
        }
    }
}