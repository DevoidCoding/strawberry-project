﻿using Strawberry.Common;
using Strawberry.Common.Frames;
using Strawberry.Common.Messages;
using Strawberry.Core.Messages;
using Strawberry.Protocol.Messages;

namespace SimplePlugin.Handlers
{
    internal static class WelcomeMessageRegister
    {
        [MessageHandler(typeof(BotAddedMessage))]
        public static void OnBotAdded(object sender, BotAddedMessage message)
        {
            message.Bot.AddFrame(new WelcomeMessage(message.Bot));
        }
    }

    public class WelcomeMessage : Frame<WelcomeMessage>
    {
        private bool _messageSent;

        public WelcomeMessage(Bot bot)
            : base(bot)
        {
        }

        [MessageHandler(typeof(GameContextCreateMessage))]
        public void HandleGameContextCreateMessage(Bot bot, GameContextCreateMessage message)
        {
            if (_messageSent)
                return;

            var settings = bot.Settings.GetOrAddEntry<Settings>();

            bot.Character.SendMessage(settings.WelcomeMessage);

            if (settings.WelcomeMessage.EndsWith("!"))
                settings.WelcomeMessage = settings.WelcomeMessage.Remove(settings.WelcomeMessage.Length - 1, 1);
            else
                settings.WelcomeMessage += "!";

            _messageSent = true;
        }
    }
}