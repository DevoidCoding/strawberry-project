﻿using System;
using Strawberry.Common.Plugins;

namespace SimplePlugin
{
    public class SimplePlugin : PluginBase
    {
        #region Public Constructors

        public SimplePlugin(PluginContext context) : base(context)
        {
            if (CurrentPlugin != null)
                throw new Exception("Can be instancied only once");

            CurrentPlugin = this;
        }

        public static SimplePlugin CurrentPlugin
        {
            get;
            private set;
        }

        #endregion Public Constructors

        #region Public Properties

        public override string Author => "Strawberry";
        public override string Description => "Just a plugin example";
        public override string Name => "SimplePlugin";
        public override bool UseConfig => true;
        public override Version Version => new Version(1, 0, 0);

        #endregion Public Properties

        #region Public Methods

        public override void Dispose()
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        #endregion Public Methods
    }
}