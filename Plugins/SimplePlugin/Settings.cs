﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Common.Settings;

namespace SimplePlugin
{
    public class Settings : SettingsEntry
    {
        public Settings()
        {
            WelcomeMessage = "Welcome";
        }

        public override string EntryName => "SimplePlugin";

        public string WelcomeMessage
        {
            get;
            set;
        }
    }
}
