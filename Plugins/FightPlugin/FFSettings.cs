﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Common.Game.Actors.RolePlay;
using Strawberry.Common.Settings;
using Strawberry.Protocol.Enums;

namespace FightPlugin
{
    public class FFSettings : SettingsEntry
    {
        public FFSettings()
            : base()
        {
            /*FavoredAttackSpells = new List<uint>();
            FavoredBoostSpells = new List<uint>();
            FavoredCurseSpells = new List<uint>();
            FavoredHealSpells = new List<uint>();
            FavoredInvocSpells = new List<uint>();

            IsInvoker = false;
            IsHealer = false;*/
        }

        public void Init(PlayedCharacter character)
        {
            FavoredAttackSpells = new List<int>();
            FavoredBoostSpells = new List<int>();
            FavoredCurseSpells = new List<int>();
            FavoredHealSpells = new List<int>();
            FavoredInvocSpells = new List<int>();
            IsInvoker = false;
            IsHealer = false;
            // You can set here default preference for each breed   
            MaxPower = 1.0;
            switch ((BreedEnum) character.Breed.Id)
            {
                case BreedEnum.Cra:
                    FavoredAttackSpells = new List<int>() {169};
                    break;
                case BreedEnum.Ecaflip:
                    break;
                case BreedEnum.Eniripsa:
                    IsHealer = true;
                    MaxPower = 0.8;
                    FavoredBoostSpells = new List<int>() {126}; // Mot stimulant                   
                    break;
                case BreedEnum.Enutrof:
                    break;
                case BreedEnum.Feca:
                    break;
                case BreedEnum.Iop:
                    MaxPower = 1.5;
                    break;
                case BreedEnum.Osamodas:
                    FavoredBoostSpells = new List<int>() {26}; // Bénédiction animale
                    IsInvoker = true;
                    break;
                case BreedEnum.Pandawa:
                    break;
                case BreedEnum.Roublard:
                    break;
                case BreedEnum.Sacrieur:
                    break;
                case BreedEnum.Sadida:
                    break;
                case BreedEnum.Sram:
                    break;
                case BreedEnum.Steamer:
                    break;
                case BreedEnum.Xelor:
                    break;
                case BreedEnum.Zobal:
                    break;
                default:
                    break;
            }
        }

        public override string EntryName
        {
            get { return "FFight"; }
        }

        public double MaxPower { get; set; }

        public bool IsInvoker { get; set; }
        public bool IsHealer { get; set; }
        public List<int> FavoredBoostSpells { get; set; }
        public List<int> FavoredAttackSpells { get; set; }
        public List<int> FavoredCurseSpells { get; set; }
        public List<int> FavoredHealSpells { get; set; }
        public List<int> FavoredInvocSpells { get; set; }
        public int MobKilled { get; set; }
        public int FightStarted { get; set; }
        public int FightWin { get; set; }
        public int FightLost { get; set; }
        public int XPDone { get; set; }
        public int RPMovesSucceded { get; set; }
        public int RPMovesFailed { get; set; }
        public int FMovesSucceded { get; set; }
        public int FMovesFailed { get; set; }
        public int SpellsSucceded { get; set; }
        public int SpellsFailed { get; set; }
        public int UnvalidAckFailed { get; set; }
        public int UnvalidAckSucceeded { get; set; }

        public int Restarts { get; set; }
        public int BotElapsedSeconds { get; set; }
        public int BotFightingElapsedSecond { get; set; }
        public int BotHealingElapsedSeconds { get; set; }
        public DateTime LastFightDate { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
}
