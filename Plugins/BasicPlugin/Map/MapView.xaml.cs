﻿namespace BasicPlugin.Map
{
    using System.Windows.Controls;

    using Strawberry.Common.View;

    /// <summary>
    ///     Interaction logic for MapView.xaml
    /// </summary>
    public partial class MapView : UserControl, IView<MapViewModel>
    {
        #region Fields

        private MapViewModel _viewModel;

        #endregion

        #region Constructors and Destructors

        public MapView()
            => InitializeComponent();

        #endregion

        #region Public Properties

        public MapViewModel ViewModel
        {
            get => _viewModel;
            set
            {
                _viewModel = value;
                DataContext = null;
                DataContext = value;
            }
        }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MapViewModel)value;
        }

        #endregion
    }
}