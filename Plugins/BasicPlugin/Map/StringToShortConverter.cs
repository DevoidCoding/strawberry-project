﻿namespace BasicPlugin.Map
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class StringToShortConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => value?.ToString() ?? string.Empty;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string s && short.TryParse(s, out var n)))
                return 0;

            return n;
        }

        #endregion
    }
}