﻿namespace BasicPlugin.Map
{
    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.World;
    using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;
    using Strawberry.UI;

    internal class MapRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new MapViewModel(bot));

        #endregion
    }

    public class MapViewModel : Frame<MapViewModel>, IViewModel<MapView>
    {
        #region Fields

        private RelayCommand<string> _moveCommand;

        private short _toCell;

        #endregion

        #region Constructors and Destructors

        public MapViewModel(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Properties

        public RelayCommand<string> MoveCommand => _moveCommand ?? (_moveCommand = new RelayCommand<string>(ExecuteMoveCommand));

        public short ToCell
        {
            get => _toCell;
            set
            {
                if (_toCell == value) return;

                _toCell = value;
                RaisePropertyChanged(() => ToCell);
            }
        }

        public MapView View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (MapView)value;
        }

        #endregion

        #region Public Methods and Operators

        public override async void OnAttached()
        {
            base.OnAttached();

            var layout = await Bot.GetViewModel().AddView(this, () => new MapView());
            layout.Header = "Map";
        }

        #endregion

        #region Methods

        private void ExecuteMoveCommand(string direction)
        {
            var mapNeighbour = MapNeighbour.None;

            switch (direction)
            {
                case "top":
                    mapNeighbour = MapNeighbour.Top;
                    break;
                case "bottom":
                    mapNeighbour = MapNeighbour.Bottom;
                    break;
                case "left":
                    mapNeighbour = MapNeighbour.Left;
                    break;
                case "right":
                    mapNeighbour = MapNeighbour.Right;
                    break;
                default:
                    mapNeighbour = MapNeighbour.None;
                    break;
            }

            var pathfinder = new PathFinder(Bot.Character.Map, false);
            var path = pathfinder.FindPath(Bot.Character.Cell.Id, ToCell);
            Bot.Character.Move((ushort)ToCell);
        }

        #endregion
    }
}