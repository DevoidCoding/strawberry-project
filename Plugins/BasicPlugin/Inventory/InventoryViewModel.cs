﻿#region License GNU GPL

// InventoryViewModel.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace BasicPlugin.Inventory
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;

    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Items;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;
    using Strawberry.UI;

    internal class InventoryViewModelRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new InventoryViewModel(bot));

        #endregion
    }

    public class InventoryViewModel : Frame<InventoryViewModel>, IViewModel<InventoryView>
    {
        #region Fields

        private InputNumberDialog m_dialog;

        #endregion

        #region Constructors and Destructors

        public InventoryViewModel(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public InventoryView View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (InventoryView)value;
        }

        #endregion

        #region Public Methods and Operators

        public override async void OnAttached()
        {
            base.OnAttached();

            var viewModel = Bot.GetViewModel();
            var layout = await viewModel.AddView(this, () => new InventoryView());
            layout.Header = "Inventory";
            //layout.CanClose = false;
        }

        public override void OnDetached()
        {
            base.OnDetached();

            if (m_dialog != null && m_dialog.Visibility == Visibility.Visible)
                View.Dispatcher.Invoke(m_dialog.Close);
        }

        #endregion

        #region Methods

        private uint? InputNumber(uint max)
        {
            if (m_dialog != null && m_dialog.Visibility == Visibility.Visible)
                m_dialog.Close();

            if (max == 1)
                return 1;

            m_dialog = new InputNumberDialog { Min = 1, Max = max, Value = max };

            var mouse = View.PointToScreen(Mouse.GetPosition(View));
            m_dialog.Left = mouse.X;
            m_dialog.Top = mouse.Y;

            if (m_dialog.ShowDialog() == true)
                return m_dialog.Value;

            return null;
        }

        private void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        #endregion

        #region EquipItemCommand

        private RelayCommand<Item> m_equipItemCommand;

        public RelayCommand<Item> EquipItemCommand => m_equipItemCommand ?? (m_equipItemCommand = new RelayCommand<Item>(OnEquipItem, CanEquipItem));

        private bool CanEquipItem(Item parameter)
            => parameter is Item && Bot.Character.Inventory.CanEquip(parameter);

        private void OnEquipItem(Item parameter)
        {
            if (parameter == null || !CanEquipItem(parameter))
                return;

            Bot.Character.Inventory.Equip(parameter);
        }

        #endregion

        #region UnEquipCommand

        private RelayCommand<Item> m_unEquipCommand;

        public RelayCommand<Item> UnEquipCommand => m_unEquipCommand ?? (m_unEquipCommand = new RelayCommand<Item>(OnUnEquip, CanUnEquip));

        private bool CanUnEquip(Item parameter)
            => parameter != null && parameter.IsEquipped;

        private void OnUnEquip(Item parameter)
        {
            if (parameter == null || !CanUnEquip(parameter))
                return;

            var item = parameter;

            Bot.Character.Inventory.Move(item, CharacterInventoryPositionEnum.InventoryPositionNotEquiped);
        }

        #endregion

        #region RemoveItemCommand

        private RelayCommand<Item> m_removeItemCommand;

        public RelayCommand<Item> RemoveItemCommand => m_removeItemCommand ?? (m_removeItemCommand = new RelayCommand<Item>(OnRemoveItem, CanRemoveItem));

        private bool CanRemoveItem(Item parameter)
            => parameter != null && Bot.Character.Inventory.CanDelete(parameter);

        private void OnRemoveItem(Item parameter)
        {
            if (parameter == null || !CanRemoveItem(parameter))
                return;

            var item = parameter;

            var quantity = InputNumber(item.Quantity);

            if (quantity != null)
                Bot.Character.Inventory.Delete(item, quantity.Value);
        }

        #endregion

        #region DropItemCommand

        private RelayCommand<Item> m_dropItemCommand;

        public RelayCommand<Item> DropItemCommand => m_dropItemCommand ?? (m_dropItemCommand = new RelayCommand<Item>(OnDropItem, CanDropItem));

        private bool CanDropItem(Item parameter)
            => parameter != null && Bot.Character.Inventory.CanDrop(parameter);

        private void OnDropItem(Item parameter)
        {
            if (parameter == null || !CanDropItem(parameter))
                return;

            var item = parameter;

            var quantity = InputNumber(item.Quantity);

            if (quantity != null)
                Bot.Character.Inventory.Drop(item, quantity.Value);
        }

        #endregion

        #region UseCommand

        private RelayCommand<Item> m_useCommand;

        public RelayCommand<Item> UseCommand => m_useCommand ?? (m_useCommand = new RelayCommand<Item>(OnUse, CanUse));

        private bool CanUse(Item parameter)
            => parameter != null && parameter.IsUsable && Bot.Character.Inventory.CanUse(parameter);

        private void OnUse(Item parameter)
        {
            if (parameter == null || !CanUse(parameter))
                return;

            var item = parameter;

            Bot.Character.Inventory.Use(item);
        }

        #endregion
    }
}