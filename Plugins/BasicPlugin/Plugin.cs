﻿namespace BasicPlugin
{
    using System;

    using Strawberry.Common.Plugins;

    public class Plugin : PluginBase
    {
        #region Constructors and Destructors

        #region Public Constructors

        public Plugin(PluginContext context)
            : base(context)
        {
            if (CurrentPlugin != null)
                throw new Exception("Can be instancied only once");

            CurrentPlugin = this;
        }

        #endregion Public Constructors

        #endregion

        #region Public Properties

        public static Plugin CurrentPlugin { get; private set; }

        public override string Author => "timorem";

        public override string Description => "Manage basics stuff (inventory, server selection ...)";

        public override string Name => "Basic Plugin";

        public override bool UseConfig => true;

        public override Version Version => new Version(1, 0);

        #endregion

        #region Public Methods and Operators

        public override void Dispose()
        {
        }

        #endregion
    }
}