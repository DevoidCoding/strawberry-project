﻿#region License GNU GPL

// FloodEntry.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace BasicPlugin.Chat
{
    using System;
    using System.ComponentModel;

    using Strawberry.Protocol.Enums;

    [Serializable]
    public class FloodEntry : INotifyPropertyChanged
    {
        #region Constructors and Destructors

        public FloodEntry()
        {
            Timer = 10;

            Channels = new[]
                           {
                               new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelGlobal }, new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelTeam },
                               new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelGuild }, new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelAlliance },
                               new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelParty }, new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelSales },
                               new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelSeek }, new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelNoob },
                               new FloodedChannel { Channel = ChatActivableChannelsEnum.ChannelArena }, new FloodedChannel { Channel = ChatActivableChannelsEnum.PseudoChannelPrivate }
                           };
        }

        #endregion

        #region Public Events

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public FloodedChannel[] Channels { get; set; }

        public bool IsEnabled { get; set; }

        public DateTime LastSend { get; set; }

        public bool OnCharacterEnterMap { get; set; }

        public string Text { get; set; }

        public int Timer { get; set; }

        public TimeSpan TimeUntilNextSend => UseTimer ? DateTime.Now - LastSend - TimeSpan.FromSeconds(Timer) : TimeSpan.Zero;

        public bool UseTimer { get; set; }

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    [Serializable]
    public class FloodedChannel : INotifyPropertyChanged
    {
        #region Public Events

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public ChatActivableChannelsEnum Channel { get; set; }

        public bool IsEnabled { get; set; }

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}