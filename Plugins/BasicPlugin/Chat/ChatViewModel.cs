﻿#region License GNU GPL

// ChatViewModel.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace BasicPlugin.Chat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Media;

    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Data.I18N;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Actors.Interfaces;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.Chat;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;
    using Strawberry.UI;

    public class ChatRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new ChatViewModel(bot));

        #endregion
    }

    public class ChatViewModel : Frame<ChatViewModel>, IViewModel<ChatView>
    {
        #region Constants

        public const string MacroCharacter = "##character##";

        public const string MacroCounter = "##counter##";

        #endregion

        #region Static Fields

        private static readonly Dictionary<ChatActivableChannelsEnum, Color> m_colorsRules = new Dictionary<ChatActivableChannelsEnum, Color>
                                                                                                 {
                                                                                                     { ChatActivableChannelsEnum.ChannelGlobal, ColorFromHtmlCode("312D24") },
                                                                                                     { ChatActivableChannelsEnum.ChannelTeam, ColorFromHtmlCode("006699") },
                                                                                                     { ChatActivableChannelsEnum.ChannelGuild, ColorFromHtmlCode("663399") },
                                                                                                     { ChatActivableChannelsEnum.ChannelAlliance, ColorFromHtmlCode("BB6200") },
                                                                                                     { ChatActivableChannelsEnum.ChannelParty, ColorFromHtmlCode("006699") },
                                                                                                     { ChatActivableChannelsEnum.ChannelSales, ColorFromHtmlCode("663300") },
                                                                                                     { ChatActivableChannelsEnum.ChannelSeek, ColorFromHtmlCode("737373") },
                                                                                                     { ChatActivableChannelsEnum.ChannelNoob, ColorFromHtmlCode("0000cc") },
                                                                                                     { ChatActivableChannelsEnum.ChannelAdmin, ColorFromHtmlCode("ff00ff") },
                                                                                                     { ChatActivableChannelsEnum.PseudoChannelPrivate, ColorFromHtmlCode("0066ff") },
                                                                                                     { ChatActivableChannelsEnum.PseudoChannelInfo, ColorFromHtmlCode("009900") },
                                                                                                     { ChatActivableChannelsEnum.PseudoChannelFightLog, ColorFromHtmlCode("009900") },
                                                                                                     { ChatActivableChannelsEnum.ChannelAds, ColorFromHtmlCode("24a394") },
                                                                                                     { ChatActivableChannelsEnum.ChannelArena, ColorFromHtmlCode("f16392") },
                                                                                                     { (ChatActivableChannelsEnum)666, ColorFromHtmlCode("FF0000") }
                                                                                                 };

        #endregion

        #region Fields

        private ChannelNameConverter m_channelNameConverter;

        private int m_counter;

        private ObservableCollection<FloodEntry> m_floodEntries;

        private bool m_isFloodEnabled;

        private readonly string m_wordFrom = I18NDataManager.Instance.ReadText("ui.chat.from");

        private readonly string m_wordTo = I18NDataManager.Instance.ReadText("ui.chat.to");

        #endregion

        #region Constructors and Destructors

        public ChatViewModel(Bot bot)
            : base(bot)
        {
            AvailableChannels = new[]
                                    {
                                        ChatActivableChannelsEnum.ChannelGlobal, ChatActivableChannelsEnum.ChannelTeam, ChatActivableChannelsEnum.ChannelGuild, ChatActivableChannelsEnum.ChannelAlliance,
                                        ChatActivableChannelsEnum.ChannelParty, ChatActivableChannelsEnum.ChannelSales, ChatActivableChannelsEnum.ChannelSeek, ChatActivableChannelsEnum.ChannelNoob, ChatActivableChannelsEnum.ChannelArena,
                                        ChatActivableChannelsEnum.PseudoChannelPrivate
                                    };

            m_floodEntries = new ObservableCollection<FloodEntry>();
            FloodEntries = new ReadOnlyObservableCollection<FloodEntry>(m_floodEntries);

            m_channelNameConverter = new ChannelNameConverter();
        }

        #endregion

        #region Public Properties

        public ChatActivableChannelsEnum[] AvailableChannels { get; set; }

        public ChatActivableChannelsEnum Channel { get; set; }

        public ReadOnlyObservableCollection<FloodEntry> FloodEntries { get; private set; }

        public bool IsFloodEnabled
        {
            get => m_isFloodEnabled;
            set
            {
                m_isFloodEnabled = value;

                if (value)
                    StartFlood();
            }
        }

        public string ReceiverName { get; set; }

        public FloodEntry SelectedEntry { get; set; }

        public string TextToSend { get; set; }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(GameRolePlayShowActorMessage))]
        public void HandleGameRolePlayShowActorMessage(Bot bot, GameRolePlayShowActorMessage message)
        {
            if (!IsFloodEnabled)
                return;

            var character = bot.Character.Context.GetActor(message.Informations.ContextualId) as IPlayer;

            if (character != null)
                foreach (var floodEntry in FloodEntries.Where(x => x.OnCharacterEnterMap))
                    ExecuteEntry(floodEntry, new[] { character });
        }

        public override async void OnAttached()
        {
            base.OnAttached();

            var settings = Bot.Settings.GetOrAddEntry<BasicPluginSettings>();

            m_floodEntries = new ObservableCollection<FloodEntry>(settings.FloodEntries);
            FloodEntries = new ReadOnlyObservableCollection<FloodEntry>(m_floodEntries);

            var viewModel = Bot.GetViewModel();
            var layout = await viewModel.AddView(this, () => new ChatView());
            layout.Header = "Chat";
            View.Dispatcher.Invoke(new Action(() => m_channelNameConverter = View.Resources["ChannelNameConverter"] as ChannelNameConverter));
        }

        public override void OnDetached()
        {
            base.OnDetached();

            var settings = Bot.Settings.GetOrAddEntry<BasicPluginSettings>();

            settings.FloodEntries = FloodEntries.ToArray();

            var viewModel = Bot.GetViewModel();
            //viewModel.RemoveDocument(View);
        }

        #endregion

        #region Methods

        private static Color ColorFromHtmlCode(string htmlColor)
        {
            var colorInt = int.Parse(htmlColor.Replace("#", ""), NumberStyles.HexNumber);

            return Color.FromRgb((byte)((colorInt >> 16) & 255), (byte)((colorInt >> 8) & 255), (byte)(colorInt & 255));
        }

        private void ExecuteEntry(FloodEntry entry, IEnumerable<IPlayer> receivers)
        {
            var text = entry.Text.Replace(MacroCounter, m_counter.ToString());

            foreach (var channel in entry.Channels.Where(x => x.IsEnabled))
            {
                if (channel.Channel != ChatActivableChannelsEnum.PseudoChannelPrivate)
                    Bot.Character.Say(text, channel.Channel);
                else
                    foreach (Character character in receivers)
                        Bot.Character.SayTo(text.Replace(MacroCharacter, character.Name), character.Name);

                m_counter++;
            }

            entry.LastSend = DateTime.Now;
        }

        private void OnTimerEnded(FloodEntry entry)
        {
            if (!IsFloodEnabled)
                return;

            ExecuteEntry(entry, Bot.Character.Context.Actors.OfType<IPlayer>());

            if (IsFloodEnabled) Bot.CallDelayed(entry.Timer * 1000, () => OnTimerEnded(entry));
        }

        private void StartFlood()
        {
            foreach (var floodEntry in FloodEntries)
                if (floodEntry.IsEnabled && floodEntry.UseTimer)
                {
                    var entry = floodEntry;
                    floodEntry.LastSend = DateTime.Now;
                    Bot.CallDelayed(floodEntry.Timer * 1000, () => OnTimerEnded(entry));
                }
        }

        #endregion

        #region SendTextCommand

        private RelayCommand m_sendTextCommand;

        public RelayCommand SendTextCommand => m_sendTextCommand ?? (m_sendTextCommand = new RelayCommand(OnSendText, CanSendText));

        private bool CanSendText()
            => true;

        private void OnSendText()
        {
            if (string.IsNullOrEmpty(TextToSend))
                return;

            if (Channel == ChatActivableChannelsEnum.PseudoChannelPrivate) Bot.Character.SayTo(TextToSend, ReceiverName);
            else Bot.Character.Say(TextToSend, Channel);
        }

        #endregion

        #region StartFloodCommand

        private RelayCommand m_startFloodCommand;

        public RelayCommand StartFloodCommand => m_startFloodCommand ?? (m_startFloodCommand = new RelayCommand(OnStartFlood, CanStartFlood));

        private bool CanStartFlood()
            => !IsFloodEnabled;

        private void OnStartFlood()
            => IsFloodEnabled = true;

        #endregion

        #region StopFloodCommand

        private RelayCommand m_stopFloodCommand;

        public RelayCommand StopFloodCommand => m_stopFloodCommand ?? (m_stopFloodCommand = new RelayCommand(OnStopFlood, CanStopFlood));

        private bool CanStopFlood()
            => IsFloodEnabled;

        private void OnStopFlood()
            => IsFloodEnabled = false;

        #endregion

        #region AddFloodEntryCommand

        private RelayCommand m_addFloodEntryCommand;

        public RelayCommand AddFloodEntryCommand => m_addFloodEntryCommand ?? (m_addFloodEntryCommand = new RelayCommand(OnAddFloodEntry, CanAddFloodEntry));

        private bool CanAddFloodEntry()
            => !IsFloodEnabled;

        private void OnAddFloodEntry()
            => m_floodEntries.Add(new FloodEntry());

        #endregion

        #region RemoveFloodEntryCommand

        private RelayCommand m_removeFloodEntryCommand;

        public RelayCommand RemoveFloodEntryCommand => m_removeFloodEntryCommand ?? (m_removeFloodEntryCommand = new RelayCommand(OnRemoveFloodEntry, CanRemoveFloodEntry));

        private bool CanRemoveFloodEntry()
            => !IsFloodEnabled && SelectedEntry != null;

        private void OnRemoveFloodEntry()
            => m_floodEntries.Remove(SelectedEntry);

        #endregion

        #region IViewModel<ChatView> Members

        object IViewModel.View
        {
            get => View;
            set => View = (ChatView)value;
        }

        public ChatView View { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        #endregion

        #region Chat Handlers

        [MessageHandler(typeof(BotChatMessageServer))]
        public void HandleChatMessageServer(Bot bot, BotChatMessageServer message)
        {
            // server sends a copy from private messages
            if (message.Copy)
                return;

            if (message.Channel == ChatActivableChannelsEnum.PseudoChannelPrivate)
                AppendChatText($"[{DateTime.Now.ToString("hh:mm")}] ({GetChannelName(message.Channel)}) {m_wordFrom} [{message.SenderName}] : {message.Content}\n", GetChannelColor(message.Channel));
            else AppendChatText($"[{message.SentTime.ToString("hh:mm")}] ({GetChannelName(message.Channel)}) [{message.SenderName}] : {message.Content}\n", GetChannelColor(message.Channel));
        }

        [MessageHandler(typeof(BotChatMessageClient))]
        public void HandleChatMessageClient(Bot bot, BotChatMessageClient message)
        {
            if (message.Channel == ChatActivableChannelsEnum.PseudoChannelPrivate)
                AppendChatText($"[{DateTime.Now.ToString("hh:mm")}] ({GetChannelName(message.Channel)}) {m_wordTo} [{message.ReceiverName}] : {message.Content}\n", GetChannelColor(message.Channel));
        }

        [MessageHandler(typeof(TextInformationMessage))]
        public void HandleTextInformationMessage(Bot bot, TextInformationMessage message)
        {
            var data = ObjectDataManager.Instance.Get<InfoMessage>(message.MsgType * 10000 + message.MsgId);
            var text = I18NDataManager.Instance.ReadText(data.TextId);

            // todo : params
            for (var i = 0; i < message.Parameters.Length; i++)
            {
                var parameter = message.Parameters[i];
                text = text.Replace("%" + (i + 1), parameter);
            }

            ChatActivableChannelsEnum channel;
            var type = (TextInformationTypeEnum)message.MsgType;

            if (type == TextInformationTypeEnum.TextInformationError) channel = (ChatActivableChannelsEnum)666;
            else if (type == TextInformationTypeEnum.TextInformationMessage) channel = ChatActivableChannelsEnum.PseudoChannelInfo;
            else if (type == TextInformationTypeEnum.TextInformationPvp) channel = ChatActivableChannelsEnum.ChannelAlliance;
            else if (type == TextInformationTypeEnum.TextInformationFight) channel = ChatActivableChannelsEnum.PseudoChannelFightLog;
            else channel = ChatActivableChannelsEnum.PseudoChannelInfo;

            AppendChatText($"[{DateTime.Now:hh:mm}] {text}\n", GetChannelColor(channel));
        }

        public void AppendChatText(string message, Color color)
            => View?.Dispatcher.BeginInvoke(new Action(() => View.AppendText(message, color)));

        private string GetChannelName(ChatActivableChannelsEnum channel)
            => m_channelNameConverter.GetChannelName(channel);

        private Color GetChannelColor(ChatActivableChannelsEnum channel)
        {
            if (!m_colorsRules.ContainsKey(channel))
                return Colors.Black;

            return m_colorsRules[channel];
        }

        #endregion
    }
}