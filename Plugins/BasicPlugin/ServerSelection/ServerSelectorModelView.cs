﻿#region License GNU GPL

// ServerSelectorModelView.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace BasicPlugin.ServerSelection
{
    using System;
    using System.ComponentModel;

    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Authentification;
    using Strawberry.Common.Frames;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;

    public static class ServerSelectionRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(ServersListMessage))]
        public static void HandleServersListMessage(Bot bot, ServersListMessage message)
            => bot.AddFrame(new ServerSelectorModelView(bot));

        [MessageHandler(typeof(SelectedServerDataMessage))]
        public static void OnSelectedServerDataMessage(Bot bot, SelectedServerDataMessage message)
            => bot.RemoveFrame<ServerSelectorModelView>();

        #endregion
    }

    public class ServerSelectorModelView : Frame<ServerSelectorModelView>, IViewModel<ServerSelectionView>
    {
        #region Fields

        private RelayCommand<ServersListEntry> m_selectServer;

        #endregion

        #region Constructors and Destructors

        public ServerSelectorModelView(Bot bot)
            : base(bot)
        {
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public RelayCommand<ServersListEntry> SelectServerCommand => m_selectServer ?? (m_selectServer = new RelayCommand<ServersListEntry>(OnSelect, CanSelect));

        public ServerSelectionView View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (ServerSelectionView)value;
        }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(SelectedServerDataMessage))]
        public void HandleSelectedServerDataMessage(Bot bot, SelectedServerDataMessage message)
            => Bot.RemoveFrame(this);

        [MessageHandler(typeof(ServersListMessage))]
        public void HandleServersListMessage(Bot bot, ServersListMessage message)
            => View.Dispatcher.BeginInvoke(new Action(SelectServerCommand.RaiseCanExecuteChanged));

        [MessageHandler(typeof(ServerStatusUpdateMessage))]
        public void HandleServerStatusUpdateMessage(Bot bot, ServerStatusUpdateMessage message)
            => View.Dispatcher.BeginInvoke(new Action(SelectServerCommand.RaiseCanExecuteChanged));

        public override async void OnAttached()
        {
            base.OnAttached();

            var viewModel = Bot.GetViewModel();
            var layout = await viewModel.AddView(this, () => new ServerSelectionView());
            layout.Header = "Servers";
            //layout.CanClose = false;
        }

        public override void OnDetached()
        {
            base.OnDetached();

            var viewModel = Bot.GetViewModel();
            //viewModel.RemoveDocument(View);
        }

        #endregion

        #region Methods

        private bool CanSelect(ServersListEntry parameter)
        {
            if (parameter == null)
                return false;

            var server = parameter;

            return server.IsSelectable && server.Status == ServerStatusEnum.Online && (server.Server.RestrictedToLanguages.Count == 0 || server.Server.RestrictedToLanguages.Contains(Bot.ClientInformations.Lang));
        }

        private void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void OnSelect(ServersListEntry parameter)
        {
            if (parameter == null || !CanSelect(parameter))
                return;

            var server = parameter;
            Bot.AddMessage(() => Bot.SendToServer(new ServerSelectionMessage(server.Id)));
        }

        #endregion
    }
}