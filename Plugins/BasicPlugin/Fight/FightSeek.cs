﻿namespace BasicPlugin.Fight
{
    using System.Linq;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Actors;
    using Strawberry.Common.Game.Actors.RolePlay;
    using Strawberry.Common.Game.Fights;
    using Strawberry.Common.Game.World;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    public class FightSeekRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new FightSeek(bot));

        #endregion
    }

    public class FightSeek : Frame<FightSeek>
    {
        #region Constructors and Destructors

        public FightSeek(Bot bot)
            : base(bot)
        {
            void OnCharacterOnMapJoined(PlayedCharacter character, Map map)
                => character.Context.ActorJoined += ContextOnActorJoined;

            bot.Character.MapJoined += OnCharacterOnMapJoined;
            bot.Character.FightJoined += CharacterOnFightJoined;

            foreach (var spellsBookSpell in bot.Character.SpellsBook.Spells) bot.Character.SendMessage($"{spellsBookSpell.Name} - {spellsBookSpell.Template.Id}");
        }

        #endregion

        #region Methods

        private void CharacterOnFightJoined(PlayedCharacter character, Fight fight)
            => character.Context.ActorJoined -= ContextOnActorJoined;

        private void ContextOnActorJoined(ContextActor contextActor)
        {
            if (!(contextActor is GroupMonster groupMonster)) return;

            Seek(groupMonster);
        }

        private void Seek(GroupMonster groupMonster)
        {
            if (groupMonster == null)
                return;

            if (groupMonster.Monsters.Any(e => e.Name.Contains("Sapeur")))
            {
                Bot.Character.TryStartFightWith(groupMonster);
                Bot.Character.SendMessage("Launching fight");
            }
        }

        #endregion
    }
}