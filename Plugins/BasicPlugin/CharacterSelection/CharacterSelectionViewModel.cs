﻿// CharacterSelectionViewModel.cs
//
// Copyright (C) 2012 - BehaviorIsManaged
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

namespace BasicPlugin.CharacterSelection
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Authentification;
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Frames;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Cryptography;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;
    using Strawberry.UI.Helpers;

    public class CharacterSelectionRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharactersListMessage))]
        public static void HandleCharactersListMessage(Bot bot, CharactersListMessage message)
            => bot.AddFrame(new CharacterSelectionViewModel(bot));

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void OnCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.RemoveFrame<CharacterSelectionViewModel>();

        #endregion
    }

    public class CharacterSelectionViewModel : Frame<CharacterSelectionViewModel>, IViewModel<CharacterSelectionView>
    {
        #region Fields

        private CharacterCreationData m_characterCreationData;

        private CharacterCreationDialog m_characterCreationDialog;

        private RelayCommand m_createCharacterCommand;

        private RelayCommand<CharactersListEntry> m_deleteCharacterCommand;

        private DeletionDialog m_deletionDialog;

        private Head[] m_heads;

        private RelayCommand<CharactersListEntry> m_selectCharacterCommand;

        #endregion

        #region Constructors and Destructors

        #region Public Constructors

        public CharacterSelectionViewModel(Bot bot)
            : base(bot)
        {
        }

        #endregion Public Constructors

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public RelayCommand CreateCharacterCommand => m_createCharacterCommand ?? (m_createCharacterCommand = new RelayCommand(OnCreateCharacter, CanCreateCharacter));

        public RelayCommand<CharactersListEntry> DeleteCharacterCommand => m_deleteCharacterCommand ?? (m_deleteCharacterCommand = new RelayCommand<CharactersListEntry>(OnDeleteCharacter, CanDeleteCharacter));

        public RelayCommand<CharactersListEntry> SelectCharacterCommand => m_selectCharacterCommand ?? (m_selectCharacterCommand = new RelayCommand<CharactersListEntry>(OnSelectCharacter, CanSelectCharacter));

        public CharacterSelectionView View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (CharacterSelectionView)value;
        }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterCreationResultMessage))]
        public void HandleCharacterCreationResultMessage(Bot bot, CharacterCreationResultMessage message)
        {
            var result = (CharacterCreationResultEnum)message.Result;

            if (result != CharacterCreationResultEnum.Ok)
                MessageService.ShowError(View, "Cannot create the character : " + result);
            else
                m_characterCreationData = null;
        }

        [MessageHandler(typeof(CharacterDeletionErrorMessage))]
        public void HandleCharacterDeletionErrorMessage(Bot bot, CharacterDeletionErrorMessage message)
            => MessageService.ShowError(View, "Cannot delete the character : " + (CharacterDeletionErrorEnum)message.Reason);

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => Bot.RemoveFrame(this);

        [MessageHandler(typeof(CharactersListMessage))]
        public void HandleCharactersListMessage(Bot bot, CharactersListMessage message)
            => View.Dispatcher.BeginInvoke(
                new Action(
                    () =>
                        {
                            SelectCharacterCommand.RaiseCanExecuteChanged();
                            DeleteCharacterCommand.RaiseCanExecuteChanged();
                        }));

        public override async void OnAttached()
        {
            base.OnAttached();

            var botViewModel = Bot.GetViewModel();
            var layout = await botViewModel.AddView(this, () => new CharacterSelectionView());
            layout.Header = "Characters";
        }

        public override void OnDetached()
        {
            base.OnDetached();

            var viewModel = Bot.GetViewModel();
            //viewModel.RemoveDocument(View);

            if (m_deletionDialog != null && m_deletionDialog.Visibility == Visibility.Visible)
            {
                View.Dispatcher.Invoke(m_deletionDialog.Close);
                m_deletionDialog = null;
            }

            if (m_characterCreationDialog != null && m_characterCreationDialog.Visibility == Visibility.Visible)
            {
                View.Dispatcher.Invoke(m_characterCreationDialog.Close);
                m_characterCreationDialog = null;
            }
        }

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private bool CanCreateCharacter()
            => true;

        private bool CanDeleteCharacter(object parameter)
            => parameter is CharactersListEntry;

        private bool CanSelectCharacter(object parameter)
            => parameter is CharactersListEntry;

        private int ColorToInt(Color color)
            => BitConverter.ToInt32(new byte[] { color.B, color.G, color.R, 0x00 }, 0);

        private void OnCreateCharacter()
        {
            if (m_characterCreationDialog != null && m_characterCreationDialog.Visibility == Visibility.Visible)
                m_characterCreationDialog.Close();

            m_characterCreationDialog = new CharacterCreationDialog();

            if (m_characterCreationData != null) m_characterCreationDialog.Data = m_characterCreationData;
            else
            {
                m_characterCreationDialog.Data.EnabledBreeds = Bot.ClientInformations.AvailableBreeds;
                m_characterCreationData = m_characterCreationDialog.Data;
            }

            if (m_characterCreationDialog.ShowDialog() == true)
            {
                if (m_heads == null)
                    m_heads = ObjectDataManager.Instance.EnumerateObjects<Head>().ToArray();

                var colors = new int[5];
                colors[0] = m_characterCreationData.Color1Used ? ColorToInt(m_characterCreationData.Color1) : -1;
                colors[1] = m_characterCreationData.Color2Used ? ColorToInt(m_characterCreationData.Color2) : -1;
                colors[2] = m_characterCreationData.Color3Used ? ColorToInt(m_characterCreationData.Color3) : -1;
                colors[3] = m_characterCreationData.Color4Used ? ColorToInt(m_characterCreationData.Color4) : -1;
                colors[4] = m_characterCreationData.Color5Used ? ColorToInt(m_characterCreationData.Color5) : -1;

                Bot.SendToServer(
                    new CharacterCreationRequestMessage(
                        m_characterCreationData.CharacterName,
                        (sbyte)m_characterCreationData.Breed,
                        m_characterCreationData.Sex == SexTypeEnum.Female,
                        colors,
                        (ushort)m_heads.First(x => x.Breed == (uint)m_characterCreationData.Breed && x.Gender == (int)m_characterCreationData.Sex).Gender));
            }
        }

        private void OnDeleteCharacter(object parameter)
        {
            if (parameter == null || !CanDeleteCharacter(parameter))
                return;

            if (m_deletionDialog != null && m_deletionDialog.Visibility == Visibility.Visible)
                m_deletionDialog.Close();

            var character = (CharactersListEntry)parameter;

            if (character.Level >= 20)
            {
                m_deletionDialog = new DeletionDialog();
                m_deletionDialog.CharacterName = character.Name;
                m_deletionDialog.SecretQuestion = Bot.ClientInformations.SecretQuestion;

                if (m_deletionDialog.ShowDialog() == true)
                    Bot.SendToServer(new CharacterDeletionRequestMessage(character.Id, Cryptography.GetMD5Hash(character.Id + "~" + m_deletionDialog.SecretAnswer)));
            }
            else if (MessageService.ShowYesNoQuestion(View, $"Are you sure you want to delete {character.Name} ?"))
                Bot.SendToServer(new CharacterDeletionRequestMessage(character.Id, Cryptography.GetMD5Hash(character.Id + "~" + "000000000000000000")));
        }

        private void OnSelectCharacter(CharactersListEntry parameter)
        {
            if (parameter == null || !CanSelectCharacter(parameter))
                return;

            Bot.SendToServer(new CharacterSelectionMessage(parameter.Id));
        }

        #endregion
    }
}