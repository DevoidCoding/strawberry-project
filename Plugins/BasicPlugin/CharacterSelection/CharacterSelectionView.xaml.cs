﻿// CharacterSelectionView.xaml.cs
//
// Copyright (C) 2012 - BehaviorIsManaged
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

namespace BasicPlugin.CharacterSelection
{
    using System.Windows.Controls;

    using Strawberry.Common.View;

    /// <summary>
    ///     Interaction logic for CharacterSelectionView.xaml
    /// </summary>
    public partial class CharacterSelectionView : UserControl, IView<CharacterSelectionViewModel>
    {
        #region Fields

        private CharacterSelectionViewModel m_viewModel;

        #endregion

        #region Constructors and Destructors

        #region Public Constructors

        public CharacterSelectionView()
            => InitializeComponent();

        #endregion Public Constructors

        #endregion

        #region Public Properties

        public CharacterSelectionViewModel ViewModel
        {
            get => m_viewModel;
            set
            {
                m_viewModel = value;
                DataContext = null;
                DataContext = value;
            }
        }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (CharacterSelectionViewModel)value;
        }

        #endregion
    }
}