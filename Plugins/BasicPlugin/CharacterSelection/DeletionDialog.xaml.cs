﻿// DeletionDialog.xaml.cs
//
// Copyright (C) 2012 - BehaviorIsManaged
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

namespace BasicPlugin.CharacterSelection
{
    using System.ComponentModel;
    using System.Windows;

    /// <summary>
    ///     Interaction logic for DeletionDialog.xaml
    /// </summary>
    public partial class DeletionDialog : Window, INotifyPropertyChanged
    {
        #region Constructors and Destructors

        #region Public Constructors

        public DeletionDialog()
            => InitializeComponent();

        #endregion Public Constructors

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public string CharacterName { get; set; }

        public string SecretAnswer { get; set; }

        public string SecretQuestion { get; set; }

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void ButtonOkClicked(object sender, RoutedEventArgs e)
            => DialogResult = true;

        #endregion
    }
}