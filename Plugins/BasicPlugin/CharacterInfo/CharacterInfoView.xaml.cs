﻿namespace BasicPlugin.CharacterInfo
{
    using System.Windows.Controls;

    using Strawberry.Common.View;

    /// <summary>
    ///     Logique d'interaction pour CharacterInfoView.xaml
    /// </summary>
    public partial class CharacterInfoView : UserControl, IView<CharacterInfoViewModel>
    {
        #region Fields

        private CharacterInfoViewModel _viewModel;

        #endregion

        #region Constructors and Destructors

        #region Public Constructors

        public CharacterInfoView()
            => InitializeComponent();

        #endregion Public Constructors

        #endregion

        #region Public Properties

        public CharacterInfoViewModel ViewModel
        {
            get => _viewModel;
            set
            {
                _viewModel = value;
                DataContext = null;
                DataContext = value;
            }
        }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (CharacterInfoViewModel)value;
        }

        #endregion
    }
}