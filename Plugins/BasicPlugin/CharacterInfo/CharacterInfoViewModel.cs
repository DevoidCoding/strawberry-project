﻿namespace BasicPlugin.CharacterInfo
{
    using System.ComponentModel;

    using GalaSoft.MvvmLight.Command;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.Stats;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;
    using Strawberry.UI;

    public class CharacterInfoViewModel : Frame<CharacterInfoViewModel>, IViewModel<CharacterInfoView>
    {
        #region Fields

        private RelayCommand<int> upgradeStatCommand;

        #endregion

        #region Constructors and Destructors

        #region Public Constructors

        public CharacterInfoViewModel(Bot bot)
            : base(bot)
        {
        }

        #endregion Public Constructors

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public RelayCommand<int> UpgradeStatCommand => upgradeStatCommand ?? (upgradeStatCommand = new RelayCommand<int>(OnUpgradeButtonClick));

        public CharacterInfoView View { get; set; }

        public CharacterInfoView ViewModel { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => ViewModel;
            set => ViewModel = (CharacterInfoView)value;
        }

        #endregion

        #region Public Methods and Operators

        public override async void OnAttached()
        {
            base.OnAttached();
            var botViewModel = Bot.GetViewModel();
            var layout = await botViewModel.AddView(this, () => new CharacterInfoView());
            layout.Header = "Character Info";
        }

        public override void OnDetached()
            => base.OnDetached();

        #endregion

        #region Methods

        private void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void OnUpgradeButtonClick(int stat)
            => Bot.Character.SpendStatsPoints((BoostableStat)stat, Bot.Character.GetPointsForBoostAmount((BoostableStat)stat, 1));

        #endregion
    }

    public class CharacterStatsListRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void HandleCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new CharacterInfoViewModel(bot));

        #endregion
    }
}