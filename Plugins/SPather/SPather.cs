﻿namespace SPather
{
    using System;

    using Strawberry.Common.Plugins;

    public class SPather : PluginBase
    {
        #region Constructors and Destructors

        public SPather(PluginContext context)
            : base(context)
        {
        }

        #endregion

        #region Public Properties

        public override string Author => "Strawberry";

        public override string Description => string.Empty;

        public override string Name => "SPather";

        public override bool UseConfig => true;

        public override Version Version => new Version(0, 0, 1);

        #endregion

        #region Public Methods and Operators

        public override void Dispose()
        {
        }

        #endregion
    }
}