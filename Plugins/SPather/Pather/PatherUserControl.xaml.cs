﻿namespace SPather.Pather
{
    using System.Windows.Controls;

    using Strawberry.Common.View;

    /// <summary>
    ///     Interaction logic for PatherUserControl.xaml
    /// </summary>
    public partial class PatherUserControl : UserControl, IView<PatherViewModel>
    {
        #region Constructors and Destructors

        public PatherUserControl()
            => InitializeComponent();

        #endregion

        #region Public Properties

        public PatherViewModel ViewModel { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (PatherViewModel)value;
        }

        #endregion
    }
}