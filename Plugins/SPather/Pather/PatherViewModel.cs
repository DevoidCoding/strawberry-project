﻿namespace SPather.Pather
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Threading.Tasks;

    using JetBrains.Annotations;

    using Strawberry.Common;
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.World.Areas;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    [UsedImplicitly]
    public class PatherRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void OnCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new PatherViewModel(bot));

        #endregion
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class PatherViewModel : Frame<PatherViewModel>, IViewModel<PatherUserControl>
    {
        #region Constructors and Destructors

        public PatherViewModel(Bot bot)
            : base(bot)
            => Task.Factory.StartNew(Initialize);

        #endregion

        #region Public Properties

        public ObservableCollection<SubArea> SubAreas { get; private set; }

        public PatherUserControl View { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IViewModel.View
        {
            get => View;
            set => View = (PatherUserControl)value;
        }

        #endregion

        #region Public Methods and Operators

        public override async void OnAttached()
        {
            base.OnAttached();
            var botViewModel = Bot.GetViewModel();
            var view = await botViewModel.AddView(this, () => new PatherUserControl());
            view.Header = "SPather";
        }

        #endregion

        #region Methods

        private void Initialize()
        {
            SubAreas = new ObservableCollection<SubArea>(ObjectDataManager.Instance.EnumerateObjects<Strawberry.Protocol.Data.SubArea>().Select(e => new SubArea(e.Id)));
        }

        #endregion
    }
}