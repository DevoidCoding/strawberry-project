﻿namespace MyFirstPlugin
{
    using System;

    using Strawberry.Common.Plugins;

    public class MyFirstPlugin : PluginBase
    {
        #region Constructors and Destructors

        public MyFirstPlugin(PluginContext context)
            : base(context)
        {
        }

        #endregion

        #region Public Properties

        public override string Author => "Strawberry";

        public override string Description => "A simple plugin";

        public override string Name => "MyFirstPlugin";

        public override bool UseConfig => false;

        public override Version Version => new Version(0, 0, 1);

        #endregion

        #region Public Methods and Operators

        public override void Dispose()
        {
        }

        #endregion
    }
}