﻿namespace MyFirstPlugin.MyUserInterface
{
    using GalaSoft.MvvmLight;

    public class GeneralViewModel : ViewModelBase
    {
        private string labelContent;

        /// <summary>
        /// Sets and gets the LabelContent property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string LabelContent
        {
            get
            {
                return labelContent;
            }
            set
            {
                Set(() => LabelContent, ref labelContent, value);
            }
        }
    }
}