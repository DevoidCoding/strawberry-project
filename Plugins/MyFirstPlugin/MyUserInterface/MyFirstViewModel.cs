﻿namespace MyFirstPlugin.MyUserInterface
{
    using System.Collections.ObjectModel;

    using Strawberry.Common;
    using Strawberry.Common.Frames;
    using Strawberry.Common.ViewModel;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Messages;

    public class MyUserInterfaceRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void OnCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
            => bot.AddFrame(new MyFirstViewModel(bot));

        #endregion
    }

    public class MyFirstViewModel : Frame<MyFirstViewModel>, IViewModel<MyFirstView>
    {
        #region Fields

        private ObservableCollection<UserControlWithHeader> myGeneralUserControls;

        #endregion

        #region Constructors and Destructors

        public MyFirstViewModel(Bot bot)
            : base(bot)
            => MyGeneralUserControls = new ObservableCollection<UserControlWithHeader> { new UserControlWithHeader("MyFirst", new GeneralViewModel()), new UserControlWithHeader("MySecond", new GeneralViewModel()) };

        #endregion

        #region Public Properties

        /// <summary>
        ///     Sets and gets the MyGeneralUserControls property.
        ///     Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public ObservableCollection<UserControlWithHeader> MyGeneralUserControls
        {
            get => myGeneralUserControls;
            set => Set(() => MyGeneralUserControls, ref myGeneralUserControls, value);
        }

        #endregion

        #region IViewModel Impl

        object IViewModel.View
        {
            get => View;
            set => View = (MyFirstView)value;
        }

        public MyFirstView View { get; set; }

        #endregion
    }

    public class UserControlWithHeader
    {
        #region Constructors and Destructors

        public UserControlWithHeader(string header, GeneralViewModel dataContext)
        {
            Header = header;
            DataContextOfMyUserControl = dataContext;
        }

        #endregion

        #region Public Properties

        public GeneralViewModel DataContextOfMyUserControl { get; set; }

        public string Header { get; set; }

        #endregion
    }
}