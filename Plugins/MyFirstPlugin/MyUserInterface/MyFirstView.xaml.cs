﻿namespace MyFirstPlugin.MyUserInterface
{
    using System.Windows.Controls;

    using Strawberry.Common.View;

    /// <summary>
    ///     Interaction logic for MyFirstView.xaml
    /// </summary>
    public partial class MyFirstView : UserControl, IView<MyFirstViewModel>
    {
        #region Constructors and Destructors

        public MyFirstView()
            => InitializeComponent();

        #endregion

        #region Public Properties

        public MyFirstViewModel ViewModel { get; set; }

        #endregion

        #region Explicit Interface Properties

        object IView.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MyFirstViewModel)value;
        }

        #endregion
    }
}