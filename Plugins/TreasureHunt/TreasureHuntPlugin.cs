﻿namespace TreasureHuntPlugin
{
    using System;

    using global::TreasureHuntPlugin.Services;

    using Strawberry.Common.Plugins;

    public class TreasureHuntPlugin : PluginBase
    {
        #region Constructors and Destructors

        public TreasureHuntPlugin(PluginContext context)
            : base(context)
        {
            if (CurrentPlugin != null)
                throw new Exception("Can be instancied only once");

            CurrentPlugin = this;
        }

        #endregion

        #region Public Properties

        public override string Author { get; } = "Strawberry";

        public TreasureHuntPlugin CurrentPlugin { get; set; }

        public override string Description { get; } = "Treasure hunter plugin";

        public override string Name { get; } = "TreasureHunt";

        public override bool UseConfig { get; } = false;

        public override Version Version { get; } = new Version(0, 1, 1, 0);

        #endregion

        #region Public Methods and Operators

        public override void Dispose()
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            PoiService.Instance.Initiliaze();
        }

        #endregion
    }
}