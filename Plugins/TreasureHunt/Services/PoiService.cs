﻿namespace TreasureHuntPlugin.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Strawberry.Common.Data.Maps;
    using Strawberry.Common.Game.World;
    using Strawberry.Core.Reflection;
    using Strawberry.Protocol.Data;

    public class PoiService : Singleton<PoiService>
    {
        #region Constants

        private const string ResourceName = "TreasureHuntPlugin.Pois.txt";

        #endregion

        #region Fields

        private Dictionary<uint, int> pois = new Dictionary<uint, int>();

        #endregion

        #region Public Methods and Operators

        public async Task<Point> Get(uint id, Map map, string direction)
        {
            var client = new WebClient();
            var json = await client.DownloadStringTaskAsync($"https://dofus-map.com/huntTool/getData.php?x={map.X}&y={map.Y}&direction={direction.ToLower()}&world=0&language=en");
            var jobject = JObject.Parse(json);
            var hints = jobject["hints"].Values<JObject>();
            var hint = hints.FirstOrDefault(e => e["n"].Value<int>() == pois[id]);

            return hint == null ? null : new Point(hint["x"].Value<int>(), hint["y"].Value<int>());
        }

        public void Initiliaze()
        {
            pois.Clear();
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(ResourceName))
            using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
            {
                var serializer = new JsonSerializer();
                pois = serializer.Deserialize<Dictionary<uint, int>>(new JsonTextReader(reader));
            }
        }

        #endregion
    }
}