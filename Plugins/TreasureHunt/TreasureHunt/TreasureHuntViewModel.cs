﻿namespace TreasureHuntPlugin.Hunt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using global::TreasureHuntPlugin.Services;

    using Strawberry.Common;
    using Strawberry.Common.Data.D2O;
    using Strawberry.Common.Frames;
    using Strawberry.Common.Game.TreasureHunt;
    using Strawberry.Common.Game.TreasureHunt.Steps;
    using Strawberry.Common.Game.World;
    using Strawberry.Common.Game.World.Pathfinding.FFPathFinding;
    using Strawberry.Core.Messages;
    using Strawberry.Protocol.Data;
    using Strawberry.Protocol.Enums;
    using Strawberry.Protocol.Messages;

    using Npc = Strawberry.Common.Game.Actors.RolePlay.Npc;
    using WorldMap = Strawberry.Protocol.Data.WorldMap;

    public class TreasureHuntRegister
    {
        #region Public Methods and Operators

        [MessageHandler(typeof(CharacterSelectedSuccessMessage))]
        public static void OnCharacterSelectedSuccessMessage(Bot bot, CharacterSelectedSuccessMessage message)
        {
            bot.Character.TreasureHunt.Started += TreasureHuntOnStarted;
            bot.Character.TreasureHunt.Finished += TreasureHuntOnFinished;
        }

        #endregion

        #region Methods

        private static void TreasureHuntOnFinished(TreasureHunt hunt, Bot bot)
        {
            if (bot.HasFrame<TreasureHuntViewModel>())
                bot.RemoveFrame<TreasureHuntViewModel>();
        }

        private static void TreasureHuntOnStarted(TreasureHunt hunt, Bot bot)
            => bot.AddFrame(new TreasureHuntViewModel(bot));

        #endregion
    }

    // ReSharper disable once StyleCop.SA1402
    public class TreasureHuntViewModel : Frame<TreasureHuntViewModel>
    {
        #region Fields

        private readonly Bot bot;

        private double nextMapId;

        private Point poiPosition;

        #endregion

        #region Constructors and Destructors

        public TreasureHuntViewModel(Bot bot)
            : base(bot)
        {
            this.bot = bot;
            bot.Character.SendMessage("Treasure hunt started. Waiting for map");
            bot.Character.TreasureHunt.StartHunting += TreasureHuntOnStartHunting;
            bot.Character.WorldPathFinder.PathEnded += OnWorldPathEnd;
        }

        #endregion

        #region Public Methods and Operators

        public Point GetNextMapFromDirection(DirectionsEnum direction)
        {
            var position = new Point(bot.Character.Map.X, bot.Character.Map.Y);

            switch (direction)
            {
                case DirectionsEnum.DirectionEast:
                    return new Point(position.X + 1, position.Y);
                case DirectionsEnum.DirectionSouth:
                    return new Point(position.X, position.Y + 1);
                case DirectionsEnum.DirectionWest:
                    return new Point(position.X - 1, position.Y);
                case DirectionsEnum.DirectionNorth:
                    return new Point(position.X, position.Y - 1);
                default:
                    return null;
            }
        }

        [MessageHandler(typeof(ChatClientMultiMessage))]
        public void OnChatClientMultiMessage(Bot bot, ChatClientMultiMessage message)
        {
            if (!message.Content.StartsWith(".th")) return;

            if (bot.Character.Map.Id != bot.Character.TreasureHunt.StartMap.Id && !bot.Character.TreasureHunt.IsHunting)
            {
                bot.Character.SendMessage("Going to the start map");
                bot.Character.WorldPathFinder.Move(bot.Character.TreasureHunt.StartMap.Id);
            }
            else
            {
                bot.Character.SendMessage("Starting hunt !");
                TreasureHuntOnUpdated(bot.Character.TreasureHunt, bot);
            }

            message.BlockProgression();
        }

        public override void OnDetached()
        {
            base.OnDetached();

            bot.Character.TreasureHunt.Updated -= TreasureHuntOnUpdated;
            bot.Character.TreasureHunt.StartHunting -= TreasureHuntOnStartHunting;
            bot.Character.WorldPathFinder.PathEnded -= OnWorldPathEnd;
        }

        #endregion

        #region Methods

        private MapNeighbour DirectionToMapNeighbour(DirectionsEnum direction)
        {
            switch (direction)
            {
                case DirectionsEnum.DirectionEast:
                    return MapNeighbour.Right;
                case DirectionsEnum.DirectionSouth:
                    return MapNeighbour.Bottom;
                case DirectionsEnum.DirectionWest:
                    return MapNeighbour.Left;
                case DirectionsEnum.DirectionNorth:
                    return MapNeighbour.Top;
            }

            return MapNeighbour.Any;
        }

        private double GetMapIdFromCoords(int x, int y)
        {
            var getCompressedValue = new Func<int, uint>(v => v < 0 ? (uint)(32768 | (v & 32767)) : (uint)(v & 32767));
            var xCompressed = getCompressedValue(x);
            var yCompressed = getCompressedValue(y);

            var mapCoordinates = ObjectDataManager.Instance.Get<MapCoordinates>((xCompressed << 16) + yCompressed);
            var maps = mapCoordinates.MapIds.Select(e => ObjectDataManager.Instance.Get<MapPosition>((uint)e));
            var orderedMap = new List<(int order, double mapId)>();

            foreach (var mapPosition in maps)
            {
                var order = 0;
                var subArea = ObjectDataManager.Instance.Get<SubArea>(mapPosition.SubAreaId);
                var area = ObjectDataManager.Instance.Get<Area>(subArea.AreaId);
                var superArea = ObjectDataManager.Instance.Get<SuperArea>(area.SuperAreaId);
                var worldMap = ObjectDataManager.Instance.Get<WorldMap>(subArea.CustomWorldMap.Count > 0 ? subArea.CustomWorldMap[0] : area.HasWorldMap ? area.WorldmapId : superArea.WorldmapId);
                var worldId = new WorldPoint((int)mapPosition.Id).WorldId;
                if (mapPosition.WorldMap == bot.Character.Map.WorldMap) order = order + 100000;

                if (mapPosition.HasPriorityOnWorldmap) order = order + 10000;

                if (mapPosition.Outdoor == bot.Character.Map.Outdoor) order++;

                if (subArea.Id == bot.Character.Map.SubAreaId) order = order + 100;

                if (area.Id == bot.Character.Map.SubArea.AreaId) order = order + 50;

                if (superArea.Id == bot.Character.Map.SubArea.Area.SuperAreaId) order = order + 25;

                if (worldId == bot.Character.Map.WorldMap) order = order + 100;

                orderedMap.Add((order, mapPosition.Id));
            }

            return orderedMap.OrderByDescending(e => e.order).First().mapId;
        }

        private void OnWorldPathEnd()
        {
            if (!bot.Character.TreasureHunt.IsHunting || nextMapId == 0)
                    return;

            if (bot.Character.TreasureHunt.ActualStep is PointOfInterestStep poiStep)
            {
                if (nextMapId != bot.Character.Map.Id)
                {
                    bot.Character.SendMessage("Bot is lost...");
                    return;
                }

                bot.Character.TreasureHunt.Flag();
            }
            else if (bot.Character.TreasureHunt.ActualStep is HintStep hintStep)
            {
                if (bot.Character.Map.Actors.OfType<Npc>().Any(e => e.Id == hintStep.NpcId))
                {
                    bot.Character.TreasureHunt.Flag();
                }
                else
                {
                    var position = GetNextMapFromDirection(hintStep.Direction);
                    if (position == null) return;
                    nextMapId = GetMapIdFromCoords(position.X, position.Y);
                    bot.Character.WorldPathFinder.Move((int)nextMapId);
                }
            }
        }

        private void TreasureHuntOnStartHunting(TreasureHunt hunt, Bot bot)
        {
            if (nextMapId == 0) TreasureHuntOnUpdated(bot.Character.TreasureHunt, bot);
            hunt.Updated += TreasureHuntOnUpdated;
        }

        private async void TreasureHuntOnUpdated(TreasureHunt hunt, Bot bot)
        {
            try
            {
                if (bot.Character.IsFighting()) return;
                if (bot.Character?.Map == null) return;

                if (hunt.Flags.Count() == hunt.TotalStep || hunt.ActualStep is FightStep)
                {
                    hunt.Dig();
                    return;
                }

                switch (hunt.ActualStep)
                {
                    case PointOfInterestStep poiStep:
                        poiPosition = await PoiService.Instance.Get(poiStep.PointOfInterest.Id, bot.Character.Map, DirectionToMapNeighbour(poiStep.Direction).ToString());
                        if (poiPosition == null)
                            bot.Character.SendError("POI is null, WTF !");
                        // TODO : Do something is position is null
                        nextMapId = GetMapIdFromCoords(poiPosition.X, poiPosition.Y);
                        break;
                    case HintStep hintStep:
                        var position = GetNextMapFromDirection(hintStep.Direction);
                        if (position == null) return;
                        nextMapId = GetMapIdFromCoords(position.X, position.Y);
                        break;
                }

                if (hunt.IsHunting)
                    bot.Character.WorldPathFinder.Move((int)nextMapId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion
    }
}