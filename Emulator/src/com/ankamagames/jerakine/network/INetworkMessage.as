package com.ankamagames.jerakine.network
{
   public interface INetworkMessage
   {
       
      function pack(param1:ICustomDataOutput) : void;
      
      function unpack(param1:ICustomDataInput, param2:uint) : void;
      
      function get isInitialized() : Boolean;
   }
}
