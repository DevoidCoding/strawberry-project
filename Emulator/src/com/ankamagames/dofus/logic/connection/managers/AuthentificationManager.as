package com.ankamagames.dofus.logic.connection.managers
{
	public class AuthentificationManager extends Object
	{
		private static var _self:AuthentificationManager;
		
		private var _gameServerTicket:String;
		
		public function AuthentificationManager()
		{
		}
		
		
		public static function getInstance():AuthentificationManager
		{
			if (!_self) 
			{
				_self = new AuthentificationManager();
			}
			return _self;
		}

		public function get gameServerTicket() : String
		{
			return this._gameServerTicket;
		}
		
		public function set gameServerTicket(param1:String) : void
		{
			this._gameServerTicket = param1;
		}
	}
}