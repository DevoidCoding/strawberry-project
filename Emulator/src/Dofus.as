package    
{
	import com.ankamagames.dofus.logic.connection.managers.AuthentificationManager;
	import com.ankamagames.jerakine.network.NetworkMessage;
	import com.hurlant.crypto.hash.MD5;
	import com.hurlant.crypto.symmetric.AESKey;
	import com.hurlant.crypto.symmetric.CBCMode;
	import com.hurlant.crypto.symmetric.IPad;
	import com.hurlant.crypto.symmetric.IVMode;
	import com.hurlant.crypto.symmetric.PKCS5;
	import com.hurlant.crypto.symmetric.SimpleIVMode;
	import com.hurlant.util.der.ByteString;
	
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.drm.AuthenticationMethod;
import flash.system.ApplicationDomain;
import flash.utils.ByteArray;
	
	import by.blooddy.crypto.Base64;

	public class Dofus extends Sprite
	{
		private static var _self:Dofus;
		private var _dofusBytes:ByteArray = new ByteArray();
		private var _key2:ByteArray;
		private var _publicModulo:ByteArray;
        private var _sigOk:String;
        private var _err:Error;

        public function get err():Error {
            return _err;
        }

        public function set err(value:Error):void {
            _err = value;
        }

        public function Dofus()
		{
			super();
			// DofusInvoker must be uncompress
			var dofusFile:File = File.applicationDirectory.resolvePath("DofusInvoker.swf");
			var sr:FileStream = new FileStream();
			sr.open(dofusFile, FileMode.READ);
			while(sr.bytesAvailable)
				_dofusBytes.writeByte(sr.readByte());
		}

        public function get key2():ByteArray {
            return _key2;
        }

        // Used by HumanCheck
        public function set key2(value:ByteArray):void {
            	_key2 = value;
        }

        public function get publicModulo():ByteArray {
            return _publicModulo;
        }

        // Used by HumanCheck
        public function set publicModulo(value:ByteArray):void {
            	_publicModulo = value;
        }

        public static function getInstance() : Dofus
		{
			if(!_self)
			{
				_self = new Dofus();
			}
			return _self;
		}

        // Used by HumanCheck
        public function get dofusBytes():ByteArray
        {
            _dofusBytes.position = 0;
            return _dofusBytes;
        }

        // Used by HumanCheck
        public function get bytes():ByteArray
        {
            _dofusBytes.position = 0;
            return _dofusBytes;
        }

        public function get sigOk():String {
            return _sigOk;
        }

        // Used by HumanCheck
        public function set sigOk(value:String):void {
            _sigOk = value;

            // Should be the end of HumanCheck
            RawDataSocket.getInstance().respond();
        }
    }
}