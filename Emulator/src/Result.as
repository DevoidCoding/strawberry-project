package     
{
	public class Result extends Object
	{
		private static var _self:Result;
		private var _test:String = "base";
		
		public static function getInstance() : Result
		{
			if(!_self)
			{
				_self = new Result();
			}
			return _self;
		}
		
		public function getTest() : String
		{
			return _test;
		}
		
		public function setTest(str:String) : void
		{
			_test = str;
		}
		
		public function Result()
		{
		}
		
		public function traceTest() : void
		{
			trace(getTest());
		}
	}
}