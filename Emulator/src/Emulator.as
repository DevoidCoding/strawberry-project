package
{
import com.ankamagames.dofus.kernel.net.ConnectionsHandler;
import com.ankamagames.dofus.logic.connection.managers.AuthentificationManager;
import com.ankamagames.dofus.network.messages.security.CheckIntegrityMessage;
import com.ankamagames.jerakine.utils.display.StageShareManager;

import flash.desktop.NativeApplication;
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.errors.IllegalOperationError;
import flash.events.Event;
import flash.events.InvokeEvent;

public class Emulator extends Sprite
	{
		private static var _loader:ClassLoader;
        public static var _rawData:String;
		
		public function Emulator() 
		{
			//new AppIdModifier();
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvokeEvent);
            initDofus();
            /*var dec:Function = function (_arg_1:String, _arg_2:String):String
            {
                var _local_3:ByteArray = Base64.decodeToByteArray(_arg_1);
                var _local_4:ByteArray = Base64.decodeToByteArray(_arg_2);
                var _local_5:uint = 0;
                while (_local_5 < _local_3.length)
                {
                    _local_3[_local_5] = (_local_3[_local_5] ^ _local_4[(_local_5 % _local_4["length"])]);
                    _local_5++;
                };
                trace("dec: " + _local_3.toString());
                return (_local_3.toString());
            };

            var publicModulo:ByteArray = Base64.decodeToByteArray(dec("UiRxlpr6gsL9yUJRU8CQgN7s9IdbMX7aguSu0fv5WlsbnbiFhfSYw38OAfqkkrD41p4iHnzoopeM2vbHfw5ThZjkn9rgz2EZSsWQpYTM/dtKBkrUgLGWzeHuWlkJ26CyhNSc3nkDAMLlsIil9ct6IF7M5ZqT+/bFPAxBnf+tk8zamGIQec+evK3EgdxbK1n8o6GA/9vZdyR8mf3hgNvAwVEvcczlh67AgZVqAX/qhIaAzcrtYQtB+7qAn9LZ20Ysep+tpa3t15olLFWY/6ekp/ngJANf5o3+k6eF1lUPCO39r63BxOciBwKZs7GI5OrcSw4Ay47tqtby1icGf/+Qv5ze6/13KlnsqZ+MpOfBal9RyZqhoPOEwyIMAv+IrYf80NpBOwnUvb6ewtz5Wx9d2/z+pPbA+CQpe5erhqj2x/VjOkmcm+Gezf3GURoJ26ujg+XHmlUuZZM=", "E2gwrsrV5pWzrA=="));
            TraceByteArray(publicModulo);
            var bi:BigInteger = new BigInteger(publicModulo);
            trace("Bi {" + bi.bitLength() + "} = " + bi);
            var rsaKeyNetwork:RSAKey = new RSAKey(bi, parseInt(dec("fyeeDRs=", "SRKrPiw=")));
            var rsaCryptedData:ByteArray = new ByteArray();
            var t:Array =  [77, -68, -76, 17, -31, -37, 118, 50, 113, 123, 50, 37, 58, -32, 76, -74, 65, -43, -43, -76];
            var ba = new ByteArray();
            t.forEach(function(element:*, index:int, arr:Array):void {
                ba.writeByte(element);
            });
            ba.position = 0;
            TraceByteArray(ba);
            rsaKeyNetwork.encrypt(ba, rsaCryptedData, ba.length);
            TraceByteArray(rsaCryptedData);
            TraceByteArray(ba);
            rsaCryptedData.position = 0;
            ba.position = 0;
            rsaKeyNetwork = new RSAKey(bi, parseInt(dec("fyeeDRs=", "SRKrPiw=")));
            rsaKeyNetwork.verify(ba, rsaCryptedData, ba.length);
            TraceByteArray(rsaCryptedData);*/
		}

        public static function onInvokeEvent(invocation:InvokeEvent):void {
            _rawData = invocation.arguments[0];
            loadSwf();
        }

		private function initDofus() : void
		{
			addChild(Dofus.getInstance());
			StageShareManager.stage = stage;
			AuthentificationManager.getInstance().gameServerTicket = "";
			ConnectionsHandler.getConnection();
			var cimsg = new CheckIntegrityMessage();
		}
		
		private static function loadSwf():void
		{
			_loader = new ClassLoader();
			_loader.addEventListener(ClassLoader.LOAD_ERROR,loadErrorHandler);
			_loader.addEventListener(ClassLoader.CLASS_LOADED,classLoadedHandler);
			trace(_rawData + ".swf");
			_loader.load(_rawData + ".swf");
			
		}
		
		private static function loadErrorHandler(e:Event):void {
			throw new IllegalOperationError("Cannot load the specified file.");
		}
		
		private static function classLoadedHandler(e:Event):void {
			RawDataSocket.getInstance().humanCheckClass = _loader.getClass("HumanCheck");
		}
	}
}