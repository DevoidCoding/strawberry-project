package     
{
	public class Test extends Object
	{
		private static var _self:Test;
		private var _test:String = "base";
		
		public static function getInstance() : Test
		{
			if(!_self)
			{
				_self = new Test();
			}
			return _self;
		}
		
		public function getTest() : String
		{
			return _test;
		}
		
		public function setTest(str:String) : void
		{
			_test = str;
		}
		
		public function Test()
		{
		}
		
		public function traceTest() : void
		{
			trace(getTest());
		}
	}
}