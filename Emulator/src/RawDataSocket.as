package
{
import by.blooddy.crypto.serialization.JSON;
import com.worlize.websocket.WebSocket;

import flash.desktop.NativeApplication;


import flash.utils.ByteArray;
import flash.utils.setTimeout;

public class RawDataSocket
	{
		private static var _self:RawDataSocket;
		private var _socket:WebSocket;
		private var _checkIntegrityData:Vector.<int>;
		private var _hashKey:ByteArray;
		private var _ticket:String;
		private var _emulator:Emulator;
		private var _humanCheckClass:Class;
		private var _firstSend:Boolean;
		
		public function RawDataSocket()
		{
			_firstSend = true;
			initSocket();
		}

		public function get humanCheckClass():Class
		{
			return _humanCheckClass;
		}

		public function set humanCheckClass(value:Class):void
		{
			_humanCheckClass = value;
		}

		public function get hashKey():ByteArray
		{
			return _hashKey;
		}

		public function set hashKey(value:ByteArray):void
		{
			_hashKey = value;
		}

		public function get checkIntegrityData():Vector.<int>
		{
			return _checkIntegrityData;
		}

		public function set checkIntegrityData(value:Vector.<int>):void
		{
			_checkIntegrityData = value;
		}
		
		public static function getInstance() : RawDataSocket
		{
			if (!_self) _self = new RawDataSocket();
			return _self;
		}

		private function initSocket() : void
		{
			_socket = new WebSocket("ws://localhost:3001/", "*");
            _socket.connect();
		}

		public function respond() : void
		{
            if (!_socket.connected) {
                setTimeout(respond, 500);
            } else {
                sendResponse();
            }
		}

        private function sendResponse() : void
        {
            var key:Array = byteArrayToArray(Dofus.getInstance().key2);
            var modulo:Array = byteArrayToArray(Dofus.getInstance().publicModulo);
            var sigOk:Boolean = Dofus.getInstance().sigOk == "true";
            var _messageType = "RawDataInformationsMessage";
			var md5 = Emulator._rawData;
            _socket.sendUTF(by.blooddy.crypto.serialization.JSON.encode({_messageType: _messageType, key: key, modulo: modulo, sigOk: sigOk, md5: md5}));
            _socket.close();
            NativeApplication.nativeApplication.exit();
        }
		
		private function toArray(iterable:*):Array {
			var ret:Array = [];
			for each (var elem in iterable) ret.push(elem);
			return ret;
		}
		
		private function byteArrayToArray(iterable:ByteArray):Array {
			var ret:Array = [];
			iterable.position = 0;
			while (iterable.bytesAvailable) ret.push(iterable.readByte());
			return ret;
		}
	}
}