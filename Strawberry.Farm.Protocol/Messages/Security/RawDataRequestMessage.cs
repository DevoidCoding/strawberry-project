﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    using Strawberry.Core.Messages;

    public class RawDataRequestMessage : JsonMessage
    {
        #region Public Properties

        [JsonProperty(nameof(GameServerTicket))]
        public string GameServerTicket { get; set; }

        [JsonProperty(nameof(MD5))]
        // ReSharper disable once InconsistentNaming
        public string MD5 { get; set; }

        #endregion
    }
}