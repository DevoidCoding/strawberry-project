﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    using Strawberry.Core.Messages;

    public class RawDataInformationsMessage : JsonMessage
    {
        #region Public Properties

        [JsonProperty("key")]
        public sbyte[] Key { get; set; }

        [JsonProperty("md5")]
        // ReSharper disable once InconsistentNaming
        public string MD5 { get; set; }

        [JsonProperty("modulo")]
        public sbyte[] Modulo { get; set; }

        [JsonProperty("sigOk")]
        public bool SigOk { get; set; }

        #endregion
    }
}