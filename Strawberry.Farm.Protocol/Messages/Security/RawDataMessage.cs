﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    using Strawberry.Core.Messages;
    using Strawberry.Farm.Protocol.Types;

    public class RawDataMessage : JsonMessage
    {
        #region Public Properties

        [JsonProperty(nameof(Assembly))]
        public Assembly Assembly { get; set; }

        #endregion
    }
}