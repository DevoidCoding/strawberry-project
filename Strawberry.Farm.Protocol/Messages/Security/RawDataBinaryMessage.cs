﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    using Strawberry.Farm.Protocol.Types;

    public class RawDataBinaryMessage : RawDataRequestMessage
    {
        #region Public Properties

        [JsonProperty(nameof(Assembly))]
        public Assembly Assembly { get; set; }

        #endregion
    }
}