﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    public class RawDataRespondMessage : RawDataBinaryRequestMessage
    {
        #region Public Properties

        [JsonProperty(nameof(CheckIntegrityContent))]
        public byte[] CheckIntegrityContent { get; set; }

        [JsonProperty(nameof(HashKey))]
        public byte[] HashKey { get; set; }

        #endregion
    }
}