﻿namespace Strawberry.Farm.Protocol.Messages.Security
{
    using Newtonsoft.Json;

    using Strawberry.Core.Messages;

    public class RawDataBinaryRequestMessage : JsonMessage
    {
        #region Public Properties

        [JsonProperty(nameof(GameServerTicket))]
        public string GameServerTicket { get; set; }

        #endregion
    }
}