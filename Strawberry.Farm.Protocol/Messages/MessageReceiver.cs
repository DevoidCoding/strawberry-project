﻿namespace Strawberry.Farm.Protocol.Messages
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    using Strawberry.Core.Messages;

    public class MessageReceiver
    {
        #region Fields

        // private readonly JSchemaGenerator generator = new JSchemaGenerator(); // { ContractResolver = new CamelCasePropertyNamesContractResolver() };

        private readonly Dictionary<string, Type> messages = new Dictionary<string, Type>();

        #endregion

        #region Public Methods and Operators

        public JsonMessage BuildMessage(string json)
        {
            try
            {
                var reader = new JsonTextReader(new StringReader(json));

                // var validatingReader = new JSchemaValidatingReader(reader) { Schema = generator.Generate(typeof(JsonMessage)) };

                var serializer = new JsonSerializer();
                var message = serializer.Deserialize<JsonMessage>(reader);

                if (!messages.ContainsKey(message.MessageType))
                    throw new MessageNotFoundException($"NetworkMessage <id:{message.MessageType}> doesn't exist");

                var messageType = messages[message.MessageType];

                if (messageType == null)
                    throw new MessageNotFoundException($"Constructors[{message.MessageType}] (delegate {messages[message.MessageType]}) does not exist");

                reader = new JsonTextReader(new StringReader(json));
                // validatingReader = new JSchemaValidatingReader(reader) { Schema = generator.Generate(messageType) };

                serializer = new JsonSerializer();

                return (JsonMessage)serializer.Deserialize(reader, messageType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public void Initialize()
        {
            if (messages.Any()) return;

            var asm = Assembly.GetAssembly(typeof(MessageReceiver));

            foreach (var type in asm.GetTypes())
            {
                if (!type.IsSubclassOf(typeof(JsonMessage))) continue;

                var id = type.Name;

                if (messages.ContainsKey(id))
                    throw new AmbiguousMatchException($"MessageReceiver() => {id} item is already in the dictionary, old type is : {messages[id]}, new type is  {type}");

                messages.Add(id, type);
            }
        }

        #endregion

        #region Nested type: MessageNotFoundException

        [Serializable]
        public class MessageNotFoundException : Exception
        {
            #region Constructors and Destructors

            public MessageNotFoundException()
            {
            }

            public MessageNotFoundException(string message)
                : base(message)
            {
            }

            public MessageNotFoundException(string message, Exception inner)
                : base(message, inner)
            {
            }

            protected MessageNotFoundException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }

            #endregion
        }

        #endregion
    }
}