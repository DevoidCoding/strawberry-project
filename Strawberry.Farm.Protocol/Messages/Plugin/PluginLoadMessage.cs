﻿namespace Strawberry.Farm.Protocol.Messages.Plugin
{
    using Newtonsoft.Json;

    using Strawberry.Farm.Protocol.Messages.Security;

    public class PluginLoadMessage : RawDataMessage
    {
        #region Public Properties

        [JsonProperty(nameof(HasServerHandler))]
        public bool HasServerHandler { get; set; }

        #endregion
    }
}