﻿namespace Strawberry.Farm.Protocol.Types
{
    using Newtonsoft.Json;

    public class Assembly
    {
        #region Constructors and Destructors

        public Assembly()
        {
        }

        public Assembly(string filename, byte[] content)
        {
            Filename = filename;
            Content = content;
        }

        #endregion

        #region Public Properties

        [JsonProperty(nameof(Assembly))]
        public byte[] Content { get; set; }

        [JsonProperty(nameof(Filename))]
        public string Filename { get; set; }

        #endregion
    }
}