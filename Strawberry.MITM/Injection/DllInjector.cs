﻿namespace Strawberry.MITM.Injection
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    public sealed class DllInjector
    {
        #region Static Fields

        private static readonly IntPtr IntptrZero = (IntPtr)0;

        private static DllInjector _instance;

        #endregion

        #region Public Properties

        public static DllInjector GetInstance => _instance ?? (_instance = new DllInjector());

        #endregion

        #region Public Methods and Operators

        public DllInjectionResult Inject(string sProcName, string sDllPath)
        {
            if (!File.Exists(sDllPath)) return DllInjectionResult.DllNotFound;

            uint _procId = 0;

            var _procs = Process.GetProcesses();

            for (var i = 0; i < _procs.Length; i++)
                if (_procs[i].ProcessName == sProcName)
                {
                    _procId = (uint)_procs[i].Id;
                    break;
                }

            if (_procId == 0) return DllInjectionResult.GameProcessNotFound;

            if (!bInject(_procId, sDllPath)) return DllInjectionResult.InjectionFailed;
            Console.WriteLine("Success !");
            return DllInjectionResult.Success;
        }

        public DllInjectionResult Inject(uint sProcId, string sDllPath)
        {
            if (!File.Exists(sDllPath)) return DllInjectionResult.DllNotFound;

            if (sProcId == 0) return DllInjectionResult.GameProcessNotFound;

            if (!bInject(sProcId, sDllPath)) return DllInjectionResult.InjectionFailed;

            return DllInjectionResult.Success;
        }

        #endregion

        #region Methods

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttribute, IntPtr dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr OpenProcess(uint dwDesiredAccess, int bInheritHandle, uint dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, IntPtr dwSize, uint flAllocationType, uint flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] buffer, uint size, int lpNumberOfBytesWritten);

        private bool bInject(uint pToBeInjected, string sDllPath)
        {
            var hndProc = OpenProcess(0x2 | 0x8 | 16 | 0x20 | 0x400, 1, pToBeInjected);

            if (hndProc == IntptrZero) return false;

            var lpLLAddress = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");

            if (lpLLAddress == IntptrZero) return false;

            var lpAddress = VirtualAllocEx(hndProc, (IntPtr)null, (IntPtr)sDllPath.Length, 0x1000 | 0x2000, 0X40);

            if (lpAddress == IntptrZero) return false;

            var bytes = Encoding.ASCII.GetBytes(sDllPath);

            if (WriteProcessMemory(hndProc, lpAddress, bytes, (uint)bytes.Length, 0) == 0) return false;

            if (CreateRemoteThread(hndProc, (IntPtr)null, IntptrZero, lpLLAddress, lpAddress, 0, (IntPtr)null) == IntptrZero) return false;

            CloseHandle(hndProc);

            return true;
        }

        #endregion
    }
}