﻿namespace Strawberry.MITM.Injection
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using Strawberry.Core.Threading;

    public class Injector : SelfRunningTaskQueue
    {
        #region Static Fields

        private static Injector _instance;

        #endregion

        #region Fields

        private readonly List<int> _dofusInstances = new List<int>();

        #endregion

        #region Constructors and Destructors

        public Injector(int updateInterval)
            : base(updateInterval)
        {
        }

        #endregion

        #region Public Properties

        public static Injector Instance => _instance ?? (_instance = new Injector(500));

        #endregion

        #region Methods

        protected override void OnTick()
            => DofusInject();

        private void DofusInject()
        {
            var processes = Process.GetProcessesByName("Dofus").Where(e => !_dofusInstances.Contains(e.Id)).ToList();
            var dllInjector = new DllInjector();

            foreach (var prc in processes)
            {
                if (_dofusInstances.Contains(prc.Id)) continue;
                var result = dllInjector.Inject((uint)prc.Id, Path.Combine(Environment.CurrentDirectory, @"No.Ankama.dll"));
                _dofusInstances.Add(prc.Id);
            }
        }

        #endregion
    }
}