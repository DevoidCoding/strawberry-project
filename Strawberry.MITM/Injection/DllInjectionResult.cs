﻿namespace Strawberry.MITM.Injection
{
    public enum DllInjectionResult
    {
        DllNotFound,

        GameProcessNotFound,

        InjectionFailed,

        Success
    }
}