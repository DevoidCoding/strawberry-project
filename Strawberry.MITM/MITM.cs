﻿#region License GNU GPL

// MITM.cs
// 
// Copyright (C) 2012 - BehaviorIsManaged
// 
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details. 
// You should have received a copy of the GNU General Public License along with this program; 
// if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#endregion

namespace Strawberry.MITM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Sockets;

    using IpHlpApidotnet;

    using JetBrains.Annotations;

    using NLog;

    using Strawberry.Common;
    using Strawberry.Core.Config;
    using Strawberry.Core.Extensions;
    using Strawberry.Core.Messages;
    using Strawberry.Core.Network;
    using Strawberry.MITM.Network;
    using Strawberry.Protocol.Messages;

    // ReSharper disable once InconsistentNaming
    public class MITM
    {
        #region Static Fields

        [Configurable("ServerConnectionTimeout", "Timeout in seconds before closing the connection")]
        public static int ServerConnectionTimeout = 4;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly MITMConfiguration _configuration;

        private readonly List<(BotMITM bot, SelectedServerDataMessage message)> _tickets = new List<(BotMITM, SelectedServerDataMessage)>();

        private readonly TCPUDPConnections connections;

        #endregion

        #region Constructors and Destructors

        public MITM(MITMConfiguration configuration)
        {
            _configuration = configuration;
            connections = new TCPUDPConnections();
            AuthConnections = new ClientManager<ConnectionMITM>(DnsExtensions.GetIPEndPointFromHostName(_configuration.FakeAuthHost, _configuration.FakeAuthPort, AddressFamily.InterNetwork), CreateAuthClient);
            WorldConnections = new ClientManager<ConnectionMITM>(DnsExtensions.GetIPEndPointFromHostName(_configuration.FakeWorldHost, _configuration.FakeWorldPort, AddressFamily.InterNetwork), CreateWorldClient);

            AuthConnections.ClientConnected += OnAuthClientConnected;
            AuthConnections.ClientDisconnected += OnAuthClientDisconnected;
            WorldConnections.ClientConnected += OnWorldClientConnected;
            WorldConnections.ClientDisconnected += OnWorldClientDisconnected;

            MessageBuilder = new MessageReceiver();
            MessageBuilder.Initialize();

            MessageDispatcher.RegisterSharedContainer(this);
        }

        #endregion

        #region Public Properties

        public ClientManager<ConnectionMITM> AuthConnections { get; }

        public MessageReceiver MessageBuilder { get; set; }

        public ClientManager<ConnectionMITM> WorldConnections { get; }

        #endregion

        #region Public Methods and Operators

        [MessageHandler(typeof(AuthenticationTicketMessage))]
        public static void OnAuthenticationTicketMessage(Bot bot, AuthenticationTicketMessage message)
            => bot.ClientInformations.Ticket = message.Ticket;

        public void Start()
        {
            AuthConnections.Start();
            WorldConnections.Start();

            Logger.Info("MITM started");
        }

        public void Stop()
        {
            AuthConnections.Stop();
            WorldConnections.Stop();

            Logger.Info("MITM stopped");
        }

        #endregion

        #region Methods

        private static void OnAuthClientDisconnected(ConnectionMITM client)
            => client.Bot.AddMessage(
                () =>
                    {
                        if (client.Bot.ExpectedDisconnection)
                            client.Bot.Stop();
                        else client.Bot.Dispose();
                    });

        private static void OnAuthClientMessageReceived(Client client, NetworkMessage message)
        {
            if (!(client is ConnectionMITM))
                throw new ArgumentException("client is not of type ConnectionMITM");

            var mitm = (ConnectionMITM)client;

            if (mitm.Bot == null)
                throw new NullReferenceException("mitm.Bot");

            mitm.Bot.Dispatcher.Enqueue(message, mitm.Bot);

            Logger.Debug("{0} FROM {1}", message, message.From);
        }

        private static void OnServerConnectionTimedOut(ConnectionMITM connection)
        {
            if (!connection.Server.IsConnected) Logger.Error("Cannot etablish a connection to the server ({0}). Time out {1}ms", connection.Server.IP, ServerConnectionTimeout * 1000);
            else
            {
                Logger.Warn("Send a BasicPingMessage to unblock the server connection");

                // unblock the connection (fix #14)
                connection.SendToServer(new BasicPingMessage());
            }
        }

        private static void OnWorldClientDisconnected(ConnectionMITM client)
            => client.Bot?.AddMessage(client.Bot.Dispose);

        private ConnectionMITM CreateAuthClient(Socket socket)
        {
            if (socket == null) throw new ArgumentNullException(nameof(socket));
            var client = new ConnectionMITM(socket, MessageBuilder);
            client.MessageReceived += OnAuthClientMessageReceived;

            var dispatcher = new NetworkMessageDispatcher { Client = client, Server = client.Server };

            var bot = new BotMITM(client, dispatcher);
            client.Bot = bot;
            bot.ConnectionType = ClientConnectionType.Authentification;

            BotManager.Instance.RegisterBot(bot);

            return client;
        }

        private ConnectionMITM CreateWorldClient(Socket socket)
        {
            if (socket == null) throw new ArgumentNullException(nameof(socket));
            var client = new ConnectionMITM(socket, MessageBuilder);
            client.MessageReceived += OnWorldClientMessageReceived;

            return client;
        }

        [UsedImplicitly]
        [MessageHandler(typeof(SelectedServerDataMessage), FromFilter = ListenerEntry.Server)]
        private void HandleSelectedServerDataMessage(Bot bot, SelectedServerDataMessage message)
        {
            var botMitm = (BotMITM)bot;
            _tickets.Add((botMitm, new SelectedServerDataMessage(message.ServerId, message.Address, message.Port, message.CanCreateNewCharacter, message.Ticket)));

            message.Address = _configuration.FakeWorldHost;
            message.Port = (ushort)_configuration.FakeWorldPort;

            ((BotMITM)bot).ExpectedDisconnection = true;
        }

        private void OnAuthClientConnected(ConnectionMITM client)
        {
            connections.RefreshTCP();
            client.Bot.Start();

            try
            {
                client.Server.Connected += connection =>
                    {
                        var process = connections.FirstOrDefault(e => client.Socket.LocalEndPoint.Equals(e.Remote) && e.PID != 0);

                        if (process != null)
                            client.Bot.PID = process.PID;
                    };

                client.BindToServer(_configuration.RealAuthHost, _configuration.RealAuthPort);
            }
            catch (Exception)
            {
                Logger.Error($"Cannot connect to {_configuration.RealAuthHost}:{_configuration.RealAuthPort}.");
                client.Bot.Stop();
                return;
            }

            Logger.Debug("Auth client connected");
        }

        private void OnWorldClientConnected(ConnectionMITM client)
        {
            // client.Send(new ProtocolRequired(1818, 1820));
            // client.Send(new HelloGameMessage());
            Logger.Debug("World client connected");

            connections.RefreshTCP();
            var process = connections.ToArray().FirstOrDefault(e => client.Socket.LocalEndPoint.Equals(e.Remote) && _tickets.Any(a => a.bot.PID == e.PID));

            if (process == null)
                throw new NullReferenceException(nameof(process));

            var tuple = _tickets.Find(e => e.bot.PID == process.PID);

            if (tuple.bot != null)
                _tickets.Remove(tuple);

            try
            {
                client.Bot = tuple.bot;
                client.Bot.ChangeConnection(client);
                client.Bot.ConnectionType = ClientConnectionType.GameConnection;
                client.Bot.CancelAllMessages(); // avoid to handle message from the auth client.
                client.Bot.Start();

                ((NetworkMessageDispatcher)client.Bot.Dispatcher).Client = client;
                ((NetworkMessageDispatcher)client.Bot.Dispatcher).Server = client.Server;
                client.BindToServer(tuple.message.Address, tuple.message.Port);
                tuple.bot.Start();
            }
            catch (Exception)
            {
                Logger.Error("Cannot connect to {0}:{1}.", tuple.message.Address, tuple.message.Port);
                client.Bot.Stop();
            }
        }

        private void OnWorldClientMessageReceived(Client client, NetworkMessage message)
        {
            if (!(client is ConnectionMITM mitm))
                throw new ArgumentException("client is not of type ConnectionMITM");

            if (mitm.Bot == null)
                throw new NullReferenceException("mitm.Bot");

            if (mitm.Bot.Dispatcher.Stopped)
                Logger.Warn("Enqueue a message but the dispatcher is stopped !");

            mitm.Bot.Dispatcher.Enqueue(message, mitm.Bot);

            Logger.Debug($"{message} FROM {message.From}");
        }

        #endregion
    }
}