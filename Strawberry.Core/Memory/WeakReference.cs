﻿// <copyright file="WeakReference.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Memory
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    ///     Represents a weak reference, which references an object while still allowing that object to be reclaimed by garbage
    ///     collection.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         We define our own type, unrelated to <see cref="System.WeakReference" /> both to provide type safety and
    ///         because <see cref="System.WeakReference" /> is an incorrect implementation (it does not implement
    ///         <see cref="IDisposable" />).
    ///     </para>
    /// </remarks>
    /// <typeparam name="T">The type of object to reference.</typeparam>
    public sealed class WeakReference<T> : IDisposable
        where T : class
    {
        #region Fields

        /// <summary>
        ///     The contained <see cref="SafeGCHandle" />.
        /// </summary>
        private readonly SafeGCHandle safeHandle;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="WeakReference{T}" /> class, referencing the specified object.
        /// </summary>
        /// <param name="target">The object to track. May not be null.</param>
        public WeakReference(T target)
            => safeHandle = new SafeGCHandle(target, GCHandleType.Weak);

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether the object is still alive (has not been garbage collected).
        /// </summary>
        public bool IsAlive => safeHandle.Handle.Target != null;

        /// <summary>
        ///     Gets the referenced object. Will return null if the object has been garbage collected.
        /// </summary>
        public T Target => safeHandle.Handle.Target as T;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Frees the weak reference.
        /// </summary>
        public void Dispose()
            => safeHandle.Dispose();

        #endregion
    }
}