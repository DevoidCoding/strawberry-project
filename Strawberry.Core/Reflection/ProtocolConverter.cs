﻿namespace Strawberry.Core.Reflection
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;

    using global::NLog;

    public class ProtocolConverter : Singleton<ProtocolConverter>
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly Dictionary<Type, Type> types = new Dictionary<Type, Type>();

        #endregion

        #region Public Methods and Operators

        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1502:ElementMustNotBeOnSingleLine", Justification = "Reviewed. Suppression is OK here.")]
        public T Convert<T>(object o, params object[] args)
            => (T)Convert(new[] { o }.Concat(args).ToArray());

        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1502:ElementMustNotBeOnSingleLine", Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<T> Convert<T>(IEnumerable objects, params object[] args)
            => from object o in objects select (T)Convert(new[] { o }.Concat(args).ToArray());

        public void RegisterTypes(Assembly typeAssembly, Assembly wrapperAssembly)
        {
            var types = typeAssembly.GetTypes().Where(e => e.Namespace != null && e.Namespace.Equals("Strawberry.Protocol.Types")).ToArray();

            var wrappers = wrapperAssembly.GetTypes().SelectMany(e => e.GetConstructors()).Where(e => e.GetParameters().Length != 0 && types.Contains(e.GetParameters()[0].ParameterType)).ToArray();

            foreach (var wrapper in wrappers)
            {
                var parameterType = wrapper.GetParameters()[0].ParameterType;

                if (this.types.ContainsKey(parameterType))
                    continue;

                this.types.Add(types.First(e => e == parameterType), wrapper.DeclaringType);
            }
        }

        #endregion

        #region Methods

        private object Convert(params object[] o)
        {
            try
            {
                if (o == null || o.Length == 0) throw new ArgumentNullException(nameof(o));
                if (!types.TryGetValue(o[0].GetType(), out var type)) throw new ConverterNotFoundException($"Converter not found for {o[0].GetType()}");
                return Activator.CreateInstance(type, o);
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                return null;
            }
        }

        #endregion

        public class ConverterNotFoundException : Exception
        {
            #region Constructors and Destructors

            public ConverterNotFoundException()
            {
            }

            public ConverterNotFoundException(string message)
                : base(message)
            {
            }

            public ConverterNotFoundException(string message, Exception innerException)
                : base(message, innerException)
            {
            }

            protected ConverterNotFoundException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }

            #endregion
        }
    }
}