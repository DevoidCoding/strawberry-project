﻿// <copyright file="Singleton.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Reflection
{
    using System;
    using System.Reflection;

    public abstract class Singleton<T>
        where T : class
    {
        #region Public Properties

        /// <summary>
        ///     Returns the singleton instance.
        /// </summary>
        public static T Instance => SingletonAllocator.InstanceAllocator;

        #endregion

        #region Nested type: SingletonAllocator

        private static class SingletonAllocator
        {
            #region Constructors and Destructors

            static SingletonAllocator()
                => CreateInstance(typeof(T));

            #endregion

            #region Properties

            internal static T InstanceAllocator { get; private set; }

            #endregion

            #region Methods

            private static T CreateInstance(Type type)
            {
                var ctorsPublic = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public);

                if (ctorsPublic.Length > 0)
                    return InstanceAllocator = (T)Activator.CreateInstance(type);

                var ctorNonPublic = type.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, Type.EmptyTypes, new ParameterModifier[0]);

                if (ctorNonPublic == null)
                    throw new Exception(type.FullName + " doesn't have a private/protected constructor so the property cannot be enforced.");

                try
                {
                    return InstanceAllocator = (T)ctorNonPublic.Invoke(new object[0]);
                }
                catch (Exception e)
                {
                    throw new Exception("The Singleton couldnt be constructed, check if " + type.FullName + " has a default constructor", e);
                }
            }

            #endregion
        }

        #endregion
    }
}