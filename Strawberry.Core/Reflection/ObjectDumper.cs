﻿// <copyright file="ObjectDumper.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Reflection
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Text;

    public class ObjectDumper
    {
        #region Fields

        private readonly BindingFlags binding;

        private readonly bool fields;

        private readonly int indentSize;

        private readonly bool properties;

        #endregion

        #region Constructors and Destructors

        public ObjectDumper(int indentSize, bool fields, bool properties, BindingFlags binding = BindingFlags.Default)
        {
            this.indentSize = indentSize;
            this.fields = fields;
            this.properties = properties;
            this.binding = binding;
        }

        #endregion

        #region Public Methods and Operators

        public string Dump(object obj)
            => InternalDump(obj, new StringBuilder(), 0);

        #endregion

        #region Methods

        private string FormatValue(object o)
        {
            if (o == null)
                return "null";

            if (o is DateTime)
                return ((DateTime)o).ToShortDateString();

            if (o is string)
                return $"\"{o}\"";

            if (o is ValueType)
                return o.ToString();

            if (o is IEnumerable)
                return "...";

            return "{ }";
        }

        private string InternalDump(object element, StringBuilder stringBuilder, int level)
        {
            if (element == null || element is ValueType || element is string) Write(FormatValue(element), stringBuilder, level);
            else
            {
                var objectType = element.GetType();

                if (!typeof(IEnumerable).IsAssignableFrom(objectType))
                {
                    Write("{{{0}}}", stringBuilder, level, objectType.FullName);
                    level++;
                }

                var enumerableElement = element as IEnumerable;

                if (enumerableElement != null)
                {
                    var count = 0;

                    foreach (var item in enumerableElement)
                    {
                        count++;

                        if (item is IEnumerable && !(item is string))
                        {
                            level++;
                            InternalDump(item, stringBuilder, level);
                            level--;
                        }
                        else InternalDump(item, stringBuilder, level);
                    }

                    if (count == 0)
                        Write("-Empty-", stringBuilder, level);
                }
                else
                {
                    if (fields)
                    {
                        var fields = element.GetType().GetFields(binding);

                        foreach (var field in fields)
                        {
                            var value = field.GetValue(element);

                            //events
                            if (value is MulticastDelegate)
                                continue;

                            if (field.FieldType.IsValueType || field.FieldType == typeof(string)) Write("{0}: {1}", stringBuilder, level, field.Name, FormatValue(value));
                            else
                            {
                                Write("{0}: {1}", stringBuilder, level, field.Name, typeof(IEnumerable).IsAssignableFrom(field.FieldType) ? "..." : "{ }");
                                level++;
                                InternalDump(value, stringBuilder, level);
                                level--;
                            }
                        }
                    }

                    if (properties)
                    {
                        var properties = element.GetType().GetProperties(binding);

                        foreach (var property in properties)
                        {
                            var value = property.GetValue(element, null);

                            //events
                            if (value is MulticastDelegate)
                                continue;

                            if (property.PropertyType.IsValueType || property.PropertyType == typeof(string)) Write("{0}: {1}", stringBuilder, level, property.Name, FormatValue(value));
                            else
                            {
                                Write("{0}: {1}", stringBuilder, level, property.Name, typeof(IEnumerable).IsAssignableFrom(property.PropertyType) ? "..." : "{ }");
                                level++;
                                InternalDump(value, stringBuilder, level);
                                level--;
                            }
                        }
                    }
                }

                if (!typeof(IEnumerable).IsAssignableFrom(objectType)) level--;
            }

            return stringBuilder.ToString();
        }

        private void Write(string value, StringBuilder builder, int level, params object[] args)
        {
            var space = new string(' ', level * indentSize);

            if (args != null)
                value = string.Format(value, args);

            builder.AppendLine(space + value);
        }

        #endregion
    }
}