﻿// <copyright file="BigEndianWriter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.IO
{
    using System;
    using System.IO;
    using System.Text;

    public class BigEndianWriter : IDataWriter, IDisposable
    {
        #region Fields

        private BinaryWriter writer;

        #endregion

        #region Public Properties

        public Stream BaseStream => writer.BaseStream;

        /// <summary>
        ///     Gets available bytes number in the buffer
        /// </summary>
        public long BytesAvailable => writer.BaseStream.Length - writer.BaseStream.Position;

        public byte[] Data
        {
            get
            {
                var pos = writer.BaseStream.Position;

                var data = new byte[writer.BaseStream.Length];
                writer.BaseStream.Position = 0;
                writer.BaseStream.Read(data, 0, (int)writer.BaseStream.Length);

                writer.BaseStream.Position = pos;

                return data;
            }
        }

        public long Position
        {
            get => writer.BaseStream.Position;
            set => writer.BaseStream.Position = value;
        }

        #endregion

        #region Public Methods and Operators

        public void Clear()
            => writer = new BinaryWriter(new MemoryStream(), Encoding.UTF8);

        #region Dispose

        public void Dispose()
        {
            writer.Dispose();
            writer = null;
        }

        #endregion

        public void Seek(int offset, SeekOrigin seekOrigin)
            => writer.BaseStream.Seek(offset, seekOrigin);

        /// <summary>
        ///     Write a Boolean into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteBoolean(bool @bool)
        {
            if (@bool) writer.Write((byte)1);
            else writer.Write((byte)0);
        }

        /// <summary>
        ///     Write a byte into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteByte(byte @byte)
            => writer.Write(@byte);

        /// <summary>
        ///     Write a bytes array into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteBytes(byte[] data)
            => writer.Write(data);

        /// <summary>
        ///     Write a bytes array into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteBytes(byte[] data, uint offset, uint length)
        {
            var array = new byte[length];
            Array.Copy(data, offset, array, 0, length);
            writer.Write(array);
        }

        /// <summary>
        ///     Write a Char into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteChar(char @char)
            => WriteBigEndianBytes(BitConverter.GetBytes(@char));

        /// <summary>
        ///     Write a Double into the buffer
        /// </summary>
        public void WriteDouble(double @double)
            => WriteBigEndianBytes(BitConverter.GetBytes(@double));

        /// <summary>
        ///     Write a Float into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteFloat(float @float)
            => writer.Write(@float);

        /// <summary>
        ///     Write a int into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteInt(int @int)
            => WriteBigEndianBytes(BitConverter.GetBytes(@int));

        /// <summary>
        ///     Write a long into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteLong(long @long)
            => WriteBigEndianBytes(BitConverter.GetBytes(@long));

        public void WriteSByte(sbyte @byte)
            => writer.Write(@byte);

        /// <summary>
        ///     Write a Short into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteShort(short @short)
            => WriteBigEndianBytes(BitConverter.GetBytes(@short));

        /// <summary>
        ///     Write a Single into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteSingle(float single)
            => WriteBigEndianBytes(BitConverter.GetBytes(single));

        /// <summary>
        ///     Write a int into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteUInt(uint @uint)
            => WriteBigEndianBytes(BitConverter.GetBytes(@uint));

        /// <summary>
        ///     Write a long into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteULong(ulong @ulong)
            => WriteBigEndianBytes(BitConverter.GetBytes(@ulong));

        /// <summary>
        ///     Write a UShort into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteUShort(ushort @ushort)
            => WriteBigEndianBytes(BitConverter.GetBytes(@ushort));

        /// <summary>
        ///     Write a string into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteUTF(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            var len = (ushort)bytes.Length;
            WriteUShort(len);

            int i;

            for (i = 0; i < len; i++)
                writer.Write(bytes[i]);
        }

        /// <summary>
        ///     Write a string into the buffer
        /// </summary>
        /// <returns></returns>
        public void WriteUTFBytes(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            var len = bytes.Length;
            int i;

            for (i = 0; i < len; i++)
                writer.Write(bytes[i]);
        }

        public void WriteVarInt(int @int)
            => WriteVar((uint)@int);

        public void WriteVarInt(uint @uint)
            => WriteVar(@uint);

        public void WriteVarLong(long @long)
            => WriteVar((ulong)@long);

        public void WriteVarLong(ulong @ulong)
            => WriteVar(@ulong);

        public void WriteVarShort(short @int)
            => WriteVar((ushort)@int);

        public void WriteVarShort(ushort @uint)
            => WriteVar(@uint);

        #endregion

        #region Methods

        /// <summary>
        ///     Reverse bytes and write them into the buffer
        /// </summary>
        private void WriteBigEndianBytes(byte[] endianBytes)
        {
            for (var i = endianBytes.Length - 1; i >= 0; i--) writer.Write(endianBytes[i]);
        }

        private void WriteVar(ulong number)
        {
            while (number > 127)
            {
                WriteByte((byte)((number & 127) | 128));
                number >>= 7;
            }

            WriteByte((byte)number);
        }

        #endregion

        #region Initialisation

        /// <summary>
        ///     Initializes a new instance of the <see cref="BigEndianWriter" /> class.
        /// </summary>
        public BigEndianWriter()
            => writer = new BinaryWriter(new MemoryStream(), Encoding.UTF8);

        /// <summary>
        ///     Initializes a new instance of the <see cref="BigEndianWriter" /> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        public BigEndianWriter(Stream stream)
            => writer = new BinaryWriter(stream, Encoding.UTF8);

        public BigEndianWriter(byte[] buffer)
            => writer = new BinaryWriter(new MemoryStream(buffer));

        #endregion
    }
}