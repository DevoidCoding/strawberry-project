﻿// <copyright file="BigEndianReader.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.IO
{
    using System;
    using System.IO;
    using System.Text;

    public class BigEndianReader : IDataReader, IDisposable
    {
        #region Fields

        private BinaryReader reader;

        #endregion

        #region Public Properties

        public Stream BaseStream => reader.BaseStream;

        /// <summary>
        ///     Gets availiable bytes number in the buffer
        /// </summary>
        public long BytesAvailable => reader.BaseStream.Length - reader.BaseStream.Position;

        public long Position => reader.BaseStream.Position;

        #endregion

        #region Public Methods and Operators

        #region Dispose

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            reader.Dispose();
            reader = null;
        }

        #endregion

        public short ReadInt16()
            => ReadShort();

        public int ReadInt32()
            => ReadInt();

        public long ReadInt64()
            => ReadLong();

        public string ReadString()
            => ReadUTF();

        public ushort ReadUInt16()
            => ReadUShort();

        public uint ReadUInt32()
            => ReadUInt();

        public ulong ReadUInt64()
            => ReadULong();

        public int ReadVarInt()
            => (int)ReadVar<uint>();

        public long ReadVarLong()
            => (long)ReadVar<ulong>();

        public short ReadVarShort()
            => (short)ReadVar<ushort>();

        public uint ReadVarUhInt()
            => ReadVar<uint>();

        public ulong ReadVarUhLong()
            => ReadVar<ulong>();

        public ushort ReadVarUhShort()
            => ReadVar<ushort>();

        #endregion

        #region Methods

        /// <summary>
        ///     Read bytes in big endian format
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private byte[] ReadBigEndianBytes(int count)
        {
            var bytes = new byte[count];
            int i;

            for (i = count - 1; i >= 0; i--)
                bytes[i] = (byte)BaseStream.ReadByte();

            return bytes;
        }

        private T ReadVar<T>()
        {
            if (typeof(T) == typeof(short) || typeof(T) == typeof(int) || typeof(T) == typeof(long))
                throw new Exception("Type must be unsigned");

            var size = 0;

            if (typeof(T) == typeof(ushort)) size = 8 * 2;
            else if (typeof(T) == typeof(uint)) size = 8 * 4;
            else if (typeof(T) == typeof(ulong)) size = 8 * 8;

            var shift = 0;
            ulong result = 0;

            while (shift < size)
            {
                var b = ReadByte();
                result |= (ulong)(b & 127) << shift;

                if ((b & 128) == 0)
                    break;

                shift += 7;
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        #endregion

        #region Initialisation

        /// <summary>
        ///     Initializes a new instance of the <see cref="BigEndianReader" /> class.
        /// </summary>
        public BigEndianReader()
            => reader = new BinaryReader(new MemoryStream(), Encoding.UTF8);

        /// <summary>
        ///     Initializes a new instance of the <see cref="BigEndianReader" /> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        public BigEndianReader(Stream stream)
            => reader = new BinaryReader(stream, Encoding.UTF8);

        /// <summary>
        ///     Initializes a new instance of the <see cref="BigEndianReader" /> class.
        /// </summary>
        /// <param name="tab">Memory buffer.</param>
        public BigEndianReader(byte[] tab)
            => reader = new BinaryReader(new MemoryStream(tab), Encoding.UTF8);

        #endregion

        #region Public Method

        /// <summary>
        ///     Read a Short from the Buffer
        /// </summary>
        /// <returns></returns>
        public short ReadShort()
            => BitConverter.ToInt16(ReadBigEndianBytes(2), 0);

        /// <summary>
        ///     Read a int from the Buffer
        /// </summary>
        /// <returns></returns>
        public int ReadInt()
            => BitConverter.ToInt32(ReadBigEndianBytes(4), 0);

        /// <summary>
        ///     Read a long from the Buffer
        /// </summary>
        /// <returns></returns>
        public long ReadLong()
            => BitConverter.ToInt64(ReadBigEndianBytes(8), 0);

        /// <summary>
        ///     Read a Float from the Buffer
        /// </summary>
        /// <returns></returns>
        public float ReadFloat()
            => BitConverter.ToSingle(ReadBigEndianBytes(4), 0);

        /// <summary>
        ///     Read a UShort from the Buffer
        /// </summary>
        /// <returns></returns>
        public ushort ReadUShort()
            => BitConverter.ToUInt16(ReadBigEndianBytes(2), 0);

        /// <summary>
        ///     Read a int from the Buffer
        /// </summary>
        /// <returns></returns>
        public uint ReadUInt()
            => BitConverter.ToUInt32(ReadBigEndianBytes(4), 0);

        /// <summary>
        ///     Read a long from the Buffer
        /// </summary>
        /// <returns></returns>
        public ulong ReadULong()
            => BitConverter.ToUInt64(ReadBigEndianBytes(8), 0);

        /// <summary>
        ///     Read a byte from the Buffer
        /// </summary>
        /// <returns></returns>
        public byte ReadByte()
            => reader.ReadByte();

        public sbyte ReadSByte()
            => reader.ReadSByte();

        public byte[] Data
        {
            get
            {
                var pos = BaseStream.Position;

                var data = new byte[BaseStream.Length];
                BaseStream.Position = 0;
                BaseStream.Read(data, 0, (int)BaseStream.Length);

                BaseStream.Position = pos;

                return data;
            }
        }

        /// <summary>
        ///     Returns N bytes from the buffer
        /// </summary>
        /// <param name="n">Number of read bytes.</param>
        /// <returns></returns>
        public byte[] ReadBytes(int n)
            => reader.ReadBytes(n);

        /// <summary>
        ///     Returns N bytes from the buffer
        /// </summary>
        /// <param name="n">Number of read bytes.</param>
        /// <returns></returns>
        public BigEndianReader ReadBytesInNewBigEndianReader(int n)
            => new BigEndianReader(reader.ReadBytes(n));

        /// <summary>
        ///     Read a Boolean from the Buffer
        /// </summary>
        /// <returns></returns>
        public bool ReadBoolean()
            => reader.ReadByte() == 1;

        /// <summary>
        ///     Read a Char from the Buffer
        /// </summary>
        /// <returns></returns>
        public char ReadChar()
            => (char)ReadUShort();

        /// <summary>
        ///     Read a Double from the Buffer
        /// </summary>
        /// <returns></returns>
        public double ReadDouble()
            => BitConverter.ToDouble(ReadBigEndianBytes(8), 0);

        /// <summary>
        ///     Read a Single from the Buffer
        /// </summary>
        /// <returns></returns>
        public float ReadSingle()
            => BitConverter.ToSingle(ReadBigEndianBytes(4), 0);

        /// <summary>
        ///     Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTF()
        {
            var length = ReadUShort();

            var bytes = ReadBytes(length);
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        ///     Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTF7BitLength()
        {
            var length = ReadInt();

            return ReadUTFBytes(length);
        }

        /// <summary>
        ///     Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTFBytes(int len)
        {
            var bytes = ReadBytes(len);

            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        ///     Skip bytes
        /// </summary>
        /// <param name="n"></param>
        public void SkipBytes(int n)
        {
            int i;
            for (i = 0; i < n; i++) reader.ReadByte();
        }

        public void SetPosition(int position)
            => SetPosition(position, SeekOrigin.Begin);

        public void SetPosition(int position, SeekOrigin origin)
            => Seek(position, origin);

        public void Seek(int offset, SeekOrigin seekOrigin)
            => reader.BaseStream.Seek(offset, seekOrigin);

        /// <summary>
        ///     Add a bytes array to the end of the buffer
        /// </summary>
        public void Add(byte[] data, int offset, int count)
        {
            var pos = reader.BaseStream.Position;

            reader.BaseStream.Position = reader.BaseStream.Length;
            reader.BaseStream.Write(data, offset, count);
            reader.BaseStream.Position = pos;
        }

        public void Close()
            => BaseStream.Close();

        #endregion
    }
}