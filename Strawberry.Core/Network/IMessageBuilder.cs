﻿// <copyright file="IMessageBuilder.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using Strawberry.Core.IO;

    public interface IMessageBuilder
    {
        #region Public Methods and Operators

        NetworkMessage BuildMessage(uint messageid, IDataReader reader);

        #endregion
    }
}