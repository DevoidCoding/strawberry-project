﻿// <copyright file="NetworkMessage.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Linq;

    using global::NLog;

    using Strawberry.Core.Cryptography;
    using Strawberry.Core.IO;
    using Strawberry.Core.Messages;

    [Serializable]
    public abstract class NetworkMessage : Message
    {
        #region Constants

        private const byte BitMask = 3;

        private const byte BitRightShiftLenPacketId = 2;

        #endregion

        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Properties

        public ListenerEntry Destinations { get; set; }

        public ListenerEntry From { get; set; }

        public byte[] HashKey { get; set; }

        public uint InstanceId { get; set; }

        public abstract uint MessageID { get; }

        #endregion

        #region Public Methods and Operators

        public void BlockNetworkSend()
        {
            Destinations = ListenerEntry.Local;

            Logger.Debug("Block message {0}", this);
        }

        public override void BlockProgression()
        {
            Canceled = true;
            Destinations = ListenerEntry.Local;
        }

        public abstract void Deserialize(IDataReader reader);

        public void Pack(IDataWriter writer)
        {
            Serialize(writer);
            WritePacket(writer);
        }

        public abstract void Serialize(IDataWriter writer);

        public override string ToString()
            => GetType().Name;

        public void Unpack(IDataReader reader)
            => Deserialize(reader);

        #endregion

        #region Methods

        private static byte ComputeTypeLen(int param1)
        {
            if (param1 > 65535)
                return 3;

            if (param1 > 255)
                return 2;

            if (param1 > 0)
                return 1;

            return 0;
        }

        private static uint SubComputeStaticHeader(uint id, byte typeLen)
            => (id << BitRightShiftLenPacketId) | typeLen;

        private byte[] Hash(byte[] packet)
        {
            if (HashKey == null || HashKey.Length == 0) return packet;
            var md5Hash = Cryptography.Pkcs5Pad(Cryptography.MD5Hash(packet));
            var aesHash = Cryptography.HashSimpleIVMode(md5Hash, HashKey);
            return packet.Concat(aesHash).ToArray();
        }

        private bool IsHashMessage()
        {
            var type = GetType();
            return GetType().CustomAttributes.Any(e => e.AttributeType == typeof(HashMessageAttribute));
        }

        private void WritePacket(IDataWriter writer)
        {
            var packet = writer.Data;

            if (IsHashMessage()) packet = Hash(packet);

            writer.Clear();

            var typeLen = ComputeTypeLen(packet.Length);
            var header = (short)SubComputeStaticHeader(MessageID, typeLen);
            writer.WriteShort(header);

            if (Destinations.HasFlag(ListenerEntry.Server))
                writer.WriteUInt(InstanceId);

            switch (typeLen)
            {
                case 0:
                    break;
                case 1:
                    writer.WriteByte((byte)packet.Length);
                    break;
                case 2:
                    writer.WriteShort((short)packet.Length);
                    break;
                case 3:
                    writer.WriteByte((byte)((packet.Length >> 16) & 255));
                    writer.WriteShort((short)(packet.Length & 65535));
                    break;
                default:

                    throw new Exception("Packet's length can't be encoded on 4 or more bytes");
            }

            writer.WriteBytes(packet);
        }

        #endregion
    }
}