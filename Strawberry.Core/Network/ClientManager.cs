﻿// <copyright file="ClientManager.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    using global::NLog;

    public class ClientManager<T>
        where T : class, IClient
    {
        #region Constants

        public const int BufferSize = 8192;

        public const int MaxConcurrentConnections = 1000;

        #endregion

        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly SocketAsyncEventArgs acceptArgs = new SocketAsyncEventArgs();
        // allocate memory dedicated to a client to avoid memory alloc on each send/recv

        private readonly ClientCreationHandler clientCreationDelegate;

        private readonly List<T> clients = new List<T>();

        private readonly IPEndPoint endPoint;

        private readonly Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        // async arg used on client connection

        private BufferManager bufferManager;

        private SemaphoreSlim semaphore; // limit the number of threads accessing to a ressource

        #endregion

        #region Constructors and Destructors

        public ClientManager(IPEndPoint endPoint, ClientCreationHandler clientCreationDelegate)
        {
            this.endPoint = endPoint;
            this.clientCreationDelegate = clientCreationDelegate;

            Initialize();
        }

        #endregion

        #region Delegates

        public delegate T ClientCreationHandler(Socket socket);

        #endregion

        #region Public Events

        public event Action<T> ClientConnected;

        public event Action<T> ClientDisconnected;

        #endregion

        #region Public Properties

        /// <summary>
        ///     List of connected Clients
        /// </summary>
        public ReadOnlyCollection<T> Clients => clients.AsReadOnly();

        public int Count => clients.Count;

        public bool Running { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Start()
        {
            if (Running)
                return;

            Running = true;

            socket.Bind(endPoint);
            socket.Listen(MaxConcurrentConnections);

            StartAccept();
        }

        public void Stop()
        {
            if (!Running)
                return;

            Running = false;

            foreach (var client in clients) client.Disconnect();

            semaphore = new SemaphoreSlim(MaxConcurrentConnections, MaxConcurrentConnections);

            clients.Clear();
            socket.Close();
        }

        #endregion

        #region Methods

        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            var client = e.UserToken as T;

            if (client == null)
            {
                e.Dispose();
                return;
            }

            try
            {
                client.Disconnect();
            }
            finally
            {
                var removed = false;

                lock (clients)
                {
                    removed = clients.Remove(client);
                }

                if (removed)
                {
                    OnClientDisconnected(client);
                    semaphore.Release();
                }

                e.Dispose();
            }
        }

        private void Initialize()
        {
            // init buffer manager
            bufferManager = new BufferManager(MaxConcurrentConnections * BufferSize, BufferSize);
            bufferManager.InitializeBuffer();

            // init semaphore
            semaphore = new SemaphoreSlim(MaxConcurrentConnections, MaxConcurrentConnections);
            acceptArgs.Completed += (sender, e) => ProcessAccept(e);
        }

        private void OnClientConnected(T client)
        {
            var handler = ClientConnected;
            if (handler != null) handler(client);
        }

        private void OnClientDisconnected(T client)
        {
            var handler = ClientDisconnected;
            if (handler != null) handler(client);
        }

        private void OnReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                switch (e.LastOperation)
                {
                    case SocketAsyncOperation.Receive:
                        ProcessReceive(e);
                        break;
                    case SocketAsyncOperation.Disconnect:
                        CloseClientSocket(e);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                // theoretically it shouldn't go up to there.
                //logger.Error("Last chance exception on receiving ! : " + exception);
            }
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            if (!Running)
                return;

            T client = null;

            try
            {
                // use a async arg from the pool avoid to re-allocate memory on each connection
                var args = new SocketAsyncEventArgs();
                args.Completed += OnReceiveCompleted;

                bufferManager.SetBuffer(args);

                // create the client instance
                client = clientCreationDelegate(e.AcceptSocket);
                args.UserToken = client;

                lock (clients)
                {
                    clients.Add(client);
                }

                OnClientConnected(client);

                // if the event is not raised we first check new connections before parsing message that can blocks the connection queue
                if (!e.AcceptSocket.ReceiveAsync(args))
                {
                    StartAccept();
                    ProcessReceive(args);
                }
                else StartAccept();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception occured during client connection : ");

                client?.Disconnect();

                StartAccept();
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (!Running)
                return;

            if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
            {
                CloseClientSocket(e);
                return;
            }

            if (!(e.UserToken is Client client))
            {
                CloseClientSocket(e);
                return;
            }

            client.Receive(e.Buffer, e.Offset, e.BytesTransferred);

            if (client.Socket == null)
            {
                CloseClientSocket(e);
                return;
            }

            // just continue to receive
            var willRaiseEvent = client.Socket.ReceiveAsync(e);

            if (!willRaiseEvent) ProcessReceive(e);
        }

        private void StartAccept()
        {
            acceptArgs.AcceptSocket = null;

            // thread block if the max connections limit is reached
            semaphore.Wait();

            if (!Running)
                return;

            // raise or not the event depending on AcceptAsync return
            if (!socket.AcceptAsync(acceptArgs)) ProcessAccept(acceptArgs);
        }

        #endregion
    }
}