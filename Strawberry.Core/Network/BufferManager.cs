﻿// <copyright file="BufferManager.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Collections.Generic;
    using System.Net.Sockets;

    public sealed class BufferManager : IDisposable
    {
        #region Fields

        private readonly int bufferSize;

        private readonly int numBytes; // the total number of bytes controlled by the buffer pool

        private byte[] buffer; // the underlying byte array maintained by the Buffer Manager

        private int currentIndex;

        private Stack<int> freeIndexPool;

        #endregion

        #region Constructors and Destructors

        public BufferManager(int totalBytes, int bufferSize)
        {
            numBytes = totalBytes;
            currentIndex = 0;
            this.bufferSize = bufferSize;
            freeIndexPool = new Stack<int>();
        }

        #endregion

        #region Public Methods and Operators

        #region IDisposable Members

        public void Dispose()
        {
            buffer = null;
            freeIndexPool = null;
        }

        #endregion

        // Removes the buffer from a SocketAsyncEventArg object.  
        // This frees the buffer back to the buffer pool
        public void FreeBuffer(SocketAsyncEventArgs args)
        {
            freeIndexPool.Push(args.Offset);
            args.SetBuffer(null, 0, 0);
        }

        // Allocates buffer space used by the buffer pool
        public void InitializeBuffer()
            => buffer = new byte[numBytes];

        // Assigns a buffer from the buffer pool to the 
        // specified SocketAsyncEventArgs object
        //
        // <returns>true if the buffer was successfully set, else false</returns>
        public bool SetBuffer(SocketAsyncEventArgs args)
        {
            if (freeIndexPool.Count > 0) args.SetBuffer(buffer, freeIndexPool.Pop(), bufferSize);
            else
            {
                if (numBytes - bufferSize < currentIndex) return false;
                args.SetBuffer(buffer, currentIndex, bufferSize);
                currentIndex += bufferSize;
            }

            return true;
        }

        #endregion
    }
}