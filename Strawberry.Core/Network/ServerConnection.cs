﻿// <copyright file="ServerConnection.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Threading.Tasks;

    using global::NLog;

    using Strawberry.Core.IO;

    public class ServerConnection : IClient, INotifyPropertyChanged
    {
        #region Fields

        private byte[] buffer = new byte[8192];

        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly BigEndianReader reader = new BigEndianReader();

        #endregion

        #region Constructors and Destructors

        public ServerConnection(IMessageBuilder messageBuilder)
        {
            TcpClient = new TcpClient(AddressFamily.InterNetwork);
            MessageBuilder = messageBuilder;
        }

        #endregion

        #region Public Events

        public event Action<ServerConnection> Connected;

        public event Action<ServerConnection> Disconnected;

        public event Action<ServerConnection, LogEventInfo> LogMessage;

        public event Action<ServerConnection, NetworkMessage> MessageReceived;

        public event Action<ServerConnection, NetworkMessage> MessageSent;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public string Host { get; private set; }

        public string IP => $"{Host}:{Port}";

        public bool IsConnected => TcpClient != null && TcpClient.Connected;

        /// <summary>
        ///     True whenever the server has sent at least one packet
        /// </summary>
        public bool IsResponding { get; private set; }

        /// <summary>
        ///     Last activity as a socket client (since last packet received sent )
        /// </summary>
        public DateTime LastActivity { get; private set; }

        public IMessageBuilder MessageBuilder { get; }

        public int Port { get; private set; }

        public TcpClient TcpClient { get; set; }

        #endregion

        #region Public Methods and Operators

        public void Connect(string host, int port)
        {
            if (TcpClient == null) throw new Exception("Socket already closed");

            if (TcpClient.Connected) throw new Exception("Socket already connected");

            Host = host;
            Port = port;
            
            TcpClient = new TcpClient();
            TcpClient.Connect(host, port);
            OnClientConnected();

            Task.Factory.StartNew(ReceiveLoop);
        }

        /// <summary>
        ///     Disconnect the Client
        /// </summary>
        public void Disconnect()
        {
            if (IsConnected) Close();

            OnClientDisconnected();
        }

        private MessagePart currentMessage;

        public virtual void Receive(byte[] data, int offset, int count)
        {
            try
            {
                reader.Add(data, offset, count);

                while (reader.BytesAvailable > 0)
                {
                    if (currentMessage == null)
                        currentMessage = new MessagePart();

                    if (currentMessage.Build(reader, false))
                    {
                        var messageDataReader = new BigEndianReader(currentMessage.Data);
                        var message = MessageBuilder.BuildMessage((uint)currentMessage.MessageId.Value, messageDataReader);

                        if (message.MessageID == 2)
                        {
                            var property = message.GetType().GetField("Content", BindingFlags.Public | BindingFlags.Instance);

                            if (property != null)
                                if (property.GetValue(message) is byte[] content)
                                    reader.Add(content, 0, content.Length);
                        }

                        LastActivity = DateTime.Now;
                        OnMessageReceived(message);

                        currentMessage = null;
                    }
                }
            }
            catch (Exception error)
            {
                logger.Fatal(error);
            }
        }

        public void Reconnect()
        {
            if (IsConnected)
                Disconnect();

            TcpClient = new TcpClient(AddressFamily.InterNetwork);

            Connect(Host, Port);
        }

        public void Send(NetworkMessage message)
        {
            if (!IsConnected)
                return;

            var args = new SocketAsyncEventArgs();
            args.Completed += OnSendCompleted;
            args.UserToken = message;

            byte[] data;

            using (var writer = new BigEndianWriter())
            {
                message.Pack(writer);
                data = writer.Data;
            }

            args.SetBuffer(data, 0, data.Length);

            if (!TcpClient.Client.SendAsync(args))
            {
                OnMessageSended(message);
                args.Dispose();
            }

            LastActivity = DateTime.Now;
        }

        public override string ToString()
            => string.Concat("<", IP, ">");

        #endregion

        #region Methods

        protected void Close()
        {
            if (TcpClient == null || !TcpClient.Connected) return;
            TcpClient.Close();

            TcpClient = null;
        }

        protected void FireLogMessage(LogEventInfo logEventInfo)
            => LogMessage?.Invoke(this, logEventInfo);

        protected void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void OnClientConnected()
            => Connected?.Invoke(this);

        private void OnClientDisconnected()
            => Disconnected?.Invoke(this);

        private void OnMessageReceived(NetworkMessage message)
            => MessageReceived?.Invoke(this, message);

        private void OnMessageSended(NetworkMessage message)
            => MessageSent?.Invoke(this, message);

        private void OnReceiveCompleted(object sender, SocketAsyncEventArgs args)
        {
            switch (args.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceiveCompleted(args);
                    break;
                case SocketAsyncOperation.Disconnect:
                    Disconnect();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnSendCompleted(object sender, SocketAsyncEventArgs socketAsyncEventArgs)
        {
            OnMessageSended((NetworkMessage)socketAsyncEventArgs.UserToken);
            socketAsyncEventArgs.Dispose();
        }

        private void ProcessReceiveCompleted(SocketAsyncEventArgs args)
        {
            if (!IsConnected)
                return;

            IsResponding = true;

            if (args.BytesTransferred <= 0 || args.SocketError != SocketError.Success)
                Disconnect();
            else
            {
                Receive(args.Buffer, args.Offset, args.BytesTransferred);

                ReceiveLoop();
            }
        }

        private void ReceiveLoop()
        {
            if (!IsConnected)
                return;

            var args = new SocketAsyncEventArgs();
            args.Completed += OnReceiveCompleted;
            args.SetBuffer(buffer, 0, 8192);

            if (!TcpClient.Client.ReceiveAsync(args)) ProcessReceiveCompleted(args);
        }

        #endregion
    }
}