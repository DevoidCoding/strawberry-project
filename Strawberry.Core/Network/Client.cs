﻿// <copyright file="Client.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Globalization;
    using System.Net.Sockets;

    using global::NLog;

    using Strawberry.Core.IO;

    public abstract class Client : IClient
    {
        #region Fields

        private readonly BigEndianReader buffer = new BigEndianReader();

        #endregion

        #region Constructors and Destructors

        protected Client(Socket socket)
            => Socket = socket;

        #endregion

        #region Public Events

        public event Action<Client> Disconnected;

        public event Action<Client, LogEventInfo> LogMessage;

        public event Action<Client, NetworkMessage> MessageReceived;

        public event Action<Client, NetworkMessage> MessageSent;

        #endregion

        #region Public Properties

        public bool Connected => Socket != null && Socket.Connected;

        /// <summary>
        ///     Last activity as a socket client (last received packet or sent packet)
        /// </summary>
        public DateTime LastActivity { get; private set; }

        public abstract IMessageBuilder MessageBuilder { get; }

        public Socket Socket { get; private set; }

        #endregion

        #region Public Methods and Operators

        public virtual void Disconnect()
        {
            if (Socket != null && Socket.Connected)
            {
                Socket.Shutdown(SocketShutdown.Both);
                Socket.Close();

                Socket = null;
            }

            OnClientDisconnected();
        }

        public virtual void Log(LogLevel level, string message, params object[] args)
        {
            var log = new LogEventInfo(level, string.Empty, CultureInfo.CurrentCulture, message, args);
            LogMessage?.Invoke(this, log);
        }

        public virtual void Receive(byte[] data, int offset, int count)
        {
            try
            {
                buffer.Add(data, offset, count);

                BuildMessage(true);
            }
            catch (Exception ex)
            {
                Log(LogLevel.Fatal, "Cannot process receive " + ex);
            }
        }

        public virtual void Reset(Socket newSocket)
        {
            Disconnect();

            Socket = newSocket;
        }

        public virtual void Send(NetworkMessage message)
        {
            if (Socket == null || !Connected)
                return;

            var args = new SocketAsyncEventArgs();
            args.Completed += OnSendCompleted;
            args.UserToken = message;

            byte[] data;

            using (var writer = new BigEndianWriter())
            {
                message.Pack(writer);
                data = writer.Data;
            }

            args.SetBuffer(data, 0, data.Length);

            if (!Socket.SendAsync(args))
            {
                OnMessageSended(message);
                args.Dispose();
            }

            LastActivity = DateTime.Now;
        }

        #endregion

        #region Methods

        protected virtual void OnClientDisconnected()
            => Disconnected?.Invoke(this);

        protected virtual void OnMessageReceived(NetworkMessage message)
            => MessageReceived?.Invoke(this, message);

        protected virtual void OnMessageSended(NetworkMessage message)
            => MessageSent?.Invoke(this, message);

        private void BuildMessage(bool isFromClient)
        {
            while (buffer.BytesAvailable > 0)
            {
                var currentMessage = new MessagePart();

                // if message is complete
                if (!currentMessage.Build(buffer, isFromClient) || !currentMessage.MessageId.HasValue) return;

                var messageDataReader = new BigEndianReader(currentMessage.Data);
                var message = MessageBuilder.BuildMessage((uint)currentMessage.MessageId.Value, messageDataReader);

                LastActivity = DateTime.Now;

                OnMessageReceived(message);
            }
        }

        private void OnSendCompleted(object sender, SocketAsyncEventArgs e)
        {
            OnMessageSended((NetworkMessage)e.UserToken);
            e.Dispose();
        }

        #endregion
    }
}