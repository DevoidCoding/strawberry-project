﻿// <copyright file="ListenerEntry.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;

    [Flags]
    public enum ListenerEntry
    {
        Undefined = 0,

        Local = 1,

        Client = 2,

        Server = 4
    }
}