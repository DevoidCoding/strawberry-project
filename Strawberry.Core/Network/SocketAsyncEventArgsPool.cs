﻿// <copyright file="SocketAsyncEventArgsPool.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Collections.Generic;
    using System.Net.Sockets;

    public sealed class SocketAsyncEventArgsPool : IDisposable
    {
        #region Fields

        private readonly Stack<SocketAsyncEventArgs> pool;

        private bool disposed;

        #endregion

        #region Constructors and Destructors

        public SocketAsyncEventArgsPool(int capacity)
            => pool = new Stack<SocketAsyncEventArgs>(capacity);

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the number of SocketAsyncEventArgs instances in the pool
        /// </summary>
        public int Count => pool.Count;

        #endregion

        #region Public Methods and Operators

        #region IDisposable Members

        public void Dispose()
        {
            pool.Clear();
            disposed = true;
        }

        #endregion

        public SocketAsyncEventArgs Pop()
        {
            if (disposed)
                throw new ObjectDisposedException("SocketAsyncEventArgsPool");

            lock (pool)
            {
                return pool.Pop();
            }
        }

        public void Push(SocketAsyncEventArgs item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            lock (pool)
            {
                pool.Push(item);
            }
        }

        #endregion
    }
}