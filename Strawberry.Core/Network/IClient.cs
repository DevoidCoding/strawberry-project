﻿// <copyright file="IClient.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    public interface IClient
    {
        #region Public Methods and Operators

        void Disconnect();

        void Receive(byte[] buffer, int offset, int length);

        void Send(NetworkMessage message);

        #endregion
    }
}