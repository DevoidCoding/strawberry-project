// <copyright file="IPHlpApi32Wrapper.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Network
{
    using System;
    using System.Runtime.InteropServices;

    #region TCP

    [StructLayout(LayoutKind.Sequential)]
    public struct MibTcprowOwnerPid
    {
        #region Fields

        public uint localAddr;

        public byte localPort1;

        public byte localPort2;

        public byte localPort3;

        public byte localPort4;

        public int owningPid;

        public uint remoteAddr;

        public byte remotePort1;

        public byte remotePort2;

        public byte remotePort3;

        public byte remotePort4;

        public uint state;

        #endregion

        #region Public Properties

        public ushort LocalPort => BitConverter.ToUInt16(new byte[2] { localPort2, localPort1 }, 0);

        public ushort RemotePort => BitConverter.ToUInt16(new byte[2] { remotePort2, remotePort1 }, 0);

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MibTcptableOwnerPid
    {
        #region Fields

        public uint dwNumEntries;

        private readonly MibTcprowOwnerPid table;

        #endregion
    }

    internal enum TcpTableClass
    {
        TcpTableBasicListener,

        TcpTableBasicConnections,

        TcpTableBasicAll,

        TcpTableOwnerPidListener,

        TcpTableOwnerPidConnections,

        TcpTableOwnerPidAll,

        TcpTableOwnerModuleListener,

        TcpTableOwnerModuleConnections,

        TcpTableOwnerModuleAll
    }

    #endregion

    public class IPHlpApi32Wrapper
    {
        #region Public Methods and Operators

        public static MibTcprowOwnerPid[] GetAllTcpConnections()
        {
            //  TcpRow is my own class to display returned rows in a nice manner.
            //    TcpRow[] tTable;
            MibTcprowOwnerPid[] tTable;
            var afInet = 2; // IP_v4
            var buffSize = 0;

            // how much memory do we need?
            var ret = GetExtendedTcpTable(IntPtr.Zero, ref buffSize, true, afInet, TcpTableClass.TcpTableOwnerPidAll, 0);
            var buffTable = Marshal.AllocHGlobal(buffSize);

            try
            {
                ret = GetExtendedTcpTable(buffTable, ref buffSize, true, afInet, TcpTableClass.TcpTableOwnerPidAll, 0);
                if (ret != 0) return null;

                // get the number of entries in the table
                //MibTcpTable tab = (MibTcpTable)Marshal.PtrToStructure(buffTable, typeof(MibTcpTable));
                var tab = (MibTcptableOwnerPid)Marshal.PtrToStructure(buffTable, typeof(MibTcptableOwnerPid));
                //IntPtr rowPtr = (IntPtr)((long)buffTable + Marshal.SizeOf(tab.numberOfEntries) );
                var rowPtr = (IntPtr)((long)buffTable + Marshal.SizeOf(tab.dwNumEntries));
                // buffer we will be returning
                //tTable = new TcpRow[tab.numberOfEntries];
                tTable = new MibTcprowOwnerPid[tab.dwNumEntries];

                //for (int i = 0; i < tab.numberOfEntries; i++)        
                for (var i = 0; i < tab.dwNumEntries; i++)
                {
                    //MibTcpRow_Owner_Pid tcpRow = (MibTcpRow_Owner_Pid)Marshal.PtrToStructure(rowPtr, typeof(MibTcpRow_Owner_Pid));
                    var tcpRow = (MibTcprowOwnerPid)Marshal.PtrToStructure(rowPtr, typeof(MibTcprowOwnerPid));
                    //tTable[i] = new TcpRow(tcpRow);
                    tTable[i] = tcpRow;
                    rowPtr = (IntPtr)((long)rowPtr + Marshal.SizeOf(tcpRow)); // next entry
                }
            }
            finally
            {
                // Free the Memory
                Marshal.FreeHGlobal(buffTable);
            }

            return tTable;
        }

        #endregion

        #region Methods

        [DllImport("iphlpapi.dll", SetLastError = true)]
        private static extern uint GetExtendedTcpTable(IntPtr pTcpTable, ref int dwOutBufLen, bool sort, int ipVersion, TcpTableClass tblClass, int reserved);

        #endregion
    }
}