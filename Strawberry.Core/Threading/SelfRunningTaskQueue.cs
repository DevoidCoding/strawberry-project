﻿// <copyright file="SelfRunningTaskQueue.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Threading
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    using global::NLog;

    using Strawberry.Core.Collections;
    using Strawberry.Core.Extensions;

    public abstract class SelfRunningTaskQueue : INotifyPropertyChanged
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Fields

        private readonly LockFreeQueue<Action> messageQueue;

        private readonly Stopwatch queueTimer;

        private readonly List<SimplerTimer> timers;

        private int currentThreadId;

        private int lastUpdate;

        private Task updateTask;

        #endregion

        #region Constructors and Destructors

        protected SelfRunningTaskQueue(int updateInterval)
        {
            timers = new List<SimplerTimer>();
            messageQueue = new LockFreeQueue<Action>();
            queueTimer = Stopwatch.StartNew();
            UpdateInterval = updateInterval;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public bool IsInContext => Thread.CurrentThread.ManagedThreadId == currentThreadId;

        public virtual string Name { get; set; }

        public bool Running { get; protected set; }

        public int UpdateInterval { get; set; }

        #endregion

        #region Public Methods and Operators

        public void AddMessage(Action action)
            => messageQueue.Enqueue(action);

        public void AddTimer(SimplerTimer timer)
            => AddMessage(() => timers.Add(timer));

        public SimplerTimer CallDelayed(int delayMillis, Action callback)
        {
            var timer = new SimplerTimer(delayMillis, 0, callback);
            timer.Start();
            AddTimer(timer);
            return timer;
        }

        public SimplerTimer CallPeriodically(int delayMillis, Action callback)
        {
            var timer = new SimplerTimer(0, delayMillis, callback);
            timer.Start();
            AddTimer(timer);
            return timer;
        }

        public void CancelAllMessages()
            => messageQueue.Clear();

        public void EnsureContext()
        {
            if (!IsInContext) throw new InvalidOperationException("Not in context");
        }

        public void EnsureNotContext()
        {
            if (IsInContext) throw new InvalidOperationException("Forbidden context");
        }

        public bool ExecuteInContext(Action action)
        {
            if (IsInContext)
            {
                action();
                return true;
            }

            AddMessage(action);
            return false;
        }

        public void RemoveTimer(SimplerTimer timer)
            => AddMessage(() => timers.Remove(timer));

        public virtual void Start()
        {
            Running = true;

            updateTask = Task.Factory.StartNewDelayed(UpdateInterval, Tick);
        }

        public virtual void Stop()
            => Running = false;

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected abstract void OnTick();

        private void Tick()
        {
            if (!Running)
                return;

            try
            {
                if (Interlocked.CompareExchange(ref currentThreadId, Thread.CurrentThread.ManagedThreadId, 0) == 0)
                {
                    var timerStart = queueTimer.ElapsedMilliseconds;
                    var updateDt = (int)(timerStart - lastUpdate);
                    lastUpdate = (int)timerStart;

                    // do stuff here

                    // process timer entries
                    foreach (var timer in timers)
                    {
                        if (!timer.IsRunning)
                        {
                            RemoveTimer(timer);
                            continue;
                        }

                        try
                        {
                            timer.Update(updateDt);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, $"Failed to update {timer}");
                        }

                        if (!timer.IsRunning)
                            RemoveTimer(timer);

                        if (!Running)
                        {
                            Interlocked.Exchange(ref currentThreadId, 0);
                            return;
                        }
                    }

                    while (messageQueue.TryDequeue(out var msg))
                    {
                        try
                        {
                            msg();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, $"Failed to execute message {msg}");
                        }

                        if (!Running)
                        {
                            Interlocked.Exchange(ref currentThreadId, 0);
                            return;
                        }
                    }

                    OnTick();

                    // get the end time
                    var timerStop = queueTimer.ElapsedMilliseconds;

                    var updateLagged = timerStop - timerStart > UpdateInterval;
                    var callbackTimeout = updateLagged ? 0 : timerStart + UpdateInterval - timerStop;

                    if (updateLagged)
                        Logger.Debug("TaskPool '{0}' update lagged ({1}ms)", (object)Name, (object)(timerStop - timerStart));

                    if (Running)
                    {
                        if (callbackTimeout < 0)
                            callbackTimeout = 50;

                        updateTask = Task.Factory.StartNewDelayed((int)callbackTimeout, Tick);
                    }

                    Interlocked.Exchange(ref currentThreadId, 0);
                }
                else Debug.WriteLine("");
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex, $"Failed to run TaskQueue callback for \"{Name}\"");
            }
        }

        #endregion
    }
}