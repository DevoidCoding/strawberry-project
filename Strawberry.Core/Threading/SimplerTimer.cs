﻿// <copyright file="SimplerTimer.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Threading
{
    using System;

    public class SimplerTimer : IDisposable
    {
        #region Fields

        private Action action;

        private int intervalMillis;

        #endregion

        #region Constructors and Destructors

        public SimplerTimer()
        {
        }

        /// <summary>
        ///     Creates a new timer with the given start delay, interval, and callback.
        /// </summary>
        /// <param name="delay">the delay before firing initially</param>
        /// <param name="intervalMillis">the interval between firing</param>
        /// <param name="callback">the callback to fire</param>
        public SimplerTimer(int delay, int intervalMillis, Action callback)
        {
            MillisSinceLastTick = -1;
            action = callback;
            RemainingTime = delay;
            this.intervalMillis = intervalMillis;
        }

        public SimplerTimer(Action callback)
            : this(0, 0, callback)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Whether or not the timer is running.
        /// </summary>
        public bool IsRunning => MillisSinceLastTick >= 0;

        /// <summary>
        ///     The amount of time in milliseconds that elapsed between the last timer tick and the last update.
        /// </summary>
        public int MillisSinceLastTick { get; private set; }

        public int RemainingTime { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Stops and cleans up the timer.
        /// </summary>
        public void Dispose()
        {
            Stop();
            action = null;
        }

        public override bool Equals(object obj)
            => obj.GetType() == typeof(SimplerTimer) && Equals((SimplerTimer)obj);

        public bool Equals(SimplerTimer obj)
            => obj.intervalMillis == intervalMillis && Equals(obj.action, action);

        public override int GetHashCode()
        {
            unchecked
            {
                var result = (intervalMillis * 397) ^ (action?.GetHashCode() ?? 0);
                return result;
            }
        }

        /// <summary>
        ///     Starts the timer.
        /// </summary>
        public void Start()
            => MillisSinceLastTick = 0;

        /// <summary>
        ///     Starts the timer with the given delay.
        /// </summary>
        /// <param name="initialDelay">the delay before firing initially</param>
        public void Start(int initialDelay)
        {
            RemainingTime = initialDelay;
            MillisSinceLastTick = 0;
        }

        /// <summary>
        ///     Starts the time with the given delay and interval.
        /// </summary>
        /// <param name="initialDelay">the delay before firing initially</param>
        /// <param name="interval">the interval between firing</param>
        public void Start(int initialDelay, int interval)
        {
            RemainingTime = initialDelay;
            intervalMillis = interval;
            MillisSinceLastTick = 0;
        }

        /// <summary>
        ///     Stops the timer.
        /// </summary>
        public void Stop()
            => MillisSinceLastTick = -1;

        /// <summary>
        ///     Updates the timer, firing the callback if enough time has elapsed.
        /// </summary>
        /// <param name="dtMillis">the time change since the last update</param>
        public void Update(int dtMillis)
        {
            // means this timer is not running.
            if (MillisSinceLastTick == -1)
                return;

            if (RemainingTime > 0)
            {
                RemainingTime -= dtMillis;

                if (RemainingTime > 0) return;

                if (intervalMillis == 0)
                {
                    // we need to stop the timer if it's only
                    // supposed to fire once.
                    Stop();
                    action();
                }
                else
                {
                    action();
                    MillisSinceLastTick = 0;
                }
            }
            else
            {
                // update our idle time
                MillisSinceLastTick += dtMillis;

                if (MillisSinceLastTick < intervalMillis) return;

                // time to tick
                action();
                if (MillisSinceLastTick != -1) MillisSinceLastTick -= intervalMillis;
            }
        }

        #endregion
    }
}