﻿// <copyright file="ProgressionCounter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.UI
{
    using System;
    using System.ComponentModel;

    public class ProgressionCounter : INotifyPropertyChanged
    {
        #region Fields

        public double EpsilonComparaison = 0.1;

        #endregion

        #region Constructors and Destructors

        public ProgressionCounter()
        {
        }

        public ProgressionCounter(int total)
            => Total = total;

        #endregion

        #region Delegates

        public delegate void ProgressionEndedHandler(ProgressionCounter counter);

        public delegate void ProgressionUpdatedHandler(ProgressionCounter counter);

        #endregion

        #region Public Events

        public event ProgressionEndedHandler Ended;

        public event PropertyChangedEventHandler PropertyChanged;

        public event ProgressionUpdatedHandler Updated;

        #endregion

        #region Public Properties

        public bool IsEnded { get; private set; }

        public string Text { get; set; }

        public int Total { get; set; }

        public double Value { get; set; }

        #endregion

        #region Public Methods and Operators

        public void NotifyEnded()
        {
            IsEnded = true;

            var handler = Ended;
            if (handler != null) handler(this);
        }

        public void UpdateValue(double value)
        {
            if (Math.Abs(Value - value) > EpsilonComparaison)
            {
                Value = value;

                var handler = Updated;
                if (handler != null) handler(this);
            }
        }

        public void UpdateValue(double value, string text)
        {
            if (Math.Abs(Value - value) > EpsilonComparaison) Value = value;

            Text = text;
            var handler = Updated;
            if (handler != null) handler(this);
        }

        #endregion

        #region Methods

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}