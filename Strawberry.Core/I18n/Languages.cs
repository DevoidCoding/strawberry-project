﻿// <copyright file="Languages.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.I18N
{
    public enum Languages
    {
        English,

        French,

        German,

        Spanish,

        Italian,

        Japanish,

        Dutsh,

        Portugese,

        Russish
    }
}