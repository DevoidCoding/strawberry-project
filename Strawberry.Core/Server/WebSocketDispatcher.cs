﻿namespace Strawberry.Core.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Strawberry.Core.Extensions;
    using Strawberry.Core.Messages;

    public class WebSocketDispatcher
    {
        #region Static Fields

        private static Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>> handlers = new Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>>();

        #endregion

        #region Public Methods and Operators

        public static void DefineHierarchy(IEnumerable<Assembly> assemblies)
        {
            var handlers = WebSocketDispatcher.handlers;
            WebSocketDispatcher.handlers = assemblies.ToDictionary(entry => entry, entry => new Dictionary<Type, List<MessageHandler>>());

            foreach (var handler in handlers)
            {
                if (!WebSocketDispatcher.handlers.ContainsKey(handler.Key))
                    WebSocketDispatcher.handlers.Add(handler.Key, new Dictionary<Type, List<MessageHandler>>());

                WebSocketDispatcher.handlers[handler.Key] = handler.Value;
            }
        }

        public static void RegisterSharedAssembly(Assembly assembly)
        {
            if (assembly == null)
                throw new ArgumentNullException(nameof(assembly));

            foreach (var type in assembly.GetTypes()) RegisterSharedStaticContainer(type);
        }

        public void Dispatch(JsonMessage message, object service)
        {
            var handlers = GetHandlers(message.GetType()).ToArray();

            foreach (var handler in handlers)
                handler.Action(null, service, message);
        }

        #endregion

        #region Methods

        protected IEnumerable<MessageHandler> GetHandlers(Type messageType)
        {
            var handlers = WebSocketDispatcher.handlers.SelectMany(e => e.Value.Where(a => a.Key == messageType).SelectMany(a => a.Value)).ToArray();

            foreach (var handler in handlers)
                yield return handler;

            if (messageType.BaseType == null || !messageType.BaseType.IsSubclassOf(typeof(JsonMessage))) yield break;

            foreach (var handler in GetHandlers(messageType.BaseType))
                if (handler.Attribute.HandleChildMessages)
                    yield return handler;
        }

        private static void RegisterShared(MethodInfo method, IReadOnlyCollection<JsonMessageHandlerAttribute> attributes)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            if (attributes == null || attributes.Count == 0)
                return;

            var parameters = method.GetParameters();

            if (parameters.Length != 2 || !parameters[1].ParameterType.IsSubclassOf(typeof(JsonMessage)))
                throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object, Message)");

            Action<object, object, JsonMessage> handlerDelegate;

            try
            {
                handlerDelegate = (Action<object, object, JsonMessage>)method.CustomCreateDelegate(typeof(object), typeof(JsonMessage));
            }
            catch (Exception)
            {
                throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object Message)");
            }

            foreach (var attribute in attributes)
                RegisterShared(attribute.Type, method.DeclaringType, attribute, handlerDelegate);
        }

        private static void RegisterShared(Type messageType, Type containerType, JsonMessageHandlerAttribute attribute, Action<object, object, JsonMessage> action)
        {
            if (attribute == null)
                throw new ArgumentNullException(nameof(attribute));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var assembly = containerType.Assembly;

            if (!handlers.ContainsKey(assembly))
                handlers.Add(assembly, new Dictionary<Type, List<MessageHandler>>());

            if (!handlers[assembly].ContainsKey(messageType))
                handlers[assembly].Add(messageType, new List<MessageHandler>());

            handlers[assembly][messageType].Add(new MessageHandler(messageType, attribute, action));
        }

        private static void RegisterSharedStaticContainer(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var method in methods)
            {
                if (!(method.GetCustomAttributes(typeof(JsonMessageHandlerAttribute), false) is JsonMessageHandlerAttribute[] attributes) || attributes.Length == 0)
                    continue;

                RegisterShared(method, attributes);
            }
        }

        #endregion

        protected class MessageHandler
        {
            #region Constructors and Destructors

            public MessageHandler(Type messageType, JsonMessageHandlerAttribute attribute, Action<object, object, JsonMessage> action)
            {
                MessageType = messageType;
                Attribute = attribute;
                Action = action;
            }

            #endregion

            #region Public Properties

            public Action<object, object, JsonMessage> Action { get; set; }

            public JsonMessageHandlerAttribute Attribute { get; set; }

            public Type MessageType { get; set; }

            #endregion
        }
    }
}