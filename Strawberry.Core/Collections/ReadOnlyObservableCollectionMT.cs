﻿// <copyright file="ReadOnlyObservableCollectionMT.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Collections
{
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows.Threading;

    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Reviewed. Suppression is OK here.")]
    public class ReadOnlyObservableCollectionMT<T> : ReadOnlyObservableCollection<T>, INotifyCollectionChanged
    {
        #region Constructors and Destructors

        public ReadOnlyObservableCollectionMT(ObservableCollection<T> list)
            : base(list)
        {
        }

        #endregion

        #region Explicit Interface Events

        event NotifyCollectionChangedEventHandler INotifyCollectionChanged.CollectionChanged
        {
            add => CollectionChanged += value;
            remove => CollectionChanged -= value;
        }

        #endregion

        #region Events

        /// <inheritdoc />
        // ReSharper disable once StyleCop.SA1202
        protected override event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region Methods

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            var eventHandler = CollectionChanged;

            if (eventHandler == null) return;
            var delegates = eventHandler.GetInvocationList();

            // Walk thru invocation list
            foreach (var handler in delegates.OfType<NotifyCollectionChangedEventHandler>())
                if (handler.Target is DispatcherObject dispatcherObject && dispatcherObject.CheckAccess() == false)
                    dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, args);
                else
                    handler(this, args);
        }

        #endregion
    }
}