﻿// <copyright file="ObservableCollectionMT.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Collections
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows.Threading;

    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Reviewed. Suppression is OK here.")]
    public class ObservableCollectionMT<T> : ObservableCollection<T>
    {
        #region Constructors and Destructors

        public ObservableCollectionMT()
        {
        }

        public ObservableCollectionMT(List<T> list)
            : base(list)
        {
        }

        public ObservableCollectionMT(IEnumerable<T> collection)
            : base(collection)
        {
        }

        #endregion

        #region Public Events

        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region Methods

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            // Be nice - use BlockReentrancy like MSDN said
            using (BlockReentrancy())
            {
                var eventHandler = CollectionChanged;

                if (eventHandler == null) return;

                var delegates = eventHandler.GetInvocationList();

                // Walk thru invocation list
                foreach (var handler in delegates.Cast<NotifyCollectionChangedEventHandler>())
                    if (handler.Target is DispatcherObject dispatcherObject && dispatcherObject.CheckAccess() == false)
                        dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, e);
                    else
                        handler(this, e);
            }
        }

        #endregion
    }
}