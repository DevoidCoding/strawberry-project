// <copyright file="PriorityQueueB.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Collections
{
    using System.Collections;
    using System.Collections.Generic;

    public interface IPriorityQueue<T>
    {
        #region Public Methods and Operators

        T Peek();

        T Pop();

        int Push(T item);

        void Update(int i);

        #endregion
    }

    public class PriorityQueueB<T> : IPriorityQueue<T>, IEnumerable<T>
    {
        #region Fields

        private readonly IComparer<T> comparer;

        private readonly List<T> innerList = new List<T>();

        #endregion

        #region Constructors and Destructors

        public PriorityQueueB()
            => comparer = Comparer<T>.Default;

        public PriorityQueueB(IComparer<T> comparer)
            => this.comparer = comparer;

        public PriorityQueueB(IComparer<T> comparer, int capacity)
        {
            this.comparer = comparer;
            innerList.Capacity = capacity;
        }

        #endregion

        #region Public Properties

        public int Count => innerList.Count;

        #endregion

        #region Public Indexers

        public T this[int index]
        {
            get => innerList[index];
            set
            {
                innerList[index] = value;
                Update(index);
            }
        }

        #endregion

        #region Public Methods and Operators

        public void Clear()
            => innerList.Clear();

        public IEnumerator<T> GetEnumerator()
            => innerList.GetEnumerator();

        /// <summary>
        ///     Get the smallest object without removing it.
        /// </summary>
        /// <returns>The smallest object</returns>
        public T Peek()
            => innerList.Count > 0 ? innerList[0] : default;

        /// <summary>
        ///     Get the smallest object and remove it.
        /// </summary>
        /// <returns>The smallest object</returns>
        public T Pop()
        {
            var result = innerList[0];
            var p = 0;
            innerList[0] = innerList[innerList.Count - 1];
            innerList.RemoveAt(innerList.Count - 1);

            do
            {
                var pn = p;
                var p1 = 2 * p + 1;
                var p2 = 2 * p + 2;

                if (innerList.Count > p1 && OnCompare(p, p1) > 0)
                    p = p1;

                if (innerList.Count > p2 && OnCompare(p, p2) > 0)
                    p = p2;

                if (p == pn)
                    break;

                SwitchElements(p, pn);
            }
            while (true);

            return result;
        }

        /// <summary>
        ///     Push an object onto the PQ
        /// </summary>
        /// <param name="item">The new object</param>
        /// <returns>
        ///     The index in the list where the object is _now_. This will change when objects are taken from or put onto the
        ///     PQ.
        /// </returns>
        public int Push(T item)
        {
            var p = innerList.Count;
            innerList.Add(item); // E[p] = O

            do
            {
                if (p == 0)
                    break;

                var p2 = (p - 1) / 2;

                if (OnCompare(p, p2) < 0)
                {
                    SwitchElements(p, p2);
                    p = p2;
                }
                else
                    break;
            }
            while (true);

            return p;
        }

        public void Remove(T item)
        {
            var index = -1;

            for (var i = 0; i < innerList.Count; i++)
                if (comparer.Compare(innerList[i], item) == 0)
                    index = i;

            if (index != -1)
                innerList.RemoveAt(index);
        }

        /// <summary>
        ///     Notify the PQ that the object at position i has changed
        ///     and the PQ needs to restore order.
        ///     Since you dont have access to any indexes (except by using the
        ///     explicit IList.this) you should not call this function without knowing exactly
        ///     what you do.
        /// </summary>
        /// <param name="i">The index of the changed object.</param>
        public void Update(int i)
        {
            var p = i;
            int p2;

            do
            {
                if (p == 0)
                    break;

                p2 = (p - 1) / 2;

                if (OnCompare(p, p2) < 0)
                {
                    SwitchElements(p, p2);
                    p = p2;
                }
                else
                    break;
            }
            while (true);

            if (p < i)
                return;

            do
            {
                var pn = p;
                var p1 = 2 * p + 1;
                p2 = 2 * p + 2;

                if (innerList.Count > p1 && OnCompare(p, p1) > 0)
                    p = p1;

                if (innerList.Count > p2 && OnCompare(p, p2) > 0)
                    p = p2;

                if (p == pn)
                    break;

                SwitchElements(p, pn);
            }
            while (true);
        }

        #endregion

        #region Explicit Interface Methods

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        #endregion

        #region Methods

        protected virtual int OnCompare(int i, int j)
            => comparer.Compare(innerList[i], innerList[j]);

        protected void SwitchElements(int i, int j)
        {
            var h = innerList[i];
            innerList[i] = innerList[j];
            innerList[j] = h;
        }

        #endregion
    }
}