﻿// <copyright file="LockFreeQueue.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>


/* LockFreeQueue copyright (c) julian m bucknall
 * http://www.boyet.com/Articles/LockfreeQueue.html
*/

namespace Strawberry.Core.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;

    internal class SingleLinkNode<T>
    {
        #region Fields

        public T Item;

        public SingleLinkNode<T> Next;

        #endregion
    }

    public class LockFreeQueue<T> : IEnumerable<T>
    {
        #region Fields

        private int count;

        private SingleLinkNode<T> head;

        private SingleLinkNode<T> tail;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        public LockFreeQueue()
        {
            head = new SingleLinkNode<T>();
            tail = head;
        }

        public LockFreeQueue(IEnumerable<T> items)
            : this()
        {
            foreach (var item in items) Enqueue(item);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the number of elements contained in the queue.
        /// </summary>
        public int Count => Thread.VolatileRead(ref count);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Clears the queue.
        /// </summary>
        /// <remarks>This method is not thread-safe.</remarks>
        public void Clear()
        {
            var currentNode = head;

            while (currentNode != null)
            {
                var tempNode = currentNode;
                currentNode = currentNode.Next;

                tempNode.Item = default;
                tempNode.Next = null;
            }

            head = new SingleLinkNode<T>();
            tail = head;
            count = 0;
        }

        /// <summary>
        ///     Removes and returns the object at the beginning of the queue.
        /// </summary>
        /// <returns>the object that is removed from the beginning of the queue</returns>
        public T Dequeue()
        {
            if (!TryDequeue(out var result)) throw new InvalidOperationException("the queue is empty");

            return result;
        }

        /// <summary>
        ///     Adds an object to the end of the queue.
        /// </summary>
        /// <param name="item">the object to add to the queue</param>
        public void Enqueue(T item)
        {
            SingleLinkNode<T> oldTail = null;

            var newNode = new SingleLinkNode<T> { Item = item };

            var newNodeWasAdded = false;

            while (!newNodeWasAdded)
            {
                oldTail = tail;
                var oldTailNext = oldTail.Next;

                if (tail != oldTail) continue;

                if (oldTailNext == null)
                    newNodeWasAdded = Interlocked.CompareExchange(ref tail.Next, newNode, null) == null;
                else
                    Interlocked.CompareExchange(ref tail, oldTailNext, oldTail);
            }

            Interlocked.CompareExchange(ref tail, newNode, oldTail);
            Interlocked.Increment(ref count);
        }

        #region IEnumerable<T> Members

        /// <inheritdoc />
        /// <summary>
        ///     Returns an enumerator that iterates through the queue.
        /// </summary>
        /// <returns>an enumerator for the queue</returns>
        public IEnumerator<T> GetEnumerator()
        {
            var currentNode = head;

            do
            {
                if (currentNode.Item == null) yield break;
                yield return currentNode.Item;
            }
            while ((currentNode = currentNode.Next) != null);
        }

        #endregion

        public T TryDequeue()
        {
            TryDequeue(out var item);
            return item;
        }

        /// <summary>
        ///     Removes and returns the object at the beginning of the queue.
        /// </summary>
        /// <param name="item">
        ///     when the method returns, contains the object removed from the beginning of the queue,
        ///     if the queue is not empty; otherwise it is the default value for the element type
        /// </param>
        /// <returns>
        ///     true if an object from removed from the beginning of the queue;
        ///     false if the queue is empty
        /// </returns>
        public bool TryDequeue(out T item)
        {
            item = default;

            var haveAdvancedHead = false;

            while (!haveAdvancedHead)
            {
                var oldHead = head;
                var oldTail = tail;
                var oldHeadNext = oldHead.Next;

                if (oldHead != head) continue;

                if (oldHead == oldTail)
                {
                    if (oldHeadNext == null)
                        return false;

                    Interlocked.CompareExchange(ref tail, oldHeadNext, oldTail);
                }

                else
                {
                    item = oldHeadNext.Item;
                    haveAdvancedHead = Interlocked.CompareExchange(ref head, oldHeadNext, oldHead) == oldHead;
                }
            }

            Interlocked.Decrement(ref count);
            return true;
        }

        #endregion

        #region Explicit Interface Methods

        #region IEnumerable Members

        /// <inheritdoc />
        /// <summary>
        ///     Returns an enumerator that iterates through the queue.
        /// </summary>
        /// <returns>an enumerator for the queue</returns>
        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        #endregion

        #endregion
    }
}