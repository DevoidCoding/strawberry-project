﻿// <copyright file="Config.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Config
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using System.Xml.XPath;

    public class Config
    {
        #region Static Fields

        private static readonly List<Tuple<Assembly, Config>> BoundsAssemblies = new List<Tuple<Assembly, Config>>();

        #endregion

        #region Constructors and Destructors

        public Config(string filePath)
            => FilePath = filePath;

        #endregion

        #region Public Properties

        public string FilePath { get; }

        public List<ConfigNode> Nodes { get; } = new List<ConfigNode>();

        #endregion

        #region Public Methods and Operators

        public static Config GetAssemblyConfig(Assembly assembly)
        {
            var configs = BoundsAssemblies.Where(entry => entry.Item1 == assembly).ToArray();

            if (configs.Length > 1)
                throw new Exception($"Found {configs.Length} configs bound to assembly {assembly}");

            if (configs.Length <= 0)
                throw new Exception($"No config bound to assembly {assembly}");

            return configs.Single().Item2;
        }

        public static Config GetCurrentClassConfig()
        {
            var assembly = Assembly.GetCallingAssembly();

            return GetAssemblyConfig(assembly);
        }

        public static T GetStatic<T>(string key)
        {
            var config = GetAssemblyConfig(Assembly.GetCallingAssembly());

            return config.Get<T>(key);
        }

        public static T GetStatic<T>(string key, T defaultValue)
        {
            var config = GetAssemblyConfig(Assembly.GetCallingAssembly());

            return config.Get(key, defaultValue);
        }

        public static void SetStatic<T>(string key, T value)
        {
            var config = GetAssemblyConfig(Assembly.GetCallingAssembly());
            config.Set(key, value);
        }

        public void BindAssembly(Assembly assembly)
            => BoundsAssemblies.Add(Tuple.Create(assembly, this));

        public bool Exists(string nodeName)
            => Nodes.Any(entry => entry.Name == nodeName);

        public T Get<T>(string key)
        {
            var node = GetNode(key);

            if (node == null)
                throw new KeyNotFoundException($"Node {key} not found");

            return (T)node.GetValue(typeof(T));
        }

        public T Get<T>(string key, T defaultValue)
        {
            var node = GetNode(key);

            if (node == null)
            {
                Nodes.Add(new ConfigNode(key, defaultValue));

                return defaultValue;
            }

            return (T)node.GetValue(typeof(T));
        }

        public ConfigNode GetNode(string name)
        {
            var nodes = Nodes.Where(entry => entry.Name == name).ToArray();

            if (nodes.Length > 1)
                throw new Exception($"Found {nodes.Length} nodes with name {name}, a node name must be unique");

            return nodes.SingleOrDefault();
        }

        public void Load()
        {
            if (!File.Exists(FilePath))
                Save(); // create the config file

            var document = new XmlDocument();
            document.Load(FilePath);

            var navigator = document.CreateNavigator();
            var listedNames = new List<string>();

            foreach (XPathNavigator iterator in navigator.Select("//" + ConfigNode.NodeName + "[@" + ConfigNode.AttributeName + "]"))
            {
                if (!iterator.IsNode)
                    continue;

                var xmlNode = ((IHasXmlNode)iterator).GetNode();
                var name = ConfigNode.GetNodeName(xmlNode);
                var node = GetNode(name);

                if (listedNames.Contains(name))
                    throw new Exception($"Node with name {name} already used, a node name must be unique");

                // if the noad already exist we set the value
                if (node != null) node.Load(xmlNode);
                else
                {
                    node = new ConfigNode(xmlNode);
                    Nodes.Add(node);
                }

                listedNames.Add(name);
            }
        }

        public void RegisterAttributes()
            => RegisterAttributes(Assembly.GetCallingAssembly());

        public void RegisterAttributes(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                foreach (var field in type.GetFields())
                {
                    var attr = field.GetCustomAttribute<ConfigurableAttribute>();

                    if (attr == null)
                        continue;

                    if (Exists(attr.Name))
                        throw new Exception($"Node with name {attr.Name} already used, a node name must be unique");

                    Nodes.Add(new ConfigVariable(attr, field));
                }

                foreach (var property in type.GetProperties())
                {
                    var attr = property.GetCustomAttribute<ConfigurableAttribute>();

                    if (attr == null)
                        continue;

                    if (Exists(attr.Name))
                        throw new Exception($"Node with name {attr.Name} already used, a node name must be unique");

                    Nodes.Add(new ConfigVariable(attr, property));
                }
            }
        }

        public void Reload()
        {
            Nodes.Clear();
            Load();
        }

        public void Save()
        {
            var writer = XmlWriter.Create(FilePath, new XmlWriterSettings { Indent = true, IndentChars = " ", Encoding = Encoding.UTF8 });

            writer.WriteStartDocument();
            writer.WriteStartElement("Configuration");

            foreach (var node in Nodes) node.Save(writer);

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Close();
        }

        public void Set<T>(string key, T value)
        {
            var node = GetNode(key);

            if (node == null)
                throw new KeyNotFoundException($"Node {key} not found");

            node.SetValue(value);
        }

        public void UnBindAssembly(Assembly assembly)
            => BoundsAssemblies.RemoveAll(entry => entry.Item1 == assembly);

        #endregion
    }
}