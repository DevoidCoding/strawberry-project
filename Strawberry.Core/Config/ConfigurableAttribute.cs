﻿// <copyright file="ConfigurableAttribute.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Config
{
    using System;

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ConfigurableAttribute : Attribute
    {
        #region Constructors and Destructors

        public ConfigurableAttribute(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name");

            Name = name;
        }

        public ConfigurableAttribute(string name, string comment)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name");

            Name = name;
            Comment = comment ?? throw new ArgumentNullException(nameof(comment));
        }

        #endregion

        #region Public Properties

        public string Comment { get; set; }

        public string Name { get; }

        #endregion
    }
}