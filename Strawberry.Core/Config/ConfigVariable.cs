﻿// <copyright file="ConfigVariable.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Config
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    public class ConfigVariable : ConfigNode
    {
        #region Constants

        public const string CommentAttribute = "comment";

        #endregion

        #region Constructors and Destructors

        public ConfigVariable(ConfigurableAttribute attribute, FieldInfo field)
        {
            if (field == null) throw new ArgumentNullException(nameof(field));
            Attribute = attribute ?? throw new ArgumentNullException(nameof(attribute));
            Field = field;
        }

        public ConfigVariable(ConfigurableAttribute attribute, PropertyInfo property)
        {
            if (property == null) throw new ArgumentNullException(nameof(property));
            Attribute = attribute ?? throw new ArgumentNullException(nameof(attribute));
            Property = property;
        }

        #endregion

        #region Public Properties

        public ConfigurableAttribute Attribute { get; }

        public FieldInfo Field { get; }

        public override string Name
        {
            get => Attribute.Name;
            set => throw new InvalidOperationException("Cannot set an attribute name");
        }

        public PropertyInfo Property { get; }

        public Type VariableType => Field != null ? Field.FieldType : Property.PropertyType;

        #endregion

        #region Public Methods and Operators

        public override object GetValue(Type type)
            => GetValue();

        public object GetValue()
        {
            if (Field != null)
            {
                if (!Field.IsStatic)
                    throw new Exception($"Cannot get a non-static field ({Field.Name}) in a static context (variable={Attribute.Name})");

                return Field.GetValue(null);
            }

            if (Property != null)
            {
                var method = Property.GetGetMethod(true);

                if (method == null)
                    throw new Exception($"Property {Property.Name} has no set method (variable={Attribute.Name})");

                if (!method.IsStatic)
                    throw new Exception($"Cannot get a non-static property ({Property.Name}) in a static context (variable={Attribute.Name})");

                return Property.GetValue(null, new object[0]);
            }

            throw new Exception($"No property and no field bound to the variable {Attribute.Name}");
        }

        public object GetValue(object instance)
        {
            if (Field != null)
            {
                if (Field.IsStatic)
                    throw new Exception($"Cannot get a static field ({Field.Name}) in a non-static context (variable={Attribute.Name})");

                return Field.GetValue(instance);
            }

            if (Property != null)
            {
                var method = Property.GetSetMethod(true);

                if (method == null)
                    throw new Exception($"Property {Property.Name} has no set method (variable={Attribute.Name})");

                if (method.IsStatic)
                    throw new Exception($"Cannot get a static property ({Property.Name}) in a non-static context (variable={Attribute.Name})");

                return Property.GetValue(instance, new object[0]);
            }

            throw new Exception($"No property and no field bound to the variable {Attribute.Name}");
        }

        public override void SetValue(object value)
        {
            if (Field != null)
            {
                if (!Field.IsStatic)
                    throw new Exception($"Cannot assign a non-static field ({Field.Name}) in a static context (variable={Attribute.Name})");

                Field.SetValue(null, value);
            }
            else if (Property != null)
            {
                var method = Property.GetSetMethod(true);

                if (method == null)
                    throw new Exception($"Property {Property.Name} has no set method (variable={Attribute.Name})");

                if (!method.IsStatic)
                    throw new Exception($"Cannot assign a non-static property ({Property.Name}) in a static context (variable={Attribute.Name})");

                Property.SetValue(null, value, new object[0]);
            }
            else throw new Exception($"No property and no field bound to the variable {Attribute.Name}");

            IsSynchronised = false;
        }

        public void SetValue(object value, object instance)
        {
            if (Field != null)
            {
                if (Field.IsStatic)
                    throw new Exception($"Cannot assign a static field ({Field.Name}) in a non-static context (variable={Attribute.Name})");

                Field.SetValue(instance, value);
            }
            else if (Property != null)
            {
                var method = Property.GetSetMethod(true);

                if (method == null)
                    throw new Exception($"Property {Property.Name} has no set method (variable={Attribute.Name})");

                if (method.IsStatic)
                    throw new Exception($"Cannot assign a static property ({Property.Name}) in a non-static context (variable={Attribute.Name})");

                Property.SetValue(instance, value, new object[0]);
            }
            else throw new Exception($"No property and no field bound to the variable {Attribute.Name}");

            IsSynchronised = false;
        }

        #endregion

        #region Methods

        internal override void Load(XmlNode node)
        {
            SetValue(new XmlSerializer(VariableType).Deserialize(new StringReader(node.InnerXml)));
            IsSynchronised = true;
        }

        internal override void Save(XmlWriter writer)
        {
            if (writer == null) throw new ArgumentNullException(nameof(writer));

            writer.WriteStartElement(NodeName);
            writer.WriteAttributeString(AttributeName, Name);

            if (!string.IsNullOrEmpty(Attribute.Comment))
                writer.WriteAttributeString(CommentAttribute, Attribute.Comment);

            var value = GetValue();
            new XmlSerializer(value.GetType()).Serialize(writer, value);

            IsSynchronised = true;

            writer.WriteEndElement();
        }

        #endregion
    }
}