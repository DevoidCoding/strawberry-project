﻿// <copyright file="ConfigNode.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Config
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    public class ConfigNode
    {
        #region Constants

        public const string AttributeName = "name";

        public const string NodeName = "Key";

        #endregion

        #region Fields

        private string name;

        private XmlNode node;

        private object value;

        #endregion

        #region Constructors and Destructors

        public ConfigNode(XmlNode node)
        {
            if (node == null) throw new ArgumentNullException(nameof(node));

            Load(node);
        }

        public ConfigNode(string name, object value)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.value = value ?? throw new ArgumentNullException(nameof(value));
        }

        protected ConfigNode()
        {
        }

        #endregion

        #region Public Properties

        public bool IsSynchronised { get; protected set; }

        public virtual string Name
        {
            get => name;
            set => name = value;
        }

        #endregion

        #region Public Methods and Operators

        public static string GetNodeName(XmlNode node)
        {
            if (node.Attributes?[AttributeName] == null)
                throw new Exception($"Attribute {AttributeName} not found");

            return node.Attributes[AttributeName].Value;
        }

        public virtual object GetValue(Type type)
        {
            if (node == null) return value;
            value = new XmlSerializer(type).Deserialize(new StringReader(node.InnerXml));
            node = null;
            IsSynchronised = true;

            return value;
        }

        // ReSharper disable once ParameterHidesMember
        public virtual void SetValue(object value)
        {
            this.value = value;
            IsSynchronised = false;
        }

        #endregion

        #region Methods

        // ReSharper disable once ParameterHidesMember
        internal virtual void Load(XmlNode node)
        {
            name = GetNodeName(node);
            this.node = node;
        }

        internal virtual void Save(XmlWriter writer)
        {
            if (writer == null) throw new ArgumentNullException(nameof(writer));

            writer.WriteStartElement(NodeName);
            writer.WriteAttributeString(AttributeName, Name);

            if (value != null)
                new XmlSerializer(value.GetType()).Serialize(writer, value);

            IsSynchronised = true;

            writer.WriteEndElement();
        }

        #endregion
    }
}