﻿// <copyright file="DispatcherTask.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System.Threading.Tasks;

    public class DispatcherTask
    {
        #region Constructors and Destructors

        public DispatcherTask(MessageDispatcher dispatcher)
        {
            Dispatcher = dispatcher;
            Processor = this;
        }

        public DispatcherTask(MessageDispatcher dispatcher, object processor)
        {
            Dispatcher = dispatcher;
            Processor = processor;
        }

        #endregion

        #region Public Properties

        public MessageDispatcher Dispatcher { get; }

        public object Processor { get; set; }

        public bool Running { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Start()
        {
            Running = true;
            Task.Factory.StartNew(Process);
        }

        public void Stop()
            => Running = false;

        #endregion

        #region Methods

        private void Process()
        {
            while (Running)
            {
                Dispatcher.Wait();

                if (Running)
                    Dispatcher.ProcessDispatching(Processor);
            }
        }

        #endregion
    }
}