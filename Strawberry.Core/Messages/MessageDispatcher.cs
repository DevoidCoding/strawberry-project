﻿// <copyright file="MessageDispatcher.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading;

    using global::NLog;

    using Strawberry.Core.Extensions;

    /// <summary>
    ///     Classic message dispatcher
    /// </summary>
    /// <typeparam>Messsage dispatcher type</typeparam>
    public class MessageDispatcher
    {
        #region Static Fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>> handlers = new Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>>();

        #endregion

        #region Fields

        private readonly ManualResetEventSlim messageEnqueuedEvent = new ManualResetEventSlim(false);

        private readonly SortedDictionary<MessagePriority, Queue<Tuple<Message, object>>> messagesToDispatch = new SortedDictionary<MessagePriority, Queue<Tuple<Message, object>>>();

        private readonly Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>> nonSharedHandlers = new Dictionary<Assembly, Dictionary<Type, List<MessageHandler>>>();

        private readonly ManualResetEventSlim resumeEvent = new ManualResetEventSlim(true);

        private int currentThreadId;

        private bool dispatching;

        private Stopwatch spy;

        #endregion

        #region Constructors and Destructors

        public MessageDispatcher()
        {
            foreach (var value in Enum.GetValues(typeof(MessagePriority)))
                messagesToDispatch.Add((MessagePriority)value, new Queue<Tuple<Message, object>>());
        }

        #endregion

        #region Public Events

        public event Action<MessageDispatcher, Message> MessageDispatched;

        #endregion

        #region Public Properties

        public object CurrentProcessor { get; private set; }

        public int CurrentThreadId => currentThreadId;

        /// <summary>
        ///     Says how many milliseconds elapsed since last message.
        /// </summary>
        public long DelayFromLastMessage
        {
            get
            {
                if (spy == null)
                    spy = Stopwatch.StartNew();

                return spy.ElapsedMilliseconds;
            }
        }

        public bool Stopped { get; private set; }

        #endregion

        #region Public Methods and Operators

        public static void DefineHierarchy(IEnumerable<Assembly> assemblies)
        {
            var handlers = MessageDispatcher.handlers;
            MessageDispatcher.handlers = assemblies.ToDictionary(entry => entry, entry => new Dictionary<Type, List<MessageHandler>>());

            // assembly that are first are the basic ones
            // the handlers in this assemblies are called first
            foreach (var handler in handlers)
            {
                if (!MessageDispatcher.handlers.ContainsKey(handler.Key))
                    MessageDispatcher.handlers.Add(handler.Key, new Dictionary<Type, List<MessageHandler>>());

                MessageDispatcher.handlers[handler.Key] = handler.Value;
            }
        }

        public static void RegisterSharedAssembly(Assembly assembly)
        {
            if (assembly == null)
                throw new ArgumentNullException(nameof(assembly));

            foreach (var type in assembly.GetTypes())
                RegisterSharedStaticContainer(type);
        }

        public static void RegisterSharedContainer(object container)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            var type = container.GetType();

            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var method in methods)
            {
                if (!(method.GetCustomAttributes(typeof(MessageHandlerAttribute), false) is MessageHandlerAttribute[] attributes) || attributes.Length == 0)
                    continue;

                RegisterShared(method, container, attributes);
            }
        }

        public static void RegisterSharedStaticContainer(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var method in methods)
            {
                var attributes = method.GetCustomAttributes(typeof(MessageHandlerAttribute), false) as MessageHandlerAttribute[];

                if (attributes == null || attributes.Length == 0)
                    continue;

                RegisterShared(method, null, attributes);
            }
        }

        public static void UnRegisterShared(Type messageType)
        {
            foreach (var keyPair in handlers)
            foreach (var handler in keyPair.Value)
                if (handler.Key == messageType)
                    handler.Value.Clear();
        }

        public static void UnRegisterSharedAssembly(Assembly assembly)
            => handlers.Remove(assembly);

        public static void UnRegisterSharedContainer(Type containerType)
        {
            foreach (var keyPair in handlers[containerType.Assembly])
                keyPair.Value.RemoveAll(entry => entry.ContainerType == containerType);
        }

        public void Dispose()
        {
            Stop();

            foreach (var messages in messagesToDispatch)
                messages.Value.Clear();
        }

        public void Enqueue(Message message, bool executeIfCan = true)
            => Enqueue(message, null, executeIfCan);

        public virtual void Enqueue(Message message, object token, bool executeIfCan = true)
        {
            lock (messageEnqueuedEvent)
            {
                messagesToDispatch[message.Priority].Enqueue(Tuple.Create(message, token));

                if (!dispatching)
                    messageEnqueuedEvent.Set();
            }
        }

        public bool HasNonSharedContainer(Type type)
            => nonSharedHandlers.Any(assembly => assembly.Value.Any(x => x.Value.Any(handler => handler.ContainerType == type)));

        public bool IsInDispatchingContext()
            => Thread.CurrentThread.ManagedThreadId == currentThreadId && CurrentProcessor != null;

        public bool IsRegistered(Type messageType)
            => handlers.Concat(nonSharedHandlers).Any(entry => entry.Value.ContainsKey(messageType));

        public void ProcessDispatching(object processor)
        {
            if (Stopped)
                return;

            if (Interlocked.CompareExchange(ref currentThreadId, Thread.CurrentThread.ManagedThreadId, 0) == 0)
            {
                CurrentProcessor = processor;
                dispatching = true;

                var copy = messagesToDispatch.ToArray();

                foreach (var keyPair in copy)
                {
                    if (Stopped)
                        break;

                    while (keyPair.Value.Count != 0)
                    {
                        if (Stopped)
                            break;

                        var message = keyPair.Value.Dequeue();

                        if (message != null)
                            Dispatch(message.Item1, message.Item2);
                    }
                }

                CurrentProcessor = null;
                dispatching = false;
                Interlocked.Exchange(ref currentThreadId, 0);
            }

            lock (messagesToDispatch)
            {
                if (messagesToDispatch.Sum(x => x.Value.Count) > 0)
                    messageEnqueuedEvent.Set();
                else
                    messageEnqueuedEvent.Reset();
            }
        }

        public void RegisterNonShared(object handler)
        {
            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            var type = handler.GetType();

            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var method in methods)
            {
                var attributes = method.GetCustomAttributes(typeof(MessageHandlerAttribute), false) as MessageHandlerAttribute[];

                if (attributes == null || attributes.Length == 0)
                    continue;

                RegisterNonShared(method, handler, attributes);
            }
        }

        public void Resume()
        {
            if (!Stopped)
                return;

            Stopped = false;
            resumeEvent.Set();
        }

        public void Stop()
        {
            if (Stopped)
                return;

            Stopped = true;
            resumeEvent.Reset();
        }

        public void UnRegisterNonShared(Type type)
        {
            foreach (var dict in nonSharedHandlers.Values)
            {
                var handlers = dict.Values; // copy

                foreach (var handler in handlers)
                    handler.RemoveAll(entry => entry.ContainerType == type);
            }
        }

        public void UnRegisterNonShared(object container)
        {
            foreach (var dict in nonSharedHandlers.Values)
            {
                var handlers = dict.Values; // copy

                foreach (var handler in handlers)
                    handler.RemoveAll(entry => entry.Container == container);
            }
        }

        /// <summary>
        ///     Block the current thread until a message is enqueued
        /// </summary>
        public void Wait()
        {
            if (Stopped)
                resumeEvent.Wait();

            if (messagesToDispatch.Sum(x => x.Value.Count) > 0)
                return;

            messageEnqueuedEvent.Wait();
        }

        #endregion

        #region Methods

        protected static void UnRegisterShared(MessageHandler handler)
            => handlers[handler.ContainerType.Assembly][handler.MessageType].Remove(handler);

        protected virtual void Dispatch(Message message, object token)
        {
            try
            {
                foreach (var handler in GetHandlers(message.GetType(), token).ToArray())
                {
                    handler.Action(handler.Container, token, message);

                    if (message.Canceled)
                        break;
                }

                message.OnDispatched();
                OnMessageDispatched(message);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot dispatch {message}", ex);
            }
        }

        protected IEnumerable<MessageHandler> GetHandlers(Type messageType, object token)
        {
            foreach (var list in nonSharedHandlers.Values.Concat(handlers.Values).ToArray())
            {
                if (!list.TryGetValue(messageType, out var handlersList))
                    continue;

                foreach (var handler in handlersList)
                    if (token == null || handler.TokenType.IsInstanceOfType(token))
                        yield return handler;
            }

            if (messageType.BaseType == null || !messageType.BaseType.IsSubclassOf(typeof(Message))) yield break;

            {
                foreach (var handler in GetHandlers(messageType.BaseType, token))
                    if (handler.Attribute.HandleChildMessages)
                        yield return handler;
            }
        }

        protected void OnMessageDispatched(Message message)
        {
            ActivityUpdate();
            MessageDispatched?.Invoke(this, message);
        }

        private static void RegisterShared(MethodInfo method, object container)
        {
            var attributes = method.GetCustomAttributes(typeof(MessageHandlerAttribute), false) as MessageHandlerAttribute[];

            if (attributes == null || attributes.Length == 0)
                throw new Exception("A handler method must have a at least one MessageHandler attribute");

            RegisterShared(method, container, attributes);
        }

        private static void RegisterShared(MethodInfo method, object container, params MessageHandlerAttribute[] attributes)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            if (attributes == null || attributes.Length == 0)
                return;

            var parameters = method.GetParameters();

            if (parameters.Length != 2 || !parameters[1].ParameterType.IsSubclassOf(typeof(Message))) throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object, Message)");

            if (!method.IsStatic && container == null || method.IsStatic && container != null)
                return;

            Action<object, object, Message> handlerDelegate;

            try
            {
                handlerDelegate = (Action<object, object, Message>)method.CustomCreateDelegate(typeof(object), typeof(Message));
            }
            catch (Exception)
            {
                throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object Message)");
            }

            foreach (var attribute in attributes)
                RegisterShared(attribute.MessageType, method.DeclaringType, attribute, handlerDelegate, parameters[0].ParameterType, method.IsStatic ? null : container);
        }

        private static void RegisterShared(Type messageType, Type containerType, MessageHandlerAttribute attribute, Action<object, object, Message> action, Type tokenType, object container = null)
        {
            if (attribute == null)
                throw new ArgumentNullException(nameof(attribute));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var assembly = containerType.Assembly;

            // handlers are organized by assemblies to build an hierarchie
            // if the assembly is not registered yet we add it to the end
            if (!handlers.ContainsKey(assembly))
                handlers.Add(assembly, new Dictionary<Type, List<MessageHandler>>());

            if (!handlers[assembly].ContainsKey(messageType))
                handlers[assembly].Add(messageType, new List<MessageHandler>());

            handlers[assembly][messageType].Add(new MessageHandler(container, containerType, messageType, attribute, action, tokenType));
        }

        /// <summary>
        ///     Reset timer for last received message
        /// </summary>
        private void ActivityUpdate()
        {
            if (spy == null)
                spy = Stopwatch.StartNew();
            else
                spy.Restart();
        }

        private void RegisterNonShared(MethodInfo method, object container, params MessageHandlerAttribute[] attributes)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            if (attributes == null || attributes.Length == 0)
                return;

            var parameters = method.GetParameters();

            if (parameters.Length != 2 || !parameters[1].ParameterType.IsSubclassOf(typeof(Message)))
                throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object, Message)");

            if (!method.IsStatic && container == null || method.IsStatic && container != null)
                return;

            Action<object, object, Message> handlerDelegate;

            try
            {
                handlerDelegate = (Action<object, object, Message>)method.CustomCreateDelegate(typeof(object), typeof(Message));
            }
            catch (Exception)
            {
                throw new ArgumentException($"Method handler {method} has incorrect parameters. Right definition is Handler(object Message)");
            }

            foreach (var attribute in attributes)
                RegisterNonShared(attribute.MessageType, method.DeclaringType, attribute, handlerDelegate, parameters[0].ParameterType, method.IsStatic ? null : container);
        }

        private void RegisterNonShared(Type messageType, Type containerType, MessageHandlerAttribute attribute, Action<object, object, Message> action, Type tokenType, object container = null)
        {
            if (attribute == null)
                throw new ArgumentNullException(nameof(attribute));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var assembly = containerType.Assembly;

            // handlers are organized by assemblies to build an hierarchie
            // if the assembly is not registered yet we add it to the end
            if (!nonSharedHandlers.ContainsKey(assembly))
                nonSharedHandlers.Add(assembly, new Dictionary<Type, List<MessageHandler>>());

            if (!nonSharedHandlers[assembly].ContainsKey(messageType))
                nonSharedHandlers[assembly].Add(messageType, new List<MessageHandler>());

            nonSharedHandlers[assembly][messageType].Add(new MessageHandler(container, containerType, messageType, attribute, action, tokenType));
        }

        #endregion

        protected class MessageHandler
        {
            #region Constructors and Destructors

            public MessageHandler(object container, Type containerType, Type messageType, MessageHandlerAttribute handlerAttribute, Action<object, object, Message> action, Type tokenType)
            {
                Container = container;
                ContainerType = containerType;
                MessageType = messageType;
                Attribute = handlerAttribute;
                Action = action;
                TokenType = tokenType;
            }

            #endregion

            #region Public Properties

            public Action<object, object, Message> Action { get; }

            public MessageHandlerAttribute Attribute { get; }

            public object Container { get; }

            public Type ContainerType { get; }

            public Type MessageType { get; }

            public Type TokenType { get; }

            #endregion
        }
    }
}