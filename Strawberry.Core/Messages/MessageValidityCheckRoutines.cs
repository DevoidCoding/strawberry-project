﻿// <copyright file="MessageValidityCheckRoutines.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System;

    // note : not sure if i keep that
    [Flags]
    public enum MessageValidityCheckRoutines
    {
        None = 0,

        BotNotNull = 1,

        CharacterNotNull = 3,

        CharacterFighting = 7
    }
}