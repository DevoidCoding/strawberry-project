﻿// <copyright file="MessagePriority.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    public enum MessagePriority
    {
        VeryHigh = 4,

        High = 3,

        Normal = 2,

        Low = 1,

        VeryLow = 0
    }
}