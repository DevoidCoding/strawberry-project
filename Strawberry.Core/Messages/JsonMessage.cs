﻿namespace Strawberry.Core.Messages
{
    using Newtonsoft.Json;

    public class JsonMessage
    {
        #region Constructors and Destructors

        public JsonMessage()
            => MessageType = GetType().Name;

        #endregion

        #region Public Properties

        [JsonProperty("_messageType")]
        public string MessageType { get; private set; }

        #endregion
    }
}