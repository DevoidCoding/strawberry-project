﻿// <copyright file="IMessageFilter.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System;

    public interface IMessageFilter
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Method called to allow or not a message handling.
        /// </summary>
        /// <param name="message">The handled message</param>
        /// <param name="sender">The message sender</param>
        /// <param name="handlerContainer">The container type of the handler</param>
        /// <param name="container">The container of the handler (null if static)</param>
        /// <returns>True to allow the message handling</returns>
        bool Handle(Message message, object sender, Type handlerContainer, object container);

        #endregion
    }
}