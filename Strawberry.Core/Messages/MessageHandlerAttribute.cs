// <copyright file="MessageHandlerAttribute.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System;

    using Strawberry.Core.Network;

    public class MessageHandlerAttribute : Attribute
    {
        #region Constructors and Destructors

        public MessageHandlerAttribute()
            => HandleChildMessages = true;

        public MessageHandlerAttribute(Type type)
        {
            MessageType = type;
            HandleChildMessages = true;
        }

        #endregion

        #region Public Properties

        public ListenerEntry DestinationFilter { get; set; }

        public Type FilterType { get; set; }

        public ListenerEntry FromFilter { get; set; }

        public bool HandleChildMessages { get; set; }

        public Type MessageType { get; set; }

        #endregion
    }
}