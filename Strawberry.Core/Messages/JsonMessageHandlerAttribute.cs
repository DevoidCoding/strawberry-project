﻿namespace Strawberry.Core.Messages
{
    using System;

    public class JsonMessageHandlerAttribute : Attribute
    {
        #region Constructors and Destructors

        public JsonMessageHandlerAttribute()
            => HandleChildMessages = true;

        public JsonMessageHandlerAttribute(Type type)
        {
            Type = type;
            HandleChildMessages = true;
        }

        #endregion

        #region Public Properties

        public bool HandleChildMessages { get; set; }

        public Type Type { get; set; }

        #endregion
    }
}