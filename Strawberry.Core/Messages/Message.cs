﻿// <copyright file="Message.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Messages
{
    using System;
    using System.Threading;

    [Serializable]
    public abstract class Message
    {
        #region Constructors and Destructors

        public Message()
            => Priority = MessagePriority.Normal;

        #endregion

        #region Public Events

        public event Action<Message> Dispatched;

        #endregion

        #region Public Properties

        public bool Canceled { get; set; }

        public bool IsDispatched { get; private set; }

        public MessagePriority Priority { get; set; }

        #endregion

        #region Public Methods and Operators

        public virtual void BlockProgression()
            => Canceled = true;

        /// <summary>
        ///     Internal use only
        /// </summary>
        public void OnDispatched()
        {
            if (IsDispatched)
                return;

            lock (this)
            {
                Monitor.PulseAll(this);
            }

            IsDispatched = true;

            var evnt = Dispatched;

            if (evnt != null)
                evnt(this);
        }

        /// <summary>
        ///     Block the current thread until the message is dispatched
        /// </summary>
        /// <returns>false whenever the message has already been dispatched</returns>
        public bool Wait()
            => Wait(TimeSpan.Zero);

        /// <summary>
        ///     Block the current thread until the message is dispatched
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns>false whenever the message has already been dispatched</returns>
        public bool Wait(TimeSpan timeout)
        {
            if (IsDispatched)
                return false;

            lock (this)
            {
                if (timeout > TimeSpan.Zero)
                    Monitor.Wait(this, timeout);
                else
                    Monitor.Wait(this);
            }

            return true;
        }

        #endregion
    }
}