﻿// <copyright file="Cryptography.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Cryptography
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    public static class Cryptography
    {
        #region Public Methods and Operators

        #region AES

        public static byte[] HashSimpleIVMode(byte[] data, byte[] key, CipherMode mode = CipherMode.CBC, PaddingMode pad = PaddingMode.PKCS7)
        {
            byte[] encrypted;
            byte[] iv;

            using (var aes = Aes.Create())
            {
                if (aes == null) return null;

                aes.Mode = mode;
                aes.Padding = PaddingMode.None;
                aes.GenerateIV();

                if (aes.BlockSize / 8 != aes.IV.Length) throw new Exception("IV is not correct");

                iv = aes.IV;
                var encryptor = aes.CreateEncryptor(key, iv);

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                        encrypted = memoryStream.ToArray();
                    }
                }
            }

            return iv.Concat(encrypted).ToArray();
        }

        #endregion

        #region PKCS5

        public static byte[] Pkcs5Pad(byte[] a, int blockSize = 16)
        {
            var c = blockSize - a.Length % blockSize;
            var ret = new byte[a.Length + c];
            a.CopyTo(ret, 0);
            for (var i = 0; i < c; i++) ret[a.Length + i] = (byte)c;
            return ret;
        }

        #endregion

        #endregion

        #region MD5

        /// <summary>
        ///     Get the md5 from a string
        /// </summary>
        /// <param name="input">String input</param>
        /// <returns>MD5 Hash</returns>
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Reviewed. Suppression is OK here.")]
        public static string GetMD5Hash(string input)
            => GetMD5Hash(Encoding.Default.GetBytes(input));

        public static string GetMD5Hash(byte[] data)
        {
            var stringBuilder = new StringBuilder();

            data = MD5Hash(data);

            foreach (var t in data)
                stringBuilder.Append(t.ToString("x2", CultureInfo.CurrentCulture));

            return stringBuilder.ToString().ToUpperInvariant();
        }

        public static byte[] MD5Hash(byte[] data)
            => MD5.Create().ComputeHash(data);

        /// <summary>
        ///     Check if the given hash equals to the hash of the given string
        /// </summary>
        /// <param name="chaine">String</param>
        /// <param name="hash">MD5 hash to check</param>
        /// <returns></returns>
        public static bool VerifyMD5Hash(string chaine, string hash)
        {
            var hashOfInput = GetMD5Hash(chaine);

            var comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hashOfInput, hash) == 0;
        }

        #endregion

        #region RSA

        public static string EncryptRSA(string encryptValue, RSAParameters parameters)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(parameters);

            var bytesToEncrypt = Encoding.UTF8.GetBytes(encryptValue);
            var bytesEncrypted = rsa.Encrypt(bytesToEncrypt, false);

            var encryptedValue = Convert.ToBase64String(bytesEncrypted);

            return encryptedValue;
        }

        public static string DecryptRSA(byte[] encryptedValue, RSAParameters parameters)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(parameters);

            var decryptedValue = Encoding.UTF8.GetString(rsa.Decrypt(encryptedValue, false));

            return decryptedValue;
        }

        #endregion
    }
}