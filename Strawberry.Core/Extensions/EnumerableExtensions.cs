﻿// <copyright file="EnumerableExtensions.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtensions
    {
        #region Static Fields

        private static readonly Random Rnd = new Random();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Select a random item within the elements of an IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputSet"></param>
        /// <returns></returns>
        public static T GetRandom<T>(this IEnumerable<T> inputSet)
            => GetRandom(inputSet.ToArray());

        /// <summary>
        ///     Select a random item within the elements of an array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputSet"></param>
        /// <returns></returns>
        public static T GetRandom<T>(this T[] inputSet)
        {
            if (inputSet.Length == 0) return default;
            return inputSet[Rnd.Next(inputSet.Length)];
        }

        #endregion
    }
}