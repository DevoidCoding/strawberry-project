// <copyright file="TaskSchedulerExtensions.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Extensions
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public static class TaskSchedulerExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets a SynchronizationContext that targets this TaskScheduler.
        /// </summary>
        /// <param name="scheduler">The target scheduler.</param>
        /// <returns>A SynchronizationContext that targets this scheduler.</returns>
        public static SynchronizationContext ToSynchronizationContext(this TaskScheduler scheduler)
            => new TaskSchedulerSynchronizationContext(scheduler);

        #endregion

        #region Nested type: TaskSchedulerSynchronizationContext

        /// <summary>
        ///     Provides a SynchronizationContext wrapper for a TaskScheduler.
        /// </summary>
        private sealed class TaskSchedulerSynchronizationContext : SynchronizationContext
        {
            #region Fields

            /// <summary>
            ///     The scheduler.
            /// </summary>
            private readonly TaskScheduler scheduler;

            #endregion

            #region Constructors and Destructors

            /// <summary>
            ///     Initializes the context with the specified scheduler.
            /// </summary>
            /// <param name="scheduler">The scheduler to target.</param>
            internal TaskSchedulerSynchronizationContext(TaskScheduler scheduler)
            {
                this.scheduler = scheduler ?? throw new ArgumentNullException(nameof(scheduler));
            }

            #endregion

            #region Public Methods and Operators

            /// <summary>
            ///     Dispatches an asynchronous message to the synchronization context.
            /// </summary>
            /// <param name="d">The System.Threading.SendOrPostCallback delegate to call.</param>
            /// <param name="state">The object passed to the delegate.</param>
            public override void Post(SendOrPostCallback d, object state)
                => Task.Factory.StartNew(() => d(state), CancellationToken.None, TaskCreationOptions.None, scheduler);

            /// <summary>
            ///     Dispatches a synchronous message to the synchronization context.
            /// </summary>
            /// <param name="d">The System.Threading.SendOrPostCallback delegate to call.</param>
            /// <param name="state">The object passed to the delegate.</param>
            public override void Send(SendOrPostCallback d, object state)
            {
                var t = new Task(() => d(state));
                t.RunSynchronously(scheduler);
                t.Wait();
            }

            #endregion
        }

        #endregion
    }
}