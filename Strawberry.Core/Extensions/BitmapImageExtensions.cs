﻿namespace Strawberry.Core.Extensions
{
    using System.IO;
    using System.Windows.Media.Imaging;

    public static class BitmapImageExtensions
    {
        #region Public Methods and Operators

        public static void Save(this BitmapImage image, string filePath)
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                encoder.Save(fileStream);
            }
        }

        #endregion
    }
}