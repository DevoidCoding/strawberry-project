﻿// <copyright file="DateExtensions.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Extensions
{
    using System;

    public static class DateExtensions
    {
        #region Public Methods and Operators

        // milliseconds or seconds ??

        public static double DateTimeToUnixTimestamp(this DateTime dateTime)
            => (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalMilliseconds;

        public static int DateTimeToUnixTimestampSeconds(this DateTime dateTime)
            => (int)(dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;

        public static DateTime UnixTimestampToDateTime(this double unixTimeStamp)
            => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(unixTimeStamp).ToLocalTime();

        public static DateTime UnixTimestampToDateTime(this int unixTimeStamp)
            => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(unixTimeStamp).ToLocalTime();

        #endregion
    }
}