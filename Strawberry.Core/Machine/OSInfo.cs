﻿// <copyright file="OSInfo.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:27</date>

namespace Strawberry.Core.Machine
{
    using System;

    public static class OSInfo
    {
        #region Public Methods and Operators

        public static string GetAppData()
            => Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public static string GetLocalAppData()
            => Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        public static string GetProgramFiles()
            => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);

        public static string GetProgramFilesX86()
            => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

        #endregion
    }
}