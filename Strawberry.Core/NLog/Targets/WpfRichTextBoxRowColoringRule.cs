﻿#if !NET_CF && !MONO && !SILVERLIGHT

namespace Strawberry.Core.NLog.Targets
{
    using System.ComponentModel;
    using System.Windows;

    using global::NLog;
    using global::NLog.Conditions;
    using global::NLog.Config;

    [NLogConfigurationItem]
    public class WpfRichTextBoxRowColoringRule
    {
        #region Constructors and Destructors

        static WpfRichTextBoxRowColoringRule()
            => Default = new WpfRichTextBoxRowColoringRule();

        public WpfRichTextBoxRowColoringRule()
            : this(null, "Empty", "Empty", FontStyles.Normal, FontWeights.Normal)
        {
        }

        public WpfRichTextBoxRowColoringRule(string condition, string fontColor, string backColor, FontStyle fontStyle, FontWeight fontWeight)
        {
            Condition = condition;
            FontColor = fontColor;
            BackgroundColor = backColor;
            Style = fontStyle;
            Weight = fontWeight;
        }

        public WpfRichTextBoxRowColoringRule(string condition, string fontColor, string backColor)
        {
            Condition = condition;
            FontColor = fontColor;
            BackgroundColor = backColor;
            Style = FontStyles.Normal;
            Weight = FontWeights.Normal;
        }

        #endregion

        #region Public Properties

        public static WpfRichTextBoxRowColoringRule Default { get; }

        [DefaultValue("Empty")]
        public string BackgroundColor { get; set; }

        [RequiredParameter]
        public ConditionExpression Condition { get; set; }

        [DefaultValue("Empty")]
        public string FontColor { get; set; }

        public FontStyle Style { get; set; }

        public FontWeight Weight { get; set; }

        #endregion

        #region Public Methods and Operators

        public bool CheckCondition(LogEventInfo logEvent)
            => true.Equals(Condition.Evaluate(logEvent));

        #endregion
    }
}

#endif