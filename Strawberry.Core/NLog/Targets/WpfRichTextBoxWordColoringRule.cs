﻿#if !NET_CF && !MONO && !SILVERLIGHT

namespace Strawberry.Core.NLog.Targets
{
    using System.ComponentModel;
    using System.Text.RegularExpressions;
    using System.Windows;

    using global::NLog.Config;

    [NLogConfigurationItem]
    public class WpfRichTextBoxWordColoringRule
    {
        #region Fields

        private Regex compiledRegex;

        #endregion

        #region Constructors and Destructors

        public WpfRichTextBoxWordColoringRule()
        {
            FontColor = "Empty";
            BackgroundColor = "Empty";
        }

        public WpfRichTextBoxWordColoringRule(string text, string fontColor, string backgroundColor)
        {
            Text = text;
            FontColor = fontColor;
            BackgroundColor = backgroundColor;
            Style = FontStyles.Normal;
            Weight = FontWeights.Normal;
        }

        public WpfRichTextBoxWordColoringRule(string text, string textColor, string backgroundColor, FontStyle fontStyle, FontWeight fontWeight)
        {
            Text = text;
            FontColor = textColor;
            BackgroundColor = backgroundColor;
            Style = fontStyle;
            Weight = fontWeight;
        }

        #endregion

        #region Public Properties

        [DefaultValue("Empty")]
        public string BackgroundColor { get; set; }

        public Regex CompiledRegex
        {
            get
            {
                if (compiledRegex == null)
                {
                    var regexpression = Regex;

                    if (regexpression == null && Text != null)
                    {
                        regexpression = System.Text.RegularExpressions.Regex.Escape(Text);
                        if (WholeWords) regexpression = "\b" + regexpression + "\b";
                    }

                    var regexOptions = RegexOptions.Compiled;
                    if (IgnoreCase) regexOptions |= RegexOptions.IgnoreCase;

                    compiledRegex = new Regex(regexpression, regexOptions);
                }

                return compiledRegex;
            }
        }

        [DefaultValue("Empty")]
        public string FontColor { get; set; }

        [DefaultValue(false)]
        public bool IgnoreCase { get; set; }

        public string Regex { get; set; }

        public FontStyle Style { get; set; }

        public string Text { get; set; }

        public FontWeight Weight { get; set; }

        [DefaultValue(false)]
        public bool WholeWords { get; set; }

        #endregion
    }
}

#endif