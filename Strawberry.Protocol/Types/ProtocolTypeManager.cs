﻿// <copyright file="ProtocolTypeManager.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/21/2016 03:54</date>

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Strawberry.Core.Extensions;

namespace Strawberry.Protocol.Types
{
    public static class ProtocolTypeManager
    {
        private static readonly Dictionary<short, Type> types = new Dictionary<short, Type>(200);

        private static readonly Dictionary<short, Func<object>> typesConstructors =
            new Dictionary<short, Func<object>>(200);

        static ProtocolTypeManager()
        {
            var asm = Assembly.GetAssembly(typeof(ProtocolTypeManager));

            foreach (var type in asm.GetTypes())
            {
                if (type.Namespace == null || !type.Namespace.StartsWith(typeof(ProtocolTypeManager).Namespace))
                    continue;

                var field = type.GetField("ProtocolId");

                if (field != null)
                {
                    // le cast uint est obligatoire car l'objet n'a pas de type
                    var id = (short) field.GetValue(type);

                    types.Add(id, type);

                    var ctor = type.GetConstructor(Type.EmptyTypes);

                    if (ctor == null)
                        throw new Exception($"'{type}' doesn't implemented a parameterless constructor");

                    typesConstructors.Add(id, ctor.CustomCreateDelegate<Func<object>>());
                }
            }
        }

        /// <summary>
        ///     Gets instance of the type defined by id.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="id">id.</param>
        /// <returns></returns>
        public static T GetInstance<T>(short id) where T : class
        {
            if (!types.ContainsKey(id))
            {
                throw new ProtocolTypeNotFoundException($"Type <id:{id}> doesn't exist");
            }

            return typesConstructors[id]() as T;
        }

        [Serializable]
        public class ProtocolTypeNotFoundException : Exception
        {
            public ProtocolTypeNotFoundException()
            {
            }

            public ProtocolTypeNotFoundException(string message)
                : base(message)
            {
            }

            public ProtocolTypeNotFoundException(string message, Exception inner)
                : base(message, inner)
            {
            }

            protected ProtocolTypeNotFoundException(
                SerializationInfo info,
                StreamingContext context)
                : base(info, context)
            {
            }
        }
    }
}