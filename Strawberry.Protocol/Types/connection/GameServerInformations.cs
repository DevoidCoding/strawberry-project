

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameServerInformations
    {
        public const short ProtocolId = 25;
        public virtual short TypeID => ProtocolId;
        
        public ushort Id { get; set; }
        public sbyte Type { get; set; }
        public sbyte Status { get; set; }
        public sbyte Completion { get; set; }
        public bool IsSelectable { get; set; }
        public sbyte CharactersCount { get; set; }
        public sbyte CharactersSlots { get; set; }
        public double Date { get; set; }
        
        public GameServerInformations()
        {
        }
        
        public GameServerInformations(ushort id, sbyte type, sbyte status, sbyte completion, bool isSelectable, sbyte charactersCount, sbyte charactersSlots, double date)
        {
            this.Id = id;
            this.Type = type;
            this.Status = status;
            this.Completion = completion;
            this.IsSelectable = isSelectable;
            this.CharactersCount = charactersCount;
            this.CharactersSlots = charactersSlots;
            this.Date = date;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteSByte(Type);
            writer.WriteSByte(Status);
            writer.WriteSByte(Completion);
            writer.WriteBoolean(IsSelectable);
            writer.WriteSByte(CharactersCount);
            writer.WriteSByte(CharactersSlots);
            writer.WriteDouble(Date);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            Type = reader.ReadSByte();
            Status = reader.ReadSByte();
            if (Status < 0)
                throw new Exception("Forbidden value on Status = " + Status + ", it doesn't respect the following condition : status < 0");
            Completion = reader.ReadSByte();
            if (Completion < 0)
                throw new Exception("Forbidden value on Completion = " + Completion + ", it doesn't respect the following condition : completion < 0");
            IsSelectable = reader.ReadBoolean();
            CharactersCount = reader.ReadSByte();
            if (CharactersCount < 0)
                throw new Exception("Forbidden value on CharactersCount = " + CharactersCount + ", it doesn't respect the following condition : charactersCount < 0");
            CharactersSlots = reader.ReadSByte();
            if (CharactersSlots < 0)
                throw new Exception("Forbidden value on CharactersSlots = " + CharactersSlots + ", it doesn't respect the following condition : charactersSlots < 0");
            Date = reader.ReadDouble();
            if (Date < -9007199254740990 || Date > 9007199254740990)
                throw new Exception("Forbidden value on Date = " + Date + ", it doesn't respect the following condition : date < -9007199254740990 || date > 9007199254740990");
        }
        
    }
    
}