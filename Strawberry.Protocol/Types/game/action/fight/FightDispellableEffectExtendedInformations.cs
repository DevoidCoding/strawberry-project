

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightDispellableEffectExtendedInformations
    {
        public const short ProtocolId = 208;
        public virtual short TypeID => ProtocolId;
        
        public ushort ActionId { get; set; }
        public double SourceId { get; set; }
        public Types.AbstractFightDispellableEffect Effect { get; set; }
        
        public FightDispellableEffectExtendedInformations()
        {
        }
        
        public FightDispellableEffectExtendedInformations(ushort actionId, double sourceId, Types.AbstractFightDispellableEffect effect)
        {
            this.ActionId = actionId;
            this.SourceId = sourceId;
            this.Effect = effect;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(SourceId);
            writer.WriteShort(Effect.TypeID);
            Effect.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ActionId = reader.ReadVarUhShort();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
            SourceId = reader.ReadDouble();
            if (SourceId < -9007199254740990 || SourceId > 9007199254740990)
                throw new Exception("Forbidden value on SourceId = " + SourceId + ", it doesn't respect the following condition : sourceId < -9007199254740990 || sourceId > 9007199254740990");
            Effect = Types.ProtocolTypeManager.GetInstance<Types.AbstractFightDispellableEffect>(reader.ReadShort());
            Effect.Deserialize(reader);
        }
        
    }
    
}