

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameActionMark
    {
        public const short ProtocolId = 351;
        public virtual short TypeID => ProtocolId;
        
        public double MarkAuthorId { get; set; }
        public sbyte MarkTeamId { get; set; }
        public int MarkSpellId { get; set; }
        public short MarkSpellLevel { get; set; }
        public short MarkId { get; set; }
        public sbyte MarkType { get; set; }
        public short MarkimpactCell { get; set; }
        public Types.GameActionMarkedCell[] Cells { get; set; }
        public bool Active { get; set; }
        
        public GameActionMark()
        {
        }
        
        public GameActionMark(double markAuthorId, sbyte markTeamId, int markSpellId, short markSpellLevel, short markId, sbyte markType, short markimpactCell, Types.GameActionMarkedCell[] cells, bool active)
        {
            this.MarkAuthorId = markAuthorId;
            this.MarkTeamId = markTeamId;
            this.MarkSpellId = markSpellId;
            this.MarkSpellLevel = markSpellLevel;
            this.MarkId = markId;
            this.MarkType = markType;
            this.MarkimpactCell = markimpactCell;
            this.Cells = cells;
            this.Active = active;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MarkAuthorId);
            writer.WriteSByte(MarkTeamId);
            writer.WriteInt(MarkSpellId);
            writer.WriteShort(MarkSpellLevel);
            writer.WriteShort(MarkId);
            writer.WriteSByte(MarkType);
            writer.WriteShort(MarkimpactCell);
            writer.WriteUShort((ushort)Cells.Length);
            foreach (var entry in Cells)
            {
                 entry.Serialize(writer);
            }
            writer.WriteBoolean(Active);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            MarkAuthorId = reader.ReadDouble();
            if (MarkAuthorId < -9007199254740990 || MarkAuthorId > 9007199254740990)
                throw new Exception("Forbidden value on MarkAuthorId = " + MarkAuthorId + ", it doesn't respect the following condition : markAuthorId < -9007199254740990 || markAuthorId > 9007199254740990");
            MarkTeamId = reader.ReadSByte();
            if (MarkTeamId < 0)
                throw new Exception("Forbidden value on MarkTeamId = " + MarkTeamId + ", it doesn't respect the following condition : markTeamId < 0");
            MarkSpellId = reader.ReadInt();
            if (MarkSpellId < 0)
                throw new Exception("Forbidden value on MarkSpellId = " + MarkSpellId + ", it doesn't respect the following condition : markSpellId < 0");
            MarkSpellLevel = reader.ReadShort();
            if (MarkSpellLevel < 1 || MarkSpellLevel > 200)
                throw new Exception("Forbidden value on MarkSpellLevel = " + MarkSpellLevel + ", it doesn't respect the following condition : markSpellLevel < 1 || markSpellLevel > 200");
            MarkId = reader.ReadShort();
            MarkType = reader.ReadSByte();
            MarkimpactCell = reader.ReadShort();
            if (MarkimpactCell < -1 || MarkimpactCell > 559)
                throw new Exception("Forbidden value on MarkimpactCell = " + MarkimpactCell + ", it doesn't respect the following condition : markimpactCell < -1 || markimpactCell > 559");
            var limit = reader.ReadUShort();
            Cells = new Types.GameActionMarkedCell[limit];
            for (int i = 0; i < limit; i++)
            {
                 Cells[i] = new Types.GameActionMarkedCell();
                 Cells[i].Deserialize(reader);
            }
            Active = reader.ReadBoolean();
        }
        
    }
    
}