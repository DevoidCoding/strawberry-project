

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTriggeredEffect : AbstractFightDispellableEffect
    {
        public new const short ProtocolId = 210;
        public override short TypeID => ProtocolId;
        
        public int Arg1 { get; set; }
        public int Arg2 { get; set; }
        public int Arg3 { get; set; }
        public short Delay { get; set; }
        
        public FightTriggeredEffect()
        {
        }
        
        public FightTriggeredEffect(uint uid, double targetId, short turnDuration, sbyte dispelable, ushort spellId, uint effectId, uint parentBoostUid, int arg1, int arg2, int arg3, short delay)
         : base(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid)
        {
            this.Arg1 = arg1;
            this.Arg2 = arg2;
            this.Arg3 = arg3;
            this.Delay = delay;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Arg1);
            writer.WriteInt(Arg2);
            writer.WriteInt(Arg3);
            writer.WriteShort(Delay);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Arg1 = reader.ReadInt();
            Arg2 = reader.ReadInt();
            Arg3 = reader.ReadInt();
            Delay = reader.ReadShort();
        }
        
    }
    
}