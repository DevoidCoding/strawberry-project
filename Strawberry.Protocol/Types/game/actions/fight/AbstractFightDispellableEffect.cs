

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AbstractFightDispellableEffect
    {
        public const short ProtocolId = 206;
        public virtual short TypeID => ProtocolId;
        
        public uint Uid { get; set; }
        public double TargetId { get; set; }
        public short TurnDuration { get; set; }
        public sbyte Dispelable { get; set; }
        public ushort SpellId { get; set; }
        public uint EffectId { get; set; }
        public uint ParentBoostUid { get; set; }
        
        public AbstractFightDispellableEffect()
        {
        }
        
        public AbstractFightDispellableEffect(uint uid, double targetId, short turnDuration, sbyte dispelable, ushort spellId, uint effectId, uint parentBoostUid)
        {
            this.Uid = uid;
            this.TargetId = targetId;
            this.TurnDuration = turnDuration;
            this.Dispelable = dispelable;
            this.SpellId = spellId;
            this.EffectId = effectId;
            this.ParentBoostUid = parentBoostUid;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteDouble(TargetId);
            writer.WriteShort(TurnDuration);
            writer.WriteSByte(Dispelable);
            writer.WriteVarShort(SpellId);
            writer.WriteVarInt(EffectId);
            writer.WriteVarInt(ParentBoostUid);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Uid = reader.ReadVarUhInt();
            if (Uid < 0)
                throw new Exception("Forbidden value on Uid = " + Uid + ", it doesn't respect the following condition : uid < 0");
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            TurnDuration = reader.ReadShort();
            Dispelable = reader.ReadSByte();
            if (Dispelable < 0)
                throw new Exception("Forbidden value on Dispelable = " + Dispelable + ", it doesn't respect the following condition : dispelable < 0");
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            EffectId = reader.ReadVarUhInt();
            if (EffectId < 0)
                throw new Exception("Forbidden value on EffectId = " + EffectId + ", it doesn't respect the following condition : effectId < 0");
            ParentBoostUid = reader.ReadVarUhInt();
            if (ParentBoostUid < 0)
                throw new Exception("Forbidden value on ParentBoostUid = " + ParentBoostUid + ", it doesn't respect the following condition : parentBoostUid < 0");
        }
        
    }
    
}