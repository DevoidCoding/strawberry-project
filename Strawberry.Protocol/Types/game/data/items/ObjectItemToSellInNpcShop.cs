

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemToSellInNpcShop : ObjectItemMinimalInformation
    {
        public new const short ProtocolId = 352;
        public override short TypeID => ProtocolId;
        
        public ulong ObjectPrice { get; set; }
        public string BuyCriterion { get; set; }
        
        public ObjectItemToSellInNpcShop()
        {
        }
        
        public ObjectItemToSellInNpcShop(ushort objectGID, Types.ObjectEffect[] effects, ulong objectPrice, string buyCriterion)
         : base(objectGID, effects)
        {
            this.ObjectPrice = objectPrice;
            this.BuyCriterion = buyCriterion;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(ObjectPrice);
            writer.WriteUTF(BuyCriterion);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectPrice = reader.ReadVarUhLong();
            if (ObjectPrice < 0 || ObjectPrice > 9007199254740990)
                throw new Exception("Forbidden value on ObjectPrice = " + ObjectPrice + ", it doesn't respect the following condition : objectPrice < 0 || objectPrice > 9007199254740990");
            BuyCriterion = reader.ReadUTF();
        }
        
    }
    
}