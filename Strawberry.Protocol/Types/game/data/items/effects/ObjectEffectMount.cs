

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectMount : ObjectEffect
    {
        public new const short ProtocolId = 179;
        public override short TypeID => ProtocolId;
        
        public int MountId { get; set; }
        public double Date { get; set; }
        public ushort ModelId { get; set; }
        
        public ObjectEffectMount()
        {
        }
        
        public ObjectEffectMount(ushort actionId, int mountId, double date, ushort modelId)
         : base(actionId)
        {
            this.MountId = mountId;
            this.Date = date;
            this.ModelId = modelId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(MountId);
            writer.WriteDouble(Date);
            writer.WriteVarShort(ModelId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MountId = reader.ReadInt();
            if (MountId < 0)
                throw new Exception("Forbidden value on MountId = " + MountId + ", it doesn't respect the following condition : mountId < 0");
            Date = reader.ReadDouble();
            if (Date < -9007199254740990 || Date > 9007199254740990)
                throw new Exception("Forbidden value on Date = " + Date + ", it doesn't respect the following condition : date < -9007199254740990 || date > 9007199254740990");
            ModelId = reader.ReadVarUhShort();
            if (ModelId < 0)
                throw new Exception("Forbidden value on ModelId = " + ModelId + ", it doesn't respect the following condition : modelId < 0");
        }
        
    }
    
}