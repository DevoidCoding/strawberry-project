

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectDuration : ObjectEffect
    {
        public new const short ProtocolId = 75;
        public override short TypeID => ProtocolId;
        
        public ushort Days { get; set; }
        public sbyte Hours { get; set; }
        public sbyte Minutes { get; set; }
        
        public ObjectEffectDuration()
        {
        }
        
        public ObjectEffectDuration(ushort actionId, ushort days, sbyte hours, sbyte minutes)
         : base(actionId)
        {
            this.Days = days;
            this.Hours = hours;
            this.Minutes = minutes;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Days);
            writer.WriteSByte(Hours);
            writer.WriteSByte(Minutes);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Days = reader.ReadVarUhShort();
            if (Days < 0)
                throw new Exception("Forbidden value on Days = " + Days + ", it doesn't respect the following condition : days < 0");
            Hours = reader.ReadSByte();
            if (Hours < 0)
                throw new Exception("Forbidden value on Hours = " + Hours + ", it doesn't respect the following condition : hours < 0");
            Minutes = reader.ReadSByte();
            if (Minutes < 0)
                throw new Exception("Forbidden value on Minutes = " + Minutes + ", it doesn't respect the following condition : minutes < 0");
        }
        
    }
    
}