

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectLadder : ObjectEffectCreature
    {
        public new const short ProtocolId = 81;
        public override short TypeID => ProtocolId;
        
        public uint MonsterCount { get; set; }
        
        public ObjectEffectLadder()
        {
        }
        
        public ObjectEffectLadder(ushort actionId, ushort monsterFamilyId, uint monsterCount)
         : base(actionId, monsterFamilyId)
        {
            this.MonsterCount = monsterCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(MonsterCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MonsterCount = reader.ReadVarUhInt();
            if (MonsterCount < 0)
                throw new Exception("Forbidden value on MonsterCount = " + MonsterCount + ", it doesn't respect the following condition : monsterCount < 0");
        }
        
    }
    
}