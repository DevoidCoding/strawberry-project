

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectString : ObjectEffect
    {
        public new const short ProtocolId = 74;
        public override short TypeID => ProtocolId;
        
        public string Value { get; set; }
        
        public ObjectEffectString()
        {
        }
        
        public ObjectEffectString(ushort actionId, string value)
         : base(actionId)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadUTF();
        }
        
    }
    
}