

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectDice : ObjectEffect
    {
        public new const short ProtocolId = 73;
        public override short TypeID => ProtocolId;
        
        public ushort DiceNum { get; set; }
        public ushort DiceSide { get; set; }
        public ushort DiceConst { get; set; }
        
        public ObjectEffectDice()
        {
        }
        
        public ObjectEffectDice(ushort actionId, ushort diceNum, ushort diceSide, ushort diceConst)
         : base(actionId)
        {
            this.DiceNum = diceNum;
            this.DiceSide = diceSide;
            this.DiceConst = diceConst;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(DiceNum);
            writer.WriteVarShort(DiceSide);
            writer.WriteVarShort(DiceConst);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            DiceNum = reader.ReadVarUhShort();
            if (DiceNum < 0)
                throw new Exception("Forbidden value on DiceNum = " + DiceNum + ", it doesn't respect the following condition : diceNum < 0");
            DiceSide = reader.ReadVarUhShort();
            if (DiceSide < 0)
                throw new Exception("Forbidden value on DiceSide = " + DiceSide + ", it doesn't respect the following condition : diceSide < 0");
            DiceConst = reader.ReadVarUhShort();
            if (DiceConst < 0)
                throw new Exception("Forbidden value on DiceConst = " + DiceConst + ", it doesn't respect the following condition : diceConst < 0");
        }
        
    }
    
}