

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectInteger : ObjectEffect
    {
        public new const short ProtocolId = 70;
        public override short TypeID => ProtocolId;
        
        public uint Value { get; set; }
        
        public ObjectEffectInteger()
        {
        }
        
        public ObjectEffectInteger(ushort actionId, uint value)
         : base(actionId)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadVarUhInt();
            if (Value < 0)
                throw new Exception("Forbidden value on Value = " + Value + ", it doesn't respect the following condition : value < 0");
        }
        
    }
    
}