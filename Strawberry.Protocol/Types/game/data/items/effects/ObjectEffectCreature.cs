

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectCreature : ObjectEffect
    {
        public new const short ProtocolId = 71;
        public override short TypeID => ProtocolId;
        
        public ushort MonsterFamilyId { get; set; }
        
        public ObjectEffectCreature()
        {
        }
        
        public ObjectEffectCreature(ushort actionId, ushort monsterFamilyId)
         : base(actionId)
        {
            this.MonsterFamilyId = monsterFamilyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MonsterFamilyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MonsterFamilyId = reader.ReadVarUhShort();
            if (MonsterFamilyId < 0)
                throw new Exception("Forbidden value on MonsterFamilyId = " + MonsterFamilyId + ", it doesn't respect the following condition : monsterFamilyId < 0");
        }
        
    }
    
}