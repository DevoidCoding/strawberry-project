

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectMinMax : ObjectEffect
    {
        public new const short ProtocolId = 82;
        public override short TypeID => ProtocolId;
        
        public uint Min { get; set; }
        public uint Max { get; set; }
        
        public ObjectEffectMinMax()
        {
        }
        
        public ObjectEffectMinMax(ushort actionId, uint min, uint max)
         : base(actionId)
        {
            this.Min = min;
            this.Max = max;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Min);
            writer.WriteVarInt(Max);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Min = reader.ReadVarUhInt();
            if (Min < 0)
                throw new Exception("Forbidden value on Min = " + Min + ", it doesn't respect the following condition : min < 0");
            Max = reader.ReadVarUhInt();
            if (Max < 0)
                throw new Exception("Forbidden value on Max = " + Max + ", it doesn't respect the following condition : max < 0");
        }
        
    }
    
}