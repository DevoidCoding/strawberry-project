

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffectDate : ObjectEffect
    {
        public new const short ProtocolId = 72;
        public override short TypeID => ProtocolId;
        
        public ushort Year { get; set; }
        public sbyte Month { get; set; }
        public sbyte Day { get; set; }
        public sbyte Hour { get; set; }
        public sbyte Minute { get; set; }
        
        public ObjectEffectDate()
        {
        }
        
        public ObjectEffectDate(ushort actionId, ushort year, sbyte month, sbyte day, sbyte hour, sbyte minute)
         : base(actionId)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;
            this.Hour = hour;
            this.Minute = minute;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Year);
            writer.WriteSByte(Month);
            writer.WriteSByte(Day);
            writer.WriteSByte(Hour);
            writer.WriteSByte(Minute);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Year = reader.ReadVarUhShort();
            if (Year < 0)
                throw new Exception("Forbidden value on Year = " + Year + ", it doesn't respect the following condition : year < 0");
            Month = reader.ReadSByte();
            if (Month < 0)
                throw new Exception("Forbidden value on Month = " + Month + ", it doesn't respect the following condition : month < 0");
            Day = reader.ReadSByte();
            if (Day < 0)
                throw new Exception("Forbidden value on Day = " + Day + ", it doesn't respect the following condition : day < 0");
            Hour = reader.ReadSByte();
            if (Hour < 0)
                throw new Exception("Forbidden value on Hour = " + Hour + ", it doesn't respect the following condition : hour < 0");
            Minute = reader.ReadSByte();
            if (Minute < 0)
                throw new Exception("Forbidden value on Minute = " + Minute + ", it doesn't respect the following condition : minute < 0");
        }
        
    }
    
}