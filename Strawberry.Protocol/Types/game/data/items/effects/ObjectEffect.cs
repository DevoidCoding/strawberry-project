

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectEffect
    {
        public const short ProtocolId = 76;
        public virtual short TypeID => ProtocolId;
        
        public ushort ActionId { get; set; }
        
        public ObjectEffect()
        {
        }
        
        public ObjectEffect(ushort actionId)
        {
            this.ActionId = actionId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ActionId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ActionId = reader.ReadVarUhShort();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
        }
        
    }
    
}