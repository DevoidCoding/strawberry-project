

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class BidExchangerObjectInfo
    {
        public const short ProtocolId = 122;
        public virtual short TypeID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public Types.ObjectEffect[] Effects { get; set; }
        public ulong[] Prices { get; set; }
        
        public BidExchangerObjectInfo()
        {
        }
        
        public BidExchangerObjectInfo(uint objectUID, Types.ObjectEffect[] effects, ulong[] prices)
        {
            this.ObjectUID = objectUID;
            this.Effects = effects;
            this.Prices = prices;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Prices.Length);
            foreach (var entry in Prices)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            var limit = reader.ReadUShort();
            Effects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 Effects[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Prices = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 Prices[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}