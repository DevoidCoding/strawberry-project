

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItem : Item
    {
        public new const short ProtocolId = 37;
        public override short TypeID => ProtocolId;
        
        public byte Position { get; set; }
        public ushort ObjectGID { get; set; }
        public Types.ObjectEffect[] Effects { get; set; }
        public uint ObjectUID { get; set; }
        public uint Quantity { get; set; }
        
        public ObjectItem()
        {
        }
        
        public ObjectItem(byte position, ushort objectGID, Types.ObjectEffect[] effects, uint objectUID, uint quantity)
        {
            this.Position = position;
            this.ObjectGID = objectGID;
            this.Effects = effects;
            this.ObjectUID = objectUID;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Position);
            writer.WriteVarShort(ObjectGID);
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Position = reader.ReadByte();
            if (Position < 0 || Position > 255)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 255");
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
            var limit = reader.ReadUShort();
            Effects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 Effects[i].Deserialize(reader);
            }
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}