

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SpellItem : Item
    {
        public new const short ProtocolId = 49;
        public override short TypeID => ProtocolId;
        
        public int SpellId { get; set; }
        public short SpellLevel { get; set; }
        
        public SpellItem()
        {
        }
        
        public SpellItem(int spellId, short spellLevel)
        {
            this.SpellId = spellId;
            this.SpellLevel = spellLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(SpellId);
            writer.WriteShort(SpellLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadInt();
            SpellLevel = reader.ReadShort();
            if (SpellLevel < 1 || SpellLevel > 200)
                throw new Exception("Forbidden value on SpellLevel = " + SpellLevel + ", it doesn't respect the following condition : spellLevel < 1 || spellLevel > 200");
        }
        
    }
    
}