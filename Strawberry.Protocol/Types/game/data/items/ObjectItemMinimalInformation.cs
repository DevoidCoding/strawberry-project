

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemMinimalInformation : Item
    {
        public new const short ProtocolId = 124;
        public override short TypeID => ProtocolId;
        
        public ushort ObjectGID { get; set; }
        public Types.ObjectEffect[] Effects { get; set; }
        
        public ObjectItemMinimalInformation()
        {
        }
        
        public ObjectItemMinimalInformation(ushort objectGID, Types.ObjectEffect[] effects)
        {
            this.ObjectGID = objectGID;
            this.Effects = effects;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
            var limit = reader.ReadUShort();
            Effects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 Effects[i].Deserialize(reader);
            }
        }
        
    }
    
}