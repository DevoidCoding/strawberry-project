

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GoldItem : Item
    {
        public new const short ProtocolId = 123;
        public override short TypeID => ProtocolId;
        
        public ulong Sum { get; set; }
        
        public GoldItem()
        {
        }
        
        public GoldItem(ulong sum)
        {
            this.Sum = sum;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Sum);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Sum = reader.ReadVarUhLong();
            if (Sum < 0 || Sum > 9007199254740990)
                throw new Exception("Forbidden value on Sum = " + Sum + ", it doesn't respect the following condition : sum < 0 || sum > 9007199254740990");
        }
        
    }
    
}