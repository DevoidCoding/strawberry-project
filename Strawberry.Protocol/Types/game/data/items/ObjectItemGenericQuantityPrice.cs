

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemGenericQuantityPrice : ObjectItemGenericQuantity
    {
        public new const short ProtocolId = 494;
        public override short TypeID => ProtocolId;
        
        public ulong Price { get; set; }
        
        public ObjectItemGenericQuantityPrice()
        {
        }
        
        public ObjectItemGenericQuantityPrice(ushort objectGID, uint quantity, ulong price)
         : base(objectGID, quantity)
        {
            this.Price = price;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Price);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}