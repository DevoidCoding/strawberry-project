

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemQuantity : Item
    {
        public new const short ProtocolId = 119;
        public override short TypeID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public uint Quantity { get; set; }
        
        public ObjectItemQuantity()
        {
        }
        
        public ObjectItemQuantity(uint objectUID, uint quantity)
        {
            this.ObjectUID = objectUID;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}