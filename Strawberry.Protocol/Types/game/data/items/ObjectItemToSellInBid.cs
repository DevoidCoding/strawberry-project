

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemToSellInBid : ObjectItemToSell
    {
        public new const short ProtocolId = 164;
        public override short TypeID => ProtocolId;
        
        public int UnsoldDelay { get; set; }
        
        public ObjectItemToSellInBid()
        {
        }
        
        public ObjectItemToSellInBid(ushort objectGID, Types.ObjectEffect[] effects, uint objectUID, uint quantity, ulong objectPrice, int unsoldDelay)
         : base(objectGID, effects, objectUID, quantity, objectPrice)
        {
            this.UnsoldDelay = unsoldDelay;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(UnsoldDelay);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            UnsoldDelay = reader.ReadInt();
            if (UnsoldDelay < 0)
                throw new Exception("Forbidden value on UnsoldDelay = " + UnsoldDelay + ", it doesn't respect the following condition : unsoldDelay < 0");
        }
        
    }
    
}