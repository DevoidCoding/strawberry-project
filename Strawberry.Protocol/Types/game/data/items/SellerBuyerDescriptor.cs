

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SellerBuyerDescriptor
    {
        public const short ProtocolId = 121;
        public virtual short TypeID => ProtocolId;
        
        public uint[] Quantities { get; set; }
        public uint[] Types { get; set; }
        public float TaxPercentage { get; set; }
        public float TaxModificationPercentage { get; set; }
        public byte MaxItemLevel { get; set; }
        public uint MaxItemPerAccount { get; set; }
        public int NpcContextualId { get; set; }
        public ushort UnsoldDelay { get; set; }
        
        public SellerBuyerDescriptor()
        {
        }
        
        public SellerBuyerDescriptor(uint[] quantities, uint[] types, float taxPercentage, float taxModificationPercentage, byte maxItemLevel, uint maxItemPerAccount, int npcContextualId, ushort unsoldDelay)
        {
            this.Quantities = quantities;
            this.Types = types;
            this.TaxPercentage = taxPercentage;
            this.TaxModificationPercentage = taxModificationPercentage;
            this.MaxItemLevel = maxItemLevel;
            this.MaxItemPerAccount = maxItemPerAccount;
            this.NpcContextualId = npcContextualId;
            this.UnsoldDelay = unsoldDelay;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Quantities.Length);
            foreach (var entry in Quantities)
            {
                 writer.WriteVarInt(entry);
            }
            writer.WriteUShort((ushort)Types.Length);
            foreach (var entry in Types)
            {
                 writer.WriteVarInt(entry);
            }
            writer.WriteFloat(TaxPercentage);
            writer.WriteFloat(TaxModificationPercentage);
            writer.WriteByte(MaxItemLevel);
            writer.WriteVarInt(MaxItemPerAccount);
            writer.WriteInt(NpcContextualId);
            writer.WriteVarShort(UnsoldDelay);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Quantities = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Quantities[i] = reader.ReadVarUhInt();
            }
            limit = reader.ReadUShort();
            Types = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Types[i] = reader.ReadVarUhInt();
            }
            TaxPercentage = reader.ReadFloat();
            TaxModificationPercentage = reader.ReadFloat();
            MaxItemLevel = reader.ReadByte();
            if (MaxItemLevel < 0 || MaxItemLevel > 255)
                throw new Exception("Forbidden value on MaxItemLevel = " + MaxItemLevel + ", it doesn't respect the following condition : maxItemLevel < 0 || maxItemLevel > 255");
            MaxItemPerAccount = reader.ReadVarUhInt();
            if (MaxItemPerAccount < 0)
                throw new Exception("Forbidden value on MaxItemPerAccount = " + MaxItemPerAccount + ", it doesn't respect the following condition : maxItemPerAccount < 0");
            NpcContextualId = reader.ReadInt();
            UnsoldDelay = reader.ReadVarUhShort();
            if (UnsoldDelay < 0)
                throw new Exception("Forbidden value on UnsoldDelay = " + UnsoldDelay + ", it doesn't respect the following condition : unsoldDelay < 0");
        }
        
    }
    
}