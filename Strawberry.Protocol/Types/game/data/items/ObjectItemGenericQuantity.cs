

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemGenericQuantity : Item
    {
        public new const short ProtocolId = 483;
        public override short TypeID => ProtocolId;
        
        public ushort ObjectGID { get; set; }
        public uint Quantity { get; set; }
        
        public ObjectItemGenericQuantity()
        {
        }
        
        public ObjectItemGenericQuantity(ushort objectGID, uint quantity)
        {
            this.ObjectGID = objectGID;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}