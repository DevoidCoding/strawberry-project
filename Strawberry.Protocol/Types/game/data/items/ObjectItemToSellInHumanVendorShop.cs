

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemToSellInHumanVendorShop : Item
    {
        public new const short ProtocolId = 359;
        public override short TypeID => ProtocolId;
        
        public ushort ObjectGID { get; set; }
        public Types.ObjectEffect[] Effects { get; set; }
        public uint ObjectUID { get; set; }
        public uint Quantity { get; set; }
        public ulong ObjectPrice { get; set; }
        public ulong PublicPrice { get; set; }
        
        public ObjectItemToSellInHumanVendorShop()
        {
        }
        
        public ObjectItemToSellInHumanVendorShop(ushort objectGID, Types.ObjectEffect[] effects, uint objectUID, uint quantity, ulong objectPrice, ulong publicPrice)
        {
            this.ObjectGID = objectGID;
            this.Effects = effects;
            this.ObjectUID = objectUID;
            this.Quantity = quantity;
            this.ObjectPrice = objectPrice;
            this.PublicPrice = publicPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGID);
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
            writer.WriteVarLong(ObjectPrice);
            writer.WriteVarLong(PublicPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
            var limit = reader.ReadUShort();
            Effects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 Effects[i].Deserialize(reader);
            }
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
            ObjectPrice = reader.ReadVarUhLong();
            if (ObjectPrice < 0 || ObjectPrice > 9007199254740990)
                throw new Exception("Forbidden value on ObjectPrice = " + ObjectPrice + ", it doesn't respect the following condition : objectPrice < 0 || objectPrice > 9007199254740990");
            PublicPrice = reader.ReadVarUhLong();
            if (PublicPrice < 0 || PublicPrice > 9007199254740990)
                throw new Exception("Forbidden value on PublicPrice = " + PublicPrice + ", it doesn't respect the following condition : publicPrice < 0 || publicPrice > 9007199254740990");
        }
        
    }
    
}