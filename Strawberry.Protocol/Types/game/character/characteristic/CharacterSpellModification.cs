

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterSpellModification
    {
        public const short ProtocolId = 215;
        public virtual short TypeID => ProtocolId;
        
        public sbyte ModificationType { get; set; }
        public ushort SpellId { get; set; }
        public Types.CharacterBaseCharacteristic Value { get; set; }
        
        public CharacterSpellModification()
        {
        }
        
        public CharacterSpellModification(sbyte modificationType, ushort spellId, Types.CharacterBaseCharacteristic value)
        {
            this.ModificationType = modificationType;
            this.SpellId = spellId;
            this.Value = value;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ModificationType);
            writer.WriteVarShort(SpellId);
            Value.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ModificationType = reader.ReadSByte();
            if (ModificationType < 0)
                throw new Exception("Forbidden value on ModificationType = " + ModificationType + ", it doesn't respect the following condition : modificationType < 0");
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            Value = new Types.CharacterBaseCharacteristic();
            Value.Deserialize(reader);
        }
        
    }
    
}