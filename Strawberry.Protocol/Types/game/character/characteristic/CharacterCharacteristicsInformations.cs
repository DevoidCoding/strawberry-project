

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterCharacteristicsInformations
    {
        public const short ProtocolId = 8;
        public virtual short TypeID => ProtocolId;
        
        public ulong Experience { get; set; }
        public ulong ExperienceLevelFloor { get; set; }
        public ulong ExperienceNextLevelFloor { get; set; }
        public ulong ExperienceBonusLimit { get; set; }
        public ulong Kamas { get; set; }
        public ushort StatsPoints { get; set; }
        public ushort AdditionnalPoints { get; set; }
        public ushort SpellsPoints { get; set; }
        public Types.ActorExtendedAlignmentInformations AlignmentInfos { get; set; }
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        public ushort EnergyPoints { get; set; }
        public ushort MaxEnergyPoints { get; set; }
        public short ActionPointsCurrent { get; set; }
        public short MovementPointsCurrent { get; set; }
        public Types.CharacterBaseCharacteristic Initiative { get; set; }
        public Types.CharacterBaseCharacteristic Prospecting { get; set; }
        public Types.CharacterBaseCharacteristic ActionPoints { get; set; }
        public Types.CharacterBaseCharacteristic MovementPoints { get; set; }
        public Types.CharacterBaseCharacteristic Strength { get; set; }
        public Types.CharacterBaseCharacteristic Vitality { get; set; }
        public Types.CharacterBaseCharacteristic Wisdom { get; set; }
        public Types.CharacterBaseCharacteristic Chance { get; set; }
        public Types.CharacterBaseCharacteristic Agility { get; set; }
        public Types.CharacterBaseCharacteristic Intelligence { get; set; }
        public Types.CharacterBaseCharacteristic Range { get; set; }
        public Types.CharacterBaseCharacteristic SummonableCreaturesBoost { get; set; }
        public Types.CharacterBaseCharacteristic Reflect { get; set; }
        public Types.CharacterBaseCharacteristic CriticalHit { get; set; }
        public ushort CriticalHitWeapon { get; set; }
        public Types.CharacterBaseCharacteristic CriticalMiss { get; set; }
        public Types.CharacterBaseCharacteristic HealBonus { get; set; }
        public Types.CharacterBaseCharacteristic AllDamagesBonus { get; set; }
        public Types.CharacterBaseCharacteristic WeaponDamagesBonusPercent { get; set; }
        public Types.CharacterBaseCharacteristic DamagesBonusPercent { get; set; }
        public Types.CharacterBaseCharacteristic TrapBonus { get; set; }
        public Types.CharacterBaseCharacteristic TrapBonusPercent { get; set; }
        public Types.CharacterBaseCharacteristic GlyphBonusPercent { get; set; }
        public Types.CharacterBaseCharacteristic RuneBonusPercent { get; set; }
        public Types.CharacterBaseCharacteristic PermanentDamagePercent { get; set; }
        public Types.CharacterBaseCharacteristic TackleBlock { get; set; }
        public Types.CharacterBaseCharacteristic TackleEvade { get; set; }
        public Types.CharacterBaseCharacteristic PAAttack { get; set; }
        public Types.CharacterBaseCharacteristic PMAttack { get; set; }
        public Types.CharacterBaseCharacteristic PushDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic CriticalDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic NeutralDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic EarthDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic WaterDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic AirDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic FireDamageBonus { get; set; }
        public Types.CharacterBaseCharacteristic DodgePALostProbability { get; set; }
        public Types.CharacterBaseCharacteristic DodgePMLostProbability { get; set; }
        public Types.CharacterBaseCharacteristic NeutralElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic EarthElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic WaterElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic AirElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic FireElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic NeutralElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic EarthElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic WaterElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic AirElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic FireElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic PushDamageReduction { get; set; }
        public Types.CharacterBaseCharacteristic CriticalDamageReduction { get; set; }
        public Types.CharacterBaseCharacteristic PvpNeutralElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic PvpEarthElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic PvpWaterElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic PvpAirElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic PvpFireElementResistPercent { get; set; }
        public Types.CharacterBaseCharacteristic PvpNeutralElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic PvpEarthElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic PvpWaterElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic PvpAirElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic PvpFireElementReduction { get; set; }
        public Types.CharacterBaseCharacteristic MeleeDamageDonePercent { get; set; }
        public Types.CharacterBaseCharacteristic MeleeDamageReceivedPercent { get; set; }
        public Types.CharacterBaseCharacteristic RangedDamageDonePercent { get; set; }
        public Types.CharacterBaseCharacteristic RangedDamageReceivedPercent { get; set; }
        public Types.CharacterBaseCharacteristic WeaponDamageDonePercent { get; set; }
        public Types.CharacterBaseCharacteristic WeaponDamageReceivedPercent { get; set; }
        public Types.CharacterBaseCharacteristic SpellDamageDonePercent { get; set; }
        public Types.CharacterBaseCharacteristic SpellDamageReceivedPercent { get; set; }
        public Types.CharacterSpellModification[] SpellModifications { get; set; }
        public int ProbationTime { get; set; }
        
        public CharacterCharacteristicsInformations()
        {
        }
        
        public CharacterCharacteristicsInformations(ulong experience, ulong experienceLevelFloor, ulong experienceNextLevelFloor, ulong experienceBonusLimit, ulong kamas, ushort statsPoints, ushort additionnalPoints, ushort spellsPoints, Types.ActorExtendedAlignmentInformations alignmentInfos, uint lifePoints, uint maxLifePoints, ushort energyPoints, ushort maxEnergyPoints, short actionPointsCurrent, short movementPointsCurrent, Types.CharacterBaseCharacteristic initiative, Types.CharacterBaseCharacteristic prospecting, Types.CharacterBaseCharacteristic actionPoints, Types.CharacterBaseCharacteristic movementPoints, Types.CharacterBaseCharacteristic strength, Types.CharacterBaseCharacteristic vitality, Types.CharacterBaseCharacteristic wisdom, Types.CharacterBaseCharacteristic chance, Types.CharacterBaseCharacteristic agility, Types.CharacterBaseCharacteristic intelligence, Types.CharacterBaseCharacteristic range, Types.CharacterBaseCharacteristic summonableCreaturesBoost, Types.CharacterBaseCharacteristic reflect, Types.CharacterBaseCharacteristic criticalHit, ushort criticalHitWeapon, Types.CharacterBaseCharacteristic criticalMiss, Types.CharacterBaseCharacteristic healBonus, Types.CharacterBaseCharacteristic allDamagesBonus, Types.CharacterBaseCharacteristic weaponDamagesBonusPercent, Types.CharacterBaseCharacteristic damagesBonusPercent, Types.CharacterBaseCharacteristic trapBonus, Types.CharacterBaseCharacteristic trapBonusPercent, Types.CharacterBaseCharacteristic glyphBonusPercent, Types.CharacterBaseCharacteristic runeBonusPercent, Types.CharacterBaseCharacteristic permanentDamagePercent, Types.CharacterBaseCharacteristic tackleBlock, Types.CharacterBaseCharacteristic tackleEvade, Types.CharacterBaseCharacteristic PAAttack, Types.CharacterBaseCharacteristic PMAttack, Types.CharacterBaseCharacteristic pushDamageBonus, Types.CharacterBaseCharacteristic criticalDamageBonus, Types.CharacterBaseCharacteristic neutralDamageBonus, Types.CharacterBaseCharacteristic earthDamageBonus, Types.CharacterBaseCharacteristic waterDamageBonus, Types.CharacterBaseCharacteristic airDamageBonus, Types.CharacterBaseCharacteristic fireDamageBonus, Types.CharacterBaseCharacteristic dodgePALostProbability, Types.CharacterBaseCharacteristic dodgePMLostProbability, Types.CharacterBaseCharacteristic neutralElementResistPercent, Types.CharacterBaseCharacteristic earthElementResistPercent, Types.CharacterBaseCharacteristic waterElementResistPercent, Types.CharacterBaseCharacteristic airElementResistPercent, Types.CharacterBaseCharacteristic fireElementResistPercent, Types.CharacterBaseCharacteristic neutralElementReduction, Types.CharacterBaseCharacteristic earthElementReduction, Types.CharacterBaseCharacteristic waterElementReduction, Types.CharacterBaseCharacteristic airElementReduction, Types.CharacterBaseCharacteristic fireElementReduction, Types.CharacterBaseCharacteristic pushDamageReduction, Types.CharacterBaseCharacteristic criticalDamageReduction, Types.CharacterBaseCharacteristic pvpNeutralElementResistPercent, Types.CharacterBaseCharacteristic pvpEarthElementResistPercent, Types.CharacterBaseCharacteristic pvpWaterElementResistPercent, Types.CharacterBaseCharacteristic pvpAirElementResistPercent, Types.CharacterBaseCharacteristic pvpFireElementResistPercent, Types.CharacterBaseCharacteristic pvpNeutralElementReduction, Types.CharacterBaseCharacteristic pvpEarthElementReduction, Types.CharacterBaseCharacteristic pvpWaterElementReduction, Types.CharacterBaseCharacteristic pvpAirElementReduction, Types.CharacterBaseCharacteristic pvpFireElementReduction, Types.CharacterBaseCharacteristic meleeDamageDonePercent, Types.CharacterBaseCharacteristic meleeDamageReceivedPercent, Types.CharacterBaseCharacteristic rangedDamageDonePercent, Types.CharacterBaseCharacteristic rangedDamageReceivedPercent, Types.CharacterBaseCharacteristic weaponDamageDonePercent, Types.CharacterBaseCharacteristic weaponDamageReceivedPercent, Types.CharacterBaseCharacteristic spellDamageDonePercent, Types.CharacterBaseCharacteristic spellDamageReceivedPercent, Types.CharacterSpellModification[] spellModifications, int probationTime)
        {
            this.Experience = experience;
            this.ExperienceLevelFloor = experienceLevelFloor;
            this.ExperienceNextLevelFloor = experienceNextLevelFloor;
            this.ExperienceBonusLimit = experienceBonusLimit;
            this.Kamas = kamas;
            this.StatsPoints = statsPoints;
            this.AdditionnalPoints = additionnalPoints;
            this.SpellsPoints = spellsPoints;
            this.AlignmentInfos = alignmentInfos;
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
            this.EnergyPoints = energyPoints;
            this.MaxEnergyPoints = maxEnergyPoints;
            this.ActionPointsCurrent = actionPointsCurrent;
            this.MovementPointsCurrent = movementPointsCurrent;
            this.Initiative = initiative;
            this.Prospecting = prospecting;
            this.ActionPoints = actionPoints;
            this.MovementPoints = movementPoints;
            this.Strength = strength;
            this.Vitality = vitality;
            this.Wisdom = wisdom;
            this.Chance = chance;
            this.Agility = agility;
            this.Intelligence = intelligence;
            this.Range = range;
            this.SummonableCreaturesBoost = summonableCreaturesBoost;
            this.Reflect = reflect;
            this.CriticalHit = criticalHit;
            this.CriticalHitWeapon = criticalHitWeapon;
            this.CriticalMiss = criticalMiss;
            this.HealBonus = healBonus;
            this.AllDamagesBonus = allDamagesBonus;
            this.WeaponDamagesBonusPercent = weaponDamagesBonusPercent;
            this.DamagesBonusPercent = damagesBonusPercent;
            this.TrapBonus = trapBonus;
            this.TrapBonusPercent = trapBonusPercent;
            this.GlyphBonusPercent = glyphBonusPercent;
            this.RuneBonusPercent = runeBonusPercent;
            this.PermanentDamagePercent = permanentDamagePercent;
            this.TackleBlock = tackleBlock;
            this.TackleEvade = tackleEvade;
            this.PAAttack = PAAttack;
            this.PMAttack = PMAttack;
            this.PushDamageBonus = pushDamageBonus;
            this.CriticalDamageBonus = criticalDamageBonus;
            this.NeutralDamageBonus = neutralDamageBonus;
            this.EarthDamageBonus = earthDamageBonus;
            this.WaterDamageBonus = waterDamageBonus;
            this.AirDamageBonus = airDamageBonus;
            this.FireDamageBonus = fireDamageBonus;
            this.DodgePALostProbability = dodgePALostProbability;
            this.DodgePMLostProbability = dodgePMLostProbability;
            this.NeutralElementResistPercent = neutralElementResistPercent;
            this.EarthElementResistPercent = earthElementResistPercent;
            this.WaterElementResistPercent = waterElementResistPercent;
            this.AirElementResistPercent = airElementResistPercent;
            this.FireElementResistPercent = fireElementResistPercent;
            this.NeutralElementReduction = neutralElementReduction;
            this.EarthElementReduction = earthElementReduction;
            this.WaterElementReduction = waterElementReduction;
            this.AirElementReduction = airElementReduction;
            this.FireElementReduction = fireElementReduction;
            this.PushDamageReduction = pushDamageReduction;
            this.CriticalDamageReduction = criticalDamageReduction;
            this.PvpNeutralElementResistPercent = pvpNeutralElementResistPercent;
            this.PvpEarthElementResistPercent = pvpEarthElementResistPercent;
            this.PvpWaterElementResistPercent = pvpWaterElementResistPercent;
            this.PvpAirElementResistPercent = pvpAirElementResistPercent;
            this.PvpFireElementResistPercent = pvpFireElementResistPercent;
            this.PvpNeutralElementReduction = pvpNeutralElementReduction;
            this.PvpEarthElementReduction = pvpEarthElementReduction;
            this.PvpWaterElementReduction = pvpWaterElementReduction;
            this.PvpAirElementReduction = pvpAirElementReduction;
            this.PvpFireElementReduction = pvpFireElementReduction;
            this.MeleeDamageDonePercent = meleeDamageDonePercent;
            this.MeleeDamageReceivedPercent = meleeDamageReceivedPercent;
            this.RangedDamageDonePercent = rangedDamageDonePercent;
            this.RangedDamageReceivedPercent = rangedDamageReceivedPercent;
            this.WeaponDamageDonePercent = weaponDamageDonePercent;
            this.WeaponDamageReceivedPercent = weaponDamageReceivedPercent;
            this.SpellDamageDonePercent = spellDamageDonePercent;
            this.SpellDamageReceivedPercent = spellDamageReceivedPercent;
            this.SpellModifications = spellModifications;
            this.ProbationTime = probationTime;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExperienceLevelFloor);
            writer.WriteVarLong(ExperienceNextLevelFloor);
            writer.WriteVarLong(ExperienceBonusLimit);
            writer.WriteVarLong(Kamas);
            writer.WriteVarShort(StatsPoints);
            writer.WriteVarShort(AdditionnalPoints);
            writer.WriteVarShort(SpellsPoints);
            AlignmentInfos.Serialize(writer);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(EnergyPoints);
            writer.WriteVarShort(MaxEnergyPoints);
            writer.WriteVarShort(ActionPointsCurrent);
            writer.WriteVarShort(MovementPointsCurrent);
            Initiative.Serialize(writer);
            Prospecting.Serialize(writer);
            ActionPoints.Serialize(writer);
            MovementPoints.Serialize(writer);
            Strength.Serialize(writer);
            Vitality.Serialize(writer);
            Wisdom.Serialize(writer);
            Chance.Serialize(writer);
            Agility.Serialize(writer);
            Intelligence.Serialize(writer);
            Range.Serialize(writer);
            SummonableCreaturesBoost.Serialize(writer);
            Reflect.Serialize(writer);
            CriticalHit.Serialize(writer);
            writer.WriteVarShort(CriticalHitWeapon);
            CriticalMiss.Serialize(writer);
            HealBonus.Serialize(writer);
            AllDamagesBonus.Serialize(writer);
            WeaponDamagesBonusPercent.Serialize(writer);
            DamagesBonusPercent.Serialize(writer);
            TrapBonus.Serialize(writer);
            TrapBonusPercent.Serialize(writer);
            GlyphBonusPercent.Serialize(writer);
            RuneBonusPercent.Serialize(writer);
            PermanentDamagePercent.Serialize(writer);
            TackleBlock.Serialize(writer);
            TackleEvade.Serialize(writer);
            PAAttack.Serialize(writer);
            PMAttack.Serialize(writer);
            PushDamageBonus.Serialize(writer);
            CriticalDamageBonus.Serialize(writer);
            NeutralDamageBonus.Serialize(writer);
            EarthDamageBonus.Serialize(writer);
            WaterDamageBonus.Serialize(writer);
            AirDamageBonus.Serialize(writer);
            FireDamageBonus.Serialize(writer);
            DodgePALostProbability.Serialize(writer);
            DodgePMLostProbability.Serialize(writer);
            NeutralElementResistPercent.Serialize(writer);
            EarthElementResistPercent.Serialize(writer);
            WaterElementResistPercent.Serialize(writer);
            AirElementResistPercent.Serialize(writer);
            FireElementResistPercent.Serialize(writer);
            NeutralElementReduction.Serialize(writer);
            EarthElementReduction.Serialize(writer);
            WaterElementReduction.Serialize(writer);
            AirElementReduction.Serialize(writer);
            FireElementReduction.Serialize(writer);
            PushDamageReduction.Serialize(writer);
            CriticalDamageReduction.Serialize(writer);
            PvpNeutralElementResistPercent.Serialize(writer);
            PvpEarthElementResistPercent.Serialize(writer);
            PvpWaterElementResistPercent.Serialize(writer);
            PvpAirElementResistPercent.Serialize(writer);
            PvpFireElementResistPercent.Serialize(writer);
            PvpNeutralElementReduction.Serialize(writer);
            PvpEarthElementReduction.Serialize(writer);
            PvpWaterElementReduction.Serialize(writer);
            PvpAirElementReduction.Serialize(writer);
            PvpFireElementReduction.Serialize(writer);
            MeleeDamageDonePercent.Serialize(writer);
            MeleeDamageReceivedPercent.Serialize(writer);
            RangedDamageDonePercent.Serialize(writer);
            RangedDamageReceivedPercent.Serialize(writer);
            WeaponDamageDonePercent.Serialize(writer);
            WeaponDamageReceivedPercent.Serialize(writer);
            SpellDamageDonePercent.Serialize(writer);
            SpellDamageReceivedPercent.Serialize(writer);
            writer.WriteUShort((ushort)SpellModifications.Length);
            foreach (var entry in SpellModifications)
            {
                 entry.Serialize(writer);
            }
            writer.WriteInt(ProbationTime);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > ulong.MaxValue)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            ExperienceLevelFloor = reader.ReadVarUhLong();
            if (ExperienceLevelFloor < 0 || ExperienceLevelFloor > ulong.MaxValue)
                throw new Exception("Forbidden value on ExperienceLevelFloor = " + ExperienceLevelFloor + ", it doesn't respect the following condition : experienceLevelFloor < 0 || experienceLevelFloor > 9007199254740990");
            ExperienceNextLevelFloor = reader.ReadVarUhLong();
            if (ExperienceNextLevelFloor < 0 || ExperienceNextLevelFloor > ulong.MaxValue)
                throw new Exception("Forbidden value on ExperienceNextLevelFloor = " + ExperienceNextLevelFloor + ", it doesn't respect the following condition : experienceNextLevelFloor < 0 || experienceNextLevelFloor > 9007199254740990");
            ExperienceBonusLimit = reader.ReadVarUhLong();
            if (ExperienceBonusLimit < 0 || ExperienceBonusLimit > ulong.MaxValue)
                throw new Exception("Forbidden value on ExperienceBonusLimit = " + ExperienceBonusLimit + ", it doesn't respect the following condition : experienceBonusLimit < 0 || experienceBonusLimit > 9007199254740990");
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
            StatsPoints = reader.ReadVarUhShort();
            if (StatsPoints < 0)
                throw new Exception("Forbidden value on StatsPoints = " + StatsPoints + ", it doesn't respect the following condition : statsPoints < 0");
            AdditionnalPoints = reader.ReadVarUhShort();
            if (AdditionnalPoints < 0)
                throw new Exception("Forbidden value on AdditionnalPoints = " + AdditionnalPoints + ", it doesn't respect the following condition : additionnalPoints < 0");
            SpellsPoints = reader.ReadVarUhShort();
            if (SpellsPoints < 0)
                throw new Exception("Forbidden value on SpellsPoints = " + SpellsPoints + ", it doesn't respect the following condition : spellsPoints < 0");
            AlignmentInfos = new Types.ActorExtendedAlignmentInformations();
            AlignmentInfos.Deserialize(reader);
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
            EnergyPoints = reader.ReadVarUhShort();
            if (EnergyPoints < 0)
                throw new Exception("Forbidden value on EnergyPoints = " + EnergyPoints + ", it doesn't respect the following condition : energyPoints < 0");
            MaxEnergyPoints = reader.ReadVarUhShort();
            if (MaxEnergyPoints < 0)
                throw new Exception("Forbidden value on MaxEnergyPoints = " + MaxEnergyPoints + ", it doesn't respect the following condition : maxEnergyPoints < 0");
            ActionPointsCurrent = reader.ReadVarShort();
            MovementPointsCurrent = reader.ReadVarShort();
            Initiative = new Types.CharacterBaseCharacteristic();
            Initiative.Deserialize(reader);
            Prospecting = new Types.CharacterBaseCharacteristic();
            Prospecting.Deserialize(reader);
            ActionPoints = new Types.CharacterBaseCharacteristic();
            ActionPoints.Deserialize(reader);
            MovementPoints = new Types.CharacterBaseCharacteristic();
            MovementPoints.Deserialize(reader);
            Strength = new Types.CharacterBaseCharacteristic();
            Strength.Deserialize(reader);
            Vitality = new Types.CharacterBaseCharacteristic();
            Vitality.Deserialize(reader);
            Wisdom = new Types.CharacterBaseCharacteristic();
            Wisdom.Deserialize(reader);
            Chance = new Types.CharacterBaseCharacteristic();
            Chance.Deserialize(reader);
            Agility = new Types.CharacterBaseCharacteristic();
            Agility.Deserialize(reader);
            Intelligence = new Types.CharacterBaseCharacteristic();
            Intelligence.Deserialize(reader);
            Range = new Types.CharacterBaseCharacteristic();
            Range.Deserialize(reader);
            SummonableCreaturesBoost = new Types.CharacterBaseCharacteristic();
            SummonableCreaturesBoost.Deserialize(reader);
            Reflect = new Types.CharacterBaseCharacteristic();
            Reflect.Deserialize(reader);
            CriticalHit = new Types.CharacterBaseCharacteristic();
            CriticalHit.Deserialize(reader);
            CriticalHitWeapon = reader.ReadVarUhShort();
            if (CriticalHitWeapon < 0)
                throw new Exception("Forbidden value on CriticalHitWeapon = " + CriticalHitWeapon + ", it doesn't respect the following condition : criticalHitWeapon < 0");
            CriticalMiss = new Types.CharacterBaseCharacteristic();
            CriticalMiss.Deserialize(reader);
            HealBonus = new Types.CharacterBaseCharacteristic();
            HealBonus.Deserialize(reader);
            AllDamagesBonus = new Types.CharacterBaseCharacteristic();
            AllDamagesBonus.Deserialize(reader);
            WeaponDamagesBonusPercent = new Types.CharacterBaseCharacteristic();
            WeaponDamagesBonusPercent.Deserialize(reader);
            DamagesBonusPercent = new Types.CharacterBaseCharacteristic();
            DamagesBonusPercent.Deserialize(reader);
            TrapBonus = new Types.CharacterBaseCharacteristic();
            TrapBonus.Deserialize(reader);
            TrapBonusPercent = new Types.CharacterBaseCharacteristic();
            TrapBonusPercent.Deserialize(reader);
            GlyphBonusPercent = new Types.CharacterBaseCharacteristic();
            GlyphBonusPercent.Deserialize(reader);
            RuneBonusPercent = new Types.CharacterBaseCharacteristic();
            RuneBonusPercent.Deserialize(reader);
            PermanentDamagePercent = new Types.CharacterBaseCharacteristic();
            PermanentDamagePercent.Deserialize(reader);
            TackleBlock = new Types.CharacterBaseCharacteristic();
            TackleBlock.Deserialize(reader);
            TackleEvade = new Types.CharacterBaseCharacteristic();
            TackleEvade.Deserialize(reader);
            PAAttack = new Types.CharacterBaseCharacteristic();
            PAAttack.Deserialize(reader);
            PMAttack = new Types.CharacterBaseCharacteristic();
            PMAttack.Deserialize(reader);
            PushDamageBonus = new Types.CharacterBaseCharacteristic();
            PushDamageBonus.Deserialize(reader);
            CriticalDamageBonus = new Types.CharacterBaseCharacteristic();
            CriticalDamageBonus.Deserialize(reader);
            NeutralDamageBonus = new Types.CharacterBaseCharacteristic();
            NeutralDamageBonus.Deserialize(reader);
            EarthDamageBonus = new Types.CharacterBaseCharacteristic();
            EarthDamageBonus.Deserialize(reader);
            WaterDamageBonus = new Types.CharacterBaseCharacteristic();
            WaterDamageBonus.Deserialize(reader);
            AirDamageBonus = new Types.CharacterBaseCharacteristic();
            AirDamageBonus.Deserialize(reader);
            FireDamageBonus = new Types.CharacterBaseCharacteristic();
            FireDamageBonus.Deserialize(reader);
            DodgePALostProbability = new Types.CharacterBaseCharacteristic();
            DodgePALostProbability.Deserialize(reader);
            DodgePMLostProbability = new Types.CharacterBaseCharacteristic();
            DodgePMLostProbability.Deserialize(reader);
            NeutralElementResistPercent = new Types.CharacterBaseCharacteristic();
            NeutralElementResistPercent.Deserialize(reader);
            EarthElementResistPercent = new Types.CharacterBaseCharacteristic();
            EarthElementResistPercent.Deserialize(reader);
            WaterElementResistPercent = new Types.CharacterBaseCharacteristic();
            WaterElementResistPercent.Deserialize(reader);
            AirElementResistPercent = new Types.CharacterBaseCharacteristic();
            AirElementResistPercent.Deserialize(reader);
            FireElementResistPercent = new Types.CharacterBaseCharacteristic();
            FireElementResistPercent.Deserialize(reader);
            NeutralElementReduction = new Types.CharacterBaseCharacteristic();
            NeutralElementReduction.Deserialize(reader);
            EarthElementReduction = new Types.CharacterBaseCharacteristic();
            EarthElementReduction.Deserialize(reader);
            WaterElementReduction = new Types.CharacterBaseCharacteristic();
            WaterElementReduction.Deserialize(reader);
            AirElementReduction = new Types.CharacterBaseCharacteristic();
            AirElementReduction.Deserialize(reader);
            FireElementReduction = new Types.CharacterBaseCharacteristic();
            FireElementReduction.Deserialize(reader);
            PushDamageReduction = new Types.CharacterBaseCharacteristic();
            PushDamageReduction.Deserialize(reader);
            CriticalDamageReduction = new Types.CharacterBaseCharacteristic();
            CriticalDamageReduction.Deserialize(reader);
            PvpNeutralElementResistPercent = new Types.CharacterBaseCharacteristic();
            PvpNeutralElementResistPercent.Deserialize(reader);
            PvpEarthElementResistPercent = new Types.CharacterBaseCharacteristic();
            PvpEarthElementResistPercent.Deserialize(reader);
            PvpWaterElementResistPercent = new Types.CharacterBaseCharacteristic();
            PvpWaterElementResistPercent.Deserialize(reader);
            PvpAirElementResistPercent = new Types.CharacterBaseCharacteristic();
            PvpAirElementResistPercent.Deserialize(reader);
            PvpFireElementResistPercent = new Types.CharacterBaseCharacteristic();
            PvpFireElementResistPercent.Deserialize(reader);
            PvpNeutralElementReduction = new Types.CharacterBaseCharacteristic();
            PvpNeutralElementReduction.Deserialize(reader);
            PvpEarthElementReduction = new Types.CharacterBaseCharacteristic();
            PvpEarthElementReduction.Deserialize(reader);
            PvpWaterElementReduction = new Types.CharacterBaseCharacteristic();
            PvpWaterElementReduction.Deserialize(reader);
            PvpAirElementReduction = new Types.CharacterBaseCharacteristic();
            PvpAirElementReduction.Deserialize(reader);
            PvpFireElementReduction = new Types.CharacterBaseCharacteristic();
            PvpFireElementReduction.Deserialize(reader);
            MeleeDamageDonePercent = new Types.CharacterBaseCharacteristic();
            MeleeDamageDonePercent.Deserialize(reader);
            MeleeDamageReceivedPercent = new Types.CharacterBaseCharacteristic();
            MeleeDamageReceivedPercent.Deserialize(reader);
            RangedDamageDonePercent = new Types.CharacterBaseCharacteristic();
            RangedDamageDonePercent.Deserialize(reader);
            RangedDamageReceivedPercent = new Types.CharacterBaseCharacteristic();
            RangedDamageReceivedPercent.Deserialize(reader);
            WeaponDamageDonePercent = new Types.CharacterBaseCharacteristic();
            WeaponDamageDonePercent.Deserialize(reader);
            WeaponDamageReceivedPercent = new Types.CharacterBaseCharacteristic();
            WeaponDamageReceivedPercent.Deserialize(reader);
            SpellDamageDonePercent = new Types.CharacterBaseCharacteristic();
            SpellDamageDonePercent.Deserialize(reader);
            SpellDamageReceivedPercent = new Types.CharacterBaseCharacteristic();
            SpellDamageReceivedPercent.Deserialize(reader);
            var limit = reader.ReadUShort();
            SpellModifications = new Types.CharacterSpellModification[limit];
            for (int i = 0; i < limit; i++)
            {
                 SpellModifications[i] = new Types.CharacterSpellModification();
                 SpellModifications[i].Deserialize(reader);
            }
            ProbationTime = reader.ReadInt();
            if (ProbationTime < 0)
                throw new Exception("Forbidden value on ProbationTime = " + ProbationTime + ", it doesn't respect the following condition : probationTime < 0");
        }
        
    }
    
}