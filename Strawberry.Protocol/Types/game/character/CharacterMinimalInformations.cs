

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterMinimalInformations : CharacterBasicMinimalInformations
    {
        public new const short ProtocolId = 110;
        public override short TypeID => ProtocolId;
        
        public ushort Level { get; set; }
        
        public CharacterMinimalInformations()
        {
        }
        
        public CharacterMinimalInformations(ulong id, string name, ushort level)
         : base(id, name)
        {
            this.Level = level;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
        }
        
    }
    
}