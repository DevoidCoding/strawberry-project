

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AbstractCharacterInformation
    {
        public const short ProtocolId = 400;
        public virtual short TypeID => ProtocolId;
        
        public ulong Id { get; set; }
        
        public AbstractCharacterInformation()
        {
        }
        
        public AbstractCharacterInformation(ulong id)
        {
            this.Id = id;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Id);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhLong();
            if (Id < 0 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0 || id > 9007199254740990");
        }
        
    }
    
}