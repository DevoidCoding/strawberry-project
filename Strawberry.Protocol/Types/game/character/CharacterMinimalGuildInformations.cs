

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterMinimalGuildInformations : CharacterMinimalPlusLookInformations
    {
        public new const short ProtocolId = 445;
        public override short TypeID => ProtocolId;
        
        public Types.BasicGuildInformations Guild { get; set; }
        
        public CharacterMinimalGuildInformations()
        {
        }
        
        public CharacterMinimalGuildInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed, Types.BasicGuildInformations guild)
         : base(id, name, level, entityLook, breed)
        {
            this.Guild = guild;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Guild.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Guild = new Types.BasicGuildInformations();
            Guild.Deserialize(reader);
        }
        
    }
    
}