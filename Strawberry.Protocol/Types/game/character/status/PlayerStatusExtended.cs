

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PlayerStatusExtended : PlayerStatus
    {
        public new const short ProtocolId = 414;
        public override short TypeID => ProtocolId;
        
        public string Message { get; set; }
        
        public PlayerStatusExtended()
        {
        }
        
        public PlayerStatusExtended(sbyte statusId, string message)
         : base(statusId)
        {
            this.Message = message;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Message);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Message = reader.ReadUTF();
        }
        
    }
    
}