

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PlayerStatus
    {
        public const short ProtocolId = 415;
        public virtual short TypeID => ProtocolId;
        
        public sbyte StatusId { get; set; }
        
        public PlayerStatus()
        {
        }
        
        public PlayerStatus(sbyte statusId)
        {
            this.StatusId = statusId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(StatusId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            StatusId = reader.ReadSByte();
            if (StatusId < 0)
                throw new Exception("Forbidden value on StatusId = " + StatusId + ", it doesn't respect the following condition : statusId < 0");
        }
        
    }
    
}