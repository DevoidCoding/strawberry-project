

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ActorExtendedAlignmentInformations : ActorAlignmentInformations
    {
        public new const short ProtocolId = 202;
        public override short TypeID => ProtocolId;
        
        public ushort Honor { get; set; }
        public ushort HonorGradeFloor { get; set; }
        public ushort HonorNextGradeFloor { get; set; }
        public sbyte Aggressable { get; set; }
        
        public ActorExtendedAlignmentInformations()
        {
        }
        
        public ActorExtendedAlignmentInformations(sbyte alignmentSide, sbyte alignmentValue, sbyte alignmentGrade, double characterPower, ushort honor, ushort honorGradeFloor, ushort honorNextGradeFloor, sbyte aggressable)
         : base(alignmentSide, alignmentValue, alignmentGrade, characterPower)
        {
            this.Honor = honor;
            this.HonorGradeFloor = honorGradeFloor;
            this.HonorNextGradeFloor = honorNextGradeFloor;
            this.Aggressable = aggressable;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Honor);
            writer.WriteVarShort(HonorGradeFloor);
            writer.WriteVarShort(HonorNextGradeFloor);
            writer.WriteSByte(Aggressable);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Honor = reader.ReadVarUhShort();
            if (Honor < 0 || Honor > 20000)
                throw new Exception("Forbidden value on Honor = " + Honor + ", it doesn't respect the following condition : honor < 0 || honor > 20000");
            HonorGradeFloor = reader.ReadVarUhShort();
            if (HonorGradeFloor < 0 || HonorGradeFloor > 20000)
                throw new Exception("Forbidden value on HonorGradeFloor = " + HonorGradeFloor + ", it doesn't respect the following condition : honorGradeFloor < 0 || honorGradeFloor > 20000");
            HonorNextGradeFloor = reader.ReadVarUhShort();
            if (HonorNextGradeFloor < 0 || HonorNextGradeFloor > 20000)
                throw new Exception("Forbidden value on HonorNextGradeFloor = " + HonorNextGradeFloor + ", it doesn't respect the following condition : honorNextGradeFloor < 0 || honorNextGradeFloor > 20000");
            Aggressable = reader.ReadSByte();
            if (Aggressable < 0)
                throw new Exception("Forbidden value on Aggressable = " + Aggressable + ", it doesn't respect the following condition : aggressable < 0");
        }
        
    }
    
}