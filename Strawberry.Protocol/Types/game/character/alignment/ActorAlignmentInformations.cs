

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ActorAlignmentInformations
    {
        public const short ProtocolId = 201;
        public virtual short TypeID => ProtocolId;
        
        public sbyte AlignmentSide { get; set; }
        public sbyte AlignmentValue { get; set; }
        public sbyte AlignmentGrade { get; set; }
        public double CharacterPower { get; set; }
        
        public ActorAlignmentInformations()
        {
        }
        
        public ActorAlignmentInformations(sbyte alignmentSide, sbyte alignmentValue, sbyte alignmentGrade, double characterPower)
        {
            this.AlignmentSide = alignmentSide;
            this.AlignmentValue = alignmentValue;
            this.AlignmentGrade = alignmentGrade;
            this.CharacterPower = characterPower;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(AlignmentSide);
            writer.WriteSByte(AlignmentValue);
            writer.WriteSByte(AlignmentGrade);
            writer.WriteDouble(CharacterPower);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            AlignmentSide = reader.ReadSByte();
            AlignmentValue = reader.ReadSByte();
            if (AlignmentValue < 0)
                throw new Exception("Forbidden value on AlignmentValue = " + AlignmentValue + ", it doesn't respect the following condition : alignmentValue < 0");
            AlignmentGrade = reader.ReadSByte();
            if (AlignmentGrade < 0)
                throw new Exception("Forbidden value on AlignmentGrade = " + AlignmentGrade + ", it doesn't respect the following condition : alignmentGrade < 0");
            CharacterPower = reader.ReadDouble();
            if (CharacterPower < -9007199254740990 || CharacterPower > 9007199254740990)
                throw new Exception("Forbidden value on CharacterPower = " + CharacterPower + ", it doesn't respect the following condition : characterPower < -9007199254740990 || characterPower > 9007199254740990");
        }
        
    }
    
}