

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ActorRestrictionsInformations
    {
        public const short ProtocolId = 204;
        public virtual short TypeID => ProtocolId;
        
        public bool CantBeAggressed { get; set; }
        public bool CantBeChallenged { get; set; }
        public bool CantTrade { get; set; }
        public bool CantBeAttackedByMutant { get; set; }
        public bool CantRun { get; set; }
        public bool ForceSlowWalk { get; set; }
        public bool CantMinimize { get; set; }
        public bool CantMove { get; set; }
        public bool CantAggress { get; set; }
        public bool CantChallenge { get; set; }
        public bool CantExchange { get; set; }
        public bool CantAttack { get; set; }
        public bool CantChat { get; set; }
        public bool CantBeMerchant { get; set; }
        public bool CantUseObject { get; set; }
        public bool CantUseTaxCollector { get; set; }
        public bool CantUseInteractive { get; set; }
        public bool CantSpeakToNPC { get; set; }
        public bool CantChangeZone { get; set; }
        public bool CantAttackMonster { get; set; }
        public bool CantWalk8Directions { get; set; }
        
        public ActorRestrictionsInformations()
        {
        }
        
        public ActorRestrictionsInformations(bool cantBeAggressed, bool cantBeChallenged, bool cantTrade, bool cantBeAttackedByMutant, bool cantRun, bool forceSlowWalk, bool cantMinimize, bool cantMove, bool cantAggress, bool cantChallenge, bool cantExchange, bool cantAttack, bool cantChat, bool cantBeMerchant, bool cantUseObject, bool cantUseTaxCollector, bool cantUseInteractive, bool cantSpeakToNPC, bool cantChangeZone, bool cantAttackMonster, bool cantWalk8Directions)
        {
            this.CantBeAggressed = cantBeAggressed;
            this.CantBeChallenged = cantBeChallenged;
            this.CantTrade = cantTrade;
            this.CantBeAttackedByMutant = cantBeAttackedByMutant;
            this.CantRun = cantRun;
            this.ForceSlowWalk = forceSlowWalk;
            this.CantMinimize = cantMinimize;
            this.CantMove = cantMove;
            this.CantAggress = cantAggress;
            this.CantChallenge = cantChallenge;
            this.CantExchange = cantExchange;
            this.CantAttack = cantAttack;
            this.CantChat = cantChat;
            this.CantBeMerchant = cantBeMerchant;
            this.CantUseObject = cantUseObject;
            this.CantUseTaxCollector = cantUseTaxCollector;
            this.CantUseInteractive = cantUseInteractive;
            this.CantSpeakToNPC = cantSpeakToNPC;
            this.CantChangeZone = cantChangeZone;
            this.CantAttackMonster = cantAttackMonster;
            this.CantWalk8Directions = cantWalk8Directions;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, CantBeAggressed);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CantBeChallenged);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, CantTrade);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, CantBeAttackedByMutant);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 4, CantRun);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 5, ForceSlowWalk);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 6, CantMinimize);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 7, CantMove);
            writer.WriteByte(flag1);
            byte flag2 = 0;
            flag2 = BooleanByteWrapper.SetFlag(flag2, 0, CantAggress);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 1, CantChallenge);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 2, CantExchange);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 3, CantAttack);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 4, CantChat);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 5, CantBeMerchant);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 6, CantUseObject);
            flag2 = BooleanByteWrapper.SetFlag(flag2, 7, CantUseTaxCollector);
            writer.WriteByte(flag2);
            byte flag3 = 0;
            flag3 = BooleanByteWrapper.SetFlag(flag3, 0, CantUseInteractive);
            flag3 = BooleanByteWrapper.SetFlag(flag3, 1, CantSpeakToNPC);
            flag3 = BooleanByteWrapper.SetFlag(flag3, 2, CantChangeZone);
            flag3 = BooleanByteWrapper.SetFlag(flag3, 3, CantAttackMonster);
            flag3 = BooleanByteWrapper.SetFlag(flag3, 4, CantWalk8Directions);
            writer.WriteByte(flag3);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            CantBeAggressed = BooleanByteWrapper.GetFlag(flag1, 0);
            CantBeChallenged = BooleanByteWrapper.GetFlag(flag1, 1);
            CantTrade = BooleanByteWrapper.GetFlag(flag1, 2);
            CantBeAttackedByMutant = BooleanByteWrapper.GetFlag(flag1, 3);
            CantRun = BooleanByteWrapper.GetFlag(flag1, 4);
            ForceSlowWalk = BooleanByteWrapper.GetFlag(flag1, 5);
            CantMinimize = BooleanByteWrapper.GetFlag(flag1, 6);
            CantMove = BooleanByteWrapper.GetFlag(flag1, 7);
            byte flag2 = reader.ReadByte();
            CantAggress = BooleanByteWrapper.GetFlag(flag2, 0);
            CantChallenge = BooleanByteWrapper.GetFlag(flag2, 1);
            CantExchange = BooleanByteWrapper.GetFlag(flag2, 2);
            CantAttack = BooleanByteWrapper.GetFlag(flag2, 3);
            CantChat = BooleanByteWrapper.GetFlag(flag2, 4);
            CantBeMerchant = BooleanByteWrapper.GetFlag(flag2, 5);
            CantUseObject = BooleanByteWrapper.GetFlag(flag2, 6);
            CantUseTaxCollector = BooleanByteWrapper.GetFlag(flag2, 7);
            byte flag3 = reader.ReadByte();
            CantUseInteractive = BooleanByteWrapper.GetFlag(flag3, 0);
            CantSpeakToNPC = BooleanByteWrapper.GetFlag(flag3, 1);
            CantChangeZone = BooleanByteWrapper.GetFlag(flag3, 2);
            CantAttackMonster = BooleanByteWrapper.GetFlag(flag3, 3);
            CantWalk8Directions = BooleanByteWrapper.GetFlag(flag3, 4);
        }
        
    }
    
}