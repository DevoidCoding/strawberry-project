

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterMinimalPlusLookInformations : CharacterMinimalInformations
    {
        public new const short ProtocolId = 163;
        public override short TypeID => ProtocolId;
        
        public Types.EntityLook EntityLook { get; set; }
        public sbyte Breed { get; set; }
        
        public CharacterMinimalPlusLookInformations()
        {
        }
        
        public CharacterMinimalPlusLookInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed)
         : base(id, name, level)
        {
            this.EntityLook = entityLook;
            this.Breed = breed;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            EntityLook.Serialize(writer);
            writer.WriteSByte(Breed);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            EntityLook = new Types.EntityLook();
            EntityLook.Deserialize(reader);
            Breed = reader.ReadSByte();
        }
        
    }
    
}