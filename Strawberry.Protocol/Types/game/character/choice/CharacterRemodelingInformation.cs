

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterRemodelingInformation : AbstractCharacterInformation
    {
        public new const short ProtocolId = 479;
        public override short TypeID => ProtocolId;
        
        public string Name { get; set; }
        public sbyte Breed { get; set; }
        public bool Sex { get; set; }
        public ushort CosmeticId { get; set; }
        public int[] Colors { get; set; }
        
        public CharacterRemodelingInformation()
        {
        }
        
        public CharacterRemodelingInformation(ulong id, string name, sbyte breed, bool sex, ushort cosmeticId, int[] colors)
         : base(id)
        {
            this.Name = name;
            this.Breed = breed;
            this.Sex = sex;
            this.CosmeticId = cosmeticId;
            this.Colors = colors;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteSByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(CosmeticId);
            writer.WriteUShort((ushort)Colors.Length);
            foreach (var entry in Colors)
            {
                 writer.WriteInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Breed = reader.ReadSByte();
            Sex = reader.ReadBoolean();
            CosmeticId = reader.ReadVarUhShort();
            if (CosmeticId < 0)
                throw new Exception("Forbidden value on CosmeticId = " + CosmeticId + ", it doesn't respect the following condition : cosmeticId < 0");
            var limit = reader.ReadUShort();
            Colors = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 Colors[i] = reader.ReadInt();
            }
        }
        
    }
    
}