

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterHardcoreOrEpicInformations : CharacterBaseInformations
    {
        public new const short ProtocolId = 474;
        public override short TypeID => ProtocolId;
        
        public sbyte DeathState { get; set; }
        public ushort DeathCount { get; set; }
        public ushort DeathMaxLevel { get; set; }
        
        public CharacterHardcoreOrEpicInformations()
        {
        }
        
        public CharacterHardcoreOrEpicInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed, bool sex, sbyte deathState, ushort deathCount, ushort deathMaxLevel)
         : base(id, name, level, entityLook, breed, sex)
        {
            this.DeathState = deathState;
            this.DeathCount = deathCount;
            this.DeathMaxLevel = deathMaxLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(DeathState);
            writer.WriteVarShort(DeathCount);
            writer.WriteVarShort(DeathMaxLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            DeathState = reader.ReadSByte();
            if (DeathState < 0)
                throw new Exception("Forbidden value on DeathState = " + DeathState + ", it doesn't respect the following condition : deathState < 0");
            DeathCount = reader.ReadVarUhShort();
            if (DeathCount < 0)
                throw new Exception("Forbidden value on DeathCount = " + DeathCount + ", it doesn't respect the following condition : deathCount < 0");
            DeathMaxLevel = reader.ReadVarUhShort();
            if (DeathMaxLevel < 0)
                throw new Exception("Forbidden value on DeathMaxLevel = " + DeathMaxLevel + ", it doesn't respect the following condition : deathMaxLevel < 0");
        }
        
    }
    
}