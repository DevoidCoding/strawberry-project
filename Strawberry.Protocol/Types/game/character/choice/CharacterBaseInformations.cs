

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterBaseInformations : CharacterMinimalPlusLookInformations
    {
        public new const short ProtocolId = 45;
        public override short TypeID => ProtocolId;
        
        public bool Sex { get; set; }
        
        public CharacterBaseInformations()
        {
        }
        
        public CharacterBaseInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed, bool sex)
         : base(id, name, level, entityLook, breed)
        {
            this.Sex = sex;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Sex);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Sex = reader.ReadBoolean();
        }
        
    }
    
}