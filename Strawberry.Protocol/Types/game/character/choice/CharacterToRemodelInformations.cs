

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class CharacterToRemodelInformations : CharacterRemodelingInformation
    {
        public new const short ProtocolId = 477;
        public override short TypeID => ProtocolId;
        
        public sbyte PossibleChangeMask { get; set; }
        public sbyte MandatoryChangeMask { get; set; }
        
        public CharacterToRemodelInformations()
        {
        }
        
        public CharacterToRemodelInformations(ulong id, string name, sbyte breed, bool sex, ushort cosmeticId, int[] colors, sbyte possibleChangeMask, sbyte mandatoryChangeMask)
         : base(id, name, breed, sex, cosmeticId, colors)
        {
            this.PossibleChangeMask = possibleChangeMask;
            this.MandatoryChangeMask = mandatoryChangeMask;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PossibleChangeMask);
            writer.WriteSByte(MandatoryChangeMask);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PossibleChangeMask = reader.ReadSByte();
            if (PossibleChangeMask < 0)
                throw new Exception("Forbidden value on PossibleChangeMask = " + PossibleChangeMask + ", it doesn't respect the following condition : possibleChangeMask < 0");
            MandatoryChangeMask = reader.ReadSByte();
            if (MandatoryChangeMask < 0)
                throw new Exception("Forbidden value on MandatoryChangeMask = " + MandatoryChangeMask + ", it doesn't respect the following condition : mandatoryChangeMask < 0");
        }
        
    }
    
}