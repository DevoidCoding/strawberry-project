

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class IndexedEntityLook
    {
        public const short ProtocolId = 405;
        public virtual short TypeID => ProtocolId;
        
        public Types.EntityLook Look { get; set; }
        public sbyte Index { get; set; }
        
        public IndexedEntityLook()
        {
        }
        
        public IndexedEntityLook(Types.EntityLook look, sbyte index)
        {
            this.Look = look;
            this.Index = index;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            Look.Serialize(writer);
            writer.WriteSByte(Index);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
            Index = reader.ReadSByte();
            if (Index < 0)
                throw new Exception("Forbidden value on Index = " + Index + ", it doesn't respect the following condition : index < 0");
        }
        
    }
    
}