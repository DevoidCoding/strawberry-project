

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class EntityLook
    {
        public const short ProtocolId = 55;
        public virtual short TypeID => ProtocolId;
        
        public ushort BonesId { get; set; }
        public ushort[] Skins { get; set; }
        public int[] IndexedColors { get; set; }
        public short[] Scales { get; set; }
        public Types.SubEntity[] Subentities { get; set; }
        
        public EntityLook()
        {
        }
        
        public EntityLook(ushort bonesId, ushort[] skins, int[] indexedColors, short[] scales, Types.SubEntity[] subentities)
        {
            this.BonesId = bonesId;
            this.Skins = skins;
            this.IndexedColors = indexedColors;
            this.Scales = scales;
            this.Subentities = subentities;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(BonesId);
            writer.WriteUShort((ushort)Skins.Length);
            foreach (var entry in Skins)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)IndexedColors.Length);
            foreach (var entry in IndexedColors)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUShort((ushort)Scales.Length);
            foreach (var entry in Scales)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)Subentities.Length);
            foreach (var entry in Subentities)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            BonesId = reader.ReadVarUhShort();
            if (BonesId < 0)
                throw new Exception("Forbidden value on BonesId = " + BonesId + ", it doesn't respect the following condition : bonesId < 0");
            var limit = reader.ReadUShort();
            Skins = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Skins[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            IndexedColors = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 IndexedColors[i] = reader.ReadInt();
            }
            limit = reader.ReadUShort();
            Scales = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 Scales[i] = reader.ReadVarShort();
            }
            limit = reader.ReadUShort();
            Subentities = new Types.SubEntity[limit];
            for (int i = 0; i < limit; i++)
            {
                 Subentities[i] = new Types.SubEntity();
                 Subentities[i].Deserialize(reader);
            }
        }
        
    }
    
}