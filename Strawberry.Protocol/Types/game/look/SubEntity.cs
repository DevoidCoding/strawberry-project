

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SubEntity
    {
        public const short ProtocolId = 54;
        public virtual short TypeID => ProtocolId;
        
        public sbyte BindingPointCategory { get; set; }
        public sbyte BindingPointIndex { get; set; }
        public Types.EntityLook SubEntityLook { get; set; }
        
        public SubEntity()
        {
        }
        
        public SubEntity(sbyte bindingPointCategory, sbyte bindingPointIndex, Types.EntityLook subEntityLook)
        {
            this.BindingPointCategory = bindingPointCategory;
            this.BindingPointIndex = bindingPointIndex;
            this.SubEntityLook = subEntityLook;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(BindingPointCategory);
            writer.WriteSByte(BindingPointIndex);
            SubEntityLook.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            BindingPointCategory = reader.ReadSByte();
            if (BindingPointCategory < 0)
                throw new Exception("Forbidden value on BindingPointCategory = " + BindingPointCategory + ", it doesn't respect the following condition : bindingPointCategory < 0");
            BindingPointIndex = reader.ReadSByte();
            if (BindingPointIndex < 0)
                throw new Exception("Forbidden value on BindingPointIndex = " + BindingPointIndex + ", it doesn't respect the following condition : bindingPointIndex < 0");
            SubEntityLook = new Types.EntityLook();
            SubEntityLook.Deserialize(reader);
        }
        
    }
    
}