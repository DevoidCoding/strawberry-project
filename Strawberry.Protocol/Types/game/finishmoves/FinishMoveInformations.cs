

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FinishMoveInformations
    {
        public const short ProtocolId = 506;
        public virtual short TypeID => ProtocolId;
        
        public int FinishMoveId { get; set; }
        public bool FinishMoveState { get; set; }
        
        public FinishMoveInformations()
        {
        }
        
        public FinishMoveInformations(int finishMoveId, bool finishMoveState)
        {
            this.FinishMoveId = finishMoveId;
            this.FinishMoveState = finishMoveState;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(FinishMoveId);
            writer.WriteBoolean(FinishMoveState);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            FinishMoveId = reader.ReadInt();
            if (FinishMoveId < 0)
                throw new Exception("Forbidden value on FinishMoveId = " + FinishMoveId + ", it doesn't respect the following condition : finishMoveId < 0");
            FinishMoveState = reader.ReadBoolean();
        }
        
    }
    
}