

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AllianceVersatileInformations
    {
        public const short ProtocolId = 432;
        public virtual short TypeID => ProtocolId;
        
        public uint AllianceId { get; set; }
        public ushort NbGuilds { get; set; }
        public ushort NbMembers { get; set; }
        public ushort NbSubarea { get; set; }
        
        public AllianceVersatileInformations()
        {
        }
        
        public AllianceVersatileInformations(uint allianceId, ushort nbGuilds, ushort nbMembers, ushort nbSubarea)
        {
            this.AllianceId = allianceId;
            this.NbGuilds = nbGuilds;
            this.NbMembers = nbMembers;
            this.NbSubarea = nbSubarea;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(AllianceId);
            writer.WriteVarShort(NbGuilds);
            writer.WriteVarShort(NbMembers);
            writer.WriteVarShort(NbSubarea);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            AllianceId = reader.ReadVarUhInt();
            if (AllianceId < 0)
                throw new Exception("Forbidden value on AllianceId = " + AllianceId + ", it doesn't respect the following condition : allianceId < 0");
            NbGuilds = reader.ReadVarUhShort();
            if (NbGuilds < 0)
                throw new Exception("Forbidden value on NbGuilds = " + NbGuilds + ", it doesn't respect the following condition : nbGuilds < 0");
            NbMembers = reader.ReadVarUhShort();
            if (NbMembers < 0)
                throw new Exception("Forbidden value on NbMembers = " + NbMembers + ", it doesn't respect the following condition : nbMembers < 0");
            NbSubarea = reader.ReadVarUhShort();
            if (NbSubarea < 0)
                throw new Exception("Forbidden value on NbSubarea = " + NbSubarea + ", it doesn't respect the following condition : nbSubarea < 0");
        }
        
    }
    
}