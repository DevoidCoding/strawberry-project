

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildVersatileInformations
    {
        public const short ProtocolId = 435;
        public virtual short TypeID => ProtocolId;
        
        public uint GuildId { get; set; }
        public ulong LeaderId { get; set; }
        public byte GuildLevel { get; set; }
        public byte NbMembers { get; set; }
        
        public GuildVersatileInformations()
        {
        }
        
        public GuildVersatileInformations(uint guildId, ulong leaderId, byte guildLevel, byte nbMembers)
        {
            this.GuildId = guildId;
            this.LeaderId = leaderId;
            this.GuildLevel = guildLevel;
            this.NbMembers = nbMembers;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(GuildId);
            writer.WriteVarLong(LeaderId);
            writer.WriteByte(GuildLevel);
            writer.WriteByte(NbMembers);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
            LeaderId = reader.ReadVarUhLong();
            if (LeaderId < 0 || LeaderId > 9007199254740990)
                throw new Exception("Forbidden value on LeaderId = " + LeaderId + ", it doesn't respect the following condition : leaderId < 0 || leaderId > 9007199254740990");
            GuildLevel = reader.ReadByte();
            if (GuildLevel < 1 || GuildLevel > 200)
                throw new Exception("Forbidden value on GuildLevel = " + GuildLevel + ", it doesn't respect the following condition : guildLevel < 1 || guildLevel > 200");
            NbMembers = reader.ReadByte();
            if (NbMembers < 1 || NbMembers > 240)
                throw new Exception("Forbidden value on NbMembers = " + NbMembers + ", it doesn't respect the following condition : nbMembers < 1 || nbMembers > 240");
        }
        
    }
    
}