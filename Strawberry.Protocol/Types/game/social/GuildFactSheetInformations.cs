

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildFactSheetInformations : GuildInformations
    {
        public new const short ProtocolId = 424;
        public override short TypeID => ProtocolId;
        
        public ulong LeaderId { get; set; }
        public ushort NbMembers { get; set; }
        
        public GuildFactSheetInformations()
        {
        }
        
        public GuildFactSheetInformations(uint guildId, string guildName, byte guildLevel, Types.GuildEmblem guildEmblem, ulong leaderId, ushort nbMembers)
         : base(guildId, guildName, guildLevel, guildEmblem)
        {
            this.LeaderId = leaderId;
            this.NbMembers = nbMembers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(LeaderId);
            writer.WriteVarShort(NbMembers);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LeaderId = reader.ReadVarUhLong();
            if (LeaderId < 0 || LeaderId > 9007199254740990)
                throw new Exception("Forbidden value on LeaderId = " + LeaderId + ", it doesn't respect the following condition : leaderId < 0 || leaderId > 9007199254740990");
            NbMembers = reader.ReadVarUhShort();
            if (NbMembers < 0)
                throw new Exception("Forbidden value on NbMembers = " + NbMembers + ", it doesn't respect the following condition : nbMembers < 0");
        }
        
    }
    
}