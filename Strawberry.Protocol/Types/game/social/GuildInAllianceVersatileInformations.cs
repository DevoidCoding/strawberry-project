

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildInAllianceVersatileInformations : GuildVersatileInformations
    {
        public new const short ProtocolId = 437;
        public override short TypeID => ProtocolId;
        
        public uint AllianceId { get; set; }
        
        public GuildInAllianceVersatileInformations()
        {
        }
        
        public GuildInAllianceVersatileInformations(uint guildId, ulong leaderId, byte guildLevel, byte nbMembers, uint allianceId)
         : base(guildId, leaderId, guildLevel, nbMembers)
        {
            this.AllianceId = allianceId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(AllianceId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AllianceId = reader.ReadVarUhInt();
            if (AllianceId < 0)
                throw new Exception("Forbidden value on AllianceId = " + AllianceId + ", it doesn't respect the following condition : allianceId < 0");
        }
        
    }
    
}