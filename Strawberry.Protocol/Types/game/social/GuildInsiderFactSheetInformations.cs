

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildInsiderFactSheetInformations : GuildFactSheetInformations
    {
        public new const short ProtocolId = 423;
        public override short TypeID => ProtocolId;
        
        public string LeaderName { get; set; }
        public ushort NbConnectedMembers { get; set; }
        public sbyte NbTaxCollectors { get; set; }
        public int LastActivity { get; set; }
        
        public GuildInsiderFactSheetInformations()
        {
        }
        
        public GuildInsiderFactSheetInformations(uint guildId, string guildName, byte guildLevel, Types.GuildEmblem guildEmblem, ulong leaderId, ushort nbMembers, string leaderName, ushort nbConnectedMembers, sbyte nbTaxCollectors, int lastActivity)
         : base(guildId, guildName, guildLevel, guildEmblem, leaderId, nbMembers)
        {
            this.LeaderName = leaderName;
            this.NbConnectedMembers = nbConnectedMembers;
            this.NbTaxCollectors = nbTaxCollectors;
            this.LastActivity = lastActivity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(LeaderName);
            writer.WriteVarShort(NbConnectedMembers);
            writer.WriteSByte(NbTaxCollectors);
            writer.WriteInt(LastActivity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LeaderName = reader.ReadUTF();
            NbConnectedMembers = reader.ReadVarUhShort();
            if (NbConnectedMembers < 0)
                throw new Exception("Forbidden value on NbConnectedMembers = " + NbConnectedMembers + ", it doesn't respect the following condition : nbConnectedMembers < 0");
            NbTaxCollectors = reader.ReadSByte();
            if (NbTaxCollectors < 0)
                throw new Exception("Forbidden value on NbTaxCollectors = " + NbTaxCollectors + ", it doesn't respect the following condition : nbTaxCollectors < 0");
            LastActivity = reader.ReadInt();
            if (LastActivity < 0)
                throw new Exception("Forbidden value on LastActivity = " + LastActivity + ", it doesn't respect the following condition : lastActivity < 0");
        }
        
    }
    
}