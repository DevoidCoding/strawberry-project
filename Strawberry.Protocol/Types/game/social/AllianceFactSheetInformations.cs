

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AllianceFactSheetInformations : AllianceInformations
    {
        public new const short ProtocolId = 421;
        public override short TypeID => ProtocolId;
        
        public int CreationDate { get; set; }
        
        public AllianceFactSheetInformations()
        {
        }
        
        public AllianceFactSheetInformations(uint allianceId, string allianceTag, string allianceName, Types.GuildEmblem allianceEmblem, int creationDate)
         : base(allianceId, allianceTag, allianceName, allianceEmblem)
        {
            this.CreationDate = creationDate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(CreationDate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CreationDate = reader.ReadInt();
            if (CreationDate < 0)
                throw new Exception("Forbidden value on CreationDate = " + CreationDate + ", it doesn't respect the following condition : creationDate < 0");
        }
        
    }
    
}