

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class EntityDispositionInformations
    {
        public const short ProtocolId = 60;
        public virtual short TypeID => ProtocolId;
        
        public short CellId { get; set; }
        public sbyte Direction { get; set; }
        
        public EntityDispositionInformations()
        {
        }
        
        public EntityDispositionInformations(short cellId, sbyte direction)
        {
            this.CellId = cellId;
            this.Direction = direction;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteShort(CellId);
            writer.WriteSByte(Direction);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CellId = reader.ReadShort();
            if (CellId < -1 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < -1 || cellId > 559");
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
        }
        
    }
    
}