

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TaxCollectorStaticInformations
    {
        public const short ProtocolId = 147;
        public virtual short TypeID => ProtocolId;
        
        public ushort FirstNameId { get; set; }
        public ushort LastNameId { get; set; }
        public Types.GuildInformations GuildIdentity { get; set; }
        
        public TaxCollectorStaticInformations()
        {
        }
        
        public TaxCollectorStaticInformations(ushort firstNameId, ushort lastNameId, Types.GuildInformations guildIdentity)
        {
            this.FirstNameId = firstNameId;
            this.LastNameId = lastNameId;
            this.GuildIdentity = guildIdentity;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            GuildIdentity.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            FirstNameId = reader.ReadVarUhShort();
            if (FirstNameId < 0)
                throw new Exception("Forbidden value on FirstNameId = " + FirstNameId + ", it doesn't respect the following condition : firstNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
            GuildIdentity = new Types.GuildInformations();
            GuildIdentity.Deserialize(reader);
        }
        
    }
    
}