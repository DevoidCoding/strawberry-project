

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ActorOrientation
    {
        public const short ProtocolId = 353;
        public virtual short TypeID => ProtocolId;
        
        public double Id { get; set; }
        public sbyte Direction { get; set; }
        
        public ActorOrientation()
        {
        }
        
        public ActorOrientation(double id, sbyte direction)
        {
            this.Id = id;
            this.Direction = direction;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteSByte(Direction);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
        }
        
    }
    
}