

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterNamedInformations : GameFightFighterInformations
    {
        public new const short ProtocolId = 158;
        public override short TypeID => ProtocolId;
        
        public string Name { get; set; }
        public Types.PlayerStatus Status { get; set; }
        
        public GameFightFighterNamedInformations()
        {
        }
        
        public GameFightFighterNamedInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions, string name, Types.PlayerStatus status)
         : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            this.Name = name;
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            Status.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Status = new Types.PlayerStatus();
            Status.Deserialize(reader);
        }
        
    }
    
}