

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultExperienceData : FightResultAdditionalData
    {
        public new const short ProtocolId = 192;
        public override short TypeID => ProtocolId;
        
        public bool ShowExperience { get; set; }
        public bool ShowExperienceLevelFloor { get; set; }
        public bool ShowExperienceNextLevelFloor { get; set; }
        public bool ShowExperienceFightDelta { get; set; }
        public bool ShowExperienceForGuild { get; set; }
        public bool ShowExperienceForMount { get; set; }
        public bool IsIncarnationExperience { get; set; }
        public ulong Experience { get; set; }
        public ulong ExperienceLevelFloor { get; set; }
        public ulong ExperienceNextLevelFloor { get; set; }
        public ulong ExperienceFightDelta { get; set; }
        public ulong ExperienceForGuild { get; set; }
        public ulong ExperienceForMount { get; set; }
        public sbyte RerollExperienceMul { get; set; }
        
        public FightResultExperienceData()
        {
        }
        
        public FightResultExperienceData(bool showExperience, bool showExperienceLevelFloor, bool showExperienceNextLevelFloor, bool showExperienceFightDelta, bool showExperienceForGuild, bool showExperienceForMount, bool isIncarnationExperience, ulong experience, ulong experienceLevelFloor, ulong experienceNextLevelFloor, ulong experienceFightDelta, ulong experienceForGuild, ulong experienceForMount, sbyte rerollExperienceMul)
        {
            this.ShowExperience = showExperience;
            this.ShowExperienceLevelFloor = showExperienceLevelFloor;
            this.ShowExperienceNextLevelFloor = showExperienceNextLevelFloor;
            this.ShowExperienceFightDelta = showExperienceFightDelta;
            this.ShowExperienceForGuild = showExperienceForGuild;
            this.ShowExperienceForMount = showExperienceForMount;
            this.IsIncarnationExperience = isIncarnationExperience;
            this.Experience = experience;
            this.ExperienceLevelFloor = experienceLevelFloor;
            this.ExperienceNextLevelFloor = experienceNextLevelFloor;
            this.ExperienceFightDelta = experienceFightDelta;
            this.ExperienceForGuild = experienceForGuild;
            this.ExperienceForMount = experienceForMount;
            this.RerollExperienceMul = rerollExperienceMul;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, ShowExperience);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, ShowExperienceLevelFloor);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, ShowExperienceNextLevelFloor);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, ShowExperienceFightDelta);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 4, ShowExperienceForGuild);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 5, ShowExperienceForMount);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 6, IsIncarnationExperience);
            writer.WriteByte(flag1);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExperienceLevelFloor);
            writer.WriteVarLong(ExperienceNextLevelFloor);
            writer.WriteVarLong(ExperienceFightDelta);
            writer.WriteVarLong(ExperienceForGuild);
            writer.WriteVarLong(ExperienceForMount);
            writer.WriteSByte(RerollExperienceMul);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            ShowExperience = BooleanByteWrapper.GetFlag(flag1, 0);
            ShowExperienceLevelFloor = BooleanByteWrapper.GetFlag(flag1, 1);
            ShowExperienceNextLevelFloor = BooleanByteWrapper.GetFlag(flag1, 2);
            ShowExperienceFightDelta = BooleanByteWrapper.GetFlag(flag1, 3);
            ShowExperienceForGuild = BooleanByteWrapper.GetFlag(flag1, 4);
            ShowExperienceForMount = BooleanByteWrapper.GetFlag(flag1, 5);
            IsIncarnationExperience = BooleanByteWrapper.GetFlag(flag1, 6);
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            ExperienceLevelFloor = reader.ReadVarUhLong();
            if (ExperienceLevelFloor < 0 || ExperienceLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceLevelFloor = " + ExperienceLevelFloor + ", it doesn't respect the following condition : experienceLevelFloor < 0 || experienceLevelFloor > 9007199254740990");
            ExperienceNextLevelFloor = reader.ReadVarUhLong();
            if (ExperienceNextLevelFloor < 0 || ExperienceNextLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceNextLevelFloor = " + ExperienceNextLevelFloor + ", it doesn't respect the following condition : experienceNextLevelFloor < 0 || experienceNextLevelFloor > 9007199254740990");
            ExperienceFightDelta = reader.ReadVarUhLong();
            if (ExperienceFightDelta < 0 || ExperienceFightDelta > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceFightDelta = " + ExperienceFightDelta + ", it doesn't respect the following condition : experienceFightDelta < 0 || experienceFightDelta > 9007199254740990");
            ExperienceForGuild = reader.ReadVarUhLong();
            if (ExperienceForGuild < 0 || ExperienceForGuild > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceForGuild = " + ExperienceForGuild + ", it doesn't respect the following condition : experienceForGuild < 0 || experienceForGuild > 9007199254740990");
            ExperienceForMount = reader.ReadVarUhLong();
            if (ExperienceForMount < 0 || ExperienceForMount > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceForMount = " + ExperienceForMount + ", it doesn't respect the following condition : experienceForMount < 0 || experienceForMount > 9007199254740990");
            RerollExperienceMul = reader.ReadSByte();
            if (RerollExperienceMul < 0)
                throw new Exception("Forbidden value on RerollExperienceMul = " + RerollExperienceMul + ", it doesn't respect the following condition : rerollExperienceMul < 0");
        }
        
    }
    
}