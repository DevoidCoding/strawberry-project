

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultPlayerListEntry : FightResultFighterListEntry
    {
        public new const short ProtocolId = 24;
        public override short TypeID => ProtocolId;
        
        public ushort Level { get; set; }
        public Types.FightResultAdditionalData[] Additional { get; set; }
        
        public FightResultPlayerListEntry()
        {
        }
        
        public FightResultPlayerListEntry(ushort outcome, sbyte wave, Types.FightLoot rewards, double id, bool alive, ushort level, Types.FightResultAdditionalData[] additional)
         : base(outcome, wave, rewards, id, alive)
        {
            this.Level = level;
            this.Additional = additional;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
            writer.WriteUShort((ushort)Additional.Length);
            foreach (var entry in Additional)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
            var limit = reader.ReadUShort();
            Additional = new Types.FightResultAdditionalData[limit];
            for (int i = 0; i < limit; i++)
            {
                 Additional[i] = Types.ProtocolTypeManager.GetInstance<Types.FightResultAdditionalData>(reader.ReadShort());
                 Additional[i].Deserialize(reader);
            }
        }
        
    }
    
}