

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightMinimalStats
    {
        public const short ProtocolId = 31;
        public virtual short TypeID => ProtocolId;
        
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        public uint BaseMaxLifePoints { get; set; }
        public uint PermanentDamagePercent { get; set; }
        public uint ShieldPoints { get; set; }
        public short ActionPoints { get; set; }
        public short MaxActionPoints { get; set; }
        public short MovementPoints { get; set; }
        public short MaxMovementPoints { get; set; }
        public double Summoner { get; set; }
        public bool Summoned { get; set; }
        public short NeutralElementResistPercent { get; set; }
        public short EarthElementResistPercent { get; set; }
        public short WaterElementResistPercent { get; set; }
        public short AirElementResistPercent { get; set; }
        public short FireElementResistPercent { get; set; }
        public short NeutralElementReduction { get; set; }
        public short EarthElementReduction { get; set; }
        public short WaterElementReduction { get; set; }
        public short AirElementReduction { get; set; }
        public short FireElementReduction { get; set; }
        public short CriticalDamageFixedResist { get; set; }
        public short PushDamageFixedResist { get; set; }
        public short PvpNeutralElementResistPercent { get; set; }
        public short PvpEarthElementResistPercent { get; set; }
        public short PvpWaterElementResistPercent { get; set; }
        public short PvpAirElementResistPercent { get; set; }
        public short PvpFireElementResistPercent { get; set; }
        public short PvpNeutralElementReduction { get; set; }
        public short PvpEarthElementReduction { get; set; }
        public short PvpWaterElementReduction { get; set; }
        public short PvpAirElementReduction { get; set; }
        public short PvpFireElementReduction { get; set; }
        public ushort DodgePALostProbability { get; set; }
        public ushort DodgePMLostProbability { get; set; }
        public short TackleBlock { get; set; }
        public short TackleEvade { get; set; }
        public short FixedDamageReflection { get; set; }
        public sbyte InvisibilityState { get; set; }
        public ushort MeleeDamageReceivedPercent { get; set; }
        public ushort RangedDamageReceivedPercent { get; set; }
        public ushort WeaponDamageReceivedPercent { get; set; }
        public ushort SpellDamageReceivedPercent { get; set; }
        
        public GameFightMinimalStats()
        {
        }
        
        public GameFightMinimalStats(uint lifePoints, uint maxLifePoints, uint baseMaxLifePoints, uint permanentDamagePercent, uint shieldPoints, short actionPoints, short maxActionPoints, short movementPoints, short maxMovementPoints, double summoner, bool summoned, short neutralElementResistPercent, short earthElementResistPercent, short waterElementResistPercent, short airElementResistPercent, short fireElementResistPercent, short neutralElementReduction, short earthElementReduction, short waterElementReduction, short airElementReduction, short fireElementReduction, short criticalDamageFixedResist, short pushDamageFixedResist, short pvpNeutralElementResistPercent, short pvpEarthElementResistPercent, short pvpWaterElementResistPercent, short pvpAirElementResistPercent, short pvpFireElementResistPercent, short pvpNeutralElementReduction, short pvpEarthElementReduction, short pvpWaterElementReduction, short pvpAirElementReduction, short pvpFireElementReduction, ushort dodgePALostProbability, ushort dodgePMLostProbability, short tackleBlock, short tackleEvade, short fixedDamageReflection, sbyte invisibilityState, ushort meleeDamageReceivedPercent, ushort rangedDamageReceivedPercent, ushort weaponDamageReceivedPercent, ushort spellDamageReceivedPercent)
        {
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
            this.BaseMaxLifePoints = baseMaxLifePoints;
            this.PermanentDamagePercent = permanentDamagePercent;
            this.ShieldPoints = shieldPoints;
            this.ActionPoints = actionPoints;
            this.MaxActionPoints = maxActionPoints;
            this.MovementPoints = movementPoints;
            this.MaxMovementPoints = maxMovementPoints;
            this.Summoner = summoner;
            this.Summoned = summoned;
            this.NeutralElementResistPercent = neutralElementResistPercent;
            this.EarthElementResistPercent = earthElementResistPercent;
            this.WaterElementResistPercent = waterElementResistPercent;
            this.AirElementResistPercent = airElementResistPercent;
            this.FireElementResistPercent = fireElementResistPercent;
            this.NeutralElementReduction = neutralElementReduction;
            this.EarthElementReduction = earthElementReduction;
            this.WaterElementReduction = waterElementReduction;
            this.AirElementReduction = airElementReduction;
            this.FireElementReduction = fireElementReduction;
            this.CriticalDamageFixedResist = criticalDamageFixedResist;
            this.PushDamageFixedResist = pushDamageFixedResist;
            this.PvpNeutralElementResistPercent = pvpNeutralElementResistPercent;
            this.PvpEarthElementResistPercent = pvpEarthElementResistPercent;
            this.PvpWaterElementResistPercent = pvpWaterElementResistPercent;
            this.PvpAirElementResistPercent = pvpAirElementResistPercent;
            this.PvpFireElementResistPercent = pvpFireElementResistPercent;
            this.PvpNeutralElementReduction = pvpNeutralElementReduction;
            this.PvpEarthElementReduction = pvpEarthElementReduction;
            this.PvpWaterElementReduction = pvpWaterElementReduction;
            this.PvpAirElementReduction = pvpAirElementReduction;
            this.PvpFireElementReduction = pvpFireElementReduction;
            this.DodgePALostProbability = dodgePALostProbability;
            this.DodgePMLostProbability = dodgePMLostProbability;
            this.TackleBlock = tackleBlock;
            this.TackleEvade = tackleEvade;
            this.FixedDamageReflection = fixedDamageReflection;
            this.InvisibilityState = invisibilityState;
            this.MeleeDamageReceivedPercent = meleeDamageReceivedPercent;
            this.RangedDamageReceivedPercent = rangedDamageReceivedPercent;
            this.WeaponDamageReceivedPercent = weaponDamageReceivedPercent;
            this.SpellDamageReceivedPercent = spellDamageReceivedPercent;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarInt(BaseMaxLifePoints);
            writer.WriteVarInt(PermanentDamagePercent);
            writer.WriteVarInt(ShieldPoints);
            writer.WriteVarShort(ActionPoints);
            writer.WriteVarShort(MaxActionPoints);
            writer.WriteVarShort(MovementPoints);
            writer.WriteVarShort(MaxMovementPoints);
            writer.WriteDouble(Summoner);
            writer.WriteBoolean(Summoned);
            writer.WriteVarShort(NeutralElementResistPercent);
            writer.WriteVarShort(EarthElementResistPercent);
            writer.WriteVarShort(WaterElementResistPercent);
            writer.WriteVarShort(AirElementResistPercent);
            writer.WriteVarShort(FireElementResistPercent);
            writer.WriteVarShort(NeutralElementReduction);
            writer.WriteVarShort(EarthElementReduction);
            writer.WriteVarShort(WaterElementReduction);
            writer.WriteVarShort(AirElementReduction);
            writer.WriteVarShort(FireElementReduction);
            writer.WriteVarShort(CriticalDamageFixedResist);
            writer.WriteVarShort(PushDamageFixedResist);
            writer.WriteVarShort(PvpNeutralElementResistPercent);
            writer.WriteVarShort(PvpEarthElementResistPercent);
            writer.WriteVarShort(PvpWaterElementResistPercent);
            writer.WriteVarShort(PvpAirElementResistPercent);
            writer.WriteVarShort(PvpFireElementResistPercent);
            writer.WriteVarShort(PvpNeutralElementReduction);
            writer.WriteVarShort(PvpEarthElementReduction);
            writer.WriteVarShort(PvpWaterElementReduction);
            writer.WriteVarShort(PvpAirElementReduction);
            writer.WriteVarShort(PvpFireElementReduction);
            writer.WriteVarShort(DodgePALostProbability);
            writer.WriteVarShort(DodgePMLostProbability);
            writer.WriteVarShort(TackleBlock);
            writer.WriteVarShort(TackleEvade);
            writer.WriteVarShort(FixedDamageReflection);
            writer.WriteSByte(InvisibilityState);
            writer.WriteVarShort(MeleeDamageReceivedPercent);
            writer.WriteVarShort(RangedDamageReceivedPercent);
            writer.WriteVarShort(WeaponDamageReceivedPercent);
            writer.WriteVarShort(SpellDamageReceivedPercent);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
            BaseMaxLifePoints = reader.ReadVarUhInt();
            if (BaseMaxLifePoints < 0)
                throw new Exception("Forbidden value on BaseMaxLifePoints = " + BaseMaxLifePoints + ", it doesn't respect the following condition : baseMaxLifePoints < 0");
            PermanentDamagePercent = reader.ReadVarUhInt();
            if (PermanentDamagePercent < 0)
                throw new Exception("Forbidden value on PermanentDamagePercent = " + PermanentDamagePercent + ", it doesn't respect the following condition : permanentDamagePercent < 0");
            ShieldPoints = reader.ReadVarUhInt();
            if (ShieldPoints < 0)
                throw new Exception("Forbidden value on ShieldPoints = " + ShieldPoints + ", it doesn't respect the following condition : shieldPoints < 0");
            ActionPoints = reader.ReadVarShort();
            MaxActionPoints = reader.ReadVarShort();
            MovementPoints = reader.ReadVarShort();
            MaxMovementPoints = reader.ReadVarShort();
            Summoner = reader.ReadDouble();
            if (Summoner < -9007199254740990 || Summoner > 9007199254740990)
                throw new Exception("Forbidden value on Summoner = " + Summoner + ", it doesn't respect the following condition : summoner < -9007199254740990 || summoner > 9007199254740990");
            Summoned = reader.ReadBoolean();
            NeutralElementResistPercent = reader.ReadVarShort();
            EarthElementResistPercent = reader.ReadVarShort();
            WaterElementResistPercent = reader.ReadVarShort();
            AirElementResistPercent = reader.ReadVarShort();
            FireElementResistPercent = reader.ReadVarShort();
            NeutralElementReduction = reader.ReadVarShort();
            EarthElementReduction = reader.ReadVarShort();
            WaterElementReduction = reader.ReadVarShort();
            AirElementReduction = reader.ReadVarShort();
            FireElementReduction = reader.ReadVarShort();
            CriticalDamageFixedResist = reader.ReadVarShort();
            PushDamageFixedResist = reader.ReadVarShort();
            PvpNeutralElementResistPercent = reader.ReadVarShort();
            PvpEarthElementResistPercent = reader.ReadVarShort();
            PvpWaterElementResistPercent = reader.ReadVarShort();
            PvpAirElementResistPercent = reader.ReadVarShort();
            PvpFireElementResistPercent = reader.ReadVarShort();
            PvpNeutralElementReduction = reader.ReadVarShort();
            PvpEarthElementReduction = reader.ReadVarShort();
            PvpWaterElementReduction = reader.ReadVarShort();
            PvpAirElementReduction = reader.ReadVarShort();
            PvpFireElementReduction = reader.ReadVarShort();
            DodgePALostProbability = reader.ReadVarUhShort();
            if (DodgePALostProbability < 0)
                throw new Exception("Forbidden value on DodgePALostProbability = " + DodgePALostProbability + ", it doesn't respect the following condition : dodgePALostProbability < 0");
            DodgePMLostProbability = reader.ReadVarUhShort();
            if (DodgePMLostProbability < 0)
                throw new Exception("Forbidden value on DodgePMLostProbability = " + DodgePMLostProbability + ", it doesn't respect the following condition : dodgePMLostProbability < 0");
            TackleBlock = reader.ReadVarShort();
            TackleEvade = reader.ReadVarShort();
            FixedDamageReflection = reader.ReadVarShort();
            InvisibilityState = reader.ReadSByte();
            if (InvisibilityState < 0)
                throw new Exception("Forbidden value on InvisibilityState = " + InvisibilityState + ", it doesn't respect the following condition : invisibilityState < 0");
            MeleeDamageReceivedPercent = reader.ReadVarUhShort();
            if (MeleeDamageReceivedPercent < 0)
                throw new Exception("Forbidden value on MeleeDamageReceivedPercent = " + MeleeDamageReceivedPercent + ", it doesn't respect the following condition : meleeDamageReceivedPercent < 0");
            RangedDamageReceivedPercent = reader.ReadVarUhShort();
            if (RangedDamageReceivedPercent < 0)
                throw new Exception("Forbidden value on RangedDamageReceivedPercent = " + RangedDamageReceivedPercent + ", it doesn't respect the following condition : rangedDamageReceivedPercent < 0");
            WeaponDamageReceivedPercent = reader.ReadVarUhShort();
            if (WeaponDamageReceivedPercent < 0)
                throw new Exception("Forbidden value on WeaponDamageReceivedPercent = " + WeaponDamageReceivedPercent + ", it doesn't respect the following condition : weaponDamageReceivedPercent < 0");
            SpellDamageReceivedPercent = reader.ReadVarUhShort();
            if (SpellDamageReceivedPercent < 0)
                throw new Exception("Forbidden value on SpellDamageReceivedPercent = " + SpellDamageReceivedPercent + ", it doesn't respect the following condition : spellDamageReceivedPercent < 0");
        }
        
    }
    
}