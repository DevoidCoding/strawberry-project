

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightCommonInformations
    {
        public const short ProtocolId = 43;
        public virtual short TypeID => ProtocolId;
        
        public ushort FightId { get; set; }
        public sbyte FightType { get; set; }
        public Types.FightTeamInformations[] FightTeams { get; set; }
        public ushort[] FightTeamsPositions { get; set; }
        public Types.FightOptionsInformations[] FightTeamsOptions { get; set; }
        
        public FightCommonInformations()
        {
        }
        
        public FightCommonInformations(ushort fightId, sbyte fightType, Types.FightTeamInformations[] fightTeams, ushort[] fightTeamsPositions, Types.FightOptionsInformations[] fightTeamsOptions)
        {
            this.FightId = fightId;
            this.FightType = fightType;
            this.FightTeams = fightTeams;
            this.FightTeamsPositions = fightTeamsPositions;
            this.FightTeamsOptions = fightTeamsOptions;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteSByte(FightType);
            writer.WriteUShort((ushort)FightTeams.Length);
            foreach (var entry in FightTeams)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)FightTeamsPositions.Length);
            foreach (var entry in FightTeamsPositions)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)FightTeamsOptions.Length);
            foreach (var entry in FightTeamsOptions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            FightType = reader.ReadSByte();
            if (FightType < 0)
                throw new Exception("Forbidden value on FightType = " + FightType + ", it doesn't respect the following condition : fightType < 0");
            var limit = reader.ReadUShort();
            FightTeams = new Types.FightTeamInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 FightTeams[i] = Types.ProtocolTypeManager.GetInstance<Types.FightTeamInformations>(reader.ReadShort());
                 FightTeams[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            FightTeamsPositions = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 FightTeamsPositions[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            FightTeamsOptions = new Types.FightOptionsInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 FightTeamsOptions[i] = new Types.FightOptionsInformations();
                 FightTeamsOptions[i].Deserialize(reader);
            }
        }
        
    }
    
}