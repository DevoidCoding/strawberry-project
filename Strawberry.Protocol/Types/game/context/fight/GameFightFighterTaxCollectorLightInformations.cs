

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterTaxCollectorLightInformations : GameFightFighterLightInformations
    {
        public new const short ProtocolId = 457;
        public override short TypeID => ProtocolId;
        
        public ushort FirstNameId { get; set; }
        public ushort LastNameId { get; set; }
        
        public GameFightFighterTaxCollectorLightInformations()
        {
        }
        
        public GameFightFighterTaxCollectorLightInformations(bool sex, bool alive, double id, sbyte wave, ushort level, sbyte breed, ushort firstNameId, ushort lastNameId)
         : base(sex, alive, id, wave, level, breed)
        {
            this.FirstNameId = firstNameId;
            this.LastNameId = lastNameId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarUhShort();
            if (FirstNameId < 0)
                throw new Exception("Forbidden value on FirstNameId = " + FirstNameId + ", it doesn't respect the following condition : firstNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
        }
        
    }
    
}