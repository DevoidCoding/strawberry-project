

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightExternalInformations
    {
        public const short ProtocolId = 117;
        public virtual short TypeID => ProtocolId;
        
        public ushort FightId { get; set; }
        public sbyte FightType { get; set; }
        public int FightStart { get; set; }
        public bool FightSpectatorLocked { get; set; }
        public Types.FightTeamLightInformations[] FightTeams { get; set; }
        public Types.FightOptionsInformations[] FightTeamsOptions { get; set; }
        
        public FightExternalInformations()
        {
        }
        
        public FightExternalInformations(ushort fightId, sbyte fightType, int fightStart, bool fightSpectatorLocked, Types.FightTeamLightInformations[] fightTeams, Types.FightOptionsInformations[] fightTeamsOptions)
        {
            this.FightId = fightId;
            this.FightType = fightType;
            this.FightStart = fightStart;
            this.FightSpectatorLocked = fightSpectatorLocked;
            this.FightTeams = fightTeams;
            this.FightTeamsOptions = fightTeamsOptions;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteSByte(FightType);
            writer.WriteInt(FightStart);
            writer.WriteBoolean(FightSpectatorLocked);
            foreach (var entry in FightTeams)
            {
                 entry.Serialize(writer);
            }
            foreach (var entry in FightTeamsOptions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            FightType = reader.ReadSByte();
            if (FightType < 0)
                throw new Exception("Forbidden value on FightType = " + FightType + ", it doesn't respect the following condition : fightType < 0");
            FightStart = reader.ReadInt();
            if (FightStart < 0)
                throw new Exception("Forbidden value on FightStart = " + FightStart + ", it doesn't respect the following condition : fightStart < 0");
            FightSpectatorLocked = reader.ReadBoolean();
            FightTeams = new Types.FightTeamLightInformations[2];
            for (int i = 0; i < 2; i++)
            {
                 FightTeams[i] = new Types.FightTeamLightInformations();
                 FightTeams[i].Deserialize(reader);
            }
            FightTeamsOptions = new Types.FightOptionsInformations[2];
            for (int i = 0; i < 2; i++)
            {
                 FightTeamsOptions[i] = new Types.FightOptionsInformations();
                 FightTeamsOptions[i].Deserialize(reader);
            }
        }
        
    }
    
}