

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightOptionsInformations
    {
        public const short ProtocolId = 20;
        public virtual short TypeID => ProtocolId;
        
        public bool IsSecret { get; set; }
        public bool IsRestrictedToPartyOnly { get; set; }
        public bool IsClosed { get; set; }
        public bool IsAskingForHelp { get; set; }
        
        public FightOptionsInformations()
        {
        }
        
        public FightOptionsInformations(bool isSecret, bool isRestrictedToPartyOnly, bool isClosed, bool isAskingForHelp)
        {
            this.IsSecret = isSecret;
            this.IsRestrictedToPartyOnly = isRestrictedToPartyOnly;
            this.IsClosed = isClosed;
            this.IsAskingForHelp = isAskingForHelp;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, IsSecret);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsRestrictedToPartyOnly);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, IsClosed);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, IsAskingForHelp);
            writer.WriteByte(flag1);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            IsSecret = BooleanByteWrapper.GetFlag(flag1, 0);
            IsRestrictedToPartyOnly = BooleanByteWrapper.GetFlag(flag1, 1);
            IsClosed = BooleanByteWrapper.GetFlag(flag1, 2);
            IsAskingForHelp = BooleanByteWrapper.GetFlag(flag1, 3);
        }
        
    }
    
}