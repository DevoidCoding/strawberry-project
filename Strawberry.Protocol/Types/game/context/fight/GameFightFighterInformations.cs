

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterInformations : GameContextActorInformations
    {
        public new const short ProtocolId = 143;
        public override short TypeID => ProtocolId;
        
        public sbyte TeamId { get; set; }
        public sbyte Wave { get; set; }
        public bool Alive { get; set; }
        public Types.GameFightMinimalStats Stats { get; set; }
        public ushort[] PreviousPositions { get; set; }
        
        public GameFightFighterInformations()
        {
        }
        
        public GameFightFighterInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions)
         : base(contextualId, look, disposition)
        {
            this.TeamId = teamId;
            this.Wave = wave;
            this.Alive = alive;
            this.Stats = stats;
            this.PreviousPositions = previousPositions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(TeamId);
            writer.WriteSByte(Wave);
            writer.WriteBoolean(Alive);
            writer.WriteShort(Stats.TypeID);
            Stats.Serialize(writer);
            writer.WriteUShort((ushort)PreviousPositions.Length);
            foreach (var entry in PreviousPositions)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            Wave = reader.ReadSByte();
            if (Wave < 0)
                throw new Exception("Forbidden value on Wave = " + Wave + ", it doesn't respect the following condition : wave < 0");
            Alive = reader.ReadBoolean();
            Stats = Types.ProtocolTypeManager.GetInstance<Types.GameFightMinimalStats>(reader.ReadShort());
            Stats.Deserialize(reader);
            var limit = reader.ReadUShort();
            PreviousPositions = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PreviousPositions[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}