

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterLightInformations
    {
        public const short ProtocolId = 413;
        public virtual short TypeID => ProtocolId;
        
        public bool Sex { get; set; }
        public bool Alive { get; set; }
        public double Id { get; set; }
        public sbyte Wave { get; set; }
        public ushort Level { get; set; }
        public sbyte Breed { get; set; }
        
        public GameFightFighterLightInformations()
        {
        }
        
        public GameFightFighterLightInformations(bool sex, bool alive, double id, sbyte wave, ushort level, sbyte breed)
        {
            this.Sex = sex;
            this.Alive = alive;
            this.Id = id;
            this.Wave = wave;
            this.Level = level;
            this.Breed = breed;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Sex);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Alive);
            writer.WriteByte(flag1);
            writer.WriteDouble(Id);
            writer.WriteSByte(Wave);
            writer.WriteVarShort(Level);
            writer.WriteSByte(Breed);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag1, 0);
            Alive = BooleanByteWrapper.GetFlag(flag1, 1);
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Wave = reader.ReadSByte();
            if (Wave < 0)
                throw new Exception("Forbidden value on Wave = " + Wave + ", it doesn't respect the following condition : wave < 0");
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
            Breed = reader.ReadSByte();
        }
        
    }
    
}