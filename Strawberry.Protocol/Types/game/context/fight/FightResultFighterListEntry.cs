

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultFighterListEntry : FightResultListEntry
    {
        public new const short ProtocolId = 189;
        public override short TypeID => ProtocolId;
        
        public double Id { get; set; }
        public bool Alive { get; set; }
        
        public FightResultFighterListEntry()
        {
        }
        
        public FightResultFighterListEntry(ushort outcome, sbyte wave, Types.FightLoot rewards, double id, bool alive)
         : base(outcome, wave, rewards)
        {
            this.Id = id;
            this.Alive = alive;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(Id);
            writer.WriteBoolean(Alive);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Alive = reader.ReadBoolean();
        }
        
    }
    
}