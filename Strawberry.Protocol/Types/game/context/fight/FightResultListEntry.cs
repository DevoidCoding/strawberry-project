

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultListEntry
    {
        public const short ProtocolId = 16;
        public virtual short TypeID => ProtocolId;
        
        public ushort Outcome { get; set; }
        public sbyte Wave { get; set; }
        public Types.FightLoot Rewards { get; set; }
        
        public FightResultListEntry()
        {
        }
        
        public FightResultListEntry(ushort outcome, sbyte wave, Types.FightLoot rewards)
        {
            this.Outcome = outcome;
            this.Wave = wave;
            this.Rewards = rewards;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Outcome);
            writer.WriteSByte(Wave);
            Rewards.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Outcome = reader.ReadVarUhShort();
            if (Outcome < 0)
                throw new Exception("Forbidden value on Outcome = " + Outcome + ", it doesn't respect the following condition : outcome < 0");
            Wave = reader.ReadSByte();
            if (Wave < 0)
                throw new Exception("Forbidden value on Wave = " + Wave + ", it doesn't respect the following condition : wave < 0");
            Rewards = new Types.FightLoot();
            Rewards.Deserialize(reader);
        }
        
    }
    
}