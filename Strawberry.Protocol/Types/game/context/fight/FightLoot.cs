

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightLoot
    {
        public const short ProtocolId = 41;
        public virtual short TypeID => ProtocolId;
        
        public ushort[] Objects { get; set; }
        public ulong Kamas { get; set; }
        
        public FightLoot()
        {
        }
        
        public FightLoot(ushort[] objects, ulong kamas)
        {
            this.Objects = objects;
            this.Kamas = kamas;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Objects.Length);
            foreach (var entry in Objects)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteVarLong(Kamas);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Objects = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Objects[i] = reader.ReadVarUhShort();
            }
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
        }
        
    }
    
}