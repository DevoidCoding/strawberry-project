

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightCompanionInformations : GameFightFighterInformations
    {
        public new const short ProtocolId = 450;
        public override short TypeID => ProtocolId;
        
        public sbyte CompanionGenericId { get; set; }
        public ushort Level { get; set; }
        public double MasterId { get; set; }
        
        public GameFightCompanionInformations()
        {
        }
        
        public GameFightCompanionInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions, sbyte companionGenericId, ushort level, double masterId)
         : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            this.CompanionGenericId = companionGenericId;
            this.Level = level;
            this.MasterId = masterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(CompanionGenericId);
            writer.WriteVarShort(Level);
            writer.WriteDouble(MasterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CompanionGenericId = reader.ReadSByte();
            if (CompanionGenericId < 0)
                throw new Exception("Forbidden value on CompanionGenericId = " + CompanionGenericId + ", it doesn't respect the following condition : companionGenericId < 0");
            Level = reader.ReadVarUhShort();
            if (Level < 1 || Level > 200)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 1 || level > 200");
            MasterId = reader.ReadDouble();
            if (MasterId < -9007199254740990 || MasterId > 9007199254740990)
                throw new Exception("Forbidden value on MasterId = " + MasterId + ", it doesn't respect the following condition : masterId < -9007199254740990 || masterId > 9007199254740990");
        }
        
    }
    
}