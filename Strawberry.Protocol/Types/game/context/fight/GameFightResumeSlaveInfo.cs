

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightResumeSlaveInfo
    {
        public const short ProtocolId = 364;
        public virtual short TypeID => ProtocolId;
        
        public double SlaveId { get; set; }
        public Types.GameFightSpellCooldown[] SpellCooldowns { get; set; }
        public sbyte SummonCount { get; set; }
        public sbyte BombCount { get; set; }
        
        public GameFightResumeSlaveInfo()
        {
        }
        
        public GameFightResumeSlaveInfo(double slaveId, Types.GameFightSpellCooldown[] spellCooldowns, sbyte summonCount, sbyte bombCount)
        {
            this.SlaveId = slaveId;
            this.SpellCooldowns = spellCooldowns;
            this.SummonCount = summonCount;
            this.BombCount = bombCount;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(SlaveId);
            writer.WriteUShort((ushort)SpellCooldowns.Length);
            foreach (var entry in SpellCooldowns)
            {
                 entry.Serialize(writer);
            }
            writer.WriteSByte(SummonCount);
            writer.WriteSByte(BombCount);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SlaveId = reader.ReadDouble();
            if (SlaveId < -9007199254740990 || SlaveId > 9007199254740990)
                throw new Exception("Forbidden value on SlaveId = " + SlaveId + ", it doesn't respect the following condition : slaveId < -9007199254740990 || slaveId > 9007199254740990");
            var limit = reader.ReadUShort();
            SpellCooldowns = new Types.GameFightSpellCooldown[limit];
            for (int i = 0; i < limit; i++)
            {
                 SpellCooldowns[i] = new Types.GameFightSpellCooldown();
                 SpellCooldowns[i].Deserialize(reader);
            }
            SummonCount = reader.ReadSByte();
            if (SummonCount < 0)
                throw new Exception("Forbidden value on SummonCount = " + SummonCount + ", it doesn't respect the following condition : summonCount < 0");
            BombCount = reader.ReadSByte();
            if (BombCount < 0)
                throw new Exception("Forbidden value on BombCount = " + BombCount + ", it doesn't respect the following condition : bombCount < 0");
        }
        
    }
    
}