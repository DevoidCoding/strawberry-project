

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultPvpData : FightResultAdditionalData
    {
        public new const short ProtocolId = 190;
        public override short TypeID => ProtocolId;
        
        public byte Grade { get; set; }
        public ushort MinHonorForGrade { get; set; }
        public ushort MaxHonorForGrade { get; set; }
        public ushort Honor { get; set; }
        public short HonorDelta { get; set; }
        
        public FightResultPvpData()
        {
        }
        
        public FightResultPvpData(byte grade, ushort minHonorForGrade, ushort maxHonorForGrade, ushort honor, short honorDelta)
        {
            this.Grade = grade;
            this.MinHonorForGrade = minHonorForGrade;
            this.MaxHonorForGrade = maxHonorForGrade;
            this.Honor = honor;
            this.HonorDelta = honorDelta;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Grade);
            writer.WriteVarShort(MinHonorForGrade);
            writer.WriteVarShort(MaxHonorForGrade);
            writer.WriteVarShort(Honor);
            writer.WriteVarShort(HonorDelta);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Grade = reader.ReadByte();
            if (Grade < 0 || Grade > 255)
                throw new Exception("Forbidden value on Grade = " + Grade + ", it doesn't respect the following condition : grade < 0 || grade > 255");
            MinHonorForGrade = reader.ReadVarUhShort();
            if (MinHonorForGrade < 0 || MinHonorForGrade > 20000)
                throw new Exception("Forbidden value on MinHonorForGrade = " + MinHonorForGrade + ", it doesn't respect the following condition : minHonorForGrade < 0 || minHonorForGrade > 20000");
            MaxHonorForGrade = reader.ReadVarUhShort();
            if (MaxHonorForGrade < 0 || MaxHonorForGrade > 20000)
                throw new Exception("Forbidden value on MaxHonorForGrade = " + MaxHonorForGrade + ", it doesn't respect the following condition : maxHonorForGrade < 0 || maxHonorForGrade > 20000");
            Honor = reader.ReadVarUhShort();
            if (Honor < 0 || Honor > 20000)
                throw new Exception("Forbidden value on Honor = " + Honor + ", it doesn't respect the following condition : honor < 0 || honor > 20000");
            HonorDelta = reader.ReadVarShort();
        }
        
    }
    
}