

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamMemberMonsterInformations : FightTeamMemberInformations
    {
        public new const short ProtocolId = 6;
        public override short TypeID => ProtocolId;
        
        public int MonsterId { get; set; }
        public sbyte Grade { get; set; }
        
        public FightTeamMemberMonsterInformations()
        {
        }
        
        public FightTeamMemberMonsterInformations(double id, int monsterId, sbyte grade)
         : base(id)
        {
            this.MonsterId = monsterId;
            this.Grade = grade;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(MonsterId);
            writer.WriteSByte(Grade);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MonsterId = reader.ReadInt();
            Grade = reader.ReadSByte();
            if (Grade < 0)
                throw new Exception("Forbidden value on Grade = " + Grade + ", it doesn't respect the following condition : grade < 0");
        }
        
    }
    
}