

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultMutantListEntry : FightResultFighterListEntry
    {
        public new const short ProtocolId = 216;
        public override short TypeID => ProtocolId;
        
        public ushort Level { get; set; }
        
        public FightResultMutantListEntry()
        {
        }
        
        public FightResultMutantListEntry(ushort outcome, sbyte wave, Types.FightLoot rewards, double id, bool alive, ushort level)
         : base(outcome, wave, rewards, id, alive)
        {
            this.Level = level;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Level);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
        }
        
    }
    
}