

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightTaxCollectorInformations : GameFightAIInformations
    {
        public new const short ProtocolId = 48;
        public override short TypeID => ProtocolId;
        
        public ushort FirstNameId { get; set; }
        public ushort LastNameId { get; set; }
        public byte Level { get; set; }
        
        public GameFightTaxCollectorInformations()
        {
        }
        
        public GameFightTaxCollectorInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions, ushort firstNameId, ushort lastNameId, byte level)
         : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            this.FirstNameId = firstNameId;
            this.LastNameId = lastNameId;
            this.Level = level;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteByte(Level);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarUhShort();
            if (FirstNameId < 0)
                throw new Exception("Forbidden value on FirstNameId = " + FirstNameId + ", it doesn't respect the following condition : firstNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
            Level = reader.ReadByte();
            if (Level < 0 || Level > 255)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0 || level > 255");
        }
        
    }
    
}