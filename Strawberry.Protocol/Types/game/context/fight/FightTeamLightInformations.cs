

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamLightInformations : AbstractFightTeamInformations
    {
        public new const short ProtocolId = 115;
        public override short TypeID => ProtocolId;
        
        public bool HasFriend { get; set; }
        public bool HasGuildMember { get; set; }
        public bool HasAllianceMember { get; set; }
        public bool HasGroupMember { get; set; }
        public bool HasMyTaxCollector { get; set; }
        public sbyte TeamMembersCount { get; set; }
        public uint MeanLevel { get; set; }
        
        public FightTeamLightInformations()
        {
        }
        
        public FightTeamLightInformations(sbyte teamId, double leaderId, sbyte teamSide, sbyte teamTypeId, sbyte nbWaves, bool hasFriend, bool hasGuildMember, bool hasAllianceMember, bool hasGroupMember, bool hasMyTaxCollector, sbyte teamMembersCount, uint meanLevel)
         : base(teamId, leaderId, teamSide, teamTypeId, nbWaves)
        {
            this.HasFriend = hasFriend;
            this.HasGuildMember = hasGuildMember;
            this.HasAllianceMember = hasAllianceMember;
            this.HasGroupMember = hasGroupMember;
            this.HasMyTaxCollector = hasMyTaxCollector;
            this.TeamMembersCount = teamMembersCount;
            this.MeanLevel = meanLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, HasFriend);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, HasGuildMember);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, HasAllianceMember);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, HasGroupMember);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 4, HasMyTaxCollector);
            writer.WriteByte(flag1);
            writer.WriteSByte(TeamMembersCount);
            writer.WriteVarInt(MeanLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            HasFriend = BooleanByteWrapper.GetFlag(flag1, 0);
            HasGuildMember = BooleanByteWrapper.GetFlag(flag1, 1);
            HasAllianceMember = BooleanByteWrapper.GetFlag(flag1, 2);
            HasGroupMember = BooleanByteWrapper.GetFlag(flag1, 3);
            HasMyTaxCollector = BooleanByteWrapper.GetFlag(flag1, 4);
            TeamMembersCount = reader.ReadSByte();
            if (TeamMembersCount < 0)
                throw new Exception("Forbidden value on TeamMembersCount = " + TeamMembersCount + ", it doesn't respect the following condition : teamMembersCount < 0");
            MeanLevel = reader.ReadVarUhInt();
            if (MeanLevel < 0)
                throw new Exception("Forbidden value on MeanLevel = " + MeanLevel + ", it doesn't respect the following condition : meanLevel < 0");
        }
        
    }
    
}