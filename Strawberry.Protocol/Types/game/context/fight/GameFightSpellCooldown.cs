

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightSpellCooldown
    {
        public const short ProtocolId = 205;
        public virtual short TypeID => ProtocolId;
        
        public int SpellId { get; set; }
        public sbyte Cooldown { get; set; }
        
        public GameFightSpellCooldown()
        {
        }
        
        public GameFightSpellCooldown(int spellId, sbyte cooldown)
        {
            this.SpellId = spellId;
            this.Cooldown = cooldown;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(SpellId);
            writer.WriteSByte(Cooldown);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SpellId = reader.ReadInt();
            Cooldown = reader.ReadSByte();
            if (Cooldown < 0)
                throw new Exception("Forbidden value on Cooldown = " + Cooldown + ", it doesn't respect the following condition : cooldown < 0");
        }
        
    }
    
}