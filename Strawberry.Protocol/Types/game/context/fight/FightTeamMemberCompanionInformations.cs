

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamMemberCompanionInformations : FightTeamMemberInformations
    {
        public new const short ProtocolId = 451;
        public override short TypeID => ProtocolId;
        
        public sbyte CompanionId { get; set; }
        public ushort Level { get; set; }
        public double MasterId { get; set; }
        
        public FightTeamMemberCompanionInformations()
        {
        }
        
        public FightTeamMemberCompanionInformations(double id, sbyte companionId, ushort level, double masterId)
         : base(id)
        {
            this.CompanionId = companionId;
            this.Level = level;
            this.MasterId = masterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(CompanionId);
            writer.WriteVarShort(Level);
            writer.WriteDouble(MasterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CompanionId = reader.ReadSByte();
            if (CompanionId < 0)
                throw new Exception("Forbidden value on CompanionId = " + CompanionId + ", it doesn't respect the following condition : companionId < 0");
            Level = reader.ReadVarUhShort();
            if (Level < 1 || Level > 200)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 1 || level > 200");
            MasterId = reader.ReadDouble();
            if (MasterId < -9007199254740990 || MasterId > 9007199254740990)
                throw new Exception("Forbidden value on MasterId = " + MasterId + ", it doesn't respect the following condition : masterId < -9007199254740990 || masterId > 9007199254740990");
        }
        
    }
    
}