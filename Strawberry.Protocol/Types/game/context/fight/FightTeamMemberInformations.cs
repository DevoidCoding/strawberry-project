

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamMemberInformations
    {
        public const short ProtocolId = 44;
        public virtual short TypeID => ProtocolId;
        
        public double Id { get; set; }
        
        public FightTeamMemberInformations()
        {
        }
        
        public FightTeamMemberInformations(double id)
        {
            this.Id = id;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Id);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
        }
        
    }
    
}