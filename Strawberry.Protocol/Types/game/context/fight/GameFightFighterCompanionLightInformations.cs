

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterCompanionLightInformations : GameFightFighterLightInformations
    {
        public new const short ProtocolId = 454;
        public override short TypeID => ProtocolId;
        
        public sbyte CompanionId { get; set; }
        public double MasterId { get; set; }
        
        public GameFightFighterCompanionLightInformations()
        {
        }
        
        public GameFightFighterCompanionLightInformations(bool sex, bool alive, double id, sbyte wave, ushort level, sbyte breed, sbyte companionId, double masterId)
         : base(sex, alive, id, wave, level, breed)
        {
            this.CompanionId = companionId;
            this.MasterId = masterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(CompanionId);
            writer.WriteDouble(MasterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CompanionId = reader.ReadSByte();
            if (CompanionId < 0)
                throw new Exception("Forbidden value on CompanionId = " + CompanionId + ", it doesn't respect the following condition : companionId < 0");
            MasterId = reader.ReadDouble();
            if (MasterId < -9007199254740990 || MasterId > 9007199254740990)
                throw new Exception("Forbidden value on MasterId = " + MasterId + ", it doesn't respect the following condition : masterId < -9007199254740990 || masterId > 9007199254740990");
        }
        
    }
    
}