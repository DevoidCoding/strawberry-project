

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightMonsterInformations : GameFightAIInformations
    {
        public new const short ProtocolId = 29;
        public override short TypeID => ProtocolId;
        
        public ushort CreatureGenericId { get; set; }
        public sbyte CreatureGrade { get; set; }
        
        public GameFightMonsterInformations()
        {
        }
        
        public GameFightMonsterInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions, ushort creatureGenericId, sbyte creatureGrade)
         : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions)
        {
            this.CreatureGenericId = creatureGenericId;
            this.CreatureGrade = creatureGrade;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CreatureGenericId);
            writer.WriteSByte(CreatureGrade);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CreatureGenericId = reader.ReadVarUhShort();
            if (CreatureGenericId < 0)
                throw new Exception("Forbidden value on CreatureGenericId = " + CreatureGenericId + ", it doesn't respect the following condition : creatureGenericId < 0");
            CreatureGrade = reader.ReadSByte();
            if (CreatureGrade < 0)
                throw new Exception("Forbidden value on CreatureGrade = " + CreatureGrade + ", it doesn't respect the following condition : creatureGrade < 0");
        }
        
    }
    
}