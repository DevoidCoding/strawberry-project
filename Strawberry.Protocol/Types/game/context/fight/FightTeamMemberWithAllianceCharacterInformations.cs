

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamMemberWithAllianceCharacterInformations : FightTeamMemberCharacterInformations
    {
        public new const short ProtocolId = 426;
        public override short TypeID => ProtocolId;
        
        public Types.BasicAllianceInformations AllianceInfos { get; set; }
        
        public FightTeamMemberWithAllianceCharacterInformations()
        {
        }
        
        public FightTeamMemberWithAllianceCharacterInformations(double id, string name, ushort level, Types.BasicAllianceInformations allianceInfos)
         : base(id, name, level)
        {
            this.AllianceInfos = allianceInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            AllianceInfos.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AllianceInfos = new Types.BasicAllianceInformations();
            AllianceInfos.Deserialize(reader);
        }
        
    }
    
}