

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightMutantInformations : GameFightFighterNamedInformations
    {
        public new const short ProtocolId = 50;
        public override short TypeID => ProtocolId;
        
        public sbyte PowerLevel { get; set; }
        
        public GameFightMutantInformations()
        {
        }
        
        public GameFightMutantInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, sbyte wave, bool alive, Types.GameFightMinimalStats stats, ushort[] previousPositions, string name, Types.PlayerStatus status, sbyte powerLevel)
         : base(contextualId, look, disposition, teamId, wave, alive, stats, previousPositions, name, status)
        {
            this.PowerLevel = powerLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PowerLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PowerLevel = reader.ReadSByte();
            if (PowerLevel < 0)
                throw new Exception("Forbidden value on PowerLevel = " + PowerLevel + ", it doesn't respect the following condition : powerLevel < 0");
        }
        
    }
    
}