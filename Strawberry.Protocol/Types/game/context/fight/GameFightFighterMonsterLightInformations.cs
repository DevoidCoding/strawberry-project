

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameFightFighterMonsterLightInformations : GameFightFighterLightInformations
    {
        public new const short ProtocolId = 455;
        public override short TypeID => ProtocolId;
        
        public ushort CreatureGenericId { get; set; }
        
        public GameFightFighterMonsterLightInformations()
        {
        }
        
        public GameFightFighterMonsterLightInformations(bool sex, bool alive, double id, sbyte wave, ushort level, sbyte breed, ushort creatureGenericId)
         : base(sex, alive, id, wave, level, breed)
        {
            this.CreatureGenericId = creatureGenericId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CreatureGenericId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CreatureGenericId = reader.ReadVarUhShort();
            if (CreatureGenericId < 0)
                throw new Exception("Forbidden value on CreatureGenericId = " + CreatureGenericId + ", it doesn't respect the following condition : creatureGenericId < 0");
        }
        
    }
    
}