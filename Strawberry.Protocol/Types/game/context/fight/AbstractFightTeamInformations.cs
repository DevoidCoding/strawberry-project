

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AbstractFightTeamInformations
    {
        public const short ProtocolId = 116;
        public virtual short TypeID => ProtocolId;
        
        public sbyte TeamId { get; set; }
        public double LeaderId { get; set; }
        public sbyte TeamSide { get; set; }
        public sbyte TeamTypeId { get; set; }
        public sbyte NbWaves { get; set; }
        
        public AbstractFightTeamInformations()
        {
        }
        
        public AbstractFightTeamInformations(sbyte teamId, double leaderId, sbyte teamSide, sbyte teamTypeId, sbyte nbWaves)
        {
            this.TeamId = teamId;
            this.LeaderId = leaderId;
            this.TeamSide = teamSide;
            this.TeamTypeId = teamTypeId;
            this.NbWaves = nbWaves;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(TeamId);
            writer.WriteDouble(LeaderId);
            writer.WriteSByte(TeamSide);
            writer.WriteSByte(TeamTypeId);
            writer.WriteSByte(NbWaves);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            LeaderId = reader.ReadDouble();
            if (LeaderId < -9007199254740990 || LeaderId > 9007199254740990)
                throw new Exception("Forbidden value on LeaderId = " + LeaderId + ", it doesn't respect the following condition : leaderId < -9007199254740990 || leaderId > 9007199254740990");
            TeamSide = reader.ReadSByte();
            TeamTypeId = reader.ReadSByte();
            if (TeamTypeId < 0)
                throw new Exception("Forbidden value on TeamTypeId = " + TeamTypeId + ", it doesn't respect the following condition : teamTypeId < 0");
            NbWaves = reader.ReadSByte();
            if (NbWaves < 0)
                throw new Exception("Forbidden value on NbWaves = " + NbWaves + ", it doesn't respect the following condition : nbWaves < 0");
        }
        
    }
    
}