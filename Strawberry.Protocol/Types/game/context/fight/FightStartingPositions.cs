

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightStartingPositions
    {
        public const short ProtocolId = 513;
        public virtual short TypeID => ProtocolId;
        
        public ushort[] PositionsForChallengers { get; set; }
        public ushort[] PositionsForDefenders { get; set; }
        
        public FightStartingPositions()
        {
        }
        
        public FightStartingPositions(ushort[] positionsForChallengers, ushort[] positionsForDefenders)
        {
            this.PositionsForChallengers = positionsForChallengers;
            this.PositionsForDefenders = positionsForDefenders;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)PositionsForChallengers.Length);
            foreach (var entry in PositionsForChallengers)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)PositionsForDefenders.Length);
            foreach (var entry in PositionsForDefenders)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            PositionsForChallengers = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PositionsForChallengers[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            PositionsForDefenders = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PositionsForDefenders[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}