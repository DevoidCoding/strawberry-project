

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightResultTaxCollectorListEntry : FightResultFighterListEntry
    {
        public new const short ProtocolId = 84;
        public override short TypeID => ProtocolId;
        
        public byte Level { get; set; }
        public Types.BasicGuildInformations GuildInfo { get; set; }
        public int ExperienceForGuild { get; set; }
        
        public FightResultTaxCollectorListEntry()
        {
        }
        
        public FightResultTaxCollectorListEntry(ushort outcome, sbyte wave, Types.FightLoot rewards, double id, bool alive, byte level, Types.BasicGuildInformations guildInfo, int experienceForGuild)
         : base(outcome, wave, rewards, id, alive)
        {
            this.Level = level;
            this.GuildInfo = guildInfo;
            this.ExperienceForGuild = experienceForGuild;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Level);
            GuildInfo.Serialize(writer);
            writer.WriteInt(ExperienceForGuild);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Level = reader.ReadByte();
            if (Level < 1 || Level > 200)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 1 || level > 200");
            GuildInfo = new Types.BasicGuildInformations();
            GuildInfo.Deserialize(reader);
            ExperienceForGuild = reader.ReadInt();
        }
        
    }
    
}