

// Generated on 02/12/2018 03:56:52
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightTeamMemberTaxCollectorInformations : FightTeamMemberInformations
    {
        public new const short ProtocolId = 177;
        public override short TypeID => ProtocolId;
        
        public ushort FirstNameId { get; set; }
        public ushort LastNameId { get; set; }
        public byte Level { get; set; }
        public uint GuildId { get; set; }
        public double Uid { get; set; }
        
        public FightTeamMemberTaxCollectorInformations()
        {
        }
        
        public FightTeamMemberTaxCollectorInformations(double id, ushort firstNameId, ushort lastNameId, byte level, uint guildId, double uid)
         : base(id)
        {
            this.FirstNameId = firstNameId;
            this.LastNameId = lastNameId;
            this.Level = level;
            this.GuildId = guildId;
            this.Uid = uid;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteByte(Level);
            writer.WriteVarInt(GuildId);
            writer.WriteDouble(Uid);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FirstNameId = reader.ReadVarUhShort();
            if (FirstNameId < 0)
                throw new Exception("Forbidden value on FirstNameId = " + FirstNameId + ", it doesn't respect the following condition : firstNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
            Level = reader.ReadByte();
            if (Level < 1 || Level > 200)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 1 || level > 200");
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
            Uid = reader.ReadDouble();
            if (Uid < 0 || Uid > 9007199254740990)
                throw new Exception("Forbidden value on Uid = " + Uid + ", it doesn't respect the following condition : uid < 0 || uid > 9007199254740990");
        }
        
    }
    
}