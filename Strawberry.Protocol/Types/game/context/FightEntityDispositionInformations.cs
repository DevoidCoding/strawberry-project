

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FightEntityDispositionInformations : EntityDispositionInformations
    {
        public new const short ProtocolId = 217;
        public override short TypeID => ProtocolId;
        
        public double CarryingCharacterId { get; set; }
        
        public FightEntityDispositionInformations()
        {
        }
        
        public FightEntityDispositionInformations(short cellId, sbyte direction, double carryingCharacterId)
         : base(cellId, direction)
        {
            this.CarryingCharacterId = carryingCharacterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(CarryingCharacterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CarryingCharacterId = reader.ReadDouble();
            if (CarryingCharacterId < -9007199254740990 || CarryingCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CarryingCharacterId = " + CarryingCharacterId + ", it doesn't respect the following condition : carryingCharacterId < -9007199254740990 || carryingCharacterId > 9007199254740990");
        }
        
    }
    
}