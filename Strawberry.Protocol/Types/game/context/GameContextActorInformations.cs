

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameContextActorInformations
    {
        public const short ProtocolId = 150;
        public virtual short TypeID => ProtocolId;
        
        public double ContextualId { get; set; }
        public Types.EntityLook Look { get; set; }
        public Types.EntityDispositionInformations Disposition { get; set; }
        
        public GameContextActorInformations()
        {
        }
        
        public GameContextActorInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition)
        {
            this.ContextualId = contextualId;
            this.Look = look;
            this.Disposition = disposition;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(ContextualId);
            Look.Serialize(writer);
            writer.WriteShort(Disposition.TypeID);
            Disposition.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ContextualId = reader.ReadDouble();
            if (ContextualId < -9007199254740990 || ContextualId > 9007199254740990)
                throw new Exception("Forbidden value on ContextualId = " + ContextualId + ", it doesn't respect the following condition : contextualId < -9007199254740990 || contextualId > 9007199254740990");
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
            Disposition = Types.ProtocolTypeManager.GetInstance<Types.EntityDispositionInformations>(reader.ReadShort());
            Disposition.Deserialize(reader);
        }
        
    }
    
}