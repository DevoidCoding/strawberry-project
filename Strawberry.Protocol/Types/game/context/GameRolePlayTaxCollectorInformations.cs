

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayTaxCollectorInformations : GameRolePlayActorInformations
    {
        public new const short ProtocolId = 148;
        public override short TypeID => ProtocolId;
        
        public Types.TaxCollectorStaticInformations Identification { get; set; }
        public byte GuildLevel { get; set; }
        public int TaxCollectorAttack { get; set; }
        
        public GameRolePlayTaxCollectorInformations()
        {
        }
        
        public GameRolePlayTaxCollectorInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, Types.TaxCollectorStaticInformations identification, byte guildLevel, int taxCollectorAttack)
         : base(contextualId, look, disposition)
        {
            this.Identification = identification;
            this.GuildLevel = guildLevel;
            this.TaxCollectorAttack = taxCollectorAttack;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Identification.TypeID);
            Identification.Serialize(writer);
            writer.WriteByte(GuildLevel);
            writer.WriteInt(TaxCollectorAttack);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Identification = Types.ProtocolTypeManager.GetInstance<Types.TaxCollectorStaticInformations>(reader.ReadShort());
            Identification.Deserialize(reader);
            GuildLevel = reader.ReadByte();
            if (GuildLevel < 0 || GuildLevel > 255)
                throw new Exception("Forbidden value on GuildLevel = " + GuildLevel + ", it doesn't respect the following condition : guildLevel < 0 || guildLevel > 255");
            TaxCollectorAttack = reader.ReadInt();
        }
        
    }
    
}