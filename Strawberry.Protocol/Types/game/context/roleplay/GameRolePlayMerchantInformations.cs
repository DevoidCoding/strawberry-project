

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayMerchantInformations : GameRolePlayNamedActorInformations
    {
        public new const short ProtocolId = 129;
        public override short TypeID => ProtocolId;
        
        public sbyte SellType { get; set; }
        public Types.HumanOption[] Options { get; set; }
        
        public GameRolePlayMerchantInformations()
        {
        }
        
        public GameRolePlayMerchantInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, string name, sbyte sellType, Types.HumanOption[] options)
         : base(contextualId, look, disposition, name)
        {
            this.SellType = sellType;
            this.Options = options;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(SellType);
            writer.WriteUShort((ushort)Options.Length);
            foreach (var entry in Options)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SellType = reader.ReadSByte();
            if (SellType < 0)
                throw new Exception("Forbidden value on SellType = " + SellType + ", it doesn't respect the following condition : sellType < 0");
            var limit = reader.ReadUShort();
            Options = new Types.HumanOption[limit];
            for (int i = 0; i < limit; i++)
            {
                 Options[i] = Types.ProtocolTypeManager.GetInstance<Types.HumanOption>(reader.ReadShort());
                 Options[i].Deserialize(reader);
            }
        }
        
    }
    
}