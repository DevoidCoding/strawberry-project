

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyMemberArenaInformations : PartyMemberInformations
    {
        public new const short ProtocolId = 391;
        public override short TypeID => ProtocolId;
        
        public ushort Rank { get; set; }
        
        public PartyMemberArenaInformations()
        {
        }
        
        public PartyMemberArenaInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed, bool sex, uint lifePoints, uint maxLifePoints, ushort prospecting, byte regenRate, ushort initiative, sbyte alignmentSide, short worldX, short worldY, double mapId, ushort subAreaId, Types.PlayerStatus status, Types.PartyCompanionMemberInformations[] companions, ushort rank)
         : base(id, name, level, entityLook, breed, sex, lifePoints, maxLifePoints, prospecting, regenRate, initiative, alignmentSide, worldX, worldY, mapId, subAreaId, status, companions)
        {
            this.Rank = rank;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Rank);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Rank = reader.ReadVarUhShort();
            if (Rank < 0 || Rank > 20000)
                throw new Exception("Forbidden value on Rank = " + Rank + ", it doesn't respect the following condition : rank < 0 || rank > 20000");
        }
        
    }
    
}