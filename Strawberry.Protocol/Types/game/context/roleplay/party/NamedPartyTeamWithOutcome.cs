

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class NamedPartyTeamWithOutcome
    {
        public const short ProtocolId = 470;
        public virtual short TypeID => ProtocolId;
        
        public Types.NamedPartyTeam Team { get; set; }
        public ushort Outcome { get; set; }
        
        public NamedPartyTeamWithOutcome()
        {
        }
        
        public NamedPartyTeamWithOutcome(Types.NamedPartyTeam team, ushort outcome)
        {
            this.Team = team;
            this.Outcome = outcome;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            Team.Serialize(writer);
            writer.WriteVarShort(Outcome);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Team = new Types.NamedPartyTeam();
            Team.Deserialize(reader);
            Outcome = reader.ReadVarUhShort();
            if (Outcome < 0)
                throw new Exception("Forbidden value on Outcome = " + Outcome + ", it doesn't respect the following condition : outcome < 0");
        }
        
    }
    
}