

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyMemberInformations : CharacterBaseInformations
    {
        public new const short ProtocolId = 90;
        public override short TypeID => ProtocolId;
        
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        public ushort Prospecting { get; set; }
        public byte RegenRate { get; set; }
        public ushort Initiative { get; set; }
        public sbyte AlignmentSide { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        public Types.PlayerStatus Status { get; set; }
        public Types.PartyCompanionMemberInformations[] Companions { get; set; }
        
        public PartyMemberInformations()
        {
        }
        
        public PartyMemberInformations(ulong id, string name, ushort level, Types.EntityLook entityLook, sbyte breed, bool sex, uint lifePoints, uint maxLifePoints, ushort prospecting, byte regenRate, ushort initiative, sbyte alignmentSide, short worldX, short worldY, double mapId, ushort subAreaId, Types.PlayerStatus status, Types.PartyCompanionMemberInformations[] companions)
         : base(id, name, level, entityLook, breed, sex)
        {
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
            this.Prospecting = prospecting;
            this.RegenRate = regenRate;
            this.Initiative = initiative;
            this.AlignmentSide = alignmentSide;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
            this.Status = status;
            this.Companions = companions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
            writer.WriteVarShort(Initiative);
            writer.WriteSByte(AlignmentSide);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
            writer.WriteUShort((ushort)Companions.Length);
            foreach (var entry in Companions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
            Prospecting = reader.ReadVarUhShort();
            if (Prospecting < 0)
                throw new Exception("Forbidden value on Prospecting = " + Prospecting + ", it doesn't respect the following condition : prospecting < 0");
            RegenRate = reader.ReadByte();
            if (RegenRate < 0 || RegenRate > 255)
                throw new Exception("Forbidden value on RegenRate = " + RegenRate + ", it doesn't respect the following condition : regenRate < 0 || regenRate > 255");
            Initiative = reader.ReadVarUhShort();
            if (Initiative < 0)
                throw new Exception("Forbidden value on Initiative = " + Initiative + ", it doesn't respect the following condition : initiative < 0");
            AlignmentSide = reader.ReadSByte();
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
            var limit = reader.ReadUShort();
            Companions = new Types.PartyCompanionMemberInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Companions[i] = new Types.PartyCompanionMemberInformations();
                 Companions[i].Deserialize(reader);
            }
        }
        
    }
    
}