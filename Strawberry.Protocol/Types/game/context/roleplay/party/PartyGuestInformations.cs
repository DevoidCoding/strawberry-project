

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyGuestInformations
    {
        public const short ProtocolId = 374;
        public virtual short TypeID => ProtocolId;
        
        public ulong GuestId { get; set; }
        public ulong HostId { get; set; }
        public string Name { get; set; }
        public Types.EntityLook GuestLook { get; set; }
        public sbyte Breed { get; set; }
        public bool Sex { get; set; }
        public Types.PlayerStatus Status { get; set; }
        public Types.PartyCompanionBaseInformations[] Companions { get; set; }
        
        public PartyGuestInformations()
        {
        }
        
        public PartyGuestInformations(ulong guestId, ulong hostId, string name, Types.EntityLook guestLook, sbyte breed, bool sex, Types.PlayerStatus status, Types.PartyCompanionBaseInformations[] companions)
        {
            this.GuestId = guestId;
            this.HostId = hostId;
            this.Name = name;
            this.GuestLook = guestLook;
            this.Breed = breed;
            this.Sex = sex;
            this.Status = status;
            this.Companions = companions;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(GuestId);
            writer.WriteVarLong(HostId);
            writer.WriteUTF(Name);
            GuestLook.Serialize(writer);
            writer.WriteSByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
            writer.WriteUShort((ushort)Companions.Length);
            foreach (var entry in Companions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            GuestId = reader.ReadVarUhLong();
            if (GuestId < 0 || GuestId > 9007199254740990)
                throw new Exception("Forbidden value on GuestId = " + GuestId + ", it doesn't respect the following condition : guestId < 0 || guestId > 9007199254740990");
            HostId = reader.ReadVarUhLong();
            if (HostId < 0 || HostId > 9007199254740990)
                throw new Exception("Forbidden value on HostId = " + HostId + ", it doesn't respect the following condition : hostId < 0 || hostId > 9007199254740990");
            Name = reader.ReadUTF();
            GuestLook = new Types.EntityLook();
            GuestLook.Deserialize(reader);
            Breed = reader.ReadSByte();
            Sex = reader.ReadBoolean();
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
            var limit = reader.ReadUShort();
            Companions = new Types.PartyCompanionBaseInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Companions[i] = new Types.PartyCompanionBaseInformations();
                 Companions[i].Deserialize(reader);
            }
        }
        
    }
    
}