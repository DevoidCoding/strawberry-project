

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyCompanionBaseInformations
    {
        public const short ProtocolId = 453;
        public virtual short TypeID => ProtocolId;
        
        public sbyte IndexId { get; set; }
        public sbyte CompanionGenericId { get; set; }
        public Types.EntityLook EntityLook { get; set; }
        
        public PartyCompanionBaseInformations()
        {
        }
        
        public PartyCompanionBaseInformations(sbyte indexId, sbyte companionGenericId, Types.EntityLook entityLook)
        {
            this.IndexId = indexId;
            this.CompanionGenericId = companionGenericId;
            this.EntityLook = entityLook;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(IndexId);
            writer.WriteSByte(CompanionGenericId);
            EntityLook.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            IndexId = reader.ReadSByte();
            if (IndexId < 0)
                throw new Exception("Forbidden value on IndexId = " + IndexId + ", it doesn't respect the following condition : indexId < 0");
            CompanionGenericId = reader.ReadSByte();
            if (CompanionGenericId < 0)
                throw new Exception("Forbidden value on CompanionGenericId = " + CompanionGenericId + ", it doesn't respect the following condition : companionGenericId < 0");
            EntityLook = new Types.EntityLook();
            EntityLook.Deserialize(reader);
        }
        
    }
    
}