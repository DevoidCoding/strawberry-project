

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyCompanionMemberInformations : PartyCompanionBaseInformations
    {
        public new const short ProtocolId = 452;
        public override short TypeID => ProtocolId;
        
        public ushort Initiative { get; set; }
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        public ushort Prospecting { get; set; }
        public byte RegenRate { get; set; }
        
        public PartyCompanionMemberInformations()
        {
        }
        
        public PartyCompanionMemberInformations(sbyte indexId, sbyte companionGenericId, Types.EntityLook entityLook, ushort initiative, uint lifePoints, uint maxLifePoints, ushort prospecting, byte regenRate)
         : base(indexId, companionGenericId, entityLook)
        {
            this.Initiative = initiative;
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
            this.Prospecting = prospecting;
            this.RegenRate = regenRate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Initiative);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Initiative = reader.ReadVarUhShort();
            if (Initiative < 0)
                throw new Exception("Forbidden value on Initiative = " + Initiative + ", it doesn't respect the following condition : initiative < 0");
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
            Prospecting = reader.ReadVarUhShort();
            if (Prospecting < 0)
                throw new Exception("Forbidden value on Prospecting = " + Prospecting + ", it doesn't respect the following condition : prospecting < 0");
            RegenRate = reader.ReadByte();
            if (RegenRate < 0 || RegenRate > 255)
                throw new Exception("Forbidden value on RegenRate = " + RegenRate + ", it doesn't respect the following condition : regenRate < 0 || regenRate > 255");
        }
        
    }
    
}