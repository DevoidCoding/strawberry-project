

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class NamedPartyTeam
    {
        public const short ProtocolId = 469;
        public virtual short TypeID => ProtocolId;
        
        public sbyte TeamId { get; set; }
        public string PartyName { get; set; }
        
        public NamedPartyTeam()
        {
        }
        
        public NamedPartyTeam(sbyte teamId, string partyName)
        {
            this.TeamId = teamId;
            this.PartyName = partyName;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(TeamId);
            writer.WriteUTF(PartyName);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            PartyName = reader.ReadUTF();
        }
        
    }
    
}