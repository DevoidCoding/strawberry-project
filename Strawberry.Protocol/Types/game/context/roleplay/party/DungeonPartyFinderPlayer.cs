

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DungeonPartyFinderPlayer
    {
        public const short ProtocolId = 373;
        public virtual short TypeID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public string PlayerName { get; set; }
        public sbyte Breed { get; set; }
        public bool Sex { get; set; }
        public ushort Level { get; set; }
        
        public DungeonPartyFinderPlayer()
        {
        }
        
        public DungeonPartyFinderPlayer(ulong playerId, string playerName, sbyte breed, bool sex, ushort level)
        {
            this.PlayerId = playerId;
            this.PlayerName = playerName;
            this.Breed = breed;
            this.Sex = sex;
            this.Level = level;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteSByte(Breed);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(Level);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            PlayerName = reader.ReadUTF();
            Breed = reader.ReadSByte();
            if (Breed < (byte)Enums.PlayableBreedEnum.Feca || Breed > (byte)Enums.PlayableBreedEnum.Ouginak)
                throw new Exception("Forbidden value on Breed = " + Breed + ", it doesn't respect the following condition : breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Ouginak");
            Sex = reader.ReadBoolean();
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
        }
        
    }
    
}