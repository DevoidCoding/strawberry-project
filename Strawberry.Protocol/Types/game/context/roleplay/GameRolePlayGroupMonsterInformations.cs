

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayGroupMonsterInformations : GameRolePlayActorInformations
    {
        public new const short ProtocolId = 160;
        public override short TypeID => ProtocolId;
        
        public bool KeyRingBonus { get; set; }
        public bool HasHardcoreDrop { get; set; }
        public bool HasAVARewardToken { get; set; }
        public Types.GroupMonsterStaticInformations StaticInfos { get; set; }
        public double CreationTime { get; set; }
        public int AgeBonusRate { get; set; }
        public sbyte LootShare { get; set; }
        public sbyte AlignmentSide { get; set; }
        
        public GameRolePlayGroupMonsterInformations()
        {
        }
        
        public GameRolePlayGroupMonsterInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, bool keyRingBonus, bool hasHardcoreDrop, bool hasAVARewardToken, Types.GroupMonsterStaticInformations staticInfos, double creationTime, int ageBonusRate, sbyte lootShare, sbyte alignmentSide)
         : base(contextualId, look, disposition)
        {
            this.KeyRingBonus = keyRingBonus;
            this.HasHardcoreDrop = hasHardcoreDrop;
            this.HasAVARewardToken = hasAVARewardToken;
            this.StaticInfos = staticInfos;
            this.CreationTime = creationTime;
            this.AgeBonusRate = ageBonusRate;
            this.LootShare = lootShare;
            this.AlignmentSide = alignmentSide;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, KeyRingBonus);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, HasHardcoreDrop);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, HasAVARewardToken);
            writer.WriteByte(flag1);
            writer.WriteShort(StaticInfos.TypeID);
            StaticInfos.Serialize(writer);
            writer.WriteDouble(CreationTime);
            writer.WriteInt(AgeBonusRate);
            writer.WriteSByte(LootShare);
            writer.WriteSByte(AlignmentSide);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            KeyRingBonus = BooleanByteWrapper.GetFlag(flag1, 0);
            HasHardcoreDrop = BooleanByteWrapper.GetFlag(flag1, 1);
            HasAVARewardToken = BooleanByteWrapper.GetFlag(flag1, 2);
            StaticInfos = Types.ProtocolTypeManager.GetInstance<Types.GroupMonsterStaticInformations>(reader.ReadShort());
            StaticInfos.Deserialize(reader);
            CreationTime = reader.ReadDouble();
            if (CreationTime < 0 || CreationTime > 9007199254740990)
                throw new Exception("Forbidden value on CreationTime = " + CreationTime + ", it doesn't respect the following condition : creationTime < 0 || creationTime > 9007199254740990");
            AgeBonusRate = reader.ReadInt();
            if (AgeBonusRate < 0)
                throw new Exception("Forbidden value on AgeBonusRate = " + AgeBonusRate + ", it doesn't respect the following condition : ageBonusRate < 0");
            LootShare = reader.ReadSByte();
            if (LootShare < -1 || LootShare > 8)
                throw new Exception("Forbidden value on LootShare = " + LootShare + ", it doesn't respect the following condition : lootShare < -1 || lootShare > 8");
            AlignmentSide = reader.ReadSByte();
        }
        
    }
    
}