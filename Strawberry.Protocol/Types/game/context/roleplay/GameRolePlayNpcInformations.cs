

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayNpcInformations : GameRolePlayActorInformations
    {
        public new const short ProtocolId = 156;
        public override short TypeID => ProtocolId;
        
        public ushort NpcId { get; set; }
        public bool Sex { get; set; }
        public ushort SpecialArtworkId { get; set; }
        
        public GameRolePlayNpcInformations()
        {
        }
        
        public GameRolePlayNpcInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, ushort npcId, bool sex, ushort specialArtworkId)
         : base(contextualId, look, disposition)
        {
            this.NpcId = npcId;
            this.Sex = sex;
            this.SpecialArtworkId = specialArtworkId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(NpcId);
            writer.WriteBoolean(Sex);
            writer.WriteVarShort(SpecialArtworkId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            NpcId = reader.ReadVarUhShort();
            if (NpcId < 0)
                throw new Exception("Forbidden value on NpcId = " + NpcId + ", it doesn't respect the following condition : npcId < 0");
            Sex = reader.ReadBoolean();
            SpecialArtworkId = reader.ReadVarUhShort();
            if (SpecialArtworkId < 0)
                throw new Exception("Forbidden value on SpecialArtworkId = " + SpecialArtworkId + ", it doesn't respect the following condition : specialArtworkId < 0");
        }
        
    }
    
}