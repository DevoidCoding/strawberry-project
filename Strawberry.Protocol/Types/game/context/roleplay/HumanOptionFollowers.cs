

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionFollowers : HumanOption
    {
        public new const short ProtocolId = 410;
        public override short TypeID => ProtocolId;
        
        public Types.IndexedEntityLook[] FollowingCharactersLook { get; set; }
        
        public HumanOptionFollowers()
        {
        }
        
        public HumanOptionFollowers(Types.IndexedEntityLook[] followingCharactersLook)
        {
            this.FollowingCharactersLook = followingCharactersLook;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)FollowingCharactersLook.Length);
            foreach (var entry in FollowingCharactersLook)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            FollowingCharactersLook = new Types.IndexedEntityLook[limit];
            for (int i = 0; i < limit; i++)
            {
                 FollowingCharactersLook[i] = new Types.IndexedEntityLook();
                 FollowingCharactersLook[i].Deserialize(reader);
            }
        }
        
    }
    
}