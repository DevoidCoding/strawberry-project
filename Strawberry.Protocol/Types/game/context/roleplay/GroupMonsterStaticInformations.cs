

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GroupMonsterStaticInformations
    {
        public const short ProtocolId = 140;
        public virtual short TypeID => ProtocolId;
        
        public Types.MonsterInGroupLightInformations MainCreatureLightInfos { get; set; }
        public Types.MonsterInGroupInformations[] Underlings { get; set; }
        
        public GroupMonsterStaticInformations()
        {
        }
        
        public GroupMonsterStaticInformations(Types.MonsterInGroupLightInformations mainCreatureLightInfos, Types.MonsterInGroupInformations[] underlings)
        {
            this.MainCreatureLightInfos = mainCreatureLightInfos;
            this.Underlings = underlings;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            MainCreatureLightInfos.Serialize(writer);
            writer.WriteUShort((ushort)Underlings.Length);
            foreach (var entry in Underlings)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            MainCreatureLightInfos = new Types.MonsterInGroupLightInformations();
            MainCreatureLightInfos.Deserialize(reader);
            var limit = reader.ReadUShort();
            Underlings = new Types.MonsterInGroupInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Underlings[i] = new Types.MonsterInGroupInformations();
                 Underlings[i].Deserialize(reader);
            }
        }
        
    }
    
}