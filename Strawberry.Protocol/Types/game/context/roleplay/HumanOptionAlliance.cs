

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionAlliance : HumanOption
    {
        public new const short ProtocolId = 425;
        public override short TypeID => ProtocolId;
        
        public Types.AllianceInformations AllianceInformations { get; set; }
        public sbyte Aggressable { get; set; }
        
        public HumanOptionAlliance()
        {
        }
        
        public HumanOptionAlliance(Types.AllianceInformations allianceInformations, sbyte aggressable)
        {
            this.AllianceInformations = allianceInformations;
            this.Aggressable = aggressable;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            AllianceInformations.Serialize(writer);
            writer.WriteSByte(Aggressable);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AllianceInformations = new Types.AllianceInformations();
            AllianceInformations.Deserialize(reader);
            Aggressable = reader.ReadSByte();
            if (Aggressable < 0)
                throw new Exception("Forbidden value on Aggressable = " + Aggressable + ", it doesn't respect the following condition : aggressable < 0");
        }
        
    }
    
}