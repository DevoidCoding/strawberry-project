

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionOrnament : HumanOption
    {
        public new const short ProtocolId = 411;
        public override short TypeID => ProtocolId;
        
        public ushort OrnamentId { get; set; }
        public ushort Level { get; set; }
        
        public HumanOptionOrnament()
        {
        }
        
        public HumanOptionOrnament(ushort ornamentId, ushort level)
        {
            this.OrnamentId = ornamentId;
            this.Level = level;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(OrnamentId);
            writer.WriteVarShort(Level);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            OrnamentId = reader.ReadVarUhShort();
            if (OrnamentId < 0)
                throw new Exception("Forbidden value on OrnamentId = " + OrnamentId + ", it doesn't respect the following condition : ornamentId < 0");
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
        }
        
    }
    
}