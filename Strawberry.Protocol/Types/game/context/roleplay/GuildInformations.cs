

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildInformations : BasicGuildInformations
    {
        public new const short ProtocolId = 127;
        public override short TypeID => ProtocolId;
        
        public Types.GuildEmblem GuildEmblem { get; set; }
        
        public GuildInformations()
        {
        }
        
        public GuildInformations(uint guildId, string guildName, byte guildLevel, Types.GuildEmblem guildEmblem)
         : base(guildId, guildName, guildLevel)
        {
            this.GuildEmblem = guildEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            GuildEmblem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            GuildEmblem = new Types.GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
        
    }
    
}