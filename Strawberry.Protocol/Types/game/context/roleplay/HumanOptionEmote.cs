

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionEmote : HumanOption
    {
        public new const short ProtocolId = 407;
        public override short TypeID => ProtocolId;
        
        public byte EmoteId { get; set; }
        public double EmoteStartTime { get; set; }
        
        public HumanOptionEmote()
        {
        }
        
        public HumanOptionEmote(byte emoteId, double emoteStartTime)
        {
            this.EmoteId = emoteId;
            this.EmoteStartTime = emoteStartTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(EmoteId);
            writer.WriteDouble(EmoteStartTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            EmoteId = reader.ReadByte();
            if (EmoteId < 0 || EmoteId > 255)
                throw new Exception("Forbidden value on EmoteId = " + EmoteId + ", it doesn't respect the following condition : emoteId < 0 || emoteId > 255");
            EmoteStartTime = reader.ReadDouble();
            if (EmoteStartTime < -9007199254740990 || EmoteStartTime > 9007199254740990)
                throw new Exception("Forbidden value on EmoteStartTime = " + EmoteStartTime + ", it doesn't respect the following condition : emoteStartTime < -9007199254740990 || emoteStartTime > 9007199254740990");
        }
        
    }
    
}