

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ArenaRankInfos
    {
        public const short ProtocolId = 499;
        public virtual short TypeID => ProtocolId;
        
        public ushort Rank { get; set; }
        public ushort BestRank { get; set; }
        public ushort VictoryCount { get; set; }
        public ushort Fightcount { get; set; }
        public bool ValidForLadder { get; set; }
        
        public ArenaRankInfos()
        {
        }
        
        public ArenaRankInfos(ushort rank, ushort bestRank, ushort victoryCount, ushort fightcount, bool validForLadder)
        {
            this.Rank = rank;
            this.BestRank = bestRank;
            this.VictoryCount = victoryCount;
            this.Fightcount = fightcount;
            this.ValidForLadder = validForLadder;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Rank);
            writer.WriteVarShort(BestRank);
            writer.WriteVarShort(VictoryCount);
            writer.WriteVarShort(Fightcount);
            writer.WriteBoolean(ValidForLadder);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Rank = reader.ReadVarUhShort();
            if (Rank < 0 || Rank > 20000)
                throw new Exception("Forbidden value on Rank = " + Rank + ", it doesn't respect the following condition : rank < 0 || rank > 20000");
            BestRank = reader.ReadVarUhShort();
            if (BestRank < 0 || BestRank > 20000)
                throw new Exception("Forbidden value on BestRank = " + BestRank + ", it doesn't respect the following condition : bestRank < 0 || bestRank > 20000");
            VictoryCount = reader.ReadVarUhShort();
            if (VictoryCount < 0)
                throw new Exception("Forbidden value on VictoryCount = " + VictoryCount + ", it doesn't respect the following condition : victoryCount < 0");
            Fightcount = reader.ReadVarUhShort();
            if (Fightcount < 0)
                throw new Exception("Forbidden value on Fightcount = " + Fightcount + ", it doesn't respect the following condition : fightcount < 0");
            ValidForLadder = reader.ReadBoolean();
        }
        
    }
    
}