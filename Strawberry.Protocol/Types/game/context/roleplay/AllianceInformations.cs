

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AllianceInformations : BasicNamedAllianceInformations
    {
        public new const short ProtocolId = 417;
        public override short TypeID => ProtocolId;
        
        public Types.GuildEmblem AllianceEmblem { get; set; }
        
        public AllianceInformations()
        {
        }
        
        public AllianceInformations(uint allianceId, string allianceTag, string allianceName, Types.GuildEmblem allianceEmblem)
         : base(allianceId, allianceTag, allianceName)
        {
            this.AllianceEmblem = allianceEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            AllianceEmblem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AllianceEmblem = new Types.GuildEmblem();
            AllianceEmblem.Deserialize(reader);
        }
        
    }
    
}