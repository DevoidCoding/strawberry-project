

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AlternativeMonstersInGroupLightInformations
    {
        public const short ProtocolId = 394;
        public virtual short TypeID => ProtocolId;
        
        public int PlayerCount { get; set; }
        public Types.MonsterInGroupLightInformations[] Monsters { get; set; }
        
        public AlternativeMonstersInGroupLightInformations()
        {
        }
        
        public AlternativeMonstersInGroupLightInformations(int playerCount, Types.MonsterInGroupLightInformations[] monsters)
        {
            this.PlayerCount = playerCount;
            this.Monsters = monsters;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(PlayerCount);
            writer.WriteUShort((ushort)Monsters.Length);
            foreach (var entry in Monsters)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            PlayerCount = reader.ReadInt();
            var limit = reader.ReadUShort();
            Monsters = new Types.MonsterInGroupLightInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Monsters[i] = new Types.MonsterInGroupLightInformations();
                 Monsters[i].Deserialize(reader);
            }
        }
        
    }
    
}