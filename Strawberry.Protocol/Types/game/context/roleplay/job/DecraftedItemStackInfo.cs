

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DecraftedItemStackInfo
    {
        public const short ProtocolId = 481;
        public virtual short TypeID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public float BonusMin { get; set; }
        public float BonusMax { get; set; }
        public ushort[] RunesId { get; set; }
        public uint[] RunesQty { get; set; }
        
        public DecraftedItemStackInfo()
        {
        }
        
        public DecraftedItemStackInfo(uint objectUID, float bonusMin, float bonusMax, ushort[] runesId, uint[] runesQty)
        {
            this.ObjectUID = objectUID;
            this.BonusMin = bonusMin;
            this.BonusMax = bonusMax;
            this.RunesId = runesId;
            this.RunesQty = runesQty;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteFloat(BonusMin);
            writer.WriteFloat(BonusMax);
            writer.WriteUShort((ushort)RunesId.Length);
            foreach (var entry in RunesId)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)RunesQty.Length);
            foreach (var entry in RunesQty)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            BonusMin = reader.ReadFloat();
            BonusMax = reader.ReadFloat();
            var limit = reader.ReadUShort();
            RunesId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 RunesId[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            RunesQty = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 RunesQty[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}