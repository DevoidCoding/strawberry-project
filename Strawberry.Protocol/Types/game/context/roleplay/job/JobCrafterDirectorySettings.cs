

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobCrafterDirectorySettings
    {
        public const short ProtocolId = 97;
        public virtual short TypeID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public byte MinLevel { get; set; }
        public bool Free { get; set; }
        
        public JobCrafterDirectorySettings()
        {
        }
        
        public JobCrafterDirectorySettings(sbyte jobId, byte minLevel, bool free)
        {
            this.JobId = jobId;
            this.MinLevel = minLevel;
            this.Free = free;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteByte(MinLevel);
            writer.WriteBoolean(Free);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            MinLevel = reader.ReadByte();
            if (MinLevel < 0 || MinLevel > 255)
                throw new Exception("Forbidden value on MinLevel = " + MinLevel + ", it doesn't respect the following condition : minLevel < 0 || minLevel > 255");
            Free = reader.ReadBoolean();
        }
        
    }
    
}