

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobDescription
    {
        public const short ProtocolId = 101;
        public virtual short TypeID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public Types.SkillActionDescription[] Skills { get; set; }
        
        public JobDescription()
        {
        }
        
        public JobDescription(sbyte jobId, Types.SkillActionDescription[] skills)
        {
            this.JobId = jobId;
            this.Skills = skills;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteUShort((ushort)Skills.Length);
            foreach (var entry in Skills)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            var limit = reader.ReadUShort();
            Skills = new Types.SkillActionDescription[limit];
            for (int i = 0; i < limit; i++)
            {
                 Skills[i] = Types.ProtocolTypeManager.GetInstance<Types.SkillActionDescription>(reader.ReadShort());
                 Skills[i].Deserialize(reader);
            }
        }
        
    }
    
}