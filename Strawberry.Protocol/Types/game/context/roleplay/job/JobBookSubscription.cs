

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobBookSubscription
    {
        public const short ProtocolId = 500;
        public virtual short TypeID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public bool Subscribed { get; set; }
        
        public JobBookSubscription()
        {
        }
        
        public JobBookSubscription(sbyte jobId, bool subscribed)
        {
            this.JobId = jobId;
            this.Subscribed = subscribed;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteBoolean(Subscribed);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            Subscribed = reader.ReadBoolean();
        }
        
    }
    
}