

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobCrafterDirectoryListEntry
    {
        public const short ProtocolId = 196;
        public virtual short TypeID => ProtocolId;
        
        public Types.JobCrafterDirectoryEntryPlayerInfo PlayerInfo { get; set; }
        public Types.JobCrafterDirectoryEntryJobInfo JobInfo { get; set; }
        
        public JobCrafterDirectoryListEntry()
        {
        }
        
        public JobCrafterDirectoryListEntry(Types.JobCrafterDirectoryEntryPlayerInfo playerInfo, Types.JobCrafterDirectoryEntryJobInfo jobInfo)
        {
            this.PlayerInfo = playerInfo;
            this.JobInfo = jobInfo;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            PlayerInfo.Serialize(writer);
            JobInfo.Serialize(writer);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            PlayerInfo = new Types.JobCrafterDirectoryEntryPlayerInfo();
            PlayerInfo.Deserialize(reader);
            JobInfo = new Types.JobCrafterDirectoryEntryJobInfo();
            JobInfo.Deserialize(reader);
        }
        
    }
    
}