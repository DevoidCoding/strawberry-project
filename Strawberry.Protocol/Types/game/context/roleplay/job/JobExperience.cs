

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobExperience
    {
        public const short ProtocolId = 98;
        public virtual short TypeID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public byte JobLevel { get; set; }
        public ulong JobXP { get; set; }
        public ulong JobXpLevelFloor { get; set; }
        public ulong JobXpNextLevelFloor { get; set; }
        
        public JobExperience()
        {
        }
        
        public JobExperience(sbyte jobId, byte jobLevel, ulong jobXP, ulong jobXpLevelFloor, ulong jobXpNextLevelFloor)
        {
            this.JobId = jobId;
            this.JobLevel = jobLevel;
            this.JobXP = jobXP;
            this.JobXpLevelFloor = jobXpLevelFloor;
            this.JobXpNextLevelFloor = jobXpNextLevelFloor;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteByte(JobLevel);
            writer.WriteVarLong(JobXP);
            writer.WriteVarLong(JobXpLevelFloor);
            writer.WriteVarLong(JobXpNextLevelFloor);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            JobLevel = reader.ReadByte();
            if (JobLevel < 0 || JobLevel > 255)
                throw new Exception("Forbidden value on JobLevel = " + JobLevel + ", it doesn't respect the following condition : jobLevel < 0 || jobLevel > 255");
            JobXP = reader.ReadVarUhLong();
            if (JobXP < 0 || JobXP > 9007199254740990)
                throw new Exception("Forbidden value on JobXP = " + JobXP + ", it doesn't respect the following condition : jobXP < 0 || jobXP > 9007199254740990");
            JobXpLevelFloor = reader.ReadVarUhLong();
            if (JobXpLevelFloor < 0 || JobXpLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on JobXpLevelFloor = " + JobXpLevelFloor + ", it doesn't respect the following condition : jobXpLevelFloor < 0 || jobXpLevelFloor > 9007199254740990");
            JobXpNextLevelFloor = reader.ReadVarUhLong();
            if (JobXpNextLevelFloor < 0 || JobXpNextLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on JobXpNextLevelFloor = " + JobXpNextLevelFloor + ", it doesn't respect the following condition : jobXpNextLevelFloor < 0 || jobXpNextLevelFloor > 9007199254740990");
        }
        
    }
    
}