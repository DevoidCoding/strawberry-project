

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class JobCrafterDirectoryEntryJobInfo
    {
        public const short ProtocolId = 195;
        public virtual short TypeID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public byte JobLevel { get; set; }
        public bool Free { get; set; }
        public byte MinLevel { get; set; }
        
        public JobCrafterDirectoryEntryJobInfo()
        {
        }
        
        public JobCrafterDirectoryEntryJobInfo(sbyte jobId, byte jobLevel, bool free, byte minLevel)
        {
            this.JobId = jobId;
            this.JobLevel = jobLevel;
            this.Free = free;
            this.MinLevel = minLevel;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteByte(JobLevel);
            writer.WriteBoolean(Free);
            writer.WriteByte(MinLevel);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            JobLevel = reader.ReadByte();
            if (JobLevel < 1 || JobLevel > 200)
                throw new Exception("Forbidden value on JobLevel = " + JobLevel + ", it doesn't respect the following condition : jobLevel < 1 || jobLevel > 200");
            Free = reader.ReadBoolean();
            MinLevel = reader.ReadByte();
            if (MinLevel < 0 || MinLevel > 255)
                throw new Exception("Forbidden value on MinLevel = " + MinLevel + ", it doesn't respect the following condition : minLevel < 0 || minLevel > 255");
        }
        
    }
    
}