

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionGuild : HumanOption
    {
        public new const short ProtocolId = 409;
        public override short TypeID => ProtocolId;
        
        public Types.GuildInformations GuildInformations { get; set; }
        
        public HumanOptionGuild()
        {
        }
        
        public HumanOptionGuild(Types.GuildInformations guildInformations)
        {
            this.GuildInformations = guildInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            GuildInformations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            GuildInformations = new Types.GuildInformations();
            GuildInformations.Deserialize(reader);
        }
        
    }
    
}