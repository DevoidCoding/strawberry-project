

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionTitle : HumanOption
    {
        public new const short ProtocolId = 408;
        public override short TypeID => ProtocolId;
        
        public ushort TitleId { get; set; }
        public string TitleParam { get; set; }
        
        public HumanOptionTitle()
        {
        }
        
        public HumanOptionTitle(ushort titleId, string titleParam)
        {
            this.TitleId = titleId;
            this.TitleParam = titleParam;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(TitleId);
            writer.WriteUTF(TitleParam);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TitleId = reader.ReadVarUhShort();
            if (TitleId < 0)
                throw new Exception("Forbidden value on TitleId = " + TitleId + ", it doesn't respect the following condition : titleId < 0");
            TitleParam = reader.ReadUTF();
        }
        
    }
    
}