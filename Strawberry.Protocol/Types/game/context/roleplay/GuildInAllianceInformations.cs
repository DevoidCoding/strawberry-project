

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildInAllianceInformations : GuildInformations
    {
        public new const short ProtocolId = 420;
        public override short TypeID => ProtocolId;
        
        public byte NbMembers { get; set; }
        
        public GuildInAllianceInformations()
        {
        }
        
        public GuildInAllianceInformations(uint guildId, string guildName, byte guildLevel, Types.GuildEmblem guildEmblem, byte nbMembers)
         : base(guildId, guildName, guildLevel, guildEmblem)
        {
            this.NbMembers = nbMembers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(NbMembers);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            NbMembers = reader.ReadByte();
            if (NbMembers < 1 || NbMembers > 240)
                throw new Exception("Forbidden value on NbMembers = " + NbMembers + ", it doesn't respect the following condition : nbMembers < 1 || nbMembers > 240");
        }
        
    }
    
}