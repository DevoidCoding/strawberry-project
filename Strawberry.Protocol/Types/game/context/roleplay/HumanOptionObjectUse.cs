

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionObjectUse : HumanOption
    {
        public new const short ProtocolId = 449;
        public override short TypeID => ProtocolId;
        
        public sbyte DelayTypeId { get; set; }
        public double DelayEndTime { get; set; }
        public ushort ObjectGID { get; set; }
        
        public HumanOptionObjectUse()
        {
        }
        
        public HumanOptionObjectUse(sbyte delayTypeId, double delayEndTime, ushort objectGID)
        {
            this.DelayTypeId = delayTypeId;
            this.DelayEndTime = delayEndTime;
            this.ObjectGID = objectGID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(DelayTypeId);
            writer.WriteDouble(DelayEndTime);
            writer.WriteVarShort(ObjectGID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            DelayTypeId = reader.ReadSByte();
            if (DelayTypeId < 0)
                throw new Exception("Forbidden value on DelayTypeId = " + DelayTypeId + ", it doesn't respect the following condition : delayTypeId < 0");
            DelayEndTime = reader.ReadDouble();
            if (DelayEndTime < 0 || DelayEndTime > 9007199254740990)
                throw new Exception("Forbidden value on DelayEndTime = " + DelayEndTime + ", it doesn't respect the following condition : delayEndTime < 0 || delayEndTime > 9007199254740990");
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
        }
        
    }
    
}