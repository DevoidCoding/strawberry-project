

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MonsterBoosts
    {
        public const short ProtocolId = 497;
        public virtual short TypeID => ProtocolId;
        
        public uint Id { get; set; }
        public ushort XpBoost { get; set; }
        public ushort DropBoost { get; set; }
        
        public MonsterBoosts()
        {
        }
        
        public MonsterBoosts(uint id, ushort xpBoost, ushort dropBoost)
        {
            this.Id = id;
            this.XpBoost = xpBoost;
            this.DropBoost = dropBoost;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteVarShort(XpBoost);
            writer.WriteVarShort(DropBoost);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhInt();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            XpBoost = reader.ReadVarUhShort();
            if (XpBoost < 0)
                throw new Exception("Forbidden value on XpBoost = " + XpBoost + ", it doesn't respect the following condition : xpBoost < 0");
            DropBoost = reader.ReadVarUhShort();
            if (DropBoost < 0)
                throw new Exception("Forbidden value on DropBoost = " + DropBoost + ", it doesn't respect the following condition : dropBoost < 0");
        }
        
    }
    
}