

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanOptionSkillUse : HumanOption
    {
        public new const short ProtocolId = 495;
        public override short TypeID => ProtocolId;
        
        public uint ElementId { get; set; }
        public ushort SkillId { get; set; }
        public double SkillEndTime { get; set; }
        
        public HumanOptionSkillUse()
        {
        }
        
        public HumanOptionSkillUse(uint elementId, ushort skillId, double skillEndTime)
        {
            this.ElementId = elementId;
            this.SkillId = skillId;
            this.SkillEndTime = skillEndTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(ElementId);
            writer.WriteVarShort(SkillId);
            writer.WriteDouble(SkillEndTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ElementId = reader.ReadVarUhInt();
            if (ElementId < 0)
                throw new Exception("Forbidden value on ElementId = " + ElementId + ", it doesn't respect the following condition : elementId < 0");
            SkillId = reader.ReadVarUhShort();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
            SkillEndTime = reader.ReadDouble();
            if (SkillEndTime < -9007199254740990 || SkillEndTime > 9007199254740990)
                throw new Exception("Forbidden value on SkillEndTime = " + SkillEndTime + ", it doesn't respect the following condition : skillEndTime < -9007199254740990 || skillEndTime > 9007199254740990");
        }
        
    }
    
}