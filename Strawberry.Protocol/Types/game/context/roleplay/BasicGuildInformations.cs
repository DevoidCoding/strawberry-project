

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class BasicGuildInformations : AbstractSocialGroupInfos
    {
        public new const short ProtocolId = 365;
        public override short TypeID => ProtocolId;
        
        public uint GuildId { get; set; }
        public string GuildName { get; set; }
        public byte GuildLevel { get; set; }
        
        public BasicGuildInformations()
        {
        }
        
        public BasicGuildInformations(uint guildId, string guildName, byte guildLevel)
        {
            this.GuildId = guildId;
            this.GuildName = guildName;
            this.GuildLevel = guildLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(GuildId);
            writer.WriteUTF(GuildName);
            writer.WriteByte(GuildLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
            GuildName = reader.ReadUTF();
            GuildLevel = reader.ReadByte();
            if (GuildLevel < 0 || GuildLevel > 200)
                throw new Exception("Forbidden value on GuildLevel = " + GuildLevel + ", it doesn't respect the following condition : guildLevel < 0 || guildLevel > 200");
        }
        
    }
    
}