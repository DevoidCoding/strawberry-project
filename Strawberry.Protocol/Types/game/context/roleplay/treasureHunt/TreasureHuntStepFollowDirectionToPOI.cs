

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TreasureHuntStepFollowDirectionToPOI : TreasureHuntStep
    {
        public new const short ProtocolId = 461;
        public override short TypeID => ProtocolId;
        
        public sbyte Direction { get; set; }
        public ushort PoiLabelId { get; set; }
        
        public TreasureHuntStepFollowDirectionToPOI()
        {
        }
        
        public TreasureHuntStepFollowDirectionToPOI(sbyte direction, ushort poiLabelId)
        {
            this.Direction = direction;
            this.PoiLabelId = poiLabelId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Direction);
            writer.WriteVarShort(PoiLabelId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
            PoiLabelId = reader.ReadVarUhShort();
            if (PoiLabelId < 0)
                throw new Exception("Forbidden value on PoiLabelId = " + PoiLabelId + ", it doesn't respect the following condition : poiLabelId < 0");
        }
        
    }
    
}