

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TreasureHuntStep
    {
        public const short ProtocolId = 463;
        public virtual short TypeID => ProtocolId;
        
        
        public TreasureHuntStep()
        {
        }
        
        
        public virtual void Serialize(IDataWriter writer)
        {
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
        }
        
    }
    
}