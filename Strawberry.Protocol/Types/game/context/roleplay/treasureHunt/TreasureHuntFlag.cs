

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TreasureHuntFlag
    {
        public const short ProtocolId = 473;
        public virtual short TypeID => ProtocolId;
        
        public double MapId { get; set; }
        public sbyte State { get; set; }
        
        public TreasureHuntFlag()
        {
        }
        
        public TreasureHuntFlag(double mapId, sbyte state)
        {
            this.MapId = mapId;
            this.State = state;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteSByte(State);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
        }
        
    }
    
}