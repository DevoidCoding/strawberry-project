

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TreasureHuntStepFollowDirectionToHint : TreasureHuntStep
    {
        public new const short ProtocolId = 472;
        public override short TypeID => ProtocolId;
        
        public sbyte Direction { get; set; }
        public ushort NpcId { get; set; }
        
        public TreasureHuntStepFollowDirectionToHint()
        {
        }
        
        public TreasureHuntStepFollowDirectionToHint(sbyte direction, ushort npcId)
        {
            this.Direction = direction;
            this.NpcId = npcId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Direction);
            writer.WriteVarShort(NpcId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
            NpcId = reader.ReadVarUhShort();
            if (NpcId < 0)
                throw new Exception("Forbidden value on NpcId = " + NpcId + ", it doesn't respect the following condition : npcId < 0");
        }
        
    }
    
}