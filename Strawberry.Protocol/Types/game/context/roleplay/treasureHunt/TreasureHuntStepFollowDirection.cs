

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TreasureHuntStepFollowDirection : TreasureHuntStep
    {
        public new const short ProtocolId = 468;
        public override short TypeID => ProtocolId;
        
        public sbyte Direction { get; set; }
        public ushort MapCount { get; set; }
        
        public TreasureHuntStepFollowDirection()
        {
        }
        
        public TreasureHuntStepFollowDirection(sbyte direction, ushort mapCount)
        {
            this.Direction = direction;
            this.MapCount = mapCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Direction);
            writer.WriteVarShort(MapCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
            MapCount = reader.ReadVarUhShort();
            if (MapCount < 0)
                throw new Exception("Forbidden value on MapCount = " + MapCount + ", it doesn't respect the following condition : mapCount < 0");
        }
        
    }
    
}