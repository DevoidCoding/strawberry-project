

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AtlasPointsInformations
    {
        public const short ProtocolId = 175;
        public virtual short TypeID => ProtocolId;
        
        public sbyte Type { get; set; }
        public Types.MapCoordinatesExtended[] Coords { get; set; }
        
        public AtlasPointsInformations()
        {
        }
        
        public AtlasPointsInformations(sbyte type, Types.MapCoordinatesExtended[] coords)
        {
            this.Type = type;
            this.Coords = coords;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
            writer.WriteUShort((ushort)Coords.Length);
            foreach (var entry in Coords)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            var limit = reader.ReadUShort();
            Coords = new Types.MapCoordinatesExtended[limit];
            for (int i = 0; i < limit; i++)
            {
                 Coords[i] = new Types.MapCoordinatesExtended();
                 Coords[i].Deserialize(reader);
            }
        }
        
    }
    
}