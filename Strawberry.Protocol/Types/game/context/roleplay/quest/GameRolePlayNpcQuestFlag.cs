

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayNpcQuestFlag
    {
        public const short ProtocolId = 384;
        public virtual short TypeID => ProtocolId;
        
        public ushort[] QuestsToValidId { get; set; }
        public ushort[] QuestsToStartId { get; set; }
        
        public GameRolePlayNpcQuestFlag()
        {
        }
        
        public GameRolePlayNpcQuestFlag(ushort[] questsToValidId, ushort[] questsToStartId)
        {
            this.QuestsToValidId = questsToValidId;
            this.QuestsToStartId = questsToStartId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)QuestsToValidId.Length);
            foreach (var entry in QuestsToValidId)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)QuestsToStartId.Length);
            foreach (var entry in QuestsToStartId)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            QuestsToValidId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 QuestsToValidId[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            QuestsToStartId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 QuestsToStartId[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}