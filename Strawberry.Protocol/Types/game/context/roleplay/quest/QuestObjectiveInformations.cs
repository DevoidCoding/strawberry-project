

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class QuestObjectiveInformations
    {
        public const short ProtocolId = 385;
        public virtual short TypeID => ProtocolId;
        
        public ushort ObjectiveId { get; set; }
        public bool ObjectiveStatus { get; set; }
        public string[] DialogParams { get; set; }
        
        public QuestObjectiveInformations()
        {
        }
        
        public QuestObjectiveInformations(ushort objectiveId, bool objectiveStatus, string[] dialogParams)
        {
            this.ObjectiveId = objectiveId;
            this.ObjectiveStatus = objectiveStatus;
            this.DialogParams = dialogParams;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ObjectiveId);
            writer.WriteBoolean(ObjectiveStatus);
            writer.WriteUShort((ushort)DialogParams.Length);
            foreach (var entry in DialogParams)
            {
                 writer.WriteUTF(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ObjectiveId = reader.ReadVarUhShort();
            if (ObjectiveId < 0)
                throw new Exception("Forbidden value on ObjectiveId = " + ObjectiveId + ", it doesn't respect the following condition : objectiveId < 0");
            ObjectiveStatus = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            DialogParams = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 DialogParams[i] = reader.ReadUTF();
            }
        }
        
    }
    
}