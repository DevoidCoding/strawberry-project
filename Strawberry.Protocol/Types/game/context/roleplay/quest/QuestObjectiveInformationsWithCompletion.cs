

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class QuestObjectiveInformationsWithCompletion : QuestObjectiveInformations
    {
        public new const short ProtocolId = 386;
        public override short TypeID => ProtocolId;
        
        public ushort CurCompletion { get; set; }
        public ushort MaxCompletion { get; set; }
        
        public QuestObjectiveInformationsWithCompletion()
        {
        }
        
        public QuestObjectiveInformationsWithCompletion(ushort objectiveId, bool objectiveStatus, string[] dialogParams, ushort curCompletion, ushort maxCompletion)
         : base(objectiveId, objectiveStatus, dialogParams)
        {
            this.CurCompletion = curCompletion;
            this.MaxCompletion = maxCompletion;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(CurCompletion);
            writer.WriteVarShort(MaxCompletion);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CurCompletion = reader.ReadVarUhShort();
            if (CurCompletion < 0)
                throw new Exception("Forbidden value on CurCompletion = " + CurCompletion + ", it doesn't respect the following condition : curCompletion < 0");
            MaxCompletion = reader.ReadVarUhShort();
            if (MaxCompletion < 0)
                throw new Exception("Forbidden value on MaxCompletion = " + MaxCompletion + ", it doesn't respect the following condition : maxCompletion < 0");
        }
        
    }
    
}