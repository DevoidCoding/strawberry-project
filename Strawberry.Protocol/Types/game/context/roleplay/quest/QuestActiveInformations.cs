

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class QuestActiveInformations
    {
        public const short ProtocolId = 381;
        public virtual short TypeID => ProtocolId;
        
        public ushort QuestId { get; set; }
        
        public QuestActiveInformations()
        {
        }
        
        public QuestActiveInformations(ushort questId)
        {
            this.QuestId = questId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(QuestId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            QuestId = reader.ReadVarUhShort();
            if (QuestId < 0)
                throw new Exception("Forbidden value on QuestId = " + QuestId + ", it doesn't respect the following condition : questId < 0");
        }
        
    }
    
}