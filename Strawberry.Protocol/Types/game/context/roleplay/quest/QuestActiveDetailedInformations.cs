

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class QuestActiveDetailedInformations : QuestActiveInformations
    {
        public new const short ProtocolId = 382;
        public override short TypeID => ProtocolId;
        
        public ushort StepId { get; set; }
        public Types.QuestObjectiveInformations[] Objectives { get; set; }
        
        public QuestActiveDetailedInformations()
        {
        }
        
        public QuestActiveDetailedInformations(ushort questId, ushort stepId, Types.QuestObjectiveInformations[] objectives)
         : base(questId)
        {
            this.StepId = stepId;
            this.Objectives = objectives;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(StepId);
            writer.WriteUShort((ushort)Objectives.Length);
            foreach (var entry in Objectives)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            StepId = reader.ReadVarUhShort();
            if (StepId < 0)
                throw new Exception("Forbidden value on StepId = " + StepId + ", it doesn't respect the following condition : stepId < 0");
            var limit = reader.ReadUShort();
            Objectives = new Types.QuestObjectiveInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Objectives[i] = Types.ProtocolTypeManager.GetInstance<Types.QuestObjectiveInformations>(reader.ReadShort());
                 Objectives[i].Deserialize(reader);
            }
        }
        
    }
    
}