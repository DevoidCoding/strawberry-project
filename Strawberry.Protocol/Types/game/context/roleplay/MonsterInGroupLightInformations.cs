

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MonsterInGroupLightInformations
    {
        public const short ProtocolId = 395;
        public virtual short TypeID => ProtocolId;
        
        public int CreatureGenericId { get; set; }
        public sbyte Grade { get; set; }
        
        public MonsterInGroupLightInformations()
        {
        }
        
        public MonsterInGroupLightInformations(int creatureGenericId, sbyte grade)
        {
            this.CreatureGenericId = creatureGenericId;
            this.Grade = grade;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(CreatureGenericId);
            writer.WriteSByte(Grade);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CreatureGenericId = reader.ReadInt();
            Grade = reader.ReadSByte();
            if (Grade < 0)
                throw new Exception("Forbidden value on Grade = " + Grade + ", it doesn't respect the following condition : grade < 0");
        }
        
    }
    
}