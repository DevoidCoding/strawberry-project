

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ObjectItemInRolePlay
    {
        public const short ProtocolId = 198;
        public virtual short TypeID => ProtocolId;
        
        public ushort CellId { get; set; }
        public ushort ObjectGID { get; set; }
        
        public ObjectItemInRolePlay()
        {
        }
        
        public ObjectItemInRolePlay(ushort cellId, ushort objectGID)
        {
            this.CellId = cellId;
            this.ObjectGID = objectGID;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteVarShort(ObjectGID);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CellId = reader.ReadVarUhShort();
            if (CellId < 0 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0 || cellId > 559");
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
        }
        
    }
    
}