

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayMutantInformations : GameRolePlayHumanoidInformations
    {
        public new const short ProtocolId = 3;
        public override short TypeID => ProtocolId;
        
        public ushort MonsterId { get; set; }
        public sbyte PowerLevel { get; set; }
        
        public GameRolePlayMutantInformations()
        {
        }
        
        public GameRolePlayMutantInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, string name, Types.HumanInformations humanoidInfo, int accountId, ushort monsterId, sbyte powerLevel)
         : base(contextualId, look, disposition, name, humanoidInfo, accountId)
        {
            this.MonsterId = monsterId;
            this.PowerLevel = powerLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MonsterId);
            writer.WriteSByte(PowerLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MonsterId = reader.ReadVarUhShort();
            if (MonsterId < 0)
                throw new Exception("Forbidden value on MonsterId = " + MonsterId + ", it doesn't respect the following condition : monsterId < 0");
            PowerLevel = reader.ReadSByte();
        }
        
    }
    
}