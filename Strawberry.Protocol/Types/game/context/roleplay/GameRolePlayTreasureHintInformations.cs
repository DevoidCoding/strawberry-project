

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayTreasureHintInformations : GameRolePlayActorInformations
    {
        public new const short ProtocolId = 471;
        public override short TypeID => ProtocolId;
        
        public ushort NpcId { get; set; }
        
        public GameRolePlayTreasureHintInformations()
        {
        }
        
        public GameRolePlayTreasureHintInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, ushort npcId)
         : base(contextualId, look, disposition)
        {
            this.NpcId = npcId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(NpcId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            NpcId = reader.ReadVarUhShort();
            if (NpcId < 0)
                throw new Exception("Forbidden value on NpcId = " + NpcId + ", it doesn't respect the following condition : npcId < 0");
        }
        
    }
    
}