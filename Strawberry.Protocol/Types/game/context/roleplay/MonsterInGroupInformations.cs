

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MonsterInGroupInformations : MonsterInGroupLightInformations
    {
        public new const short ProtocolId = 144;
        public override short TypeID => ProtocolId;
        
        public Types.EntityLook Look { get; set; }
        
        public MonsterInGroupInformations()
        {
        }
        
        public MonsterInGroupInformations(int creatureGenericId, sbyte grade, Types.EntityLook look)
         : base(creatureGenericId, grade)
        {
            this.Look = look;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Look.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
        }
        
    }
    
}