

// Generated on 02/12/2018 03:56:54
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HumanInformations
    {
        public const short ProtocolId = 157;
        public virtual short TypeID => ProtocolId;
        
        public Types.ActorRestrictionsInformations Restrictions { get; set; }
        public bool Sex { get; set; }
        public Types.HumanOption[] Options { get; set; }
        
        public HumanInformations()
        {
        }
        
        public HumanInformations(Types.ActorRestrictionsInformations restrictions, bool sex, Types.HumanOption[] options)
        {
            this.Restrictions = restrictions;
            this.Sex = sex;
            this.Options = options;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            Restrictions.Serialize(writer);
            writer.WriteBoolean(Sex);
            writer.WriteUShort((ushort)Options.Length);
            foreach (var entry in Options)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Restrictions = new Types.ActorRestrictionsInformations();
            Restrictions.Deserialize(reader);
            Sex = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            Options = new Types.HumanOption[limit];
            for (int i = 0; i < limit; i++)
            {
                 Options[i] = Types.ProtocolTypeManager.GetInstance<Types.HumanOption>(reader.ReadShort());
                 Options[i].Deserialize(reader);
            }
        }
        
    }
    
}