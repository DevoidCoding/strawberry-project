

// Generated on 02/12/2018 03:56:53
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GameRolePlayMountInformations : GameRolePlayNamedActorInformations
    {
        public new const short ProtocolId = 180;
        public override short TypeID => ProtocolId;
        
        public string OwnerName { get; set; }
        public byte Level { get; set; }
        
        public GameRolePlayMountInformations()
        {
        }
        
        public GameRolePlayMountInformations(double contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, string name, string ownerName, byte level)
         : base(contextualId, look, disposition, name)
        {
            this.OwnerName = ownerName;
            this.Level = level;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(OwnerName);
            writer.WriteByte(Level);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            OwnerName = reader.ReadUTF();
            Level = reader.ReadByte();
            if (Level < 0 || Level > 255)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0 || level > 255");
        }
        
    }
    
}