

// Generated on 02/12/2018 03:56:51
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class EntityMovementInformations
    {
        public const short ProtocolId = 63;
        public virtual short TypeID => ProtocolId;
        
        public int Id { get; set; }
        public sbyte[] Steps { get; set; }
        
        public EntityMovementInformations()
        {
        }
        
        public EntityMovementInformations(int id, sbyte[] steps)
        {
            this.Id = id;
            this.Steps = steps;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Id);
            writer.WriteUShort((ushort)Steps.Length);
            foreach (var entry in Steps)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadInt();
            var limit = reader.ReadUShort();
            Steps = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Steps[i] = reader.ReadSByte();
            }
        }
        
    }
    
}