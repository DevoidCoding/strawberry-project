

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseInformationsForSell
    {
        public const short ProtocolId = 221;
        public virtual short TypeID => ProtocolId;
        
        public int InstanceId { get; set; }
        public bool SecondHand { get; set; }
        public uint ModelId { get; set; }
        public string OwnerName { get; set; }
        public bool OwnerConnected { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public ushort SubAreaId { get; set; }
        public sbyte NbRoom { get; set; }
        public sbyte NbChest { get; set; }
        public int[] SkillListIds { get; set; }
        public bool IsLocked { get; set; }
        public ulong Price { get; set; }
        
        public HouseInformationsForSell()
        {
        }
        
        public HouseInformationsForSell(int instanceId, bool secondHand, uint modelId, string ownerName, bool ownerConnected, short worldX, short worldY, ushort subAreaId, sbyte nbRoom, sbyte nbChest, int[] skillListIds, bool isLocked, ulong price)
        {
            this.InstanceId = instanceId;
            this.SecondHand = secondHand;
            this.ModelId = modelId;
            this.OwnerName = ownerName;
            this.OwnerConnected = ownerConnected;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.SubAreaId = subAreaId;
            this.NbRoom = nbRoom;
            this.NbChest = nbChest;
            this.SkillListIds = skillListIds;
            this.IsLocked = isLocked;
            this.Price = price;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteVarInt(ModelId);
            writer.WriteUTF(OwnerName);
            writer.WriteBoolean(OwnerConnected);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteSByte(NbRoom);
            writer.WriteSByte(NbChest);
            writer.WriteUShort((ushort)SkillListIds.Length);
            foreach (var entry in SkillListIds)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteBoolean(IsLocked);
            writer.WriteVarLong(Price);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            SecondHand = reader.ReadBoolean();
            ModelId = reader.ReadVarUhInt();
            if (ModelId < 0)
                throw new Exception("Forbidden value on ModelId = " + ModelId + ", it doesn't respect the following condition : modelId < 0");
            OwnerName = reader.ReadUTF();
            OwnerConnected = reader.ReadBoolean();
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            NbRoom = reader.ReadSByte();
            NbChest = reader.ReadSByte();
            var limit = reader.ReadUShort();
            SkillListIds = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 SkillListIds[i] = reader.ReadInt();
            }
            IsLocked = reader.ReadBoolean();
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}