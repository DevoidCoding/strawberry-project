

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseInformationsForGuild : HouseInformations
    {
        public new const short ProtocolId = 170;
        public override short TypeID => ProtocolId;
        
        public int InstanceId { get; set; }
        public bool SecondHand { get; set; }
        public string OwnerName { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        public int[] SkillListIds { get; set; }
        public uint GuildshareParams { get; set; }
        
        public HouseInformationsForGuild()
        {
        }
        
        public HouseInformationsForGuild(uint houseId, ushort modelId, int instanceId, bool secondHand, string ownerName, short worldX, short worldY, double mapId, ushort subAreaId, int[] skillListIds, uint guildshareParams)
         : base(houseId, modelId)
        {
            this.InstanceId = instanceId;
            this.SecondHand = secondHand;
            this.OwnerName = ownerName;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
            this.SkillListIds = skillListIds;
            this.GuildshareParams = guildshareParams;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteUTF(OwnerName);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteUShort((ushort)SkillListIds.Length);
            foreach (var entry in SkillListIds)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteVarInt(GuildshareParams);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            SecondHand = reader.ReadBoolean();
            OwnerName = reader.ReadUTF();
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            var limit = reader.ReadUShort();
            SkillListIds = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 SkillListIds[i] = reader.ReadInt();
            }
            GuildshareParams = reader.ReadVarUhInt();
            if (GuildshareParams < 0)
                throw new Exception("Forbidden value on GuildshareParams = " + GuildshareParams + ", it doesn't respect the following condition : guildshareParams < 0");
        }
        
    }
    
}