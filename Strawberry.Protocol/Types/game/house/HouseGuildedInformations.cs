

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseGuildedInformations : HouseInstanceInformations
    {
        public new const short ProtocolId = 512;
        public override short TypeID => ProtocolId;
        
        public Types.GuildInformations GuildInfo { get; set; }
        
        public HouseGuildedInformations()
        {
        }
        
        public HouseGuildedInformations(bool secondHand, bool isLocked, bool isSaleLocked, int instanceId, string ownerName, long price, Types.GuildInformations guildInfo)
         : base(secondHand, isLocked, isSaleLocked, instanceId, ownerName, price)
        {
            this.GuildInfo = guildInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            GuildInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
        }
        
    }
    
}