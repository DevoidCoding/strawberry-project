

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseInformations
    {
        public const short ProtocolId = 111;
        public virtual short TypeID => ProtocolId;
        
        public uint HouseId { get; set; }
        public ushort ModelId { get; set; }
        
        public HouseInformations()
        {
        }
        
        public HouseInformations(uint houseId, ushort modelId)
        {
            this.HouseId = houseId;
            this.ModelId = modelId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteVarShort(ModelId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            ModelId = reader.ReadVarUhShort();
            if (ModelId < 0)
                throw new Exception("Forbidden value on ModelId = " + ModelId + ", it doesn't respect the following condition : modelId < 0");
        }
        
    }
    
}