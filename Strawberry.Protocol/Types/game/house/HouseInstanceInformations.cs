

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseInstanceInformations
    {
        public const short ProtocolId = 511;
        public virtual short TypeID => ProtocolId;
        
        public bool SecondHand { get; set; }
        public bool IsLocked { get; set; }
        public bool IsSaleLocked { get; set; }
        public int InstanceId { get; set; }
        public string OwnerName { get; set; }
        public long Price { get; set; }
        
        public HouseInstanceInformations()
        {
        }
        
        public HouseInstanceInformations(bool secondHand, bool isLocked, bool isSaleLocked, int instanceId, string ownerName, long price)
        {
            this.SecondHand = secondHand;
            this.IsLocked = isLocked;
            this.IsSaleLocked = isSaleLocked;
            this.InstanceId = instanceId;
            this.OwnerName = ownerName;
            this.Price = price;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, SecondHand);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsLocked);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, IsSaleLocked);
            writer.WriteByte(flag1);
            writer.WriteInt(InstanceId);
            writer.WriteUTF(OwnerName);
            writer.WriteVarLong(Price);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            SecondHand = BooleanByteWrapper.GetFlag(flag1, 0);
            IsLocked = BooleanByteWrapper.GetFlag(flag1, 1);
            IsSaleLocked = BooleanByteWrapper.GetFlag(flag1, 2);
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            OwnerName = reader.ReadUTF();
            Price = reader.ReadVarLong();
            if (Price < -9007199254740990 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < -9007199254740990 || price > 9007199254740990");
        }
        
    }
    
}