

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AccountHouseInformations : HouseInformations
    {
        public new const short ProtocolId = 390;
        public override short TypeID => ProtocolId;
        
        public Types.HouseInstanceInformations HouseInfos { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        
        public AccountHouseInformations()
        {
        }
        
        public AccountHouseInformations(uint houseId, ushort modelId, Types.HouseInstanceInformations houseInfos, short worldX, short worldY, double mapId, ushort subAreaId)
         : base(houseId, modelId)
        {
            this.HouseInfos = houseInfos;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(HouseInfos.TypeID);
            HouseInfos.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            HouseInfos = Types.ProtocolTypeManager.GetInstance<Types.HouseInstanceInformations>(reader.ReadShort());
            HouseInfos.Deserialize(reader);
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
        }
        
    }
    
}