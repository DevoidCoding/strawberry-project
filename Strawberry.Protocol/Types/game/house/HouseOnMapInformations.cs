

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HouseOnMapInformations : HouseInformations
    {
        public new const short ProtocolId = 510;
        public override short TypeID => ProtocolId;
        
        public int[] DoorsOnMap { get; set; }
        public Types.HouseInstanceInformations[] HouseInstances { get; set; }
        
        public HouseOnMapInformations()
        {
        }
        
        public HouseOnMapInformations(uint houseId, ushort modelId, int[] doorsOnMap, Types.HouseInstanceInformations[] houseInstances)
         : base(houseId, modelId)
        {
            this.DoorsOnMap = doorsOnMap;
            this.HouseInstances = houseInstances;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)DoorsOnMap.Length);
            foreach (var entry in DoorsOnMap)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUShort((ushort)HouseInstances.Length);
            foreach (var entry in HouseInstances)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            DoorsOnMap = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 DoorsOnMap[i] = reader.ReadInt();
            }
            limit = reader.ReadUShort();
            HouseInstances = new Types.HouseInstanceInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 HouseInstances[i] = new Types.HouseInstanceInformations();
                 HouseInstances[i].Deserialize(reader);
            }
        }
        
    }
    
}