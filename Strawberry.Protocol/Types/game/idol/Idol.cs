

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class Idol
    {
        public const short ProtocolId = 489;
        public virtual short TypeID => ProtocolId;
        
        public ushort Id { get; set; }
        public ushort XpBonusPercent { get; set; }
        public ushort DropBonusPercent { get; set; }
        
        public Idol()
        {
        }
        
        public Idol(ushort id, ushort xpBonusPercent, ushort dropBonusPercent)
        {
            this.Id = id;
            this.XpBonusPercent = xpBonusPercent;
            this.DropBonusPercent = dropBonusPercent;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteVarShort(XpBonusPercent);
            writer.WriteVarShort(DropBonusPercent);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            XpBonusPercent = reader.ReadVarUhShort();
            if (XpBonusPercent < 0)
                throw new Exception("Forbidden value on XpBonusPercent = " + XpBonusPercent + ", it doesn't respect the following condition : xpBonusPercent < 0");
            DropBonusPercent = reader.ReadVarUhShort();
            if (DropBonusPercent < 0)
                throw new Exception("Forbidden value on DropBonusPercent = " + DropBonusPercent + ", it doesn't respect the following condition : dropBonusPercent < 0");
        }
        
    }
    
}