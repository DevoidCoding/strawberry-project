

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PartyIdol : Idol
    {
        public new const short ProtocolId = 490;
        public override short TypeID => ProtocolId;
        
        public ulong[] OwnersIds { get; set; }
        
        public PartyIdol()
        {
        }
        
        public PartyIdol(ushort id, ushort xpBonusPercent, ushort dropBonusPercent, ulong[] ownersIds)
         : base(id, xpBonusPercent, dropBonusPercent)
        {
            this.OwnersIds = ownersIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)OwnersIds.Length);
            foreach (var entry in OwnersIds)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            OwnersIds = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 OwnersIds[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}