

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PrismFightersInformation
    {
        public const short ProtocolId = 443;
        public virtual short TypeID => ProtocolId;
        
        public ushort SubAreaId { get; set; }
        public Types.ProtectedEntityWaitingForHelpInfo WaitingForHelpInfo { get; set; }
        public Types.CharacterMinimalPlusLookInformations[] AllyCharactersInformations { get; set; }
        public Types.CharacterMinimalPlusLookInformations[] EnemyCharactersInformations { get; set; }
        
        public PrismFightersInformation()
        {
        }
        
        public PrismFightersInformation(ushort subAreaId, Types.ProtectedEntityWaitingForHelpInfo waitingForHelpInfo, Types.CharacterMinimalPlusLookInformations[] allyCharactersInformations, Types.CharacterMinimalPlusLookInformations[] enemyCharactersInformations)
        {
            this.SubAreaId = subAreaId;
            this.WaitingForHelpInfo = waitingForHelpInfo;
            this.AllyCharactersInformations = allyCharactersInformations;
            this.EnemyCharactersInformations = enemyCharactersInformations;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            WaitingForHelpInfo.Serialize(writer);
            writer.WriteUShort((ushort)AllyCharactersInformations.Length);
            foreach (var entry in AllyCharactersInformations)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)EnemyCharactersInformations.Length);
            foreach (var entry in EnemyCharactersInformations)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            WaitingForHelpInfo = new Types.ProtectedEntityWaitingForHelpInfo();
            WaitingForHelpInfo.Deserialize(reader);
            var limit = reader.ReadUShort();
            AllyCharactersInformations = new Types.CharacterMinimalPlusLookInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllyCharactersInformations[i] = Types.ProtocolTypeManager.GetInstance<Types.CharacterMinimalPlusLookInformations>(reader.ReadShort());
                 AllyCharactersInformations[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            EnemyCharactersInformations = new Types.CharacterMinimalPlusLookInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 EnemyCharactersInformations[i] = Types.ProtocolTypeManager.GetInstance<Types.CharacterMinimalPlusLookInformations>(reader.ReadShort());
                 EnemyCharactersInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}