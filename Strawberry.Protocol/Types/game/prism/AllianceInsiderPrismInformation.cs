

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AllianceInsiderPrismInformation : PrismInformation
    {
        public new const short ProtocolId = 431;
        public override short TypeID => ProtocolId;
        
        public int LastTimeSlotModificationDate { get; set; }
        public uint LastTimeSlotModificationAuthorGuildId { get; set; }
        public ulong LastTimeSlotModificationAuthorId { get; set; }
        public string LastTimeSlotModificationAuthorName { get; set; }
        public Types.ObjectItem[] ModulesObjects { get; set; }
        
        public AllianceInsiderPrismInformation()
        {
        }
        
        public AllianceInsiderPrismInformation(sbyte typeId, sbyte state, int nextVulnerabilityDate, int placementDate, uint rewardTokenCount, int lastTimeSlotModificationDate, uint lastTimeSlotModificationAuthorGuildId, ulong lastTimeSlotModificationAuthorId, string lastTimeSlotModificationAuthorName, Types.ObjectItem[] modulesObjects)
         : base(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount)
        {
            this.LastTimeSlotModificationDate = lastTimeSlotModificationDate;
            this.LastTimeSlotModificationAuthorGuildId = lastTimeSlotModificationAuthorGuildId;
            this.LastTimeSlotModificationAuthorId = lastTimeSlotModificationAuthorId;
            this.LastTimeSlotModificationAuthorName = lastTimeSlotModificationAuthorName;
            this.ModulesObjects = modulesObjects;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(LastTimeSlotModificationDate);
            writer.WriteVarInt(LastTimeSlotModificationAuthorGuildId);
            writer.WriteVarLong(LastTimeSlotModificationAuthorId);
            writer.WriteUTF(LastTimeSlotModificationAuthorName);
            writer.WriteUShort((ushort)ModulesObjects.Length);
            foreach (var entry in ModulesObjects)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LastTimeSlotModificationDate = reader.ReadInt();
            if (LastTimeSlotModificationDate < 0)
                throw new Exception("Forbidden value on LastTimeSlotModificationDate = " + LastTimeSlotModificationDate + ", it doesn't respect the following condition : lastTimeSlotModificationDate < 0");
            LastTimeSlotModificationAuthorGuildId = reader.ReadVarUhInt();
            if (LastTimeSlotModificationAuthorGuildId < 0)
                throw new Exception("Forbidden value on LastTimeSlotModificationAuthorGuildId = " + LastTimeSlotModificationAuthorGuildId + ", it doesn't respect the following condition : lastTimeSlotModificationAuthorGuildId < 0");
            LastTimeSlotModificationAuthorId = reader.ReadVarUhLong();
            if (LastTimeSlotModificationAuthorId < 0 || LastTimeSlotModificationAuthorId > 9007199254740990)
                throw new Exception("Forbidden value on LastTimeSlotModificationAuthorId = " + LastTimeSlotModificationAuthorId + ", it doesn't respect the following condition : lastTimeSlotModificationAuthorId < 0 || lastTimeSlotModificationAuthorId > 9007199254740990");
            LastTimeSlotModificationAuthorName = reader.ReadUTF();
            var limit = reader.ReadUShort();
            ModulesObjects = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 ModulesObjects[i] = new Types.ObjectItem();
                 ModulesObjects[i].Deserialize(reader);
            }
        }
        
    }
    
}