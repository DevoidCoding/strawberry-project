

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PrismInformation
    {
        public const short ProtocolId = 428;
        public virtual short TypeID => ProtocolId;
        
        public sbyte TypeId { get; set; }
        public sbyte State { get; set; }
        public int NextVulnerabilityDate { get; set; }
        public int PlacementDate { get; set; }
        public uint RewardTokenCount { get; set; }
        
        public PrismInformation()
        {
        }
        
        public PrismInformation(sbyte typeId, sbyte state, int nextVulnerabilityDate, int placementDate, uint rewardTokenCount)
        {
            this.TypeId = typeId;
            this.State = state;
            this.NextVulnerabilityDate = nextVulnerabilityDate;
            this.PlacementDate = placementDate;
            this.RewardTokenCount = rewardTokenCount;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(TypeId);
            writer.WriteSByte(State);
            writer.WriteInt(NextVulnerabilityDate);
            writer.WriteInt(PlacementDate);
            writer.WriteVarInt(RewardTokenCount);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            TypeId = reader.ReadSByte();
            if (TypeId < 0)
                throw new Exception("Forbidden value on TypeId = " + TypeId + ", it doesn't respect the following condition : typeId < 0");
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
            NextVulnerabilityDate = reader.ReadInt();
            if (NextVulnerabilityDate < 0)
                throw new Exception("Forbidden value on NextVulnerabilityDate = " + NextVulnerabilityDate + ", it doesn't respect the following condition : nextVulnerabilityDate < 0");
            PlacementDate = reader.ReadInt();
            if (PlacementDate < 0)
                throw new Exception("Forbidden value on PlacementDate = " + PlacementDate + ", it doesn't respect the following condition : placementDate < 0");
            RewardTokenCount = reader.ReadVarUhInt();
            if (RewardTokenCount < 0)
                throw new Exception("Forbidden value on RewardTokenCount = " + RewardTokenCount + ", it doesn't respect the following condition : rewardTokenCount < 0");
        }
        
    }
    
}