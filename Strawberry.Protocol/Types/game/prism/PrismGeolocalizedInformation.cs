

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PrismGeolocalizedInformation : PrismSubareaEmptyInfo
    {
        public new const short ProtocolId = 434;
        public override short TypeID => ProtocolId;
        
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public Types.PrismInformation Prism { get; set; }
        
        public PrismGeolocalizedInformation()
        {
        }
        
        public PrismGeolocalizedInformation(ushort subAreaId, uint allianceId, short worldX, short worldY, double mapId, Types.PrismInformation prism)
         : base(subAreaId, allianceId)
        {
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.Prism = prism;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteShort(Prism.TypeID);
            Prism.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            Prism = Types.ProtocolTypeManager.GetInstance<Types.PrismInformation>(reader.ReadShort());
            Prism.Deserialize(reader);
        }
        
    }
    
}