

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockInformationsForSell
    {
        public const short ProtocolId = 222;
        public virtual short TypeID => ProtocolId;
        
        public string GuildOwner { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public ushort SubAreaId { get; set; }
        public sbyte NbMount { get; set; }
        public sbyte NbObject { get; set; }
        public ulong Price { get; set; }
        
        public PaddockInformationsForSell()
        {
        }
        
        public PaddockInformationsForSell(string guildOwner, short worldX, short worldY, ushort subAreaId, sbyte nbMount, sbyte nbObject, ulong price)
        {
            this.GuildOwner = guildOwner;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.SubAreaId = subAreaId;
            this.NbMount = nbMount;
            this.NbObject = nbObject;
            this.Price = price;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(GuildOwner);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteSByte(NbMount);
            writer.WriteSByte(NbObject);
            writer.WriteVarLong(Price);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            GuildOwner = reader.ReadUTF();
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            NbMount = reader.ReadSByte();
            NbObject = reader.ReadSByte();
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}