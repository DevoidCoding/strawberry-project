

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockContentInformations : PaddockInformations
    {
        public new const short ProtocolId = 183;
        public override short TypeID => ProtocolId;
        
        public double PaddockId { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        public bool Abandonned { get; set; }
        public Types.MountInformationsForPaddock[] MountsInformations { get; set; }
        
        public PaddockContentInformations()
        {
        }
        
        public PaddockContentInformations(ushort maxOutdoorMount, ushort maxItems, double paddockId, short worldX, short worldY, double mapId, ushort subAreaId, bool abandonned, Types.MountInformationsForPaddock[] mountsInformations)
         : base(maxOutdoorMount, maxItems)
        {
            this.PaddockId = paddockId;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
            this.Abandonned = abandonned;
            this.MountsInformations = mountsInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(PaddockId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteBoolean(Abandonned);
            writer.WriteUShort((ushort)MountsInformations.Length);
            foreach (var entry in MountsInformations)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PaddockId = reader.ReadDouble();
            if (PaddockId < 0 || PaddockId > 9007199254740990)
                throw new Exception("Forbidden value on PaddockId = " + PaddockId + ", it doesn't respect the following condition : paddockId < 0 || paddockId > 9007199254740990");
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            Abandonned = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            MountsInformations = new Types.MountInformationsForPaddock[limit];
            for (int i = 0; i < limit; i++)
            {
                 MountsInformations[i] = new Types.MountInformationsForPaddock();
                 MountsInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}