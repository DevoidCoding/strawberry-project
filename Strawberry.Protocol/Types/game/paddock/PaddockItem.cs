

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockItem : ObjectItemInRolePlay
    {
        public new const short ProtocolId = 185;
        public override short TypeID => ProtocolId;
        
        public Types.ItemDurability Durability { get; set; }
        
        public PaddockItem()
        {
        }
        
        public PaddockItem(ushort cellId, ushort objectGID, Types.ItemDurability durability)
         : base(cellId, objectGID)
        {
            this.Durability = durability;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Durability.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Durability = new Types.ItemDurability();
            Durability.Deserialize(reader);
        }
        
    }
    
}