

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockInformations
    {
        public const short ProtocolId = 132;
        public virtual short TypeID => ProtocolId;
        
        public ushort MaxOutdoorMount { get; set; }
        public ushort MaxItems { get; set; }
        
        public PaddockInformations()
        {
        }
        
        public PaddockInformations(ushort maxOutdoorMount, ushort maxItems)
        {
            this.MaxOutdoorMount = maxOutdoorMount;
            this.MaxItems = maxItems;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(MaxOutdoorMount);
            writer.WriteVarShort(MaxItems);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            MaxOutdoorMount = reader.ReadVarUhShort();
            if (MaxOutdoorMount < 0)
                throw new Exception("Forbidden value on MaxOutdoorMount = " + MaxOutdoorMount + ", it doesn't respect the following condition : maxOutdoorMount < 0");
            MaxItems = reader.ReadVarUhShort();
            if (MaxItems < 0)
                throw new Exception("Forbidden value on MaxItems = " + MaxItems + ", it doesn't respect the following condition : maxItems < 0");
        }
        
    }
    
}