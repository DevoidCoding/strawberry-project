

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockGuildedInformations : PaddockBuyableInformations
    {
        public new const short ProtocolId = 508;
        public override short TypeID => ProtocolId;
        
        public bool Deserted { get; set; }
        public Types.GuildInformations GuildInfo { get; set; }
        
        public PaddockGuildedInformations()
        {
        }
        
        public PaddockGuildedInformations(ulong price, bool locked, bool deserted, Types.GuildInformations guildInfo)
         : base(price, locked)
        {
            this.Deserted = deserted;
            this.GuildInfo = guildInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Deserted);
            GuildInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Deserted = reader.ReadBoolean();
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
        }
        
    }
    
}