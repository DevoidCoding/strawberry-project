

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MountInformationsForPaddock
    {
        public const short ProtocolId = 184;
        public virtual short TypeID => ProtocolId;
        
        public ushort ModelId { get; set; }
        public string Name { get; set; }
        public string OwnerName { get; set; }
        
        public MountInformationsForPaddock()
        {
        }
        
        public MountInformationsForPaddock(ushort modelId, string name, string ownerName)
        {
            this.ModelId = modelId;
            this.Name = name;
            this.OwnerName = ownerName;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ModelId);
            writer.WriteUTF(Name);
            writer.WriteUTF(OwnerName);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ModelId = reader.ReadVarUhShort();
            if (ModelId < 0)
                throw new Exception("Forbidden value on ModelId = " + ModelId + ", it doesn't respect the following condition : modelId < 0");
            Name = reader.ReadUTF();
            OwnerName = reader.ReadUTF();
        }
        
    }
    
}