

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockBuyableInformations
    {
        public const short ProtocolId = 130;
        public virtual short TypeID => ProtocolId;
        
        public ulong Price { get; set; }
        public bool Locked { get; set; }
        
        public PaddockBuyableInformations()
        {
        }
        
        public PaddockBuyableInformations(ulong price, bool locked)
        {
            this.Price = price;
            this.Locked = locked;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Price);
            writer.WriteBoolean(Locked);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
            Locked = reader.ReadBoolean();
        }
        
    }
    
}