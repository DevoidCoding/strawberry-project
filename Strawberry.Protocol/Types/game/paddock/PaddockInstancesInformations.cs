

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PaddockInstancesInformations : PaddockInformations
    {
        public new const short ProtocolId = 509;
        public override short TypeID => ProtocolId;
        
        public Types.PaddockBuyableInformations[] Paddocks { get; set; }
        
        public PaddockInstancesInformations()
        {
        }
        
        public PaddockInstancesInformations(ushort maxOutdoorMount, ushort maxItems, Types.PaddockBuyableInformations[] paddocks)
         : base(maxOutdoorMount, maxItems)
        {
            this.Paddocks = paddocks;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Paddocks.Length);
            foreach (var entry in Paddocks)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Paddocks = new Types.PaddockBuyableInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Paddocks[i] = Types.ProtocolTypeManager.GetInstance<Types.PaddockBuyableInformations>(reader.ReadShort());
                 Paddocks[i].Deserialize(reader);
            }
        }
        
    }
    
}