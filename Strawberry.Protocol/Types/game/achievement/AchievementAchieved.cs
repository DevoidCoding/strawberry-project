

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AchievementAchieved
    {
        public const short ProtocolId = 514;
        public virtual short TypeID => ProtocolId;
        
        public ushort Id { get; set; }
        public ulong AchievedBy { get; set; }
        
        public AchievementAchieved()
        {
        }
        
        public AchievementAchieved(ushort id, ulong achievedBy)
        {
            this.Id = id;
            this.AchievedBy = achievedBy;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteVarLong(AchievedBy);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            AchievedBy = reader.ReadVarUhLong();
            if (AchievedBy < 0 || AchievedBy > 9007199254740990)
                throw new Exception("Forbidden value on AchievedBy = " + AchievedBy + ", it doesn't respect the following condition : achievedBy < 0 || achievedBy > 9007199254740990");
        }
        
    }
    
}