

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AchievementStartedObjective : AchievementObjective
    {
        public new const short ProtocolId = 402;
        public override short TypeID => ProtocolId;
        
        public ushort Value { get; set; }
        
        public AchievementStartedObjective()
        {
        }
        
        public AchievementStartedObjective(uint id, ushort maxValue, ushort value)
         : base(id, maxValue)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadVarUhShort();
            if (Value < 0)
                throw new Exception("Forbidden value on Value = " + Value + ", it doesn't respect the following condition : value < 0");
        }
        
    }
    
}