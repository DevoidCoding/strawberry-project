

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AchievementObjective
    {
        public const short ProtocolId = 404;
        public virtual short TypeID => ProtocolId;
        
        public uint Id { get; set; }
        public ushort MaxValue { get; set; }
        
        public AchievementObjective()
        {
        }
        
        public AchievementObjective(uint id, ushort maxValue)
        {
            this.Id = id;
            this.MaxValue = maxValue;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Id);
            writer.WriteVarShort(MaxValue);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhInt();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            MaxValue = reader.ReadVarUhShort();
            if (MaxValue < 0)
                throw new Exception("Forbidden value on MaxValue = " + MaxValue + ", it doesn't respect the following condition : maxValue < 0");
        }
        
    }
    
}