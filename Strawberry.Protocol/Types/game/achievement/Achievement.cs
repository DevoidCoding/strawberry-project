

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class Achievement
    {
        public const short ProtocolId = 363;
        public virtual short TypeID => ProtocolId;
        
        public ushort Id { get; set; }
        public Types.AchievementObjective[] FinishedObjective { get; set; }
        public Types.AchievementStartedObjective[] StartedObjectives { get; set; }
        
        public Achievement()
        {
        }
        
        public Achievement(ushort id, Types.AchievementObjective[] finishedObjective, Types.AchievementStartedObjective[] startedObjectives)
        {
            this.Id = id;
            this.FinishedObjective = finishedObjective;
            this.StartedObjectives = startedObjectives;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteUShort((ushort)FinishedObjective.Length);
            foreach (var entry in FinishedObjective)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)StartedObjectives.Length);
            foreach (var entry in StartedObjectives)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            var limit = reader.ReadUShort();
            FinishedObjective = new Types.AchievementObjective[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishedObjective[i] = new Types.AchievementObjective();
                 FinishedObjective[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            StartedObjectives = new Types.AchievementStartedObjective[limit];
            for (int i = 0; i < limit; i++)
            {
                 StartedObjectives[i] = new Types.AchievementStartedObjective();
                 StartedObjectives[i].Deserialize(reader);
            }
        }
        
    }
    
}