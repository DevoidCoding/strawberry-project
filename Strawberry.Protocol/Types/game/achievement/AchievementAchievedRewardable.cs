

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AchievementAchievedRewardable : AchievementAchieved
    {
        public new const short ProtocolId = 515;
        public override short TypeID => ProtocolId;
        
        public ushort Finishedlevel { get; set; }
        
        public AchievementAchievedRewardable()
        {
        }
        
        public AchievementAchievedRewardable(ushort id, ulong achievedBy, ushort finishedlevel)
         : base(id, achievedBy)
        {
            this.Finishedlevel = finishedlevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Finishedlevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Finishedlevel = reader.ReadVarUhShort();
            if (Finishedlevel < 0 || Finishedlevel > 200)
                throw new Exception("Forbidden value on Finishedlevel = " + Finishedlevel + ", it doesn't respect the following condition : finishedlevel < 0 || finishedlevel > 200");
        }
        
    }
    
}