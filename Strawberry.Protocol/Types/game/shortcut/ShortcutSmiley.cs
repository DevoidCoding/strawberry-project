

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ShortcutSmiley : Shortcut
    {
        public new const short ProtocolId = 388;
        public override short TypeID => ProtocolId;
        
        public ushort SmileyId { get; set; }
        
        public ShortcutSmiley()
        {
        }
        
        public ShortcutSmiley(sbyte slot, ushort smileyId)
         : base(slot)
        {
            this.SmileyId = smileyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SmileyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SmileyId = reader.ReadVarUhShort();
            if (SmileyId < 0)
                throw new Exception("Forbidden value on SmileyId = " + SmileyId + ", it doesn't respect the following condition : smileyId < 0");
        }
        
    }
    
}