

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ShortcutSpell : Shortcut
    {
        public new const short ProtocolId = 368;
        public override short TypeID => ProtocolId;
        
        public ushort SpellId { get; set; }
        
        public ShortcutSpell()
        {
        }
        
        public ShortcutSpell(sbyte slot, ushort spellId)
         : base(slot)
        {
            this.SpellId = spellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
        }
        
    }
    
}