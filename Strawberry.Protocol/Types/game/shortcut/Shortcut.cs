

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class Shortcut
    {
        public const short ProtocolId = 369;
        public virtual short TypeID => ProtocolId;
        
        public sbyte Slot { get; set; }
        
        public Shortcut()
        {
        }
        
        public Shortcut(sbyte slot)
        {
            this.Slot = slot;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Slot);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Slot = reader.ReadSByte();
            if (Slot < 0 || Slot > 99)
                throw new Exception("Forbidden value on Slot = " + Slot + ", it doesn't respect the following condition : slot < 0 || slot > 99");
        }
        
    }
    
}