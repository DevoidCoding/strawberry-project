

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ShortcutObject : Shortcut
    {
        public new const short ProtocolId = 367;
        public override short TypeID => ProtocolId;
        
        
        public ShortcutObject()
        {
        }
        
        public ShortcutObject(sbyte slot)
         : base(slot)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}