

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ShortcutObjectItem : ShortcutObject
    {
        public new const short ProtocolId = 371;
        public override short TypeID => ProtocolId;
        
        public int ItemUID { get; set; }
        public int ItemGID { get; set; }
        
        public ShortcutObjectItem()
        {
        }
        
        public ShortcutObjectItem(sbyte slot, int itemUID, int itemGID)
         : base(slot)
        {
            this.ItemUID = itemUID;
            this.ItemGID = itemGID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(ItemUID);
            writer.WriteInt(ItemGID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ItemUID = reader.ReadInt();
            ItemGID = reader.ReadInt();
        }
        
    }
    
}