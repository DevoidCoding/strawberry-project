

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ShortcutObjectIdolsPreset : ShortcutObject
    {
        public new const short ProtocolId = 492;
        public override short TypeID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        
        public ShortcutObjectIdolsPreset()
        {
        }
        
        public ShortcutObjectIdolsPreset(sbyte slot, sbyte presetId)
         : base(slot)
        {
            this.PresetId = presetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PresetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
        }
        
    }
    
}