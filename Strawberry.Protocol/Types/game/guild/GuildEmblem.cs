

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildEmblem
    {
        public const short ProtocolId = 87;
        public virtual short TypeID => ProtocolId;
        
        public ushort SymbolShape { get; set; }
        public int SymbolColor { get; set; }
        public sbyte BackgroundShape { get; set; }
        public int BackgroundColor { get; set; }
        
        public GuildEmblem()
        {
        }
        
        public GuildEmblem(ushort symbolShape, int symbolColor, sbyte backgroundShape, int backgroundColor)
        {
            this.SymbolShape = symbolShape;
            this.SymbolColor = symbolColor;
            this.BackgroundShape = backgroundShape;
            this.BackgroundColor = backgroundColor;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SymbolShape);
            writer.WriteInt(SymbolColor);
            writer.WriteSByte(BackgroundShape);
            writer.WriteInt(BackgroundColor);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SymbolShape = reader.ReadVarUhShort();
            if (SymbolShape < 0)
                throw new Exception("Forbidden value on SymbolShape = " + SymbolShape + ", it doesn't respect the following condition : symbolShape < 0");
            SymbolColor = reader.ReadInt();
            BackgroundShape = reader.ReadSByte();
            if (BackgroundShape < 0)
                throw new Exception("Forbidden value on BackgroundShape = " + BackgroundShape + ", it doesn't respect the following condition : backgroundShape < 0");
            BackgroundColor = reader.ReadInt();
        }
        
    }
    
}