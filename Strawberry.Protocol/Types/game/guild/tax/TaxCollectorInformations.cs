

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TaxCollectorInformations
    {
        public const short ProtocolId = 167;
        public virtual short TypeID => ProtocolId;
        
        public double UniqueId { get; set; }
        public ushort FirtNameId { get; set; }
        public ushort LastNameId { get; set; }
        public Types.AdditionalTaxCollectorInformations AdditionalInfos { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public ushort SubAreaId { get; set; }
        public sbyte State { get; set; }
        public Types.EntityLook Look { get; set; }
        public Types.TaxCollectorComplementaryInformations[] Complements { get; set; }
        
        public TaxCollectorInformations()
        {
        }
        
        public TaxCollectorInformations(double uniqueId, ushort firtNameId, ushort lastNameId, Types.AdditionalTaxCollectorInformations additionalInfos, short worldX, short worldY, ushort subAreaId, sbyte state, Types.EntityLook look, Types.TaxCollectorComplementaryInformations[] complements)
        {
            this.UniqueId = uniqueId;
            this.FirtNameId = firtNameId;
            this.LastNameId = lastNameId;
            this.AdditionalInfos = additionalInfos;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.SubAreaId = subAreaId;
            this.State = state;
            this.Look = look;
            this.Complements = complements;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(UniqueId);
            writer.WriteVarShort(FirtNameId);
            writer.WriteVarShort(LastNameId);
            AdditionalInfos.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteVarShort(SubAreaId);
            writer.WriteSByte(State);
            Look.Serialize(writer);
            writer.WriteUShort((ushort)Complements.Length);
            foreach (var entry in Complements)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            UniqueId = reader.ReadDouble();
            if (UniqueId < 0 || UniqueId > 9007199254740990)
                throw new Exception("Forbidden value on UniqueId = " + UniqueId + ", it doesn't respect the following condition : uniqueId < 0 || uniqueId > 9007199254740990");
            FirtNameId = reader.ReadVarUhShort();
            if (FirtNameId < 0)
                throw new Exception("Forbidden value on FirtNameId = " + FirtNameId + ", it doesn't respect the following condition : firtNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
            AdditionalInfos = new Types.AdditionalTaxCollectorInformations();
            AdditionalInfos.Deserialize(reader);
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
            var limit = reader.ReadUShort();
            Complements = new Types.TaxCollectorComplementaryInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Complements[i] = Types.ProtocolTypeManager.GetInstance<Types.TaxCollectorComplementaryInformations>(reader.ReadShort());
                 Complements[i].Deserialize(reader);
            }
        }
        
    }
    
}