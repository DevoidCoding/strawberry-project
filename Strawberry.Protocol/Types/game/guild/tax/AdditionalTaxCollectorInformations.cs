

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AdditionalTaxCollectorInformations
    {
        public const short ProtocolId = 165;
        public virtual short TypeID => ProtocolId;
        
        public string CollectorCallerName { get; set; }
        public int Date { get; set; }
        
        public AdditionalTaxCollectorInformations()
        {
        }
        
        public AdditionalTaxCollectorInformations(string collectorCallerName, int date)
        {
            this.CollectorCallerName = collectorCallerName;
            this.Date = date;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(CollectorCallerName);
            writer.WriteInt(Date);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CollectorCallerName = reader.ReadUTF();
            Date = reader.ReadInt();
            if (Date < 0)
                throw new Exception("Forbidden value on Date = " + Date + ", it doesn't respect the following condition : date < 0");
        }
        
    }
    
}