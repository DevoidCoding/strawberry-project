

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TaxCollectorFightersInformation
    {
        public const short ProtocolId = 169;
        public virtual short TypeID => ProtocolId;
        
        public double CollectorId { get; set; }
        public Types.CharacterMinimalPlusLookInformations[] AllyCharactersInformations { get; set; }
        public Types.CharacterMinimalPlusLookInformations[] EnemyCharactersInformations { get; set; }
        
        public TaxCollectorFightersInformation()
        {
        }
        
        public TaxCollectorFightersInformation(double collectorId, Types.CharacterMinimalPlusLookInformations[] allyCharactersInformations, Types.CharacterMinimalPlusLookInformations[] enemyCharactersInformations)
        {
            this.CollectorId = collectorId;
            this.AllyCharactersInformations = allyCharactersInformations;
            this.EnemyCharactersInformations = enemyCharactersInformations;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(CollectorId);
            writer.WriteUShort((ushort)AllyCharactersInformations.Length);
            foreach (var entry in AllyCharactersInformations)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)EnemyCharactersInformations.Length);
            foreach (var entry in EnemyCharactersInformations)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CollectorId = reader.ReadDouble();
            if (CollectorId < 0 || CollectorId > 9007199254740990)
                throw new Exception("Forbidden value on CollectorId = " + CollectorId + ", it doesn't respect the following condition : collectorId < 0 || collectorId > 9007199254740990");
            var limit = reader.ReadUShort();
            AllyCharactersInformations = new Types.CharacterMinimalPlusLookInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllyCharactersInformations[i] = Types.ProtocolTypeManager.GetInstance<Types.CharacterMinimalPlusLookInformations>(reader.ReadShort());
                 AllyCharactersInformations[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            EnemyCharactersInformations = new Types.CharacterMinimalPlusLookInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 EnemyCharactersInformations[i] = Types.ProtocolTypeManager.GetInstance<Types.CharacterMinimalPlusLookInformations>(reader.ReadShort());
                 EnemyCharactersInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}