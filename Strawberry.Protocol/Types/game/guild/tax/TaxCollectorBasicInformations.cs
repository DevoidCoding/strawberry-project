

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TaxCollectorBasicInformations
    {
        public const short ProtocolId = 96;
        public virtual short TypeID => ProtocolId;
        
        public ushort FirstNameId { get; set; }
        public ushort LastNameId { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        
        public TaxCollectorBasicInformations()
        {
        }
        
        public TaxCollectorBasicInformations(ushort firstNameId, ushort lastNameId, short worldX, short worldY, double mapId, ushort subAreaId)
        {
            this.FirstNameId = firstNameId;
            this.LastNameId = lastNameId;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FirstNameId);
            writer.WriteVarShort(LastNameId);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            FirstNameId = reader.ReadVarUhShort();
            if (FirstNameId < 0)
                throw new Exception("Forbidden value on FirstNameId = " + FirstNameId + ", it doesn't respect the following condition : firstNameId < 0");
            LastNameId = reader.ReadVarUhShort();
            if (LastNameId < 0)
                throw new Exception("Forbidden value on LastNameId = " + LastNameId + ", it doesn't respect the following condition : lastNameId < 0");
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
        }
        
    }
    
}