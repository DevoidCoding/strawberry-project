

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TaxCollectorLootInformations : TaxCollectorComplementaryInformations
    {
        public new const short ProtocolId = 372;
        public override short TypeID => ProtocolId;
        
        public ulong Kamas { get; set; }
        public ulong Experience { get; set; }
        public uint Pods { get; set; }
        public ulong ItemsValue { get; set; }
        
        public TaxCollectorLootInformations()
        {
        }
        
        public TaxCollectorLootInformations(ulong kamas, ulong experience, uint pods, ulong itemsValue)
        {
            this.Kamas = kamas;
            this.Experience = experience;
            this.Pods = pods;
            this.ItemsValue = itemsValue;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Kamas);
            writer.WriteVarLong(Experience);
            writer.WriteVarInt(Pods);
            writer.WriteVarLong(ItemsValue);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            Pods = reader.ReadVarUhInt();
            if (Pods < 0)
                throw new Exception("Forbidden value on Pods = " + Pods + ", it doesn't respect the following condition : pods < 0");
            ItemsValue = reader.ReadVarUhLong();
            if (ItemsValue < 0 || ItemsValue > 9007199254740990)
                throw new Exception("Forbidden value on ItemsValue = " + ItemsValue + ", it doesn't respect the following condition : itemsValue < 0 || itemsValue > 9007199254740990");
        }
        
    }
    
}