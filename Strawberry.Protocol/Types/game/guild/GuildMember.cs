

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class GuildMember : CharacterMinimalInformations
    {
        public new const short ProtocolId = 88;
        public override short TypeID => ProtocolId;
        
        public bool Sex { get; set; }
        public bool HavenBagShared { get; set; }
        public sbyte Breed { get; set; }
        public ushort Rank { get; set; }
        public ulong GivenExperience { get; set; }
        public sbyte ExperienceGivenPercent { get; set; }
        public uint Rights { get; set; }
        public sbyte Connected { get; set; }
        public sbyte AlignmentSide { get; set; }
        public ushort HoursSinceLastConnection { get; set; }
        public ushort MoodSmileyId { get; set; }
        public int AccountId { get; set; }
        public int AchievementPoints { get; set; }
        public Types.PlayerStatus Status { get; set; }
        
        public GuildMember()
        {
        }
        
        public GuildMember(ulong id, string name, ushort level, bool sex, bool havenBagShared, sbyte breed, ushort rank, ulong givenExperience, sbyte experienceGivenPercent, uint rights, sbyte connected, sbyte alignmentSide, ushort hoursSinceLastConnection, ushort moodSmileyId, int accountId, int achievementPoints, Types.PlayerStatus status)
         : base(id, name, level)
        {
            this.Sex = sex;
            this.HavenBagShared = havenBagShared;
            this.Breed = breed;
            this.Rank = rank;
            this.GivenExperience = givenExperience;
            this.ExperienceGivenPercent = experienceGivenPercent;
            this.Rights = rights;
            this.Connected = connected;
            this.AlignmentSide = alignmentSide;
            this.HoursSinceLastConnection = hoursSinceLastConnection;
            this.MoodSmileyId = moodSmileyId;
            this.AccountId = accountId;
            this.AchievementPoints = achievementPoints;
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Sex);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, HavenBagShared);
            writer.WriteByte(flag1);
            writer.WriteSByte(Breed);
            writer.WriteVarShort(Rank);
            writer.WriteVarLong(GivenExperience);
            writer.WriteSByte(ExperienceGivenPercent);
            writer.WriteVarInt(Rights);
            writer.WriteSByte(Connected);
            writer.WriteSByte(AlignmentSide);
            writer.WriteUShort(HoursSinceLastConnection);
            writer.WriteVarShort(MoodSmileyId);
            writer.WriteInt(AccountId);
            writer.WriteInt(AchievementPoints);
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag1, 0);
            HavenBagShared = BooleanByteWrapper.GetFlag(flag1, 1);
            Breed = reader.ReadSByte();
            Rank = reader.ReadVarUhShort();
            if (Rank < 0)
                throw new Exception("Forbidden value on Rank = " + Rank + ", it doesn't respect the following condition : rank < 0");
            GivenExperience = reader.ReadVarUhLong();
            if (GivenExperience < 0 || GivenExperience > 9007199254740990)
                throw new Exception("Forbidden value on GivenExperience = " + GivenExperience + ", it doesn't respect the following condition : givenExperience < 0 || givenExperience > 9007199254740990");
            ExperienceGivenPercent = reader.ReadSByte();
            if (ExperienceGivenPercent < 0 || ExperienceGivenPercent > 100)
                throw new Exception("Forbidden value on ExperienceGivenPercent = " + ExperienceGivenPercent + ", it doesn't respect the following condition : experienceGivenPercent < 0 || experienceGivenPercent > 100");
            Rights = reader.ReadVarUhInt();
            if (Rights < 0)
                throw new Exception("Forbidden value on Rights = " + Rights + ", it doesn't respect the following condition : rights < 0");
            Connected = reader.ReadSByte();
            if (Connected < 0)
                throw new Exception("Forbidden value on Connected = " + Connected + ", it doesn't respect the following condition : connected < 0");
            AlignmentSide = reader.ReadSByte();
            HoursSinceLastConnection = reader.ReadUShort();
            if (HoursSinceLastConnection < 0 || HoursSinceLastConnection > 65535)
                throw new Exception("Forbidden value on HoursSinceLastConnection = " + HoursSinceLastConnection + ", it doesn't respect the following condition : hoursSinceLastConnection < 0 || hoursSinceLastConnection > 65535");
            MoodSmileyId = reader.ReadVarUhShort();
            if (MoodSmileyId < 0)
                throw new Exception("Forbidden value on MoodSmileyId = " + MoodSmileyId + ", it doesn't respect the following condition : moodSmileyId < 0");
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            AchievementPoints = reader.ReadInt();
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
        
    }
    
}