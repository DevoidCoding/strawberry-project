

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class HavenBagFurnitureInformation
    {
        public const short ProtocolId = 498;
        public virtual short TypeID => ProtocolId;
        
        public ushort CellId { get; set; }
        public int FunitureId { get; set; }
        public sbyte Orientation { get; set; }
        
        public HavenBagFurnitureInformation()
        {
        }
        
        public HavenBagFurnitureInformation(ushort cellId, int funitureId, sbyte orientation)
        {
            this.CellId = cellId;
            this.FunitureId = funitureId;
            this.Orientation = orientation;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteInt(FunitureId);
            writer.WriteSByte(Orientation);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            CellId = reader.ReadVarUhShort();
            if (CellId < 0)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0");
            FunitureId = reader.ReadInt();
            Orientation = reader.ReadSByte();
            if (Orientation < 0)
                throw new Exception("Forbidden value on Orientation = " + Orientation + ", it doesn't respect the following condition : orientation < 0");
        }
        
    }
    
}