

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FriendSpouseInformations
    {
        public const short ProtocolId = 77;
        public virtual short TypeID => ProtocolId;
        
        public int SpouseAccountId { get; set; }
        public ulong SpouseId { get; set; }
        public string SpouseName { get; set; }
        public ushort SpouseLevel { get; set; }
        public sbyte Breed { get; set; }
        public sbyte Sex { get; set; }
        public Types.EntityLook SpouseEntityLook { get; set; }
        public Types.GuildInformations GuildInfo { get; set; }
        public sbyte AlignmentSide { get; set; }
        
        public FriendSpouseInformations()
        {
        }
        
        public FriendSpouseInformations(int spouseAccountId, ulong spouseId, string spouseName, ushort spouseLevel, sbyte breed, sbyte sex, Types.EntityLook spouseEntityLook, Types.GuildInformations guildInfo, sbyte alignmentSide)
        {
            this.SpouseAccountId = spouseAccountId;
            this.SpouseId = spouseId;
            this.SpouseName = spouseName;
            this.SpouseLevel = spouseLevel;
            this.Breed = breed;
            this.Sex = sex;
            this.SpouseEntityLook = spouseEntityLook;
            this.GuildInfo = guildInfo;
            this.AlignmentSide = alignmentSide;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(SpouseAccountId);
            writer.WriteVarLong(SpouseId);
            writer.WriteUTF(SpouseName);
            writer.WriteVarShort(SpouseLevel);
            writer.WriteSByte(Breed);
            writer.WriteSByte(Sex);
            SpouseEntityLook.Serialize(writer);
            GuildInfo.Serialize(writer);
            writer.WriteSByte(AlignmentSide);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SpouseAccountId = reader.ReadInt();
            if (SpouseAccountId < 0)
                throw new Exception("Forbidden value on SpouseAccountId = " + SpouseAccountId + ", it doesn't respect the following condition : spouseAccountId < 0");
            SpouseId = reader.ReadVarUhLong();
            if (SpouseId < 0 || SpouseId > 9007199254740990)
                throw new Exception("Forbidden value on SpouseId = " + SpouseId + ", it doesn't respect the following condition : spouseId < 0 || spouseId > 9007199254740990");
            SpouseName = reader.ReadUTF();
            SpouseLevel = reader.ReadVarUhShort();
            if (SpouseLevel < 0)
                throw new Exception("Forbidden value on SpouseLevel = " + SpouseLevel + ", it doesn't respect the following condition : spouseLevel < 0");
            Breed = reader.ReadSByte();
            Sex = reader.ReadSByte();
            SpouseEntityLook = new Types.EntityLook();
            SpouseEntityLook.Deserialize(reader);
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
            AlignmentSide = reader.ReadSByte();
        }
        
    }
    
}