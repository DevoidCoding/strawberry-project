

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FriendInformations : AbstractContactInformations
    {
        public new const short ProtocolId = 78;
        public override short TypeID => ProtocolId;
        
        public sbyte PlayerState { get; set; }
        public ushort LastConnection { get; set; }
        public int AchievementPoints { get; set; }
        
        public FriendInformations()
        {
        }
        
        public FriendInformations(int accountId, string accountName, sbyte playerState, ushort lastConnection, int achievementPoints)
         : base(accountId, accountName)
        {
            this.PlayerState = playerState;
            this.LastConnection = lastConnection;
            this.AchievementPoints = achievementPoints;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PlayerState);
            writer.WriteVarShort(LastConnection);
            writer.WriteInt(AchievementPoints);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PlayerState = reader.ReadSByte();
            if (PlayerState < 0)
                throw new Exception("Forbidden value on PlayerState = " + PlayerState + ", it doesn't respect the following condition : playerState < 0");
            LastConnection = reader.ReadVarUhShort();
            if (LastConnection < 0)
                throw new Exception("Forbidden value on LastConnection = " + LastConnection + ", it doesn't respect the following condition : lastConnection < 0");
            AchievementPoints = reader.ReadInt();
        }
        
    }
    
}