

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class AbstractContactInformations
    {
        public const short ProtocolId = 380;
        public virtual short TypeID => ProtocolId;
        
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        
        public AbstractContactInformations()
        {
        }
        
        public AbstractContactInformations(int accountId, string accountName)
        {
            this.AccountId = accountId;
            this.AccountName = accountName;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteUTF(AccountName);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            AccountName = reader.ReadUTF();
        }
        
    }
    
}