

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class IgnoredOnlineInformations : IgnoredInformations
    {
        public new const short ProtocolId = 105;
        public override short TypeID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public string PlayerName { get; set; }
        public sbyte Breed { get; set; }
        public bool Sex { get; set; }
        
        public IgnoredOnlineInformations()
        {
        }
        
        public IgnoredOnlineInformations(int accountId, string accountName, ulong playerId, string playerName, sbyte breed, bool sex)
         : base(accountId, accountName)
        {
            this.PlayerId = playerId;
            this.PlayerName = playerName;
            this.Breed = breed;
            this.Sex = sex;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteSByte(Breed);
            writer.WriteBoolean(Sex);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            PlayerName = reader.ReadUTF();
            Breed = reader.ReadSByte();
            if (Breed < (byte)Enums.PlayableBreedEnum.Feca || Breed > (byte)Enums.PlayableBreedEnum.Ouginak)
                throw new Exception("Forbidden value on Breed = " + Breed + ", it doesn't respect the following condition : breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Ouginak");
            Sex = reader.ReadBoolean();
        }
        
    }
    
}