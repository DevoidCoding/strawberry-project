

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class IgnoredInformations : AbstractContactInformations
    {
        public new const short ProtocolId = 106;
        public override short TypeID => ProtocolId;
        
        
        public IgnoredInformations()
        {
        }
        
        public IgnoredInformations(int accountId, string accountName)
         : base(accountId, accountName)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}