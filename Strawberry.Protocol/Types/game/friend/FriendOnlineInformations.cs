

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FriendOnlineInformations : FriendInformations
    {
        public new const short ProtocolId = 92;
        public override short TypeID => ProtocolId;
        
        public bool Sex { get; set; }
        public bool HavenBagShared { get; set; }
        public ulong PlayerId { get; set; }
        public string PlayerName { get; set; }
        public ushort Level { get; set; }
        public sbyte AlignmentSide { get; set; }
        public sbyte Breed { get; set; }
        public Types.GuildInformations GuildInfo { get; set; }
        public ushort MoodSmileyId { get; set; }
        public Types.PlayerStatus Status { get; set; }
        
        public FriendOnlineInformations()
        {
        }
        
        public FriendOnlineInformations(int accountId, string accountName, sbyte playerState, ushort lastConnection, int achievementPoints, bool sex, bool havenBagShared, ulong playerId, string playerName, ushort level, sbyte alignmentSide, sbyte breed, Types.GuildInformations guildInfo, ushort moodSmileyId, Types.PlayerStatus status)
         : base(accountId, accountName, playerState, lastConnection, achievementPoints)
        {
            this.Sex = sex;
            this.HavenBagShared = havenBagShared;
            this.PlayerId = playerId;
            this.PlayerName = playerName;
            this.Level = level;
            this.AlignmentSide = alignmentSide;
            this.Breed = breed;
            this.GuildInfo = guildInfo;
            this.MoodSmileyId = moodSmileyId;
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Sex);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, HavenBagShared);
            writer.WriteByte(flag1);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarShort(Level);
            writer.WriteSByte(AlignmentSide);
            writer.WriteSByte(Breed);
            GuildInfo.Serialize(writer);
            writer.WriteVarShort(MoodSmileyId);
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag1, 0);
            HavenBagShared = BooleanByteWrapper.GetFlag(flag1, 1);
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            PlayerName = reader.ReadUTF();
            Level = reader.ReadVarUhShort();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
            AlignmentSide = reader.ReadSByte();
            Breed = reader.ReadSByte();
            if (Breed < (byte)Enums.PlayableBreedEnum.Feca || Breed > (byte)Enums.PlayableBreedEnum.Ouginak)
                throw new Exception("Forbidden value on Breed = " + Breed + ", it doesn't respect the following condition : breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Ouginak");
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
            MoodSmileyId = reader.ReadVarUhShort();
            if (MoodSmileyId < 0)
                throw new Exception("Forbidden value on MoodSmileyId = " + MoodSmileyId + ", it doesn't respect the following condition : moodSmileyId < 0");
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
        
    }
    
}