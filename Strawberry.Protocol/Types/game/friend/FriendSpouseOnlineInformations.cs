

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class FriendSpouseOnlineInformations : FriendSpouseInformations
    {
        public new const short ProtocolId = 93;
        public override short TypeID => ProtocolId;
        
        public bool InFight { get; set; }
        public bool FollowSpouse { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        
        public FriendSpouseOnlineInformations()
        {
        }
        
        public FriendSpouseOnlineInformations(int spouseAccountId, ulong spouseId, string spouseName, ushort spouseLevel, sbyte breed, sbyte sex, Types.EntityLook spouseEntityLook, Types.GuildInformations guildInfo, sbyte alignmentSide, bool inFight, bool followSpouse, double mapId, ushort subAreaId)
         : base(spouseAccountId, spouseId, spouseName, spouseLevel, breed, sex, spouseEntityLook, guildInfo, alignmentSide)
        {
            this.InFight = inFight;
            this.FollowSpouse = followSpouse;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, InFight);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, FollowSpouse);
            writer.WriteByte(flag1);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            InFight = BooleanByteWrapper.GetFlag(flag1, 0);
            FollowSpouse = BooleanByteWrapper.GetFlag(flag1, 1);
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
        }
        
    }
    
}