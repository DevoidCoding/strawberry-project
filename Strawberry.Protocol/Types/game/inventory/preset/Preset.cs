

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class Preset
    {
        public const short ProtocolId = 355;
        public virtual short TypeID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte SymbolId { get; set; }
        public bool Mount { get; set; }
        public Types.PresetItem[] Objects { get; set; }
        
        public Preset()
        {
        }
        
        public Preset(sbyte presetId, sbyte symbolId, bool mount, Types.PresetItem[] objects)
        {
            this.PresetId = presetId;
            this.SymbolId = symbolId;
            this.Mount = mount;
            this.Objects = objects;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(SymbolId);
            writer.WriteBoolean(Mount);
            writer.WriteUShort((ushort)Objects.Length);
            foreach (var entry in Objects)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            SymbolId = reader.ReadSByte();
            if (SymbolId < 0)
                throw new Exception("Forbidden value on SymbolId = " + SymbolId + ", it doesn't respect the following condition : symbolId < 0");
            Mount = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            Objects = new Types.PresetItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Objects[i] = new Types.PresetItem();
                 Objects[i].Deserialize(reader);
            }
        }
        
    }
    
}