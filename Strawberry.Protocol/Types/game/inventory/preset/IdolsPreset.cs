

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class IdolsPreset
    {
        public const short ProtocolId = 491;
        public virtual short TypeID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte SymbolId { get; set; }
        public ushort[] IdolId { get; set; }
        
        public IdolsPreset()
        {
        }
        
        public IdolsPreset(sbyte presetId, sbyte symbolId, ushort[] idolId)
        {
            this.PresetId = presetId;
            this.SymbolId = symbolId;
            this.IdolId = idolId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(SymbolId);
            writer.WriteUShort((ushort)IdolId.Length);
            foreach (var entry in IdolId)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            SymbolId = reader.ReadSByte();
            if (SymbolId < 0)
                throw new Exception("Forbidden value on SymbolId = " + SymbolId + ", it doesn't respect the following condition : symbolId < 0");
            var limit = reader.ReadUShort();
            IdolId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 IdolId[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}