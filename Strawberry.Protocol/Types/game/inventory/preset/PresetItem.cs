

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class PresetItem
    {
        public const short ProtocolId = 354;
        public virtual short TypeID => ProtocolId;
        
        public byte Position { get; set; }
        public ushort ObjGid { get; set; }
        public uint ObjUid { get; set; }
        
        public PresetItem()
        {
        }
        
        public PresetItem(byte position, ushort objGid, uint objUid)
        {
            this.Position = position;
            this.ObjGid = objGid;
            this.ObjUid = objUid;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteByte(Position);
            writer.WriteVarShort(ObjGid);
            writer.WriteVarInt(ObjUid);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Position = reader.ReadByte();
            if (Position < 0 || Position > 255)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 255");
            ObjGid = reader.ReadVarUhShort();
            if (ObjGid < 0)
                throw new Exception("Forbidden value on ObjGid = " + ObjGid + ", it doesn't respect the following condition : objGid < 0");
            ObjUid = reader.ReadVarUhInt();
            if (ObjUid < 0)
                throw new Exception("Forbidden value on ObjUid = " + ObjUid + ", it doesn't respect the following condition : objUid < 0");
        }
        
    }
    
}