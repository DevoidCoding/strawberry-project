

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ServerSessionConstant
    {
        public const short ProtocolId = 430;
        public virtual short TypeID => ProtocolId;
        
        public ushort Id { get; set; }
        
        public ServerSessionConstant()
        {
        }
        
        public ServerSessionConstant(ushort id)
        {
            this.Id = id;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
        }
        
    }
    
}