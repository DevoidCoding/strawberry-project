

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ServerSessionConstantInteger : ServerSessionConstant
    {
        public new const short ProtocolId = 433;
        public override short TypeID => ProtocolId;
        
        public int Value { get; set; }
        
        public ServerSessionConstantInteger()
        {
        }
        
        public ServerSessionConstantInteger(ushort id, int value)
         : base(id)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadInt();
        }
        
    }
    
}