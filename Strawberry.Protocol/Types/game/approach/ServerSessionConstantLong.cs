

// Generated on 02/12/2018 03:56:50
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ServerSessionConstantLong : ServerSessionConstant
    {
        public new const short ProtocolId = 429;
        public override short TypeID => ProtocolId;
        
        public double Value { get; set; }
        
        public ServerSessionConstantLong()
        {
        }
        
        public ServerSessionConstantLong(ushort id, double value)
         : base(id)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadDouble();
            if (Value < -9007199254740990 || Value > 9007199254740990)
                throw new Exception("Forbidden value on Value = " + Value + ", it doesn't respect the following condition : value < -9007199254740990 || value > 9007199254740990");
        }
        
    }
    
}