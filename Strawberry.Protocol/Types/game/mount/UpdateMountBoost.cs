

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class UpdateMountBoost
    {
        public const short ProtocolId = 356;
        public virtual short TypeID => ProtocolId;
        
        public sbyte Type { get; set; }
        
        public UpdateMountBoost()
        {
        }
        
        public UpdateMountBoost(sbyte type)
        {
            this.Type = type;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}