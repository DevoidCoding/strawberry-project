

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MountClientData
    {
        public const short ProtocolId = 178;
        public virtual short TypeID => ProtocolId;
        
        public bool Sex { get; set; }
        public bool IsRideable { get; set; }
        public bool IsWild { get; set; }
        public bool IsFecondationReady { get; set; }
        public bool UseHarnessColors { get; set; }
        public double Id { get; set; }
        public uint Model { get; set; }
        public int[] Ancestor { get; set; }
        public int[] Behaviors { get; set; }
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public ulong Experience { get; set; }
        public ulong ExperienceForLevel { get; set; }
        public double ExperienceForNextLevel { get; set; }
        public sbyte Level { get; set; }
        public uint MaxPods { get; set; }
        public uint Stamina { get; set; }
        public uint StaminaMax { get; set; }
        public uint Maturity { get; set; }
        public uint MaturityForAdult { get; set; }
        public uint Energy { get; set; }
        public uint EnergyMax { get; set; }
        public int Serenity { get; set; }
        public int AggressivityMax { get; set; }
        public uint SerenityMax { get; set; }
        public uint Love { get; set; }
        public uint LoveMax { get; set; }
        public int FecondationTime { get; set; }
        public int BoostLimiter { get; set; }
        public double BoostMax { get; set; }
        public int ReproductionCount { get; set; }
        public uint ReproductionCountMax { get; set; }
        public ushort HarnessGID { get; set; }
        public Types.ObjectEffectInteger[] EffectList { get; set; }
        
        public MountClientData()
        {
        }
        
        public MountClientData(bool sex, bool isRideable, bool isWild, bool isFecondationReady, bool useHarnessColors, double id, uint model, int[] ancestor, int[] behaviors, string name, int ownerId, ulong experience, ulong experienceForLevel, double experienceForNextLevel, sbyte level, uint maxPods, uint stamina, uint staminaMax, uint maturity, uint maturityForAdult, uint energy, uint energyMax, int serenity, int aggressivityMax, uint serenityMax, uint love, uint loveMax, int fecondationTime, int boostLimiter, double boostMax, int reproductionCount, uint reproductionCountMax, ushort harnessGID, Types.ObjectEffectInteger[] effectList)
        {
            this.Sex = sex;
            this.IsRideable = isRideable;
            this.IsWild = isWild;
            this.IsFecondationReady = isFecondationReady;
            this.UseHarnessColors = useHarnessColors;
            this.Id = id;
            this.Model = model;
            this.Ancestor = ancestor;
            this.Behaviors = behaviors;
            this.Name = name;
            this.OwnerId = ownerId;
            this.Experience = experience;
            this.ExperienceForLevel = experienceForLevel;
            this.ExperienceForNextLevel = experienceForNextLevel;
            this.Level = level;
            this.MaxPods = maxPods;
            this.Stamina = stamina;
            this.StaminaMax = staminaMax;
            this.Maturity = maturity;
            this.MaturityForAdult = maturityForAdult;
            this.Energy = energy;
            this.EnergyMax = energyMax;
            this.Serenity = serenity;
            this.AggressivityMax = aggressivityMax;
            this.SerenityMax = serenityMax;
            this.Love = love;
            this.LoveMax = loveMax;
            this.FecondationTime = fecondationTime;
            this.BoostLimiter = boostLimiter;
            this.BoostMax = boostMax;
            this.ReproductionCount = reproductionCount;
            this.ReproductionCountMax = reproductionCountMax;
            this.HarnessGID = harnessGID;
            this.EffectList = effectList;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Sex);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsRideable);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, IsWild);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, IsFecondationReady);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 4, UseHarnessColors);
            writer.WriteByte(flag1);
            writer.WriteDouble(Id);
            writer.WriteVarInt(Model);
            writer.WriteUShort((ushort)Ancestor.Length);
            foreach (var entry in Ancestor)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUShort((ushort)Behaviors.Length);
            foreach (var entry in Behaviors)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUTF(Name);
            writer.WriteInt(OwnerId);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExperienceForLevel);
            writer.WriteDouble(ExperienceForNextLevel);
            writer.WriteSByte(Level);
            writer.WriteVarInt(MaxPods);
            writer.WriteVarInt(Stamina);
            writer.WriteVarInt(StaminaMax);
            writer.WriteVarInt(Maturity);
            writer.WriteVarInt(MaturityForAdult);
            writer.WriteVarInt(Energy);
            writer.WriteVarInt(EnergyMax);
            writer.WriteInt(Serenity);
            writer.WriteInt(AggressivityMax);
            writer.WriteVarInt(SerenityMax);
            writer.WriteVarInt(Love);
            writer.WriteVarInt(LoveMax);
            writer.WriteInt(FecondationTime);
            writer.WriteInt(BoostLimiter);
            writer.WriteDouble(BoostMax);
            writer.WriteInt(ReproductionCount);
            writer.WriteVarInt(ReproductionCountMax);
            writer.WriteVarShort(HarnessGID);
            writer.WriteUShort((ushort)EffectList.Length);
            foreach (var entry in EffectList)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Sex = BooleanByteWrapper.GetFlag(flag1, 0);
            IsRideable = BooleanByteWrapper.GetFlag(flag1, 1);
            IsWild = BooleanByteWrapper.GetFlag(flag1, 2);
            IsFecondationReady = BooleanByteWrapper.GetFlag(flag1, 3);
            UseHarnessColors = BooleanByteWrapper.GetFlag(flag1, 4);
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Model = reader.ReadVarUhInt();
            if (Model < 0)
                throw new Exception("Forbidden value on Model = " + Model + ", it doesn't respect the following condition : model < 0");
            var limit = reader.ReadUShort();
            Ancestor = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ancestor[i] = reader.ReadInt();
            }
            limit = reader.ReadUShort();
            Behaviors = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 Behaviors[i] = reader.ReadInt();
            }
            Name = reader.ReadUTF();
            OwnerId = reader.ReadInt();
            if (OwnerId < 0)
                throw new Exception("Forbidden value on OwnerId = " + OwnerId + ", it doesn't respect the following condition : ownerId < 0");
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            ExperienceForLevel = reader.ReadVarUhLong();
            if (ExperienceForLevel < 0 || ExperienceForLevel > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceForLevel = " + ExperienceForLevel + ", it doesn't respect the following condition : experienceForLevel < 0 || experienceForLevel > 9007199254740990");
            ExperienceForNextLevel = reader.ReadDouble();
            if (ExperienceForNextLevel < -9007199254740990 || ExperienceForNextLevel > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceForNextLevel = " + ExperienceForNextLevel + ", it doesn't respect the following condition : experienceForNextLevel < -9007199254740990 || experienceForNextLevel > 9007199254740990");
            Level = reader.ReadSByte();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
            MaxPods = reader.ReadVarUhInt();
            if (MaxPods < 0)
                throw new Exception("Forbidden value on MaxPods = " + MaxPods + ", it doesn't respect the following condition : maxPods < 0");
            Stamina = reader.ReadVarUhInt();
            if (Stamina < 0)
                throw new Exception("Forbidden value on Stamina = " + Stamina + ", it doesn't respect the following condition : stamina < 0");
            StaminaMax = reader.ReadVarUhInt();
            if (StaminaMax < 0)
                throw new Exception("Forbidden value on StaminaMax = " + StaminaMax + ", it doesn't respect the following condition : staminaMax < 0");
            Maturity = reader.ReadVarUhInt();
            if (Maturity < 0)
                throw new Exception("Forbidden value on Maturity = " + Maturity + ", it doesn't respect the following condition : maturity < 0");
            MaturityForAdult = reader.ReadVarUhInt();
            if (MaturityForAdult < 0)
                throw new Exception("Forbidden value on MaturityForAdult = " + MaturityForAdult + ", it doesn't respect the following condition : maturityForAdult < 0");
            Energy = reader.ReadVarUhInt();
            if (Energy < 0)
                throw new Exception("Forbidden value on Energy = " + Energy + ", it doesn't respect the following condition : energy < 0");
            EnergyMax = reader.ReadVarUhInt();
            if (EnergyMax < 0)
                throw new Exception("Forbidden value on EnergyMax = " + EnergyMax + ", it doesn't respect the following condition : energyMax < 0");
            Serenity = reader.ReadInt();
            AggressivityMax = reader.ReadInt();
            SerenityMax = reader.ReadVarUhInt();
            if (SerenityMax < 0)
                throw new Exception("Forbidden value on SerenityMax = " + SerenityMax + ", it doesn't respect the following condition : serenityMax < 0");
            Love = reader.ReadVarUhInt();
            if (Love < 0)
                throw new Exception("Forbidden value on Love = " + Love + ", it doesn't respect the following condition : love < 0");
            LoveMax = reader.ReadVarUhInt();
            if (LoveMax < 0)
                throw new Exception("Forbidden value on LoveMax = " + LoveMax + ", it doesn't respect the following condition : loveMax < 0");
            FecondationTime = reader.ReadInt();
            BoostLimiter = reader.ReadInt();
            if (BoostLimiter < 0)
                throw new Exception("Forbidden value on BoostLimiter = " + BoostLimiter + ", it doesn't respect the following condition : boostLimiter < 0");
            BoostMax = reader.ReadDouble();
            if (BoostMax < -9007199254740990 || BoostMax > 9007199254740990)
                throw new Exception("Forbidden value on BoostMax = " + BoostMax + ", it doesn't respect the following condition : boostMax < -9007199254740990 || boostMax > 9007199254740990");
            ReproductionCount = reader.ReadInt();
            ReproductionCountMax = reader.ReadVarUhInt();
            if (ReproductionCountMax < 0)
                throw new Exception("Forbidden value on ReproductionCountMax = " + ReproductionCountMax + ", it doesn't respect the following condition : reproductionCountMax < 0");
            HarnessGID = reader.ReadVarUhShort();
            if (HarnessGID < 0)
                throw new Exception("Forbidden value on HarnessGID = " + HarnessGID + ", it doesn't respect the following condition : harnessGID < 0");
            limit = reader.ReadUShort();
            EffectList = new Types.ObjectEffectInteger[limit];
            for (int i = 0; i < limit; i++)
            {
                 EffectList[i] = new Types.ObjectEffectInteger();
                 EffectList[i].Deserialize(reader);
            }
        }
        
    }
    
}