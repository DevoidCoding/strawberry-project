

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ItemDurability
    {
        public const short ProtocolId = 168;
        public virtual short TypeID => ProtocolId;
        
        public short Durability { get; set; }
        public short DurabilityMax { get; set; }
        
        public ItemDurability()
        {
        }
        
        public ItemDurability(short durability, short durabilityMax)
        {
            this.Durability = durability;
            this.DurabilityMax = durabilityMax;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Durability);
            writer.WriteShort(DurabilityMax);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Durability = reader.ReadShort();
            DurabilityMax = reader.ReadShort();
        }
        
    }
    
}