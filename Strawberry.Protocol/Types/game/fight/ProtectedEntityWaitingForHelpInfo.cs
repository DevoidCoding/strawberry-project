

// Generated on 02/12/2018 03:56:56
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class ProtectedEntityWaitingForHelpInfo
    {
        public const short ProtocolId = 186;
        public virtual short TypeID => ProtocolId;
        
        public int TimeLeftBeforeFight { get; set; }
        public int WaitTimeForPlacement { get; set; }
        public sbyte NbPositionForDefensors { get; set; }
        
        public ProtectedEntityWaitingForHelpInfo()
        {
        }
        
        public ProtectedEntityWaitingForHelpInfo(int timeLeftBeforeFight, int waitTimeForPlacement, sbyte nbPositionForDefensors)
        {
            this.TimeLeftBeforeFight = timeLeftBeforeFight;
            this.WaitTimeForPlacement = waitTimeForPlacement;
            this.NbPositionForDefensors = nbPositionForDefensors;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(TimeLeftBeforeFight);
            writer.WriteInt(WaitTimeForPlacement);
            writer.WriteSByte(NbPositionForDefensors);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            TimeLeftBeforeFight = reader.ReadInt();
            WaitTimeForPlacement = reader.ReadInt();
            NbPositionForDefensors = reader.ReadSByte();
            if (NbPositionForDefensors < 0)
                throw new Exception("Forbidden value on NbPositionForDefensors = " + NbPositionForDefensors + ", it doesn't respect the following condition : nbPositionForDefensors < 0");
        }
        
    }
    
}