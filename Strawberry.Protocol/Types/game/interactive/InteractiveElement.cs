

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class InteractiveElement
    {
        public const short ProtocolId = 80;
        public virtual short TypeID => ProtocolId;
        
        public int ElementId { get; set; }
        public int ElementTypeId { get; set; }
        public Types.InteractiveElementSkill[] EnabledSkills { get; set; }
        public Types.InteractiveElementSkill[] DisabledSkills { get; set; }
        public bool OnCurrentMap { get; set; }
        
        public InteractiveElement()
        {
        }
        
        public InteractiveElement(int elementId, int elementTypeId, Types.InteractiveElementSkill[] enabledSkills, Types.InteractiveElementSkill[] disabledSkills, bool onCurrentMap)
        {
            this.ElementId = elementId;
            this.ElementTypeId = elementTypeId;
            this.EnabledSkills = enabledSkills;
            this.DisabledSkills = disabledSkills;
            this.OnCurrentMap = onCurrentMap;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(ElementId);
            writer.WriteInt(ElementTypeId);
            writer.WriteUShort((ushort)EnabledSkills.Length);
            foreach (var entry in EnabledSkills)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)DisabledSkills.Length);
            foreach (var entry in DisabledSkills)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteBoolean(OnCurrentMap);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ElementId = reader.ReadInt();
            if (ElementId < 0)
                throw new Exception("Forbidden value on ElementId = " + ElementId + ", it doesn't respect the following condition : elementId < 0");
            ElementTypeId = reader.ReadInt();
            var limit = reader.ReadUShort();
            EnabledSkills = new Types.InteractiveElementSkill[limit];
            for (int i = 0; i < limit; i++)
            {
                 EnabledSkills[i] = Types.ProtocolTypeManager.GetInstance<Types.InteractiveElementSkill>(reader.ReadShort());
                 EnabledSkills[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            DisabledSkills = new Types.InteractiveElementSkill[limit];
            for (int i = 0; i < limit; i++)
            {
                 DisabledSkills[i] = Types.ProtocolTypeManager.GetInstance<Types.InteractiveElementSkill>(reader.ReadShort());
                 DisabledSkills[i].Deserialize(reader);
            }
            OnCurrentMap = reader.ReadBoolean();
        }
        
    }
    
}