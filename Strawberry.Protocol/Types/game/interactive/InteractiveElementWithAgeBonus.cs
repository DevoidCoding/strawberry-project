

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class InteractiveElementWithAgeBonus : InteractiveElement
    {
        public new const short ProtocolId = 398;
        public override short TypeID => ProtocolId;
        
        public short AgeBonus { get; set; }
        
        public InteractiveElementWithAgeBonus()
        {
        }
        
        public InteractiveElementWithAgeBonus(int elementId, int elementTypeId, Types.InteractiveElementSkill[] enabledSkills, Types.InteractiveElementSkill[] disabledSkills, bool onCurrentMap, short ageBonus)
         : base(elementId, elementTypeId, enabledSkills, disabledSkills, onCurrentMap)
        {
            this.AgeBonus = ageBonus;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(AgeBonus);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AgeBonus = reader.ReadShort();
            if (AgeBonus < -1 || AgeBonus > 1000)
                throw new Exception("Forbidden value on AgeBonus = " + AgeBonus + ", it doesn't respect the following condition : ageBonus < -1 || ageBonus > 1000");
        }
        
    }
    
}