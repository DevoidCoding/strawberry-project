

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class MapObstacle
    {
        public const short ProtocolId = 200;
        public virtual short TypeID => ProtocolId;
        
        public ushort ObstacleCellId { get; set; }
        public sbyte State { get; set; }
        
        public MapObstacle()
        {
        }
        
        public MapObstacle(ushort obstacleCellId, sbyte state)
        {
            this.ObstacleCellId = obstacleCellId;
            this.State = state;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ObstacleCellId);
            writer.WriteSByte(State);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ObstacleCellId = reader.ReadVarUhShort();
            if (ObstacleCellId < 0 || ObstacleCellId > 559)
                throw new Exception("Forbidden value on ObstacleCellId = " + ObstacleCellId + ", it doesn't respect the following condition : obstacleCellId < 0 || obstacleCellId > 559");
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
        }
        
    }
    
}