

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SkillActionDescriptionCraft : SkillActionDescription
    {
        public new const short ProtocolId = 100;
        public override short TypeID => ProtocolId;
        
        public sbyte Probability { get; set; }
        
        public SkillActionDescriptionCraft()
        {
        }
        
        public SkillActionDescriptionCraft(ushort skillId, sbyte probability)
         : base(skillId)
        {
            this.Probability = probability;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Probability);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Probability = reader.ReadSByte();
            if (Probability < 0)
                throw new Exception("Forbidden value on Probability = " + Probability + ", it doesn't respect the following condition : probability < 0");
        }
        
    }
    
}