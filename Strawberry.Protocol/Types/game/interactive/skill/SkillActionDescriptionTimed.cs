

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SkillActionDescriptionTimed : SkillActionDescription
    {
        public new const short ProtocolId = 103;
        public override short TypeID => ProtocolId;
        
        public byte Time { get; set; }
        
        public SkillActionDescriptionTimed()
        {
        }
        
        public SkillActionDescriptionTimed(ushort skillId, byte time)
         : base(skillId)
        {
            this.Time = time;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteByte(Time);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Time = reader.ReadByte();
            if (Time < 0 || Time > 255)
                throw new Exception("Forbidden value on Time = " + Time + ", it doesn't respect the following condition : time < 0 || time > 255");
        }
        
    }
    
}