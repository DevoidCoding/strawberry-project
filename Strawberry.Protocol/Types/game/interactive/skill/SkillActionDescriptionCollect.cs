

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SkillActionDescriptionCollect : SkillActionDescriptionTimed
    {
        public new const short ProtocolId = 99;
        public override short TypeID => ProtocolId;
        
        public ushort Min { get; set; }
        public ushort Max { get; set; }
        
        public SkillActionDescriptionCollect()
        {
        }
        
        public SkillActionDescriptionCollect(ushort skillId, byte time, ushort min, ushort max)
         : base(skillId, time)
        {
            this.Min = min;
            this.Max = max;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Min);
            writer.WriteVarShort(Max);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Min = reader.ReadVarUhShort();
            if (Min < 0)
                throw new Exception("Forbidden value on Min = " + Min + ", it doesn't respect the following condition : min < 0");
            Max = reader.ReadVarUhShort();
            if (Max < 0)
                throw new Exception("Forbidden value on Max = " + Max + ", it doesn't respect the following condition : max < 0");
        }
        
    }
    
}