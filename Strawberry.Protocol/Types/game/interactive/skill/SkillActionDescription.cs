

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class SkillActionDescription
    {
        public const short ProtocolId = 102;
        public virtual short TypeID => ProtocolId;
        
        public ushort SkillId { get; set; }
        
        public SkillActionDescription()
        {
        }
        
        public SkillActionDescription(ushort skillId)
        {
            this.SkillId = skillId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SkillId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SkillId = reader.ReadVarUhShort();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
        }
        
    }
    
}