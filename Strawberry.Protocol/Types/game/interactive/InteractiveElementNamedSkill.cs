

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class InteractiveElementNamedSkill : InteractiveElementSkill
    {
        public new const short ProtocolId = 220;
        public override short TypeID => ProtocolId;
        
        public uint NameId { get; set; }
        
        public InteractiveElementNamedSkill()
        {
        }
        
        public InteractiveElementNamedSkill(uint skillId, int skillInstanceUid, uint nameId)
         : base(skillId, skillInstanceUid)
        {
            this.NameId = nameId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(NameId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            NameId = reader.ReadVarUhInt();
            if (NameId < 0)
                throw new Exception("Forbidden value on NameId = " + NameId + ", it doesn't respect the following condition : nameId < 0");
        }
        
    }
    
}