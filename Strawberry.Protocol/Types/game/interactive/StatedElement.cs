

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatedElement
    {
        public const short ProtocolId = 108;
        public virtual short TypeID => ProtocolId;
        
        public int ElementId { get; set; }
        public ushort ElementCellId { get; set; }
        public uint ElementState { get; set; }
        public bool OnCurrentMap { get; set; }
        
        public StatedElement()
        {
        }
        
        public StatedElement(int elementId, ushort elementCellId, uint elementState, bool onCurrentMap)
        {
            this.ElementId = elementId;
            this.ElementCellId = elementCellId;
            this.ElementState = elementState;
            this.OnCurrentMap = onCurrentMap;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(ElementId);
            writer.WriteVarShort(ElementCellId);
            writer.WriteVarInt(ElementState);
            writer.WriteBoolean(OnCurrentMap);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            ElementId = reader.ReadInt();
            if (ElementId < 0)
                throw new Exception("Forbidden value on ElementId = " + ElementId + ", it doesn't respect the following condition : elementId < 0");
            ElementCellId = reader.ReadVarUhShort();
            if (ElementCellId < 0 || ElementCellId > 559)
                throw new Exception("Forbidden value on ElementCellId = " + ElementCellId + ", it doesn't respect the following condition : elementCellId < 0 || elementCellId > 559");
            ElementState = reader.ReadVarUhInt();
            if (ElementState < 0)
                throw new Exception("Forbidden value on ElementState = " + ElementState + ", it doesn't respect the following condition : elementState < 0");
            OnCurrentMap = reader.ReadBoolean();
        }
        
    }
    
}