

// Generated on 02/12/2018 03:56:57
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class InteractiveElementSkill
    {
        public const short ProtocolId = 219;
        public virtual short TypeID => ProtocolId;
        
        public uint SkillId { get; set; }
        public int SkillInstanceUid { get; set; }
        
        public InteractiveElementSkill()
        {
        }
        
        public InteractiveElementSkill(uint skillId, int skillInstanceUid)
        {
            this.SkillId = skillId;
            this.SkillInstanceUid = skillInstanceUid;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(SkillId);
            writer.WriteInt(SkillInstanceUid);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            SkillId = reader.ReadVarUhInt();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
            SkillInstanceUid = reader.ReadInt();
            if (SkillInstanceUid < 0)
                throw new Exception("Forbidden value on SkillInstanceUid = " + SkillInstanceUid + ", it doesn't respect the following condition : skillInstanceUid < 0");
        }
        
    }
    
}