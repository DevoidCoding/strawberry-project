

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StartupActionAddObject
    {
        public const short ProtocolId = 52;
        public virtual short TypeID => ProtocolId;
        
        public int Uid { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string DescUrl { get; set; }
        public string PictureUrl { get; set; }
        public Types.ObjectItemInformationWithQuantity[] Items { get; set; }
        
        public StartupActionAddObject()
        {
        }
        
        public StartupActionAddObject(int uid, string title, string text, string descUrl, string pictureUrl, Types.ObjectItemInformationWithQuantity[] items)
        {
            this.Uid = uid;
            this.Title = title;
            this.Text = text;
            this.DescUrl = descUrl;
            this.PictureUrl = pictureUrl;
            this.Items = items;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Uid);
            writer.WriteUTF(Title);
            writer.WriteUTF(Text);
            writer.WriteUTF(DescUrl);
            writer.WriteUTF(PictureUrl);
            writer.WriteUShort((ushort)Items.Length);
            foreach (var entry in Items)
            {
                 entry.Serialize(writer);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Uid = reader.ReadInt();
            if (Uid < 0)
                throw new Exception("Forbidden value on Uid = " + Uid + ", it doesn't respect the following condition : uid < 0");
            Title = reader.ReadUTF();
            Text = reader.ReadUTF();
            DescUrl = reader.ReadUTF();
            PictureUrl = reader.ReadUTF();
            var limit = reader.ReadUShort();
            Items = new Types.ObjectItemInformationWithQuantity[limit];
            for (int i = 0; i < limit; i++)
            {
                 Items[i] = new Types.ObjectItemInformationWithQuantity();
                 Items[i].Deserialize(reader);
            }
        }
        
    }
    
}