

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DareInformations
    {
        public const short ProtocolId = 502;
        public virtual short TypeID => ProtocolId;
        
        public double DareId { get; set; }
        public Types.CharacterBasicMinimalInformations Creator { get; set; }
        public ulong SubscriptionFee { get; set; }
        public ulong Jackpot { get; set; }
        public ushort MaxCountWinners { get; set; }
        public double EndDate { get; set; }
        public bool IsPrivate { get; set; }
        public uint GuildId { get; set; }
        public uint AllianceId { get; set; }
        public Types.DareCriteria[] Criterions { get; set; }
        public double StartDate { get; set; }
        
        public DareInformations()
        {
        }
        
        public DareInformations(double dareId, Types.CharacterBasicMinimalInformations creator, ulong subscriptionFee, ulong jackpot, ushort maxCountWinners, double endDate, bool isPrivate, uint guildId, uint allianceId, Types.DareCriteria[] criterions, double startDate)
        {
            this.DareId = dareId;
            this.Creator = creator;
            this.SubscriptionFee = subscriptionFee;
            this.Jackpot = jackpot;
            this.MaxCountWinners = maxCountWinners;
            this.EndDate = endDate;
            this.IsPrivate = isPrivate;
            this.GuildId = guildId;
            this.AllianceId = allianceId;
            this.Criterions = criterions;
            this.StartDate = startDate;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DareId);
            Creator.Serialize(writer);
            writer.WriteVarLong(SubscriptionFee);
            writer.WriteVarLong(Jackpot);
            writer.WriteUShort(MaxCountWinners);
            writer.WriteDouble(EndDate);
            writer.WriteBoolean(IsPrivate);
            writer.WriteVarInt(GuildId);
            writer.WriteVarInt(AllianceId);
            writer.WriteUShort((ushort)Criterions.Length);
            foreach (var entry in Criterions)
            {
                 entry.Serialize(writer);
            }
            writer.WriteDouble(StartDate);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
            Creator = new Types.CharacterBasicMinimalInformations();
            Creator.Deserialize(reader);
            SubscriptionFee = reader.ReadVarUhLong();
            if (SubscriptionFee < 0 || SubscriptionFee > 9007199254740990)
                throw new Exception("Forbidden value on SubscriptionFee = " + SubscriptionFee + ", it doesn't respect the following condition : subscriptionFee < 0 || subscriptionFee > 9007199254740990");
            Jackpot = reader.ReadVarUhLong();
            if (Jackpot < 0 || Jackpot > 9007199254740990)
                throw new Exception("Forbidden value on Jackpot = " + Jackpot + ", it doesn't respect the following condition : jackpot < 0 || jackpot > 9007199254740990");
            MaxCountWinners = reader.ReadUShort();
            if (MaxCountWinners < 0 || MaxCountWinners > 65535)
                throw new Exception("Forbidden value on MaxCountWinners = " + MaxCountWinners + ", it doesn't respect the following condition : maxCountWinners < 0 || maxCountWinners > 65535");
            EndDate = reader.ReadDouble();
            if (EndDate < 0 || EndDate > 9007199254740990)
                throw new Exception("Forbidden value on EndDate = " + EndDate + ", it doesn't respect the following condition : endDate < 0 || endDate > 9007199254740990");
            IsPrivate = reader.ReadBoolean();
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
            AllianceId = reader.ReadVarUhInt();
            if (AllianceId < 0)
                throw new Exception("Forbidden value on AllianceId = " + AllianceId + ", it doesn't respect the following condition : allianceId < 0");
            var limit = reader.ReadUShort();
            Criterions = new Types.DareCriteria[limit];
            for (int i = 0; i < limit; i++)
            {
                 Criterions[i] = new Types.DareCriteria();
                 Criterions[i].Deserialize(reader);
            }
            StartDate = reader.ReadDouble();
            if (StartDate < 0 || StartDate > 9007199254740990)
                throw new Exception("Forbidden value on StartDate = " + StartDate + ", it doesn't respect the following condition : startDate < 0 || startDate > 9007199254740990");
        }
        
    }
    
}