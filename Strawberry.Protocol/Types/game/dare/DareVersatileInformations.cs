

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DareVersatileInformations
    {
        public const short ProtocolId = 504;
        public virtual short TypeID => ProtocolId;
        
        public double DareId { get; set; }
        public int CountEntrants { get; set; }
        public int CountWinners { get; set; }
        
        public DareVersatileInformations()
        {
        }
        
        public DareVersatileInformations(double dareId, int countEntrants, int countWinners)
        {
            this.DareId = dareId;
            this.CountEntrants = countEntrants;
            this.CountWinners = countWinners;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DareId);
            writer.WriteInt(CountEntrants);
            writer.WriteInt(CountWinners);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
            CountEntrants = reader.ReadInt();
            if (CountEntrants < 0)
                throw new Exception("Forbidden value on CountEntrants = " + CountEntrants + ", it doesn't respect the following condition : countEntrants < 0");
            CountWinners = reader.ReadInt();
            if (CountWinners < 0)
                throw new Exception("Forbidden value on CountWinners = " + CountWinners + ", it doesn't respect the following condition : countWinners < 0");
        }
        
    }
    
}