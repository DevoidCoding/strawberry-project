

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DareCriteria
    {
        public const short ProtocolId = 501;
        public virtual short TypeID => ProtocolId;
        
        public sbyte Type { get; set; }
        public int[] Params { get; set; }
        
        public DareCriteria()
        {
        }
        
        public DareCriteria(sbyte type, int[] @params)
        {
            this.Type = type;
            this.Params = @params;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
            writer.WriteUShort((ushort)Params.Length);
            foreach (var entry in Params)
            {
                 writer.WriteInt(entry);
            }
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            var limit = reader.ReadUShort();
            Params = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 Params[i] = reader.ReadInt();
            }
        }
        
    }
    
}