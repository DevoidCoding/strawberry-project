

// Generated on 02/12/2018 03:56:55
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class DareReward
    {
        public const short ProtocolId = 505;
        public virtual short TypeID => ProtocolId;
        
        public sbyte Type { get; set; }
        public ushort MonsterId { get; set; }
        public ulong Kamas { get; set; }
        public double DareId { get; set; }
        
        public DareReward()
        {
        }
        
        public DareReward(sbyte type, ushort monsterId, ulong kamas, double dareId)
        {
            this.Type = type;
            this.MonsterId = monsterId;
            this.Kamas = kamas;
            this.DareId = dareId;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
            writer.WriteVarShort(MonsterId);
            writer.WriteVarLong(Kamas);
            writer.WriteDouble(DareId);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            MonsterId = reader.ReadVarUhShort();
            if (MonsterId < 0)
                throw new Exception("Forbidden value on MonsterId = " + MonsterId + ", it doesn't respect the following condition : monsterId < 0");
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
        }
        
    }
    
}