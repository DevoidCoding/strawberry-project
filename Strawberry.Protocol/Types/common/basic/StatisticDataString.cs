

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatisticDataString : StatisticData
    {
        public new const short ProtocolId = 487;
        public override short TypeID => ProtocolId;
        
        public string Value { get; set; }
        
        public StatisticDataString()
        {
        }
        
        public StatisticDataString(string value)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadUTF();
        }
        
    }
    
}