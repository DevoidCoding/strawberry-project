

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatisticDataByte : StatisticData
    {
        public new const short ProtocolId = 486;
        public override short TypeID => ProtocolId;
        
        public sbyte Value { get; set; }
        
        public StatisticDataByte()
        {
        }
        
        public StatisticDataByte(sbyte value)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadSByte();
        }
        
    }
    
}