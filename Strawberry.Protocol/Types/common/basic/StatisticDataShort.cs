

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatisticDataShort : StatisticData
    {
        public new const short ProtocolId = 488;
        public override short TypeID => ProtocolId;
        
        public short Value { get; set; }
        
        public StatisticDataShort()
        {
        }
        
        public StatisticDataShort(short value)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadShort();
        }
        
    }
    
}