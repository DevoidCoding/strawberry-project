

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatisticDataboolean : StatisticData
    {
        public new const short ProtocolId = 482;
        public override short TypeID => ProtocolId;
        
        public bool Value { get; set; }
        
        public StatisticDataboolean()
        {
        }
        
        public StatisticDataboolean(bool value)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadBoolean();
        }
        
    }
    
}