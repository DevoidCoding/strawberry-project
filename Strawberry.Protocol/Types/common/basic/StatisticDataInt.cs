

// Generated on 02/12/2018 03:56:49
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class StatisticDataInt : StatisticData
    {
        public new const short ProtocolId = 485;
        public override short TypeID => ProtocolId;
        
        public int Value { get; set; }
        
        public StatisticDataInt()
        {
        }
        
        public StatisticDataInt(int value)
        {
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Value = reader.ReadInt();
        }
        
    }
    
}