

// Generated on 02/12/2018 03:56:58
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Types
{
    public class TrustCertificate
    {
        public const short ProtocolId = 377;
        public virtual short TypeID => ProtocolId;
        
        public int Id { get; set; }
        public string Hash { get; set; }
        
        public TrustCertificate()
        {
        }
        
        public TrustCertificate(int id, string hash)
        {
            this.Id = id;
            this.Hash = hash;
        }
        
        public virtual void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Id);
            writer.WriteUTF(Hash);
        }
        
        public virtual void Deserialize(IDataReader reader)
        {
            Id = reader.ReadInt();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            Hash = reader.ReadUTF();
        }
        
    }
    
}