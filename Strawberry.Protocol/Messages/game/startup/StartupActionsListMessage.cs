

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StartupActionsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 1301;
        public override uint MessageID => ProtocolId;
        
        public Types.StartupActionAddObject[] Actions { get; set; }
        
        public StartupActionsListMessage()
        {
        }
        
        public StartupActionsListMessage(Types.StartupActionAddObject[] actions)
        {
            this.Actions = actions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Actions.Length);
            foreach (var entry in Actions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Actions = new Types.StartupActionAddObject[limit];
            for (int i = 0; i < limit; i++)
            {
                 Actions[i] = new Types.StartupActionAddObject();
                 Actions[i].Deserialize(reader);
            }
        }
        
    }
    
}