

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StartupActionsObjetAttributionMessage : NetworkMessage
    {
        public const uint ProtocolId = 1303;
        public override uint MessageID => ProtocolId;
        
        public int ActionId { get; set; }
        public ulong CharacterId { get; set; }
        
        public StartupActionsObjetAttributionMessage()
        {
        }
        
        public StartupActionsObjetAttributionMessage(int actionId, ulong characterId)
        {
            this.ActionId = actionId;
            this.CharacterId = characterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(ActionId);
            writer.WriteVarLong(CharacterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ActionId = reader.ReadInt();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
            CharacterId = reader.ReadVarUhLong();
            if (CharacterId < 0 || CharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CharacterId = " + CharacterId + ", it doesn't respect the following condition : characterId < 0 || characterId > 9007199254740990");
        }
        
    }
    
}