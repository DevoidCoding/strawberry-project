

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StartupActionAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 6538;
        public override uint MessageID => ProtocolId;
        
        public Types.StartupActionAddObject NewAction { get; set; }
        
        public StartupActionAddMessage()
        {
        }
        
        public StartupActionAddMessage(Types.StartupActionAddObject newAction)
        {
            this.NewAction = newAction;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            NewAction.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NewAction = new Types.StartupActionAddObject();
            NewAction.Deserialize(reader);
        }
        
    }
    
}