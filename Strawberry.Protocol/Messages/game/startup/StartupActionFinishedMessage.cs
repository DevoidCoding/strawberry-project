

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StartupActionFinishedMessage : NetworkMessage
    {
        public const uint ProtocolId = 1304;
        public override uint MessageID => ProtocolId;
        
        public bool Success { get; set; }
        public bool AutomaticAction { get; set; }
        public int ActionId { get; set; }
        
        public StartupActionFinishedMessage()
        {
        }
        
        public StartupActionFinishedMessage(bool success, bool automaticAction, int actionId)
        {
            this.Success = success;
            this.AutomaticAction = automaticAction;
            this.ActionId = actionId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Success);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, AutomaticAction);
            writer.WriteByte(flag1);
            writer.WriteInt(ActionId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag1, 0);
            AutomaticAction = BooleanByteWrapper.GetFlag(flag1, 1);
            ActionId = reader.ReadInt();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
        }
        
    }
    
}