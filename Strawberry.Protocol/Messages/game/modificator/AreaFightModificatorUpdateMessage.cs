

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AreaFightModificatorUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6493;
        public override uint MessageID => ProtocolId;
        
        public int SpellPairId { get; set; }
        
        public AreaFightModificatorUpdateMessage()
        {
        }
        
        public AreaFightModificatorUpdateMessage(int spellPairId)
        {
            this.SpellPairId = spellPairId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(SpellPairId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellPairId = reader.ReadInt();
        }
        
    }
    
}