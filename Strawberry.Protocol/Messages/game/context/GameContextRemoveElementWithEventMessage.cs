

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextRemoveElementWithEventMessage : GameContextRemoveElementMessage
    {
        public new const uint ProtocolId = 6412;
        public override uint MessageID => ProtocolId;
        
        public sbyte ElementEventId { get; set; }
        
        public GameContextRemoveElementWithEventMessage()
        {
        }
        
        public GameContextRemoveElementWithEventMessage(double id, sbyte elementEventId)
         : base(id)
        {
            this.ElementEventId = elementEventId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(ElementEventId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ElementEventId = reader.ReadSByte();
            if (ElementEventId < 0)
                throw new Exception("Forbidden value on ElementEventId = " + ElementEventId + ", it doesn't respect the following condition : elementEventId < 0");
        }
        
    }
    
}