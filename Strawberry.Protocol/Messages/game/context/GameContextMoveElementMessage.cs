

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextMoveElementMessage : NetworkMessage
    {
        public const uint ProtocolId = 253;
        public override uint MessageID => ProtocolId;
        
        public Types.EntityMovementInformations Movement { get; set; }
        
        public GameContextMoveElementMessage()
        {
        }
        
        public GameContextMoveElementMessage(Types.EntityMovementInformations movement)
        {
            this.Movement = movement;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Movement.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Movement = new Types.EntityMovementInformations();
            Movement.Deserialize(reader);
        }
        
    }
    
}