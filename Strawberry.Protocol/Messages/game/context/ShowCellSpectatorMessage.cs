

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShowCellSpectatorMessage : ShowCellMessage
    {
        public new const uint ProtocolId = 6158;
        public override uint MessageID => ProtocolId;
        
        public string PlayerName { get; set; }
        
        public ShowCellSpectatorMessage()
        {
        }
        
        public ShowCellSpectatorMessage(double sourceId, ushort cellId, string playerName)
         : base(sourceId, cellId)
        {
            this.PlayerName = playerName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(PlayerName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PlayerName = reader.ReadUTF();
        }
        
    }
    
}