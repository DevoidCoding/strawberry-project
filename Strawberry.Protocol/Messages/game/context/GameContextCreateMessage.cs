

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextCreateMessage : NetworkMessage
    {
        public const uint ProtocolId = 200;
        public override uint MessageID => ProtocolId;
        
        public sbyte Context { get; set; }
        
        public GameContextCreateMessage()
        {
        }
        
        public GameContextCreateMessage(sbyte context)
        {
            this.Context = context;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Context);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Context = reader.ReadSByte();
            if (Context < 0)
                throw new Exception("Forbidden value on Context = " + Context + ", it doesn't respect the following condition : context < 0");
        }
        
    }
    
}