

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NotificationUpdateFlagMessage : NetworkMessage
    {
        public const uint ProtocolId = 6090;
        public override uint MessageID => ProtocolId;
        
        public ushort Index { get; set; }
        
        public NotificationUpdateFlagMessage()
        {
        }
        
        public NotificationUpdateFlagMessage(ushort index)
        {
            this.Index = index;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Index);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Index = reader.ReadVarUhShort();
            if (Index < 0)
                throw new Exception("Forbidden value on Index = " + Index + ", it doesn't respect the following condition : index < 0");
        }
        
    }
    
}