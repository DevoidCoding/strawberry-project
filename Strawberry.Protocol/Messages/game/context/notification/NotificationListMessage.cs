

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NotificationListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6087;
        public override uint MessageID => ProtocolId;
        
        public int[] Flags { get; set; }
        
        public NotificationListMessage()
        {
        }
        
        public NotificationListMessage(int[] flags)
        {
            this.Flags = flags;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Flags.Length);
            foreach (var entry in Flags)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Flags = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 Flags[i] = reader.ReadVarInt();
            }
        }
        
    }
    
}