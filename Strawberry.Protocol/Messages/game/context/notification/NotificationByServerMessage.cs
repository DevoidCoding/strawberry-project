

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NotificationByServerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6103;
        public override uint MessageID => ProtocolId;
        
        public ushort Id { get; set; }
        public string[] Parameters { get; set; }
        public bool ForceOpen { get; set; }
        
        public NotificationByServerMessage()
        {
        }
        
        public NotificationByServerMessage(ushort id, string[] parameters, bool forceOpen)
        {
            this.Id = id;
            this.Parameters = parameters;
            this.ForceOpen = forceOpen;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
            writer.WriteUShort((ushort)Parameters.Length);
            foreach (var entry in Parameters)
            {
                 writer.WriteUTF(entry);
            }
            writer.WriteBoolean(ForceOpen);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            var limit = reader.ReadUShort();
            Parameters = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Parameters[i] = reader.ReadUTF();
            }
            ForceOpen = reader.ReadBoolean();
        }
        
    }
    
}