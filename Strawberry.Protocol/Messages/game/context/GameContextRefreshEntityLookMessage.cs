

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextRefreshEntityLookMessage : NetworkMessage
    {
        public const uint ProtocolId = 5637;
        public override uint MessageID => ProtocolId;
        
        public double Id { get; set; }
        public Types.EntityLook Look { get; set; }
        
        public GameContextRefreshEntityLookMessage()
        {
        }
        
        public GameContextRefreshEntityLookMessage(double id, Types.EntityLook look)
        {
            this.Id = id;
            this.Look = look;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Id);
            Look.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
        }
        
    }
    
}