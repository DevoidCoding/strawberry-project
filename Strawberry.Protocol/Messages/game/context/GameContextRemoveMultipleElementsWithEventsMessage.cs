

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextRemoveMultipleElementsWithEventsMessage : GameContextRemoveMultipleElementsMessage
    {
        public new const uint ProtocolId = 6416;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] ElementEventIds { get; set; }
        
        public GameContextRemoveMultipleElementsWithEventsMessage()
        {
        }
        
        public GameContextRemoveMultipleElementsWithEventsMessage(double[] elementsIds, sbyte[] elementEventIds)
         : base(elementsIds)
        {
            this.ElementEventIds = elementEventIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)ElementEventIds.Length);
            foreach (var entry in ElementEventIds)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            ElementEventIds = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 ElementEventIds[i] = reader.ReadSByte();
            }
        }
        
    }
    
}