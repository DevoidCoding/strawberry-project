

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameMapMovementMessage : NetworkMessage
    {
        public const uint ProtocolId = 951;
        public override uint MessageID => ProtocolId;
        
        public short[] KeyMovements { get; set; }
        public short ForcedDirection { get; set; }
        public double ActorId { get; set; }
        
        public GameMapMovementMessage()
        {
        }
        
        public GameMapMovementMessage(short[] keyMovements, short forcedDirection, double actorId)
        {
            this.KeyMovements = keyMovements;
            this.ForcedDirection = forcedDirection;
            this.ActorId = actorId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)KeyMovements.Length);
            foreach (var entry in KeyMovements)
            {
                 writer.WriteShort(entry);
            }
            writer.WriteShort(ForcedDirection);
            writer.WriteDouble(ActorId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            KeyMovements = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 KeyMovements[i] = reader.ReadShort();
            }
            ForcedDirection = reader.ReadShort();
            ActorId = reader.ReadDouble();
            if (ActorId < -9007199254740990 || ActorId > 9007199254740990)
                throw new Exception("Forbidden value on ActorId = " + ActorId + ", it doesn't respect the following condition : actorId < -9007199254740990 || actorId > 9007199254740990");
        }
        
    }
    
}