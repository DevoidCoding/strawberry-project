

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameMapNoMovementMessage : NetworkMessage
    {
        public const uint ProtocolId = 954;
        public override uint MessageID => ProtocolId;
        
        public short CellX { get; set; }
        public short CellY { get; set; }
        
        public GameMapNoMovementMessage()
        {
        }
        
        public GameMapNoMovementMessage(short cellX, short cellY)
        {
            this.CellX = cellX;
            this.CellY = cellY;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(CellX);
            writer.WriteShort(CellY);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CellX = reader.ReadShort();
            CellY = reader.ReadShort();
        }
        
    }
    
}