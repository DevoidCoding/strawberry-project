

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextMoveMultipleElementsMessage : NetworkMessage
    {
        public const uint ProtocolId = 254;
        public override uint MessageID => ProtocolId;
        
        public Types.EntityMovementInformations[] Movements { get; set; }
        
        public GameContextMoveMultipleElementsMessage()
        {
        }
        
        public GameContextMoveMultipleElementsMessage(Types.EntityMovementInformations[] movements)
        {
            this.Movements = movements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Movements.Length);
            foreach (var entry in Movements)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Movements = new Types.EntityMovementInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Movements[i] = new Types.EntityMovementInformations();
                 Movements[i].Deserialize(reader);
            }
        }
        
    }
    
}