

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRefreshMonsterBoostsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6618;
        public override uint MessageID => ProtocolId;
        
        public Types.MonsterBoosts[] MonsterBoosts { get; set; }
        public Types.MonsterBoosts[] FamilyBoosts { get; set; }
        
        public GameRefreshMonsterBoostsMessage()
        {
        }
        
        public GameRefreshMonsterBoostsMessage(Types.MonsterBoosts[] monsterBoosts, Types.MonsterBoosts[] familyBoosts)
        {
            this.MonsterBoosts = monsterBoosts;
            this.FamilyBoosts = familyBoosts;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)MonsterBoosts.Length);
            foreach (var entry in MonsterBoosts)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)FamilyBoosts.Length);
            foreach (var entry in FamilyBoosts)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            MonsterBoosts = new Types.MonsterBoosts[limit];
            for (int i = 0; i < limit; i++)
            {
                 MonsterBoosts[i] = new Types.MonsterBoosts();
                 MonsterBoosts[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            FamilyBoosts = new Types.MonsterBoosts[limit];
            for (int i = 0; i < limit; i++)
            {
                 FamilyBoosts[i] = new Types.MonsterBoosts();
                 FamilyBoosts[i].Deserialize(reader);
            }
        }
        
    }
    
}