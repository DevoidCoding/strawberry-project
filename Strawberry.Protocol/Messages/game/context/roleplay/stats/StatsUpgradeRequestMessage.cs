

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StatsUpgradeRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5610;
        public override uint MessageID => ProtocolId;
        
        public bool UseAdditionnal { get; set; }
        public sbyte StatId { get; set; }
        public ushort BoostPoint { get; set; }
        
        public StatsUpgradeRequestMessage()
        {
        }
        
        public StatsUpgradeRequestMessage(bool useAdditionnal, sbyte statId, ushort boostPoint)
        {
            this.UseAdditionnal = useAdditionnal;
            this.StatId = statId;
            this.BoostPoint = boostPoint;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(UseAdditionnal);
            writer.WriteSByte(StatId);
            writer.WriteVarShort(BoostPoint);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            UseAdditionnal = reader.ReadBoolean();
            StatId = reader.ReadSByte();
            if (StatId < 0)
                throw new Exception("Forbidden value on StatId = " + StatId + ", it doesn't respect the following condition : statId < 0");
            BoostPoint = reader.ReadVarUhShort();
            if (BoostPoint < 0)
                throw new Exception("Forbidden value on BoostPoint = " + BoostPoint + ", it doesn't respect the following condition : boostPoint < 0");
        }
        
    }
    
}