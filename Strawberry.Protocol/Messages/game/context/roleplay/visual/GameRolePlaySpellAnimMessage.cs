

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlaySpellAnimMessage : NetworkMessage
    {
        public const uint ProtocolId = 6114;
        public override uint MessageID => ProtocolId;
        
        public ulong CasterId { get; set; }
        public ushort TargetCellId { get; set; }
        public ushort SpellId { get; set; }
        public short SpellLevel { get; set; }
        
        public GameRolePlaySpellAnimMessage()
        {
        }
        
        public GameRolePlaySpellAnimMessage(ulong casterId, ushort targetCellId, ushort spellId, short spellLevel)
        {
            this.CasterId = casterId;
            this.TargetCellId = targetCellId;
            this.SpellId = spellId;
            this.SpellLevel = spellLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(CasterId);
            writer.WriteVarShort(TargetCellId);
            writer.WriteVarShort(SpellId);
            writer.WriteShort(SpellLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CasterId = reader.ReadVarUhLong();
            if (CasterId < 0 || CasterId > 9007199254740990)
                throw new Exception("Forbidden value on CasterId = " + CasterId + ", it doesn't respect the following condition : casterId < 0 || casterId > 9007199254740990");
            TargetCellId = reader.ReadVarUhShort();
            if (TargetCellId < 0 || TargetCellId > 559)
                throw new Exception("Forbidden value on TargetCellId = " + TargetCellId + ", it doesn't respect the following condition : targetCellId < 0 || targetCellId > 559");
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            SpellLevel = reader.ReadShort();
            if (SpellLevel < 1 || SpellLevel > 200)
                throw new Exception("Forbidden value on SpellLevel = " + SpellLevel + ", it doesn't respect the following condition : spellLevel < 1 || spellLevel > 200");
        }
        
    }
    
}