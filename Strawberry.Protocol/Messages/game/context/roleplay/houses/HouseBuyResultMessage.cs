

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseBuyResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5735;
        public override uint MessageID => ProtocolId;
        
        public bool SecondHand { get; set; }
        public bool Bought { get; set; }
        public uint HouseId { get; set; }
        public int InstanceId { get; set; }
        public ulong RealPrice { get; set; }
        
        public HouseBuyResultMessage()
        {
        }
        
        public HouseBuyResultMessage(bool secondHand, bool bought, uint houseId, int instanceId, ulong realPrice)
        {
            this.SecondHand = secondHand;
            this.Bought = bought;
            this.HouseId = houseId;
            this.InstanceId = instanceId;
            this.RealPrice = realPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, SecondHand);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Bought);
            writer.WriteByte(flag1);
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteVarLong(RealPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            SecondHand = BooleanByteWrapper.GetFlag(flag1, 0);
            Bought = BooleanByteWrapper.GetFlag(flag1, 1);
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            RealPrice = reader.ReadVarUhLong();
            if (RealPrice < 0 || RealPrice > 9007199254740990)
                throw new Exception("Forbidden value on RealPrice = " + RealPrice + ", it doesn't respect the following condition : realPrice < 0 || realPrice > 9007199254740990");
        }
        
    }
    
}