

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseSellRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5697;
        public override uint MessageID => ProtocolId;
        
        public int InstanceId { get; set; }
        public ulong Amount { get; set; }
        public bool ForSale { get; set; }
        
        public HouseSellRequestMessage()
        {
        }
        
        public HouseSellRequestMessage(int instanceId, ulong amount, bool forSale)
        {
            this.InstanceId = instanceId;
            this.Amount = amount;
            this.ForSale = forSale;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(InstanceId);
            writer.WriteVarLong(Amount);
            writer.WriteBoolean(ForSale);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            Amount = reader.ReadVarUhLong();
            if (Amount < 0 || Amount > 9007199254740990)
                throw new Exception("Forbidden value on Amount = " + Amount + ", it doesn't respect the following condition : amount < 0 || amount > 9007199254740990");
            ForSale = reader.ReadBoolean();
        }
        
    }
    
}