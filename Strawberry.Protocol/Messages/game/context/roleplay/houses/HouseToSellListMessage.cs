

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseToSellListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6140;
        public override uint MessageID => ProtocolId;
        
        public ushort PageIndex { get; set; }
        public ushort TotalPage { get; set; }
        public Types.HouseInformationsForSell[] HouseList { get; set; }
        
        public HouseToSellListMessage()
        {
        }
        
        public HouseToSellListMessage(ushort pageIndex, ushort totalPage, Types.HouseInformationsForSell[] houseList)
        {
            this.PageIndex = pageIndex;
            this.TotalPage = totalPage;
            this.HouseList = houseList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(PageIndex);
            writer.WriteVarShort(TotalPage);
            writer.WriteUShort((ushort)HouseList.Length);
            foreach (var entry in HouseList)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PageIndex = reader.ReadVarUhShort();
            if (PageIndex < 0)
                throw new Exception("Forbidden value on PageIndex = " + PageIndex + ", it doesn't respect the following condition : pageIndex < 0");
            TotalPage = reader.ReadVarUhShort();
            if (TotalPage < 0)
                throw new Exception("Forbidden value on TotalPage = " + TotalPage + ", it doesn't respect the following condition : totalPage < 0");
            var limit = reader.ReadUShort();
            HouseList = new Types.HouseInformationsForSell[limit];
            for (int i = 0; i < limit; i++)
            {
                 HouseList[i] = new Types.HouseInformationsForSell();
                 HouseList[i].Deserialize(reader);
            }
        }
        
    }
    
}