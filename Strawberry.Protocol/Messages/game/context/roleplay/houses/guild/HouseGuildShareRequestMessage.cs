

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseGuildShareRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5704;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int InstanceId { get; set; }
        public bool Enable { get; set; }
        public uint Rights { get; set; }
        
        public HouseGuildShareRequestMessage()
        {
        }
        
        public HouseGuildShareRequestMessage(uint houseId, int instanceId, bool enable, uint rights)
        {
            this.HouseId = houseId;
            this.InstanceId = instanceId;
            this.Enable = enable;
            this.Rights = rights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(Enable);
            writer.WriteVarInt(Rights);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            Enable = reader.ReadBoolean();
            Rights = reader.ReadVarUhInt();
            if (Rights < 0)
                throw new Exception("Forbidden value on Rights = " + Rights + ", it doesn't respect the following condition : rights < 0");
        }
        
    }
    
}