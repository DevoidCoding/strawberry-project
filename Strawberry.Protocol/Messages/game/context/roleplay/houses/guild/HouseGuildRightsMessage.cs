

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseGuildRightsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5703;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int InstanceId { get; set; }
        public bool SecondHand { get; set; }
        public Types.GuildInformations GuildInfo { get; set; }
        public uint Rights { get; set; }
        
        public HouseGuildRightsMessage()
        {
        }
        
        public HouseGuildRightsMessage(uint houseId, int instanceId, bool secondHand, Types.GuildInformations guildInfo, uint rights)
        {
            this.HouseId = houseId;
            this.InstanceId = instanceId;
            this.SecondHand = secondHand;
            this.GuildInfo = guildInfo;
            this.Rights = rights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            GuildInfo.Serialize(writer);
            writer.WriteVarInt(Rights);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            SecondHand = reader.ReadBoolean();
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
            Rights = reader.ReadVarUhInt();
            if (Rights < 0)
                throw new Exception("Forbidden value on Rights = " + Rights + ", it doesn't respect the following condition : rights < 0");
        }
        
    }
    
}