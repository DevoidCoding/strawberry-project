

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseGuildRightsViewMessage : NetworkMessage
    {
        public const uint ProtocolId = 5700;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int InstanceId { get; set; }
        
        public HouseGuildRightsViewMessage()
        {
        }
        
        public HouseGuildRightsViewMessage(uint houseId, int instanceId)
        {
            this.HouseId = houseId;
            this.InstanceId = instanceId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
        }
        
    }
    
}