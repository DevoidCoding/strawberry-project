

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseSellingUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6727;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int InstanceId { get; set; }
        public bool SecondHand { get; set; }
        public ulong RealPrice { get; set; }
        public string BuyerName { get; set; }
        
        public HouseSellingUpdateMessage()
        {
        }
        
        public HouseSellingUpdateMessage(uint houseId, int instanceId, bool secondHand, ulong realPrice, string buyerName)
        {
            this.HouseId = houseId;
            this.InstanceId = instanceId;
            this.SecondHand = secondHand;
            this.RealPrice = realPrice;
            this.BuyerName = buyerName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(InstanceId);
            writer.WriteBoolean(SecondHand);
            writer.WriteVarLong(RealPrice);
            writer.WriteUTF(BuyerName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            InstanceId = reader.ReadInt();
            if (InstanceId < 0)
                throw new Exception("Forbidden value on InstanceId = " + InstanceId + ", it doesn't respect the following condition : instanceId < 0");
            SecondHand = reader.ReadBoolean();
            RealPrice = reader.ReadVarUhLong();
            if (RealPrice < 0 || RealPrice > 9007199254740990)
                throw new Exception("Forbidden value on RealPrice = " + RealPrice + ", it doesn't respect the following condition : realPrice < 0 || realPrice > 9007199254740990");
            BuyerName = reader.ReadUTF();
        }
        
    }
    
}