

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccountHouseMessage : NetworkMessage
    {
        public const uint ProtocolId = 6315;
        public override uint MessageID => ProtocolId;
        
        public Types.AccountHouseInformations[] Houses { get; set; }
        
        public AccountHouseMessage()
        {
        }
        
        public AccountHouseMessage(Types.AccountHouseInformations[] houses)
        {
            this.Houses = houses;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Houses.Length);
            foreach (var entry in Houses)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Houses = new Types.AccountHouseInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Houses[i] = new Types.AccountHouseInformations();
                 Houses[i].Deserialize(reader);
            }
        }
        
    }
    
}