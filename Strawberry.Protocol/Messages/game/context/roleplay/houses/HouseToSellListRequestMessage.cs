

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseToSellListRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6139;
        public override uint MessageID => ProtocolId;
        
        public ushort PageIndex { get; set; }
        
        public HouseToSellListRequestMessage()
        {
        }
        
        public HouseToSellListRequestMessage(ushort pageIndex)
        {
            this.PageIndex = pageIndex;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(PageIndex);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PageIndex = reader.ReadVarUhShort();
            if (PageIndex < 0)
                throw new Exception("Forbidden value on PageIndex = " + PageIndex + ", it doesn't respect the following condition : pageIndex < 0");
        }
        
    }
    
}