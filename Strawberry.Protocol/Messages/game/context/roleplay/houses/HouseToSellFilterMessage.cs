

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseToSellFilterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6137;
        public override uint MessageID => ProtocolId;
        
        public int AreaId { get; set; }
        public sbyte AtLeastNbRoom { get; set; }
        public sbyte AtLeastNbChest { get; set; }
        public ushort SkillRequested { get; set; }
        public ulong MaxPrice { get; set; }
        
        public HouseToSellFilterMessage()
        {
        }
        
        public HouseToSellFilterMessage(int areaId, sbyte atLeastNbRoom, sbyte atLeastNbChest, ushort skillRequested, ulong maxPrice)
        {
            this.AreaId = areaId;
            this.AtLeastNbRoom = atLeastNbRoom;
            this.AtLeastNbChest = atLeastNbChest;
            this.SkillRequested = skillRequested;
            this.MaxPrice = maxPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AreaId);
            writer.WriteSByte(AtLeastNbRoom);
            writer.WriteSByte(AtLeastNbChest);
            writer.WriteVarShort(SkillRequested);
            writer.WriteVarLong(MaxPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AreaId = reader.ReadInt();
            AtLeastNbRoom = reader.ReadSByte();
            if (AtLeastNbRoom < 0)
                throw new Exception("Forbidden value on AtLeastNbRoom = " + AtLeastNbRoom + ", it doesn't respect the following condition : atLeastNbRoom < 0");
            AtLeastNbChest = reader.ReadSByte();
            if (AtLeastNbChest < 0)
                throw new Exception("Forbidden value on AtLeastNbChest = " + AtLeastNbChest + ", it doesn't respect the following condition : atLeastNbChest < 0");
            SkillRequested = reader.ReadVarUhShort();
            if (SkillRequested < 0)
                throw new Exception("Forbidden value on SkillRequested = " + SkillRequested + ", it doesn't respect the following condition : skillRequested < 0");
            MaxPrice = reader.ReadVarUhLong();
            if (MaxPrice < 0 || MaxPrice > 9007199254740990)
                throw new Exception("Forbidden value on MaxPrice = " + MaxPrice + ", it doesn't respect the following condition : maxPrice < 0 || maxPrice > 9007199254740990");
        }
        
    }
    
}