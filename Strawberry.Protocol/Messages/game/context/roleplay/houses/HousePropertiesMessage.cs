

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HousePropertiesMessage : NetworkMessage
    {
        public const uint ProtocolId = 5734;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int[] DoorsOnMap { get; set; }
        public Types.HouseInstanceInformations Properties { get; set; }
        
        public HousePropertiesMessage()
        {
        }
        
        public HousePropertiesMessage(uint houseId, int[] doorsOnMap, Types.HouseInstanceInformations properties)
        {
            this.HouseId = houseId;
            this.DoorsOnMap = doorsOnMap;
            this.Properties = properties;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteUShort((ushort)DoorsOnMap.Length);
            foreach (var entry in DoorsOnMap)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteShort(Properties.TypeID);
            Properties.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            var limit = reader.ReadUShort();
            DoorsOnMap = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 DoorsOnMap[i] = reader.ReadInt();
            }
            Properties = Types.ProtocolTypeManager.GetInstance<Types.HouseInstanceInformations>(reader.ReadShort());
            Properties.Deserialize(reader);
        }
        
    }
    
}