

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EntityTalkMessage : NetworkMessage
    {
        public const uint ProtocolId = 6110;
        public override uint MessageID => ProtocolId;
        
        public double EntityId { get; set; }
        public ushort TextId { get; set; }
        public string[] Parameters { get; set; }
        
        public EntityTalkMessage()
        {
        }
        
        public EntityTalkMessage(double entityId, ushort textId, string[] parameters)
        {
            this.EntityId = entityId;
            this.TextId = textId;
            this.Parameters = parameters;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(EntityId);
            writer.WriteVarShort(TextId);
            writer.WriteUShort((ushort)Parameters.Length);
            foreach (var entry in Parameters)
            {
                 writer.WriteUTF(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            EntityId = reader.ReadDouble();
            if (EntityId < -9007199254740990 || EntityId > 9007199254740990)
                throw new Exception("Forbidden value on EntityId = " + EntityId + ", it doesn't respect the following condition : entityId < -9007199254740990 || entityId > 9007199254740990");
            TextId = reader.ReadVarUhShort();
            if (TextId < 0)
                throw new Exception("Forbidden value on TextId = " + TextId + ", it doesn't respect the following condition : textId < 0");
            var limit = reader.ReadUShort();
            Parameters = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Parameters[i] = reader.ReadUTF();
            }
        }
        
    }
    
}