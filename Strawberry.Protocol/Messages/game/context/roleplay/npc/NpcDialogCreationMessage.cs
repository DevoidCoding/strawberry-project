

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NpcDialogCreationMessage : NetworkMessage
    {
        public const uint ProtocolId = 5618;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        public int NpcId { get; set; }
        
        public NpcDialogCreationMessage()
        {
        }
        
        public NpcDialogCreationMessage(double mapId, int npcId)
        {
            this.MapId = mapId;
            this.NpcId = npcId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteInt(NpcId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            NpcId = reader.ReadInt();
        }
        
    }
    
}