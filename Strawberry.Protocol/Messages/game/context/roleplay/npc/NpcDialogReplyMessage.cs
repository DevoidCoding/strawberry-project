

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NpcDialogReplyMessage : NetworkMessage
    {
        public const uint ProtocolId = 5616;
        public override uint MessageID => ProtocolId;
        
        public uint ReplyId { get; set; }
        
        public NpcDialogReplyMessage()
        {
        }
        
        public NpcDialogReplyMessage(uint replyId)
        {
            this.ReplyId = replyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ReplyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ReplyId = reader.ReadVarUhInt();
            if (ReplyId < 0)
                throw new Exception("Forbidden value on ReplyId = " + ReplyId + ", it doesn't respect the following condition : replyId < 0");
        }
        
    }
    
}