

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapNpcsQuestStatusUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5642;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        public int[] NpcsIdsWithQuest { get; set; }
        public Types.GameRolePlayNpcQuestFlag[] QuestFlags { get; set; }
        public int[] NpcsIdsWithoutQuest { get; set; }
        
        public MapNpcsQuestStatusUpdateMessage()
        {
        }
        
        public MapNpcsQuestStatusUpdateMessage(double mapId, int[] npcsIdsWithQuest, Types.GameRolePlayNpcQuestFlag[] questFlags, int[] npcsIdsWithoutQuest)
        {
            this.MapId = mapId;
            this.NpcsIdsWithQuest = npcsIdsWithQuest;
            this.QuestFlags = questFlags;
            this.NpcsIdsWithoutQuest = npcsIdsWithoutQuest;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteUShort((ushort)NpcsIdsWithQuest.Length);
            foreach (var entry in NpcsIdsWithQuest)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUShort((ushort)QuestFlags.Length);
            foreach (var entry in QuestFlags)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)NpcsIdsWithoutQuest.Length);
            foreach (var entry in NpcsIdsWithoutQuest)
            {
                 writer.WriteInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            var limit = reader.ReadUShort();
            NpcsIdsWithQuest = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 NpcsIdsWithQuest[i] = reader.ReadInt();
            }
            limit = reader.ReadUShort();
            QuestFlags = new Types.GameRolePlayNpcQuestFlag[limit];
            for (int i = 0; i < limit; i++)
            {
                 QuestFlags[i] = new Types.GameRolePlayNpcQuestFlag();
                 QuestFlags[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            NpcsIdsWithoutQuest = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 NpcsIdsWithoutQuest[i] = reader.ReadInt();
            }
        }
        
    }
    
}