

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NpcGenericActionFailureMessage : NetworkMessage
    {
        public const uint ProtocolId = 5900;
        public override uint MessageID => ProtocolId;
        
        
        public NpcGenericActionFailureMessage()
        {
        }
        
        
        public override void Serialize(IDataWriter writer)
        {
        }
        
        public override void Deserialize(IDataReader reader)
        {
        }
        
    }
    
}