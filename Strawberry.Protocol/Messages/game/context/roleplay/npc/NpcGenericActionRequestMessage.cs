

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class NpcGenericActionRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5898;
        public override uint MessageID => ProtocolId;
        
        public int NpcId { get; set; }
        public sbyte NpcActionId { get; set; }
        public double NpcMapId { get; set; }
        
        public NpcGenericActionRequestMessage()
        {
        }
        
        public NpcGenericActionRequestMessage(int npcId, sbyte npcActionId, double npcMapId)
        {
            this.NpcId = npcId;
            this.NpcActionId = npcActionId;
            this.NpcMapId = npcMapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(NpcId);
            writer.WriteSByte(NpcActionId);
            writer.WriteDouble(NpcMapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NpcId = reader.ReadInt();
            NpcActionId = reader.ReadSByte();
            if (NpcActionId < 0)
                throw new Exception("Forbidden value on NpcActionId = " + NpcActionId + ", it doesn't respect the following condition : npcActionId < 0");
            NpcMapId = reader.ReadDouble();
            if (NpcMapId < 0 || NpcMapId > 9007199254740990)
                throw new Exception("Forbidden value on NpcMapId = " + NpcMapId + ", it doesn't respect the following condition : npcMapId < 0 || npcMapId > 9007199254740990");
        }
        
    }
    
}