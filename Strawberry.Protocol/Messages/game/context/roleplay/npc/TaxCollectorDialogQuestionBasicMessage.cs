

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorDialogQuestionBasicMessage : NetworkMessage
    {
        public const uint ProtocolId = 5619;
        public override uint MessageID => ProtocolId;
        
        public Types.BasicGuildInformations GuildInfo { get; set; }
        
        public TaxCollectorDialogQuestionBasicMessage()
        {
        }
        
        public TaxCollectorDialogQuestionBasicMessage(Types.BasicGuildInformations guildInfo)
        {
            this.GuildInfo = guildInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            GuildInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildInfo = new Types.BasicGuildInformations();
            GuildInfo.Deserialize(reader);
        }
        
    }
    
}