

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorDialogQuestionExtendedMessage : TaxCollectorDialogQuestionBasicMessage
    {
        public new const uint ProtocolId = 5615;
        public override uint MessageID => ProtocolId;
        
        public ushort MaxPods { get; set; }
        public ushort Prospecting { get; set; }
        public ushort Wisdom { get; set; }
        public sbyte TaxCollectorsCount { get; set; }
        public int TaxCollectorAttack { get; set; }
        public ulong Kamas { get; set; }
        public ulong Experience { get; set; }
        public uint Pods { get; set; }
        public ulong ItemsValue { get; set; }
        
        public TaxCollectorDialogQuestionExtendedMessage()
        {
        }
        
        public TaxCollectorDialogQuestionExtendedMessage(Types.BasicGuildInformations guildInfo, ushort maxPods, ushort prospecting, ushort wisdom, sbyte taxCollectorsCount, int taxCollectorAttack, ulong kamas, ulong experience, uint pods, ulong itemsValue)
         : base(guildInfo)
        {
            this.MaxPods = maxPods;
            this.Prospecting = prospecting;
            this.Wisdom = wisdom;
            this.TaxCollectorsCount = taxCollectorsCount;
            this.TaxCollectorAttack = taxCollectorAttack;
            this.Kamas = kamas;
            this.Experience = experience;
            this.Pods = pods;
            this.ItemsValue = itemsValue;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(MaxPods);
            writer.WriteVarShort(Prospecting);
            writer.WriteVarShort(Wisdom);
            writer.WriteSByte(TaxCollectorsCount);
            writer.WriteInt(TaxCollectorAttack);
            writer.WriteVarLong(Kamas);
            writer.WriteVarLong(Experience);
            writer.WriteVarInt(Pods);
            writer.WriteVarLong(ItemsValue);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MaxPods = reader.ReadVarUhShort();
            if (MaxPods < 0)
                throw new Exception("Forbidden value on MaxPods = " + MaxPods + ", it doesn't respect the following condition : maxPods < 0");
            Prospecting = reader.ReadVarUhShort();
            if (Prospecting < 0)
                throw new Exception("Forbidden value on Prospecting = " + Prospecting + ", it doesn't respect the following condition : prospecting < 0");
            Wisdom = reader.ReadVarUhShort();
            if (Wisdom < 0)
                throw new Exception("Forbidden value on Wisdom = " + Wisdom + ", it doesn't respect the following condition : wisdom < 0");
            TaxCollectorsCount = reader.ReadSByte();
            if (TaxCollectorsCount < 0)
                throw new Exception("Forbidden value on TaxCollectorsCount = " + TaxCollectorsCount + ", it doesn't respect the following condition : taxCollectorsCount < 0");
            TaxCollectorAttack = reader.ReadInt();
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            Pods = reader.ReadVarUhInt();
            if (Pods < 0)
                throw new Exception("Forbidden value on Pods = " + Pods + ", it doesn't respect the following condition : pods < 0");
            ItemsValue = reader.ReadVarUhLong();
            if (ItemsValue < 0 || ItemsValue > 9007199254740990)
                throw new Exception("Forbidden value on ItemsValue = " + ItemsValue + ", it doesn't respect the following condition : itemsValue < 0 || itemsValue > 9007199254740990");
        }
        
    }
    
}