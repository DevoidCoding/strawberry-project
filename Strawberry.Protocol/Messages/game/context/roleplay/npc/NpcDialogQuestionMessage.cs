

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NpcDialogQuestionMessage : NetworkMessage
    {
        public const uint ProtocolId = 5617;
        public override uint MessageID => ProtocolId;
        
        public uint MessageId { get; set; }
        public string[] DialogParams { get; set; }
        public uint[] VisibleReplies { get; set; }
        
        public NpcDialogQuestionMessage()
        {
        }
        
        public NpcDialogQuestionMessage(uint messageId, string[] dialogParams, uint[] visibleReplies)
        {
            this.MessageId = messageId;
            this.DialogParams = dialogParams;
            this.VisibleReplies = visibleReplies;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MessageId);
            writer.WriteUShort((ushort)DialogParams.Length);
            foreach (var entry in DialogParams)
            {
                 writer.WriteUTF(entry);
            }
            writer.WriteUShort((ushort)VisibleReplies.Length);
            foreach (var entry in VisibleReplies)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MessageId = reader.ReadVarUhInt();
            if (MessageId < 0)
                throw new Exception("Forbidden value on MessageId = " + MessageId + ", it doesn't respect the following condition : messageId < 0");
            var limit = reader.ReadUShort();
            DialogParams = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 DialogParams[i] = reader.ReadUTF();
            }
            limit = reader.ReadUShort();
            VisibleReplies = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 VisibleReplies[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}