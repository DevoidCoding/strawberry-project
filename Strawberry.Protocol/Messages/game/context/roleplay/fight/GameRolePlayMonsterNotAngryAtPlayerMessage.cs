

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayMonsterNotAngryAtPlayerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6742;
        public override uint MessageID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public double MonsterGroupId { get; set; }
        
        public GameRolePlayMonsterNotAngryAtPlayerMessage()
        {
        }
        
        public GameRolePlayMonsterNotAngryAtPlayerMessage(ulong playerId, double monsterGroupId)
        {
            this.PlayerId = playerId;
            this.MonsterGroupId = monsterGroupId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteDouble(MonsterGroupId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            MonsterGroupId = reader.ReadDouble();
            if (MonsterGroupId < -9007199254740990 || MonsterGroupId > 9007199254740990)
                throw new Exception("Forbidden value on MonsterGroupId = " + MonsterGroupId + ", it doesn't respect the following condition : monsterGroupId < -9007199254740990 || monsterGroupId > 9007199254740990");
        }
        
    }
    
}