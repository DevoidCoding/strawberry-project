

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayAttackMonsterRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6191;
        public override uint MessageID => ProtocolId;
        
        public double MonsterGroupId { get; set; }
        
        public GameRolePlayAttackMonsterRequestMessage()
        {
        }
        
        public GameRolePlayAttackMonsterRequestMessage(double monsterGroupId)
        {
            this.MonsterGroupId = monsterGroupId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MonsterGroupId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MonsterGroupId = reader.ReadDouble();
            if (MonsterGroupId < -9007199254740990 || MonsterGroupId > 9007199254740990)
                throw new Exception("Forbidden value on MonsterGroupId = " + MonsterGroupId + ", it doesn't respect the following condition : monsterGroupId < -9007199254740990 || monsterGroupId > 9007199254740990");
        }
        
    }
    
}