

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class GameRolePlayPlayerFightRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5731;
        public override uint MessageID => ProtocolId;
        
        public ulong TargetId { get; set; }
        public short TargetCellId { get; set; }
        public bool Friendly { get; set; }
        
        public GameRolePlayPlayerFightRequestMessage()
        {
        }
        
        public GameRolePlayPlayerFightRequestMessage(ulong targetId, short targetCellId, bool friendly)
        {
            this.TargetId = targetId;
            this.TargetCellId = targetCellId;
            this.Friendly = friendly;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(TargetId);
            writer.WriteShort(TargetCellId);
            writer.WriteBoolean(Friendly);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TargetId = reader.ReadVarUhLong();
            if (TargetId < 0 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < 0 || targetId > 9007199254740990");
            TargetCellId = reader.ReadShort();
            if (TargetCellId < -1 || TargetCellId > 559)
                throw new Exception("Forbidden value on TargetCellId = " + TargetCellId + ", it doesn't respect the following condition : targetCellId < -1 || targetCellId > 559");
            Friendly = reader.ReadBoolean();
        }
        
    }
    
}