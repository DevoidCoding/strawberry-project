

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayShowChallengeMessage : NetworkMessage
    {
        public const uint ProtocolId = 301;
        public override uint MessageID => ProtocolId;
        
        public Types.FightCommonInformations CommonsInfos { get; set; }
        
        public GameRolePlayShowChallengeMessage()
        {
        }
        
        public GameRolePlayShowChallengeMessage(Types.FightCommonInformations commonsInfos)
        {
            this.CommonsInfos = commonsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            CommonsInfos.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CommonsInfos = new Types.FightCommonInformations();
            CommonsInfos.Deserialize(reader);
        }
        
    }
    
}