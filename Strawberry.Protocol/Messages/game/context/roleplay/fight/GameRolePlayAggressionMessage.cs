

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayAggressionMessage : NetworkMessage
    {
        public const uint ProtocolId = 6073;
        public override uint MessageID => ProtocolId;
        
        public ulong AttackerId { get; set; }
        public ulong DefenderId { get; set; }
        
        public GameRolePlayAggressionMessage()
        {
        }
        
        public GameRolePlayAggressionMessage(ulong attackerId, ulong defenderId)
        {
            this.AttackerId = attackerId;
            this.DefenderId = defenderId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(AttackerId);
            writer.WriteVarLong(DefenderId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AttackerId = reader.ReadVarUhLong();
            if (AttackerId < 0 || AttackerId > 9007199254740990)
                throw new Exception("Forbidden value on AttackerId = " + AttackerId + ", it doesn't respect the following condition : attackerId < 0 || attackerId > 9007199254740990");
            DefenderId = reader.ReadVarUhLong();
            if (DefenderId < 0 || DefenderId > 9007199254740990)
                throw new Exception("Forbidden value on DefenderId = " + DefenderId + ", it doesn't respect the following condition : defenderId < 0 || defenderId > 9007199254740990");
        }
        
    }
    
}