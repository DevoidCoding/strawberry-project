

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayMonsterAngryAtPlayerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6741;
        public override uint MessageID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public double MonsterGroupId { get; set; }
        public double AngryStartTime { get; set; }
        public double AttackTime { get; set; }
        
        public GameRolePlayMonsterAngryAtPlayerMessage()
        {
        }
        
        public GameRolePlayMonsterAngryAtPlayerMessage(ulong playerId, double monsterGroupId, double angryStartTime, double attackTime)
        {
            this.PlayerId = playerId;
            this.MonsterGroupId = monsterGroupId;
            this.AngryStartTime = angryStartTime;
            this.AttackTime = attackTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteDouble(MonsterGroupId);
            writer.WriteDouble(AngryStartTime);
            writer.WriteDouble(AttackTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            MonsterGroupId = reader.ReadDouble();
            if (MonsterGroupId < -9007199254740990 || MonsterGroupId > 9007199254740990)
                throw new Exception("Forbidden value on MonsterGroupId = " + MonsterGroupId + ", it doesn't respect the following condition : monsterGroupId < -9007199254740990 || monsterGroupId > 9007199254740990");
            AngryStartTime = reader.ReadDouble();
            if (AngryStartTime < 0 || AngryStartTime > 9007199254740990)
                throw new Exception("Forbidden value on AngryStartTime = " + AngryStartTime + ", it doesn't respect the following condition : angryStartTime < 0 || angryStartTime > 9007199254740990");
            AttackTime = reader.ReadDouble();
            if (AttackTime < 0 || AttackTime > 9007199254740990)
                throw new Exception("Forbidden value on AttackTime = " + AttackTime + ", it doesn't respect the following condition : attackTime < 0 || attackTime > 9007199254740990");
        }
        
    }
    
}