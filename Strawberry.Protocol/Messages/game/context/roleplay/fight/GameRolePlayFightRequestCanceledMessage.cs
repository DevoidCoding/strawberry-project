

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayFightRequestCanceledMessage : NetworkMessage
    {
        public const uint ProtocolId = 5822;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public double SourceId { get; set; }
        public double TargetId { get; set; }
        
        public GameRolePlayFightRequestCanceledMessage()
        {
        }
        
        public GameRolePlayFightRequestCanceledMessage(ushort fightId, double sourceId, double targetId)
        {
            this.FightId = fightId;
            this.SourceId = sourceId;
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteDouble(SourceId);
            writer.WriteDouble(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            SourceId = reader.ReadDouble();
            if (SourceId < -9007199254740990 || SourceId > 9007199254740990)
                throw new Exception("Forbidden value on SourceId = " + SourceId + ", it doesn't respect the following condition : sourceId < -9007199254740990 || sourceId > 9007199254740990");
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
        }
        
    }
    
}