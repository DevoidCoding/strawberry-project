

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayPlayerFightFriendlyRequestedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5937;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public ulong SourceId { get; set; }
        public ulong TargetId { get; set; }
        
        public GameRolePlayPlayerFightFriendlyRequestedMessage()
        {
        }
        
        public GameRolePlayPlayerFightFriendlyRequestedMessage(ushort fightId, ulong sourceId, ulong targetId)
        {
            this.FightId = fightId;
            this.SourceId = sourceId;
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteVarLong(SourceId);
            writer.WriteVarLong(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            SourceId = reader.ReadVarUhLong();
            if (SourceId < 0 || SourceId > 9007199254740990)
                throw new Exception("Forbidden value on SourceId = " + SourceId + ", it doesn't respect the following condition : sourceId < 0 || sourceId > 9007199254740990");
            TargetId = reader.ReadVarUhLong();
            if (TargetId < 0 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < 0 || targetId > 9007199254740990");
        }
        
    }
    
}