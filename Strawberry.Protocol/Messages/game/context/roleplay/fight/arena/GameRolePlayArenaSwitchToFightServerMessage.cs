

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class GameRolePlayArenaSwitchToFightServerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6575;
        public override uint MessageID => ProtocolId;
        
        public string Address { get; set; }
        public ushort[] Ports { get; set; }
        public sbyte[] Ticket { get; set; }
        
        public GameRolePlayArenaSwitchToFightServerMessage()
        {
        }
        
        public GameRolePlayArenaSwitchToFightServerMessage(string address, ushort[] ports, sbyte[] ticket)
        {
            this.Address = address;
            this.Ports = ports;
            this.Ticket = ticket;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Address);
            writer.WriteUShort((ushort)Ports.Length);
            foreach (var entry in Ports)
            {
                 writer.WriteUShort(entry);
            }
            writer.WriteVarInt((int)Ticket.Length);
            foreach (var entry in Ticket)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Address = reader.ReadUTF();
            var limit = reader.ReadUShort();
            Ports = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ports[i] = reader.ReadUShort();
            }
            var limit2 = reader.ReadVarInt();
            Ticket = new sbyte[limit2];
            for (int i = 0; i < limit2; i++)
            {
                 Ticket[i] = reader.ReadSByte();
            }
        }
        
    }
    
}