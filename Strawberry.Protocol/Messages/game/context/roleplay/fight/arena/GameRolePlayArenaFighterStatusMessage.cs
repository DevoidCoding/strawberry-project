

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaFighterStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6281;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public int PlayerId { get; set; }
        public bool Accepted { get; set; }
        
        public GameRolePlayArenaFighterStatusMessage()
        {
        }
        
        public GameRolePlayArenaFighterStatusMessage(ushort fightId, int playerId, bool accepted)
        {
            this.FightId = fightId;
            this.PlayerId = playerId;
            this.Accepted = accepted;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteInt(PlayerId);
            writer.WriteBoolean(Accepted);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            PlayerId = reader.ReadInt();
            Accepted = reader.ReadBoolean();
        }
        
    }
    
}