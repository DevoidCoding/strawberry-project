

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaRegisterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6280;
        public override uint MessageID => ProtocolId;
        
        public int BattleMode { get; set; }
        
        public GameRolePlayArenaRegisterMessage()
        {
        }
        
        public GameRolePlayArenaRegisterMessage(int battleMode)
        {
            this.BattleMode = battleMode;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(BattleMode);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BattleMode = reader.ReadInt();
            if (BattleMode < 0)
                throw new Exception("Forbidden value on BattleMode = " + BattleMode + ", it doesn't respect the following condition : battleMode < 0");
        }
        
    }
    
}