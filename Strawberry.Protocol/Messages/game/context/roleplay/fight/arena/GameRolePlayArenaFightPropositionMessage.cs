

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaFightPropositionMessage : NetworkMessage
    {
        public const uint ProtocolId = 6276;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public double[] AlliesId { get; set; }
        public ushort Duration { get; set; }
        
        public GameRolePlayArenaFightPropositionMessage()
        {
        }
        
        public GameRolePlayArenaFightPropositionMessage(ushort fightId, double[] alliesId, ushort duration)
        {
            this.FightId = fightId;
            this.AlliesId = alliesId;
            this.Duration = duration;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteUShort((ushort)AlliesId.Length);
            foreach (var entry in AlliesId)
            {
                 writer.WriteDouble(entry);
            }
            writer.WriteVarShort(Duration);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            var limit = reader.ReadUShort();
            AlliesId = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 AlliesId[i] = reader.ReadDouble();
            }
            Duration = reader.ReadVarUhShort();
            if (Duration < 0)
                throw new Exception("Forbidden value on Duration = " + Duration + ", it doesn't respect the following condition : duration < 0");
        }
        
    }
    
}