

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaRegistrationStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6284;
        public override uint MessageID => ProtocolId;
        
        public bool Registered { get; set; }
        public sbyte Step { get; set; }
        public int BattleMode { get; set; }
        
        public GameRolePlayArenaRegistrationStatusMessage()
        {
        }
        
        public GameRolePlayArenaRegistrationStatusMessage(bool registered, sbyte step, int battleMode)
        {
            this.Registered = registered;
            this.Step = step;
            this.BattleMode = battleMode;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Registered);
            writer.WriteSByte(Step);
            writer.WriteInt(BattleMode);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Registered = reader.ReadBoolean();
            Step = reader.ReadSByte();
            if (Step < 0)
                throw new Exception("Forbidden value on Step = " + Step + ", it doesn't respect the following condition : step < 0");
            BattleMode = reader.ReadInt();
            if (BattleMode < 0)
                throw new Exception("Forbidden value on BattleMode = " + BattleMode + ", it doesn't respect the following condition : battleMode < 0");
        }
        
    }
    
}