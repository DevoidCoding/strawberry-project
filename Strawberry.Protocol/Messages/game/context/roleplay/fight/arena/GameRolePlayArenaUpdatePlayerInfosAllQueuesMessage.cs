

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage : GameRolePlayArenaUpdatePlayerInfosMessage
    {
        public new const uint ProtocolId = 6728;
        public override uint MessageID => ProtocolId;
        
        public Types.ArenaRankInfos Team { get; set; }
        public Types.ArenaRankInfos Duel { get; set; }
        
        public GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage()
        {
        }
        
        public GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage(Types.ArenaRankInfos solo, Types.ArenaRankInfos team, Types.ArenaRankInfos duel)
         : base(solo)
        {
            this.Team = team;
            this.Duel = duel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Team.Serialize(writer);
            Duel.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Team = new Types.ArenaRankInfos();
            Team.Deserialize(reader);
            Duel = new Types.ArenaRankInfos();
            Duel.Deserialize(reader);
        }
        
    }
    
}