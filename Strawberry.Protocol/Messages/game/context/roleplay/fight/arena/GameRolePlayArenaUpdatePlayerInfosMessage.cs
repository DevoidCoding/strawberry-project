

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayArenaUpdatePlayerInfosMessage : NetworkMessage
    {
        public const uint ProtocolId = 6301;
        public override uint MessageID => ProtocolId;
        
        public Types.ArenaRankInfos Solo { get; set; }
        
        public GameRolePlayArenaUpdatePlayerInfosMessage()
        {
        }
        
        public GameRolePlayArenaUpdatePlayerInfosMessage(Types.ArenaRankInfos solo)
        {
            this.Solo = solo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Solo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Solo = new Types.ArenaRankInfos();
            Solo.Deserialize(reader);
        }
        
    }
    
}