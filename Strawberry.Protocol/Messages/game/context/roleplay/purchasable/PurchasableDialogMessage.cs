

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PurchasableDialogMessage : NetworkMessage
    {
        public const uint ProtocolId = 5739;
        public override uint MessageID => ProtocolId;
        
        public bool BuyOrSell { get; set; }
        public bool SecondHand { get; set; }
        public double PurchasableId { get; set; }
        public int PurchasableInstanceId { get; set; }
        public ulong Price { get; set; }
        
        public PurchasableDialogMessage()
        {
        }
        
        public PurchasableDialogMessage(bool buyOrSell, bool secondHand, double purchasableId, int purchasableInstanceId, ulong price)
        {
            this.BuyOrSell = buyOrSell;
            this.SecondHand = secondHand;
            this.PurchasableId = purchasableId;
            this.PurchasableInstanceId = purchasableInstanceId;
            this.Price = price;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, BuyOrSell);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, SecondHand);
            writer.WriteByte(flag1);
            writer.WriteDouble(PurchasableId);
            writer.WriteInt(PurchasableInstanceId);
            writer.WriteVarLong(Price);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            BuyOrSell = BooleanByteWrapper.GetFlag(flag1, 0);
            SecondHand = BooleanByteWrapper.GetFlag(flag1, 1);
            PurchasableId = reader.ReadDouble();
            if (PurchasableId < 0 || PurchasableId > 9007199254740990)
                throw new Exception("Forbidden value on PurchasableId = " + PurchasableId + ", it doesn't respect the following condition : purchasableId < 0 || purchasableId > 9007199254740990");
            PurchasableInstanceId = reader.ReadInt();
            if (PurchasableInstanceId < 0)
                throw new Exception("Forbidden value on PurchasableInstanceId = " + PurchasableInstanceId + ", it doesn't respect the following condition : purchasableInstanceId < 0");
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}