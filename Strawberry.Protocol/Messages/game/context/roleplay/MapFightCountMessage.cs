

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapFightCountMessage : NetworkMessage
    {
        public const uint ProtocolId = 210;
        public override uint MessageID => ProtocolId;
        
        public ushort FightCount { get; set; }
        
        public MapFightCountMessage()
        {
        }
        
        public MapFightCountMessage(ushort fightCount)
        {
            this.FightCount = fightCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightCount = reader.ReadVarUhShort();
            if (FightCount < 0)
                throw new Exception("Forbidden value on FightCount = " + FightCount + ", it doesn't respect the following condition : fightCount < 0");
        }
        
    }
    
}