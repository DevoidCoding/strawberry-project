

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportOnSameMapMessage : NetworkMessage
    {
        public const uint ProtocolId = 6048;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public ushort CellId { get; set; }
        
        public TeleportOnSameMapMessage()
        {
        }
        
        public TeleportOnSameMapMessage(double targetId, ushort cellId)
        {
            this.TargetId = targetId;
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(CellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            CellId = reader.ReadVarUhShort();
            if (CellId < 0 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0 || cellId > 559");
        }
        
    }
    
}