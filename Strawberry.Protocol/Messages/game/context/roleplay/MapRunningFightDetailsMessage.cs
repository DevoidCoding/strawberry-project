

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapRunningFightDetailsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5751;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public Types.GameFightFighterLightInformations[] Attackers { get; set; }
        public Types.GameFightFighterLightInformations[] Defenders { get; set; }
        
        public MapRunningFightDetailsMessage()
        {
        }
        
        public MapRunningFightDetailsMessage(ushort fightId, Types.GameFightFighterLightInformations[] attackers, Types.GameFightFighterLightInformations[] defenders)
        {
            this.FightId = fightId;
            this.Attackers = attackers;
            this.Defenders = defenders;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteUShort((ushort)Attackers.Length);
            foreach (var entry in Attackers)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Defenders.Length);
            foreach (var entry in Defenders)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            var limit = reader.ReadUShort();
            Attackers = new Types.GameFightFighterLightInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Attackers[i] = Types.ProtocolTypeManager.GetInstance<Types.GameFightFighterLightInformations>(reader.ReadShort());
                 Attackers[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Defenders = new Types.GameFightFighterLightInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Defenders[i] = Types.ProtocolTypeManager.GetInstance<Types.GameFightFighterLightInformations>(reader.ReadShort());
                 Defenders[i].Deserialize(reader);
            }
        }
        
    }
    
}