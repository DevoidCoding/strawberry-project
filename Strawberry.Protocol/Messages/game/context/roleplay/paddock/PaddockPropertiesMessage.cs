

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockPropertiesMessage : NetworkMessage
    {
        public const uint ProtocolId = 5824;
        public override uint MessageID => ProtocolId;
        
        public Types.PaddockInstancesInformations Properties { get; set; }
        
        public PaddockPropertiesMessage()
        {
        }
        
        public PaddockPropertiesMessage(Types.PaddockInstancesInformations properties)
        {
            this.Properties = properties;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Properties.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Properties = new Types.PaddockInstancesInformations();
            Properties.Deserialize(reader);
        }
        
    }
    
}