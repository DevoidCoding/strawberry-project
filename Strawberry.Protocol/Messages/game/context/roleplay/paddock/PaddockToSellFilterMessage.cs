

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockToSellFilterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6161;
        public override uint MessageID => ProtocolId;
        
        public int AreaId { get; set; }
        public sbyte AtLeastNbMount { get; set; }
        public sbyte AtLeastNbMachine { get; set; }
        public ulong MaxPrice { get; set; }
        
        public PaddockToSellFilterMessage()
        {
        }
        
        public PaddockToSellFilterMessage(int areaId, sbyte atLeastNbMount, sbyte atLeastNbMachine, ulong maxPrice)
        {
            this.AreaId = areaId;
            this.AtLeastNbMount = atLeastNbMount;
            this.AtLeastNbMachine = atLeastNbMachine;
            this.MaxPrice = maxPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AreaId);
            writer.WriteSByte(AtLeastNbMount);
            writer.WriteSByte(AtLeastNbMachine);
            writer.WriteVarLong(MaxPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AreaId = reader.ReadInt();
            AtLeastNbMount = reader.ReadSByte();
            AtLeastNbMachine = reader.ReadSByte();
            MaxPrice = reader.ReadVarUhLong();
            if (MaxPrice < 0 || MaxPrice > 9007199254740990)
                throw new Exception("Forbidden value on MaxPrice = " + MaxPrice + ", it doesn't respect the following condition : maxPrice < 0 || maxPrice > 9007199254740990");
        }
        
    }
    
}