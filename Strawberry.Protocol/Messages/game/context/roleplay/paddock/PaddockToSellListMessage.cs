

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockToSellListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6138;
        public override uint MessageID => ProtocolId;
        
        public ushort PageIndex { get; set; }
        public ushort TotalPage { get; set; }
        public Types.PaddockInformationsForSell[] PaddockList { get; set; }
        
        public PaddockToSellListMessage()
        {
        }
        
        public PaddockToSellListMessage(ushort pageIndex, ushort totalPage, Types.PaddockInformationsForSell[] paddockList)
        {
            this.PageIndex = pageIndex;
            this.TotalPage = totalPage;
            this.PaddockList = paddockList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(PageIndex);
            writer.WriteVarShort(TotalPage);
            writer.WriteUShort((ushort)PaddockList.Length);
            foreach (var entry in PaddockList)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PageIndex = reader.ReadVarUhShort();
            if (PageIndex < 0)
                throw new Exception("Forbidden value on PageIndex = " + PageIndex + ", it doesn't respect the following condition : pageIndex < 0");
            TotalPage = reader.ReadVarUhShort();
            if (TotalPage < 0)
                throw new Exception("Forbidden value on TotalPage = " + TotalPage + ", it doesn't respect the following condition : totalPage < 0");
            var limit = reader.ReadUShort();
            PaddockList = new Types.PaddockInformationsForSell[limit];
            for (int i = 0; i < limit; i++)
            {
                 PaddockList[i] = new Types.PaddockInformationsForSell();
                 PaddockList[i].Deserialize(reader);
            }
        }
        
    }
    
}