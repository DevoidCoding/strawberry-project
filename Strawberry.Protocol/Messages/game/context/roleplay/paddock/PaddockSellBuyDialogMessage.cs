

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockSellBuyDialogMessage : NetworkMessage
    {
        public const uint ProtocolId = 6018;
        public override uint MessageID => ProtocolId;
        
        public bool Bsell { get; set; }
        public uint OwnerId { get; set; }
        public ulong Price { get; set; }
        
        public PaddockSellBuyDialogMessage()
        {
        }
        
        public PaddockSellBuyDialogMessage(bool bsell, uint ownerId, ulong price)
        {
            this.Bsell = bsell;
            this.OwnerId = ownerId;
            this.Price = price;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Bsell);
            writer.WriteVarInt(OwnerId);
            writer.WriteVarLong(Price);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Bsell = reader.ReadBoolean();
            OwnerId = reader.ReadVarUhInt();
            if (OwnerId < 0)
                throw new Exception("Forbidden value on OwnerId = " + OwnerId + ", it doesn't respect the following condition : ownerId < 0");
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}