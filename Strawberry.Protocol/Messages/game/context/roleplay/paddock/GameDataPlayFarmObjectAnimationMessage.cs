

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameDataPlayFarmObjectAnimationMessage : NetworkMessage
    {
        public const uint ProtocolId = 6026;
        public override uint MessageID => ProtocolId;
        
        public ushort[] CellId { get; set; }
        
        public GameDataPlayFarmObjectAnimationMessage()
        {
        }
        
        public GameDataPlayFarmObjectAnimationMessage(ushort[] cellId)
        {
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)CellId.Length);
            foreach (var entry in CellId)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            CellId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 CellId[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}