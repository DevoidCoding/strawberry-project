

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FollowedQuestsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6717;
        public override uint MessageID => ProtocolId;
        
        public Types.QuestActiveDetailedInformations[] Quests { get; set; }
        
        public FollowedQuestsMessage()
        {
        }
        
        public FollowedQuestsMessage(Types.QuestActiveDetailedInformations[] quests)
        {
            this.Quests = quests;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Quests.Length);
            foreach (var entry in Quests)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Quests = new Types.QuestActiveDetailedInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Quests[i] = new Types.QuestActiveDetailedInformations();
                 Quests[i].Deserialize(reader);
            }
        }
        
    }
    
}