

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class QuestStepValidatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6099;
        public override uint MessageID => ProtocolId;
        
        public ushort QuestId { get; set; }
        public ushort StepId { get; set; }
        
        public QuestStepValidatedMessage()
        {
        }
        
        public QuestStepValidatedMessage(ushort questId, ushort stepId)
        {
            this.QuestId = questId;
            this.StepId = stepId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteVarShort(StepId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestId = reader.ReadVarUhShort();
            if (QuestId < 0)
                throw new Exception("Forbidden value on QuestId = " + QuestId + ", it doesn't respect the following condition : questId < 0");
            StepId = reader.ReadVarUhShort();
            if (StepId < 0)
                throw new Exception("Forbidden value on StepId = " + StepId + ", it doesn't respect the following condition : stepId < 0");
        }
        
    }
    
}