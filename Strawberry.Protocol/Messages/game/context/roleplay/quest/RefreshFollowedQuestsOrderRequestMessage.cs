

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class RefreshFollowedQuestsOrderRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6722;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Quests { get; set; }
        
        public RefreshFollowedQuestsOrderRequestMessage()
        {
        }
        
        public RefreshFollowedQuestsOrderRequestMessage(ushort[] quests)
        {
            this.Quests = quests;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Quests.Length);
            foreach (var entry in Quests)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Quests = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Quests[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}