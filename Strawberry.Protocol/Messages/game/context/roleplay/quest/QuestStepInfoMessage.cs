

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class QuestStepInfoMessage : NetworkMessage
    {
        public const uint ProtocolId = 5625;
        public override uint MessageID => ProtocolId;
        
        public Types.QuestActiveInformations Infos { get; set; }
        
        public QuestStepInfoMessage()
        {
        }
        
        public QuestStepInfoMessage(Types.QuestActiveInformations infos)
        {
            this.Infos = infos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Infos.TypeID);
            Infos.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Infos = Types.ProtocolTypeManager.GetInstance<Types.QuestActiveInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
        }
        
    }
    
}