

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class QuestListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5626;
        public override uint MessageID => ProtocolId;
        
        public ushort[] FinishedQuestsIds { get; set; }
        public ushort[] FinishedQuestsCounts { get; set; }
        public Types.QuestActiveInformations[] ActiveQuests { get; set; }
        public ushort[] ReinitDoneQuestsIds { get; set; }
        
        public QuestListMessage()
        {
        }
        
        public QuestListMessage(ushort[] finishedQuestsIds, ushort[] finishedQuestsCounts, Types.QuestActiveInformations[] activeQuests, ushort[] reinitDoneQuestsIds)
        {
            this.FinishedQuestsIds = finishedQuestsIds;
            this.FinishedQuestsCounts = finishedQuestsCounts;
            this.ActiveQuests = activeQuests;
            this.ReinitDoneQuestsIds = reinitDoneQuestsIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)FinishedQuestsIds.Length);
            foreach (var entry in FinishedQuestsIds)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)FinishedQuestsCounts.Length);
            foreach (var entry in FinishedQuestsCounts)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)ActiveQuests.Length);
            foreach (var entry in ActiveQuests)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)ReinitDoneQuestsIds.Length);
            foreach (var entry in ReinitDoneQuestsIds)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            FinishedQuestsIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishedQuestsIds[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            FinishedQuestsCounts = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishedQuestsCounts[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            ActiveQuests = new Types.QuestActiveInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 ActiveQuests[i] = Types.ProtocolTypeManager.GetInstance<Types.QuestActiveInformations>(reader.ReadShort());
                 ActiveQuests[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            ReinitDoneQuestsIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 ReinitDoneQuestsIds[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}