

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FollowQuestObjectiveRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6724;
        public override uint MessageID => ProtocolId;
        
        public ushort QuestId { get; set; }
        public short ObjectiveId { get; set; }
        
        public FollowQuestObjectiveRequestMessage()
        {
        }
        
        public FollowQuestObjectiveRequestMessage(ushort questId, short objectiveId)
        {
            this.QuestId = questId;
            this.ObjectiveId = objectiveId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(QuestId);
            writer.WriteShort(ObjectiveId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestId = reader.ReadVarUhShort();
            if (QuestId < 0)
                throw new Exception("Forbidden value on QuestId = " + QuestId + ", it doesn't respect the following condition : questId < 0");
            ObjectiveId = reader.ReadShort();
        }
        
    }
    
}