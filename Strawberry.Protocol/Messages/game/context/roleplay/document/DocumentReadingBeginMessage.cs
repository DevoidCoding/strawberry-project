

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DocumentReadingBeginMessage : NetworkMessage
    {
        public const uint ProtocolId = 5675;
        public override uint MessageID => ProtocolId;
        
        public ushort DocumentId { get; set; }
        
        public DocumentReadingBeginMessage()
        {
        }
        
        public DocumentReadingBeginMessage(ushort documentId)
        {
            this.DocumentId = documentId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DocumentId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DocumentId = reader.ReadVarUhShort();
            if (DocumentId < 0)
                throw new Exception("Forbidden value on DocumentId = " + DocumentId + ", it doesn't respect the following condition : documentId < 0");
        }
        
    }
    
}