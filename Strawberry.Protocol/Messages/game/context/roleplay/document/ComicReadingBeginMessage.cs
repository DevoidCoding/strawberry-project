

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ComicReadingBeginMessage : NetworkMessage
    {
        public const uint ProtocolId = 6536;
        public override uint MessageID => ProtocolId;
        
        public ushort ComicId { get; set; }
        
        public ComicReadingBeginMessage()
        {
        }
        
        public ComicReadingBeginMessage(ushort comicId)
        {
            this.ComicId = comicId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ComicId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ComicId = reader.ReadVarUhShort();
            if (ComicId < 0)
                throw new Exception("Forbidden value on ComicId = " + ComicId + ", it doesn't respect the following condition : comicId < 0");
        }
        
    }
    
}