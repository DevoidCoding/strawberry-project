

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CurrentMapMessage : NetworkMessage
    {
        public const uint ProtocolId = 220;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        public string MapKey { get; set; }
        
        public CurrentMapMessage()
        {
        }
        
        public CurrentMapMessage(double mapId, string mapKey)
        {
            this.MapId = mapId;
            this.MapKey = mapKey;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteUTF(MapKey);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            MapKey = reader.ReadUTF();
        }
        
    }
    
}