

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CurrentMapInstanceMessage : CurrentMapMessage
    {
        public new const uint ProtocolId = 6738;
        public override uint MessageID => ProtocolId;
        
        public double InstantiatedMapId { get; set; }
        
        public CurrentMapInstanceMessage()
        {
        }
        
        public CurrentMapInstanceMessage(double mapId, string mapKey, double instantiatedMapId)
         : base(mapId, mapKey)
        {
            this.InstantiatedMapId = instantiatedMapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(InstantiatedMapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            InstantiatedMapId = reader.ReadDouble();
            if (InstantiatedMapId < 0 || InstantiatedMapId > 9007199254740990)
                throw new Exception("Forbidden value on InstantiatedMapId = " + InstantiatedMapId + ", it doesn't respect the following condition : instantiatedMapId < 0 || instantiatedMapId > 9007199254740990");
        }
        
    }
    
}