

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayShowActorMessage : NetworkMessage
    {
        public const uint ProtocolId = 5632;
        public override uint MessageID => ProtocolId;
        
        public Types.GameRolePlayActorInformations Informations { get; set; }
        
        public GameRolePlayShowActorMessage()
        {
        }
        
        public GameRolePlayShowActorMessage(Types.GameRolePlayActorInformations informations)
        {
            this.Informations = informations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Informations.TypeID);
            Informations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Informations = Types.ProtocolTypeManager.GetInstance<Types.GameRolePlayActorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
        
    }
    
}