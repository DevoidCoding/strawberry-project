

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChangeMapMessage : NetworkMessage
    {
        public const uint ProtocolId = 221;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        public bool Autopilot { get; set; }
        
        public ChangeMapMessage()
        {
        }
        
        public ChangeMapMessage(double mapId, bool autopilot)
        {
            this.MapId = mapId;
            this.Autopilot = autopilot;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
            writer.WriteBoolean(Autopilot);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            Autopilot = reader.ReadBoolean();
        }
        
    }
    
}