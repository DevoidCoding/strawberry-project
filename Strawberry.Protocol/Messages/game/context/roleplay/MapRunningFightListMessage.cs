

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapRunningFightListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5743;
        public override uint MessageID => ProtocolId;
        
        public Types.FightExternalInformations[] Fights { get; set; }
        
        public MapRunningFightListMessage()
        {
        }
        
        public MapRunningFightListMessage(Types.FightExternalInformations[] fights)
        {
            this.Fights = fights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Fights.Length);
            foreach (var entry in Fights)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Fights = new Types.FightExternalInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Fights[i] = new Types.FightExternalInformations();
                 Fights[i].Deserialize(reader);
            }
        }
        
    }
    
}