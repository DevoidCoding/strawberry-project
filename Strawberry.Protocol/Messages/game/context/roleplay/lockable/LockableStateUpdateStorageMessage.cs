

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LockableStateUpdateStorageMessage : LockableStateUpdateAbstractMessage
    {
        public new const uint ProtocolId = 5669;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        public uint ElementId { get; set; }
        
        public LockableStateUpdateStorageMessage()
        {
        }
        
        public LockableStateUpdateStorageMessage(double mapId, uint elementId)
        {
            this.MapId = mapId;
            this.ElementId = elementId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(MapId);
            writer.WriteVarInt(ElementId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            ElementId = reader.ReadVarUhInt();
            if (ElementId < 0)
                throw new Exception("Forbidden value on ElementId = " + ElementId + ", it doesn't respect the following condition : elementId < 0");
        }
        
    }
    
}