

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LockableShowCodeDialogMessage : NetworkMessage
    {
        public const uint ProtocolId = 5740;
        public override uint MessageID => ProtocolId;
        
        public bool ChangeOrUse { get; set; }
        public sbyte CodeSize { get; set; }
        
        public LockableShowCodeDialogMessage()
        {
        }
        
        public LockableShowCodeDialogMessage(bool changeOrUse, sbyte codeSize)
        {
            this.ChangeOrUse = changeOrUse;
            this.CodeSize = codeSize;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(ChangeOrUse);
            writer.WriteSByte(CodeSize);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ChangeOrUse = reader.ReadBoolean();
            CodeSize = reader.ReadSByte();
            if (CodeSize < 0)
                throw new Exception("Forbidden value on CodeSize = " + CodeSize + ", it doesn't respect the following condition : codeSize < 0");
        }
        
    }
    
}