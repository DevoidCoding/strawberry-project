

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LockableUseCodeMessage : NetworkMessage
    {
        public const uint ProtocolId = 5667;
        public override uint MessageID => ProtocolId;
        
        public string Code { get; set; }
        
        public LockableUseCodeMessage()
        {
        }
        
        public LockableUseCodeMessage(string code)
        {
            this.Code = code;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Code);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Code = reader.ReadUTF();
        }
        
    }
    
}