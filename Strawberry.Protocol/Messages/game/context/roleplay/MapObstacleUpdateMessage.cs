

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapObstacleUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6051;
        public override uint MessageID => ProtocolId;
        
        public Types.MapObstacle[] Obstacles { get; set; }
        
        public MapObstacleUpdateMessage()
        {
        }
        
        public MapObstacleUpdateMessage(Types.MapObstacle[] obstacles)
        {
            this.Obstacles = obstacles;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Obstacles.Length);
            foreach (var entry in Obstacles)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Obstacles = new Types.MapObstacle[limit];
            for (int i = 0; i < limit; i++)
            {
                 Obstacles[i] = new Types.MapObstacle();
                 Obstacles[i].Deserialize(reader);
            }
        }
        
    }
    
}