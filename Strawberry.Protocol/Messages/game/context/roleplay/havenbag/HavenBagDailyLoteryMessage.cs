

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HavenBagDailyLoteryMessage : NetworkMessage
    {
        public const uint ProtocolId = 6644;
        public override uint MessageID => ProtocolId;
        
        public sbyte ReturnType { get; set; }
        public string TokenId { get; set; }
        
        public HavenBagDailyLoteryMessage()
        {
        }
        
        public HavenBagDailyLoteryMessage(sbyte returnType, string tokenId)
        {
            this.ReturnType = returnType;
            this.TokenId = tokenId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ReturnType);
            writer.WriteUTF(TokenId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ReturnType = reader.ReadSByte();
            if (ReturnType < 0)
                throw new Exception("Forbidden value on ReturnType = " + ReturnType + ", it doesn't respect the following condition : returnType < 0");
            TokenId = reader.ReadUTF();
        }
        
    }
    
}