

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class RoomAvailableUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6630;
        public override uint MessageID => ProtocolId;
        
        public byte NbRoom { get; set; }
        
        public RoomAvailableUpdateMessage()
        {
        }
        
        public RoomAvailableUpdateMessage(byte nbRoom)
        {
            this.NbRoom = nbRoom;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(NbRoom);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NbRoom = reader.ReadByte();
            if (NbRoom < 0 || NbRoom > 255)
                throw new Exception("Forbidden value on NbRoom = " + NbRoom + ", it doesn't respect the following condition : nbRoom < 0 || nbRoom > 255");
        }
        
    }
    
}