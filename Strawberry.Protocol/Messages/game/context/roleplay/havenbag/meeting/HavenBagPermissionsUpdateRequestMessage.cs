

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HavenBagPermissionsUpdateRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6714;
        public override uint MessageID => ProtocolId;
        
        public int Permissions { get; set; }
        
        public HavenBagPermissionsUpdateRequestMessage()
        {
        }
        
        public HavenBagPermissionsUpdateRequestMessage(int permissions)
        {
            this.Permissions = permissions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Permissions);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Permissions = reader.ReadInt();
            if (Permissions < 0)
                throw new Exception("Forbidden value on Permissions = " + Permissions + ", it doesn't respect the following condition : permissions < 0");
        }
        
    }
    
}