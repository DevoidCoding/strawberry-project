

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportHavenBagRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6647;
        public override uint MessageID => ProtocolId;
        
        public ulong GuestId { get; set; }
        
        public TeleportHavenBagRequestMessage()
        {
        }
        
        public TeleportHavenBagRequestMessage(ulong guestId)
        {
            this.GuestId = guestId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(GuestId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuestId = reader.ReadVarUhLong();
            if (GuestId < 0 || GuestId > 9007199254740990)
                throw new Exception("Forbidden value on GuestId = " + GuestId + ", it doesn't respect the following condition : guestId < 0 || guestId > 9007199254740990");
        }
        
    }
    
}