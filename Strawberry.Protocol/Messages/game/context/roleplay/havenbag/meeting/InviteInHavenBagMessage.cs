

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InviteInHavenBagMessage : NetworkMessage
    {
        public const uint ProtocolId = 6642;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterMinimalInformations GuestInformations { get; set; }
        public bool Accept { get; set; }
        
        public InviteInHavenBagMessage()
        {
        }
        
        public InviteInHavenBagMessage(Types.CharacterMinimalInformations guestInformations, bool accept)
        {
            this.GuestInformations = guestInformations;
            this.Accept = accept;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            GuestInformations.Serialize(writer);
            writer.WriteBoolean(Accept);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuestInformations = new Types.CharacterMinimalInformations();
            GuestInformations.Deserialize(reader);
            Accept = reader.ReadBoolean();
        }
        
    }
    
}