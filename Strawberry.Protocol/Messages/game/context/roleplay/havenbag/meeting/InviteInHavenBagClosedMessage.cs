

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InviteInHavenBagClosedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6645;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterMinimalInformations HostInformations { get; set; }
        
        public InviteInHavenBagClosedMessage()
        {
        }
        
        public InviteInHavenBagClosedMessage(Types.CharacterMinimalInformations hostInformations)
        {
            this.HostInformations = hostInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            HostInformations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HostInformations = new Types.CharacterMinimalInformations();
            HostInformations.Deserialize(reader);
        }
        
    }
    
}