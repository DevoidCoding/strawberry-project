

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HavenBagFurnituresRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6637;
        public override uint MessageID => ProtocolId;
        
        public ushort[] CellIds { get; set; }
        public int[] FunitureIds { get; set; }
        public sbyte[] Orientations { get; set; }
        
        public HavenBagFurnituresRequestMessage()
        {
        }
        
        public HavenBagFurnituresRequestMessage(ushort[] cellIds, int[] funitureIds, sbyte[] orientations)
        {
            this.CellIds = cellIds;
            this.FunitureIds = funitureIds;
            this.Orientations = orientations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)CellIds.Length);
            foreach (var entry in CellIds)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)FunitureIds.Length);
            foreach (var entry in FunitureIds)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteUShort((ushort)Orientations.Length);
            foreach (var entry in Orientations)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            CellIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 CellIds[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            FunitureIds = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 FunitureIds[i] = reader.ReadInt();
            }
            limit = reader.ReadUShort();
            Orientations = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Orientations[i] = reader.ReadSByte();
            }
        }
        
    }
    
}