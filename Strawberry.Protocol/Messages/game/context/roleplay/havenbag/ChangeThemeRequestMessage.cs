

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChangeThemeRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6639;
        public override uint MessageID => ProtocolId;
        
        public sbyte Theme { get; set; }
        
        public ChangeThemeRequestMessage()
        {
        }
        
        public ChangeThemeRequestMessage(sbyte theme)
        {
            this.Theme = theme;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Theme);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Theme = reader.ReadSByte();
        }
        
    }
    
}