

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EnterHavenBagRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6636;
        public override uint MessageID => ProtocolId;
        
        public ulong HavenBagOwner { get; set; }
        
        public EnterHavenBagRequestMessage()
        {
        }
        
        public EnterHavenBagRequestMessage(ulong havenBagOwner)
        {
            this.HavenBagOwner = havenBagOwner;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(HavenBagOwner);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HavenBagOwner = reader.ReadVarUhLong();
            if (HavenBagOwner < 0 || HavenBagOwner > 9007199254740990)
                throw new Exception("Forbidden value on HavenBagOwner = " + HavenBagOwner + ", it doesn't respect the following condition : havenBagOwner < 0 || havenBagOwner > 9007199254740990");
        }
        
    }
    
}