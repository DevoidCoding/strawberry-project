

// Generated on 02/12/2018 03:56:23
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChangeHavenBagRoomRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6638;
        public override uint MessageID => ProtocolId;
        
        public sbyte RoomId { get; set; }
        
        public ChangeHavenBagRoomRequestMessage()
        {
        }
        
        public ChangeHavenBagRoomRequestMessage(sbyte roomId)
        {
            this.RoomId = roomId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(RoomId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RoomId = reader.ReadSByte();
            if (RoomId < 0)
                throw new Exception("Forbidden value on RoomId = " + RoomId + ", it doesn't respect the following condition : roomId < 0");
        }
        
    }
    
}