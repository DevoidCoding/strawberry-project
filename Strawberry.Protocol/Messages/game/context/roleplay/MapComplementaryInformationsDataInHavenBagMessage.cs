

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapComplementaryInformationsDataInHavenBagMessage : MapComplementaryInformationsDataMessage
    {
        public new const uint ProtocolId = 6622;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterMinimalInformations OwnerInformations { get; set; }
        public sbyte Theme { get; set; }
        public sbyte RoomId { get; set; }
        public sbyte MaxRoomId { get; set; }
        
        public MapComplementaryInformationsDataInHavenBagMessage()
        {
        }
        
        public MapComplementaryInformationsDataInHavenBagMessage(ushort subAreaId, double mapId, Types.HouseInformations[] houses, Types.GameRolePlayActorInformations[] actors, Types.InteractiveElement[] interactiveElements, Types.StatedElement[] statedElements, Types.MapObstacle[] obstacles, Types.FightCommonInformations[] fights, bool hasAggressiveMonsters, Types.FightStartingPositions fightStartPositions, Types.CharacterMinimalInformations ownerInformations, sbyte theme, sbyte roomId, sbyte maxRoomId)
         : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            this.OwnerInformations = ownerInformations;
            this.Theme = theme;
            this.RoomId = roomId;
            this.MaxRoomId = maxRoomId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            OwnerInformations.Serialize(writer);
            writer.WriteSByte(Theme);
            writer.WriteSByte(RoomId);
            writer.WriteSByte(MaxRoomId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            OwnerInformations = new Types.CharacterMinimalInformations();
            OwnerInformations.Deserialize(reader);
            Theme = reader.ReadSByte();
            RoomId = reader.ReadSByte();
            if (RoomId < 0)
                throw new Exception("Forbidden value on RoomId = " + RoomId + ", it doesn't respect the following condition : roomId < 0");
            MaxRoomId = reader.ReadSByte();
            if (MaxRoomId < 0)
                throw new Exception("Forbidden value on MaxRoomId = " + MaxRoomId + ", it doesn't respect the following condition : maxRoomId < 0");
        }
        
    }
    
}