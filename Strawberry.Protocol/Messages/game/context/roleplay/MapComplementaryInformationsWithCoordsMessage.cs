

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapComplementaryInformationsWithCoordsMessage : MapComplementaryInformationsDataMessage
    {
        public new const uint ProtocolId = 6268;
        public override uint MessageID => ProtocolId;
        
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        
        public MapComplementaryInformationsWithCoordsMessage()
        {
        }
        
        public MapComplementaryInformationsWithCoordsMessage(ushort subAreaId, double mapId, Types.HouseInformations[] houses, Types.GameRolePlayActorInformations[] actors, Types.InteractiveElement[] interactiveElements, Types.StatedElement[] statedElements, Types.MapObstacle[] obstacles, Types.FightCommonInformations[] fights, bool hasAggressiveMonsters, Types.FightStartingPositions fightStartPositions, short worldX, short worldY)
         : base(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions)
        {
            this.WorldX = worldX;
            this.WorldY = worldY;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
        }
        
    }
    
}