

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DungeonPartyFinderAvailableDungeonsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6242;
        public override uint MessageID => ProtocolId;
        
        public ushort[] DungeonIds { get; set; }
        
        public DungeonPartyFinderAvailableDungeonsMessage()
        {
        }
        
        public DungeonPartyFinderAvailableDungeonsMessage(ushort[] dungeonIds)
        {
            this.DungeonIds = dungeonIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)DungeonIds.Length);
            foreach (var entry in DungeonIds)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            DungeonIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 DungeonIds[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}