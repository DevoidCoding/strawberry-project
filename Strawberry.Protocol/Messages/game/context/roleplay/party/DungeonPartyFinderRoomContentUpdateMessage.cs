

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DungeonPartyFinderRoomContentUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6250;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        public Types.DungeonPartyFinderPlayer[] AddedPlayers { get; set; }
        public ulong[] RemovedPlayersIds { get; set; }
        
        public DungeonPartyFinderRoomContentUpdateMessage()
        {
        }
        
        public DungeonPartyFinderRoomContentUpdateMessage(ushort dungeonId, Types.DungeonPartyFinderPlayer[] addedPlayers, ulong[] removedPlayersIds)
        {
            this.DungeonId = dungeonId;
            this.AddedPlayers = addedPlayers;
            this.RemovedPlayersIds = removedPlayersIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteUShort((ushort)AddedPlayers.Length);
            foreach (var entry in AddedPlayers)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)RemovedPlayersIds.Length);
            foreach (var entry in RemovedPlayersIds)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
            var limit = reader.ReadUShort();
            AddedPlayers = new Types.DungeonPartyFinderPlayer[limit];
            for (int i = 0; i < limit; i++)
            {
                 AddedPlayers[i] = new Types.DungeonPartyFinderPlayer();
                 AddedPlayers[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            RemovedPlayersIds = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 RemovedPlayersIds[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}