

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyInvitationDetailsMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6263;
        public override uint MessageID => ProtocolId;
        
        public sbyte PartyType { get; set; }
        public string PartyName { get; set; }
        public ulong FromId { get; set; }
        public string FromName { get; set; }
        public ulong LeaderId { get; set; }
        public Types.PartyInvitationMemberInformations[] Members { get; set; }
        public Types.PartyGuestInformations[] Guests { get; set; }
        
        public PartyInvitationDetailsMessage()
        {
        }
        
        public PartyInvitationDetailsMessage(uint partyId, sbyte partyType, string partyName, ulong fromId, string fromName, ulong leaderId, Types.PartyInvitationMemberInformations[] members, Types.PartyGuestInformations[] guests)
         : base(partyId)
        {
            this.PartyType = partyType;
            this.PartyName = partyName;
            this.FromId = fromId;
            this.FromName = fromName;
            this.LeaderId = leaderId;
            this.Members = members;
            this.Guests = guests;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PartyType);
            writer.WriteUTF(PartyName);
            writer.WriteVarLong(FromId);
            writer.WriteUTF(FromName);
            writer.WriteVarLong(LeaderId);
            writer.WriteUShort((ushort)Members.Length);
            foreach (var entry in Members)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Guests.Length);
            foreach (var entry in Guests)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadSByte();
            if (PartyType < 0)
                throw new Exception("Forbidden value on PartyType = " + PartyType + ", it doesn't respect the following condition : partyType < 0");
            PartyName = reader.ReadUTF();
            FromId = reader.ReadVarUhLong();
            if (FromId < 0 || FromId > 9007199254740990)
                throw new Exception("Forbidden value on FromId = " + FromId + ", it doesn't respect the following condition : fromId < 0 || fromId > 9007199254740990");
            FromName = reader.ReadUTF();
            LeaderId = reader.ReadVarUhLong();
            if (LeaderId < 0 || LeaderId > 9007199254740990)
                throw new Exception("Forbidden value on LeaderId = " + LeaderId + ", it doesn't respect the following condition : leaderId < 0 || leaderId > 9007199254740990");
            var limit = reader.ReadUShort();
            Members = new Types.PartyInvitationMemberInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Members[i] = new Types.PartyInvitationMemberInformations();
                 Members[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Guests = new Types.PartyGuestInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Guests[i] = new Types.PartyGuestInformations();
                 Guests[i].Deserialize(reader);
            }
        }
        
    }
    
}