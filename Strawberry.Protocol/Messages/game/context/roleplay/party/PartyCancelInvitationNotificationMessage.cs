

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyCancelInvitationNotificationMessage : AbstractPartyEventMessage
    {
        public new const uint ProtocolId = 6251;
        public override uint MessageID => ProtocolId;
        
        public ulong CancelerId { get; set; }
        public ulong GuestId { get; set; }
        
        public PartyCancelInvitationNotificationMessage()
        {
        }
        
        public PartyCancelInvitationNotificationMessage(uint partyId, ulong cancelerId, ulong guestId)
         : base(partyId)
        {
            this.CancelerId = cancelerId;
            this.GuestId = guestId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CancelerId);
            writer.WriteVarLong(GuestId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CancelerId = reader.ReadVarUhLong();
            if (CancelerId < 0 || CancelerId > 9007199254740990)
                throw new Exception("Forbidden value on CancelerId = " + CancelerId + ", it doesn't respect the following condition : cancelerId < 0 || cancelerId > 9007199254740990");
            GuestId = reader.ReadVarUhLong();
            if (GuestId < 0 || GuestId > 9007199254740990)
                throw new Exception("Forbidden value on GuestId = " + GuestId + ", it doesn't respect the following condition : guestId < 0 || guestId > 9007199254740990");
        }
        
    }
    
}