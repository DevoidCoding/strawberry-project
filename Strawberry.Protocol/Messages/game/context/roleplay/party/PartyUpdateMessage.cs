

// Generated on 02/12/2018 03:56:29
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyUpdateMessage : AbstractPartyEventMessage
    {
        public new const uint ProtocolId = 5575;
        public override uint MessageID => ProtocolId;
        
        public Types.PartyMemberInformations MemberInformations { get; set; }
        
        public PartyUpdateMessage()
        {
        }
        
        public PartyUpdateMessage(uint partyId, Types.PartyMemberInformations memberInformations)
         : base(partyId)
        {
            this.MemberInformations = memberInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MemberInformations.TypeID);
            MemberInformations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MemberInformations = Types.ProtocolTypeManager.GetInstance<Types.PartyMemberInformations>(reader.ReadShort());
            MemberInformations.Deserialize(reader);
        }
        
    }
    
}