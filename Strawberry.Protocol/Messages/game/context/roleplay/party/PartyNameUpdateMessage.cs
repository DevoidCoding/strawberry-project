

// Generated on 02/12/2018 03:56:29
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyNameUpdateMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6502;
        public override uint MessageID => ProtocolId;
        
        public string PartyName { get; set; }
        
        public PartyNameUpdateMessage()
        {
        }
        
        public PartyNameUpdateMessage(uint partyId, string partyName)
         : base(partyId)
        {
            this.PartyName = partyName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(PartyName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PartyName = reader.ReadUTF();
        }
        
    }
    
}