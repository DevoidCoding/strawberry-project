

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyMemberRemoveMessage : AbstractPartyEventMessage
    {
        public new const uint ProtocolId = 5579;
        public override uint MessageID => ProtocolId;
        
        public ulong LeavingPlayerId { get; set; }
        
        public PartyMemberRemoveMessage()
        {
        }
        
        public PartyMemberRemoveMessage(uint partyId, ulong leavingPlayerId)
         : base(partyId)
        {
            this.LeavingPlayerId = leavingPlayerId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(LeavingPlayerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LeavingPlayerId = reader.ReadVarUhLong();
            if (LeavingPlayerId < 0 || LeavingPlayerId > 9007199254740990)
                throw new Exception("Forbidden value on LeavingPlayerId = " + LeavingPlayerId + ", it doesn't respect the following condition : leavingPlayerId < 0 || leavingPlayerId > 9007199254740990");
        }
        
    }
    
}