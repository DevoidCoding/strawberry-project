

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyJoinMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 5576;
        public override uint MessageID => ProtocolId;
        
        public sbyte PartyType { get; set; }
        public ulong PartyLeaderId { get; set; }
        public sbyte MaxParticipants { get; set; }
        public Types.PartyMemberInformations[] Members { get; set; }
        public Types.PartyGuestInformations[] Guests { get; set; }
        public bool Restricted { get; set; }
        public string PartyName { get; set; }
        
        public PartyJoinMessage()
        {
        }
        
        public PartyJoinMessage(uint partyId, sbyte partyType, ulong partyLeaderId, sbyte maxParticipants, Types.PartyMemberInformations[] members, Types.PartyGuestInformations[] guests, bool restricted, string partyName)
         : base(partyId)
        {
            this.PartyType = partyType;
            this.PartyLeaderId = partyLeaderId;
            this.MaxParticipants = maxParticipants;
            this.Members = members;
            this.Guests = guests;
            this.Restricted = restricted;
            this.PartyName = partyName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PartyType);
            writer.WriteVarLong(PartyLeaderId);
            writer.WriteSByte(MaxParticipants);
            writer.WriteUShort((ushort)Members.Length);
            foreach (var entry in Members)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Guests.Length);
            foreach (var entry in Guests)
            {
                 entry.Serialize(writer);
            }
            writer.WriteBoolean(Restricted);
            writer.WriteUTF(PartyName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadSByte();
            if (PartyType < 0)
                throw new Exception("Forbidden value on PartyType = " + PartyType + ", it doesn't respect the following condition : partyType < 0");
            PartyLeaderId = reader.ReadVarUhLong();
            if (PartyLeaderId < 0 || PartyLeaderId > 9007199254740990)
                throw new Exception("Forbidden value on PartyLeaderId = " + PartyLeaderId + ", it doesn't respect the following condition : partyLeaderId < 0 || partyLeaderId > 9007199254740990");
            MaxParticipants = reader.ReadSByte();
            if (MaxParticipants < 0)
                throw new Exception("Forbidden value on MaxParticipants = " + MaxParticipants + ", it doesn't respect the following condition : maxParticipants < 0");
            var limit = reader.ReadUShort();
            Members = new Types.PartyMemberInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Members[i] = Types.ProtocolTypeManager.GetInstance<Types.PartyMemberInformations>(reader.ReadShort());
                 Members[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Guests = new Types.PartyGuestInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Guests[i] = new Types.PartyGuestInformations();
                 Guests[i].Deserialize(reader);
            }
            Restricted = reader.ReadBoolean();
            PartyName = reader.ReadUTF();
        }
        
    }
    
}