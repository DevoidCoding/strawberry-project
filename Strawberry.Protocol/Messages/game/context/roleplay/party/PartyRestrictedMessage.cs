

// Generated on 02/12/2018 03:56:29
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyRestrictedMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6175;
        public override uint MessageID => ProtocolId;
        
        public bool Restricted { get; set; }
        
        public PartyRestrictedMessage()
        {
        }
        
        public PartyRestrictedMessage(uint partyId, bool restricted)
         : base(partyId)
        {
            this.Restricted = restricted;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(Restricted);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Restricted = reader.ReadBoolean();
        }
        
    }
    
}