

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyMemberEjectedMessage : PartyMemberRemoveMessage
    {
        public new const uint ProtocolId = 6252;
        public override uint MessageID => ProtocolId;
        
        public ulong KickerId { get; set; }
        
        public PartyMemberEjectedMessage()
        {
        }
        
        public PartyMemberEjectedMessage(uint partyId, ulong leavingPlayerId, ulong kickerId)
         : base(partyId, leavingPlayerId)
        {
            this.KickerId = kickerId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(KickerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            KickerId = reader.ReadVarUhLong();
            if (KickerId < 0 || KickerId > 9007199254740990)
                throw new Exception("Forbidden value on KickerId = " + KickerId + ", it doesn't respect the following condition : kickerId < 0 || kickerId > 9007199254740990");
        }
        
    }
    
}