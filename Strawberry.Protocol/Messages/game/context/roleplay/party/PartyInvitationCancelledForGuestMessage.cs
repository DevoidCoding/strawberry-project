

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyInvitationCancelledForGuestMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6256;
        public override uint MessageID => ProtocolId;
        
        public ulong CancelerId { get; set; }
        
        public PartyInvitationCancelledForGuestMessage()
        {
        }
        
        public PartyInvitationCancelledForGuestMessage(uint partyId, ulong cancelerId)
         : base(partyId)
        {
            this.CancelerId = cancelerId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CancelerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CancelerId = reader.ReadVarUhLong();
            if (CancelerId < 0 || CancelerId > 9007199254740990)
                throw new Exception("Forbidden value on CancelerId = " + CancelerId + ", it doesn't respect the following condition : cancelerId < 0 || cancelerId > 9007199254740990");
        }
        
    }
    
}