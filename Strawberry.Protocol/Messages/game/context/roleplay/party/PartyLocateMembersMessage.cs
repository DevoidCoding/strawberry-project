

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyLocateMembersMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 5595;
        public override uint MessageID => ProtocolId;
        
        public Types.PartyMemberGeoPosition[] Geopositions { get; set; }
        
        public PartyLocateMembersMessage()
        {
        }
        
        public PartyLocateMembersMessage(uint partyId, Types.PartyMemberGeoPosition[] geopositions)
         : base(partyId)
        {
            this.Geopositions = geopositions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Geopositions.Length);
            foreach (var entry in Geopositions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Geopositions = new Types.PartyMemberGeoPosition[limit];
            for (int i = 0; i < limit; i++)
            {
                 Geopositions[i] = new Types.PartyMemberGeoPosition();
                 Geopositions[i].Deserialize(reader);
            }
        }
        
    }
    
}