

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyMemberInFightMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6342;
        public override uint MessageID => ProtocolId;
        
        public sbyte Reason { get; set; }
        public ulong MemberId { get; set; }
        public int MemberAccountId { get; set; }
        public string MemberName { get; set; }
        public ushort FightId { get; set; }
        public Types.MapCoordinatesExtended FightMap { get; set; }
        public short TimeBeforeFightStart { get; set; }
        
        public PartyMemberInFightMessage()
        {
        }
        
        public PartyMemberInFightMessage(uint partyId, sbyte reason, ulong memberId, int memberAccountId, string memberName, ushort fightId, Types.MapCoordinatesExtended fightMap, short timeBeforeFightStart)
         : base(partyId)
        {
            this.Reason = reason;
            this.MemberId = memberId;
            this.MemberAccountId = memberAccountId;
            this.MemberName = memberName;
            this.FightId = fightId;
            this.FightMap = fightMap;
            this.TimeBeforeFightStart = timeBeforeFightStart;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Reason);
            writer.WriteVarLong(MemberId);
            writer.WriteInt(MemberAccountId);
            writer.WriteUTF(MemberName);
            writer.WriteVarShort(FightId);
            FightMap.Serialize(writer);
            writer.WriteVarShort(TimeBeforeFightStart);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Reason = reader.ReadSByte();
            if (Reason < 0)
                throw new Exception("Forbidden value on Reason = " + Reason + ", it doesn't respect the following condition : reason < 0");
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
            MemberAccountId = reader.ReadInt();
            if (MemberAccountId < 0)
                throw new Exception("Forbidden value on MemberAccountId = " + MemberAccountId + ", it doesn't respect the following condition : memberAccountId < 0");
            MemberName = reader.ReadUTF();
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            FightMap = new Types.MapCoordinatesExtended();
            FightMap.Deserialize(reader);
            TimeBeforeFightStart = reader.ReadVarShort();
        }
        
    }
    
}