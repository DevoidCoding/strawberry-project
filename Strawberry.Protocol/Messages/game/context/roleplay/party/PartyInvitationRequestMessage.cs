

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class PartyInvitationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5585;
        public override uint MessageID => ProtocolId;
        
        public string Name { get; set; }
        
        public PartyInvitationRequestMessage()
        {
        }
        
        public PartyInvitationRequestMessage(string name)
        {
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Name = reader.ReadUTF();
        }
        
    }
    
}