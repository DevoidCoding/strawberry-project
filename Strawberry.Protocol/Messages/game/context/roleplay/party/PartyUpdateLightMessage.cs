

// Generated on 02/12/2018 03:56:29
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyUpdateLightMessage : AbstractPartyEventMessage
    {
        public new const uint ProtocolId = 6054;
        public override uint MessageID => ProtocolId;
        
        public ulong Id { get; set; }
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        public ushort Prospecting { get; set; }
        public byte RegenRate { get; set; }
        
        public PartyUpdateLightMessage()
        {
        }
        
        public PartyUpdateLightMessage(uint partyId, ulong id, uint lifePoints, uint maxLifePoints, ushort prospecting, byte regenRate)
         : base(partyId)
        {
            this.Id = id;
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
            this.Prospecting = prospecting;
            this.RegenRate = regenRate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Id);
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
            writer.WriteVarShort(Prospecting);
            writer.WriteByte(RegenRate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Id = reader.ReadVarUhLong();
            if (Id < 0 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0 || id > 9007199254740990");
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
            Prospecting = reader.ReadVarUhShort();
            if (Prospecting < 0)
                throw new Exception("Forbidden value on Prospecting = " + Prospecting + ", it doesn't respect the following condition : prospecting < 0");
            RegenRate = reader.ReadByte();
            if (RegenRate < 0 || RegenRate > 255)
                throw new Exception("Forbidden value on RegenRate = " + RegenRate + ", it doesn't respect the following condition : regenRate < 0 || regenRate > 255");
        }
        
    }
    
}