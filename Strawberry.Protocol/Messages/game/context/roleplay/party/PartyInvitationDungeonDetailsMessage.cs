

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyInvitationDungeonDetailsMessage : PartyInvitationDetailsMessage
    {
        public new const uint ProtocolId = 6262;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        public bool[] PlayersDungeonReady { get; set; }
        
        public PartyInvitationDungeonDetailsMessage()
        {
        }
        
        public PartyInvitationDungeonDetailsMessage(uint partyId, sbyte partyType, string partyName, ulong fromId, string fromName, ulong leaderId, Types.PartyInvitationMemberInformations[] members, Types.PartyGuestInformations[] guests, ushort dungeonId, bool[] playersDungeonReady)
         : base(partyId, partyType, partyName, fromId, fromName, leaderId, members, guests)
        {
            this.DungeonId = dungeonId;
            this.PlayersDungeonReady = playersDungeonReady;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(DungeonId);
            writer.WriteUShort((ushort)PlayersDungeonReady.Length);
            foreach (var entry in PlayersDungeonReady)
            {
                 writer.WriteBoolean(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
            var limit = reader.ReadUShort();
            PlayersDungeonReady = new bool[limit];
            for (int i = 0; i < limit; i++)
            {
                 PlayersDungeonReady[i] = reader.ReadBoolean();
            }
        }
        
    }
    
}