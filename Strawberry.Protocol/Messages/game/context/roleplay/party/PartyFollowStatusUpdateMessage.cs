

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyFollowStatusUpdateMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 5581;
        public override uint MessageID => ProtocolId;
        
        public bool Success { get; set; }
        public bool IsFollowed { get; set; }
        public ulong FollowedId { get; set; }
        
        public PartyFollowStatusUpdateMessage()
        {
        }
        
        public PartyFollowStatusUpdateMessage(uint partyId, bool success, bool isFollowed, ulong followedId)
         : base(partyId)
        {
            this.Success = success;
            this.IsFollowed = isFollowed;
            this.FollowedId = followedId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Success);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsFollowed);
            writer.WriteByte(flag1);
            writer.WriteVarLong(FollowedId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag1, 0);
            IsFollowed = BooleanByteWrapper.GetFlag(flag1, 1);
            FollowedId = reader.ReadVarUhLong();
            if (FollowedId < 0 || FollowedId > 9007199254740990)
                throw new Exception("Forbidden value on FollowedId = " + FollowedId + ", it doesn't respect the following condition : followedId < 0 || followedId > 9007199254740990");
        }
        
    }
    
}