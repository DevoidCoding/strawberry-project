

// Generated on 02/12/2018 03:56:28
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyInvitationMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 5586;
        public override uint MessageID => ProtocolId;
        
        public sbyte PartyType { get; set; }
        public string PartyName { get; set; }
        public sbyte MaxParticipants { get; set; }
        public ulong FromId { get; set; }
        public string FromName { get; set; }
        public ulong ToId { get; set; }
        
        public PartyInvitationMessage()
        {
        }
        
        public PartyInvitationMessage(uint partyId, sbyte partyType, string partyName, sbyte maxParticipants, ulong fromId, string fromName, ulong toId)
         : base(partyId)
        {
            this.PartyType = partyType;
            this.PartyName = partyName;
            this.MaxParticipants = maxParticipants;
            this.FromId = fromId;
            this.FromName = fromName;
            this.ToId = toId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(PartyType);
            writer.WriteUTF(PartyName);
            writer.WriteSByte(MaxParticipants);
            writer.WriteVarLong(FromId);
            writer.WriteUTF(FromName);
            writer.WriteVarLong(ToId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PartyType = reader.ReadSByte();
            if (PartyType < 0)
                throw new Exception("Forbidden value on PartyType = " + PartyType + ", it doesn't respect the following condition : partyType < 0");
            PartyName = reader.ReadUTF();
            MaxParticipants = reader.ReadSByte();
            if (MaxParticipants < 0)
                throw new Exception("Forbidden value on MaxParticipants = " + MaxParticipants + ", it doesn't respect the following condition : maxParticipants < 0");
            FromId = reader.ReadVarUhLong();
            if (FromId < 0 || FromId > 9007199254740990)
                throw new Exception("Forbidden value on FromId = " + FromId + ", it doesn't respect the following condition : fromId < 0 || fromId > 9007199254740990");
            FromName = reader.ReadUTF();
            ToId = reader.ReadVarUhLong();
            if (ToId < 0 || ToId > 9007199254740990)
                throw new Exception("Forbidden value on ToId = " + ToId + ", it doesn't respect the following condition : toId < 0 || toId > 9007199254740990");
        }
        
    }
    
}