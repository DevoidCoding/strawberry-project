

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AbstractPartyMessage : NetworkMessage
    {
        public const uint ProtocolId = 6274;
        public override uint MessageID => ProtocolId;
        
        public uint PartyId { get; set; }
        
        public AbstractPartyMessage()
        {
        }
        
        public AbstractPartyMessage(uint partyId)
        {
            this.PartyId = partyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(PartyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PartyId = reader.ReadVarUhInt();
            if (PartyId < 0)
                throw new Exception("Forbidden value on PartyId = " + PartyId + ", it doesn't respect the following condition : partyId < 0");
        }
        
    }
    
}