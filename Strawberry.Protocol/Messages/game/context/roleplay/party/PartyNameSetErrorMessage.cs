

// Generated on 02/12/2018 03:56:29
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PartyNameSetErrorMessage : AbstractPartyMessage
    {
        public new const uint ProtocolId = 6501;
        public override uint MessageID => ProtocolId;
        
        public sbyte Result { get; set; }
        
        public PartyNameSetErrorMessage()
        {
        }
        
        public PartyNameSetErrorMessage(uint partyId, sbyte result)
         : base(partyId)
        {
            this.Result = result;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Result);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Result = reader.ReadSByte();
            if (Result < 0)
                throw new Exception("Forbidden value on Result = " + Result + ", it doesn't respect the following condition : result < 0");
        }
        
    }
    
}