

// Generated on 02/12/2018 03:56:27
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DungeonPartyFinderRoomContentMessage : NetworkMessage
    {
        public const uint ProtocolId = 6247;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        public Types.DungeonPartyFinderPlayer[] Players { get; set; }
        
        public DungeonPartyFinderRoomContentMessage()
        {
        }
        
        public DungeonPartyFinderRoomContentMessage(ushort dungeonId, Types.DungeonPartyFinderPlayer[] players)
        {
            this.DungeonId = dungeonId;
            this.Players = players;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteUShort((ushort)Players.Length);
            foreach (var entry in Players)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
            var limit = reader.ReadUShort();
            Players = new Types.DungeonPartyFinderPlayer[limit];
            for (int i = 0; i < limit; i++)
            {
                 Players[i] = new Types.DungeonPartyFinderPlayer();
                 Players[i].Deserialize(reader);
            }
        }
        
    }
    
}