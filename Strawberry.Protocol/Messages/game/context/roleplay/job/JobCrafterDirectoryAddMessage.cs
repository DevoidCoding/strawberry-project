

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5651;
        public override uint MessageID => ProtocolId;
        
        public Types.JobCrafterDirectoryListEntry ListEntry { get; set; }
        
        public JobCrafterDirectoryAddMessage()
        {
        }
        
        public JobCrafterDirectoryAddMessage(Types.JobCrafterDirectoryListEntry listEntry)
        {
            this.ListEntry = listEntry;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            ListEntry.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ListEntry = new Types.JobCrafterDirectoryListEntry();
            ListEntry.Deserialize(reader);
        }
        
    }
    
}