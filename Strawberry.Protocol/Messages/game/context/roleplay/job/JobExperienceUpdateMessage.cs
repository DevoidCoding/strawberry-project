

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobExperienceUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5654;
        public override uint MessageID => ProtocolId;
        
        public Types.JobExperience ExperiencesUpdate { get; set; }
        
        public JobExperienceUpdateMessage()
        {
        }
        
        public JobExperienceUpdateMessage(Types.JobExperience experiencesUpdate)
        {
            this.ExperiencesUpdate = experiencesUpdate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            ExperiencesUpdate.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ExperiencesUpdate = new Types.JobExperience();
            ExperiencesUpdate.Deserialize(reader);
        }
        
    }
    
}