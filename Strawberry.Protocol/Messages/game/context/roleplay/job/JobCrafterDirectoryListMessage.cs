

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6046;
        public override uint MessageID => ProtocolId;
        
        public Types.JobCrafterDirectoryListEntry[] ListEntries { get; set; }
        
        public JobCrafterDirectoryListMessage()
        {
        }
        
        public JobCrafterDirectoryListMessage(Types.JobCrafterDirectoryListEntry[] listEntries)
        {
            this.ListEntries = listEntries;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ListEntries.Length);
            foreach (var entry in ListEntries)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ListEntries = new Types.JobCrafterDirectoryListEntry[limit];
            for (int i = 0; i < limit; i++)
            {
                 ListEntries[i] = new Types.JobCrafterDirectoryListEntry();
                 ListEntries[i].Deserialize(reader);
            }
        }
        
    }
    
}