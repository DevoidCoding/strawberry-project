

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobAllowMultiCraftRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5748;
        public override uint MessageID => ProtocolId;
        
        public bool Enabled { get; set; }
        
        public JobAllowMultiCraftRequestMessage()
        {
        }
        
        public JobAllowMultiCraftRequestMessage(bool enabled)
        {
            this.Enabled = enabled;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Enabled);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Enabled = reader.ReadBoolean();
        }
        
    }
    
}