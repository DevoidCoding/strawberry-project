

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobMultiCraftAvailableSkillsMessage : JobAllowMultiCraftRequestMessage
    {
        public new const uint ProtocolId = 5747;
        public override uint MessageID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public ushort[] Skills { get; set; }
        
        public JobMultiCraftAvailableSkillsMessage()
        {
        }
        
        public JobMultiCraftAvailableSkillsMessage(bool enabled, ulong playerId, ushort[] skills)
         : base(enabled)
        {
            this.PlayerId = playerId;
            this.Skills = skills;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUShort((ushort)Skills.Length);
            foreach (var entry in Skills)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            var limit = reader.ReadUShort();
            Skills = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Skills[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}