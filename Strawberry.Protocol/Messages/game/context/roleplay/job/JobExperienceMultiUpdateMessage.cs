

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobExperienceMultiUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5809;
        public override uint MessageID => ProtocolId;
        
        public Types.JobExperience[] ExperiencesUpdate { get; set; }
        
        public JobExperienceMultiUpdateMessage()
        {
        }
        
        public JobExperienceMultiUpdateMessage(Types.JobExperience[] experiencesUpdate)
        {
            this.ExperiencesUpdate = experiencesUpdate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ExperiencesUpdate.Length);
            foreach (var entry in ExperiencesUpdate)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ExperiencesUpdate = new Types.JobExperience[limit];
            for (int i = 0; i < limit; i++)
            {
                 ExperiencesUpdate[i] = new Types.JobExperience();
                 ExperiencesUpdate[i].Deserialize(reader);
            }
        }
        
    }
    
}