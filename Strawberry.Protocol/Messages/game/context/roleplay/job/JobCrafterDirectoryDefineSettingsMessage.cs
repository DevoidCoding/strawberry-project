

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryDefineSettingsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5649;
        public override uint MessageID => ProtocolId;
        
        public Types.JobCrafterDirectorySettings Settings { get; set; }
        
        public JobCrafterDirectoryDefineSettingsMessage()
        {
        }
        
        public JobCrafterDirectoryDefineSettingsMessage(Types.JobCrafterDirectorySettings settings)
        {
            this.Settings = settings;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Settings.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Settings = new Types.JobCrafterDirectorySettings();
            Settings.Deserialize(reader);
        }
        
    }
    
}