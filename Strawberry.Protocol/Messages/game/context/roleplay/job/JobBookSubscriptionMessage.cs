

// Generated on 02/12/2018 03:56:24
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobBookSubscriptionMessage : NetworkMessage
    {
        public const uint ProtocolId = 6593;
        public override uint MessageID => ProtocolId;
        
        public Types.JobBookSubscription[] Subscriptions { get; set; }
        
        public JobBookSubscriptionMessage()
        {
        }
        
        public JobBookSubscriptionMessage(Types.JobBookSubscription[] subscriptions)
        {
            this.Subscriptions = subscriptions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Subscriptions.Length);
            foreach (var entry in Subscriptions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Subscriptions = new Types.JobBookSubscription[limit];
            for (int i = 0; i < limit; i++)
            {
                 Subscriptions[i] = new Types.JobBookSubscription();
                 Subscriptions[i].Deserialize(reader);
            }
        }
        
    }
    
}