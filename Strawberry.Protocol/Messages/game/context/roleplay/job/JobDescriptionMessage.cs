

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobDescriptionMessage : NetworkMessage
    {
        public const uint ProtocolId = 5655;
        public override uint MessageID => ProtocolId;
        
        public Types.JobDescription[] JobsDescription { get; set; }
        
        public JobDescriptionMessage()
        {
        }
        
        public JobDescriptionMessage(Types.JobDescription[] jobsDescription)
        {
            this.JobsDescription = jobsDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)JobsDescription.Length);
            foreach (var entry in JobsDescription)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            JobsDescription = new Types.JobDescription[limit];
            for (int i = 0; i < limit; i++)
            {
                 JobsDescription[i] = new Types.JobDescription();
                 JobsDescription[i].Deserialize(reader);
            }
        }
        
    }
    
}