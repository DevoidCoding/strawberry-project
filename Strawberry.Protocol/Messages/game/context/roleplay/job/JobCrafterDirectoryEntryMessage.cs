

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryEntryMessage : NetworkMessage
    {
        public const uint ProtocolId = 6044;
        public override uint MessageID => ProtocolId;
        
        public Types.JobCrafterDirectoryEntryPlayerInfo PlayerInfo { get; set; }
        public Types.JobCrafterDirectoryEntryJobInfo[] JobInfoList { get; set; }
        public Types.EntityLook PlayerLook { get; set; }
        
        public JobCrafterDirectoryEntryMessage()
        {
        }
        
        public JobCrafterDirectoryEntryMessage(Types.JobCrafterDirectoryEntryPlayerInfo playerInfo, Types.JobCrafterDirectoryEntryJobInfo[] jobInfoList, Types.EntityLook playerLook)
        {
            this.PlayerInfo = playerInfo;
            this.JobInfoList = jobInfoList;
            this.PlayerLook = playerLook;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            PlayerInfo.Serialize(writer);
            writer.WriteUShort((ushort)JobInfoList.Length);
            foreach (var entry in JobInfoList)
            {
                 entry.Serialize(writer);
            }
            PlayerLook.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PlayerInfo = new Types.JobCrafterDirectoryEntryPlayerInfo();
            PlayerInfo.Deserialize(reader);
            var limit = reader.ReadUShort();
            JobInfoList = new Types.JobCrafterDirectoryEntryJobInfo[limit];
            for (int i = 0; i < limit; i++)
            {
                 JobInfoList[i] = new Types.JobCrafterDirectoryEntryJobInfo();
                 JobInfoList[i].Deserialize(reader);
            }
            PlayerLook = new Types.EntityLook();
            PlayerLook.Deserialize(reader);
        }
        
    }
    
}