

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryRemoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 5653;
        public override uint MessageID => ProtocolId;
        
        public sbyte JobId { get; set; }
        public ulong PlayerId { get; set; }
        
        public JobCrafterDirectoryRemoveMessage()
        {
        }
        
        public JobCrafterDirectoryRemoveMessage(sbyte jobId, ulong playerId)
        {
            this.JobId = jobId;
            this.PlayerId = playerId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
            writer.WriteVarLong(PlayerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
        }
        
    }
    
}