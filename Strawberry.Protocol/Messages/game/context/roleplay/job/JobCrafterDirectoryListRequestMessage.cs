

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobCrafterDirectoryListRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6047;
        public override uint MessageID => ProtocolId;
        
        public sbyte JobId { get; set; }
        
        public JobCrafterDirectoryListRequestMessage()
        {
        }
        
        public JobCrafterDirectoryListRequestMessage(sbyte jobId)
        {
            this.JobId = jobId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(JobId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            JobId = reader.ReadSByte();
            if (JobId < 0)
                throw new Exception("Forbidden value on JobId = " + JobId + ", it doesn't respect the following condition : jobId < 0");
        }
        
    }
    
}