

// Generated on 02/12/2018 03:56:25
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobLevelUpMessage : NetworkMessage
    {
        public const uint ProtocolId = 5656;
        public override uint MessageID => ProtocolId;
        
        public byte NewLevel { get; set; }
        public Types.JobDescription JobsDescription { get; set; }
        
        public JobLevelUpMessage()
        {
        }
        
        public JobLevelUpMessage(byte newLevel, Types.JobDescription jobsDescription)
        {
            this.NewLevel = newLevel;
            this.JobsDescription = jobsDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(NewLevel);
            JobsDescription.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NewLevel = reader.ReadByte();
            if (NewLevel < 0 || NewLevel > 255)
                throw new Exception("Forbidden value on NewLevel = " + NewLevel + ", it doesn't respect the following condition : newLevel < 0 || newLevel > 255");
            JobsDescription = new Types.JobDescription();
            JobsDescription.Deserialize(reader);
        }
        
    }
    
}