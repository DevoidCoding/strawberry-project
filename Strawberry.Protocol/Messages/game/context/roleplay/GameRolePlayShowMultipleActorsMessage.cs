

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayShowMultipleActorsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6712;
        public override uint MessageID => ProtocolId;
        
        public Types.GameRolePlayActorInformations[] InformationsList { get; set; }
        
        public GameRolePlayShowMultipleActorsMessage()
        {
        }
        
        public GameRolePlayShowMultipleActorsMessage(Types.GameRolePlayActorInformations[] informationsList)
        {
            this.InformationsList = informationsList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)InformationsList.Length);
            foreach (var entry in InformationsList)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            InformationsList = new Types.GameRolePlayActorInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 InformationsList[i] = Types.ProtocolTypeManager.GetInstance<Types.GameRolePlayActorInformations>(reader.ReadShort());
                 InformationsList[i].Deserialize(reader);
            }
        }
        
    }
    
}