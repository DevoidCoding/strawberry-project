

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectGroundRemovedMultipleMessage : NetworkMessage
    {
        public const uint ProtocolId = 5944;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Cells { get; set; }
        
        public ObjectGroundRemovedMultipleMessage()
        {
        }
        
        public ObjectGroundRemovedMultipleMessage(ushort[] cells)
        {
            this.Cells = cells;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Cells.Length);
            foreach (var entry in Cells)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Cells = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Cells[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}