

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectGroundAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 3017;
        public override uint MessageID => ProtocolId;
        
        public ushort CellId { get; set; }
        public ushort ObjectGID { get; set; }
        
        public ObjectGroundAddedMessage()
        {
        }
        
        public ObjectGroundAddedMessage(ushort cellId, ushort objectGID)
        {
            this.CellId = cellId;
            this.ObjectGID = objectGID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CellId);
            writer.WriteVarShort(ObjectGID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CellId = reader.ReadVarUhShort();
            if (CellId < 0 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0 || cellId > 559");
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
        }
        
    }
    
}