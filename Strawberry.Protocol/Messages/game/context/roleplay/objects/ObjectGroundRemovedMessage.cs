

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectGroundRemovedMessage : NetworkMessage
    {
        public const uint ProtocolId = 3014;
        public override uint MessageID => ProtocolId;
        
        public ushort Cell { get; set; }
        
        public ObjectGroundRemovedMessage()
        {
        }
        
        public ObjectGroundRemovedMessage(ushort cell)
        {
            this.Cell = cell;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Cell);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Cell = reader.ReadVarUhShort();
            if (Cell < 0 || Cell > 559)
                throw new Exception("Forbidden value on Cell = " + Cell + ", it doesn't respect the following condition : cell < 0 || cell > 559");
        }
        
    }
    
}