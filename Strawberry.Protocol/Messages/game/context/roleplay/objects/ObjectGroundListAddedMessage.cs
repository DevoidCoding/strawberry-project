

// Generated on 02/12/2018 03:56:26
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectGroundListAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5925;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Cells { get; set; }
        public ushort[] ReferenceIds { get; set; }
        
        public ObjectGroundListAddedMessage()
        {
        }
        
        public ObjectGroundListAddedMessage(ushort[] cells, ushort[] referenceIds)
        {
            this.Cells = cells;
            this.ReferenceIds = referenceIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Cells.Length);
            foreach (var entry in Cells)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)ReferenceIds.Length);
            foreach (var entry in ReferenceIds)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Cells = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Cells[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            ReferenceIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 ReferenceIds[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}