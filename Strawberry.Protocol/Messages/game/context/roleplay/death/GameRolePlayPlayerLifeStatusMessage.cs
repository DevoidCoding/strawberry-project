

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayPlayerLifeStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 5996;
        public override uint MessageID => ProtocolId;
        
        public sbyte State { get; set; }
        public double PhenixMapId { get; set; }
        
        public GameRolePlayPlayerLifeStatusMessage()
        {
        }
        
        public GameRolePlayPlayerLifeStatusMessage(sbyte state, double phenixMapId)
        {
            this.State = state;
            this.PhenixMapId = phenixMapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(State);
            writer.WriteDouble(PhenixMapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
            PhenixMapId = reader.ReadDouble();
            if (PhenixMapId < 0 || PhenixMapId > 9007199254740990)
                throw new Exception("Forbidden value on PhenixMapId = " + PhenixMapId + ", it doesn't respect the following condition : phenixMapId < 0 || phenixMapId > 9007199254740990");
        }
        
    }
    
}