

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayDelayedActionMessage : NetworkMessage
    {
        public const uint ProtocolId = 6153;
        public override uint MessageID => ProtocolId;
        
        public double DelayedCharacterId { get; set; }
        public sbyte DelayTypeId { get; set; }
        public double DelayEndTime { get; set; }
        
        public GameRolePlayDelayedActionMessage()
        {
        }
        
        public GameRolePlayDelayedActionMessage(double delayedCharacterId, sbyte delayTypeId, double delayEndTime)
        {
            this.DelayedCharacterId = delayedCharacterId;
            this.DelayTypeId = delayTypeId;
            this.DelayEndTime = delayEndTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DelayedCharacterId);
            writer.WriteSByte(DelayTypeId);
            writer.WriteDouble(DelayEndTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DelayedCharacterId = reader.ReadDouble();
            if (DelayedCharacterId < -9007199254740990 || DelayedCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on DelayedCharacterId = " + DelayedCharacterId + ", it doesn't respect the following condition : delayedCharacterId < -9007199254740990 || delayedCharacterId > 9007199254740990");
            DelayTypeId = reader.ReadSByte();
            if (DelayTypeId < 0)
                throw new Exception("Forbidden value on DelayTypeId = " + DelayTypeId + ", it doesn't respect the following condition : delayTypeId < 0");
            DelayEndTime = reader.ReadDouble();
            if (DelayEndTime < 0 || DelayEndTime > 9007199254740990)
                throw new Exception("Forbidden value on DelayEndTime = " + DelayEndTime + ", it doesn't respect the following condition : delayEndTime < 0 || delayEndTime > 9007199254740990");
        }
        
    }
    
}