

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameRolePlayDelayedActionFinishedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6150;
        public override uint MessageID => ProtocolId;
        
        public double DelayedCharacterId { get; set; }
        public sbyte DelayTypeId { get; set; }
        
        public GameRolePlayDelayedActionFinishedMessage()
        {
        }
        
        public GameRolePlayDelayedActionFinishedMessage(double delayedCharacterId, sbyte delayTypeId)
        {
            this.DelayedCharacterId = delayedCharacterId;
            this.DelayTypeId = delayTypeId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DelayedCharacterId);
            writer.WriteSByte(DelayTypeId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DelayedCharacterId = reader.ReadDouble();
            if (DelayedCharacterId < -9007199254740990 || DelayedCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on DelayedCharacterId = " + DelayedCharacterId + ", it doesn't respect the following condition : delayedCharacterId < -9007199254740990 || delayedCharacterId > 9007199254740990");
            DelayTypeId = reader.ReadSByte();
            if (DelayTypeId < 0)
                throw new Exception("Forbidden value on DelayTypeId = " + DelayTypeId + ", it doesn't respect the following condition : delayTypeId < 0");
        }
        
    }
    
}