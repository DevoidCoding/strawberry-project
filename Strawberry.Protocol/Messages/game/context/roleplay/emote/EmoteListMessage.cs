

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EmoteListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5689;
        public override uint MessageID => ProtocolId;
        
        public byte[] EmoteIds { get; set; }
        
        public EmoteListMessage()
        {
        }
        
        public EmoteListMessage(byte[] emoteIds)
        {
            this.EmoteIds = emoteIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)EmoteIds.Length);
            foreach (var entry in EmoteIds)
            {
                 writer.WriteByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            EmoteIds = new byte[limit];
            for (int i = 0; i < limit; i++)
            {
                 EmoteIds[i] = reader.ReadByte();
            }
        }
        
    }
    
}