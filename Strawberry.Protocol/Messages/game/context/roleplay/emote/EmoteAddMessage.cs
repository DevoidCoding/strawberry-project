

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EmoteAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5644;
        public override uint MessageID => ProtocolId;
        
        public byte EmoteId { get; set; }
        
        public EmoteAddMessage()
        {
        }
        
        public EmoteAddMessage(byte emoteId)
        {
            this.EmoteId = emoteId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(EmoteId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            EmoteId = reader.ReadByte();
            if (EmoteId < 0 || EmoteId > 255)
                throw new Exception("Forbidden value on EmoteId = " + EmoteId + ", it doesn't respect the following condition : emoteId < 0 || emoteId > 255");
        }
        
    }
    
}