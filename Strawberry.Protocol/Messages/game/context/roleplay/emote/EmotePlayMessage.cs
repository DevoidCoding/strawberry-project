

// Generated on 02/12/2018 03:56:22
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EmotePlayMessage : EmotePlayAbstractMessage
    {
        public new const uint ProtocolId = 5683;
        public override uint MessageID => ProtocolId;
        
        public double ActorId { get; set; }
        public int AccountId { get; set; }
        
        public EmotePlayMessage()
        {
        }
        
        public EmotePlayMessage(byte emoteId, double emoteStartTime, double actorId, int accountId)
         : base(emoteId, emoteStartTime)
        {
            this.ActorId = actorId;
            this.AccountId = accountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(ActorId);
            writer.WriteInt(AccountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ActorId = reader.ReadDouble();
            if (ActorId < -9007199254740990 || ActorId > 9007199254740990)
                throw new Exception("Forbidden value on ActorId = " + ActorId + ", it doesn't respect the following condition : actorId < -9007199254740990 || actorId > 9007199254740990");
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
        }
        
    }
    
}