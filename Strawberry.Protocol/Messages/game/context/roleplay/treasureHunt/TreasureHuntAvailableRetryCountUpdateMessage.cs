

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntAvailableRetryCountUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6491;
        public override uint MessageID => ProtocolId;
        
        public sbyte QuestType { get; set; }
        public int AvailableRetryCount { get; set; }
        
        public TreasureHuntAvailableRetryCountUpdateMessage()
        {
        }
        
        public TreasureHuntAvailableRetryCountUpdateMessage(sbyte questType, int availableRetryCount)
        {
            this.QuestType = questType;
            this.AvailableRetryCount = availableRetryCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(QuestType);
            writer.WriteInt(AvailableRetryCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestType = reader.ReadSByte();
            if (QuestType < 0)
                throw new Exception("Forbidden value on QuestType = " + QuestType + ", it doesn't respect the following condition : questType < 0");
            AvailableRetryCount = reader.ReadInt();
        }
        
    }
    
}