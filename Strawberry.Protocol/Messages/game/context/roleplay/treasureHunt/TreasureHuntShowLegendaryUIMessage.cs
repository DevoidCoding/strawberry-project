

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntShowLegendaryUIMessage : NetworkMessage
    {
        public const uint ProtocolId = 6498;
        public override uint MessageID => ProtocolId;
        
        public ushort[] AvailableLegendaryIds { get; set; }
        
        public TreasureHuntShowLegendaryUIMessage()
        {
        }
        
        public TreasureHuntShowLegendaryUIMessage(ushort[] availableLegendaryIds)
        {
            this.AvailableLegendaryIds = availableLegendaryIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)AvailableLegendaryIds.Length);
            foreach (var entry in AvailableLegendaryIds)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            AvailableLegendaryIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 AvailableLegendaryIds[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}