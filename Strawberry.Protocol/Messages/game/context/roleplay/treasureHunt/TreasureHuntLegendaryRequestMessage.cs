

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntLegendaryRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6499;
        public override uint MessageID => ProtocolId;
        
        public ushort LegendaryId { get; set; }
        
        public TreasureHuntLegendaryRequestMessage()
        {
        }
        
        public TreasureHuntLegendaryRequestMessage(ushort legendaryId)
        {
            this.LegendaryId = legendaryId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(LegendaryId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            LegendaryId = reader.ReadVarUhShort();
            if (LegendaryId < 0)
                throw new Exception("Forbidden value on LegendaryId = " + LegendaryId + ", it doesn't respect the following condition : legendaryId < 0");
        }
        
    }
    
}