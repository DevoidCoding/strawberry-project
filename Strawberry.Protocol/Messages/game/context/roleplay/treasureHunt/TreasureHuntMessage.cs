

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntMessage : NetworkMessage
    {
        public const uint ProtocolId = 6486;
        public override uint MessageID => ProtocolId;
        
        public sbyte QuestType { get; set; }
        public double StartMapId { get; set; }
        public Types.TreasureHuntStep[] KnownStepsList { get; set; }
        public sbyte TotalStepCount { get; set; }
        public uint CheckPointCurrent { get; set; }
        public uint CheckPointTotal { get; set; }
        public int AvailableRetryCount { get; set; }
        public Types.TreasureHuntFlag[] Flags { get; set; }
        
        public TreasureHuntMessage()
        {
        }
        
        public TreasureHuntMessage(sbyte questType, double startMapId, Types.TreasureHuntStep[] knownStepsList, sbyte totalStepCount, uint checkPointCurrent, uint checkPointTotal, int availableRetryCount, Types.TreasureHuntFlag[] flags)
        {
            this.QuestType = questType;
            this.StartMapId = startMapId;
            this.KnownStepsList = knownStepsList;
            this.TotalStepCount = totalStepCount;
            this.CheckPointCurrent = checkPointCurrent;
            this.CheckPointTotal = checkPointTotal;
            this.AvailableRetryCount = availableRetryCount;
            this.Flags = flags;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(QuestType);
            writer.WriteDouble(StartMapId);
            writer.WriteUShort((ushort)KnownStepsList.Length);
            foreach (var entry in KnownStepsList)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteSByte(TotalStepCount);
            writer.WriteVarInt(CheckPointCurrent);
            writer.WriteVarInt(CheckPointTotal);
            writer.WriteInt(AvailableRetryCount);
            writer.WriteUShort((ushort)Flags.Length);
            foreach (var entry in Flags)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestType = reader.ReadSByte();
            if (QuestType < 0)
                throw new Exception("Forbidden value on QuestType = " + QuestType + ", it doesn't respect the following condition : questType < 0");
            StartMapId = reader.ReadDouble();
            if (StartMapId < 0 || StartMapId > 9007199254740990)
                throw new Exception("Forbidden value on StartMapId = " + StartMapId + ", it doesn't respect the following condition : startMapId < 0 || startMapId > 9007199254740990");
            var limit = reader.ReadUShort();
            KnownStepsList = new Types.TreasureHuntStep[limit];
            for (int i = 0; i < limit; i++)
            {
                 KnownStepsList[i] = Types.ProtocolTypeManager.GetInstance<Types.TreasureHuntStep>(reader.ReadShort());
                 KnownStepsList[i].Deserialize(reader);
            }
            TotalStepCount = reader.ReadSByte();
            if (TotalStepCount < 0)
                throw new Exception("Forbidden value on TotalStepCount = " + TotalStepCount + ", it doesn't respect the following condition : totalStepCount < 0");
            CheckPointCurrent = reader.ReadVarUhInt();
            if (CheckPointCurrent < 0)
                throw new Exception("Forbidden value on CheckPointCurrent = " + CheckPointCurrent + ", it doesn't respect the following condition : checkPointCurrent < 0");
            CheckPointTotal = reader.ReadVarUhInt();
            if (CheckPointTotal < 0)
                throw new Exception("Forbidden value on CheckPointTotal = " + CheckPointTotal + ", it doesn't respect the following condition : checkPointTotal < 0");
            AvailableRetryCount = reader.ReadInt();
            limit = reader.ReadUShort();
            Flags = new Types.TreasureHuntFlag[limit];
            for (int i = 0; i < limit; i++)
            {
                 Flags[i] = new Types.TreasureHuntFlag();
                 Flags[i].Deserialize(reader);
            }
        }
        
    }
    
}