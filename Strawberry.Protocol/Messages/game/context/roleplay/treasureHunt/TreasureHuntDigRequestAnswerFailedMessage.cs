

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntDigRequestAnswerFailedMessage : TreasureHuntDigRequestAnswerMessage
    {
        public new const uint ProtocolId = 6509;
        public override uint MessageID => ProtocolId;
        
        public sbyte WrongFlagCount { get; set; }
        
        public TreasureHuntDigRequestAnswerFailedMessage()
        {
        }
        
        public TreasureHuntDigRequestAnswerFailedMessage(sbyte questType, sbyte result, sbyte wrongFlagCount)
         : base(questType, result)
        {
            this.WrongFlagCount = wrongFlagCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(WrongFlagCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            WrongFlagCount = reader.ReadSByte();
            if (WrongFlagCount < 0)
                throw new Exception("Forbidden value on WrongFlagCount = " + WrongFlagCount + ", it doesn't respect the following condition : wrongFlagCount < 0");
        }
        
    }
    
}