

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PortalUseRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6492;
        public override uint MessageID => ProtocolId;
        
        public uint PortalId { get; set; }
        
        public PortalUseRequestMessage()
        {
        }
        
        public PortalUseRequestMessage(uint portalId)
        {
            this.PortalId = portalId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(PortalId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PortalId = reader.ReadVarUhInt();
            if (PortalId < 0)
                throw new Exception("Forbidden value on PortalId = " + PortalId + ", it doesn't respect the following condition : portalId < 0");
        }
        
    }
    
}