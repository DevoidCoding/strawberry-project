

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6488;
        public override uint MessageID => ProtocolId;
        
        public ushort QuestLevel { get; set; }
        public sbyte QuestType { get; set; }
        
        public TreasureHuntRequestMessage()
        {
        }
        
        public TreasureHuntRequestMessage(ushort questLevel, sbyte questType)
        {
            this.QuestLevel = questLevel;
            this.QuestType = questType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(QuestLevel);
            writer.WriteSByte(QuestType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestLevel = reader.ReadVarUhShort();
            if (QuestLevel < 0)
                throw new Exception("Forbidden value on QuestLevel = " + QuestLevel + ", it doesn't respect the following condition : questLevel < 0");
            QuestType = reader.ReadSByte();
            if (QuestType < 0)
                throw new Exception("Forbidden value on QuestType = " + QuestType + ", it doesn't respect the following condition : questType < 0");
        }
        
    }
    
}