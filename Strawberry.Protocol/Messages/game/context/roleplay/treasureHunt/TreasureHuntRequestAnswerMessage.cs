

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TreasureHuntRequestAnswerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6489;
        public override uint MessageID => ProtocolId;
        
        public sbyte QuestType { get; set; }
        public sbyte Result { get; set; }
        
        public TreasureHuntRequestAnswerMessage()
        {
        }
        
        public TreasureHuntRequestAnswerMessage(sbyte questType, sbyte result)
        {
            this.QuestType = questType;
            this.Result = result;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(QuestType);
            writer.WriteSByte(Result);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            QuestType = reader.ReadSByte();
            if (QuestType < 0)
                throw new Exception("Forbidden value on QuestType = " + QuestType + ", it doesn't respect the following condition : questType < 0");
            Result = reader.ReadSByte();
            if (Result < 0)
                throw new Exception("Forbidden value on Result = " + Result + ", it doesn't respect the following condition : result < 0");
        }
        
    }
    
}