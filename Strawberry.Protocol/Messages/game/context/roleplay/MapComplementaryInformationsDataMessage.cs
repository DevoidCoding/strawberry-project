

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapComplementaryInformationsDataMessage : NetworkMessage
    {
        public const uint ProtocolId = 226;
        public override uint MessageID => ProtocolId;
        
        public ushort SubAreaId { get; set; }
        public double MapId { get; set; }
        public Types.HouseInformations[] Houses { get; set; }
        public Types.GameRolePlayActorInformations[] Actors { get; set; }
        public Types.InteractiveElement[] InteractiveElements { get; set; }
        public Types.StatedElement[] StatedElements { get; set; }
        public Types.MapObstacle[] Obstacles { get; set; }
        public Types.FightCommonInformations[] Fights { get; set; }
        public bool HasAggressiveMonsters { get; set; }
        public Types.FightStartingPositions FightStartPositions { get; set; }
        
        public MapComplementaryInformationsDataMessage()
        {
        }
        
        public MapComplementaryInformationsDataMessage(ushort subAreaId, double mapId, Types.HouseInformations[] houses, Types.GameRolePlayActorInformations[] actors, Types.InteractiveElement[] interactiveElements, Types.StatedElement[] statedElements, Types.MapObstacle[] obstacles, Types.FightCommonInformations[] fights, bool hasAggressiveMonsters, Types.FightStartingPositions fightStartPositions)
        {
            this.SubAreaId = subAreaId;
            this.MapId = mapId;
            this.Houses = houses;
            this.Actors = actors;
            this.InteractiveElements = interactiveElements;
            this.StatedElements = statedElements;
            this.Obstacles = obstacles;
            this.Fights = fights;
            this.HasAggressiveMonsters = hasAggressiveMonsters;
            this.FightStartPositions = fightStartPositions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteDouble(MapId);
            writer.WriteUShort((ushort)Houses.Length);
            foreach (var entry in Houses)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Actors.Length);
            foreach (var entry in Actors)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)InteractiveElements.Length);
            foreach (var entry in InteractiveElements)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)StatedElements.Length);
            foreach (var entry in StatedElements)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Obstacles.Length);
            foreach (var entry in Obstacles)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Fights.Length);
            foreach (var entry in Fights)
            {
                 entry.Serialize(writer);
            }
            writer.WriteBoolean(HasAggressiveMonsters);
            FightStartPositions.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            var limit = reader.ReadUShort();
            Houses = new Types.HouseInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Houses[i] = Types.ProtocolTypeManager.GetInstance<Types.HouseInformations>(reader.ReadShort());
                 Houses[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Actors = new Types.GameRolePlayActorInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Actors[i] = Types.ProtocolTypeManager.GetInstance<Types.GameRolePlayActorInformations>(reader.ReadShort());
                 Actors[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            InteractiveElements = new Types.InteractiveElement[limit];
            for (int i = 0; i < limit; i++)
            {
                 InteractiveElements[i] = Types.ProtocolTypeManager.GetInstance<Types.InteractiveElement>(reader.ReadShort());
                 InteractiveElements[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            StatedElements = new Types.StatedElement[limit];
            for (int i = 0; i < limit; i++)
            {
                 StatedElements[i] = new Types.StatedElement();
                 StatedElements[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Obstacles = new Types.MapObstacle[limit];
            for (int i = 0; i < limit; i++)
            {
                 Obstacles[i] = new Types.MapObstacle();
                 Obstacles[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Fights = new Types.FightCommonInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Fights[i] = new Types.FightCommonInformations();
                 Fights[i].Deserialize(reader);
            }
            HasAggressiveMonsters = reader.ReadBoolean();
            FightStartPositions = new Types.FightStartingPositions();
            FightStartPositions.Deserialize(reader);
        }
        
    }
    
}