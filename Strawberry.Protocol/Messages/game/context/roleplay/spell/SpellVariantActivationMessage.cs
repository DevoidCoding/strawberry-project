

// Generated on 02/12/2018 03:56:30
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SpellVariantActivationMessage : NetworkMessage
    {
        public const uint ProtocolId = 6705;
        public override uint MessageID => ProtocolId;
        
        public ushort SpellId { get; set; }
        public bool Result { get; set; }
        
        public SpellVariantActivationMessage()
        {
        }
        
        public SpellVariantActivationMessage(ushort spellId, bool result)
        {
            this.SpellId = spellId;
            this.Result = result;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteBoolean(Result);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            Result = reader.ReadBoolean();
        }
        
    }
    
}