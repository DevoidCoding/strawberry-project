

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MapInformationsRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 225;
        public override uint MessageID => ProtocolId;
        
        public double MapId { get; set; }
        
        public MapInformationsRequestMessage()
        {
        }
        
        public MapInformationsRequestMessage(double mapId)
        {
            this.MapId = mapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
        }
        
    }
    
}