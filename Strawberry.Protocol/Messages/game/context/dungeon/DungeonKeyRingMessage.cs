

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DungeonKeyRingMessage : NetworkMessage
    {
        public const uint ProtocolId = 6299;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Availables { get; set; }
        public ushort[] Unavailables { get; set; }
        
        public DungeonKeyRingMessage()
        {
        }
        
        public DungeonKeyRingMessage(ushort[] availables, ushort[] unavailables)
        {
            this.Availables = availables;
            this.Unavailables = unavailables;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Availables.Length);
            foreach (var entry in Availables)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)Unavailables.Length);
            foreach (var entry in Unavailables)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Availables = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Availables[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            Unavailables = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Unavailables[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}