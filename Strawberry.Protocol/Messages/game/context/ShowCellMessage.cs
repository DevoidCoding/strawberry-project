

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShowCellMessage : NetworkMessage
    {
        public const uint ProtocolId = 5612;
        public override uint MessageID => ProtocolId;
        
        public double SourceId { get; set; }
        public ushort CellId { get; set; }
        
        public ShowCellMessage()
        {
        }
        
        public ShowCellMessage(double sourceId, ushort cellId)
        {
            this.SourceId = sourceId;
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(SourceId);
            writer.WriteVarShort(CellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SourceId = reader.ReadDouble();
            if (SourceId < -9007199254740990 || SourceId > 9007199254740990)
                throw new Exception("Forbidden value on SourceId = " + SourceId + ", it doesn't respect the following condition : sourceId < -9007199254740990 || sourceId > 9007199254740990");
            CellId = reader.ReadVarUhShort();
            if (CellId < 0 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0 || cellId > 559");
        }
        
    }
    
}