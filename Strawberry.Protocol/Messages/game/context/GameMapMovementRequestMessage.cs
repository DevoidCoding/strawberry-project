

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class GameMapMovementRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 950;
        public override uint MessageID => ProtocolId;
        
        public short[] KeyMovements { get; set; }
        public double MapId { get; set; }
        
        public GameMapMovementRequestMessage()
        {
        }
        
        public GameMapMovementRequestMessage(short[] keyMovements, double mapId)
        {
            this.KeyMovements = keyMovements;
            this.MapId = mapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)KeyMovements.Length);
            foreach (var entry in KeyMovements)
            {
                 writer.WriteShort(entry);
            }
            writer.WriteDouble(MapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            KeyMovements = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 KeyMovements[i] = reader.ReadShort();
            }
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
        }
        
    }
    
}