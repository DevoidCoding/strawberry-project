

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameContextRemoveMultipleElementsMessage : NetworkMessage
    {
        public const uint ProtocolId = 252;
        public override uint MessageID => ProtocolId;
        
        public double[] ElementsIds { get; set; }
        
        public GameContextRemoveMultipleElementsMessage()
        {
        }
        
        public GameContextRemoveMultipleElementsMessage(double[] elementsIds)
        {
            this.ElementsIds = elementsIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ElementsIds.Length);
            foreach (var entry in ElementsIds)
            {
                 writer.WriteDouble(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ElementsIds = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 ElementsIds[i] = reader.ReadDouble();
            }
        }
        
    }
    
}