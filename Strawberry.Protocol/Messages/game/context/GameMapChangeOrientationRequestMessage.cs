

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameMapChangeOrientationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 945;
        public override uint MessageID => ProtocolId;
        
        public sbyte Direction { get; set; }
        
        public GameMapChangeOrientationRequestMessage()
        {
        }
        
        public GameMapChangeOrientationRequestMessage(sbyte direction)
        {
            this.Direction = direction;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Direction);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Direction = reader.ReadSByte();
            if (Direction < 0)
                throw new Exception("Forbidden value on Direction = " + Direction + ", it doesn't respect the following condition : direction < 0");
        }
        
    }
    
}