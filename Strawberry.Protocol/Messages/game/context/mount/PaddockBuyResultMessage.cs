

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockBuyResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6516;
        public override uint MessageID => ProtocolId;
        
        public double PaddockId { get; set; }
        public bool Bought { get; set; }
        public ulong RealPrice { get; set; }
        
        public PaddockBuyResultMessage()
        {
        }
        
        public PaddockBuyResultMessage(double paddockId, bool bought, ulong realPrice)
        {
            this.PaddockId = paddockId;
            this.Bought = bought;
            this.RealPrice = realPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(PaddockId);
            writer.WriteBoolean(Bought);
            writer.WriteVarLong(RealPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PaddockId = reader.ReadDouble();
            if (PaddockId < 0 || PaddockId > 9007199254740990)
                throw new Exception("Forbidden value on PaddockId = " + PaddockId + ", it doesn't respect the following condition : paddockId < 0 || paddockId > 9007199254740990");
            Bought = reader.ReadBoolean();
            RealPrice = reader.ReadVarUhLong();
            if (RealPrice < 0 || RealPrice > 9007199254740990)
                throw new Exception("Forbidden value on RealPrice = " + RealPrice + ", it doesn't respect the following condition : realPrice < 0 || realPrice > 9007199254740990");
        }
        
    }
    
}