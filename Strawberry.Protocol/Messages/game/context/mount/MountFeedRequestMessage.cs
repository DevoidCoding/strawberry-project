

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountFeedRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6189;
        public override uint MessageID => ProtocolId;
        
        public uint MountUid { get; set; }
        public sbyte MountLocation { get; set; }
        public uint MountFoodUid { get; set; }
        public uint Quantity { get; set; }
        
        public MountFeedRequestMessage()
        {
        }
        
        public MountFeedRequestMessage(uint mountUid, sbyte mountLocation, uint mountFoodUid, uint quantity)
        {
            this.MountUid = mountUid;
            this.MountLocation = mountLocation;
            this.MountFoodUid = mountFoodUid;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MountUid);
            writer.WriteSByte(MountLocation);
            writer.WriteVarInt(MountFoodUid);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountUid = reader.ReadVarUhInt();
            if (MountUid < 0)
                throw new Exception("Forbidden value on MountUid = " + MountUid + ", it doesn't respect the following condition : mountUid < 0");
            MountLocation = reader.ReadSByte();
            MountFoodUid = reader.ReadVarUhInt();
            if (MountFoodUid < 0)
                throw new Exception("Forbidden value on MountFoodUid = " + MountFoodUid + ", it doesn't respect the following condition : mountFoodUid < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}