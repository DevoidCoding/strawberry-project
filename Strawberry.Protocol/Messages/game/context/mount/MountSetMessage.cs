

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountSetMessage : NetworkMessage
    {
        public const uint ProtocolId = 5968;
        public override uint MessageID => ProtocolId;
        
        public Types.MountClientData MountData { get; set; }
        
        public MountSetMessage()
        {
        }
        
        public MountSetMessage(Types.MountClientData mountData)
        {
            this.MountData = mountData;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            MountData.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountData = new Types.MountClientData();
            MountData.Deserialize(reader);
        }
        
    }
    
}