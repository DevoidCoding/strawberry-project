

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountDataErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6172;
        public override uint MessageID => ProtocolId;
        
        public sbyte Reason { get; set; }
        
        public MountDataErrorMessage()
        {
        }
        
        public MountDataErrorMessage(sbyte reason)
        {
            this.Reason = reason;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Reason);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Reason = reader.ReadSByte();
            if (Reason < 0)
                throw new Exception("Forbidden value on Reason = " + Reason + ", it doesn't respect the following condition : reason < 0");
        }
        
    }
    
}