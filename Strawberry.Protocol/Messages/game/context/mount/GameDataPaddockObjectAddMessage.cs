

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameDataPaddockObjectAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5990;
        public override uint MessageID => ProtocolId;
        
        public Types.PaddockItem PaddockItemDescription { get; set; }
        
        public GameDataPaddockObjectAddMessage()
        {
        }
        
        public GameDataPaddockObjectAddMessage(Types.PaddockItem paddockItemDescription)
        {
            this.PaddockItemDescription = paddockItemDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            PaddockItemDescription.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PaddockItemDescription = new Types.PaddockItem();
            PaddockItemDescription.Deserialize(reader);
        }
        
    }
    
}