

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockMoveItemRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6052;
        public override uint MessageID => ProtocolId;
        
        public ushort OldCellId { get; set; }
        public ushort NewCellId { get; set; }
        
        public PaddockMoveItemRequestMessage()
        {
        }
        
        public PaddockMoveItemRequestMessage(ushort oldCellId, ushort newCellId)
        {
            this.OldCellId = oldCellId;
            this.NewCellId = newCellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(OldCellId);
            writer.WriteVarShort(NewCellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            OldCellId = reader.ReadVarUhShort();
            if (OldCellId < 0 || OldCellId > 559)
                throw new Exception("Forbidden value on OldCellId = " + OldCellId + ", it doesn't respect the following condition : oldCellId < 0 || oldCellId > 559");
            NewCellId = reader.ReadVarUhShort();
            if (NewCellId < 0 || NewCellId > 559)
                throw new Exception("Forbidden value on NewCellId = " + NewCellId + ", it doesn't respect the following condition : newCellId < 0 || newCellId > 559");
        }
        
    }
    
}