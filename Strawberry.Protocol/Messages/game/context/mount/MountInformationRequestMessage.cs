

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountInformationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5972;
        public override uint MessageID => ProtocolId;
        
        public double Id { get; set; }
        public double Time { get; set; }
        
        public MountInformationRequestMessage()
        {
        }
        
        public MountInformationRequestMessage(double id, double time)
        {
            this.Id = id;
            this.Time = time;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Id);
            writer.WriteDouble(Time);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
            Time = reader.ReadDouble();
            if (Time < -9007199254740990 || Time > 9007199254740990)
                throw new Exception("Forbidden value on Time = " + Time + ", it doesn't respect the following condition : time < -9007199254740990 || time > 9007199254740990");
        }
        
    }
    
}