

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountReleasedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6308;
        public override uint MessageID => ProtocolId;
        
        public int MountId { get; set; }
        
        public MountReleasedMessage()
        {
        }
        
        public MountReleasedMessage(int mountId)
        {
            this.MountId = mountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountId = reader.ReadVarInt();
        }
        
    }
    
}