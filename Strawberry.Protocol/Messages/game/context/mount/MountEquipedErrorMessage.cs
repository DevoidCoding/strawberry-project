

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountEquipedErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 5963;
        public override uint MessageID => ProtocolId;
        
        public sbyte ErrorType { get; set; }
        
        public MountEquipedErrorMessage()
        {
        }
        
        public MountEquipedErrorMessage(sbyte errorType)
        {
            this.ErrorType = errorType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ErrorType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ErrorType = reader.ReadSByte();
            if (ErrorType < 0)
                throw new Exception("Forbidden value on ErrorType = " + ErrorType + ", it doesn't respect the following condition : errorType < 0");
        }
        
    }
    
}