

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountSetXpRatioRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5989;
        public override uint MessageID => ProtocolId;
        
        public sbyte XpRatio { get; set; }
        
        public MountSetXpRatioRequestMessage()
        {
        }
        
        public MountSetXpRatioRequestMessage(sbyte xpRatio)
        {
            this.XpRatio = xpRatio;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(XpRatio);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            XpRatio = reader.ReadSByte();
            if (XpRatio < 0)
                throw new Exception("Forbidden value on XpRatio = " + XpRatio + ", it doesn't respect the following condition : xpRatio < 0");
        }
        
    }
    
}