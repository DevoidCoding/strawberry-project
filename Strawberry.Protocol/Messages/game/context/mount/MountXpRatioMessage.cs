

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountXpRatioMessage : NetworkMessage
    {
        public const uint ProtocolId = 5970;
        public override uint MessageID => ProtocolId;
        
        public sbyte Ratio { get; set; }
        
        public MountXpRatioMessage()
        {
        }
        
        public MountXpRatioMessage(sbyte ratio)
        {
            this.Ratio = ratio;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Ratio);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Ratio = reader.ReadSByte();
            if (Ratio < 0)
                throw new Exception("Forbidden value on Ratio = " + Ratio + ", it doesn't respect the following condition : ratio < 0");
        }
        
    }
    
}