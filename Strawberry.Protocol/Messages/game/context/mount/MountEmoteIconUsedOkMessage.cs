

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountEmoteIconUsedOkMessage : NetworkMessage
    {
        public const uint ProtocolId = 5978;
        public override uint MessageID => ProtocolId;
        
        public int MountId { get; set; }
        public sbyte ReactionType { get; set; }
        
        public MountEmoteIconUsedOkMessage()
        {
        }
        
        public MountEmoteIconUsedOkMessage(int mountId, sbyte reactionType)
        {
            this.MountId = mountId;
            this.ReactionType = reactionType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MountId);
            writer.WriteSByte(ReactionType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountId = reader.ReadVarInt();
            ReactionType = reader.ReadSByte();
            if (ReactionType < 0)
                throw new Exception("Forbidden value on ReactionType = " + ReactionType + ", it doesn't respect the following condition : reactionType < 0");
        }
        
    }
    
}