

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountRenamedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5983;
        public override uint MessageID => ProtocolId;
        
        public int MountId { get; set; }
        public string Name { get; set; }
        
        public MountRenamedMessage()
        {
        }
        
        public MountRenamedMessage(int mountId, string name)
        {
            this.MountId = mountId;
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MountId);
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountId = reader.ReadVarInt();
            Name = reader.ReadUTF();
        }
        
    }
    
}