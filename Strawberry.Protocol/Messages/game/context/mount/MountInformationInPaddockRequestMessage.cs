

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountInformationInPaddockRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5975;
        public override uint MessageID => ProtocolId;
        
        public int MapRideId { get; set; }
        
        public MountInformationInPaddockRequestMessage()
        {
        }
        
        public MountInformationInPaddockRequestMessage(int mapRideId)
        {
            this.MapRideId = mapRideId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MapRideId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MapRideId = reader.ReadVarInt();
        }
        
    }
    
}