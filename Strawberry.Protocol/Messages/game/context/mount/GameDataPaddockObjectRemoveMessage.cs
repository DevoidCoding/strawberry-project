

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameDataPaddockObjectRemoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 5993;
        public override uint MessageID => ProtocolId;
        
        public ushort CellId { get; set; }
        
        public GameDataPaddockObjectRemoveMessage()
        {
        }
        
        public GameDataPaddockObjectRemoveMessage(ushort cellId)
        {
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CellId = reader.ReadVarUhShort();
            if (CellId < 0 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < 0 || cellId > 559");
        }
        
    }
    
}