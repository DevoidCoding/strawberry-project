

// Generated on 02/12/2018 03:56:21
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockSellRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5953;
        public override uint MessageID => ProtocolId;
        
        public ulong Price { get; set; }
        public bool ForSale { get; set; }
        
        public PaddockSellRequestMessage()
        {
        }
        
        public PaddockSellRequestMessage(ulong price, bool forSale)
        {
            this.Price = price;
            this.ForSale = forSale;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Price);
            writer.WriteBoolean(ForSale);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
            ForSale = reader.ReadBoolean();
        }
        
    }
    
}