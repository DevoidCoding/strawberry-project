

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountSterilizedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5977;
        public override uint MessageID => ProtocolId;
        
        public int MountId { get; set; }
        
        public MountSterilizedMessage()
        {
        }
        
        public MountSterilizedMessage(int mountId)
        {
            this.MountId = mountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(MountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MountId = reader.ReadVarInt();
        }
        
    }
    
}