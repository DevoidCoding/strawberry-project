

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PaddockBuyRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5951;
        public override uint MessageID => ProtocolId;
        
        public ulong ProposedPrice { get; set; }
        
        public PaddockBuyRequestMessage()
        {
        }
        
        public PaddockBuyRequestMessage(ulong proposedPrice)
        {
            this.ProposedPrice = proposedPrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(ProposedPrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ProposedPrice = reader.ReadVarUhLong();
            if (ProposedPrice < 0 || ProposedPrice > 9007199254740990)
                throw new Exception("Forbidden value on ProposedPrice = " + ProposedPrice + ", it doesn't respect the following condition : proposedPrice < 0 || proposedPrice > 9007199254740990");
        }
        
    }
    
}