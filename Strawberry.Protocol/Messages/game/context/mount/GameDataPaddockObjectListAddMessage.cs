

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameDataPaddockObjectListAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5992;
        public override uint MessageID => ProtocolId;
        
        public Types.PaddockItem[] PaddockItemDescription { get; set; }
        
        public GameDataPaddockObjectListAddMessage()
        {
        }
        
        public GameDataPaddockObjectListAddMessage(Types.PaddockItem[] paddockItemDescription)
        {
            this.PaddockItemDescription = paddockItemDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)PaddockItemDescription.Length);
            foreach (var entry in PaddockItemDescription)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            PaddockItemDescription = new Types.PaddockItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 PaddockItemDescription[i] = new Types.PaddockItem();
                 PaddockItemDescription[i].Deserialize(reader);
            }
        }
        
    }
    
}