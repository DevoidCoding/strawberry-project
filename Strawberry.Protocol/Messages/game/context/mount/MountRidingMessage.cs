

// Generated on 02/12/2018 03:56:20
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MountRidingMessage : NetworkMessage
    {
        public const uint ProtocolId = 5967;
        public override uint MessageID => ProtocolId;
        
        public bool IsRiding { get; set; }
        public bool IsAutopilot { get; set; }
        
        public MountRidingMessage()
        {
        }
        
        public MountRidingMessage(bool isRiding, bool isAutopilot)
        {
            this.IsRiding = isRiding;
            this.IsAutopilot = isAutopilot;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, IsRiding);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsAutopilot);
            writer.WriteByte(flag1);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            IsRiding = BooleanByteWrapper.GetFlag(flag1, 0);
            IsAutopilot = BooleanByteWrapper.GetFlag(flag1, 1);
        }
        
    }
    
}