

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameMapChangeOrientationsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6155;
        public override uint MessageID => ProtocolId;
        
        public Types.ActorOrientation[] Orientations { get; set; }
        
        public GameMapChangeOrientationsMessage()
        {
        }
        
        public GameMapChangeOrientationsMessage(Types.ActorOrientation[] orientations)
        {
            this.Orientations = orientations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Orientations.Length);
            foreach (var entry in Orientations)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Orientations = new Types.ActorOrientation[limit];
            for (int i = 0; i < limit; i++)
            {
                 Orientations[i] = new Types.ActorOrientation();
                 Orientations[i].Deserialize(reader);
            }
        }
        
    }
    
}