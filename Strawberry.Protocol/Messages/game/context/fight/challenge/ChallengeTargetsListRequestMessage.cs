

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChallengeTargetsListRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5614;
        public override uint MessageID => ProtocolId;
        
        public ushort ChallengeId { get; set; }
        
        public ChallengeTargetsListRequestMessage()
        {
        }
        
        public ChallengeTargetsListRequestMessage(ushort challengeId)
        {
            this.ChallengeId = challengeId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ChallengeId = reader.ReadVarUhShort();
            if (ChallengeId < 0)
                throw new Exception("Forbidden value on ChallengeId = " + ChallengeId + ", it doesn't respect the following condition : challengeId < 0");
        }
        
    }
    
}