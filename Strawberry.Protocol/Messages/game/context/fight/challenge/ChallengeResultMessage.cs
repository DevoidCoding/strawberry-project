

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChallengeResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6019;
        public override uint MessageID => ProtocolId;
        
        public ushort ChallengeId { get; set; }
        public bool Success { get; set; }
        
        public ChallengeResultMessage()
        {
        }
        
        public ChallengeResultMessage(ushort challengeId, bool success)
        {
            this.ChallengeId = challengeId;
            this.Success = success;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteBoolean(Success);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ChallengeId = reader.ReadVarUhShort();
            if (ChallengeId < 0)
                throw new Exception("Forbidden value on ChallengeId = " + ChallengeId + ", it doesn't respect the following condition : challengeId < 0");
            Success = reader.ReadBoolean();
        }
        
    }
    
}