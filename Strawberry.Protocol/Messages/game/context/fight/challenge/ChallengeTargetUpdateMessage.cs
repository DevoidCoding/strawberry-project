

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChallengeTargetUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6123;
        public override uint MessageID => ProtocolId;
        
        public ushort ChallengeId { get; set; }
        public double TargetId { get; set; }
        
        public ChallengeTargetUpdateMessage()
        {
        }
        
        public ChallengeTargetUpdateMessage(ushort challengeId, double targetId)
        {
            this.ChallengeId = challengeId;
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteDouble(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ChallengeId = reader.ReadVarUhShort();
            if (ChallengeId < 0)
                throw new Exception("Forbidden value on ChallengeId = " + ChallengeId + ", it doesn't respect the following condition : challengeId < 0");
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
        }
        
    }
    
}