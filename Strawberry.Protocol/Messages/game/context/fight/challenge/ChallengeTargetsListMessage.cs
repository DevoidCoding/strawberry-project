

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChallengeTargetsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5613;
        public override uint MessageID => ProtocolId;
        
        public double[] TargetIds { get; set; }
        public short[] TargetCells { get; set; }
        
        public ChallengeTargetsListMessage()
        {
        }
        
        public ChallengeTargetsListMessage(double[] targetIds, short[] targetCells)
        {
            this.TargetIds = targetIds;
            this.TargetCells = targetCells;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)TargetIds.Length);
            foreach (var entry in TargetIds)
            {
                 writer.WriteDouble(entry);
            }
            writer.WriteUShort((ushort)TargetCells.Length);
            foreach (var entry in TargetCells)
            {
                 writer.WriteShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            TargetIds = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 TargetIds[i] = reader.ReadDouble();
            }
            limit = reader.ReadUShort();
            TargetCells = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 TargetCells[i] = reader.ReadShort();
            }
        }
        
    }
    
}