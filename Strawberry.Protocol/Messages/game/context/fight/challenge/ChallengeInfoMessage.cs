

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChallengeInfoMessage : NetworkMessage
    {
        public const uint ProtocolId = 6022;
        public override uint MessageID => ProtocolId;
        
        public ushort ChallengeId { get; set; }
        public double TargetId { get; set; }
        public uint XpBonus { get; set; }
        public uint DropBonus { get; set; }
        
        public ChallengeInfoMessage()
        {
        }
        
        public ChallengeInfoMessage(ushort challengeId, double targetId, uint xpBonus, uint dropBonus)
        {
            this.ChallengeId = challengeId;
            this.TargetId = targetId;
            this.XpBonus = xpBonus;
            this.DropBonus = dropBonus;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ChallengeId);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(XpBonus);
            writer.WriteVarInt(DropBonus);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ChallengeId = reader.ReadVarUhShort();
            if (ChallengeId < 0)
                throw new Exception("Forbidden value on ChallengeId = " + ChallengeId + ", it doesn't respect the following condition : challengeId < 0");
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            XpBonus = reader.ReadVarUhInt();
            if (XpBonus < 0)
                throw new Exception("Forbidden value on XpBonus = " + XpBonus + ", it doesn't respect the following condition : xpBonus < 0");
            DropBonus = reader.ReadVarUhInt();
            if (DropBonus < 0)
                throw new Exception("Forbidden value on DropBonus = " + DropBonus + ", it doesn't respect the following condition : dropBonus < 0");
        }
        
    }
    
}