

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightStartMessage : NetworkMessage
    {
        public const uint ProtocolId = 712;
        public override uint MessageID => ProtocolId;
        
        public Types.Idol[] Idols { get; set; }
        
        public GameFightStartMessage()
        {
        }
        
        public GameFightStartMessage(Types.Idol[] idols)
        {
            this.Idols = idols;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Idols.Length);
            foreach (var entry in Idols)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Idols = new Types.Idol[limit];
            for (int i = 0; i < limit; i++)
            {
                 Idols[i] = new Types.Idol();
                 Idols[i].Deserialize(reader);
            }
        }
        
    }
    
}