

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightSpectateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6069;
        public override uint MessageID => ProtocolId;
        
        public Types.FightDispellableEffectExtendedInformations[] Effects { get; set; }
        public Types.GameActionMark[] Marks { get; set; }
        public ushort GameTurn { get; set; }
        public int FightStart { get; set; }
        public Types.Idol[] Idols { get; set; }
        
        public GameFightSpectateMessage()
        {
        }
        
        public GameFightSpectateMessage(Types.FightDispellableEffectExtendedInformations[] effects, Types.GameActionMark[] marks, ushort gameTurn, int fightStart, Types.Idol[] idols)
        {
            this.Effects = effects;
            this.Marks = marks;
            this.GameTurn = gameTurn;
            this.FightStart = fightStart;
            this.Idols = idols;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Marks.Length);
            foreach (var entry in Marks)
            {
                 entry.Serialize(writer);
            }
            writer.WriteVarShort(GameTurn);
            writer.WriteInt(FightStart);
            writer.WriteUShort((ushort)Idols.Length);
            foreach (var entry in Idols)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Effects = new Types.FightDispellableEffectExtendedInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = new Types.FightDispellableEffectExtendedInformations();
                 Effects[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Marks = new Types.GameActionMark[limit];
            for (int i = 0; i < limit; i++)
            {
                 Marks[i] = new Types.GameActionMark();
                 Marks[i].Deserialize(reader);
            }
            GameTurn = reader.ReadVarUhShort();
            if (GameTurn < 0)
                throw new Exception("Forbidden value on GameTurn = " + GameTurn + ", it doesn't respect the following condition : gameTurn < 0");
            FightStart = reader.ReadInt();
            if (FightStart < 0)
                throw new Exception("Forbidden value on FightStart = " + FightStart + ", it doesn't respect the following condition : fightStart < 0");
            limit = reader.ReadUShort();
            Idols = new Types.Idol[limit];
            for (int i = 0; i < limit; i++)
            {
                 Idols[i] = new Types.Idol();
                 Idols[i].Deserialize(reader);
            }
        }
        
    }
    
}