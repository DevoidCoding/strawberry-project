

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightTurnFinishMessage : NetworkMessage
    {
        public const uint ProtocolId = 718;
        public override uint MessageID => ProtocolId;
        
        public bool IsAfk { get; set; }
        
        public GameFightTurnFinishMessage()
        {
        }
        
        public GameFightTurnFinishMessage(bool isAfk)
        {
            this.IsAfk = isAfk;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(IsAfk);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IsAfk = reader.ReadBoolean();
        }
        
    }
    
}