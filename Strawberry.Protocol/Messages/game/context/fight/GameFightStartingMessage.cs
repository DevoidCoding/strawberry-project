

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightStartingMessage : NetworkMessage
    {
        public const uint ProtocolId = 700;
        public override uint MessageID => ProtocolId;
        
        public sbyte FightType { get; set; }
        public ushort FightId { get; set; }
        public double AttackerId { get; set; }
        public double DefenderId { get; set; }
        
        public GameFightStartingMessage()
        {
        }
        
        public GameFightStartingMessage(sbyte fightType, ushort fightId, double attackerId, double defenderId)
        {
            this.FightType = fightType;
            this.FightId = fightId;
            this.AttackerId = attackerId;
            this.DefenderId = defenderId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(FightType);
            writer.WriteVarShort(FightId);
            writer.WriteDouble(AttackerId);
            writer.WriteDouble(DefenderId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightType = reader.ReadSByte();
            if (FightType < 0)
                throw new Exception("Forbidden value on FightType = " + FightType + ", it doesn't respect the following condition : fightType < 0");
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            AttackerId = reader.ReadDouble();
            if (AttackerId < -9007199254740990 || AttackerId > 9007199254740990)
                throw new Exception("Forbidden value on AttackerId = " + AttackerId + ", it doesn't respect the following condition : attackerId < -9007199254740990 || attackerId > 9007199254740990");
            DefenderId = reader.ReadDouble();
            if (DefenderId < -9007199254740990 || DefenderId > 9007199254740990)
                throw new Exception("Forbidden value on DefenderId = " + DefenderId + ", it doesn't respect the following condition : defenderId < -9007199254740990 || defenderId > 9007199254740990");
        }
        
    }
    
}