

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ArenaFighterLeaveMessage : NetworkMessage
    {
        public const uint ProtocolId = 6700;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterBasicMinimalInformations Leaver { get; set; }
        
        public ArenaFighterLeaveMessage()
        {
        }
        
        public ArenaFighterLeaveMessage(Types.CharacterBasicMinimalInformations leaver)
        {
            this.Leaver = leaver;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Leaver.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Leaver = new Types.CharacterBasicMinimalInformations();
            Leaver.Deserialize(reader);
        }
        
    }
    
}