

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightOptionToggleMessage : NetworkMessage
    {
        public const uint ProtocolId = 707;
        public override uint MessageID => ProtocolId;
        
        public sbyte Option { get; set; }
        
        public GameFightOptionToggleMessage()
        {
        }
        
        public GameFightOptionToggleMessage(sbyte option)
        {
            this.Option = option;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Option);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Option = reader.ReadSByte();
            if (Option < 0)
                throw new Exception("Forbidden value on Option = " + Option + ", it doesn't respect the following condition : option < 0");
        }
        
    }
    
}