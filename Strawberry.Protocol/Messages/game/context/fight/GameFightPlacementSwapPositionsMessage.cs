

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightPlacementSwapPositionsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6544;
        public override uint MessageID => ProtocolId;
        
        public Types.IdentifiedEntityDispositionInformations[] Dispositions { get; set; }
        
        public GameFightPlacementSwapPositionsMessage()
        {
        }
        
        public GameFightPlacementSwapPositionsMessage(Types.IdentifiedEntityDispositionInformations[] dispositions)
        {
            this.Dispositions = dispositions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            foreach (var entry in Dispositions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Dispositions = new Types.IdentifiedEntityDispositionInformations[2];
            for (int i = 0; i < 2; i++)
            {
                 Dispositions[i] = new Types.IdentifiedEntityDispositionInformations();
                 Dispositions[i].Deserialize(reader);
            }
        }
        
    }
    
}