

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightPlacementPossiblePositionsMessage : NetworkMessage
    {
        public const uint ProtocolId = 703;
        public override uint MessageID => ProtocolId;
        
        public ushort[] PositionsForChallengers { get; set; }
        public ushort[] PositionsForDefenders { get; set; }
        public sbyte TeamNumber { get; set; }
        
        public GameFightPlacementPossiblePositionsMessage()
        {
        }
        
        public GameFightPlacementPossiblePositionsMessage(ushort[] positionsForChallengers, ushort[] positionsForDefenders, sbyte teamNumber)
        {
            this.PositionsForChallengers = positionsForChallengers;
            this.PositionsForDefenders = positionsForDefenders;
            this.TeamNumber = teamNumber;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)PositionsForChallengers.Length);
            foreach (var entry in PositionsForChallengers)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)PositionsForDefenders.Length);
            foreach (var entry in PositionsForDefenders)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteSByte(TeamNumber);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            PositionsForChallengers = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PositionsForChallengers[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            PositionsForDefenders = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PositionsForDefenders[i] = reader.ReadVarUhShort();
            }
            TeamNumber = reader.ReadSByte();
            if (TeamNumber < 0)
                throw new Exception("Forbidden value on TeamNumber = " + TeamNumber + ", it doesn't respect the following condition : teamNumber < 0");
        }
        
    }
    
}