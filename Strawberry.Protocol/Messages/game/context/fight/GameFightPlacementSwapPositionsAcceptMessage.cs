

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightPlacementSwapPositionsAcceptMessage : NetworkMessage
    {
        public const uint ProtocolId = 6547;
        public override uint MessageID => ProtocolId;
        
        public int RequestId { get; set; }
        
        public GameFightPlacementSwapPositionsAcceptMessage()
        {
        }
        
        public GameFightPlacementSwapPositionsAcceptMessage(int requestId)
        {
            this.RequestId = requestId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(RequestId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadInt();
            if (RequestId < 0)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0");
        }
        
    }
    
}