

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightNewRoundMessage : NetworkMessage
    {
        public const uint ProtocolId = 6239;
        public override uint MessageID => ProtocolId;
        
        public uint RoundNumber { get; set; }
        
        public GameFightNewRoundMessage()
        {
        }
        
        public GameFightNewRoundMessage(uint roundNumber)
        {
            this.RoundNumber = roundNumber;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(RoundNumber);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RoundNumber = reader.ReadVarUhInt();
            if (RoundNumber < 0)
                throw new Exception("Forbidden value on RoundNumber = " + RoundNumber + ", it doesn't respect the following condition : roundNumber < 0");
        }
        
    }
    
}