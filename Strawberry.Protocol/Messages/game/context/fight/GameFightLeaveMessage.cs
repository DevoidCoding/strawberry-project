

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightLeaveMessage : NetworkMessage
    {
        public const uint ProtocolId = 721;
        public override uint MessageID => ProtocolId;
        
        public double CharId { get; set; }
        
        public GameFightLeaveMessage()
        {
        }
        
        public GameFightLeaveMessage(double charId)
        {
            this.CharId = charId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(CharId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CharId = reader.ReadDouble();
            if (CharId < -9007199254740990 || CharId > 9007199254740990)
                throw new Exception("Forbidden value on CharId = " + CharId + ", it doesn't respect the following condition : charId < -9007199254740990 || charId > 9007199254740990");
        }
        
    }
    
}