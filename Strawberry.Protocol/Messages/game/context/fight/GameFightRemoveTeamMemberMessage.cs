

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightRemoveTeamMemberMessage : NetworkMessage
    {
        public const uint ProtocolId = 711;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public sbyte TeamId { get; set; }
        public double CharId { get; set; }
        
        public GameFightRemoveTeamMemberMessage()
        {
        }
        
        public GameFightRemoveTeamMemberMessage(ushort fightId, sbyte teamId, double charId)
        {
            this.FightId = fightId;
            this.TeamId = teamId;
            this.CharId = charId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteSByte(TeamId);
            writer.WriteDouble(CharId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            CharId = reader.ReadDouble();
            if (CharId < -9007199254740990 || CharId > 9007199254740990)
                throw new Exception("Forbidden value on CharId = " + CharId + ", it doesn't respect the following condition : charId < -9007199254740990 || charId > 9007199254740990");
        }
        
    }
    
}