

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightPlacementSwapPositionsOfferMessage : NetworkMessage
    {
        public const uint ProtocolId = 6542;
        public override uint MessageID => ProtocolId;
        
        public int RequestId { get; set; }
        public double RequesterId { get; set; }
        public ushort RequesterCellId { get; set; }
        public double RequestedId { get; set; }
        public ushort RequestedCellId { get; set; }
        
        public GameFightPlacementSwapPositionsOfferMessage()
        {
        }
        
        public GameFightPlacementSwapPositionsOfferMessage(int requestId, double requesterId, ushort requesterCellId, double requestedId, ushort requestedCellId)
        {
            this.RequestId = requestId;
            this.RequesterId = requesterId;
            this.RequesterCellId = requesterCellId;
            this.RequestedId = requestedId;
            this.RequestedCellId = requestedCellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(RequestId);
            writer.WriteDouble(RequesterId);
            writer.WriteVarShort(RequesterCellId);
            writer.WriteDouble(RequestedId);
            writer.WriteVarShort(RequestedCellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadInt();
            if (RequestId < 0)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0");
            RequesterId = reader.ReadDouble();
            if (RequesterId < -9007199254740990 || RequesterId > 9007199254740990)
                throw new Exception("Forbidden value on RequesterId = " + RequesterId + ", it doesn't respect the following condition : requesterId < -9007199254740990 || requesterId > 9007199254740990");
            RequesterCellId = reader.ReadVarUhShort();
            if (RequesterCellId < 0 || RequesterCellId > 559)
                throw new Exception("Forbidden value on RequesterCellId = " + RequesterCellId + ", it doesn't respect the following condition : requesterCellId < 0 || requesterCellId > 559");
            RequestedId = reader.ReadDouble();
            if (RequestedId < -9007199254740990 || RequestedId > 9007199254740990)
                throw new Exception("Forbidden value on RequestedId = " + RequestedId + ", it doesn't respect the following condition : requestedId < -9007199254740990 || requestedId > 9007199254740990");
            RequestedCellId = reader.ReadVarUhShort();
            if (RequestedCellId < 0 || RequestedCellId > 559)
                throw new Exception("Forbidden value on RequestedCellId = " + RequestedCellId + ", it doesn't respect the following condition : requestedCellId < 0 || requestedCellId > 559");
        }
        
    }
    
}