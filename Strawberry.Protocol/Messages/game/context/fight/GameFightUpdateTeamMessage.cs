

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightUpdateTeamMessage : NetworkMessage
    {
        public const uint ProtocolId = 5572;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public Types.FightTeamInformations Team { get; set; }
        
        public GameFightUpdateTeamMessage()
        {
        }
        
        public GameFightUpdateTeamMessage(ushort fightId, Types.FightTeamInformations team)
        {
            this.FightId = fightId;
            this.Team = team;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            Team.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            Team = new Types.FightTeamInformations();
            Team.Deserialize(reader);
        }
        
    }
    
}