

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class RefreshCharacterStatsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6699;
        public override uint MessageID => ProtocolId;
        
        public double FighterId { get; set; }
        public Types.GameFightMinimalStats Stats { get; set; }
        
        public RefreshCharacterStatsMessage()
        {
        }
        
        public RefreshCharacterStatsMessage(double fighterId, Types.GameFightMinimalStats stats)
        {
            this.FighterId = fighterId;
            this.Stats = stats;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(FighterId);
            Stats.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FighterId = reader.ReadDouble();
            if (FighterId < -9007199254740990 || FighterId > 9007199254740990)
                throw new Exception("Forbidden value on FighterId = " + FighterId + ", it doesn't respect the following condition : fighterId < -9007199254740990 || fighterId > 9007199254740990");
            Stats = new Types.GameFightMinimalStats();
            Stats.Deserialize(reader);
        }
        
    }
    
}