

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightTurnListMessage : NetworkMessage
    {
        public const uint ProtocolId = 713;
        public override uint MessageID => ProtocolId;
        
        public double[] Ids { get; set; }
        public double[] DeadsIds { get; set; }
        
        public GameFightTurnListMessage()
        {
        }
        
        public GameFightTurnListMessage(double[] ids, double[] deadsIds)
        {
            this.Ids = ids;
            this.DeadsIds = deadsIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Ids.Length);
            foreach (var entry in Ids)
            {
                 writer.WriteDouble(entry);
            }
            writer.WriteUShort((ushort)DeadsIds.Length);
            foreach (var entry in DeadsIds)
            {
                 writer.WriteDouble(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Ids = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ids[i] = reader.ReadDouble();
            }
            limit = reader.ReadUShort();
            DeadsIds = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 DeadsIds[i] = reader.ReadDouble();
            }
        }
        
    }
    
}