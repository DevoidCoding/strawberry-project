

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SlaveSwitchContextMessage : NetworkMessage
    {
        public const uint ProtocolId = 6214;
        public override uint MessageID => ProtocolId;
        
        public double MasterId { get; set; }
        public double SlaveId { get; set; }
        public Types.SpellItem[] SlaveSpells { get; set; }
        public Types.CharacterCharacteristicsInformations SlaveStats { get; set; }
        public Types.Shortcut[] Shortcuts { get; set; }
        
        public SlaveSwitchContextMessage()
        {
        }
        
        public SlaveSwitchContextMessage(double masterId, double slaveId, Types.SpellItem[] slaveSpells, Types.CharacterCharacteristicsInformations slaveStats, Types.Shortcut[] shortcuts)
        {
            this.MasterId = masterId;
            this.SlaveId = slaveId;
            this.SlaveSpells = slaveSpells;
            this.SlaveStats = slaveStats;
            this.Shortcuts = shortcuts;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(MasterId);
            writer.WriteDouble(SlaveId);
            writer.WriteUShort((ushort)SlaveSpells.Length);
            foreach (var entry in SlaveSpells)
            {
                 entry.Serialize(writer);
            }
            SlaveStats.Serialize(writer);
            writer.WriteUShort((ushort)Shortcuts.Length);
            foreach (var entry in Shortcuts)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MasterId = reader.ReadDouble();
            if (MasterId < -9007199254740990 || MasterId > 9007199254740990)
                throw new Exception("Forbidden value on MasterId = " + MasterId + ", it doesn't respect the following condition : masterId < -9007199254740990 || masterId > 9007199254740990");
            SlaveId = reader.ReadDouble();
            if (SlaveId < -9007199254740990 || SlaveId > 9007199254740990)
                throw new Exception("Forbidden value on SlaveId = " + SlaveId + ", it doesn't respect the following condition : slaveId < -9007199254740990 || slaveId > 9007199254740990");
            var limit = reader.ReadUShort();
            SlaveSpells = new Types.SpellItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 SlaveSpells[i] = new Types.SpellItem();
                 SlaveSpells[i].Deserialize(reader);
            }
            SlaveStats = new Types.CharacterCharacteristicsInformations();
            SlaveStats.Deserialize(reader);
            limit = reader.ReadUShort();
            Shortcuts = new Types.Shortcut[limit];
            for (int i = 0; i < limit; i++)
            {
                 Shortcuts[i] = Types.ProtocolTypeManager.GetInstance<Types.Shortcut>(reader.ReadShort());
                 Shortcuts[i].Deserialize(reader);
            }
        }
        
    }
    
}