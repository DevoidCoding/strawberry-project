

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightTurnReadyMessage : NetworkMessage
    {
        public const uint ProtocolId = 716;
        public override uint MessageID => ProtocolId;
        
        public bool IsReady { get; set; }
        
        public GameFightTurnReadyMessage()
        {
        }
        
        public GameFightTurnReadyMessage(bool isReady)
        {
            this.IsReady = isReady;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(IsReady);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IsReady = reader.ReadBoolean();
        }
        
    }
    
}