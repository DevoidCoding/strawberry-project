

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightJoinRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 701;
        public override uint MessageID => ProtocolId;
        
        public double FighterId { get; set; }
        public ushort FightId { get; set; }
        
        public GameFightJoinRequestMessage()
        {
        }
        
        public GameFightJoinRequestMessage(double fighterId, ushort fightId)
        {
            this.FighterId = fighterId;
            this.FightId = fightId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(FighterId);
            writer.WriteVarShort(FightId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FighterId = reader.ReadDouble();
            if (FighterId < -9007199254740990 || FighterId > 9007199254740990)
                throw new Exception("Forbidden value on FighterId = " + FighterId + ", it doesn't respect the following condition : fighterId < -9007199254740990 || fighterId > 9007199254740990");
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
        }
        
    }
    
}