

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightPlacementSwapPositionsCancelledMessage : NetworkMessage
    {
        public const uint ProtocolId = 6546;
        public override uint MessageID => ProtocolId;
        
        public int RequestId { get; set; }
        public double CancellerId { get; set; }
        
        public GameFightPlacementSwapPositionsCancelledMessage()
        {
        }
        
        public GameFightPlacementSwapPositionsCancelledMessage(int requestId, double cancellerId)
        {
            this.RequestId = requestId;
            this.CancellerId = cancellerId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(RequestId);
            writer.WriteDouble(CancellerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadInt();
            if (RequestId < 0)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0");
            CancellerId = reader.ReadDouble();
            if (CancellerId < -9007199254740990 || CancellerId > 9007199254740990)
                throw new Exception("Forbidden value on CancellerId = " + CancellerId + ", it doesn't respect the following condition : cancellerId < -9007199254740990 || cancellerId > 9007199254740990");
        }
        
    }
    
}