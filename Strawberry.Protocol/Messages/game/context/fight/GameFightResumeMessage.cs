

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightResumeMessage : GameFightSpectateMessage
    {
        public new const uint ProtocolId = 6067;
        public override uint MessageID => ProtocolId;
        
        public Types.GameFightSpellCooldown[] SpellCooldowns { get; set; }
        public sbyte SummonCount { get; set; }
        public sbyte BombCount { get; set; }
        
        public GameFightResumeMessage()
        {
        }
        
        public GameFightResumeMessage(Types.FightDispellableEffectExtendedInformations[] effects, Types.GameActionMark[] marks, ushort gameTurn, int fightStart, Types.Idol[] idols, Types.GameFightSpellCooldown[] spellCooldowns, sbyte summonCount, sbyte bombCount)
         : base(effects, marks, gameTurn, fightStart, idols)
        {
            this.SpellCooldowns = spellCooldowns;
            this.SummonCount = summonCount;
            this.BombCount = bombCount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)SpellCooldowns.Length);
            foreach (var entry in SpellCooldowns)
            {
                 entry.Serialize(writer);
            }
            writer.WriteSByte(SummonCount);
            writer.WriteSByte(BombCount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            SpellCooldowns = new Types.GameFightSpellCooldown[limit];
            for (int i = 0; i < limit; i++)
            {
                 SpellCooldowns[i] = new Types.GameFightSpellCooldown();
                 SpellCooldowns[i].Deserialize(reader);
            }
            SummonCount = reader.ReadSByte();
            if (SummonCount < 0)
                throw new Exception("Forbidden value on SummonCount = " + SummonCount + ", it doesn't respect the following condition : summonCount < 0");
            BombCount = reader.ReadSByte();
            if (BombCount < 0)
                throw new Exception("Forbidden value on BombCount = " + BombCount + ", it doesn't respect the following condition : bombCount < 0");
        }
        
    }
    
}