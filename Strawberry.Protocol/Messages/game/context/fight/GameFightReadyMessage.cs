

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightReadyMessage : NetworkMessage
    {
        public const uint ProtocolId = 708;
        public override uint MessageID => ProtocolId;
        
        public bool IsReady { get; set; }
        
        public GameFightReadyMessage()
        {
        }
        
        public GameFightReadyMessage(bool isReady)
        {
            this.IsReady = isReady;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(IsReady);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IsReady = reader.ReadBoolean();
        }
        
    }
    
}