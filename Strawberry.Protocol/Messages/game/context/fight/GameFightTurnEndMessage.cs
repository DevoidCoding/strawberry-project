

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightTurnEndMessage : NetworkMessage
    {
        public const uint ProtocolId = 719;
        public override uint MessageID => ProtocolId;
        
        public double Id { get; set; }
        
        public GameFightTurnEndMessage()
        {
        }
        
        public GameFightTurnEndMessage(double id)
        {
            this.Id = id;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Id);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadDouble();
            if (Id < -9007199254740990 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < -9007199254740990 || id > 9007199254740990");
        }
        
    }
    
}