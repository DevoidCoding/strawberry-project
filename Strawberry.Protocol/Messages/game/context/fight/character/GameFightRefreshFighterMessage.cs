

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightRefreshFighterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6309;
        public override uint MessageID => ProtocolId;
        
        public Types.GameContextActorInformations Informations { get; set; }
        
        public GameFightRefreshFighterMessage()
        {
        }
        
        public GameFightRefreshFighterMessage(Types.GameContextActorInformations informations)
        {
            this.Informations = informations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Informations.TypeID);
            Informations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Informations = Types.ProtocolTypeManager.GetInstance<Types.GameContextActorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
        
    }
    
}