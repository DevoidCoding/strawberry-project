

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightNewWaveMessage : NetworkMessage
    {
        public const uint ProtocolId = 6490;
        public override uint MessageID => ProtocolId;
        
        public sbyte Id { get; set; }
        public sbyte TeamId { get; set; }
        public short NbTurnBeforeNextWave { get; set; }
        
        public GameFightNewWaveMessage()
        {
        }
        
        public GameFightNewWaveMessage(sbyte id, sbyte teamId, short nbTurnBeforeNextWave)
        {
            this.Id = id;
            this.TeamId = teamId;
            this.NbTurnBeforeNextWave = nbTurnBeforeNextWave;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Id);
            writer.WriteSByte(TeamId);
            writer.WriteShort(NbTurnBeforeNextWave);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadSByte();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            NbTurnBeforeNextWave = reader.ReadShort();
        }
        
    }
    
}