

// Generated on 02/12/2018 03:56:19
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightSpectatorJoinMessage : GameFightJoinMessage
    {
        public new const uint ProtocolId = 6504;
        public override uint MessageID => ProtocolId;
        
        public Types.NamedPartyTeam[] NamedPartyTeams { get; set; }
        
        public GameFightSpectatorJoinMessage()
        {
        }
        
        public GameFightSpectatorJoinMessage(bool isTeamPhase, bool canBeCancelled, bool canSayReady, bool isFightStarted, short timeMaxBeforeFightStart, sbyte fightType, Types.NamedPartyTeam[] namedPartyTeams)
         : base(isTeamPhase, canBeCancelled, canSayReady, isFightStarted, timeMaxBeforeFightStart, fightType)
        {
            this.NamedPartyTeams = namedPartyTeams;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)NamedPartyTeams.Length);
            foreach (var entry in NamedPartyTeams)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            NamedPartyTeams = new Types.NamedPartyTeam[limit];
            for (int i = 0; i < limit; i++)
            {
                 NamedPartyTeams[i] = new Types.NamedPartyTeam();
                 NamedPartyTeams[i].Deserialize(reader);
            }
        }
        
    }
    
}