

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightJoinMessage : NetworkMessage
    {
        public const uint ProtocolId = 702;
        public override uint MessageID => ProtocolId;
        
        public bool IsTeamPhase { get; set; }
        public bool CanBeCancelled { get; set; }
        public bool CanSayReady { get; set; }
        public bool IsFightStarted { get; set; }
        public short TimeMaxBeforeFightStart { get; set; }
        public sbyte FightType { get; set; }
        
        public GameFightJoinMessage()
        {
        }
        
        public GameFightJoinMessage(bool isTeamPhase, bool canBeCancelled, bool canSayReady, bool isFightStarted, short timeMaxBeforeFightStart, sbyte fightType)
        {
            this.IsTeamPhase = isTeamPhase;
            this.CanBeCancelled = canBeCancelled;
            this.CanSayReady = canSayReady;
            this.IsFightStarted = isFightStarted;
            this.TimeMaxBeforeFightStart = timeMaxBeforeFightStart;
            this.FightType = fightType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, IsTeamPhase);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CanBeCancelled);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, CanSayReady);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, IsFightStarted);
            writer.WriteByte(flag1);
            writer.WriteShort(TimeMaxBeforeFightStart);
            writer.WriteSByte(FightType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            IsTeamPhase = BooleanByteWrapper.GetFlag(flag1, 0);
            CanBeCancelled = BooleanByteWrapper.GetFlag(flag1, 1);
            CanSayReady = BooleanByteWrapper.GetFlag(flag1, 2);
            IsFightStarted = BooleanByteWrapper.GetFlag(flag1, 3);
            TimeMaxBeforeFightStart = reader.ReadShort();
            if (TimeMaxBeforeFightStart < 0)
                throw new Exception("Forbidden value on TimeMaxBeforeFightStart = " + TimeMaxBeforeFightStart + ", it doesn't respect the following condition : timeMaxBeforeFightStart < 0");
            FightType = reader.ReadSByte();
            if (FightType < 0)
                throw new Exception("Forbidden value on FightType = " + FightType + ", it doesn't respect the following condition : fightType < 0");
        }
        
    }
    
}