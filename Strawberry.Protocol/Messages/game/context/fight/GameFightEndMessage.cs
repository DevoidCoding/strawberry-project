

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightEndMessage : NetworkMessage
    {
        public const uint ProtocolId = 720;
        public override uint MessageID => ProtocolId;
        
        public int Duration { get; set; }
        public short AgeBonus { get; set; }
        public short LootShareLimitMalus { get; set; }
        public Types.FightResultListEntry[] Results { get; set; }
        public Types.NamedPartyTeamWithOutcome[] NamedPartyTeamsOutcomes { get; set; }
        
        public GameFightEndMessage()
        {
        }
        
        public GameFightEndMessage(int duration, short ageBonus, short lootShareLimitMalus, Types.FightResultListEntry[] results, Types.NamedPartyTeamWithOutcome[] namedPartyTeamsOutcomes)
        {
            this.Duration = duration;
            this.AgeBonus = ageBonus;
            this.LootShareLimitMalus = lootShareLimitMalus;
            this.Results = results;
            this.NamedPartyTeamsOutcomes = namedPartyTeamsOutcomes;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Duration);
            writer.WriteShort(AgeBonus);
            writer.WriteShort(LootShareLimitMalus);
            writer.WriteUShort((ushort)Results.Length);
            foreach (var entry in Results)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)NamedPartyTeamsOutcomes.Length);
            foreach (var entry in NamedPartyTeamsOutcomes)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Duration = reader.ReadInt();
            if (Duration < 0)
                throw new Exception("Forbidden value on Duration = " + Duration + ", it doesn't respect the following condition : duration < 0");
            AgeBonus = reader.ReadShort();
            LootShareLimitMalus = reader.ReadShort();
            var limit = reader.ReadUShort();
            Results = new Types.FightResultListEntry[limit];
            for (int i = 0; i < limit; i++)
            {
                 Results[i] = Types.ProtocolTypeManager.GetInstance<Types.FightResultListEntry>(reader.ReadShort());
                 Results[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            NamedPartyTeamsOutcomes = new Types.NamedPartyTeamWithOutcome[limit];
            for (int i = 0; i < limit; i++)
            {
                 NamedPartyTeamsOutcomes[i] = new Types.NamedPartyTeamWithOutcome();
                 NamedPartyTeamsOutcomes[i].Deserialize(reader);
            }
        }
        
    }
    
}