

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameFightOptionStateUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5927;
        public override uint MessageID => ProtocolId;
        
        public ushort FightId { get; set; }
        public sbyte TeamId { get; set; }
        public sbyte Option { get; set; }
        public bool State { get; set; }
        
        public GameFightOptionStateUpdateMessage()
        {
        }
        
        public GameFightOptionStateUpdateMessage(ushort fightId, sbyte teamId, sbyte option, bool state)
        {
            this.FightId = fightId;
            this.TeamId = teamId;
            this.Option = option;
            this.State = state;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(FightId);
            writer.WriteSByte(TeamId);
            writer.WriteSByte(Option);
            writer.WriteBoolean(State);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            TeamId = reader.ReadSByte();
            if (TeamId < 0)
                throw new Exception("Forbidden value on TeamId = " + TeamId + ", it doesn't respect the following condition : teamId < 0");
            Option = reader.ReadSByte();
            if (Option < 0)
                throw new Exception("Forbidden value on Option = " + Option + ", it doesn't respect the following condition : option < 0");
            State = reader.ReadBoolean();
        }
        
    }
    
}