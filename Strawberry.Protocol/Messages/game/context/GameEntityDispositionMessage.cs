

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameEntityDispositionMessage : NetworkMessage
    {
        public const uint ProtocolId = 5693;
        public override uint MessageID => ProtocolId;
        
        public Types.IdentifiedEntityDispositionInformations Disposition { get; set; }
        
        public GameEntityDispositionMessage()
        {
        }
        
        public GameEntityDispositionMessage(Types.IdentifiedEntityDispositionInformations disposition)
        {
            this.Disposition = disposition;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Disposition.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Disposition = new Types.IdentifiedEntityDispositionInformations();
            Disposition.Deserialize(reader);
        }
        
    }
    
}