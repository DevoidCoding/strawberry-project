

// Generated on 02/12/2018 03:56:18
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DisplayNumericalValuePaddockMessage : NetworkMessage
    {
        public const uint ProtocolId = 6563;
        public override uint MessageID => ProtocolId;
        
        public int RideId { get; set; }
        public int Value { get; set; }
        public sbyte Type { get; set; }
        
        public DisplayNumericalValuePaddockMessage()
        {
        }
        
        public DisplayNumericalValuePaddockMessage(int rideId, int value, sbyte type)
        {
            this.RideId = rideId;
            this.Value = value;
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(RideId);
            writer.WriteInt(Value);
            writer.WriteSByte(Type);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RideId = reader.ReadInt();
            Value = reader.ReadInt();
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}