

// Generated on 02/12/2018 03:56:17
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameMapChangeOrientationMessage : NetworkMessage
    {
        public const uint ProtocolId = 946;
        public override uint MessageID => ProtocolId;
        
        public Types.ActorOrientation Orientation { get; set; }
        
        public GameMapChangeOrientationMessage()
        {
        }
        
        public GameMapChangeOrientationMessage(Types.ActorOrientation orientation)
        {
            this.Orientation = orientation;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Orientation.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Orientation = new Types.ActorOrientation();
            Orientation.Deserialize(reader);
        }
        
    }
    
}