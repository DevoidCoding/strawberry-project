

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceInvitationAnswerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6401;
        public override uint MessageID => ProtocolId;
        
        public bool Accept { get; set; }
        
        public AllianceInvitationAnswerMessage()
        {
        }
        
        public AllianceInvitationAnswerMessage(bool accept)
        {
            this.Accept = accept;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Accept);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Accept = reader.ReadBoolean();
        }
        
    }
    
}