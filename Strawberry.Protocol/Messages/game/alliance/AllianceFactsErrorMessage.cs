

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceFactsErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6423;
        public override uint MessageID => ProtocolId;
        
        public uint AllianceId { get; set; }
        
        public AllianceFactsErrorMessage()
        {
        }
        
        public AllianceFactsErrorMessage(uint allianceId)
        {
            this.AllianceId = allianceId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(AllianceId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AllianceId = reader.ReadVarUhInt();
            if (AllianceId < 0)
                throw new Exception("Forbidden value on AllianceId = " + AllianceId + ", it doesn't respect the following condition : allianceId < 0");
        }
        
    }
    
}