

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceVersatileInfoListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6436;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceVersatileInformations[] Alliances { get; set; }
        
        public AllianceVersatileInfoListMessage()
        {
        }
        
        public AllianceVersatileInfoListMessage(Types.AllianceVersatileInformations[] alliances)
        {
            this.Alliances = alliances;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Alliances.Length);
            foreach (var entry in Alliances)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Alliances = new Types.AllianceVersatileInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Alliances[i] = new Types.AllianceVersatileInformations();
                 Alliances[i].Deserialize(reader);
            }
        }
        
    }
    
}