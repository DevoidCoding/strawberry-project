

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceInvitationMessage : NetworkMessage
    {
        public const uint ProtocolId = 6395;
        public override uint MessageID => ProtocolId;
        
        public ulong TargetId { get; set; }
        
        public AllianceInvitationMessage()
        {
        }
        
        public AllianceInvitationMessage(ulong targetId)
        {
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TargetId = reader.ReadVarUhLong();
            if (TargetId < 0 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < 0 || targetId > 9007199254740990");
        }
        
    }
    
}