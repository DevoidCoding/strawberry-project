

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceInvitationStateRecruterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6396;
        public override uint MessageID => ProtocolId;
        
        public string RecrutedName { get; set; }
        public sbyte InvitationState { get; set; }
        
        public AllianceInvitationStateRecruterMessage()
        {
        }
        
        public AllianceInvitationStateRecruterMessage(string recrutedName, sbyte invitationState)
        {
            this.RecrutedName = recrutedName;
            this.InvitationState = invitationState;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(RecrutedName);
            writer.WriteSByte(InvitationState);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RecrutedName = reader.ReadUTF();
            InvitationState = reader.ReadSByte();
            if (InvitationState < 0)
                throw new Exception("Forbidden value on InvitationState = " + InvitationState + ", it doesn't respect the following condition : invitationState < 0");
        }
        
    }
    
}