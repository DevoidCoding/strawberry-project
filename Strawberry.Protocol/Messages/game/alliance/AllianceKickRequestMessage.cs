

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceKickRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6400;
        public override uint MessageID => ProtocolId;
        
        public uint KickedId { get; set; }
        
        public AllianceKickRequestMessage()
        {
        }
        
        public AllianceKickRequestMessage(uint kickedId)
        {
            this.KickedId = kickedId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(KickedId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            KickedId = reader.ReadVarUhInt();
            if (KickedId < 0)
                throw new Exception("Forbidden value on KickedId = " + KickedId + ", it doesn't respect the following condition : kickedId < 0");
        }
        
    }
    
}