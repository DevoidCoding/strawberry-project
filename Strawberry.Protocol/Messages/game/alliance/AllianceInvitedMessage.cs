

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceInvitedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6397;
        public override uint MessageID => ProtocolId;
        
        public ulong RecruterId { get; set; }
        public string RecruterName { get; set; }
        public Types.BasicNamedAllianceInformations AllianceInfo { get; set; }
        
        public AllianceInvitedMessage()
        {
        }
        
        public AllianceInvitedMessage(ulong recruterId, string recruterName, Types.BasicNamedAllianceInformations allianceInfo)
        {
            this.RecruterId = recruterId;
            this.RecruterName = recruterName;
            this.AllianceInfo = allianceInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(RecruterId);
            writer.WriteUTF(RecruterName);
            AllianceInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RecruterId = reader.ReadVarUhLong();
            if (RecruterId < 0 || RecruterId > 9007199254740990)
                throw new Exception("Forbidden value on RecruterId = " + RecruterId + ", it doesn't respect the following condition : recruterId < 0 || recruterId > 9007199254740990");
            RecruterName = reader.ReadUTF();
            AllianceInfo = new Types.BasicNamedAllianceInformations();
            AllianceInfo.Deserialize(reader);
        }
        
    }
    
}