

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceModificationStartedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6444;
        public override uint MessageID => ProtocolId;
        
        public bool CanChangeName { get; set; }
        public bool CanChangeTag { get; set; }
        public bool CanChangeEmblem { get; set; }
        
        public AllianceModificationStartedMessage()
        {
        }
        
        public AllianceModificationStartedMessage(bool canChangeName, bool canChangeTag, bool canChangeEmblem)
        {
            this.CanChangeName = canChangeName;
            this.CanChangeTag = canChangeTag;
            this.CanChangeEmblem = canChangeEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, CanChangeName);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CanChangeTag);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, CanChangeEmblem);
            writer.WriteByte(flag1);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            CanChangeName = BooleanByteWrapper.GetFlag(flag1, 0);
            CanChangeTag = BooleanByteWrapper.GetFlag(flag1, 1);
            CanChangeEmblem = BooleanByteWrapper.GetFlag(flag1, 2);
        }
        
    }
    
}