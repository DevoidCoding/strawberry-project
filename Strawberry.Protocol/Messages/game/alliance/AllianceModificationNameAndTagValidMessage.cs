

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceModificationNameAndTagValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6449;
        public override uint MessageID => ProtocolId;
        
        public string AllianceName { get; set; }
        public string AllianceTag { get; set; }
        
        public AllianceModificationNameAndTagValidMessage()
        {
        }
        
        public AllianceModificationNameAndTagValidMessage(string allianceName, string allianceTag)
        {
            this.AllianceName = allianceName;
            this.AllianceTag = allianceTag;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(AllianceName);
            writer.WriteUTF(AllianceTag);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AllianceName = reader.ReadUTF();
            AllianceTag = reader.ReadUTF();
        }
        
    }
    
}