

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6408;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceFactSheetInformations[] Alliances { get; set; }
        
        public AllianceListMessage()
        {
        }
        
        public AllianceListMessage(Types.AllianceFactSheetInformations[] alliances)
        {
            this.Alliances = alliances;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Alliances.Length);
            foreach (var entry in Alliances)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Alliances = new Types.AllianceFactSheetInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Alliances[i] = new Types.AllianceFactSheetInformations();
                 Alliances[i].Deserialize(reader);
            }
        }
        
    }
    
}