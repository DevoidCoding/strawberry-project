

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceFactsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6414;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceFactSheetInformations Infos { get; set; }
        public Types.GuildInAllianceInformations[] Guilds { get; set; }
        public ushort[] ControlledSubareaIds { get; set; }
        public ulong LeaderCharacterId { get; set; }
        public string LeaderCharacterName { get; set; }
        
        public AllianceFactsMessage()
        {
        }
        
        public AllianceFactsMessage(Types.AllianceFactSheetInformations infos, Types.GuildInAllianceInformations[] guilds, ushort[] controlledSubareaIds, ulong leaderCharacterId, string leaderCharacterName)
        {
            this.Infos = infos;
            this.Guilds = guilds;
            this.ControlledSubareaIds = controlledSubareaIds;
            this.LeaderCharacterId = leaderCharacterId;
            this.LeaderCharacterName = leaderCharacterName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Infos.TypeID);
            Infos.Serialize(writer);
            writer.WriteUShort((ushort)Guilds.Length);
            foreach (var entry in Guilds)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)ControlledSubareaIds.Length);
            foreach (var entry in ControlledSubareaIds)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteVarLong(LeaderCharacterId);
            writer.WriteUTF(LeaderCharacterName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Infos = Types.ProtocolTypeManager.GetInstance<Types.AllianceFactSheetInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
            var limit = reader.ReadUShort();
            Guilds = new Types.GuildInAllianceInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Guilds[i] = new Types.GuildInAllianceInformations();
                 Guilds[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            ControlledSubareaIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 ControlledSubareaIds[i] = reader.ReadVarUhShort();
            }
            LeaderCharacterId = reader.ReadVarUhLong();
            if (LeaderCharacterId < 0 || LeaderCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on LeaderCharacterId = " + LeaderCharacterId + ", it doesn't respect the following condition : leaderCharacterId < 0 || leaderCharacterId > 9007199254740990");
            LeaderCharacterName = reader.ReadUTF();
        }
        
    }
    
}