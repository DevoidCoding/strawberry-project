

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AlliancePartialListMessage : AllianceListMessage
    {
        public new const uint ProtocolId = 6427;
        public override uint MessageID => ProtocolId;
        
        
        public AlliancePartialListMessage()
        {
        }
        
        public AlliancePartialListMessage(Types.AllianceFactSheetInformations[] alliances)
         : base(alliances)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}