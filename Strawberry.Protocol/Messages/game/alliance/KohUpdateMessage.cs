

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class KohUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6439;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceInformations[] Alliances { get; set; }
        public ushort[] AllianceNbMembers { get; set; }
        public uint[] AllianceRoundWeigth { get; set; }
        public sbyte[] AllianceMatchScore { get; set; }
        public Types.BasicAllianceInformations[] AllianceMapWinners { get; set; }
        public uint AllianceMapWinnerScore { get; set; }
        public uint AllianceMapMyAllianceScore { get; set; }
        public double NextTickTime { get; set; }
        
        public KohUpdateMessage()
        {
        }
        
        public KohUpdateMessage(Types.AllianceInformations[] alliances, ushort[] allianceNbMembers, uint[] allianceRoundWeigth, sbyte[] allianceMatchScore, Types.BasicAllianceInformations[] allianceMapWinners, uint allianceMapWinnerScore, uint allianceMapMyAllianceScore, double nextTickTime)
        {
            this.Alliances = alliances;
            this.AllianceNbMembers = allianceNbMembers;
            this.AllianceRoundWeigth = allianceRoundWeigth;
            this.AllianceMatchScore = allianceMatchScore;
            this.AllianceMapWinners = allianceMapWinners;
            this.AllianceMapWinnerScore = allianceMapWinnerScore;
            this.AllianceMapMyAllianceScore = allianceMapMyAllianceScore;
            this.NextTickTime = nextTickTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Alliances.Length);
            foreach (var entry in Alliances)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)AllianceNbMembers.Length);
            foreach (var entry in AllianceNbMembers)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)AllianceRoundWeigth.Length);
            foreach (var entry in AllianceRoundWeigth)
            {
                 writer.WriteVarInt(entry);
            }
            writer.WriteUShort((ushort)AllianceMatchScore.Length);
            foreach (var entry in AllianceMatchScore)
            {
                 writer.WriteSByte(entry);
            }
            writer.WriteUShort((ushort)AllianceMapWinners.Length);
            foreach (var entry in AllianceMapWinners)
            {
                 entry.Serialize(writer);
            }
            writer.WriteVarInt(AllianceMapWinnerScore);
            writer.WriteVarInt(AllianceMapMyAllianceScore);
            writer.WriteDouble(NextTickTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Alliances = new Types.AllianceInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Alliances[i] = new Types.AllianceInformations();
                 Alliances[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            AllianceNbMembers = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllianceNbMembers[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            AllianceRoundWeigth = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllianceRoundWeigth[i] = reader.ReadVarUhInt();
            }
            limit = reader.ReadUShort();
            AllianceMatchScore = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllianceMatchScore[i] = reader.ReadSByte();
            }
            limit = reader.ReadUShort();
            AllianceMapWinners = new Types.BasicAllianceInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 AllianceMapWinners[i] = new Types.BasicAllianceInformations();
                 AllianceMapWinners[i].Deserialize(reader);
            }
            AllianceMapWinnerScore = reader.ReadVarUhInt();
            if (AllianceMapWinnerScore < 0)
                throw new Exception("Forbidden value on AllianceMapWinnerScore = " + AllianceMapWinnerScore + ", it doesn't respect the following condition : allianceMapWinnerScore < 0");
            AllianceMapMyAllianceScore = reader.ReadVarUhInt();
            if (AllianceMapMyAllianceScore < 0)
                throw new Exception("Forbidden value on AllianceMapMyAllianceScore = " + AllianceMapMyAllianceScore + ", it doesn't respect the following condition : allianceMapMyAllianceScore < 0");
            NextTickTime = reader.ReadDouble();
            if (NextTickTime < 0 || NextTickTime > 9007199254740990)
                throw new Exception("Forbidden value on NextTickTime = " + NextTickTime + ", it doesn't respect the following condition : nextTickTime < 0 || nextTickTime > 9007199254740990");
        }
        
    }
    
}