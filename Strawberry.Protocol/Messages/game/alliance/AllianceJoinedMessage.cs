

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceJoinedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6402;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceInformations AllianceInfo { get; set; }
        public bool Enabled { get; set; }
        public uint LeadingGuildId { get; set; }
        
        public AllianceJoinedMessage()
        {
        }
        
        public AllianceJoinedMessage(Types.AllianceInformations allianceInfo, bool enabled, uint leadingGuildId)
        {
            this.AllianceInfo = allianceInfo;
            this.Enabled = enabled;
            this.LeadingGuildId = leadingGuildId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            AllianceInfo.Serialize(writer);
            writer.WriteBoolean(Enabled);
            writer.WriteVarInt(LeadingGuildId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AllianceInfo = new Types.AllianceInformations();
            AllianceInfo.Deserialize(reader);
            Enabled = reader.ReadBoolean();
            LeadingGuildId = reader.ReadVarUhInt();
            if (LeadingGuildId < 0)
                throw new Exception("Forbidden value on LeadingGuildId = " + LeadingGuildId + ", it doesn't respect the following condition : leadingGuildId < 0");
        }
        
    }
    
}