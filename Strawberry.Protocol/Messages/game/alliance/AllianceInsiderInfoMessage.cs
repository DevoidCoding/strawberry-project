

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceInsiderInfoMessage : NetworkMessage
    {
        public const uint ProtocolId = 6403;
        public override uint MessageID => ProtocolId;
        
        public Types.AllianceFactSheetInformations AllianceInfos { get; set; }
        public Types.GuildInsiderFactSheetInformations[] Guilds { get; set; }
        public Types.PrismSubareaEmptyInfo[] Prisms { get; set; }
        
        public AllianceInsiderInfoMessage()
        {
        }
        
        public AllianceInsiderInfoMessage(Types.AllianceFactSheetInformations allianceInfos, Types.GuildInsiderFactSheetInformations[] guilds, Types.PrismSubareaEmptyInfo[] prisms)
        {
            this.AllianceInfos = allianceInfos;
            this.Guilds = guilds;
            this.Prisms = prisms;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            AllianceInfos.Serialize(writer);
            writer.WriteUShort((ushort)Guilds.Length);
            foreach (var entry in Guilds)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Prisms.Length);
            foreach (var entry in Prisms)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AllianceInfos = new Types.AllianceFactSheetInformations();
            AllianceInfos.Deserialize(reader);
            var limit = reader.ReadUShort();
            Guilds = new Types.GuildInsiderFactSheetInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Guilds[i] = new Types.GuildInsiderFactSheetInformations();
                 Guilds[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Prisms = new Types.PrismSubareaEmptyInfo[limit];
            for (int i = 0; i < limit; i++)
            {
                 Prisms[i] = Types.ProtocolTypeManager.GetInstance<Types.PrismSubareaEmptyInfo>(reader.ReadShort());
                 Prisms[i].Deserialize(reader);
            }
        }
        
    }
    
}