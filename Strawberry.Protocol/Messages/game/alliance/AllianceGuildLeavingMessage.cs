

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceGuildLeavingMessage : NetworkMessage
    {
        public const uint ProtocolId = 6399;
        public override uint MessageID => ProtocolId;
        
        public bool Kicked { get; set; }
        public uint GuildId { get; set; }
        
        public AllianceGuildLeavingMessage()
        {
        }
        
        public AllianceGuildLeavingMessage(bool kicked, uint guildId)
        {
            this.Kicked = kicked;
            this.GuildId = guildId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Kicked);
            writer.WriteVarInt(GuildId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Kicked = reader.ReadBoolean();
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
        }
        
    }
    
}