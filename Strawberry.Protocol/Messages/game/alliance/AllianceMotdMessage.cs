

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceMotdMessage : SocialNoticeMessage
    {
        public new const uint ProtocolId = 6685;
        public override uint MessageID => ProtocolId;
        
        
        public AllianceMotdMessage()
        {
        }
        
        public AllianceMotdMessage(string content, int timestamp, ulong memberId, string memberName)
         : base(content, timestamp, memberId, memberName)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}