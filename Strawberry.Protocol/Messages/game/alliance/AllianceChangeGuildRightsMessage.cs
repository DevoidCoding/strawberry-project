

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceChangeGuildRightsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6426;
        public override uint MessageID => ProtocolId;
        
        public uint GuildId { get; set; }
        public sbyte Rights { get; set; }
        
        public AllianceChangeGuildRightsMessage()
        {
        }
        
        public AllianceChangeGuildRightsMessage(uint guildId, sbyte rights)
        {
            this.GuildId = guildId;
            this.Rights = rights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(GuildId);
            writer.WriteSByte(Rights);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
            Rights = reader.ReadSByte();
            if (Rights < 0)
                throw new Exception("Forbidden value on Rights = " + Rights + ", it doesn't respect the following condition : rights < 0");
        }
        
    }
    
}