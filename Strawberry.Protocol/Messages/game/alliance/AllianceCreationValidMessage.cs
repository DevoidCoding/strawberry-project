

// Generated on 02/12/2018 03:56:12
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AllianceCreationValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6393;
        public override uint MessageID => ProtocolId;
        
        public string AllianceName { get; set; }
        public string AllianceTag { get; set; }
        public Types.GuildEmblem AllianceEmblem { get; set; }
        
        public AllianceCreationValidMessage()
        {
        }
        
        public AllianceCreationValidMessage(string allianceName, string allianceTag, Types.GuildEmblem allianceEmblem)
        {
            this.AllianceName = allianceName;
            this.AllianceTag = allianceTag;
            this.AllianceEmblem = allianceEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(AllianceName);
            writer.WriteUTF(AllianceTag);
            AllianceEmblem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AllianceName = reader.ReadUTF();
            AllianceTag = reader.ReadUTF();
            AllianceEmblem = new Types.GuildEmblem();
            AllianceEmblem.Deserialize(reader);
        }
        
    }
    
}