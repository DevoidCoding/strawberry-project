

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectModifiedMessage : NetworkMessage
    {
        public const uint ProtocolId = 3029;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem Object { get; set; }
        
        public ObjectModifiedMessage()
        {
        }
        
        public ObjectModifiedMessage(Types.ObjectItem @object)
        {
            this.Object = @object;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Object.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Object = new Types.ObjectItem();
            Object.Deserialize(reader);
        }
        
    }
    
}