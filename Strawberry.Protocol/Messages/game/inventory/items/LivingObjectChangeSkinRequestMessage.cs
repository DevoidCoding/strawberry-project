

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LivingObjectChangeSkinRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5725;
        public override uint MessageID => ProtocolId;
        
        public uint LivingUID { get; set; }
        public byte LivingPosition { get; set; }
        public uint SkinId { get; set; }
        
        public LivingObjectChangeSkinRequestMessage()
        {
        }
        
        public LivingObjectChangeSkinRequestMessage(uint livingUID, byte livingPosition, uint skinId)
        {
            this.LivingUID = livingUID;
            this.LivingPosition = livingPosition;
            this.SkinId = skinId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(LivingUID);
            writer.WriteByte(LivingPosition);
            writer.WriteVarInt(SkinId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            LivingUID = reader.ReadVarUhInt();
            if (LivingUID < 0)
                throw new Exception("Forbidden value on LivingUID = " + LivingUID + ", it doesn't respect the following condition : livingUID < 0");
            LivingPosition = reader.ReadByte();
            if (LivingPosition < 0 || LivingPosition > 255)
                throw new Exception("Forbidden value on LivingPosition = " + LivingPosition + ", it doesn't respect the following condition : livingPosition < 0 || livingPosition > 255");
            SkinId = reader.ReadVarUhInt();
            if (SkinId < 0)
                throw new Exception("Forbidden value on SkinId = " + SkinId + ", it doesn't respect the following condition : skinId < 0");
        }
        
    }
    
}