

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryWeightMessage : NetworkMessage
    {
        public const uint ProtocolId = 3009;
        public override uint MessageID => ProtocolId;
        
        public uint Weight { get; set; }
        public uint WeightMax { get; set; }
        
        public InventoryWeightMessage()
        {
        }
        
        public InventoryWeightMessage(uint weight, uint weightMax)
        {
            this.Weight = weight;
            this.WeightMax = weightMax;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Weight);
            writer.WriteVarInt(WeightMax);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Weight = reader.ReadVarUhInt();
            if (Weight < 0)
                throw new Exception("Forbidden value on Weight = " + Weight + ", it doesn't respect the following condition : weight < 0");
            WeightMax = reader.ReadVarUhInt();
            if (WeightMax < 0)
                throw new Exception("Forbidden value on WeightMax = " + WeightMax + ", it doesn't respect the following condition : weightMax < 0");
        }
        
    }
    
}