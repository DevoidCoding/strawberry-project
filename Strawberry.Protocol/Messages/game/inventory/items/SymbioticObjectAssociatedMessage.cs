

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SymbioticObjectAssociatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6527;
        public override uint MessageID => ProtocolId;
        
        public uint HostUID { get; set; }
        
        public SymbioticObjectAssociatedMessage()
        {
        }
        
        public SymbioticObjectAssociatedMessage(uint hostUID)
        {
            this.HostUID = hostUID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HostUID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HostUID = reader.ReadVarUhInt();
            if (HostUID < 0)
                throw new Exception("Forbidden value on HostUID = " + HostUID + ", it doesn't respect the following condition : hostUID < 0");
        }
        
    }
    
}