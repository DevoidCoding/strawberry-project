

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectsAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6033;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] Object { get; set; }
        
        public ObjectsAddedMessage()
        {
        }
        
        public ObjectsAddedMessage(Types.ObjectItem[] @object)
        {
            this.Object = @object;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Object.Length);
            foreach (var entry in Object)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Object = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Object[i] = new Types.ObjectItem();
                 Object[i].Deserialize(reader);
            }
        }
        
    }
    
}