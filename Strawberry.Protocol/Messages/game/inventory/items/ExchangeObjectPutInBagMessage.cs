

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectPutInBagMessage : ExchangeObjectMessage
    {
        public new const uint ProtocolId = 6009;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem Object { get; set; }
        
        public ExchangeObjectPutInBagMessage()
        {
        }
        
        public ExchangeObjectPutInBagMessage(bool remote, Types.ObjectItem @object)
         : base(remote)
        {
            this.Object = @object;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Object.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Object = new Types.ObjectItem();
            Object.Deserialize(reader);
        }
        
    }
    
}