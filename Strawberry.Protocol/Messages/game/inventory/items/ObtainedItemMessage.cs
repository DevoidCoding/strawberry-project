

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObtainedItemMessage : NetworkMessage
    {
        public const uint ProtocolId = 6519;
        public override uint MessageID => ProtocolId;
        
        public ushort GenericId { get; set; }
        public uint BaseQuantity { get; set; }
        
        public ObtainedItemMessage()
        {
        }
        
        public ObtainedItemMessage(ushort genericId, uint baseQuantity)
        {
            this.GenericId = genericId;
            this.BaseQuantity = baseQuantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(GenericId);
            writer.WriteVarInt(BaseQuantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GenericId = reader.ReadVarUhShort();
            if (GenericId < 0)
                throw new Exception("Forbidden value on GenericId = " + GenericId + ", it doesn't respect the following condition : genericId < 0");
            BaseQuantity = reader.ReadVarUhInt();
            if (BaseQuantity < 0)
                throw new Exception("Forbidden value on BaseQuantity = " + BaseQuantity + ", it doesn't respect the following condition : baseQuantity < 0");
        }
        
    }
    
}