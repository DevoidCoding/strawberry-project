

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class WrapperObjectDissociateRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6524;
        public override uint MessageID => ProtocolId;
        
        public uint HostUID { get; set; }
        public byte HostPos { get; set; }
        
        public WrapperObjectDissociateRequestMessage()
        {
        }
        
        public WrapperObjectDissociateRequestMessage(uint hostUID, byte hostPos)
        {
            this.HostUID = hostUID;
            this.HostPos = hostPos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HostUID);
            writer.WriteByte(HostPos);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HostUID = reader.ReadVarUhInt();
            if (HostUID < 0)
                throw new Exception("Forbidden value on HostUID = " + HostUID + ", it doesn't respect the following condition : hostUID < 0");
            HostPos = reader.ReadByte();
            if (HostPos < 0 || HostPos > 255)
                throw new Exception("Forbidden value on HostPos = " + HostPos + ", it doesn't respect the following condition : hostPos < 0 || hostPos > 255");
        }
        
    }
    
}