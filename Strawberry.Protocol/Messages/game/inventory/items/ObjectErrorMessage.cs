

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 3004;
        public override uint MessageID => ProtocolId;
        
        public sbyte Reason { get; set; }
        
        public ObjectErrorMessage()
        {
        }
        
        public ObjectErrorMessage(sbyte reason)
        {
            this.Reason = reason;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Reason);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Reason = reader.ReadSByte();
        }
        
    }
    
}