

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryContentAndPresetMessage : InventoryContentMessage
    {
        public new const uint ProtocolId = 6162;
        public override uint MessageID => ProtocolId;
        
        public Types.Preset[] Presets { get; set; }
        public Types.IdolsPreset[] IdolsPresets { get; set; }
        
        public InventoryContentAndPresetMessage()
        {
        }
        
        public InventoryContentAndPresetMessage(Types.ObjectItem[] objects, ulong kamas, Types.Preset[] presets, Types.IdolsPreset[] idolsPresets)
         : base(objects, kamas)
        {
            this.Presets = presets;
            this.IdolsPresets = idolsPresets;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Presets.Length);
            foreach (var entry in Presets)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)IdolsPresets.Length);
            foreach (var entry in IdolsPresets)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Presets = new Types.Preset[limit];
            for (int i = 0; i < limit; i++)
            {
                 Presets[i] = new Types.Preset();
                 Presets[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            IdolsPresets = new Types.IdolsPreset[limit];
            for (int i = 0; i < limit; i++)
            {
                 IdolsPresets[i] = new Types.IdolsPreset();
                 IdolsPresets[i].Deserialize(reader);
            }
        }
        
    }
    
}