

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectUseMultipleMessage : ObjectUseMessage
    {
        public new const uint ProtocolId = 6234;
        public override uint MessageID => ProtocolId;
        
        public uint Quantity { get; set; }
        
        public ObjectUseMultipleMessage()
        {
        }
        
        public ObjectUseMultipleMessage(uint objectUID, uint quantity)
         : base(objectUID)
        {
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}