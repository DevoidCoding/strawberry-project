

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectMovementMessage : NetworkMessage
    {
        public const uint ProtocolId = 3010;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public byte Position { get; set; }
        
        public ObjectMovementMessage()
        {
        }
        
        public ObjectMovementMessage(uint objectUID, byte position)
        {
            this.ObjectUID = objectUID;
            this.Position = position;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteByte(Position);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Position = reader.ReadByte();
            if (Position < 0 || Position > 255)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 255");
        }
        
    }
    
}