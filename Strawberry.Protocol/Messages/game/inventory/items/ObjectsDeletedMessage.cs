

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectsDeletedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6034;
        public override uint MessageID => ProtocolId;
        
        public uint[] ObjectUID { get; set; }
        
        public ObjectsDeletedMessage()
        {
        }
        
        public ObjectsDeletedMessage(uint[] objectUID)
        {
            this.ObjectUID = objectUID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectUID.Length);
            foreach (var entry in ObjectUID)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectUID = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectUID[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}