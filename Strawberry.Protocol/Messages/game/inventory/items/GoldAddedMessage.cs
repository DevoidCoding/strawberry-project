

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GoldAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6030;
        public override uint MessageID => ProtocolId;
        
        public Types.GoldItem Gold { get; set; }
        
        public GoldAddedMessage()
        {
        }
        
        public GoldAddedMessage(Types.GoldItem gold)
        {
            this.Gold = gold;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Gold.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Gold = new Types.GoldItem();
            Gold.Deserialize(reader);
        }
        
    }
    
}