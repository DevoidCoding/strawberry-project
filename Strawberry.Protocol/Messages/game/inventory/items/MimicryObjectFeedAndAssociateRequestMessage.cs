

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MimicryObjectFeedAndAssociateRequestMessage : SymbioticObjectAssociateRequestMessage
    {
        public new const uint ProtocolId = 6460;
        public override uint MessageID => ProtocolId;
        
        public uint FoodUID { get; set; }
        public byte FoodPos { get; set; }
        public bool Preview { get; set; }
        
        public MimicryObjectFeedAndAssociateRequestMessage()
        {
        }
        
        public MimicryObjectFeedAndAssociateRequestMessage(uint symbioteUID, byte symbiotePos, uint hostUID, byte hostPos, uint foodUID, byte foodPos, bool preview)
         : base(symbioteUID, symbiotePos, hostUID, hostPos)
        {
            this.FoodUID = foodUID;
            this.FoodPos = foodPos;
            this.Preview = preview;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(FoodUID);
            writer.WriteByte(FoodPos);
            writer.WriteBoolean(Preview);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FoodUID = reader.ReadVarUhInt();
            if (FoodUID < 0)
                throw new Exception("Forbidden value on FoodUID = " + FoodUID + ", it doesn't respect the following condition : foodUID < 0");
            FoodPos = reader.ReadByte();
            if (FoodPos < 0 || FoodPos > 255)
                throw new Exception("Forbidden value on FoodPos = " + FoodPos + ", it doesn't respect the following condition : foodPos < 0 || foodPos > 255");
            Preview = reader.ReadBoolean();
        }
        
    }
    
}