

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectUseOnCharacterMessage : ObjectUseMessage
    {
        public new const uint ProtocolId = 3003;
        public override uint MessageID => ProtocolId;
        
        public ulong CharacterId { get; set; }
        
        public ObjectUseOnCharacterMessage()
        {
        }
        
        public ObjectUseOnCharacterMessage(uint objectUID, ulong characterId)
         : base(objectUID)
        {
            this.CharacterId = characterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(CharacterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            CharacterId = reader.ReadVarUhLong();
            if (CharacterId < 0 || CharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CharacterId = " + CharacterId + ", it doesn't respect the following condition : characterId < 0 || characterId > 9007199254740990");
        }
        
    }
    
}