

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectFeedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6290;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public uint FoodUID { get; set; }
        public uint FoodQuantity { get; set; }
        
        public ObjectFeedMessage()
        {
        }
        
        public ObjectFeedMessage(uint objectUID, uint foodUID, uint foodQuantity)
        {
            this.ObjectUID = objectUID;
            this.FoodUID = foodUID;
            this.FoodQuantity = foodQuantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(FoodUID);
            writer.WriteVarInt(FoodQuantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            FoodUID = reader.ReadVarUhInt();
            if (FoodUID < 0)
                throw new Exception("Forbidden value on FoodUID = " + FoodUID + ", it doesn't respect the following condition : foodUID < 0");
            FoodQuantity = reader.ReadVarUhInt();
            if (FoodQuantity < 0)
                throw new Exception("Forbidden value on FoodQuantity = " + FoodQuantity + ", it doesn't respect the following condition : foodQuantity < 0");
        }
        
    }
    
}