

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectUseOnCellMessage : ObjectUseMessage
    {
        public new const uint ProtocolId = 3013;
        public override uint MessageID => ProtocolId;
        
        public ushort Cells { get; set; }
        
        public ObjectUseOnCellMessage()
        {
        }
        
        public ObjectUseOnCellMessage(uint objectUID, ushort cells)
         : base(objectUID)
        {
            this.Cells = cells;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(Cells);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Cells = reader.ReadVarUhShort();
            if (Cells < 0 || Cells > 559)
                throw new Exception("Forbidden value on Cells = " + Cells + ", it doesn't respect the following condition : cells < 0 || cells > 559");
        }
        
    }
    
}