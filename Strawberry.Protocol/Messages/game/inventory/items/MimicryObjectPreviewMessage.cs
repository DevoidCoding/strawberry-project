

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MimicryObjectPreviewMessage : NetworkMessage
    {
        public const uint ProtocolId = 6458;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem Result { get; set; }
        
        public MimicryObjectPreviewMessage()
        {
        }
        
        public MimicryObjectPreviewMessage(Types.ObjectItem result)
        {
            this.Result = result;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Result.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Result = new Types.ObjectItem();
            Result.Deserialize(reader);
        }
        
    }
    
}