

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectsQuantityMessage : NetworkMessage
    {
        public const uint ProtocolId = 6206;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemQuantity[] ObjectsUIDAndQty { get; set; }
        
        public ObjectsQuantityMessage()
        {
        }
        
        public ObjectsQuantityMessage(Types.ObjectItemQuantity[] objectsUIDAndQty)
        {
            this.ObjectsUIDAndQty = objectsUIDAndQty;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectsUIDAndQty.Length);
            foreach (var entry in ObjectsUIDAndQty)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectsUIDAndQty = new Types.ObjectItemQuantity[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsUIDAndQty[i] = new Types.ObjectItemQuantity();
                 ObjectsUIDAndQty[i].Deserialize(reader);
            }
        }
        
    }
    
}