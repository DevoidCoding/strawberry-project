

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SymbioticObjectAssociateRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6522;
        public override uint MessageID => ProtocolId;
        
        public uint SymbioteUID { get; set; }
        public byte SymbiotePos { get; set; }
        public uint HostUID { get; set; }
        public byte HostPos { get; set; }
        
        public SymbioticObjectAssociateRequestMessage()
        {
        }
        
        public SymbioticObjectAssociateRequestMessage(uint symbioteUID, byte symbiotePos, uint hostUID, byte hostPos)
        {
            this.SymbioteUID = symbioteUID;
            this.SymbiotePos = symbiotePos;
            this.HostUID = hostUID;
            this.HostPos = hostPos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(SymbioteUID);
            writer.WriteByte(SymbiotePos);
            writer.WriteVarInt(HostUID);
            writer.WriteByte(HostPos);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SymbioteUID = reader.ReadVarUhInt();
            if (SymbioteUID < 0)
                throw new Exception("Forbidden value on SymbioteUID = " + SymbioteUID + ", it doesn't respect the following condition : symbioteUID < 0");
            SymbiotePos = reader.ReadByte();
            if (SymbiotePos < 0 || SymbiotePos > 255)
                throw new Exception("Forbidden value on SymbiotePos = " + SymbiotePos + ", it doesn't respect the following condition : symbiotePos < 0 || symbiotePos > 255");
            HostUID = reader.ReadVarUhInt();
            if (HostUID < 0)
                throw new Exception("Forbidden value on HostUID = " + HostUID + ", it doesn't respect the following condition : hostUID < 0");
            HostPos = reader.ReadByte();
            if (HostPos < 0 || HostPos > 255)
                throw new Exception("Forbidden value on HostPos = " + HostPos + ", it doesn't respect the following condition : hostPos < 0 || hostPos > 255");
        }
        
    }
    
}