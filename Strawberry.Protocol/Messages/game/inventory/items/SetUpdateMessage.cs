

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SetUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5503;
        public override uint MessageID => ProtocolId;
        
        public ushort SetId { get; set; }
        public ushort[] SetObjects { get; set; }
        public Types.ObjectEffect[] SetEffects { get; set; }
        
        public SetUpdateMessage()
        {
        }
        
        public SetUpdateMessage(ushort setId, ushort[] setObjects, Types.ObjectEffect[] setEffects)
        {
            this.SetId = setId;
            this.SetObjects = setObjects;
            this.SetEffects = setEffects;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SetId);
            writer.WriteUShort((ushort)SetObjects.Length);
            foreach (var entry in SetObjects)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)SetEffects.Length);
            foreach (var entry in SetEffects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SetId = reader.ReadVarUhShort();
            if (SetId < 0)
                throw new Exception("Forbidden value on SetId = " + SetId + ", it doesn't respect the following condition : setId < 0");
            var limit = reader.ReadUShort();
            SetObjects = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 SetObjects[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            SetEffects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 SetEffects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 SetEffects[i].Deserialize(reader);
            }
        }
        
    }
    
}