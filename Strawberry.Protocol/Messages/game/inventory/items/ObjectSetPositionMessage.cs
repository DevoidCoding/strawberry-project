

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectSetPositionMessage : NetworkMessage
    {
        public const uint ProtocolId = 3021;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public byte Position { get; set; }
        public uint Quantity { get; set; }
        
        public ObjectSetPositionMessage()
        {
        }
        
        public ObjectSetPositionMessage(uint objectUID, byte position, uint quantity)
        {
            this.ObjectUID = objectUID;
            this.Position = position;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteByte(Position);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Position = reader.ReadByte();
            if (Position < 0 || Position > 255)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 255");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}