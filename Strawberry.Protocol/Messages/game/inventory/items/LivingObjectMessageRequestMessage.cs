

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LivingObjectMessageRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6066;
        public override uint MessageID => ProtocolId;
        
        public ushort MsgId { get; set; }
        public string[] Parameters { get; set; }
        public uint LivingObject { get; set; }
        
        public LivingObjectMessageRequestMessage()
        {
        }
        
        public LivingObjectMessageRequestMessage(ushort msgId, string[] parameters, uint livingObject)
        {
            this.MsgId = msgId;
            this.Parameters = parameters;
            this.LivingObject = livingObject;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(MsgId);
            writer.WriteUShort((ushort)Parameters.Length);
            foreach (var entry in Parameters)
            {
                 writer.WriteUTF(entry);
            }
            writer.WriteVarInt(LivingObject);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MsgId = reader.ReadVarUhShort();
            if (MsgId < 0)
                throw new Exception("Forbidden value on MsgId = " + MsgId + ", it doesn't respect the following condition : msgId < 0");
            var limit = reader.ReadUShort();
            Parameters = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Parameters[i] = reader.ReadUTF();
            }
            LivingObject = reader.ReadVarUhInt();
            if (LivingObject < 0)
                throw new Exception("Forbidden value on LivingObject = " + LivingObject + ", it doesn't respect the following condition : livingObject < 0");
        }
        
    }
    
}