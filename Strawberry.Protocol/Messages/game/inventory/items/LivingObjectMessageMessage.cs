

// Generated on 02/12/2018 03:56:42
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LivingObjectMessageMessage : NetworkMessage
    {
        public const uint ProtocolId = 6065;
        public override uint MessageID => ProtocolId;
        
        public ushort MsgId { get; set; }
        public int TimeStamp { get; set; }
        public string Owner { get; set; }
        public ushort ObjectGenericId { get; set; }
        
        public LivingObjectMessageMessage()
        {
        }
        
        public LivingObjectMessageMessage(ushort msgId, int timeStamp, string owner, ushort objectGenericId)
        {
            this.MsgId = msgId;
            this.TimeStamp = timeStamp;
            this.Owner = owner;
            this.ObjectGenericId = objectGenericId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(MsgId);
            writer.WriteInt(TimeStamp);
            writer.WriteUTF(Owner);
            writer.WriteVarShort(ObjectGenericId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MsgId = reader.ReadVarUhShort();
            if (MsgId < 0)
                throw new Exception("Forbidden value on MsgId = " + MsgId + ", it doesn't respect the following condition : msgId < 0");
            TimeStamp = reader.ReadInt();
            if (TimeStamp < 0)
                throw new Exception("Forbidden value on TimeStamp = " + TimeStamp + ", it doesn't respect the following condition : timeStamp < 0");
            Owner = reader.ReadUTF();
            ObjectGenericId = reader.ReadVarUhShort();
            if (ObjectGenericId < 0)
                throw new Exception("Forbidden value on ObjectGenericId = " + ObjectGenericId + ", it doesn't respect the following condition : objectGenericId < 0");
        }
        
    }
    
}