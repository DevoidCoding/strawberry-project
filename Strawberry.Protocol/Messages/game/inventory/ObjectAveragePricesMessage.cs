

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ObjectAveragePricesMessage : NetworkMessage
    {
        public const uint ProtocolId = 6335;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Ids { get; set; }
        public ulong[] AvgPrices { get; set; }
        
        public ObjectAveragePricesMessage()
        {
        }
        
        public ObjectAveragePricesMessage(ushort[] ids, ulong[] avgPrices)
        {
            this.Ids = ids;
            this.AvgPrices = avgPrices;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Ids.Length);
            foreach (var entry in Ids)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)AvgPrices.Length);
            foreach (var entry in AvgPrices)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Ids = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ids[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            AvgPrices = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 AvgPrices[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}