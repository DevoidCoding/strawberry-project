

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectTransfertListToInvMessage : NetworkMessage
    {
        public const uint ProtocolId = 6039;
        public override uint MessageID => ProtocolId;
        
        public uint[] Ids { get; set; }
        
        public ExchangeObjectTransfertListToInvMessage()
        {
        }
        
        public ExchangeObjectTransfertListToInvMessage(uint[] ids)
        {
            this.Ids = ids;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Ids.Length);
            foreach (var entry in Ids)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Ids = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ids[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}