

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartedBidBuyerMessage : NetworkMessage
    {
        public const uint ProtocolId = 5904;
        public override uint MessageID => ProtocolId;
        
        public Types.SellerBuyerDescriptor BuyerDescriptor { get; set; }
        
        public ExchangeStartedBidBuyerMessage()
        {
        }
        
        public ExchangeStartedBidBuyerMessage(Types.SellerBuyerDescriptor buyerDescriptor)
        {
            this.BuyerDescriptor = buyerDescriptor;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            BuyerDescriptor.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BuyerDescriptor = new Types.SellerBuyerDescriptor();
            BuyerDescriptor.Deserialize(reader);
        }
        
    }
    
}