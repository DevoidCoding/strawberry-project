

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseBuyResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6272;
        public override uint MessageID => ProtocolId;
        
        public uint Uid { get; set; }
        public bool Bought { get; set; }
        
        public ExchangeBidHouseBuyResultMessage()
        {
        }
        
        public ExchangeBidHouseBuyResultMessage(uint uid, bool bought)
        {
            this.Uid = uid;
            this.Bought = bought;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteBoolean(Bought);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Uid = reader.ReadVarUhInt();
            if (Uid < 0)
                throw new Exception("Forbidden value on Uid = " + Uid + ", it doesn't respect the following condition : uid < 0");
            Bought = reader.ReadBoolean();
        }
        
    }
    
}