

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseUnsoldItemsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6612;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemGenericQuantity[] Items { get; set; }
        
        public ExchangeBidHouseUnsoldItemsMessage()
        {
        }
        
        public ExchangeBidHouseUnsoldItemsMessage(Types.ObjectItemGenericQuantity[] items)
        {
            this.Items = items;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Items.Length);
            foreach (var entry in Items)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Items = new Types.ObjectItemGenericQuantity[limit];
            for (int i = 0; i < limit; i++)
            {
                 Items[i] = new Types.ObjectItemGenericQuantity();
                 Items[i].Deserialize(reader);
            }
        }
        
    }
    
}