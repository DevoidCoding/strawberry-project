

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 5513;
        public override uint MessageID => ProtocolId;
        
        public sbyte ErrorType { get; set; }
        
        public ExchangeErrorMessage()
        {
        }
        
        public ExchangeErrorMessage(sbyte errorType)
        {
            this.ErrorType = errorType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ErrorType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ErrorType = reader.ReadSByte();
        }
        
    }
    
}