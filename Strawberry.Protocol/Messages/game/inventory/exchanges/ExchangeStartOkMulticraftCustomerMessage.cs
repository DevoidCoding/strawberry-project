

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkMulticraftCustomerMessage : NetworkMessage
    {
        public const uint ProtocolId = 5817;
        public override uint MessageID => ProtocolId;
        
        public uint SkillId { get; set; }
        public byte CrafterJobLevel { get; set; }
        
        public ExchangeStartOkMulticraftCustomerMessage()
        {
        }
        
        public ExchangeStartOkMulticraftCustomerMessage(uint skillId, byte crafterJobLevel)
        {
            this.SkillId = skillId;
            this.CrafterJobLevel = crafterJobLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(SkillId);
            writer.WriteByte(CrafterJobLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SkillId = reader.ReadVarUhInt();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
            CrafterJobLevel = reader.ReadByte();
            if (CrafterJobLevel < 0 || CrafterJobLevel > 255)
                throw new Exception("Forbidden value on CrafterJobLevel = " + CrafterJobLevel + ", it doesn't respect the following condition : crafterJobLevel < 0 || crafterJobLevel > 255");
        }
        
    }
    
}