

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeShopStockStartedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5910;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemToSell[] ObjectsInfos { get; set; }
        
        public ExchangeShopStockStartedMessage()
        {
        }
        
        public ExchangeShopStockStartedMessage(Types.ObjectItemToSell[] objectsInfos)
        {
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItemToSell[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItemToSell();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}