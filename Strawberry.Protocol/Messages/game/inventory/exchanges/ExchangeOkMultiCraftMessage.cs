

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeOkMultiCraftMessage : NetworkMessage
    {
        public const uint ProtocolId = 5768;
        public override uint MessageID => ProtocolId;
        
        public ulong InitiatorId { get; set; }
        public ulong OtherId { get; set; }
        public sbyte Role { get; set; }
        
        public ExchangeOkMultiCraftMessage()
        {
        }
        
        public ExchangeOkMultiCraftMessage(ulong initiatorId, ulong otherId, sbyte role)
        {
            this.InitiatorId = initiatorId;
            this.OtherId = otherId;
            this.Role = role;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(InitiatorId);
            writer.WriteVarLong(OtherId);
            writer.WriteSByte(Role);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            InitiatorId = reader.ReadVarUhLong();
            if (InitiatorId < 0 || InitiatorId > 9007199254740990)
                throw new Exception("Forbidden value on InitiatorId = " + InitiatorId + ", it doesn't respect the following condition : initiatorId < 0 || initiatorId > 9007199254740990");
            OtherId = reader.ReadVarUhLong();
            if (OtherId < 0 || OtherId > 9007199254740990)
                throw new Exception("Forbidden value on OtherId = " + OtherId + ", it doesn't respect the following condition : otherId < 0 || otherId > 9007199254740990");
            Role = reader.ReadSByte();
        }
        
    }
    
}