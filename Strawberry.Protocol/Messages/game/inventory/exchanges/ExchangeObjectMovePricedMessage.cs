

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectMovePricedMessage : ExchangeObjectMoveMessage
    {
        public new const uint ProtocolId = 5514;
        public override uint MessageID => ProtocolId;
        
        public ulong Price { get; set; }
        
        public ExchangeObjectMovePricedMessage()
        {
        }
        
        public ExchangeObjectMovePricedMessage(uint objectUID, int quantity, ulong price)
         : base(objectUID, quantity)
        {
            this.Price = price;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Price);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}