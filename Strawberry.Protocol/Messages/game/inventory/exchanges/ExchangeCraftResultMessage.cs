

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCraftResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5790;
        public override uint MessageID => ProtocolId;
        
        public sbyte CraftResult { get; set; }
        
        public ExchangeCraftResultMessage()
        {
        }
        
        public ExchangeCraftResultMessage(sbyte craftResult)
        {
            this.CraftResult = craftResult;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(CraftResult);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CraftResult = reader.ReadSByte();
            if (CraftResult < 0)
                throw new Exception("Forbidden value on CraftResult = " + CraftResult + ", it doesn't respect the following condition : craftResult < 0");
        }
        
    }
    
}