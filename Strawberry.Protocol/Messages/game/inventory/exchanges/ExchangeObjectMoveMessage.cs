

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectMoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 5518;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectUID { get; set; }
        public int Quantity { get; set; }
        
        public ExchangeObjectMoveMessage()
        {
        }
        
        public ExchangeObjectMoveMessage(uint objectUID, int quantity)
        {
            this.ObjectUID = objectUID;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectUID);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectUID = reader.ReadVarUhInt();
            if (ObjectUID < 0)
                throw new Exception("Forbidden value on ObjectUID = " + ObjectUID + ", it doesn't respect the following condition : objectUID < 0");
            Quantity = reader.ReadVarInt();
        }
        
    }
    
}