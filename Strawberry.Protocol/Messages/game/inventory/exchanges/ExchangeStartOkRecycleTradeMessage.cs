

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkRecycleTradeMessage : NetworkMessage
    {
        public const uint ProtocolId = 6600;
        public override uint MessageID => ProtocolId;
        
        public short PercentToPrism { get; set; }
        public short PercentToPlayer { get; set; }
        
        public ExchangeStartOkRecycleTradeMessage()
        {
        }
        
        public ExchangeStartOkRecycleTradeMessage(short percentToPrism, short percentToPlayer)
        {
            this.PercentToPrism = percentToPrism;
            this.PercentToPlayer = percentToPlayer;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(PercentToPrism);
            writer.WriteShort(PercentToPlayer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PercentToPrism = reader.ReadShort();
            if (PercentToPrism < 0)
                throw new Exception("Forbidden value on PercentToPrism = " + PercentToPrism + ", it doesn't respect the following condition : percentToPrism < 0");
            PercentToPlayer = reader.ReadShort();
            if (PercentToPlayer < 0)
                throw new Exception("Forbidden value on PercentToPlayer = " + PercentToPlayer + ", it doesn't respect the following condition : percentToPlayer < 0");
        }
        
    }
    
}