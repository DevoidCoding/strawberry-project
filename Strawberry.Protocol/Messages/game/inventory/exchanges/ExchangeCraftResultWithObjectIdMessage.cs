

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCraftResultWithObjectIdMessage : ExchangeCraftResultMessage
    {
        public new const uint ProtocolId = 6000;
        public override uint MessageID => ProtocolId;
        
        public ushort ObjectGenericId { get; set; }
        
        public ExchangeCraftResultWithObjectIdMessage()
        {
        }
        
        public ExchangeCraftResultWithObjectIdMessage(sbyte craftResult, ushort objectGenericId)
         : base(craftResult)
        {
            this.ObjectGenericId = objectGenericId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ObjectGenericId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ObjectGenericId = reader.ReadVarUhShort();
            if (ObjectGenericId < 0)
                throw new Exception("Forbidden value on ObjectGenericId = " + ObjectGenericId + ", it doesn't respect the following condition : objectGenericId < 0");
        }
        
    }
    
}