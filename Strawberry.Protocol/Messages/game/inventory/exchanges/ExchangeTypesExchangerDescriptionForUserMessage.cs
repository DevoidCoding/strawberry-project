

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeTypesExchangerDescriptionForUserMessage : NetworkMessage
    {
        public const uint ProtocolId = 5765;
        public override uint MessageID => ProtocolId;
        
        public uint[] TypeDescription { get; set; }
        
        public ExchangeTypesExchangerDescriptionForUserMessage()
        {
        }
        
        public ExchangeTypesExchangerDescriptionForUserMessage(uint[] typeDescription)
        {
            this.TypeDescription = typeDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)TypeDescription.Length);
            foreach (var entry in TypeDescription)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            TypeDescription = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 TypeDescription[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}