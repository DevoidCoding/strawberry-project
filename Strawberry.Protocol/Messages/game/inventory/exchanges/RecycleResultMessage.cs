

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class RecycleResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6601;
        public override uint MessageID => ProtocolId;
        
        public uint NuggetsForPrism { get; set; }
        public uint NuggetsForPlayer { get; set; }
        
        public RecycleResultMessage()
        {
        }
        
        public RecycleResultMessage(uint nuggetsForPrism, uint nuggetsForPlayer)
        {
            this.NuggetsForPrism = nuggetsForPrism;
            this.NuggetsForPlayer = nuggetsForPlayer;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(NuggetsForPrism);
            writer.WriteVarInt(NuggetsForPlayer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NuggetsForPrism = reader.ReadVarUhInt();
            if (NuggetsForPrism < 0)
                throw new Exception("Forbidden value on NuggetsForPrism = " + NuggetsForPrism + ", it doesn't respect the following condition : nuggetsForPrism < 0");
            NuggetsForPlayer = reader.ReadVarUhInt();
            if (NuggetsForPlayer < 0)
                throw new Exception("Forbidden value on NuggetsForPlayer = " + NuggetsForPlayer + ", it doesn't respect the following condition : nuggetsForPlayer < 0");
        }
        
    }
    
}