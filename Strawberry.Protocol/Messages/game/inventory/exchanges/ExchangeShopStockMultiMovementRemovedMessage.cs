

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeShopStockMultiMovementRemovedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6037;
        public override uint MessageID => ProtocolId;
        
        public uint[] ObjectIdList { get; set; }
        
        public ExchangeShopStockMultiMovementRemovedMessage()
        {
        }
        
        public ExchangeShopStockMultiMovementRemovedMessage(uint[] objectIdList)
        {
            this.ObjectIdList = objectIdList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectIdList.Length);
            foreach (var entry in ObjectIdList)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectIdList = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectIdList[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}