

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeMountsPaddockAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 6561;
        public override uint MessageID => ProtocolId;
        
        public Types.MountClientData[] MountDescription { get; set; }
        
        public ExchangeMountsPaddockAddMessage()
        {
        }
        
        public ExchangeMountsPaddockAddMessage(Types.MountClientData[] mountDescription)
        {
            this.MountDescription = mountDescription;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)MountDescription.Length);
            foreach (var entry in MountDescription)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            MountDescription = new Types.MountClientData[limit];
            for (int i = 0; i < limit; i++)
            {
                 MountDescription[i] = new Types.MountClientData();
                 MountDescription[i].Deserialize(reader);
            }
        }
        
    }
    
}