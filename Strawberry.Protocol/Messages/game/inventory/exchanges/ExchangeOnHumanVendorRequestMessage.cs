

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeOnHumanVendorRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5772;
        public override uint MessageID => ProtocolId;
        
        public ulong HumanVendorId { get; set; }
        public ushort HumanVendorCell { get; set; }
        
        public ExchangeOnHumanVendorRequestMessage()
        {
        }
        
        public ExchangeOnHumanVendorRequestMessage(ulong humanVendorId, ushort humanVendorCell)
        {
            this.HumanVendorId = humanVendorId;
            this.HumanVendorCell = humanVendorCell;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(HumanVendorId);
            writer.WriteVarShort(HumanVendorCell);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HumanVendorId = reader.ReadVarUhLong();
            if (HumanVendorId < 0 || HumanVendorId > 9007199254740990)
                throw new Exception("Forbidden value on HumanVendorId = " + HumanVendorId + ", it doesn't respect the following condition : humanVendorId < 0 || humanVendorId > 9007199254740990");
            HumanVendorCell = reader.ReadVarUhShort();
            if (HumanVendorCell < 0 || HumanVendorCell > 559)
                throw new Exception("Forbidden value on HumanVendorCell = " + HumanVendorCell + ", it doesn't respect the following condition : humanVendorCell < 0 || humanVendorCell > 559");
        }
        
    }
    
}