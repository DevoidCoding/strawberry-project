

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FocusedExchangeReadyMessage : ExchangeReadyMessage
    {
        public new const uint ProtocolId = 6701;
        public override uint MessageID => ProtocolId;
        
        public uint FocusActionId { get; set; }
        
        public FocusedExchangeReadyMessage()
        {
        }
        
        public FocusedExchangeReadyMessage(bool ready, ushort step, uint focusActionId)
         : base(ready, step)
        {
            this.FocusActionId = focusActionId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(FocusActionId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FocusActionId = reader.ReadVarUhInt();
            if (FocusActionId < 0)
                throw new Exception("Forbidden value on FocusActionId = " + FocusActionId + ", it doesn't respect the following condition : focusActionId < 0");
        }
        
    }
    
}