

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartedMountStockMessage : NetworkMessage
    {
        public const uint ProtocolId = 5984;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] ObjectsInfos { get; set; }
        
        public ExchangeStartedMountStockMessage()
        {
        }
        
        public ExchangeStartedMountStockMessage(Types.ObjectItem[] objectsInfos)
        {
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItem();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}