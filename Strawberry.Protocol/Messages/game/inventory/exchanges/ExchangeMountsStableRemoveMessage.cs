

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeMountsStableRemoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 6556;
        public override uint MessageID => ProtocolId;
        
        public int[] MountsId { get; set; }
        
        public ExchangeMountsStableRemoveMessage()
        {
        }
        
        public ExchangeMountsStableRemoveMessage(int[] mountsId)
        {
            this.MountsId = mountsId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)MountsId.Length);
            foreach (var entry in MountsId)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            MountsId = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 MountsId[i] = reader.ReadVarInt();
            }
        }
        
    }
    
}