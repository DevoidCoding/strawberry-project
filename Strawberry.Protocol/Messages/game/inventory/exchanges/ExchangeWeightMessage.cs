

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeWeightMessage : NetworkMessage
    {
        public const uint ProtocolId = 5793;
        public override uint MessageID => ProtocolId;
        
        public uint CurrentWeight { get; set; }
        public uint MaxWeight { get; set; }
        
        public ExchangeWeightMessage()
        {
        }
        
        public ExchangeWeightMessage(uint currentWeight, uint maxWeight)
        {
            this.CurrentWeight = currentWeight;
            this.MaxWeight = maxWeight;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(CurrentWeight);
            writer.WriteVarInt(MaxWeight);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CurrentWeight = reader.ReadVarUhInt();
            if (CurrentWeight < 0)
                throw new Exception("Forbidden value on CurrentWeight = " + CurrentWeight + ", it doesn't respect the following condition : currentWeight < 0");
            MaxWeight = reader.ReadVarUhInt();
            if (MaxWeight < 0)
                throw new Exception("Forbidden value on MaxWeight = " + MaxWeight + ", it doesn't respect the following condition : maxWeight < 0");
        }
        
    }
    
}