

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkNpcTradeMessage : NetworkMessage
    {
        public const uint ProtocolId = 5785;
        public override uint MessageID => ProtocolId;
        
        public double NpcId { get; set; }
        
        public ExchangeStartOkNpcTradeMessage()
        {
        }
        
        public ExchangeStartOkNpcTradeMessage(double npcId)
        {
            this.NpcId = npcId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(NpcId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NpcId = reader.ReadDouble();
            if (NpcId < -9007199254740990 || NpcId > 9007199254740990)
                throw new Exception("Forbidden value on NpcId = " + NpcId + ", it doesn't respect the following condition : npcId < -9007199254740990 || npcId > 9007199254740990");
        }
        
    }
    
}