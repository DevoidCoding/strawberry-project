

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkHumanVendorMessage : NetworkMessage
    {
        public const uint ProtocolId = 5767;
        public override uint MessageID => ProtocolId;
        
        public double SellerId { get; set; }
        public Types.ObjectItemToSellInHumanVendorShop[] ObjectsInfos { get; set; }
        
        public ExchangeStartOkHumanVendorMessage()
        {
        }
        
        public ExchangeStartOkHumanVendorMessage(double sellerId, Types.ObjectItemToSellInHumanVendorShop[] objectsInfos)
        {
            this.SellerId = sellerId;
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(SellerId);
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SellerId = reader.ReadDouble();
            if (SellerId < -9007199254740990 || SellerId > 9007199254740990)
                throw new Exception("Forbidden value on SellerId = " + SellerId + ", it doesn't respect the following condition : sellerId < -9007199254740990 || sellerId > 9007199254740990");
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItemToSellInHumanVendorShop[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItemToSellInHumanVendorShop();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}