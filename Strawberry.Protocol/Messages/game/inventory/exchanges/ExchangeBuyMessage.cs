

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBuyMessage : NetworkMessage
    {
        public const uint ProtocolId = 5774;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectToBuyId { get; set; }
        public uint Quantity { get; set; }
        
        public ExchangeBuyMessage()
        {
        }
        
        public ExchangeBuyMessage(uint objectToBuyId, uint quantity)
        {
            this.ObjectToBuyId = objectToBuyId;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectToBuyId);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectToBuyId = reader.ReadVarUhInt();
            if (ObjectToBuyId < 0)
                throw new Exception("Forbidden value on ObjectToBuyId = " + ObjectToBuyId + ", it doesn't respect the following condition : objectToBuyId < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}