

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeReadyMessage : NetworkMessage
    {
        public const uint ProtocolId = 5511;
        public override uint MessageID => ProtocolId;
        
        public bool Ready { get; set; }
        public ushort Step { get; set; }
        
        public ExchangeReadyMessage()
        {
        }
        
        public ExchangeReadyMessage(bool ready, ushort step)
        {
            this.Ready = ready;
            this.Step = step;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Ready);
            writer.WriteVarShort(Step);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Ready = reader.ReadBoolean();
            Step = reader.ReadVarUhShort();
            if (Step < 0)
                throw new Exception("Forbidden value on Step = " + Step + ", it doesn't respect the following condition : step < 0");
        }
        
    }
    
}