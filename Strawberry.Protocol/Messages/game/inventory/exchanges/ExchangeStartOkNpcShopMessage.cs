

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkNpcShopMessage : NetworkMessage
    {
        public const uint ProtocolId = 5761;
        public override uint MessageID => ProtocolId;
        
        public double NpcSellerId { get; set; }
        public ushort TokenId { get; set; }
        public Types.ObjectItemToSellInNpcShop[] ObjectsInfos { get; set; }
        
        public ExchangeStartOkNpcShopMessage()
        {
        }
        
        public ExchangeStartOkNpcShopMessage(double npcSellerId, ushort tokenId, Types.ObjectItemToSellInNpcShop[] objectsInfos)
        {
            this.NpcSellerId = npcSellerId;
            this.TokenId = tokenId;
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(NpcSellerId);
            writer.WriteVarShort(TokenId);
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NpcSellerId = reader.ReadDouble();
            if (NpcSellerId < -9007199254740990 || NpcSellerId > 9007199254740990)
                throw new Exception("Forbidden value on NpcSellerId = " + NpcSellerId + ", it doesn't respect the following condition : npcSellerId < -9007199254740990 || npcSellerId > 9007199254740990");
            TokenId = reader.ReadVarUhShort();
            if (TokenId < 0)
                throw new Exception("Forbidden value on TokenId = " + TokenId + ", it doesn't respect the following condition : tokenId < 0");
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItemToSellInNpcShop[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItemToSellInNpcShop();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}