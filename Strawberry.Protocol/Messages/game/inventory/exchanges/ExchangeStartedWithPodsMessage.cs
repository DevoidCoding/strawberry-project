

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartedWithPodsMessage : ExchangeStartedMessage
    {
        public new const uint ProtocolId = 6129;
        public override uint MessageID => ProtocolId;
        
        public double FirstCharacterId { get; set; }
        public uint FirstCharacterCurrentWeight { get; set; }
        public uint FirstCharacterMaxWeight { get; set; }
        public double SecondCharacterId { get; set; }
        public uint SecondCharacterCurrentWeight { get; set; }
        public uint SecondCharacterMaxWeight { get; set; }
        
        public ExchangeStartedWithPodsMessage()
        {
        }
        
        public ExchangeStartedWithPodsMessage(sbyte exchangeType, double firstCharacterId, uint firstCharacterCurrentWeight, uint firstCharacterMaxWeight, double secondCharacterId, uint secondCharacterCurrentWeight, uint secondCharacterMaxWeight)
         : base(exchangeType)
        {
            this.FirstCharacterId = firstCharacterId;
            this.FirstCharacterCurrentWeight = firstCharacterCurrentWeight;
            this.FirstCharacterMaxWeight = firstCharacterMaxWeight;
            this.SecondCharacterId = secondCharacterId;
            this.SecondCharacterCurrentWeight = secondCharacterCurrentWeight;
            this.SecondCharacterMaxWeight = secondCharacterMaxWeight;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(FirstCharacterId);
            writer.WriteVarInt(FirstCharacterCurrentWeight);
            writer.WriteVarInt(FirstCharacterMaxWeight);
            writer.WriteDouble(SecondCharacterId);
            writer.WriteVarInt(SecondCharacterCurrentWeight);
            writer.WriteVarInt(SecondCharacterMaxWeight);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            FirstCharacterId = reader.ReadDouble();
            if (FirstCharacterId < -9007199254740990 || FirstCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on FirstCharacterId = " + FirstCharacterId + ", it doesn't respect the following condition : firstCharacterId < -9007199254740990 || firstCharacterId > 9007199254740990");
            FirstCharacterCurrentWeight = reader.ReadVarUhInt();
            if (FirstCharacterCurrentWeight < 0)
                throw new Exception("Forbidden value on FirstCharacterCurrentWeight = " + FirstCharacterCurrentWeight + ", it doesn't respect the following condition : firstCharacterCurrentWeight < 0");
            FirstCharacterMaxWeight = reader.ReadVarUhInt();
            if (FirstCharacterMaxWeight < 0)
                throw new Exception("Forbidden value on FirstCharacterMaxWeight = " + FirstCharacterMaxWeight + ", it doesn't respect the following condition : firstCharacterMaxWeight < 0");
            SecondCharacterId = reader.ReadDouble();
            if (SecondCharacterId < -9007199254740990 || SecondCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on SecondCharacterId = " + SecondCharacterId + ", it doesn't respect the following condition : secondCharacterId < -9007199254740990 || secondCharacterId > 9007199254740990");
            SecondCharacterCurrentWeight = reader.ReadVarUhInt();
            if (SecondCharacterCurrentWeight < 0)
                throw new Exception("Forbidden value on SecondCharacterCurrentWeight = " + SecondCharacterCurrentWeight + ", it doesn't respect the following condition : secondCharacterCurrentWeight < 0");
            SecondCharacterMaxWeight = reader.ReadVarUhInt();
            if (SecondCharacterMaxWeight < 0)
                throw new Exception("Forbidden value on SecondCharacterMaxWeight = " + SecondCharacterMaxWeight + ", it doesn't respect the following condition : secondCharacterMaxWeight < 0");
        }
        
    }
    
}