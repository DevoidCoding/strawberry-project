

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseItemAddOkMessage : NetworkMessage
    {
        public const uint ProtocolId = 5945;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemToSellInBid ItemInfo { get; set; }
        
        public ExchangeBidHouseItemAddOkMessage()
        {
        }
        
        public ExchangeBidHouseItemAddOkMessage(Types.ObjectItemToSellInBid itemInfo)
        {
            this.ItemInfo = itemInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            ItemInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ItemInfo = new Types.ObjectItemToSellInBid();
            ItemInfo.Deserialize(reader);
        }
        
    }
    
}