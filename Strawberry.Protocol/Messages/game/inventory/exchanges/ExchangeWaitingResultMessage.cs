

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeWaitingResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5786;
        public override uint MessageID => ProtocolId;
        
        public bool Bwait { get; set; }
        
        public ExchangeWaitingResultMessage()
        {
        }
        
        public ExchangeWaitingResultMessage(bool bwait)
        {
            this.Bwait = bwait;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Bwait);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Bwait = reader.ReadBoolean();
        }
        
    }
    
}