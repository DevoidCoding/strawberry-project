

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseInListAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5949;
        public override uint MessageID => ProtocolId;
        
        public int ItemUID { get; set; }
        public int ObjGenericId { get; set; }
        public Types.ObjectEffect[] Effects { get; set; }
        public ulong[] Prices { get; set; }
        
        public ExchangeBidHouseInListAddedMessage()
        {
        }
        
        public ExchangeBidHouseInListAddedMessage(int itemUID, int objGenericId, Types.ObjectEffect[] effects, ulong[] prices)
        {
            this.ItemUID = itemUID;
            this.ObjGenericId = objGenericId;
            this.Effects = effects;
            this.Prices = prices;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(ItemUID);
            writer.WriteInt(ObjGenericId);
            writer.WriteUShort((ushort)Effects.Length);
            foreach (var entry in Effects)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)Prices.Length);
            foreach (var entry in Prices)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ItemUID = reader.ReadInt();
            ObjGenericId = reader.ReadInt();
            var limit = reader.ReadUShort();
            Effects = new Types.ObjectEffect[limit];
            for (int i = 0; i < limit; i++)
            {
                 Effects[i] = Types.ProtocolTypeManager.GetInstance<Types.ObjectEffect>(reader.ReadShort());
                 Effects[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            Prices = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 Prices[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}