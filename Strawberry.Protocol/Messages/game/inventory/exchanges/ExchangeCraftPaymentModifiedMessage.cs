

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCraftPaymentModifiedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6578;
        public override uint MessageID => ProtocolId;
        
        public ulong GoldSum { get; set; }
        
        public ExchangeCraftPaymentModifiedMessage()
        {
        }
        
        public ExchangeCraftPaymentModifiedMessage(ulong goldSum)
        {
            this.GoldSum = goldSum;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(GoldSum);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GoldSum = reader.ReadVarUhLong();
            if (GoldSum < 0 || GoldSum > 9007199254740990)
                throw new Exception("Forbidden value on GoldSum = " + GoldSum + ", it doesn't respect the following condition : goldSum < 0 || goldSum > 9007199254740990");
        }
        
    }
    
}