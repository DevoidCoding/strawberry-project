

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeReplyTaxVendorMessage : NetworkMessage
    {
        public const uint ProtocolId = 5787;
        public override uint MessageID => ProtocolId;
        
        public ulong ObjectValue { get; set; }
        public ulong TotalTaxValue { get; set; }
        
        public ExchangeReplyTaxVendorMessage()
        {
        }
        
        public ExchangeReplyTaxVendorMessage(ulong objectValue, ulong totalTaxValue)
        {
            this.ObjectValue = objectValue;
            this.TotalTaxValue = totalTaxValue;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(ObjectValue);
            writer.WriteVarLong(TotalTaxValue);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectValue = reader.ReadVarUhLong();
            if (ObjectValue < 0 || ObjectValue > 9007199254740990)
                throw new Exception("Forbidden value on ObjectValue = " + ObjectValue + ", it doesn't respect the following condition : objectValue < 0 || objectValue > 9007199254740990");
            TotalTaxValue = reader.ReadVarUhLong();
            if (TotalTaxValue < 0 || TotalTaxValue > 9007199254740990)
                throw new Exception("Forbidden value on TotalTaxValue = " + TotalTaxValue + ", it doesn't respect the following condition : totalTaxValue < 0 || totalTaxValue > 9007199254740990");
        }
        
    }
    
}