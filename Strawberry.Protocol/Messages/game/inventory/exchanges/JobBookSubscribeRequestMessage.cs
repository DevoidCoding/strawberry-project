

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class JobBookSubscribeRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6592;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] JobIds { get; set; }
        
        public JobBookSubscribeRequestMessage()
        {
        }
        
        public JobBookSubscribeRequestMessage(sbyte[] jobIds)
        {
            this.JobIds = jobIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)JobIds.Length);
            foreach (var entry in JobIds)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            JobIds = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 JobIds[i] = reader.ReadSByte();
            }
        }
        
    }
    
}