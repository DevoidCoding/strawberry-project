

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidPriceMessage : NetworkMessage
    {
        public const uint ProtocolId = 5755;
        public override uint MessageID => ProtocolId;
        
        public ushort GenericId { get; set; }
        public long AveragePrice { get; set; }
        
        public ExchangeBidPriceMessage()
        {
        }
        
        public ExchangeBidPriceMessage(ushort genericId, long averagePrice)
        {
            this.GenericId = genericId;
            this.AveragePrice = averagePrice;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(GenericId);
            writer.WriteVarLong(AveragePrice);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GenericId = reader.ReadVarUhShort();
            if (GenericId < 0)
                throw new Exception("Forbidden value on GenericId = " + GenericId + ", it doesn't respect the following condition : genericId < 0");
            AveragePrice = reader.ReadVarLong();
            if (AveragePrice < -9007199254740990 || AveragePrice > 9007199254740990)
                throw new Exception("Forbidden value on AveragePrice = " + AveragePrice + ", it doesn't respect the following condition : averagePrice < -9007199254740990 || averagePrice > 9007199254740990");
        }
        
    }
    
}