

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectMoveKamaMessage : NetworkMessage
    {
        public const uint ProtocolId = 5520;
        public override uint MessageID => ProtocolId;
        
        public long Quantity { get; set; }
        
        public ExchangeObjectMoveKamaMessage()
        {
        }
        
        public ExchangeObjectMoveKamaMessage(long quantity)
        {
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Quantity = reader.ReadVarLong();
            if (Quantity < -9007199254740990 || Quantity > 9007199254740990)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < -9007199254740990 || quantity > 9007199254740990");
        }
        
    }
    
}