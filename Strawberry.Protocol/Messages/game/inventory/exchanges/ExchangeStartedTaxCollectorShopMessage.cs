

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartedTaxCollectorShopMessage : NetworkMessage
    {
        public const uint ProtocolId = 6664;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] Objects { get; set; }
        public ulong Kamas { get; set; }
        
        public ExchangeStartedTaxCollectorShopMessage()
        {
        }
        
        public ExchangeStartedTaxCollectorShopMessage(Types.ObjectItem[] objects, ulong kamas)
        {
            this.Objects = objects;
            this.Kamas = kamas;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Objects.Length);
            foreach (var entry in Objects)
            {
                 entry.Serialize(writer);
            }
            writer.WriteVarLong(Kamas);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Objects = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Objects[i] = new Types.ObjectItem();
                 Objects[i].Deserialize(reader);
            }
            Kamas = reader.ReadVarUhLong();
            if (Kamas < 0 || Kamas > 9007199254740990)
                throw new Exception("Forbidden value on Kamas = " + Kamas + ", it doesn't respect the following condition : kamas < 0 || kamas > 9007199254740990");
        }
        
    }
    
}