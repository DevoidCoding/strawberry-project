

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class ExchangePlayerRequestMessage : ExchangeRequestMessage
    {
        public new const uint ProtocolId = 5773;
        public override uint MessageID => ProtocolId;
        
        public ulong Target { get; set; }
        
        public ExchangePlayerRequestMessage()
        {
        }
        
        public ExchangePlayerRequestMessage(sbyte exchangeType, ulong target)
         : base(exchangeType)
        {
            this.Target = target;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Target);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Target = reader.ReadVarUhLong();
            if (Target < 0 || Target > 9007199254740990)
                throw new Exception("Forbidden value on Target = " + Target + ", it doesn't respect the following condition : target < 0 || target > 9007199254740990");
        }
        
    }
    
}