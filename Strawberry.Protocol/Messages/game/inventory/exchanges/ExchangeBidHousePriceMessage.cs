

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHousePriceMessage : NetworkMessage
    {
        public const uint ProtocolId = 5805;
        public override uint MessageID => ProtocolId;
        
        public ushort GenId { get; set; }
        
        public ExchangeBidHousePriceMessage()
        {
        }
        
        public ExchangeBidHousePriceMessage(ushort genId)
        {
            this.GenId = genId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(GenId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GenId = reader.ReadVarUhShort();
            if (GenId < 0)
                throw new Exception("Forbidden value on GenId = " + GenId + ", it doesn't respect the following condition : genId < 0");
        }
        
    }
    
}