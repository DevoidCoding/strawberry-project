

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeSetCraftRecipeMessage : NetworkMessage
    {
        public const uint ProtocolId = 6389;
        public override uint MessageID => ProtocolId;
        
        public ushort ObjectGID { get; set; }
        
        public ExchangeSetCraftRecipeMessage()
        {
        }
        
        public ExchangeSetCraftRecipeMessage(ushort objectGID)
        {
            this.ObjectGID = objectGID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ObjectGID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectGID = reader.ReadVarUhShort();
            if (ObjectGID < 0)
                throw new Exception("Forbidden value on ObjectGID = " + ObjectGID + ", it doesn't respect the following condition : objectGID < 0");
        }
        
    }
    
}