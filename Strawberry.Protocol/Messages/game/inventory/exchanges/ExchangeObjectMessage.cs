

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectMessage : NetworkMessage
    {
        public const uint ProtocolId = 5515;
        public override uint MessageID => ProtocolId;
        
        public bool Remote { get; set; }
        
        public ExchangeObjectMessage()
        {
        }
        
        public ExchangeObjectMessage(bool remote)
        {
            this.Remote = remote;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Remote);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Remote = reader.ReadBoolean();
        }
        
    }
    
}