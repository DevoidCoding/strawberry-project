

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5807;
        public override uint MessageID => ProtocolId;
        
        public ushort Id { get; set; }
        
        public ExchangeBidHouseListMessage()
        {
        }
        
        public ExchangeBidHouseListMessage(ushort id)
        {
            this.Id = id;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Id);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhShort();
            if (Id < 0)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0");
        }
        
    }
    
}