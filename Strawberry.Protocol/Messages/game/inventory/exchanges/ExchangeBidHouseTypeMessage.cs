

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseTypeMessage : NetworkMessage
    {
        public const uint ProtocolId = 5803;
        public override uint MessageID => ProtocolId;
        
        public uint Type { get; set; }
        
        public ExchangeBidHouseTypeMessage()
        {
        }
        
        public ExchangeBidHouseTypeMessage(uint type)
        {
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Type);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Type = reader.ReadVarUhInt();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}