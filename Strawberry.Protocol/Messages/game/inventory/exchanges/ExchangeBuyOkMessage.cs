

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBuyOkMessage : NetworkMessage
    {
        public const uint ProtocolId = 5759;
        public override uint MessageID => ProtocolId;
        
        
        public ExchangeBuyOkMessage()
        {
        }
        
        
        public override void Serialize(IDataWriter writer)
        {
        }
        
        public override void Deserialize(IDataReader reader)
        {
        }
        
    }
    
}