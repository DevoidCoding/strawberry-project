

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidPriceForSellerMessage : ExchangeBidPriceMessage
    {
        public new const uint ProtocolId = 6464;
        public override uint MessageID => ProtocolId;
        
        public bool AllIdentical { get; set; }
        public ulong[] MinimalPrices { get; set; }
        
        public ExchangeBidPriceForSellerMessage()
        {
        }
        
        public ExchangeBidPriceForSellerMessage(ushort genericId, long averagePrice, bool allIdentical, ulong[] minimalPrices)
         : base(genericId, averagePrice)
        {
            this.AllIdentical = allIdentical;
            this.MinimalPrices = minimalPrices;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(AllIdentical);
            writer.WriteUShort((ushort)MinimalPrices.Length);
            foreach (var entry in MinimalPrices)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            AllIdentical = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            MinimalPrices = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 MinimalPrices[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}