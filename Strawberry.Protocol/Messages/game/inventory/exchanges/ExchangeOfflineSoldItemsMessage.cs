

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeOfflineSoldItemsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6613;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemGenericQuantityPrice[] BidHouseItems { get; set; }
        public Types.ObjectItemGenericQuantityPrice[] MerchantItems { get; set; }
        
        public ExchangeOfflineSoldItemsMessage()
        {
        }
        
        public ExchangeOfflineSoldItemsMessage(Types.ObjectItemGenericQuantityPrice[] bidHouseItems, Types.ObjectItemGenericQuantityPrice[] merchantItems)
        {
            this.BidHouseItems = bidHouseItems;
            this.MerchantItems = merchantItems;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)BidHouseItems.Length);
            foreach (var entry in BidHouseItems)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)MerchantItems.Length);
            foreach (var entry in MerchantItems)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            BidHouseItems = new Types.ObjectItemGenericQuantityPrice[limit];
            for (int i = 0; i < limit; i++)
            {
                 BidHouseItems[i] = new Types.ObjectItemGenericQuantityPrice();
                 BidHouseItems[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            MerchantItems = new Types.ObjectItemGenericQuantityPrice[limit];
            for (int i = 0; i < limit; i++)
            {
                 MerchantItems[i] = new Types.ObjectItemGenericQuantityPrice();
                 MerchantItems[i].Deserialize(reader);
            }
        }
        
    }
    
}