

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeShopStockMovementUpdatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5909;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItemToSell ObjectInfo { get; set; }
        
        public ExchangeShopStockMovementUpdatedMessage()
        {
        }
        
        public ExchangeShopStockMovementUpdatedMessage(Types.ObjectItemToSell objectInfo)
        {
            this.ObjectInfo = objectInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            ObjectInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectInfo = new Types.ObjectItemToSell();
            ObjectInfo.Deserialize(reader);
        }
        
    }
    
}