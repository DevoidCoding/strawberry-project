

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCraftCountRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6597;
        public override uint MessageID => ProtocolId;
        
        public int Count { get; set; }
        
        public ExchangeCraftCountRequestMessage()
        {
        }
        
        public ExchangeCraftCountRequestMessage(int count)
        {
            this.Count = count;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Count);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Count = reader.ReadVarInt();
        }
        
    }
    
}