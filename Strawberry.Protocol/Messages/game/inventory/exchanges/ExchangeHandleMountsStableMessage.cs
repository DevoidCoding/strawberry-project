

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeHandleMountsStableMessage : NetworkMessage
    {
        public const uint ProtocolId = 6562;
        public override uint MessageID => ProtocolId;
        
        public sbyte ActionType { get; set; }
        public uint[] RidesId { get; set; }
        
        public ExchangeHandleMountsStableMessage()
        {
        }
        
        public ExchangeHandleMountsStableMessage(sbyte actionType, uint[] ridesId)
        {
            this.ActionType = actionType;
            this.RidesId = ridesId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ActionType);
            writer.WriteUShort((ushort)RidesId.Length);
            foreach (var entry in RidesId)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ActionType = reader.ReadSByte();
            var limit = reader.ReadUShort();
            RidesId = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 RidesId[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}