

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeRequestedTradeMessage : ExchangeRequestedMessage
    {
        public new const uint ProtocolId = 5523;
        public override uint MessageID => ProtocolId;
        
        public ulong Source { get; set; }
        public ulong Target { get; set; }
        
        public ExchangeRequestedTradeMessage()
        {
        }
        
        public ExchangeRequestedTradeMessage(sbyte exchangeType, ulong source, ulong target)
         : base(exchangeType)
        {
            this.Source = source;
            this.Target = target;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Source);
            writer.WriteVarLong(Target);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Source = reader.ReadVarUhLong();
            if (Source < 0 || Source > 9007199254740990)
                throw new Exception("Forbidden value on Source = " + Source + ", it doesn't respect the following condition : source < 0 || source > 9007199254740990");
            Target = reader.ReadVarUhLong();
            if (Target < 0 || Target > 9007199254740990)
                throw new Exception("Forbidden value on Target = " + Target + ", it doesn't respect the following condition : target < 0 || target > 9007199254740990");
        }
        
    }
    
}