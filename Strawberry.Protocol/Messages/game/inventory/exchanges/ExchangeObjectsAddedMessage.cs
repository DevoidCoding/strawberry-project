

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectsAddedMessage : ExchangeObjectMessage
    {
        public new const uint ProtocolId = 6535;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] Object { get; set; }
        
        public ExchangeObjectsAddedMessage()
        {
        }
        
        public ExchangeObjectsAddedMessage(bool remote, Types.ObjectItem[] @object)
         : base(remote)
        {
            this.Object = @object;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Object.Length);
            foreach (var entry in Object)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Object = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Object[i] = new Types.ObjectItem();
                 Object[i].Deserialize(reader);
            }
        }
        
    }
    
}