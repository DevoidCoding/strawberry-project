

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCrafterJobLevelupMessage : NetworkMessage
    {
        public const uint ProtocolId = 6598;
        public override uint MessageID => ProtocolId;
        
        public byte CrafterJobLevel { get; set; }
        
        public ExchangeCrafterJobLevelupMessage()
        {
        }
        
        public ExchangeCrafterJobLevelupMessage(byte crafterJobLevel)
        {
            this.CrafterJobLevel = crafterJobLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(CrafterJobLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CrafterJobLevel = reader.ReadByte();
            if (CrafterJobLevel < 0 || CrafterJobLevel > 255)
                throw new Exception("Forbidden value on CrafterJobLevel = " + CrafterJobLevel + ", it doesn't respect the following condition : crafterJobLevel < 0 || crafterJobLevel > 255");
        }
        
    }
    
}