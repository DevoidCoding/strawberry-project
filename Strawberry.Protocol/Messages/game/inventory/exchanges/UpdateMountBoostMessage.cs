

// Generated on 02/12/2018 03:56:41
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class UpdateMountBoostMessage : NetworkMessage
    {
        public const uint ProtocolId = 6179;
        public override uint MessageID => ProtocolId;
        
        public int RideId { get; set; }
        public Types.UpdateMountBoost[] BoostToUpdateList { get; set; }
        
        public UpdateMountBoostMessage()
        {
        }
        
        public UpdateMountBoostMessage(int rideId, Types.UpdateMountBoost[] boostToUpdateList)
        {
            this.RideId = rideId;
            this.BoostToUpdateList = boostToUpdateList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(RideId);
            writer.WriteUShort((ushort)BoostToUpdateList.Length);
            foreach (var entry in BoostToUpdateList)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RideId = reader.ReadVarInt();
            var limit = reader.ReadUShort();
            BoostToUpdateList = new Types.UpdateMountBoost[limit];
            for (int i = 0; i < limit; i++)
            {
                 BoostToUpdateList[i] = Types.ProtocolTypeManager.GetInstance<Types.UpdateMountBoost>(reader.ReadShort());
                 BoostToUpdateList[i].Deserialize(reader);
            }
        }
        
    }
    
}