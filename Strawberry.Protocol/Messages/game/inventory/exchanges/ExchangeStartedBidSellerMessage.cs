

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartedBidSellerMessage : NetworkMessage
    {
        public const uint ProtocolId = 5905;
        public override uint MessageID => ProtocolId;
        
        public Types.SellerBuyerDescriptor SellerDescriptor { get; set; }
        public Types.ObjectItemToSellInBid[] ObjectsInfos { get; set; }
        
        public ExchangeStartedBidSellerMessage()
        {
        }
        
        public ExchangeStartedBidSellerMessage(Types.SellerBuyerDescriptor sellerDescriptor, Types.ObjectItemToSellInBid[] objectsInfos)
        {
            this.SellerDescriptor = sellerDescriptor;
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            SellerDescriptor.Serialize(writer);
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SellerDescriptor = new Types.SellerBuyerDescriptor();
            SellerDescriptor.Deserialize(reader);
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItemToSellInBid[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItemToSellInBid();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}