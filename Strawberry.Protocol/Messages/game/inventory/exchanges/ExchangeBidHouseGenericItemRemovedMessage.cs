

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseGenericItemRemovedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5948;
        public override uint MessageID => ProtocolId;
        
        public ushort ObjGenericId { get; set; }
        
        public ExchangeBidHouseGenericItemRemovedMessage()
        {
        }
        
        public ExchangeBidHouseGenericItemRemovedMessage(ushort objGenericId)
        {
            this.ObjGenericId = objGenericId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ObjGenericId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjGenericId = reader.ReadVarUhShort();
            if (ObjGenericId < 0)
                throw new Exception("Forbidden value on ObjGenericId = " + ObjGenericId + ", it doesn't respect the following condition : objGenericId < 0");
        }
        
    }
    
}