

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class ExchangePlayerMultiCraftRequestMessage : ExchangeRequestMessage
    {
        public new const uint ProtocolId = 5784;
        public override uint MessageID => ProtocolId;
        
        public ulong Target { get; set; }
        public uint SkillId { get; set; }
        
        public ExchangePlayerMultiCraftRequestMessage()
        {
        }
        
        public ExchangePlayerMultiCraftRequestMessage(sbyte exchangeType, ulong target, uint skillId)
         : base(exchangeType)
        {
            this.Target = target;
            this.SkillId = skillId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(Target);
            writer.WriteVarInt(SkillId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Target = reader.ReadVarUhLong();
            if (Target < 0 || Target > 9007199254740990)
                throw new Exception("Forbidden value on Target = " + Target + ", it doesn't respect the following condition : target < 0 || target > 9007199254740990");
            SkillId = reader.ReadVarUhInt();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
        }
        
    }
    
}