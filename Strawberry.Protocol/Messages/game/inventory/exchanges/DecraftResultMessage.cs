

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DecraftResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6569;
        public override uint MessageID => ProtocolId;
        
        public Types.DecraftedItemStackInfo[] Results { get; set; }
        
        public DecraftResultMessage()
        {
        }
        
        public DecraftResultMessage(Types.DecraftedItemStackInfo[] results)
        {
            this.Results = results;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Results.Length);
            foreach (var entry in Results)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Results = new Types.DecraftedItemStackInfo[limit];
            for (int i = 0; i < limit; i++)
            {
                 Results[i] = new Types.DecraftedItemStackInfo();
                 Results[i].Deserialize(reader);
            }
        }
        
    }
    
}