

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeStartOkJobIndexMessage : NetworkMessage
    {
        public const uint ProtocolId = 5819;
        public override uint MessageID => ProtocolId;
        
        public uint[] Jobs { get; set; }
        
        public ExchangeStartOkJobIndexMessage()
        {
        }
        
        public ExchangeStartOkJobIndexMessage(uint[] jobs)
        {
            this.Jobs = jobs;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Jobs.Length);
            foreach (var entry in Jobs)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Jobs = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Jobs[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}