

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeSellMessage : NetworkMessage
    {
        public const uint ProtocolId = 5778;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectToSellId { get; set; }
        public uint Quantity { get; set; }
        
        public ExchangeSellMessage()
        {
        }
        
        public ExchangeSellMessage(uint objectToSellId, uint quantity)
        {
            this.ObjectToSellId = objectToSellId;
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectToSellId);
            writer.WriteVarInt(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectToSellId = reader.ReadVarUhInt();
            if (ObjectToSellId < 0)
                throw new Exception("Forbidden value on ObjectToSellId = " + ObjectToSellId + ", it doesn't respect the following condition : objectToSellId < 0");
            Quantity = reader.ReadVarUhInt();
            if (Quantity < 0)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0");
        }
        
    }
    
}