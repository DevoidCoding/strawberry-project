

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeGuildTaxCollectorGetMessage : NetworkMessage
    {
        public const uint ProtocolId = 5762;
        public override uint MessageID => ProtocolId;
        
        public string CollectorName { get; set; }
        public short WorldX { get; set; }
        public short WorldY { get; set; }
        public double MapId { get; set; }
        public ushort SubAreaId { get; set; }
        public string UserName { get; set; }
        public ulong CallerId { get; set; }
        public string CallerName { get; set; }
        public double Experience { get; set; }
        public ushort Pods { get; set; }
        public Types.ObjectItemGenericQuantity[] ObjectsInfos { get; set; }
        
        public ExchangeGuildTaxCollectorGetMessage()
        {
        }
        
        public ExchangeGuildTaxCollectorGetMessage(string collectorName, short worldX, short worldY, double mapId, ushort subAreaId, string userName, ulong callerId, string callerName, double experience, ushort pods, Types.ObjectItemGenericQuantity[] objectsInfos)
        {
            this.CollectorName = collectorName;
            this.WorldX = worldX;
            this.WorldY = worldY;
            this.MapId = mapId;
            this.SubAreaId = subAreaId;
            this.UserName = userName;
            this.CallerId = callerId;
            this.CallerName = callerName;
            this.Experience = experience;
            this.Pods = pods;
            this.ObjectsInfos = objectsInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(CollectorName);
            writer.WriteShort(WorldX);
            writer.WriteShort(WorldY);
            writer.WriteDouble(MapId);
            writer.WriteVarShort(SubAreaId);
            writer.WriteUTF(UserName);
            writer.WriteVarLong(CallerId);
            writer.WriteUTF(CallerName);
            writer.WriteDouble(Experience);
            writer.WriteVarShort(Pods);
            writer.WriteUShort((ushort)ObjectsInfos.Length);
            foreach (var entry in ObjectsInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CollectorName = reader.ReadUTF();
            WorldX = reader.ReadShort();
            if (WorldX < -255 || WorldX > 255)
                throw new Exception("Forbidden value on WorldX = " + WorldX + ", it doesn't respect the following condition : worldX < -255 || worldX > 255");
            WorldY = reader.ReadShort();
            if (WorldY < -255 || WorldY > 255)
                throw new Exception("Forbidden value on WorldY = " + WorldY + ", it doesn't respect the following condition : worldY < -255 || worldY > 255");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            UserName = reader.ReadUTF();
            CallerId = reader.ReadVarUhLong();
            if (CallerId < 0 || CallerId > 9007199254740990)
                throw new Exception("Forbidden value on CallerId = " + CallerId + ", it doesn't respect the following condition : callerId < 0 || callerId > 9007199254740990");
            CallerName = reader.ReadUTF();
            Experience = reader.ReadDouble();
            if (Experience < -9007199254740990 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < -9007199254740990 || experience > 9007199254740990");
            Pods = reader.ReadVarUhShort();
            if (Pods < 0)
                throw new Exception("Forbidden value on Pods = " + Pods + ", it doesn't respect the following condition : pods < 0");
            var limit = reader.ReadUShort();
            ObjectsInfos = new Types.ObjectItemGenericQuantity[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectsInfos[i] = new Types.ObjectItemGenericQuantity();
                 ObjectsInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}