

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5505;
        public override uint MessageID => ProtocolId;
        
        public sbyte ExchangeType { get; set; }
        
        public ExchangeRequestMessage()
        {
        }
        
        public ExchangeRequestMessage(sbyte exchangeType)
        {
            this.ExchangeType = exchangeType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ExchangeType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ExchangeType = reader.ReadSByte();
        }
        
    }
    
}