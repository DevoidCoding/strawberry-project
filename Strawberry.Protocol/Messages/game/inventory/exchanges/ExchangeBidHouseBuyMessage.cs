

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeBidHouseBuyMessage : NetworkMessage
    {
        public const uint ProtocolId = 5804;
        public override uint MessageID => ProtocolId;
        
        public uint Uid { get; set; }
        public uint Qty { get; set; }
        public ulong Price { get; set; }
        
        public ExchangeBidHouseBuyMessage()
        {
        }
        
        public ExchangeBidHouseBuyMessage(uint uid, uint qty, ulong price)
        {
            this.Uid = uid;
            this.Qty = qty;
            this.Price = price;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Uid);
            writer.WriteVarInt(Qty);
            writer.WriteVarLong(Price);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Uid = reader.ReadVarUhInt();
            if (Uid < 0)
                throw new Exception("Forbidden value on Uid = " + Uid + ", it doesn't respect the following condition : uid < 0");
            Qty = reader.ReadVarUhInt();
            if (Qty < 0)
                throw new Exception("Forbidden value on Qty = " + Qty + ", it doesn't respect the following condition : qty < 0");
            Price = reader.ReadVarUhLong();
            if (Price < 0 || Price > 9007199254740990)
                throw new Exception("Forbidden value on Price = " + Price + ", it doesn't respect the following condition : price < 0 || price > 9007199254740990");
        }
        
    }
    
}