

// Generated on 02/12/2018 03:56:38
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeCraftPaymentModificationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6579;
        public override uint MessageID => ProtocolId;
        
        public ulong Quantity { get; set; }
        
        public ExchangeCraftPaymentModificationRequestMessage()
        {
        }
        
        public ExchangeCraftPaymentModificationRequestMessage(ulong quantity)
        {
            this.Quantity = quantity;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Quantity);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Quantity = reader.ReadVarUhLong();
            if (Quantity < 0 || Quantity > 9007199254740990)
                throw new Exception("Forbidden value on Quantity = " + Quantity + ", it doesn't respect the following condition : quantity < 0 || quantity > 9007199254740990");
        }
        
    }
    
}