

// Generated on 02/12/2018 03:56:40
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeShopStockMovementRemovedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5907;
        public override uint MessageID => ProtocolId;
        
        public uint ObjectId { get; set; }
        
        public ExchangeShopStockMovementRemovedMessage()
        {
        }
        
        public ExchangeShopStockMovementRemovedMessage(uint objectId)
        {
            this.ObjectId = objectId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ObjectId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ObjectId = reader.ReadVarUhInt();
            if (ObjectId < 0)
                throw new Exception("Forbidden value on ObjectId = " + ObjectId + ", it doesn't respect the following condition : objectId < 0");
        }
        
    }
    
}