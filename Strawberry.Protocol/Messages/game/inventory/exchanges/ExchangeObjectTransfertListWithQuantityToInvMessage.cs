

// Generated on 02/12/2018 03:56:39
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ExchangeObjectTransfertListWithQuantityToInvMessage : NetworkMessage
    {
        public const uint ProtocolId = 6470;
        public override uint MessageID => ProtocolId;
        
        public uint[] Ids { get; set; }
        public uint[] Qtys { get; set; }
        
        public ExchangeObjectTransfertListWithQuantityToInvMessage()
        {
        }
        
        public ExchangeObjectTransfertListWithQuantityToInvMessage(uint[] ids, uint[] qtys)
        {
            this.Ids = ids;
            this.Qtys = qtys;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Ids.Length);
            foreach (var entry in Ids)
            {
                 writer.WriteVarInt(entry);
            }
            writer.WriteUShort((ushort)Qtys.Length);
            foreach (var entry in Qtys)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Ids = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ids[i] = reader.ReadVarUhInt();
            }
            limit = reader.ReadUShort();
            Qtys = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 Qtys[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}