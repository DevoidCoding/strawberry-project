

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StorageInventoryContentMessage : InventoryContentMessage
    {
        public new const uint ProtocolId = 5646;
        public override uint MessageID => ProtocolId;
        
        
        public StorageInventoryContentMessage()
        {
        }
        
        public StorageInventoryContentMessage(Types.ObjectItem[] objects, ulong kamas)
         : base(objects, kamas)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}