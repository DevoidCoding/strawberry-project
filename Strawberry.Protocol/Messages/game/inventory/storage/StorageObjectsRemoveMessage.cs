

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StorageObjectsRemoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 6035;
        public override uint MessageID => ProtocolId;
        
        public uint[] ObjectUIDList { get; set; }
        
        public StorageObjectsRemoveMessage()
        {
        }
        
        public StorageObjectsRemoveMessage(uint[] objectUIDList)
        {
            this.ObjectUIDList = objectUIDList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectUIDList.Length);
            foreach (var entry in ObjectUIDList)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectUIDList = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectUIDList[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}