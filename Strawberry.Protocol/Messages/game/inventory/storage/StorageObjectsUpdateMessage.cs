

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StorageObjectsUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6036;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] ObjectList { get; set; }
        
        public StorageObjectsUpdateMessage()
        {
        }
        
        public StorageObjectsUpdateMessage(Types.ObjectItem[] objectList)
        {
            this.ObjectList = objectList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ObjectList.Length);
            foreach (var entry in ObjectList)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ObjectList = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 ObjectList[i] = new Types.ObjectItem();
                 ObjectList[i].Deserialize(reader);
            }
        }
        
    }
    
}