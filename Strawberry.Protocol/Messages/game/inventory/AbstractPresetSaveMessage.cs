

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AbstractPresetSaveMessage : NetworkMessage
    {
        public const uint ProtocolId = 6736;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte SymbolId { get; set; }
        
        public AbstractPresetSaveMessage()
        {
        }
        
        public AbstractPresetSaveMessage(sbyte presetId, sbyte symbolId)
        {
            this.PresetId = presetId;
            this.SymbolId = symbolId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(SymbolId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            SymbolId = reader.ReadSByte();
            if (SymbolId < 0)
                throw new Exception("Forbidden value on SymbolId = " + SymbolId + ", it doesn't respect the following condition : symbolId < 0");
        }
        
    }
    
}