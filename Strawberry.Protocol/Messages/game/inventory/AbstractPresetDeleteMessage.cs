

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AbstractPresetDeleteMessage : NetworkMessage
    {
        public const uint ProtocolId = 6735;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        
        public AbstractPresetDeleteMessage()
        {
        }
        
        public AbstractPresetDeleteMessage(sbyte presetId)
        {
            this.PresetId = presetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
        }
        
    }
    
}