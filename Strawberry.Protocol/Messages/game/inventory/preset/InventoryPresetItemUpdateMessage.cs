

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetItemUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6168;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public Types.PresetItem PresetItem { get; set; }
        
        public InventoryPresetItemUpdateMessage()
        {
        }
        
        public InventoryPresetItemUpdateMessage(sbyte presetId, Types.PresetItem presetItem)
        {
            this.PresetId = presetId;
            this.PresetItem = presetItem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            PresetItem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            PresetItem = new Types.PresetItem();
            PresetItem.Deserialize(reader);
        }
        
    }
    
}