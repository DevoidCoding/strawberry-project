

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetUseMessage : NetworkMessage
    {
        public const uint ProtocolId = 6615;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public bool Party { get; set; }
        
        public IdolsPresetUseMessage()
        {
        }
        
        public IdolsPresetUseMessage(sbyte presetId, bool party)
        {
            this.PresetId = presetId;
            this.Party = party;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteBoolean(Party);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            Party = reader.ReadBoolean();
        }
        
    }
    
}