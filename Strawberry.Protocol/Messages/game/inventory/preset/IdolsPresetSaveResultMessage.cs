

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetSaveResultMessage : AbstractPresetSaveResultMessage
    {
        public new const uint ProtocolId = 6604;
        public override uint MessageID => ProtocolId;
        
        
        public IdolsPresetSaveResultMessage()
        {
        }
        
        public IdolsPresetSaveResultMessage(sbyte presetId, sbyte code)
         : base(presetId, code)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}