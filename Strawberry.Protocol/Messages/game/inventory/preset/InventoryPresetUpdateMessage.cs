

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6171;
        public override uint MessageID => ProtocolId;
        
        public Types.Preset Preset { get; set; }
        
        public InventoryPresetUpdateMessage()
        {
        }
        
        public InventoryPresetUpdateMessage(Types.Preset preset)
        {
            this.Preset = preset;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Preset.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Preset = new Types.Preset();
            Preset.Deserialize(reader);
        }
        
    }
    
}