

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetSaveCustomMessage : AbstractPresetSaveMessage
    {
        public new const uint ProtocolId = 6329;
        public override uint MessageID => ProtocolId;
        
        public byte[] ItemsPositions { get; set; }
        public uint[] ItemsUids { get; set; }
        
        public InventoryPresetSaveCustomMessage()
        {
        }
        
        public InventoryPresetSaveCustomMessage(sbyte presetId, sbyte symbolId, byte[] itemsPositions, uint[] itemsUids)
         : base(presetId, symbolId)
        {
            this.ItemsPositions = itemsPositions;
            this.ItemsUids = itemsUids;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)ItemsPositions.Length);
            foreach (var entry in ItemsPositions)
            {
                 writer.WriteByte(entry);
            }
            writer.WriteUShort((ushort)ItemsUids.Length);
            foreach (var entry in ItemsUids)
            {
                 writer.WriteVarInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            ItemsPositions = new byte[limit];
            for (int i = 0; i < limit; i++)
            {
                 ItemsPositions[i] = reader.ReadByte();
            }
            limit = reader.ReadUShort();
            ItemsUids = new uint[limit];
            for (int i = 0; i < limit; i++)
            {
                 ItemsUids[i] = reader.ReadVarUhInt();
            }
        }
        
    }
    
}