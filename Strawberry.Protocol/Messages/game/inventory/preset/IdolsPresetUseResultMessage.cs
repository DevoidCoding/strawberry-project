

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetUseResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6614;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte Code { get; set; }
        public ushort[] MissingIdols { get; set; }
        
        public IdolsPresetUseResultMessage()
        {
        }
        
        public IdolsPresetUseResultMessage(sbyte presetId, sbyte code, ushort[] missingIdols)
        {
            this.PresetId = presetId;
            this.Code = code;
            this.MissingIdols = missingIdols;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(Code);
            writer.WriteUShort((ushort)MissingIdols.Length);
            foreach (var entry in MissingIdols)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            Code = reader.ReadSByte();
            if (Code < 0)
                throw new Exception("Forbidden value on Code = " + Code + ", it doesn't respect the following condition : code < 0");
            var limit = reader.ReadUShort();
            MissingIdols = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 MissingIdols[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}