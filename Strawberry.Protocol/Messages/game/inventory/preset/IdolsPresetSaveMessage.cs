

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetSaveMessage : AbstractPresetSaveMessage
    {
        public new const uint ProtocolId = 6603;
        public override uint MessageID => ProtocolId;
        
        
        public IdolsPresetSaveMessage()
        {
        }
        
        public IdolsPresetSaveMessage(sbyte presetId, sbyte symbolId)
         : base(presetId, symbolId)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}