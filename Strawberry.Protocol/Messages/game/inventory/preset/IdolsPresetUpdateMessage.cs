

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6606;
        public override uint MessageID => ProtocolId;
        
        public Types.IdolsPreset IdolsPreset { get; set; }
        
        public IdolsPresetUpdateMessage()
        {
        }
        
        public IdolsPresetUpdateMessage(Types.IdolsPreset idolsPreset)
        {
            this.IdolsPreset = idolsPreset;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            IdolsPreset.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IdolsPreset = new Types.IdolsPreset();
            IdolsPreset.Deserialize(reader);
        }
        
    }
    
}