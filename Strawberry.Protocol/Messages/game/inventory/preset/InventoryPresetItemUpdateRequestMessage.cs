

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetItemUpdateRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6210;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public byte Position { get; set; }
        public uint ObjUid { get; set; }
        
        public InventoryPresetItemUpdateRequestMessage()
        {
        }
        
        public InventoryPresetItemUpdateRequestMessage(sbyte presetId, byte position, uint objUid)
        {
            this.PresetId = presetId;
            this.Position = position;
            this.ObjUid = objUid;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteByte(Position);
            writer.WriteVarInt(ObjUid);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            Position = reader.ReadByte();
            if (Position < 0 || Position > 255)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 255");
            ObjUid = reader.ReadVarUhInt();
            if (ObjUid < 0)
                throw new Exception("Forbidden value on ObjUid = " + ObjUid + ", it doesn't respect the following condition : objUid < 0");
        }
        
    }
    
}