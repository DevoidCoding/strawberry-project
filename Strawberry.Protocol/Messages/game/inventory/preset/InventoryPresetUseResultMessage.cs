

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetUseResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6163;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte Code { get; set; }
        public byte[] UnlinkedPosition { get; set; }
        
        public InventoryPresetUseResultMessage()
        {
        }
        
        public InventoryPresetUseResultMessage(sbyte presetId, sbyte code, byte[] unlinkedPosition)
        {
            this.PresetId = presetId;
            this.Code = code;
            this.UnlinkedPosition = unlinkedPosition;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(Code);
            writer.WriteUShort((ushort)UnlinkedPosition.Length);
            foreach (var entry in UnlinkedPosition)
            {
                 writer.WriteByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            Code = reader.ReadSByte();
            if (Code < 0)
                throw new Exception("Forbidden value on Code = " + Code + ", it doesn't respect the following condition : code < 0");
            var limit = reader.ReadUShort();
            UnlinkedPosition = new byte[limit];
            for (int i = 0; i < limit; i++)
            {
                 UnlinkedPosition[i] = reader.ReadByte();
            }
        }
        
    }
    
}