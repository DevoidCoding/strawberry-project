

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetItemUpdateErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6211;
        public override uint MessageID => ProtocolId;
        
        public sbyte Code { get; set; }
        
        public InventoryPresetItemUpdateErrorMessage()
        {
        }
        
        public InventoryPresetItemUpdateErrorMessage(sbyte code)
        {
            this.Code = code;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Code);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Code = reader.ReadSByte();
            if (Code < 0)
                throw new Exception("Forbidden value on Code = " + Code + ", it doesn't respect the following condition : code < 0");
        }
        
    }
    
}