

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetSaveResultMessage : AbstractPresetSaveResultMessage
    {
        public new const uint ProtocolId = 6170;
        public override uint MessageID => ProtocolId;
        
        
        public InventoryPresetSaveResultMessage()
        {
        }
        
        public InventoryPresetSaveResultMessage(sbyte presetId, sbyte code)
         : base(presetId, code)
        {
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }
        
    }
    
}