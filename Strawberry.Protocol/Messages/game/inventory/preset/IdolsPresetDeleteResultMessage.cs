

// Generated on 02/12/2018 03:56:43
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolsPresetDeleteResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6605;
        public override uint MessageID => ProtocolId;
        
        public sbyte PresetId { get; set; }
        public sbyte Code { get; set; }
        
        public IdolsPresetDeleteResultMessage()
        {
        }
        
        public IdolsPresetDeleteResultMessage(sbyte presetId, sbyte code)
        {
            this.PresetId = presetId;
            this.Code = code;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(PresetId);
            writer.WriteSByte(Code);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PresetId = reader.ReadSByte();
            if (PresetId < 0)
                throw new Exception("Forbidden value on PresetId = " + PresetId + ", it doesn't respect the following condition : presetId < 0");
            Code = reader.ReadSByte();
            if (Code < 0)
                throw new Exception("Forbidden value on Code = " + Code + ", it doesn't respect the following condition : code < 0");
        }
        
    }
    
}