

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InventoryPresetSaveMessage : AbstractPresetSaveMessage
    {
        public new const uint ProtocolId = 6165;
        public override uint MessageID => ProtocolId;
        
        public bool SaveEquipment { get; set; }
        
        public InventoryPresetSaveMessage()
        {
        }
        
        public InventoryPresetSaveMessage(sbyte presetId, sbyte symbolId, bool saveEquipment)
         : base(presetId, symbolId)
        {
            this.SaveEquipment = saveEquipment;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteBoolean(SaveEquipment);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SaveEquipment = reader.ReadBoolean();
        }
        
    }
    
}