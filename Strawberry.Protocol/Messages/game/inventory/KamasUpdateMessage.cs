

// Generated on 02/12/2018 03:56:37
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class KamasUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5537;
        public override uint MessageID => ProtocolId;
        
        public ulong KamasTotal { get; set; }
        
        public KamasUpdateMessage()
        {
        }
        
        public KamasUpdateMessage(ulong kamasTotal)
        {
            this.KamasTotal = kamasTotal;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(KamasTotal);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            KamasTotal = reader.ReadVarUhLong();
            if (KamasTotal < 0 || KamasTotal > 9007199254740990)
                throw new Exception("Forbidden value on KamasTotal = " + KamasTotal + ", it doesn't respect the following condition : kamasTotal < 0 || kamasTotal > 9007199254740990");
        }
        
    }
    
}