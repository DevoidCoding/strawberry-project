

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SpellListMessage : NetworkMessage
    {
        public const uint ProtocolId = 1200;
        public override uint MessageID => ProtocolId;
        
        public bool SpellPrevisualization { get; set; }
        public Types.SpellItem[] Spells { get; set; }
        
        public SpellListMessage()
        {
        }
        
        public SpellListMessage(bool spellPrevisualization, Types.SpellItem[] spells)
        {
            this.SpellPrevisualization = spellPrevisualization;
            this.Spells = spells;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(SpellPrevisualization);
            writer.WriteUShort((ushort)Spells.Length);
            foreach (var entry in Spells)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellPrevisualization = reader.ReadBoolean();
            var limit = reader.ReadUShort();
            Spells = new Types.SpellItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Spells[i] = new Types.SpellItem();
                 Spells[i].Deserialize(reader);
            }
        }
        
    }
    
}