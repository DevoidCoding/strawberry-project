

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SpellLevelUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6743;
        public override uint MessageID => ProtocolId;
        
        public Types.SpellItem Spell { get; set; }
        
        public SpellLevelUpdateMessage()
        {
        }
        
        public SpellLevelUpdateMessage(Types.SpellItem spell)
        {
            this.Spell = spell;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Spell.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Spell = new Types.SpellItem();
            Spell.Deserialize(reader);
        }
        
    }
    
}