

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccessoryPreviewMessage : NetworkMessage
    {
        public const uint ProtocolId = 6517;
        public override uint MessageID => ProtocolId;
        
        public Types.EntityLook Look { get; set; }
        
        public AccessoryPreviewMessage()
        {
        }
        
        public AccessoryPreviewMessage(Types.EntityLook look)
        {
            this.Look = look;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Look.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
        }
        
    }
    
}