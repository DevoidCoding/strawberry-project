

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccessoryPreviewRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6518;
        public override uint MessageID => ProtocolId;
        
        public ushort[] GenericId { get; set; }
        
        public AccessoryPreviewRequestMessage()
        {
        }
        
        public AccessoryPreviewRequestMessage(ushort[] genericId)
        {
            this.GenericId = genericId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)GenericId.Length);
            foreach (var entry in GenericId)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            GenericId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 GenericId[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}