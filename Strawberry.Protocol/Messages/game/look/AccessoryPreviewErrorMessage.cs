

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccessoryPreviewErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6521;
        public override uint MessageID => ProtocolId;
        
        public sbyte Error { get; set; }
        
        public AccessoryPreviewErrorMessage()
        {
        }
        
        public AccessoryPreviewErrorMessage(sbyte error)
        {
            this.Error = error;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Error);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Error = reader.ReadSByte();
            if (Error < 0)
                throw new Exception("Forbidden value on Error = " + Error + ", it doesn't respect the following condition : error < 0");
        }
        
    }
    
}