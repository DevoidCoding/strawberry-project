

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterReportMessage : NetworkMessage
    {
        public const uint ProtocolId = 6079;
        public override uint MessageID => ProtocolId;
        
        public ulong ReportedId { get; set; }
        public sbyte Reason { get; set; }
        
        public CharacterReportMessage()
        {
        }
        
        public CharacterReportMessage(ulong reportedId, sbyte reason)
        {
            this.ReportedId = reportedId;
            this.Reason = reason;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(ReportedId);
            writer.WriteSByte(Reason);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ReportedId = reader.ReadVarUhLong();
            if (ReportedId < 0 || ReportedId > 9007199254740990)
                throw new Exception("Forbidden value on ReportedId = " + ReportedId + ", it doesn't respect the following condition : reportedId < 0 || reportedId > 9007199254740990");
            Reason = reader.ReadSByte();
            if (Reason < 0)
                throw new Exception("Forbidden value on Reason = " + Reason + ", it doesn't respect the following condition : reason < 0");
        }
        
    }
    
}