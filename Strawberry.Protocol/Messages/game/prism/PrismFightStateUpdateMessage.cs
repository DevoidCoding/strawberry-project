

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismFightStateUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6040;
        public override uint MessageID => ProtocolId;
        
        public sbyte State { get; set; }
        
        public PrismFightStateUpdateMessage()
        {
        }
        
        public PrismFightStateUpdateMessage(sbyte state)
        {
            this.State = state;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(State);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
        }
        
    }
    
}