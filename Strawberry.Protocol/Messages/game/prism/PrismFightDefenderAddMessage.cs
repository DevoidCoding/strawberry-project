

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismFightDefenderAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5895;
        public override uint MessageID => ProtocolId;
        
        public ushort SubAreaId { get; set; }
        public ushort FightId { get; set; }
        public Types.CharacterMinimalPlusLookInformations Defender { get; set; }
        
        public PrismFightDefenderAddMessage()
        {
        }
        
        public PrismFightDefenderAddMessage(ushort subAreaId, ushort fightId, Types.CharacterMinimalPlusLookInformations defender)
        {
            this.SubAreaId = subAreaId;
            this.FightId = fightId;
            this.Defender = defender;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteVarShort(FightId);
            writer.WriteShort(Defender.TypeID);
            Defender.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            FightId = reader.ReadVarUhShort();
            if (FightId < 0)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0");
            Defender = Types.ProtocolTypeManager.GetInstance<Types.CharacterMinimalPlusLookInformations>(reader.ReadShort());
            Defender.Deserialize(reader);
        }
        
    }
    
}