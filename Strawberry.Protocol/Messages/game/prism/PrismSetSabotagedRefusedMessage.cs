

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismSetSabotagedRefusedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6466;
        public override uint MessageID => ProtocolId;
        
        public ushort SubAreaId { get; set; }
        public sbyte Reason { get; set; }
        
        public PrismSetSabotagedRefusedMessage()
        {
        }
        
        public PrismSetSabotagedRefusedMessage(ushort subAreaId, sbyte reason)
        {
            this.SubAreaId = subAreaId;
            this.Reason = reason;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteSByte(Reason);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            Reason = reader.ReadSByte();
        }
        
    }
    
}