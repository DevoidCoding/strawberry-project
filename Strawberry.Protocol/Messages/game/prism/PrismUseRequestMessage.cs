

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismUseRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6041;
        public override uint MessageID => ProtocolId;
        
        public sbyte ModuleToUse { get; set; }
        
        public PrismUseRequestMessage()
        {
        }
        
        public PrismUseRequestMessage(sbyte moduleToUse)
        {
            this.ModuleToUse = moduleToUse;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ModuleToUse);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ModuleToUse = reader.ReadSByte();
            if (ModuleToUse < 0)
                throw new Exception("Forbidden value on ModuleToUse = " + ModuleToUse + ", it doesn't respect the following condition : moduleToUse < 0");
        }
        
    }
    
}