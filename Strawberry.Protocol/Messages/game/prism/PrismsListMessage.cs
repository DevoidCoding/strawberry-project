

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6440;
        public override uint MessageID => ProtocolId;
        
        public Types.PrismSubareaEmptyInfo[] Prisms { get; set; }
        
        public PrismsListMessage()
        {
        }
        
        public PrismsListMessage(Types.PrismSubareaEmptyInfo[] prisms)
        {
            this.Prisms = prisms;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Prisms.Length);
            foreach (var entry in Prisms)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Prisms = new Types.PrismSubareaEmptyInfo[limit];
            for (int i = 0; i < limit; i++)
            {
                 Prisms[i] = Types.ProtocolTypeManager.GetInstance<Types.PrismSubareaEmptyInfo>(reader.ReadShort());
                 Prisms[i].Deserialize(reader);
            }
        }
        
    }
    
}