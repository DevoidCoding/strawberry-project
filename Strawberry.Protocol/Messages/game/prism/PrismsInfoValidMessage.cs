

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismsInfoValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6451;
        public override uint MessageID => ProtocolId;
        
        public Types.PrismFightersInformation[] Fights { get; set; }
        
        public PrismsInfoValidMessage()
        {
        }
        
        public PrismsInfoValidMessage(Types.PrismFightersInformation[] fights)
        {
            this.Fights = fights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Fights.Length);
            foreach (var entry in Fights)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Fights = new Types.PrismFightersInformation[limit];
            for (int i = 0; i < limit; i++)
            {
                 Fights[i] = new Types.PrismFightersInformation();
                 Fights[i].Deserialize(reader);
            }
        }
        
    }
    
}