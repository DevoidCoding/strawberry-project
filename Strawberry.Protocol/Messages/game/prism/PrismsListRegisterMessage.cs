

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismsListRegisterMessage : NetworkMessage
    {
        public const uint ProtocolId = 6441;
        public override uint MessageID => ProtocolId;
        
        public sbyte Listen { get; set; }
        
        public PrismsListRegisterMessage()
        {
        }
        
        public PrismsListRegisterMessage(sbyte listen)
        {
            this.Listen = listen;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Listen);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Listen = reader.ReadSByte();
            if (Listen < 0)
                throw new Exception("Forbidden value on Listen = " + Listen + ", it doesn't respect the following condition : listen < 0");
        }
        
    }
    
}