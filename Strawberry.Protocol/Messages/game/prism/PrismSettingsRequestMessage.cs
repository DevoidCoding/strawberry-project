

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismSettingsRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6437;
        public override uint MessageID => ProtocolId;
        
        public ushort SubAreaId { get; set; }
        public sbyte StartDefenseTime { get; set; }
        
        public PrismSettingsRequestMessage()
        {
        }
        
        public PrismSettingsRequestMessage(ushort subAreaId, sbyte startDefenseTime)
        {
            this.SubAreaId = subAreaId;
            this.StartDefenseTime = startDefenseTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SubAreaId);
            writer.WriteSByte(StartDefenseTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SubAreaId = reader.ReadVarUhShort();
            if (SubAreaId < 0)
                throw new Exception("Forbidden value on SubAreaId = " + SubAreaId + ", it doesn't respect the following condition : subAreaId < 0");
            StartDefenseTime = reader.ReadSByte();
            if (StartDefenseTime < 0)
                throw new Exception("Forbidden value on StartDefenseTime = " + StartDefenseTime + ", it doesn't respect the following condition : startDefenseTime < 0");
        }
        
    }
    
}