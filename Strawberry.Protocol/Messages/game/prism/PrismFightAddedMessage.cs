

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismFightAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6452;
        public override uint MessageID => ProtocolId;
        
        public Types.PrismFightersInformation Fight { get; set; }
        
        public PrismFightAddedMessage()
        {
        }
        
        public PrismFightAddedMessage(Types.PrismFightersInformation fight)
        {
            this.Fight = fight;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Fight.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Fight = new Types.PrismFightersInformation();
            Fight.Deserialize(reader);
        }
        
    }
    
}