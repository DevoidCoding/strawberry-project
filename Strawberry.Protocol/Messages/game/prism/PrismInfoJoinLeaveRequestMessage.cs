

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PrismInfoJoinLeaveRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5844;
        public override uint MessageID => ProtocolId;
        
        public bool Join { get; set; }
        
        public PrismInfoJoinLeaveRequestMessage()
        {
        }
        
        public PrismInfoJoinLeaveRequestMessage(bool join)
        {
            this.Join = join;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Join);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Join = reader.ReadBoolean();
        }
        
    }
    
}