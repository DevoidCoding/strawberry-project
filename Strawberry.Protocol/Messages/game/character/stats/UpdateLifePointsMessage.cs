

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class UpdateLifePointsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5658;
        public override uint MessageID => ProtocolId;
        
        public uint LifePoints { get; set; }
        public uint MaxLifePoints { get; set; }
        
        public UpdateLifePointsMessage()
        {
        }
        
        public UpdateLifePointsMessage(uint lifePoints, uint maxLifePoints)
        {
            this.LifePoints = lifePoints;
            this.MaxLifePoints = maxLifePoints;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(LifePoints);
            writer.WriteVarInt(MaxLifePoints);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            LifePoints = reader.ReadVarUhInt();
            if (LifePoints < 0)
                throw new Exception("Forbidden value on LifePoints = " + LifePoints + ", it doesn't respect the following condition : lifePoints < 0");
            MaxLifePoints = reader.ReadVarUhInt();
            if (MaxLifePoints < 0)
                throw new Exception("Forbidden value on MaxLifePoints = " + MaxLifePoints + ", it doesn't respect the following condition : maxLifePoints < 0");
        }
        
    }
    
}