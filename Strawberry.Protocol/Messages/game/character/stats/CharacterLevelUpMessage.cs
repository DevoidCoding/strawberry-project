

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterLevelUpMessage : NetworkMessage
    {
        public const uint ProtocolId = 5670;
        public override uint MessageID => ProtocolId;
        
        public ushort NewLevel { get; set; }
        
        public CharacterLevelUpMessage()
        {
        }
        
        public CharacterLevelUpMessage(ushort newLevel)
        {
            this.NewLevel = newLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(NewLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NewLevel = reader.ReadVarUhShort();
            if (NewLevel < 0)
                throw new Exception("Forbidden value on NewLevel = " + NewLevel + ", it doesn't respect the following condition : newLevel < 0");
        }
        
    }
    
}