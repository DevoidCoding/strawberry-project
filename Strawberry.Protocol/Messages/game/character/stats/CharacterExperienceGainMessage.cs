

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterExperienceGainMessage : NetworkMessage
    {
        public const uint ProtocolId = 6321;
        public override uint MessageID => ProtocolId;
        
        public ulong ExperienceCharacter { get; set; }
        public ulong ExperienceMount { get; set; }
        public ulong ExperienceGuild { get; set; }
        public ulong ExperienceIncarnation { get; set; }
        
        public CharacterExperienceGainMessage()
        {
        }
        
        public CharacterExperienceGainMessage(ulong experienceCharacter, ulong experienceMount, ulong experienceGuild, ulong experienceIncarnation)
        {
            this.ExperienceCharacter = experienceCharacter;
            this.ExperienceMount = experienceMount;
            this.ExperienceGuild = experienceGuild;
            this.ExperienceIncarnation = experienceIncarnation;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(ExperienceCharacter);
            writer.WriteVarLong(ExperienceMount);
            writer.WriteVarLong(ExperienceGuild);
            writer.WriteVarLong(ExperienceIncarnation);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ExperienceCharacter = reader.ReadVarUhLong();
            if (ExperienceCharacter < 0 || ExperienceCharacter > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceCharacter = " + ExperienceCharacter + ", it doesn't respect the following condition : experienceCharacter < 0 || experienceCharacter > 9007199254740990");
            ExperienceMount = reader.ReadVarUhLong();
            if (ExperienceMount < 0 || ExperienceMount > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceMount = " + ExperienceMount + ", it doesn't respect the following condition : experienceMount < 0 || experienceMount > 9007199254740990");
            ExperienceGuild = reader.ReadVarUhLong();
            if (ExperienceGuild < 0 || ExperienceGuild > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceGuild = " + ExperienceGuild + ", it doesn't respect the following condition : experienceGuild < 0 || experienceGuild > 9007199254740990");
            ExperienceIncarnation = reader.ReadVarUhLong();
            if (ExperienceIncarnation < 0 || ExperienceIncarnation > 9007199254740990)
                throw new Exception("Forbidden value on ExperienceIncarnation = " + ExperienceIncarnation + ", it doesn't respect the following condition : experienceIncarnation < 0 || experienceIncarnation > 9007199254740990");
        }
        
    }
    
}