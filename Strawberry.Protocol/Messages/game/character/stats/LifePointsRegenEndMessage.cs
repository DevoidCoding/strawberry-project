

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LifePointsRegenEndMessage : UpdateLifePointsMessage
    {
        public new const uint ProtocolId = 5686;
        public override uint MessageID => ProtocolId;
        
        public uint LifePointsGained { get; set; }
        
        public LifePointsRegenEndMessage()
        {
        }
        
        public LifePointsRegenEndMessage(uint lifePoints, uint maxLifePoints, uint lifePointsGained)
         : base(lifePoints, maxLifePoints)
        {
            this.LifePointsGained = lifePointsGained;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarInt(LifePointsGained);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LifePointsGained = reader.ReadVarUhInt();
            if (LifePointsGained < 0)
                throw new Exception("Forbidden value on LifePointsGained = " + LifePointsGained + ", it doesn't respect the following condition : lifePointsGained < 0");
        }
        
    }
    
}