

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FighterStatsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6322;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterCharacteristicsInformations Stats { get; set; }
        
        public FighterStatsListMessage()
        {
        }
        
        public FighterStatsListMessage(Types.CharacterCharacteristicsInformations stats)
        {
            this.Stats = stats;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Stats.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Stats = new Types.CharacterCharacteristicsInformations();
            Stats.Deserialize(reader);
        }
        
    }
    
}