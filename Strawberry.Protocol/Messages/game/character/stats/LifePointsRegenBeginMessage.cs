

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LifePointsRegenBeginMessage : NetworkMessage
    {
        public const uint ProtocolId = 5684;
        public override uint MessageID => ProtocolId;
        
        public byte RegenRate { get; set; }
        
        public LifePointsRegenBeginMessage()
        {
        }
        
        public LifePointsRegenBeginMessage(byte regenRate)
        {
            this.RegenRate = regenRate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(RegenRate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RegenRate = reader.ReadByte();
            if (RegenRate < 0 || RegenRate > 255)
                throw new Exception("Forbidden value on RegenRate = " + RegenRate + ", it doesn't respect the following condition : regenRate < 0 || regenRate > 255");
        }
        
    }
    
}