

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterStatsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 500;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterCharacteristicsInformations Stats { get; set; }
        
        public CharacterStatsListMessage()
        {
        }
        
        public CharacterStatsListMessage(Types.CharacterCharacteristicsInformations stats)
        {
            this.Stats = stats;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Stats.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Stats = new Types.CharacterCharacteristicsInformations();
            Stats.Deserialize(reader);
        }
        
    }
    
}