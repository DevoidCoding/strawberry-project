

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterLevelUpInformationMessage : CharacterLevelUpMessage
    {
        public new const uint ProtocolId = 6076;
        public override uint MessageID => ProtocolId;
        
        public string Name { get; set; }
        public ulong Id { get; set; }
        
        public CharacterLevelUpInformationMessage()
        {
        }
        
        public CharacterLevelUpInformationMessage(ushort newLevel, string name, ulong id)
         : base(newLevel)
        {
            this.Name = name;
            this.Id = id;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Name);
            writer.WriteVarLong(Id);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Name = reader.ReadUTF();
            Id = reader.ReadVarUhLong();
            if (Id < 0 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0 || id > 9007199254740990");
        }
        
    }
    
}