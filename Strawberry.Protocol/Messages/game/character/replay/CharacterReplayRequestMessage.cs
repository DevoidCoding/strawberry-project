

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterReplayRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 167;
        public override uint MessageID => ProtocolId;
        
        public ulong CharacterId { get; set; }
        
        public CharacterReplayRequestMessage()
        {
        }
        
        public CharacterReplayRequestMessage(ulong characterId)
        {
            this.CharacterId = characterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(CharacterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CharacterId = reader.ReadVarUhLong();
            if (CharacterId < 0 || CharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CharacterId = " + CharacterId + ", it doesn't respect the following condition : characterId < 0 || characterId > 9007199254740990");
        }
        
    }
    
}