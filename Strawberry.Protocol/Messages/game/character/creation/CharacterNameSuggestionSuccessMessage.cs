

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterNameSuggestionSuccessMessage : NetworkMessage
    {
        public const uint ProtocolId = 5544;
        public override uint MessageID => ProtocolId;
        
        public string Suggestion { get; set; }
        
        public CharacterNameSuggestionSuccessMessage()
        {
        }
        
        public CharacterNameSuggestionSuccessMessage(string suggestion)
        {
            this.Suggestion = suggestion;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Suggestion);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Suggestion = reader.ReadUTF();
        }
        
    }
    
}