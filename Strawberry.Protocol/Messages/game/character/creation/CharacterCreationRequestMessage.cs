

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterCreationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 160;
        public override uint MessageID => ProtocolId;
        
        public string Name { get; set; }
        public sbyte Breed { get; set; }
        public bool Sex { get; set; }
        public int[] Colors { get; set; }
        public ushort CosmeticId { get; set; }
        
        public CharacterCreationRequestMessage()
        {
        }
        
        public CharacterCreationRequestMessage(string name, sbyte breed, bool sex, int[] colors, ushort cosmeticId)
        {
            this.Name = name;
            this.Breed = breed;
            this.Sex = sex;
            this.Colors = colors;
            this.CosmeticId = cosmeticId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Name);
            writer.WriteSByte(Breed);
            writer.WriteBoolean(Sex);
            foreach (var entry in Colors)
            {
                 writer.WriteInt(entry);
            }
            writer.WriteVarShort(CosmeticId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Name = reader.ReadUTF();
            Breed = reader.ReadSByte();
            if (Breed < (byte)Enums.PlayableBreedEnum.Feca || Breed > (byte)Enums.PlayableBreedEnum.Ouginak)
                throw new Exception("Forbidden value on Breed = " + Breed + ", it doesn't respect the following condition : breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Ouginak");
            Sex = reader.ReadBoolean();
            Colors = new int[5];
            for (int i = 0; i < 5; i++)
            {
                 Colors[i] = reader.ReadInt();
            }
            CosmeticId = reader.ReadVarUhShort();
            if (CosmeticId < 0)
                throw new Exception("Forbidden value on CosmeticId = " + CosmeticId + ", it doesn't respect the following condition : cosmeticId < 0");
        }
        
    }
    
}