

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterSelectedForceMessage : NetworkMessage
    {
        public const uint ProtocolId = 6068;
        public override uint MessageID => ProtocolId;
        
        public int Id { get; set; }
        
        public CharacterSelectedForceMessage()
        {
        }
        
        public CharacterSelectedForceMessage(int id)
        {
            this.Id = id;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Id);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadInt();
            if (Id < 1 || Id > 2147483647)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 1 || id > 2147483647");
        }
        
    }
    
}