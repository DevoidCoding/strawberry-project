

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterSelectedSuccessMessage : NetworkMessage
    {
        public const uint ProtocolId = 153;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterBaseInformations Infos { get; set; }
        public bool IsCollectingStats { get; set; }
        
        public CharacterSelectedSuccessMessage()
        {
        }
        
        public CharacterSelectedSuccessMessage(Types.CharacterBaseInformations infos, bool isCollectingStats)
        {
            this.Infos = infos;
            this.IsCollectingStats = isCollectingStats;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Infos.Serialize(writer);
            writer.WriteBoolean(IsCollectingStats);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Infos = new Types.CharacterBaseInformations();
            Infos.Deserialize(reader);
            IsCollectingStats = reader.ReadBoolean();
        }
        
    }
    
}