

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterSelectionWithRemodelMessage : CharacterSelectionMessage
    {
        public new const uint ProtocolId = 6549;
        public override uint MessageID => ProtocolId;
        
        public Types.RemodelingInformation Remodel { get; set; }
        
        public CharacterSelectionWithRemodelMessage()
        {
        }
        
        public CharacterSelectionWithRemodelMessage(ulong id, Types.RemodelingInformation remodel)
         : base(id)
        {
            this.Remodel = remodel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Remodel.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Remodel = new Types.RemodelingInformation();
            Remodel.Deserialize(reader);
        }
        
    }
    
}