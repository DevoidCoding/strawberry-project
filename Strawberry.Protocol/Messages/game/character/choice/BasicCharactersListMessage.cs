

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicCharactersListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6475;
        public override uint MessageID => ProtocolId;
        
        public Types.CharacterBaseInformations[] Characters { get; set; }
        
        public BasicCharactersListMessage()
        {
        }
        
        public BasicCharactersListMessage(Types.CharacterBaseInformations[] characters)
        {
            this.Characters = characters;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Characters.Length);
            foreach (var entry in Characters)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Characters = new Types.CharacterBaseInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Characters[i] = Types.ProtocolTypeManager.GetInstance<Types.CharacterBaseInformations>(reader.ReadShort());
                 Characters[i].Deserialize(reader);
            }
        }
        
    }
    
}