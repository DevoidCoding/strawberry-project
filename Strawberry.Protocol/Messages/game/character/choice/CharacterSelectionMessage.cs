

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterSelectionMessage : NetworkMessage
    {
        public const uint ProtocolId = 152;
        public override uint MessageID => ProtocolId;
        
        public ulong Id { get; set; }
        
        public CharacterSelectionMessage()
        {
        }
        
        public CharacterSelectionMessage(ulong id)
        {
            this.Id = id;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(Id);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Id = reader.ReadVarUhLong();
            if (Id < 0 || Id > 9007199254740990)
                throw new Exception("Forbidden value on Id = " + Id + ", it doesn't respect the following condition : id < 0 || id > 9007199254740990");
        }
        
    }
    
}