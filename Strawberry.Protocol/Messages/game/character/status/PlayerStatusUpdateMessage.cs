

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PlayerStatusUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6386;
        public override uint MessageID => ProtocolId;
        
        public int AccountId { get; set; }
        public ulong PlayerId { get; set; }
        public Types.PlayerStatus Status { get; set; }
        
        public PlayerStatusUpdateMessage()
        {
        }
        
        public PlayerStatusUpdateMessage(int accountId, ulong playerId, Types.PlayerStatus status)
        {
            this.AccountId = accountId;
            this.PlayerId = playerId;
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteVarLong(PlayerId);
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
        
    }
    
}