

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PlayerStatusUpdateRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6387;
        public override uint MessageID => ProtocolId;
        
        public Types.PlayerStatus Status { get; set; }
        
        public PlayerStatusUpdateRequestMessage()
        {
        }
        
        public PlayerStatusUpdateRequestMessage(Types.PlayerStatus status)
        {
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Status.TypeID);
            Status.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Status = Types.ProtocolTypeManager.GetInstance<Types.PlayerStatus>(reader.ReadShort());
            Status.Deserialize(reader);
        }
        
    }
    
}