

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CharacterDeletionRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 165;
        public override uint MessageID => ProtocolId;
        
        public ulong CharacterId { get; set; }
        public string SecretAnswerHash { get; set; }
        
        public CharacterDeletionRequestMessage()
        {
        }
        
        public CharacterDeletionRequestMessage(ulong characterId, string secretAnswerHash)
        {
            this.CharacterId = characterId;
            this.SecretAnswerHash = secretAnswerHash;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(CharacterId);
            writer.WriteUTF(SecretAnswerHash);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CharacterId = reader.ReadVarUhLong();
            if (CharacterId < 0 || CharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CharacterId = " + CharacterId + ", it doesn't respect the following condition : characterId < 0 || characterId > 9007199254740990");
            SecretAnswerHash = reader.ReadUTF();
        }
        
    }
    
}