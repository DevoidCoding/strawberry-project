

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FinishMoveListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6704;
        public override uint MessageID => ProtocolId;
        
        public Types.FinishMoveInformations[] FinishMoves { get; set; }
        
        public FinishMoveListMessage()
        {
        }
        
        public FinishMoveListMessage(Types.FinishMoveInformations[] finishMoves)
        {
            this.FinishMoves = finishMoves;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)FinishMoves.Length);
            foreach (var entry in FinishMoves)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            FinishMoves = new Types.FinishMoveInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishMoves[i] = new Types.FinishMoveInformations();
                 FinishMoves[i].Deserialize(reader);
            }
        }
        
    }
    
}