

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FinishMoveSetRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6703;
        public override uint MessageID => ProtocolId;
        
        public int FinishMoveId { get; set; }
        public bool FinishMoveState { get; set; }
        
        public FinishMoveSetRequestMessage()
        {
        }
        
        public FinishMoveSetRequestMessage(int finishMoveId, bool finishMoveState)
        {
            this.FinishMoveId = finishMoveId;
            this.FinishMoveState = finishMoveState;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(FinishMoveId);
            writer.WriteBoolean(FinishMoveState);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FinishMoveId = reader.ReadInt();
            if (FinishMoveId < 0)
                throw new Exception("Forbidden value on FinishMoveId = " + FinishMoveId + ", it doesn't respect the following condition : finishMoveId < 0");
            FinishMoveState = reader.ReadBoolean();
        }
        
    }
    
}