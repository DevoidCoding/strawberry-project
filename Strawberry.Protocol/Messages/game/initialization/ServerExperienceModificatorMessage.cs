

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerExperienceModificatorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6237;
        public override uint MessageID => ProtocolId;
        
        public ushort ExperiencePercent { get; set; }
        
        public ServerExperienceModificatorMessage()
        {
        }
        
        public ServerExperienceModificatorMessage(ushort experiencePercent)
        {
            this.ExperiencePercent = experiencePercent;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ExperiencePercent);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ExperiencePercent = reader.ReadVarUhShort();
            if (ExperiencePercent < 0)
                throw new Exception("Forbidden value on ExperiencePercent = " + ExperiencePercent + ", it doesn't respect the following condition : experiencePercent < 0");
        }
        
    }
    
}