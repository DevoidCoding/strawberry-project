

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class OnConnectionEventMessage : NetworkMessage
    {
        public const uint ProtocolId = 5726;
        public override uint MessageID => ProtocolId;
        
        public sbyte EventType { get; set; }
        
        public OnConnectionEventMessage()
        {
        }
        
        public OnConnectionEventMessage(sbyte eventType)
        {
            this.EventType = eventType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(EventType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            EventType = reader.ReadSByte();
            if (EventType < 0)
                throw new Exception("Forbidden value on EventType = " + EventType + ", it doesn't respect the following condition : eventType < 0");
        }
        
    }
    
}