

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SetCharacterRestrictionsMessage : NetworkMessage
    {
        public const uint ProtocolId = 170;
        public override uint MessageID => ProtocolId;
        
        public double ActorId { get; set; }
        public Types.ActorRestrictionsInformations Restrictions { get; set; }
        
        public SetCharacterRestrictionsMessage()
        {
        }
        
        public SetCharacterRestrictionsMessage(double actorId, Types.ActorRestrictionsInformations restrictions)
        {
            this.ActorId = actorId;
            this.Restrictions = restrictions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(ActorId);
            Restrictions.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ActorId = reader.ReadDouble();
            if (ActorId < -9007199254740990 || ActorId > 9007199254740990)
                throw new Exception("Forbidden value on ActorId = " + ActorId + ", it doesn't respect the following condition : actorId < -9007199254740990 || actorId > 9007199254740990");
            Restrictions = new Types.ActorRestrictionsInformations();
            Restrictions.Deserialize(reader);
        }
        
    }
    
}