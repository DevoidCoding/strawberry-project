

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolPartyLostMessage : NetworkMessage
    {
        public const uint ProtocolId = 6580;
        public override uint MessageID => ProtocolId;
        
        public ushort IdolId { get; set; }
        
        public IdolPartyLostMessage()
        {
        }
        
        public IdolPartyLostMessage(ushort idolId)
        {
            this.IdolId = idolId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(IdolId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IdolId = reader.ReadVarUhShort();
            if (IdolId < 0)
                throw new Exception("Forbidden value on IdolId = " + IdolId + ", it doesn't respect the following condition : idolId < 0");
        }
        
    }
    
}