

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolPartyRefreshMessage : NetworkMessage
    {
        public const uint ProtocolId = 6583;
        public override uint MessageID => ProtocolId;
        
        public Types.PartyIdol PartyIdol { get; set; }
        
        public IdolPartyRefreshMessage()
        {
        }
        
        public IdolPartyRefreshMessage(Types.PartyIdol partyIdol)
        {
            this.PartyIdol = partyIdol;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            PartyIdol.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PartyIdol = new Types.PartyIdol();
            PartyIdol.Deserialize(reader);
        }
        
    }
    
}