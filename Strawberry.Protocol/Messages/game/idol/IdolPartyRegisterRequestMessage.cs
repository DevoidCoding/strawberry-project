

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolPartyRegisterRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6582;
        public override uint MessageID => ProtocolId;
        
        public bool Register { get; set; }
        
        public IdolPartyRegisterRequestMessage()
        {
        }
        
        public IdolPartyRegisterRequestMessage(bool register)
        {
            this.Register = register;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Register);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Register = reader.ReadBoolean();
        }
        
    }
    
}