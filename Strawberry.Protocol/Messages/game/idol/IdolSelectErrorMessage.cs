

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolSelectErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6584;
        public override uint MessageID => ProtocolId;
        
        public bool Activate { get; set; }
        public bool Party { get; set; }
        public sbyte Reason { get; set; }
        public ushort IdolId { get; set; }
        
        public IdolSelectErrorMessage()
        {
        }
        
        public IdolSelectErrorMessage(bool activate, bool party, sbyte reason, ushort idolId)
        {
            this.Activate = activate;
            this.Party = party;
            this.Reason = reason;
            this.IdolId = idolId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Activate);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Party);
            writer.WriteByte(flag1);
            writer.WriteSByte(Reason);
            writer.WriteVarShort(IdolId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Activate = BooleanByteWrapper.GetFlag(flag1, 0);
            Party = BooleanByteWrapper.GetFlag(flag1, 1);
            Reason = reader.ReadSByte();
            if (Reason < 0)
                throw new Exception("Forbidden value on Reason = " + Reason + ", it doesn't respect the following condition : reason < 0");
            IdolId = reader.ReadVarUhShort();
            if (IdolId < 0)
                throw new Exception("Forbidden value on IdolId = " + IdolId + ", it doesn't respect the following condition : idolId < 0");
        }
        
    }
    
}