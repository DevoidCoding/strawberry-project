

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolFightPreparationUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6586;
        public override uint MessageID => ProtocolId;
        
        public sbyte IdolSource { get; set; }
        public Types.Idol[] Idols { get; set; }
        
        public IdolFightPreparationUpdateMessage()
        {
        }
        
        public IdolFightPreparationUpdateMessage(sbyte idolSource, Types.Idol[] idols)
        {
            this.IdolSource = idolSource;
            this.Idols = idols;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(IdolSource);
            writer.WriteUShort((ushort)Idols.Length);
            foreach (var entry in Idols)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IdolSource = reader.ReadSByte();
            if (IdolSource < 0)
                throw new Exception("Forbidden value on IdolSource = " + IdolSource + ", it doesn't respect the following condition : idolSource < 0");
            var limit = reader.ReadUShort();
            Idols = new Types.Idol[limit];
            for (int i = 0; i < limit; i++)
            {
                 Idols[i] = Types.ProtocolTypeManager.GetInstance<Types.Idol>(reader.ReadShort());
                 Idols[i].Deserialize(reader);
            }
        }
        
    }
    
}