

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdolListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6585;
        public override uint MessageID => ProtocolId;
        
        public ushort[] ChosenIdols { get; set; }
        public ushort[] PartyChosenIdols { get; set; }
        public Types.PartyIdol[] PartyIdols { get; set; }
        
        public IdolListMessage()
        {
        }
        
        public IdolListMessage(ushort[] chosenIdols, ushort[] partyChosenIdols, Types.PartyIdol[] partyIdols)
        {
            this.ChosenIdols = chosenIdols;
            this.PartyChosenIdols = partyChosenIdols;
            this.PartyIdols = partyIdols;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)ChosenIdols.Length);
            foreach (var entry in ChosenIdols)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)PartyChosenIdols.Length);
            foreach (var entry in PartyChosenIdols)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)PartyIdols.Length);
            foreach (var entry in PartyIdols)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            ChosenIdols = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 ChosenIdols[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            PartyChosenIdols = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 PartyChosenIdols[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            PartyIdols = new Types.PartyIdol[limit];
            for (int i = 0; i < limit; i++)
            {
                 PartyIdols[i] = Types.ProtocolTypeManager.GetInstance<Types.PartyIdol>(reader.ReadShort());
                 PartyIdols[i].Deserialize(reader);
            }
        }
        
    }
    
}