

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildChangeMemberParametersMessage : NetworkMessage
    {
        public const uint ProtocolId = 5549;
        public override uint MessageID => ProtocolId;
        
        public ulong MemberId { get; set; }
        public ushort Rank { get; set; }
        public sbyte ExperienceGivenPercent { get; set; }
        public uint Rights { get; set; }
        
        public GuildChangeMemberParametersMessage()
        {
        }
        
        public GuildChangeMemberParametersMessage(ulong memberId, ushort rank, sbyte experienceGivenPercent, uint rights)
        {
            this.MemberId = memberId;
            this.Rank = rank;
            this.ExperienceGivenPercent = experienceGivenPercent;
            this.Rights = rights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(MemberId);
            writer.WriteVarShort(Rank);
            writer.WriteSByte(ExperienceGivenPercent);
            writer.WriteVarInt(Rights);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
            Rank = reader.ReadVarUhShort();
            if (Rank < 0)
                throw new Exception("Forbidden value on Rank = " + Rank + ", it doesn't respect the following condition : rank < 0");
            ExperienceGivenPercent = reader.ReadSByte();
            if (ExperienceGivenPercent < 0 || ExperienceGivenPercent > 100)
                throw new Exception("Forbidden value on ExperienceGivenPercent = " + ExperienceGivenPercent + ", it doesn't respect the following condition : experienceGivenPercent < 0 || experienceGivenPercent > 100");
            Rights = reader.ReadVarUhInt();
            if (Rights < 0)
                throw new Exception("Forbidden value on Rights = " + Rights + ", it doesn't respect the following condition : rights < 0");
        }
        
    }
    
}