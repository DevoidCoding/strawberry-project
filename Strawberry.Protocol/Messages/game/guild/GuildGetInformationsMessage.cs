

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildGetInformationsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5550;
        public override uint MessageID => ProtocolId;
        
        public sbyte InfoType { get; set; }
        
        public GuildGetInformationsMessage()
        {
        }
        
        public GuildGetInformationsMessage(sbyte infoType)
        {
            this.InfoType = infoType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(InfoType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            InfoType = reader.ReadSByte();
            if (InfoType < 0)
                throw new Exception("Forbidden value on InfoType = " + InfoType + ", it doesn't respect the following condition : infoType < 0");
        }
        
    }
    
}