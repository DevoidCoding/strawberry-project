

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFactsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6415;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildFactSheetInformations Infos { get; set; }
        public int CreationDate { get; set; }
        public ushort NbTaxCollectors { get; set; }
        public Types.CharacterMinimalInformations[] Members { get; set; }
        
        public GuildFactsMessage()
        {
        }
        
        public GuildFactsMessage(Types.GuildFactSheetInformations infos, int creationDate, ushort nbTaxCollectors, Types.CharacterMinimalInformations[] members)
        {
            this.Infos = infos;
            this.CreationDate = creationDate;
            this.NbTaxCollectors = nbTaxCollectors;
            this.Members = members;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Infos.TypeID);
            Infos.Serialize(writer);
            writer.WriteInt(CreationDate);
            writer.WriteVarShort(NbTaxCollectors);
            writer.WriteUShort((ushort)Members.Length);
            foreach (var entry in Members)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Infos = Types.ProtocolTypeManager.GetInstance<Types.GuildFactSheetInformations>(reader.ReadShort());
            Infos.Deserialize(reader);
            CreationDate = reader.ReadInt();
            if (CreationDate < 0)
                throw new Exception("Forbidden value on CreationDate = " + CreationDate + ", it doesn't respect the following condition : creationDate < 0");
            NbTaxCollectors = reader.ReadVarUhShort();
            if (NbTaxCollectors < 0)
                throw new Exception("Forbidden value on NbTaxCollectors = " + NbTaxCollectors + ", it doesn't respect the following condition : nbTaxCollectors < 0");
            var limit = reader.ReadUShort();
            Members = new Types.CharacterMinimalInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Members[i] = new Types.CharacterMinimalInformations();
                 Members[i].Deserialize(reader);
            }
        }
        
    }
    
}