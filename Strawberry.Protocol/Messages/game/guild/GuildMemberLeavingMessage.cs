

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildMemberLeavingMessage : NetworkMessage
    {
        public const uint ProtocolId = 5923;
        public override uint MessageID => ProtocolId;
        
        public bool Kicked { get; set; }
        public ulong MemberId { get; set; }
        
        public GuildMemberLeavingMessage()
        {
        }
        
        public GuildMemberLeavingMessage(bool kicked, ulong memberId)
        {
            this.Kicked = kicked;
            this.MemberId = memberId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Kicked);
            writer.WriteVarLong(MemberId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Kicked = reader.ReadBoolean();
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
        }
        
    }
    
}