

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6413;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildInformations[] Guilds { get; set; }
        
        public GuildListMessage()
        {
        }
        
        public GuildListMessage(Types.GuildInformations[] guilds)
        {
            this.Guilds = guilds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Guilds.Length);
            foreach (var entry in Guilds)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Guilds = new Types.GuildInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Guilds[i] = new Types.GuildInformations();
                 Guilds[i].Deserialize(reader);
            }
        }
        
    }
    
}