

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildModificationNameValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6327;
        public override uint MessageID => ProtocolId;
        
        public string GuildName { get; set; }
        
        public GuildModificationNameValidMessage()
        {
        }
        
        public GuildModificationNameValidMessage(string guildName)
        {
            this.GuildName = guildName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(GuildName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildName = reader.ReadUTF();
        }
        
    }
    
}