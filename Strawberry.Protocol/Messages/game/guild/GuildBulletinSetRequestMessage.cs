

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildBulletinSetRequestMessage : SocialNoticeSetRequestMessage
    {
        public new const uint ProtocolId = 6694;
        public override uint MessageID => ProtocolId;
        
        public string Content { get; set; }
        public bool NotifyMembers { get; set; }
        
        public GuildBulletinSetRequestMessage()
        {
        }
        
        public GuildBulletinSetRequestMessage(string content, bool notifyMembers)
        {
            this.Content = content;
            this.NotifyMembers = notifyMembers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(Content);
            writer.WriteBoolean(NotifyMembers);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Content = reader.ReadUTF();
            NotifyMembers = reader.ReadBoolean();
        }
        
    }
    
}