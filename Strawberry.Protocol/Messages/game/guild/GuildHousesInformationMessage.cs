

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildHousesInformationMessage : NetworkMessage
    {
        public const uint ProtocolId = 5919;
        public override uint MessageID => ProtocolId;
        
        public Types.HouseInformationsForGuild[] HousesInformations { get; set; }
        
        public GuildHousesInformationMessage()
        {
        }
        
        public GuildHousesInformationMessage(Types.HouseInformationsForGuild[] housesInformations)
        {
            this.HousesInformations = housesInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)HousesInformations.Length);
            foreach (var entry in HousesInformations)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            HousesInformations = new Types.HouseInformationsForGuild[limit];
            for (int i = 0; i < limit; i++)
            {
                 HousesInformations[i] = new Types.HouseInformationsForGuild();
                 HousesInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}