

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildLevelUpMessage : NetworkMessage
    {
        public const uint ProtocolId = 6062;
        public override uint MessageID => ProtocolId;
        
        public byte NewLevel { get; set; }
        
        public GuildLevelUpMessage()
        {
        }
        
        public GuildLevelUpMessage(byte newLevel)
        {
            this.NewLevel = newLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(NewLevel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NewLevel = reader.ReadByte();
            if (NewLevel < 2 || NewLevel > 200)
                throw new Exception("Forbidden value on NewLevel = " + NewLevel + ", it doesn't respect the following condition : newLevel < 2 || newLevel > 200");
        }
        
    }
    
}