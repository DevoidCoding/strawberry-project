

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildCreationResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5554;
        public override uint MessageID => ProtocolId;
        
        public sbyte Result { get; set; }
        
        public GuildCreationResultMessage()
        {
        }
        
        public GuildCreationResultMessage(sbyte result)
        {
            this.Result = result;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Result);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Result = reader.ReadSByte();
            if (Result < 0)
                throw new Exception("Forbidden value on Result = " + Result + ", it doesn't respect the following condition : result < 0");
        }
        
    }
    
}