

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildPaddockBoughtMessage : NetworkMessage
    {
        public const uint ProtocolId = 5952;
        public override uint MessageID => ProtocolId;
        
        public Types.PaddockContentInformations PaddockInfo { get; set; }
        
        public GuildPaddockBoughtMessage()
        {
        }
        
        public GuildPaddockBoughtMessage(Types.PaddockContentInformations paddockInfo)
        {
            this.PaddockInfo = paddockInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            PaddockInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PaddockInfo = new Types.PaddockContentInformations();
            PaddockInfo.Deserialize(reader);
        }
        
    }
    
}