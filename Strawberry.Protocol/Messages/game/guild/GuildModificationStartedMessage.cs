

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildModificationStartedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6324;
        public override uint MessageID => ProtocolId;
        
        public bool CanChangeName { get; set; }
        public bool CanChangeEmblem { get; set; }
        
        public GuildModificationStartedMessage()
        {
        }
        
        public GuildModificationStartedMessage(bool canChangeName, bool canChangeEmblem)
        {
            this.CanChangeName = canChangeName;
            this.CanChangeEmblem = canChangeEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, CanChangeName);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CanChangeEmblem);
            writer.WriteByte(flag1);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            CanChangeName = BooleanByteWrapper.GetFlag(flag1, 0);
            CanChangeEmblem = BooleanByteWrapper.GetFlag(flag1, 1);
        }
        
    }
    
}