

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInvitationStateRecrutedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5548;
        public override uint MessageID => ProtocolId;
        
        public sbyte InvitationState { get; set; }
        
        public GuildInvitationStateRecrutedMessage()
        {
        }
        
        public GuildInvitationStateRecrutedMessage(sbyte invitationState)
        {
            this.InvitationState = invitationState;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(InvitationState);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            InvitationState = reader.ReadSByte();
            if (InvitationState < 0)
                throw new Exception("Forbidden value on InvitationState = " + InvitationState + ", it doesn't respect the following condition : invitationState < 0");
        }
        
    }
    
}