

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFactsRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6404;
        public override uint MessageID => ProtocolId;
        
        public uint GuildId { get; set; }
        
        public GuildFactsRequestMessage()
        {
        }
        
        public GuildFactsRequestMessage(uint guildId)
        {
            this.GuildId = guildId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(GuildId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildId = reader.ReadVarUhInt();
            if (GuildId < 0)
                throw new Exception("Forbidden value on GuildId = " + GuildId + ", it doesn't respect the following condition : guildId < 0");
        }
        
    }
    
}