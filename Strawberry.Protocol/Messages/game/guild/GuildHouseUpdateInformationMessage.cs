

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildHouseUpdateInformationMessage : NetworkMessage
    {
        public const uint ProtocolId = 6181;
        public override uint MessageID => ProtocolId;
        
        public Types.HouseInformationsForGuild HousesInformations { get; set; }
        
        public GuildHouseUpdateInformationMessage()
        {
        }
        
        public GuildHouseUpdateInformationMessage(Types.HouseInformationsForGuild housesInformations)
        {
            this.HousesInformations = housesInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            HousesInformations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HousesInformations = new Types.HouseInformationsForGuild();
            HousesInformations.Deserialize(reader);
        }
        
    }
    
}