

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildKickRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5887;
        public override uint MessageID => ProtocolId;
        
        public ulong KickedId { get; set; }
        
        public GuildKickRequestMessage()
        {
        }
        
        public GuildKickRequestMessage(ulong kickedId)
        {
            this.KickedId = kickedId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(KickedId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            KickedId = reader.ReadVarUhLong();
            if (KickedId < 0 || KickedId > 9007199254740990)
                throw new Exception("Forbidden value on KickedId = " + KickedId + ", it doesn't respect the following condition : kickedId < 0 || kickedId > 9007199254740990");
        }
        
    }
    
}