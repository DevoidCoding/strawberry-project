

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildPaddockRemovedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5955;
        public override uint MessageID => ProtocolId;
        
        public double PaddockId { get; set; }
        
        public GuildPaddockRemovedMessage()
        {
        }
        
        public GuildPaddockRemovedMessage(double paddockId)
        {
            this.PaddockId = paddockId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(PaddockId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PaddockId = reader.ReadDouble();
            if (PaddockId < 0 || PaddockId > 9007199254740990)
                throw new Exception("Forbidden value on PaddockId = " + PaddockId + ", it doesn't respect the following condition : paddockId < 0 || paddockId > 9007199254740990");
        }
        
    }
    
}