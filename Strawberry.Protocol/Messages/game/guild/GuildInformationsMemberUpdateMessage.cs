

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInformationsMemberUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5597;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildMember Member { get; set; }
        
        public GuildInformationsMemberUpdateMessage()
        {
        }
        
        public GuildInformationsMemberUpdateMessage(Types.GuildMember member)
        {
            this.Member = member;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Member.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Member = new Types.GuildMember();
            Member.Deserialize(reader);
        }
        
    }
    
}