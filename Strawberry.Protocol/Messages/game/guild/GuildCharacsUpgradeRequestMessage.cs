

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildCharacsUpgradeRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5706;
        public override uint MessageID => ProtocolId;
        
        public sbyte CharaTypeTarget { get; set; }
        
        public GuildCharacsUpgradeRequestMessage()
        {
        }
        
        public GuildCharacsUpgradeRequestMessage(sbyte charaTypeTarget)
        {
            this.CharaTypeTarget = charaTypeTarget;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(CharaTypeTarget);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CharaTypeTarget = reader.ReadSByte();
            if (CharaTypeTarget < 0)
                throw new Exception("Forbidden value on CharaTypeTarget = " + CharaTypeTarget + ", it doesn't respect the following condition : charaTypeTarget < 0");
        }
        
    }
    
}