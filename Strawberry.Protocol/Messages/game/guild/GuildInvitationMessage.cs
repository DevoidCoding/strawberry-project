

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInvitationMessage : NetworkMessage
    {
        public const uint ProtocolId = 5551;
        public override uint MessageID => ProtocolId;
        
        public ulong TargetId { get; set; }
        
        public GuildInvitationMessage()
        {
        }
        
        public GuildInvitationMessage(ulong targetId)
        {
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TargetId = reader.ReadVarUhLong();
            if (TargetId < 0 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < 0 || targetId > 9007199254740990");
        }
        
    }
    
}