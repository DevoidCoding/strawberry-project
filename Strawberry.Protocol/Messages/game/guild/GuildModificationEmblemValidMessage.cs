

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildModificationEmblemValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6328;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildEmblem GuildEmblem { get; set; }
        
        public GuildModificationEmblemValidMessage()
        {
        }
        
        public GuildModificationEmblemValidMessage(Types.GuildEmblem guildEmblem)
        {
            this.GuildEmblem = guildEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            GuildEmblem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildEmblem = new Types.GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
        
    }
    
}