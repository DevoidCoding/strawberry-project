

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInfosUpgradeMessage : NetworkMessage
    {
        public const uint ProtocolId = 5636;
        public override uint MessageID => ProtocolId;
        
        public sbyte MaxTaxCollectorsCount { get; set; }
        public sbyte TaxCollectorsCount { get; set; }
        public ushort TaxCollectorLifePoints { get; set; }
        public ushort TaxCollectorDamagesBonuses { get; set; }
        public ushort TaxCollectorPods { get; set; }
        public ushort TaxCollectorProspecting { get; set; }
        public ushort TaxCollectorWisdom { get; set; }
        public ushort BoostPoints { get; set; }
        public ushort[] SpellId { get; set; }
        public short[] SpellLevel { get; set; }
        
        public GuildInfosUpgradeMessage()
        {
        }
        
        public GuildInfosUpgradeMessage(sbyte maxTaxCollectorsCount, sbyte taxCollectorsCount, ushort taxCollectorLifePoints, ushort taxCollectorDamagesBonuses, ushort taxCollectorPods, ushort taxCollectorProspecting, ushort taxCollectorWisdom, ushort boostPoints, ushort[] spellId, short[] spellLevel)
        {
            this.MaxTaxCollectorsCount = maxTaxCollectorsCount;
            this.TaxCollectorsCount = taxCollectorsCount;
            this.TaxCollectorLifePoints = taxCollectorLifePoints;
            this.TaxCollectorDamagesBonuses = taxCollectorDamagesBonuses;
            this.TaxCollectorPods = taxCollectorPods;
            this.TaxCollectorProspecting = taxCollectorProspecting;
            this.TaxCollectorWisdom = taxCollectorWisdom;
            this.BoostPoints = boostPoints;
            this.SpellId = spellId;
            this.SpellLevel = spellLevel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(MaxTaxCollectorsCount);
            writer.WriteSByte(TaxCollectorsCount);
            writer.WriteVarShort(TaxCollectorLifePoints);
            writer.WriteVarShort(TaxCollectorDamagesBonuses);
            writer.WriteVarShort(TaxCollectorPods);
            writer.WriteVarShort(TaxCollectorProspecting);
            writer.WriteVarShort(TaxCollectorWisdom);
            writer.WriteVarShort(BoostPoints);
            writer.WriteUShort((ushort)SpellId.Length);
            foreach (var entry in SpellId)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)SpellLevel.Length);
            foreach (var entry in SpellLevel)
            {
                 writer.WriteShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MaxTaxCollectorsCount = reader.ReadSByte();
            if (MaxTaxCollectorsCount < 0)
                throw new Exception("Forbidden value on MaxTaxCollectorsCount = " + MaxTaxCollectorsCount + ", it doesn't respect the following condition : maxTaxCollectorsCount < 0");
            TaxCollectorsCount = reader.ReadSByte();
            if (TaxCollectorsCount < 0)
                throw new Exception("Forbidden value on TaxCollectorsCount = " + TaxCollectorsCount + ", it doesn't respect the following condition : taxCollectorsCount < 0");
            TaxCollectorLifePoints = reader.ReadVarUhShort();
            if (TaxCollectorLifePoints < 0)
                throw new Exception("Forbidden value on TaxCollectorLifePoints = " + TaxCollectorLifePoints + ", it doesn't respect the following condition : taxCollectorLifePoints < 0");
            TaxCollectorDamagesBonuses = reader.ReadVarUhShort();
            if (TaxCollectorDamagesBonuses < 0)
                throw new Exception("Forbidden value on TaxCollectorDamagesBonuses = " + TaxCollectorDamagesBonuses + ", it doesn't respect the following condition : taxCollectorDamagesBonuses < 0");
            TaxCollectorPods = reader.ReadVarUhShort();
            if (TaxCollectorPods < 0)
                throw new Exception("Forbidden value on TaxCollectorPods = " + TaxCollectorPods + ", it doesn't respect the following condition : taxCollectorPods < 0");
            TaxCollectorProspecting = reader.ReadVarUhShort();
            if (TaxCollectorProspecting < 0)
                throw new Exception("Forbidden value on TaxCollectorProspecting = " + TaxCollectorProspecting + ", it doesn't respect the following condition : taxCollectorProspecting < 0");
            TaxCollectorWisdom = reader.ReadVarUhShort();
            if (TaxCollectorWisdom < 0)
                throw new Exception("Forbidden value on TaxCollectorWisdom = " + TaxCollectorWisdom + ", it doesn't respect the following condition : taxCollectorWisdom < 0");
            BoostPoints = reader.ReadVarUhShort();
            if (BoostPoints < 0)
                throw new Exception("Forbidden value on BoostPoints = " + BoostPoints + ", it doesn't respect the following condition : boostPoints < 0");
            var limit = reader.ReadUShort();
            SpellId = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 SpellId[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            SpellLevel = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 SpellLevel[i] = reader.ReadShort();
            }
        }
        
    }
    
}