

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildModificationValidMessage : NetworkMessage
    {
        public const uint ProtocolId = 6323;
        public override uint MessageID => ProtocolId;
        
        public string GuildName { get; set; }
        public Types.GuildEmblem GuildEmblem { get; set; }
        
        public GuildModificationValidMessage()
        {
        }
        
        public GuildModificationValidMessage(string guildName, Types.GuildEmblem guildEmblem)
        {
            this.GuildName = guildName;
            this.GuildEmblem = guildEmblem;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(GuildName);
            GuildEmblem.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildName = reader.ReadUTF();
            GuildEmblem = new Types.GuildEmblem();
            GuildEmblem.Deserialize(reader);
        }
        
    }
    
}