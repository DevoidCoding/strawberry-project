

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildJoinedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5564;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildInformations GuildInfo { get; set; }
        public uint MemberRights { get; set; }
        
        public GuildJoinedMessage()
        {
        }
        
        public GuildJoinedMessage(Types.GuildInformations guildInfo, uint memberRights)
        {
            this.GuildInfo = guildInfo;
            this.MemberRights = memberRights;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            GuildInfo.Serialize(writer);
            writer.WriteVarInt(MemberRights);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            GuildInfo = new Types.GuildInformations();
            GuildInfo.Deserialize(reader);
            MemberRights = reader.ReadVarUhInt();
            if (MemberRights < 0)
                throw new Exception("Forbidden value on MemberRights = " + MemberRights + ", it doesn't respect the following condition : memberRights < 0");
        }
        
    }
    
}