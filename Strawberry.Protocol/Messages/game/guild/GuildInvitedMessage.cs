

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInvitedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5552;
        public override uint MessageID => ProtocolId;
        
        public ulong RecruterId { get; set; }
        public string RecruterName { get; set; }
        public Types.BasicGuildInformations GuildInfo { get; set; }
        
        public GuildInvitedMessage()
        {
        }
        
        public GuildInvitedMessage(ulong recruterId, string recruterName, Types.BasicGuildInformations guildInfo)
        {
            this.RecruterId = recruterId;
            this.RecruterName = recruterName;
            this.GuildInfo = guildInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(RecruterId);
            writer.WriteUTF(RecruterName);
            GuildInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RecruterId = reader.ReadVarUhLong();
            if (RecruterId < 0 || RecruterId > 9007199254740990)
                throw new Exception("Forbidden value on RecruterId = " + RecruterId + ", it doesn't respect the following condition : recruterId < 0 || recruterId > 9007199254740990");
            RecruterName = reader.ReadUTF();
            GuildInfo = new Types.BasicGuildInformations();
            GuildInfo.Deserialize(reader);
        }
        
    }
    
}