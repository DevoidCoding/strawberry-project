

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildMemberOnlineStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6061;
        public override uint MessageID => ProtocolId;
        
        public ulong MemberId { get; set; }
        public bool Online { get; set; }
        
        public GuildMemberOnlineStatusMessage()
        {
        }
        
        public GuildMemberOnlineStatusMessage(ulong memberId, bool online)
        {
            this.MemberId = memberId;
            this.Online = online;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(MemberId);
            writer.WriteBoolean(Online);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
            Online = reader.ReadBoolean();
        }
        
    }
    
}