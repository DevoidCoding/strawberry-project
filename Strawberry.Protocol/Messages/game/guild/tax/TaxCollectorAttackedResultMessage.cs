

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorAttackedResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5635;
        public override uint MessageID => ProtocolId;
        
        public bool DeadOrAlive { get; set; }
        public Types.TaxCollectorBasicInformations BasicInfos { get; set; }
        public Types.BasicGuildInformations Guild { get; set; }
        
        public TaxCollectorAttackedResultMessage()
        {
        }
        
        public TaxCollectorAttackedResultMessage(bool deadOrAlive, Types.TaxCollectorBasicInformations basicInfos, Types.BasicGuildInformations guild)
        {
            this.DeadOrAlive = deadOrAlive;
            this.BasicInfos = basicInfos;
            this.Guild = guild;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(DeadOrAlive);
            BasicInfos.Serialize(writer);
            Guild.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DeadOrAlive = reader.ReadBoolean();
            BasicInfos = new Types.TaxCollectorBasicInformations();
            BasicInfos.Deserialize(reader);
            Guild = new Types.BasicGuildInformations();
            Guild.Deserialize(reader);
        }
        
    }
    
}