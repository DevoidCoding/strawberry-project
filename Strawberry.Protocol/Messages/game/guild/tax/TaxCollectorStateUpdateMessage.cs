

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorStateUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6455;
        public override uint MessageID => ProtocolId;
        
        public double UniqueId { get; set; }
        public sbyte State { get; set; }
        
        public TaxCollectorStateUpdateMessage()
        {
        }
        
        public TaxCollectorStateUpdateMessage(double uniqueId, sbyte state)
        {
            this.UniqueId = uniqueId;
            this.State = state;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(UniqueId);
            writer.WriteSByte(State);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            UniqueId = reader.ReadDouble();
            if (UniqueId < 0 || UniqueId > 9007199254740990)
                throw new Exception("Forbidden value on UniqueId = " + UniqueId + ", it doesn't respect the following condition : uniqueId < 0 || uniqueId > 9007199254740990");
            State = reader.ReadSByte();
        }
        
    }
    
}