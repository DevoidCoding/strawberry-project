

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorListMessage : AbstractTaxCollectorListMessage
    {
        public new const uint ProtocolId = 5930;
        public override uint MessageID => ProtocolId;
        
        public sbyte NbcollectorMax { get; set; }
        public Types.TaxCollectorFightersInformation[] FightersInformations { get; set; }
        
        public TaxCollectorListMessage()
        {
        }
        
        public TaxCollectorListMessage(Types.TaxCollectorInformations[] informations, sbyte nbcollectorMax, Types.TaxCollectorFightersInformation[] fightersInformations)
         : base(informations)
        {
            this.NbcollectorMax = nbcollectorMax;
            this.FightersInformations = fightersInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(NbcollectorMax);
            writer.WriteUShort((ushort)FightersInformations.Length);
            foreach (var entry in FightersInformations)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            NbcollectorMax = reader.ReadSByte();
            if (NbcollectorMax < 0)
                throw new Exception("Forbidden value on NbcollectorMax = " + NbcollectorMax + ", it doesn't respect the following condition : nbcollectorMax < 0");
            var limit = reader.ReadUShort();
            FightersInformations = new Types.TaxCollectorFightersInformation[limit];
            for (int i = 0; i < limit; i++)
            {
                 FightersInformations[i] = new Types.TaxCollectorFightersInformation();
                 FightersInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}