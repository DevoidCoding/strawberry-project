

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFightLeaveRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5715;
        public override uint MessageID => ProtocolId;
        
        public double TaxCollectorId { get; set; }
        public ulong CharacterId { get; set; }
        
        public GuildFightLeaveRequestMessage()
        {
        }
        
        public GuildFightLeaveRequestMessage(double taxCollectorId, ulong characterId)
        {
            this.TaxCollectorId = taxCollectorId;
            this.CharacterId = characterId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(TaxCollectorId);
            writer.WriteVarLong(CharacterId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TaxCollectorId = reader.ReadDouble();
            if (TaxCollectorId < 0 || TaxCollectorId > 9007199254740990)
                throw new Exception("Forbidden value on TaxCollectorId = " + TaxCollectorId + ", it doesn't respect the following condition : taxCollectorId < 0 || taxCollectorId > 9007199254740990");
            CharacterId = reader.ReadVarUhLong();
            if (CharacterId < 0 || CharacterId > 9007199254740990)
                throw new Exception("Forbidden value on CharacterId = " + CharacterId + ", it doesn't respect the following condition : characterId < 0 || characterId > 9007199254740990");
        }
        
    }
    
}