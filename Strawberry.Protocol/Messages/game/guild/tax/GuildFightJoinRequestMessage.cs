

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFightJoinRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5717;
        public override uint MessageID => ProtocolId;
        
        public double TaxCollectorId { get; set; }
        
        public GuildFightJoinRequestMessage()
        {
        }
        
        public GuildFightJoinRequestMessage(double taxCollectorId)
        {
            this.TaxCollectorId = taxCollectorId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(TaxCollectorId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TaxCollectorId = reader.ReadDouble();
            if (TaxCollectorId < 0 || TaxCollectorId > 9007199254740990)
                throw new Exception("Forbidden value on TaxCollectorId = " + TaxCollectorId + ", it doesn't respect the following condition : taxCollectorId < 0 || taxCollectorId > 9007199254740990");
        }
        
    }
    
}