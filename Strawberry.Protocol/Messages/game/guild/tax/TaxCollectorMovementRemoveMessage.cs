

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorMovementRemoveMessage : NetworkMessage
    {
        public const uint ProtocolId = 5915;
        public override uint MessageID => ProtocolId;
        
        public double CollectorId { get; set; }
        
        public TaxCollectorMovementRemoveMessage()
        {
        }
        
        public TaxCollectorMovementRemoveMessage(double collectorId)
        {
            this.CollectorId = collectorId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(CollectorId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CollectorId = reader.ReadDouble();
            if (CollectorId < 0 || CollectorId > 9007199254740990)
                throw new Exception("Forbidden value on CollectorId = " + CollectorId + ", it doesn't respect the following condition : collectorId < 0 || collectorId > 9007199254740990");
        }
        
    }
    
}