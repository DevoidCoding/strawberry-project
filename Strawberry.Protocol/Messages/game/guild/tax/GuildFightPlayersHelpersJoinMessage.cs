

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFightPlayersHelpersJoinMessage : NetworkMessage
    {
        public const uint ProtocolId = 5720;
        public override uint MessageID => ProtocolId;
        
        public double FightId { get; set; }
        public Types.CharacterMinimalPlusLookInformations PlayerInfo { get; set; }
        
        public GuildFightPlayersHelpersJoinMessage()
        {
        }
        
        public GuildFightPlayersHelpersJoinMessage(double fightId, Types.CharacterMinimalPlusLookInformations playerInfo)
        {
            this.FightId = fightId;
            this.PlayerInfo = playerInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(FightId);
            PlayerInfo.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadDouble();
            if (FightId < 0 || FightId > 9007199254740990)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0 || fightId > 9007199254740990");
            PlayerInfo = new Types.CharacterMinimalPlusLookInformations();
            PlayerInfo.Deserialize(reader);
        }
        
    }
    
}