

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorMovementAddMessage : NetworkMessage
    {
        public const uint ProtocolId = 5917;
        public override uint MessageID => ProtocolId;
        
        public Types.TaxCollectorInformations Informations { get; set; }
        
        public TaxCollectorMovementAddMessage()
        {
        }
        
        public TaxCollectorMovementAddMessage(Types.TaxCollectorInformations informations)
        {
            this.Informations = informations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Informations.TypeID);
            Informations.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Informations = Types.ProtocolTypeManager.GetInstance<Types.TaxCollectorInformations>(reader.ReadShort());
            Informations.Deserialize(reader);
        }
        
    }
    
}