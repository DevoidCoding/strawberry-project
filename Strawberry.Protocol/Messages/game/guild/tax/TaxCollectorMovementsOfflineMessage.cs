

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorMovementsOfflineMessage : NetworkMessage
    {
        public const uint ProtocolId = 6611;
        public override uint MessageID => ProtocolId;
        
        public Types.TaxCollectorMovement[] Movements { get; set; }
        
        public TaxCollectorMovementsOfflineMessage()
        {
        }
        
        public TaxCollectorMovementsOfflineMessage(Types.TaxCollectorMovement[] movements)
        {
            this.Movements = movements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Movements.Length);
            foreach (var entry in Movements)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Movements = new Types.TaxCollectorMovement[limit];
            for (int i = 0; i < limit; i++)
            {
                 Movements[i] = new Types.TaxCollectorMovement();
                 Movements[i].Deserialize(reader);
            }
        }
        
    }
    
}