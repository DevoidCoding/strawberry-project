

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildFightPlayersEnemiesListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5928;
        public override uint MessageID => ProtocolId;
        
        public double FightId { get; set; }
        public Types.CharacterMinimalPlusLookInformations[] PlayerInfo { get; set; }
        
        public GuildFightPlayersEnemiesListMessage()
        {
        }
        
        public GuildFightPlayersEnemiesListMessage(double fightId, Types.CharacterMinimalPlusLookInformations[] playerInfo)
        {
            this.FightId = fightId;
            this.PlayerInfo = playerInfo;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(FightId);
            writer.WriteUShort((ushort)PlayerInfo.Length);
            foreach (var entry in PlayerInfo)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FightId = reader.ReadDouble();
            if (FightId < 0 || FightId > 9007199254740990)
                throw new Exception("Forbidden value on FightId = " + FightId + ", it doesn't respect the following condition : fightId < 0 || fightId > 9007199254740990");
            var limit = reader.ReadUShort();
            PlayerInfo = new Types.CharacterMinimalPlusLookInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 PlayerInfo[i] = new Types.CharacterMinimalPlusLookInformations();
                 PlayerInfo[i].Deserialize(reader);
            }
        }
        
    }
    
}