

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TaxCollectorMovementMessage : NetworkMessage
    {
        public const uint ProtocolId = 5633;
        public override uint MessageID => ProtocolId;
        
        public sbyte MovementType { get; set; }
        public Types.TaxCollectorBasicInformations BasicInfos { get; set; }
        public ulong PlayerId { get; set; }
        public string PlayerName { get; set; }
        
        public TaxCollectorMovementMessage()
        {
        }
        
        public TaxCollectorMovementMessage(sbyte movementType, Types.TaxCollectorBasicInformations basicInfos, ulong playerId, string playerName)
        {
            this.MovementType = movementType;
            this.BasicInfos = basicInfos;
            this.PlayerId = playerId;
            this.PlayerName = playerName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(MovementType);
            BasicInfos.Serialize(writer);
            writer.WriteVarLong(PlayerId);
            writer.WriteUTF(PlayerName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MovementType = reader.ReadSByte();
            if (MovementType < 0)
                throw new Exception("Forbidden value on MovementType = " + MovementType + ", it doesn't respect the following condition : movementType < 0");
            BasicInfos = new Types.TaxCollectorBasicInformations();
            BasicInfos.Deserialize(reader);
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            PlayerName = reader.ReadUTF();
        }
        
    }
    
}