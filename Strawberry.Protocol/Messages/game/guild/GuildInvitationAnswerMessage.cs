

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInvitationAnswerMessage : NetworkMessage
    {
        public const uint ProtocolId = 5556;
        public override uint MessageID => ProtocolId;
        
        public bool Accept { get; set; }
        
        public GuildInvitationAnswerMessage()
        {
        }
        
        public GuildInvitationAnswerMessage(bool accept)
        {
            this.Accept = accept;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Accept);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Accept = reader.ReadBoolean();
        }
        
    }
    
}