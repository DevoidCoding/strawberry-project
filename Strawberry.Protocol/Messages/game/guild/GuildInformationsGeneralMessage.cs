

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInformationsGeneralMessage : NetworkMessage
    {
        public const uint ProtocolId = 5557;
        public override uint MessageID => ProtocolId;
        
        public bool AbandonnedPaddock { get; set; }
        public byte Level { get; set; }
        public ulong ExpLevelFloor { get; set; }
        public ulong Experience { get; set; }
        public ulong ExpNextLevelFloor { get; set; }
        public int CreationDate { get; set; }
        public ushort NbTotalMembers { get; set; }
        public ushort NbConnectedMembers { get; set; }
        
        public GuildInformationsGeneralMessage()
        {
        }
        
        public GuildInformationsGeneralMessage(bool abandonnedPaddock, byte level, ulong expLevelFloor, ulong experience, ulong expNextLevelFloor, int creationDate, ushort nbTotalMembers, ushort nbConnectedMembers)
        {
            this.AbandonnedPaddock = abandonnedPaddock;
            this.Level = level;
            this.ExpLevelFloor = expLevelFloor;
            this.Experience = experience;
            this.ExpNextLevelFloor = expNextLevelFloor;
            this.CreationDate = creationDate;
            this.NbTotalMembers = nbTotalMembers;
            this.NbConnectedMembers = nbConnectedMembers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(AbandonnedPaddock);
            writer.WriteByte(Level);
            writer.WriteVarLong(ExpLevelFloor);
            writer.WriteVarLong(Experience);
            writer.WriteVarLong(ExpNextLevelFloor);
            writer.WriteInt(CreationDate);
            writer.WriteVarShort(NbTotalMembers);
            writer.WriteVarShort(NbConnectedMembers);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AbandonnedPaddock = reader.ReadBoolean();
            Level = reader.ReadByte();
            if (Level < 0 || Level > 255)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0 || level > 255");
            ExpLevelFloor = reader.ReadVarUhLong();
            if (ExpLevelFloor < 0 || ExpLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on ExpLevelFloor = " + ExpLevelFloor + ", it doesn't respect the following condition : expLevelFloor < 0 || expLevelFloor > 9007199254740990");
            Experience = reader.ReadVarUhLong();
            if (Experience < 0 || Experience > 9007199254740990)
                throw new Exception("Forbidden value on Experience = " + Experience + ", it doesn't respect the following condition : experience < 0 || experience > 9007199254740990");
            ExpNextLevelFloor = reader.ReadVarUhLong();
            if (ExpNextLevelFloor < 0 || ExpNextLevelFloor > 9007199254740990)
                throw new Exception("Forbidden value on ExpNextLevelFloor = " + ExpNextLevelFloor + ", it doesn't respect the following condition : expNextLevelFloor < 0 || expNextLevelFloor > 9007199254740990");
            CreationDate = reader.ReadInt();
            if (CreationDate < 0)
                throw new Exception("Forbidden value on CreationDate = " + CreationDate + ", it doesn't respect the following condition : creationDate < 0");
            NbTotalMembers = reader.ReadVarUhShort();
            if (NbTotalMembers < 0)
                throw new Exception("Forbidden value on NbTotalMembers = " + NbTotalMembers + ", it doesn't respect the following condition : nbTotalMembers < 0");
            NbConnectedMembers = reader.ReadVarUhShort();
            if (NbConnectedMembers < 0)
                throw new Exception("Forbidden value on NbConnectedMembers = " + NbConnectedMembers + ", it doesn't respect the following condition : nbConnectedMembers < 0");
        }
        
    }
    
}