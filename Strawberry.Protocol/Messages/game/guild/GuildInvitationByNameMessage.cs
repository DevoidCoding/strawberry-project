

// Generated on 02/12/2018 03:56:34
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInvitationByNameMessage : NetworkMessage
    {
        public const uint ProtocolId = 6115;
        public override uint MessageID => ProtocolId;
        
        public string Name { get; set; }
        
        public GuildInvitationByNameMessage()
        {
        }
        
        public GuildInvitationByNameMessage(string name)
        {
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Name = reader.ReadUTF();
        }
        
    }
    
}