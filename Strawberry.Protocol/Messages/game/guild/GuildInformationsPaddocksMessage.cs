

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInformationsPaddocksMessage : NetworkMessage
    {
        public const uint ProtocolId = 5959;
        public override uint MessageID => ProtocolId;
        
        public sbyte NbPaddockMax { get; set; }
        public Types.PaddockContentInformations[] PaddocksInformations { get; set; }
        
        public GuildInformationsPaddocksMessage()
        {
        }
        
        public GuildInformationsPaddocksMessage(sbyte nbPaddockMax, Types.PaddockContentInformations[] paddocksInformations)
        {
            this.NbPaddockMax = nbPaddockMax;
            this.PaddocksInformations = paddocksInformations;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(NbPaddockMax);
            writer.WriteUShort((ushort)PaddocksInformations.Length);
            foreach (var entry in PaddocksInformations)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            NbPaddockMax = reader.ReadSByte();
            if (NbPaddockMax < 0)
                throw new Exception("Forbidden value on NbPaddockMax = " + NbPaddockMax + ", it doesn't respect the following condition : nbPaddockMax < 0");
            var limit = reader.ReadUShort();
            PaddocksInformations = new Types.PaddockContentInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 PaddocksInformations[i] = new Types.PaddockContentInformations();
                 PaddocksInformations[i].Deserialize(reader);
            }
        }
        
    }
    
}