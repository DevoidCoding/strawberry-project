

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GuildInformationsMembersMessage : NetworkMessage
    {
        public const uint ProtocolId = 5558;
        public override uint MessageID => ProtocolId;
        
        public Types.GuildMember[] Members { get; set; }
        
        public GuildInformationsMembersMessage()
        {
        }
        
        public GuildInformationsMembersMessage(Types.GuildMember[] members)
        {
            this.Members = members;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Members.Length);
            foreach (var entry in Members)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Members = new Types.GuildMember[limit];
            for (int i = 0; i < limit; i++)
            {
                 Members[i] = new Types.GuildMember();
                 Members[i].Deserialize(reader);
            }
        }
        
    }
    
}