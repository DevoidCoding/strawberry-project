

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class UpdateSelfAgressableStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6456;
        public override uint MessageID => ProtocolId;
        
        public sbyte Status { get; set; }
        public int ProbationTime { get; set; }
        
        public UpdateSelfAgressableStatusMessage()
        {
        }
        
        public UpdateSelfAgressableStatusMessage(sbyte status, int probationTime)
        {
            this.Status = status;
            this.ProbationTime = probationTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Status);
            writer.WriteInt(ProbationTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Status = reader.ReadSByte();
            if (Status < 0)
                throw new Exception("Forbidden value on Status = " + Status + ", it doesn't respect the following condition : status < 0");
            ProbationTime = reader.ReadInt();
            if (ProbationTime < 0)
                throw new Exception("Forbidden value on ProbationTime = " + ProbationTime + ", it doesn't respect the following condition : probationTime < 0");
        }
        
    }
    
}