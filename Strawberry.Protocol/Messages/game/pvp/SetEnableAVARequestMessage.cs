

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SetEnableAVARequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6443;
        public override uint MessageID => ProtocolId;
        
        public bool Enable { get; set; }
        
        public SetEnableAVARequestMessage()
        {
        }
        
        public SetEnableAVARequestMessage(bool enable)
        {
            this.Enable = enable;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Enable);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Enable = reader.ReadBoolean();
        }
        
    }
    
}