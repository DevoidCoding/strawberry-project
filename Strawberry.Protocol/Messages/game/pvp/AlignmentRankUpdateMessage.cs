

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AlignmentRankUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6058;
        public override uint MessageID => ProtocolId;
        
        public sbyte AlignmentRank { get; set; }
        public bool Verbose { get; set; }
        
        public AlignmentRankUpdateMessage()
        {
        }
        
        public AlignmentRankUpdateMessage(sbyte alignmentRank, bool verbose)
        {
            this.AlignmentRank = alignmentRank;
            this.Verbose = verbose;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(AlignmentRank);
            writer.WriteBoolean(Verbose);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AlignmentRank = reader.ReadSByte();
            if (AlignmentRank < 0)
                throw new Exception("Forbidden value on AlignmentRank = " + AlignmentRank + ", it doesn't respect the following condition : alignmentRank < 0");
            Verbose = reader.ReadBoolean();
        }
        
    }
    
}