

// Generated on 02/12/2018 03:56:45
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class UpdateMapPlayersAgressableStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6454;
        public override uint MessageID => ProtocolId;
        
        public ulong[] PlayerIds { get; set; }
        public sbyte[] Enable { get; set; }
        
        public UpdateMapPlayersAgressableStatusMessage()
        {
        }
        
        public UpdateMapPlayersAgressableStatusMessage(ulong[] playerIds, sbyte[] enable)
        {
            this.PlayerIds = playerIds;
            this.Enable = enable;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)PlayerIds.Length);
            foreach (var entry in PlayerIds)
            {
                 writer.WriteVarLong(entry);
            }
            writer.WriteUShort((ushort)Enable.Length);
            foreach (var entry in Enable)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            PlayerIds = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 PlayerIds[i] = reader.ReadVarUhLong();
            }
            limit = reader.ReadUShort();
            Enable = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Enable[i] = reader.ReadSByte();
            }
        }
        
    }
    
}