

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ReloginTokenStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6539;
        public override uint MessageID => ProtocolId;
        
        public bool ValidToken { get; set; }
        public sbyte[] Ticket { get; set; }
        
        public ReloginTokenStatusMessage()
        {
        }
        
        public ReloginTokenStatusMessage(bool validToken, sbyte[] ticket)
        {
            this.ValidToken = validToken;
            this.Ticket = ticket;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(ValidToken);
            writer.WriteVarInt((int)Ticket.Length);
            foreach (var entry in Ticket)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ValidToken = reader.ReadBoolean();
            var limit = reader.ReadVarInt();
            Ticket = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ticket[i] = reader.ReadSByte();
            }
        }
        
    }
    
}