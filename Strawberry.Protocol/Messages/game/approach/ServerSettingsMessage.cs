

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerSettingsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6340;
        public override uint MessageID => ProtocolId;
        
        public string Lang { get; set; }
        public sbyte Community { get; set; }
        public sbyte GameType { get; set; }
        public ushort ArenaLeaveBanTime { get; set; }
        
        public ServerSettingsMessage()
        {
        }
        
        public ServerSettingsMessage(string lang, sbyte community, sbyte gameType, ushort arenaLeaveBanTime)
        {
            this.Lang = lang;
            this.Community = community;
            this.GameType = gameType;
            this.ArenaLeaveBanTime = arenaLeaveBanTime;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Lang);
            writer.WriteSByte(Community);
            writer.WriteSByte(GameType);
            writer.WriteVarShort(ArenaLeaveBanTime);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Lang = reader.ReadUTF();
            Community = reader.ReadSByte();
            if (Community < 0)
                throw new Exception("Forbidden value on Community = " + Community + ", it doesn't respect the following condition : community < 0");
            GameType = reader.ReadSByte();
            ArenaLeaveBanTime = reader.ReadVarUhShort();
            if (ArenaLeaveBanTime < 0)
                throw new Exception("Forbidden value on ArenaLeaveBanTime = " + ArenaLeaveBanTime + ", it doesn't respect the following condition : arenaLeaveBanTime < 0");
        }
        
    }
    
}