

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccountCapabilitiesMessage : NetworkMessage
    {
        public const uint ProtocolId = 6216;
        public override uint MessageID => ProtocolId;
        
        public bool TutorialAvailable { get; set; }
        public bool CanCreateNewCharacter { get; set; }
        public int AccountId { get; set; }
        public uint BreedsVisible { get; set; }
        public uint BreedsAvailable { get; set; }
        public sbyte Status { get; set; }
        public double UnlimitedRestatEndDate { get; set; }
        
        public AccountCapabilitiesMessage()
        {
        }
        
        public AccountCapabilitiesMessage(bool tutorialAvailable, bool canCreateNewCharacter, int accountId, uint breedsVisible, uint breedsAvailable, sbyte status, double unlimitedRestatEndDate)
        {
            this.TutorialAvailable = tutorialAvailable;
            this.CanCreateNewCharacter = canCreateNewCharacter;
            this.AccountId = accountId;
            this.BreedsVisible = breedsVisible;
            this.BreedsAvailable = breedsAvailable;
            this.Status = status;
            this.UnlimitedRestatEndDate = unlimitedRestatEndDate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, TutorialAvailable);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, CanCreateNewCharacter);
            writer.WriteByte(flag1);
            writer.WriteInt(AccountId);
            writer.WriteVarInt(BreedsVisible);
            writer.WriteVarInt(BreedsAvailable);
            writer.WriteSByte(Status);
            writer.WriteDouble(UnlimitedRestatEndDate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            TutorialAvailable = BooleanByteWrapper.GetFlag(flag1, 0);
            CanCreateNewCharacter = BooleanByteWrapper.GetFlag(flag1, 1);
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            BreedsVisible = reader.ReadVarUhInt();
            if (BreedsVisible < 0)
                throw new Exception("Forbidden value on BreedsVisible = " + BreedsVisible + ", it doesn't respect the following condition : breedsVisible < 0");
            BreedsAvailable = reader.ReadVarUhInt();
            if (BreedsAvailable < 0)
                throw new Exception("Forbidden value on BreedsAvailable = " + BreedsAvailable + ", it doesn't respect the following condition : breedsAvailable < 0");
            Status = reader.ReadSByte();
            UnlimitedRestatEndDate = reader.ReadDouble();
            if (UnlimitedRestatEndDate < 0 || UnlimitedRestatEndDate > 9007199254740990)
                throw new Exception("Forbidden value on UnlimitedRestatEndDate = " + UnlimitedRestatEndDate + ", it doesn't respect the following condition : unlimitedRestatEndDate < 0 || unlimitedRestatEndDate > 9007199254740990");
        }
        
    }
    
}