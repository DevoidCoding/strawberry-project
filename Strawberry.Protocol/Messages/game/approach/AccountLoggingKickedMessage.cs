

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccountLoggingKickedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6029;
        public override uint MessageID => ProtocolId;
        
        public ushort Days { get; set; }
        public sbyte Hours { get; set; }
        public sbyte Minutes { get; set; }
        
        public AccountLoggingKickedMessage()
        {
        }
        
        public AccountLoggingKickedMessage(ushort days, sbyte hours, sbyte minutes)
        {
            this.Days = days;
            this.Hours = hours;
            this.Minutes = minutes;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Days);
            writer.WriteSByte(Hours);
            writer.WriteSByte(Minutes);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Days = reader.ReadVarUhShort();
            if (Days < 0)
                throw new Exception("Forbidden value on Days = " + Days + ", it doesn't respect the following condition : days < 0");
            Hours = reader.ReadSByte();
            if (Hours < 0)
                throw new Exception("Forbidden value on Hours = " + Hours + ", it doesn't respect the following condition : hours < 0");
            Minutes = reader.ReadSByte();
            if (Minutes < 0)
                throw new Exception("Forbidden value on Minutes = " + Minutes + ", it doesn't respect the following condition : minutes < 0");
        }
        
    }
    
}