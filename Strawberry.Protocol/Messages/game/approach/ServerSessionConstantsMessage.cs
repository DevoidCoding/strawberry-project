

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerSessionConstantsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6434;
        public override uint MessageID => ProtocolId;
        
        public Types.ServerSessionConstant[] Variables { get; set; }
        
        public ServerSessionConstantsMessage()
        {
        }
        
        public ServerSessionConstantsMessage(Types.ServerSessionConstant[] variables)
        {
            this.Variables = variables;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Variables.Length);
            foreach (var entry in Variables)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Variables = new Types.ServerSessionConstant[limit];
            for (int i = 0; i < limit; i++)
            {
                 Variables[i] = Types.ProtocolTypeManager.GetInstance<Types.ServerSessionConstant>(reader.ReadShort());
                 Variables[i].Deserialize(reader);
            }
        }
        
    }
    
}