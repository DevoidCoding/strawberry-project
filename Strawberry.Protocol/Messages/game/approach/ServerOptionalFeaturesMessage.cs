

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerOptionalFeaturesMessage : NetworkMessage
    {
        public const uint ProtocolId = 6305;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] Features { get; set; }
        
        public ServerOptionalFeaturesMessage()
        {
        }
        
        public ServerOptionalFeaturesMessage(sbyte[] features)
        {
            this.Features = features;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Features.Length);
            foreach (var entry in Features)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Features = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Features[i] = reader.ReadSByte();
            }
        }
        
    }
    
}