

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CompassUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5591;
        public override uint MessageID => ProtocolId;
        
        public sbyte Type { get; set; }
        public Types.MapCoordinates Coords { get; set; }
        
        public CompassUpdateMessage()
        {
        }
        
        public CompassUpdateMessage(sbyte type, Types.MapCoordinates coords)
        {
            this.Type = type;
            this.Coords = coords;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
            writer.WriteShort(Coords.TypeID);
            Coords.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            Coords = Types.ProtocolTypeManager.GetInstance<Types.MapCoordinates>(reader.ReadShort());
            Coords.Deserialize(reader);
        }
        
    }
    
}