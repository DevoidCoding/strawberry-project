

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CompassUpdatePartyMemberMessage : CompassUpdateMessage
    {
        public new const uint ProtocolId = 5589;
        public override uint MessageID => ProtocolId;
        
        public ulong MemberId { get; set; }
        public bool Active { get; set; }
        
        public CompassUpdatePartyMemberMessage()
        {
        }
        
        public CompassUpdatePartyMemberMessage(sbyte type, Types.MapCoordinates coords, ulong memberId, bool active)
         : base(type, coords)
        {
            this.MemberId = memberId;
            this.Active = active;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(MemberId);
            writer.WriteBoolean(Active);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
            Active = reader.ReadBoolean();
        }
        
    }
    
}