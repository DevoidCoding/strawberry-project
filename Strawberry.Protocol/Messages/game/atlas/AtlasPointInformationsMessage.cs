

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AtlasPointInformationsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5956;
        public override uint MessageID => ProtocolId;
        
        public Types.AtlasPointsInformations Type { get; set; }
        
        public AtlasPointInformationsMessage()
        {
        }
        
        public AtlasPointInformationsMessage(Types.AtlasPointsInformations type)
        {
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Type.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Type = new Types.AtlasPointsInformations();
            Type.Deserialize(reader);
        }
        
    }
    
}