

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ClientUIOpenedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6459;
        public override uint MessageID => ProtocolId;
        
        public sbyte Type { get; set; }
        
        public ClientUIOpenedMessage()
        {
        }
        
        public ClientUIOpenedMessage(sbyte type)
        {
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}