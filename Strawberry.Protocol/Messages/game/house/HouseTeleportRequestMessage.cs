

// Generated on 02/12/2018 03:56:35
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HouseTeleportRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6726;
        public override uint MessageID => ProtocolId;
        
        public uint HouseId { get; set; }
        public int HouseInstanceId { get; set; }
        
        public HouseTeleportRequestMessage()
        {
        }
        
        public HouseTeleportRequestMessage(uint houseId, int houseInstanceId)
        {
            this.HouseId = houseId;
            this.HouseInstanceId = houseInstanceId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(HouseId);
            writer.WriteInt(HouseInstanceId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HouseId = reader.ReadVarUhInt();
            if (HouseId < 0)
                throw new Exception("Forbidden value on HouseId = " + HouseId + ", it doesn't respect the following condition : houseId < 0");
            HouseInstanceId = reader.ReadInt();
            if (HouseInstanceId < 0)
                throw new Exception("Forbidden value on HouseInstanceId = " + HouseInstanceId + ", it doesn't respect the following condition : houseInstanceId < 0");
        }
        
    }
    
}