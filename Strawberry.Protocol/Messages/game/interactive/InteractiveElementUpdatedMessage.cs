

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InteractiveElementUpdatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5708;
        public override uint MessageID => ProtocolId;
        
        public Types.InteractiveElement InteractiveElement { get; set; }
        
        public InteractiveElementUpdatedMessage()
        {
        }
        
        public InteractiveElementUpdatedMessage(Types.InteractiveElement interactiveElement)
        {
            this.InteractiveElement = interactiveElement;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            InteractiveElement.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            InteractiveElement = new Types.InteractiveElement();
            InteractiveElement.Deserialize(reader);
        }
        
    }
    
}