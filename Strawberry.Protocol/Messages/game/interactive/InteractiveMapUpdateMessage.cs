

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InteractiveMapUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5002;
        public override uint MessageID => ProtocolId;
        
        public Types.InteractiveElement[] InteractiveElements { get; set; }
        
        public InteractiveMapUpdateMessage()
        {
        }
        
        public InteractiveMapUpdateMessage(Types.InteractiveElement[] interactiveElements)
        {
            this.InteractiveElements = interactiveElements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)InteractiveElements.Length);
            foreach (var entry in InteractiveElements)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            InteractiveElements = new Types.InteractiveElement[limit];
            for (int i = 0; i < limit; i++)
            {
                 InteractiveElements[i] = Types.ProtocolTypeManager.GetInstance<Types.InteractiveElement>(reader.ReadShort());
                 InteractiveElements[i].Deserialize(reader);
            }
        }
        
    }
    
}