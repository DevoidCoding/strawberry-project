

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportBuddiesRequestedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6302;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        public ulong InviterId { get; set; }
        public ulong[] InvalidBuddiesIds { get; set; }
        
        public TeleportBuddiesRequestedMessage()
        {
        }
        
        public TeleportBuddiesRequestedMessage(ushort dungeonId, ulong inviterId, ulong[] invalidBuddiesIds)
        {
            this.DungeonId = dungeonId;
            this.InviterId = inviterId;
            this.InvalidBuddiesIds = invalidBuddiesIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(InviterId);
            writer.WriteUShort((ushort)InvalidBuddiesIds.Length);
            foreach (var entry in InvalidBuddiesIds)
            {
                 writer.WriteVarLong(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
            InviterId = reader.ReadVarUhLong();
            if (InviterId < 0 || InviterId > 9007199254740990)
                throw new Exception("Forbidden value on InviterId = " + InviterId + ", it doesn't respect the following condition : inviterId < 0 || inviterId > 9007199254740990");
            var limit = reader.ReadUShort();
            InvalidBuddiesIds = new ulong[limit];
            for (int i = 0; i < limit; i++)
            {
                 InvalidBuddiesIds[i] = reader.ReadVarUhLong();
            }
        }
        
    }
    
}