

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportBuddiesAnswerMessage : NetworkMessage
    {
        public const uint ProtocolId = 6294;
        public override uint MessageID => ProtocolId;
        
        public bool Accept { get; set; }
        
        public TeleportBuddiesAnswerMessage()
        {
        }
        
        public TeleportBuddiesAnswerMessage(bool accept)
        {
            this.Accept = accept;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Accept);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Accept = reader.ReadBoolean();
        }
        
    }
    
}