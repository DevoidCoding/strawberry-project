

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportBuddiesMessage : NetworkMessage
    {
        public const uint ProtocolId = 6289;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        
        public TeleportBuddiesMessage()
        {
        }
        
        public TeleportBuddiesMessage(ushort dungeonId)
        {
            this.DungeonId = dungeonId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DungeonId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
        }
        
    }
    
}