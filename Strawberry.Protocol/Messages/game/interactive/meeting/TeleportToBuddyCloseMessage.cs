

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportToBuddyCloseMessage : NetworkMessage
    {
        public const uint ProtocolId = 6303;
        public override uint MessageID => ProtocolId;
        
        public ushort DungeonId { get; set; }
        public ulong BuddyId { get; set; }
        
        public TeleportToBuddyCloseMessage()
        {
        }
        
        public TeleportToBuddyCloseMessage(ushort dungeonId, ulong buddyId)
        {
            this.DungeonId = dungeonId;
            this.BuddyId = buddyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(DungeonId);
            writer.WriteVarLong(BuddyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DungeonId = reader.ReadVarUhShort();
            if (DungeonId < 0)
                throw new Exception("Forbidden value on DungeonId = " + DungeonId + ", it doesn't respect the following condition : dungeonId < 0");
            BuddyId = reader.ReadVarUhLong();
            if (BuddyId < 0 || BuddyId > 9007199254740990)
                throw new Exception("Forbidden value on BuddyId = " + BuddyId + ", it doesn't respect the following condition : buddyId < 0 || buddyId > 9007199254740990");
        }
        
    }
    
}