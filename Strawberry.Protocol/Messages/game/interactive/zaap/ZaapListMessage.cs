

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ZaapListMessage : TeleportDestinationsListMessage
    {
        public new const uint ProtocolId = 1604;
        public override uint MessageID => ProtocolId;
        
        public double SpawnMapId { get; set; }
        
        public ZaapListMessage()
        {
        }
        
        public ZaapListMessage(sbyte teleporterType, double[] mapIds, ushort[] subAreaIds, ushort[] costs, sbyte[] destTeleporterType, double spawnMapId)
         : base(teleporterType, mapIds, subAreaIds, costs, destTeleporterType)
        {
            this.SpawnMapId = spawnMapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(SpawnMapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SpawnMapId = reader.ReadDouble();
            if (SpawnMapId < 0 || SpawnMapId > 9007199254740990)
                throw new Exception("Forbidden value on SpawnMapId = " + SpawnMapId + ", it doesn't respect the following condition : spawnMapId < 0 || spawnMapId > 9007199254740990");
        }
        
    }
    
}