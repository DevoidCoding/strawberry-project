

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5961;
        public override uint MessageID => ProtocolId;
        
        public sbyte TeleporterType { get; set; }
        public double MapId { get; set; }
        
        public TeleportRequestMessage()
        {
        }
        
        public TeleportRequestMessage(sbyte teleporterType, double mapId)
        {
            this.TeleporterType = teleporterType;
            this.MapId = mapId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(TeleporterType);
            writer.WriteDouble(MapId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TeleporterType = reader.ReadSByte();
            if (TeleporterType < 0)
                throw new Exception("Forbidden value on TeleporterType = " + TeleporterType + ", it doesn't respect the following condition : teleporterType < 0");
            MapId = reader.ReadDouble();
            if (MapId < 0 || MapId > 9007199254740990)
                throw new Exception("Forbidden value on MapId = " + MapId + ", it doesn't respect the following condition : mapId < 0 || mapId > 9007199254740990");
        }
        
    }
    
}