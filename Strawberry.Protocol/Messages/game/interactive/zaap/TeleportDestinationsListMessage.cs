

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TeleportDestinationsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5960;
        public override uint MessageID => ProtocolId;
        
        public sbyte TeleporterType { get; set; }
        public double[] MapIds { get; set; }
        public ushort[] SubAreaIds { get; set; }
        public ushort[] Costs { get; set; }
        public sbyte[] DestTeleporterType { get; set; }
        
        public TeleportDestinationsListMessage()
        {
        }
        
        public TeleportDestinationsListMessage(sbyte teleporterType, double[] mapIds, ushort[] subAreaIds, ushort[] costs, sbyte[] destTeleporterType)
        {
            this.TeleporterType = teleporterType;
            this.MapIds = mapIds;
            this.SubAreaIds = subAreaIds;
            this.Costs = costs;
            this.DestTeleporterType = destTeleporterType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(TeleporterType);
            writer.WriteUShort((ushort)MapIds.Length);
            foreach (var entry in MapIds)
            {
                 writer.WriteDouble(entry);
            }
            writer.WriteUShort((ushort)SubAreaIds.Length);
            foreach (var entry in SubAreaIds)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)Costs.Length);
            foreach (var entry in Costs)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)DestTeleporterType.Length);
            foreach (var entry in DestTeleporterType)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TeleporterType = reader.ReadSByte();
            if (TeleporterType < 0)
                throw new Exception("Forbidden value on TeleporterType = " + TeleporterType + ", it doesn't respect the following condition : teleporterType < 0");
            var limit = reader.ReadUShort();
            MapIds = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 MapIds[i] = reader.ReadDouble();
            }
            limit = reader.ReadUShort();
            SubAreaIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 SubAreaIds[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            Costs = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Costs[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            DestTeleporterType = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 DestTeleporterType[i] = reader.ReadSByte();
            }
        }
        
    }
    
}