

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InteractiveUsedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5745;
        public override uint MessageID => ProtocolId;
        
        public ulong EntityId { get; set; }
        public uint ElemId { get; set; }
        public ushort SkillId { get; set; }
        public ushort Duration { get; set; }
        public bool CanMove { get; set; }
        
        public InteractiveUsedMessage()
        {
        }
        
        public InteractiveUsedMessage(ulong entityId, uint elemId, ushort skillId, ushort duration, bool canMove)
        {
            this.EntityId = entityId;
            this.ElemId = elemId;
            this.SkillId = skillId;
            this.Duration = duration;
            this.CanMove = canMove;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(EntityId);
            writer.WriteVarInt(ElemId);
            writer.WriteVarShort(SkillId);
            writer.WriteVarShort(Duration);
            writer.WriteBoolean(CanMove);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            EntityId = reader.ReadVarUhLong();
            if (EntityId < 0 || EntityId > 9007199254740990)
                throw new Exception("Forbidden value on EntityId = " + EntityId + ", it doesn't respect the following condition : entityId < 0 || entityId > 9007199254740990");
            ElemId = reader.ReadVarUhInt();
            if (ElemId < 0)
                throw new Exception("Forbidden value on ElemId = " + ElemId + ", it doesn't respect the following condition : elemId < 0");
            SkillId = reader.ReadVarUhShort();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
            Duration = reader.ReadVarUhShort();
            if (Duration < 0)
                throw new Exception("Forbidden value on Duration = " + Duration + ", it doesn't respect the following condition : duration < 0");
            CanMove = reader.ReadBoolean();
        }
        
    }
    
}