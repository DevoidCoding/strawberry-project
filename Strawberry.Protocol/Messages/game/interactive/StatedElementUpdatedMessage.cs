

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StatedElementUpdatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5709;
        public override uint MessageID => ProtocolId;
        
        public Types.StatedElement StatedElement { get; set; }
        
        public StatedElementUpdatedMessage()
        {
        }
        
        public StatedElementUpdatedMessage(Types.StatedElement statedElement)
        {
            this.StatedElement = statedElement;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            StatedElement.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            StatedElement = new Types.StatedElement();
            StatedElement.Deserialize(reader);
        }
        
    }
    
}