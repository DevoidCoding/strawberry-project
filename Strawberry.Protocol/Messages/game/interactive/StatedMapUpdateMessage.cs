

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class StatedMapUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5716;
        public override uint MessageID => ProtocolId;
        
        public Types.StatedElement[] StatedElements { get; set; }
        
        public StatedMapUpdateMessage()
        {
        }
        
        public StatedMapUpdateMessage(Types.StatedElement[] statedElements)
        {
            this.StatedElements = statedElements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)StatedElements.Length);
            foreach (var entry in StatedElements)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            StatedElements = new Types.StatedElement[limit];
            for (int i = 0; i < limit; i++)
            {
                 StatedElements[i] = new Types.StatedElement();
                 StatedElements[i].Deserialize(reader);
            }
        }
        
    }
    
}