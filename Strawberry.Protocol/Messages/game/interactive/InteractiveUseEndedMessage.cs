

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class InteractiveUseEndedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6112;
        public override uint MessageID => ProtocolId;
        
        public uint ElemId { get; set; }
        public ushort SkillId { get; set; }
        
        public InteractiveUseEndedMessage()
        {
        }
        
        public InteractiveUseEndedMessage(uint elemId, ushort skillId)
        {
            this.ElemId = elemId;
            this.SkillId = skillId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ElemId);
            writer.WriteVarShort(SkillId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ElemId = reader.ReadVarUhInt();
            if (ElemId < 0)
                throw new Exception("Forbidden value on ElemId = " + ElemId + ", it doesn't respect the following condition : elemId < 0");
            SkillId = reader.ReadVarUhShort();
            if (SkillId < 0)
                throw new Exception("Forbidden value on SkillId = " + SkillId + ", it doesn't respect the following condition : skillId < 0");
        }
        
    }
    
}