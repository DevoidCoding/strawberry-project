

// Generated on 02/12/2018 03:56:36
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class InteractiveUseRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5001;
        public override uint MessageID => ProtocolId;
        
        public uint ElemId { get; set; }
        public uint SkillInstanceUid { get; set; }
        
        public InteractiveUseRequestMessage()
        {
        }
        
        public InteractiveUseRequestMessage(uint elemId, uint skillInstanceUid)
        {
            this.ElemId = elemId;
            this.SkillInstanceUid = skillInstanceUid;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(ElemId);
            writer.WriteVarInt(SkillInstanceUid);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ElemId = reader.ReadVarUhInt();
            if (ElemId < 0)
                throw new Exception("Forbidden value on ElemId = " + ElemId + ", it doesn't respect the following condition : elemId < 0");
            SkillInstanceUid = reader.ReadVarUhInt();
            if (SkillInstanceUid < 0)
                throw new Exception("Forbidden value on SkillInstanceUid = " + SkillInstanceUid + ", it doesn't respect the following condition : skillInstanceUid < 0");
        }
        
    }
    
}