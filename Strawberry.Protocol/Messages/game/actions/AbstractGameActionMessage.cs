

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AbstractGameActionMessage : NetworkMessage
    {
        public const uint ProtocolId = 1000;
        public override uint MessageID => ProtocolId;
        
        public ushort ActionId { get; set; }
        public double SourceId { get; set; }
        
        public AbstractGameActionMessage()
        {
        }
        
        public AbstractGameActionMessage(ushort actionId, double sourceId)
        {
            this.ActionId = actionId;
            this.SourceId = sourceId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(SourceId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ActionId = reader.ReadVarUhShort();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
            SourceId = reader.ReadDouble();
            if (SourceId < -9007199254740990 || SourceId > 9007199254740990)
                throw new Exception("Forbidden value on SourceId = " + SourceId + ", it doesn't respect the following condition : sourceId < -9007199254740990 || sourceId > 9007199254740990");
        }
        
    }
    
}