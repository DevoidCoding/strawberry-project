

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightNoSpellCastMessage : NetworkMessage
    {
        public const uint ProtocolId = 6132;
        public override uint MessageID => ProtocolId;
        
        public uint SpellLevelId { get; set; }
        
        public GameActionFightNoSpellCastMessage()
        {
        }
        
        public GameActionFightNoSpellCastMessage(uint spellLevelId)
        {
            this.SpellLevelId = spellLevelId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(SpellLevelId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellLevelId = reader.ReadVarUhInt();
            if (SpellLevelId < 0)
                throw new Exception("Forbidden value on SpellLevelId = " + SpellLevelId + ", it doesn't respect the following condition : spellLevelId < 0");
        }
        
    }
    
}