

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class GameActionFightCastRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 1005;
        public override uint MessageID => ProtocolId;
        
        public ushort SpellId { get; set; }
        public short CellId { get; set; }
        
        public GameActionFightCastRequestMessage()
        {
        }
        
        public GameActionFightCastRequestMessage(ushort spellId, short cellId)
        {
            this.SpellId = spellId;
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteShort(CellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            CellId = reader.ReadShort();
            if (CellId < -1 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < -1 || cellId > 559");
        }
        
    }
    
}