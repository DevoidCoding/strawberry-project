

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightDispellSpellMessage : GameActionFightDispellMessage
    {
        public new const uint ProtocolId = 6176;
        public override uint MessageID => ProtocolId;
        
        public ushort SpellId { get; set; }
        
        public GameActionFightDispellSpellMessage()
        {
        }
        
        public GameActionFightDispellSpellMessage(ushort actionId, double sourceId, double targetId, ushort spellId)
         : base(actionId, sourceId, targetId)
        {
            this.SpellId = spellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
        }
        
    }
    
}