

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightSlideMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5525;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public short StartCellId { get; set; }
        public short EndCellId { get; set; }
        
        public GameActionFightSlideMessage()
        {
        }
        
        public GameActionFightSlideMessage(ushort actionId, double sourceId, double targetId, short startCellId, short endCellId)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.StartCellId = startCellId;
            this.EndCellId = endCellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(StartCellId);
            writer.WriteShort(EndCellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            StartCellId = reader.ReadShort();
            if (StartCellId < -1 || StartCellId > 559)
                throw new Exception("Forbidden value on StartCellId = " + StartCellId + ", it doesn't respect the following condition : startCellId < -1 || startCellId > 559");
            EndCellId = reader.ReadShort();
            if (EndCellId < -1 || EndCellId > 559)
                throw new Exception("Forbidden value on EndCellId = " + EndCellId + ", it doesn't respect the following condition : endCellId < -1 || endCellId > 559");
        }
        
    }
    
}