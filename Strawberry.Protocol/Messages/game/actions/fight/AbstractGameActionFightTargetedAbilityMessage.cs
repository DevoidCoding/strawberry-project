

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AbstractGameActionFightTargetedAbilityMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 6118;
        public override uint MessageID => ProtocolId;
        
        public bool SilentCast { get; set; }
        public bool VerboseCast { get; set; }
        public double TargetId { get; set; }
        public short DestinationCellId { get; set; }
        public sbyte Critical { get; set; }
        
        public AbstractGameActionFightTargetedAbilityMessage()
        {
        }
        
        public AbstractGameActionFightTargetedAbilityMessage(ushort actionId, double sourceId, bool silentCast, bool verboseCast, double targetId, short destinationCellId, sbyte critical)
         : base(actionId, sourceId)
        {
            this.SilentCast = silentCast;
            this.VerboseCast = verboseCast;
            this.TargetId = targetId;
            this.DestinationCellId = destinationCellId;
            this.Critical = critical;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, SilentCast);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, VerboseCast);
            writer.WriteByte(flag1);
            writer.WriteDouble(TargetId);
            writer.WriteShort(DestinationCellId);
            writer.WriteSByte(Critical);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            byte flag1 = reader.ReadByte();
            SilentCast = BooleanByteWrapper.GetFlag(flag1, 0);
            VerboseCast = BooleanByteWrapper.GetFlag(flag1, 1);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            DestinationCellId = reader.ReadShort();
            if (DestinationCellId < -1 || DestinationCellId > 559)
                throw new Exception("Forbidden value on DestinationCellId = " + DestinationCellId + ", it doesn't respect the following condition : destinationCellId < -1 || destinationCellId > 559");
            Critical = reader.ReadSByte();
            if (Critical < 0)
                throw new Exception("Forbidden value on Critical = " + Critical + ", it doesn't respect the following condition : critical < 0");
        }
        
    }
    
}