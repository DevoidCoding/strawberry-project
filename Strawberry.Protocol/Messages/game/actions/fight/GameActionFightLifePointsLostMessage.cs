

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightLifePointsLostMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 6312;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public uint Loss { get; set; }
        public uint PermanentDamages { get; set; }
        
        public GameActionFightLifePointsLostMessage()
        {
        }
        
        public GameActionFightLifePointsLostMessage(ushort actionId, double sourceId, double targetId, uint loss, uint permanentDamages)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.Loss = loss;
            this.PermanentDamages = permanentDamages;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Loss);
            writer.WriteVarInt(PermanentDamages);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            Loss = reader.ReadVarUhInt();
            if (Loss < 0)
                throw new Exception("Forbidden value on Loss = " + Loss + ", it doesn't respect the following condition : loss < 0");
            PermanentDamages = reader.ReadVarUhInt();
            if (PermanentDamages < 0)
                throw new Exception("Forbidden value on PermanentDamages = " + PermanentDamages + ", it doesn't respect the following condition : permanentDamages < 0");
        }
        
    }
    
}