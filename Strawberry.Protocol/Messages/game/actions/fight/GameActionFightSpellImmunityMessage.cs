

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightSpellImmunityMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 6221;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public ushort SpellId { get; set; }
        
        public GameActionFightSpellImmunityMessage()
        {
        }
        
        public GameActionFightSpellImmunityMessage(ushort actionId, double sourceId, double targetId, ushort spellId)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.SpellId = spellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarShort(SpellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
        }
        
    }
    
}