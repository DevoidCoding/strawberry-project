

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightLifeAndShieldPointsLostMessage : GameActionFightLifePointsLostMessage
    {
        public new const uint ProtocolId = 6310;
        public override uint MessageID => ProtocolId;
        
        public ushort ShieldLoss { get; set; }
        
        public GameActionFightLifeAndShieldPointsLostMessage()
        {
        }
        
        public GameActionFightLifeAndShieldPointsLostMessage(ushort actionId, double sourceId, double targetId, uint loss, uint permanentDamages, ushort shieldLoss)
         : base(actionId, sourceId, targetId, loss, permanentDamages)
        {
            this.ShieldLoss = shieldLoss;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(ShieldLoss);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ShieldLoss = reader.ReadVarUhShort();
            if (ShieldLoss < 0)
                throw new Exception("Forbidden value on ShieldLoss = " + ShieldLoss + ", it doesn't respect the following condition : shieldLoss < 0");
        }
        
    }
    
}