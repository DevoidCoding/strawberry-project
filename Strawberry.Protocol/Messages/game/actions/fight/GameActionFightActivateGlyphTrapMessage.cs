

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightActivateGlyphTrapMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 6545;
        public override uint MessageID => ProtocolId;
        
        public short MarkId { get; set; }
        public bool Active { get; set; }
        
        public GameActionFightActivateGlyphTrapMessage()
        {
        }
        
        public GameActionFightActivateGlyphTrapMessage(ushort actionId, double sourceId, short markId, bool active)
         : base(actionId, sourceId)
        {
            this.MarkId = markId;
            this.Active = active;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MarkId);
            writer.WriteBoolean(Active);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MarkId = reader.ReadShort();
            Active = reader.ReadBoolean();
        }
        
    }
    
}