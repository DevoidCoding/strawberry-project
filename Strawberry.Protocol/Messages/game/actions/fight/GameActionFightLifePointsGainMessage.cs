

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightLifePointsGainMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 6311;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public uint Delta { get; set; }
        
        public GameActionFightLifePointsGainMessage()
        {
        }
        
        public GameActionFightLifePointsGainMessage(ushort actionId, double sourceId, double targetId, uint delta)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.Delta = delta;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Delta);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            Delta = reader.ReadVarUhInt();
            if (Delta < 0)
                throw new Exception("Forbidden value on Delta = " + Delta + ", it doesn't respect the following condition : delta < 0");
        }
        
    }
    
}