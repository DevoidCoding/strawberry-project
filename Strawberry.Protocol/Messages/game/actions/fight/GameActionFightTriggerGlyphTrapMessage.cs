

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightTriggerGlyphTrapMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5741;
        public override uint MessageID => ProtocolId;
        
        public short MarkId { get; set; }
        public ushort MarkImpactCell { get; set; }
        public double TriggeringCharacterId { get; set; }
        public ushort TriggeredSpellId { get; set; }
        
        public GameActionFightTriggerGlyphTrapMessage()
        {
        }
        
        public GameActionFightTriggerGlyphTrapMessage(ushort actionId, double sourceId, short markId, ushort markImpactCell, double triggeringCharacterId, ushort triggeredSpellId)
         : base(actionId, sourceId)
        {
            this.MarkId = markId;
            this.MarkImpactCell = markImpactCell;
            this.TriggeringCharacterId = triggeringCharacterId;
            this.TriggeredSpellId = triggeredSpellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(MarkId);
            writer.WriteVarShort(MarkImpactCell);
            writer.WriteDouble(TriggeringCharacterId);
            writer.WriteVarShort(TriggeredSpellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            MarkId = reader.ReadShort();
            MarkImpactCell = reader.ReadVarUhShort();
            if (MarkImpactCell < 0)
                throw new Exception("Forbidden value on MarkImpactCell = " + MarkImpactCell + ", it doesn't respect the following condition : markImpactCell < 0");
            TriggeringCharacterId = reader.ReadDouble();
            if (TriggeringCharacterId < -9007199254740990 || TriggeringCharacterId > 9007199254740990)
                throw new Exception("Forbidden value on TriggeringCharacterId = " + TriggeringCharacterId + ", it doesn't respect the following condition : triggeringCharacterId < -9007199254740990 || triggeringCharacterId > 9007199254740990");
            TriggeredSpellId = reader.ReadVarUhShort();
            if (TriggeredSpellId < 0)
                throw new Exception("Forbidden value on TriggeredSpellId = " + TriggeredSpellId + ", it doesn't respect the following condition : triggeredSpellId < 0");
        }
        
    }
    
}