

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightDispellEffectMessage : GameActionFightDispellMessage
    {
        public new const uint ProtocolId = 6113;
        public override uint MessageID => ProtocolId;
        
        public int BoostUID { get; set; }
        
        public GameActionFightDispellEffectMessage()
        {
        }
        
        public GameActionFightDispellEffectMessage(ushort actionId, double sourceId, double targetId, int boostUID)
         : base(actionId, sourceId, targetId)
        {
            this.BoostUID = boostUID;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(BoostUID);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            BoostUID = reader.ReadInt();
            if (BoostUID < 0)
                throw new Exception("Forbidden value on BoostUID = " + BoostUID + ", it doesn't respect the following condition : boostUID < 0");
        }
        
    }
    
}