

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightSpellCastMessage : AbstractGameActionFightTargetedAbilityMessage
    {
        public new const uint ProtocolId = 1010;
        public override uint MessageID => ProtocolId;
        
        public ushort SpellId { get; set; }
        public short SpellLevel { get; set; }
        public short[] PortalsIds { get; set; }
        
        public GameActionFightSpellCastMessage()
        {
        }
        
        public GameActionFightSpellCastMessage(ushort actionId, double sourceId, bool silentCast, bool verboseCast, double targetId, short destinationCellId, sbyte critical, ushort spellId, short spellLevel, short[] portalsIds)
         : base(actionId, sourceId, silentCast, verboseCast, targetId, destinationCellId, critical)
        {
            this.SpellId = spellId;
            this.SpellLevel = spellLevel;
            this.PortalsIds = portalsIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarShort(SpellId);
            writer.WriteShort(SpellLevel);
            writer.WriteUShort((ushort)PortalsIds.Length);
            foreach (var entry in PortalsIds)
            {
                 writer.WriteShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            SpellLevel = reader.ReadShort();
            if (SpellLevel < 1 || SpellLevel > 200)
                throw new Exception("Forbidden value on SpellLevel = " + SpellLevel + ", it doesn't respect the following condition : spellLevel < 1 || spellLevel > 200");
            var limit = reader.ReadUShort();
            PortalsIds = new short[limit];
            for (int i = 0; i < limit; i++)
            {
                 PortalsIds[i] = reader.ReadShort();
            }
        }
        
    }
    
}