

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightInvisibilityMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5821;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public sbyte State { get; set; }
        
        public GameActionFightInvisibilityMessage()
        {
        }
        
        public GameActionFightInvisibilityMessage(ushort actionId, double sourceId, double targetId, sbyte state)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.State = state;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteSByte(State);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            State = reader.ReadSByte();
            if (State < 0)
                throw new Exception("Forbidden value on State = " + State + ", it doesn't respect the following condition : state < 0");
        }
        
    }
    
}