

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightReduceDamagesMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5526;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public uint Amount { get; set; }
        
        public GameActionFightReduceDamagesMessage()
        {
        }
        
        public GameActionFightReduceDamagesMessage(ushort actionId, double sourceId, double targetId, uint amount)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.Amount = amount;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteVarInt(Amount);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            Amount = reader.ReadVarUhInt();
            if (Amount < 0)
                throw new Exception("Forbidden value on Amount = " + Amount + ", it doesn't respect the following condition : amount < 0");
        }
        
    }
    
}