

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightMarkCellsMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5540;
        public override uint MessageID => ProtocolId;
        
        public Types.GameActionMark Mark { get; set; }
        
        public GameActionFightMarkCellsMessage()
        {
        }
        
        public GameActionFightMarkCellsMessage(ushort actionId, double sourceId, Types.GameActionMark mark)
         : base(actionId, sourceId)
        {
            this.Mark = mark;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            Mark.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Mark = new Types.GameActionMark();
            Mark.Deserialize(reader);
        }
        
    }
    
}