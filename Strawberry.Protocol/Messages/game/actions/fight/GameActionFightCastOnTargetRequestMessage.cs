

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightCastOnTargetRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6330;
        public override uint MessageID => ProtocolId;
        
        public ushort SpellId { get; set; }
        public double TargetId { get; set; }
        
        public GameActionFightCastOnTargetRequestMessage()
        {
        }
        
        public GameActionFightCastOnTargetRequestMessage(ushort spellId, double targetId)
        {
            this.SpellId = spellId;
            this.TargetId = targetId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SpellId);
            writer.WriteDouble(TargetId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SpellId = reader.ReadVarUhShort();
            if (SpellId < 0)
                throw new Exception("Forbidden value on SpellId = " + SpellId + ", it doesn't respect the following condition : spellId < 0");
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
        }
        
    }
    
}