

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightTackledMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 1004;
        public override uint MessageID => ProtocolId;
        
        public double[] TacklersIds { get; set; }
        
        public GameActionFightTackledMessage()
        {
        }
        
        public GameActionFightTackledMessage(ushort actionId, double sourceId, double[] tacklersIds)
         : base(actionId, sourceId)
        {
            this.TacklersIds = tacklersIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)TacklersIds.Length);
            foreach (var entry in TacklersIds)
            {
                 writer.WriteDouble(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            TacklersIds = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 TacklersIds[i] = reader.ReadDouble();
            }
        }
        
    }
    
}