

// Generated on 02/12/2018 03:56:10
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightExchangePositionsMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5527;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public short CasterCellId { get; set; }
        public short TargetCellId { get; set; }
        
        public GameActionFightExchangePositionsMessage()
        {
        }
        
        public GameActionFightExchangePositionsMessage(ushort actionId, double sourceId, double targetId, short casterCellId, short targetCellId)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.CasterCellId = casterCellId;
            this.TargetCellId = targetCellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(CasterCellId);
            writer.WriteShort(TargetCellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            CasterCellId = reader.ReadShort();
            if (CasterCellId < -1 || CasterCellId > 559)
                throw new Exception("Forbidden value on CasterCellId = " + CasterCellId + ", it doesn't respect the following condition : casterCellId < -1 || casterCellId > 559");
            TargetCellId = reader.ReadShort();
            if (TargetCellId < -1 || TargetCellId > 559)
                throw new Exception("Forbidden value on TargetCellId = " + TargetCellId + ", it doesn't respect the following condition : targetCellId < -1 || targetCellId > 559");
        }
        
    }
    
}