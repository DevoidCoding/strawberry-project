

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightSummonMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5825;
        public override uint MessageID => ProtocolId;
        
        public Types.GameFightFighterInformations[] Summons { get; set; }
        
        public GameActionFightSummonMessage()
        {
        }
        
        public GameActionFightSummonMessage(ushort actionId, double sourceId, Types.GameFightFighterInformations[] summons)
         : base(actionId, sourceId)
        {
            this.Summons = summons;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Summons.Length);
            foreach (var entry in Summons)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Summons = new Types.GameFightFighterInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Summons[i] = Types.ProtocolTypeManager.GetInstance<Types.GameFightFighterInformations>(reader.ReadShort());
                 Summons[i].Deserialize(reader);
            }
        }
        
    }
    
}