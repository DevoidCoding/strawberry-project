

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class GameActionFightCarryCharacterMessage : AbstractGameActionMessage
    {
        public new const uint ProtocolId = 5830;
        public override uint MessageID => ProtocolId;
        
        public double TargetId { get; set; }
        public short CellId { get; set; }
        
        public GameActionFightCarryCharacterMessage()
        {
        }
        
        public GameActionFightCarryCharacterMessage(ushort actionId, double sourceId, double targetId, short cellId)
         : base(actionId, sourceId)
        {
            this.TargetId = targetId;
            this.CellId = cellId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(TargetId);
            writer.WriteShort(CellId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            TargetId = reader.ReadDouble();
            if (TargetId < -9007199254740990 || TargetId > 9007199254740990)
                throw new Exception("Forbidden value on TargetId = " + TargetId + ", it doesn't respect the following condition : targetId < -9007199254740990 || targetId > 9007199254740990");
            CellId = reader.ReadShort();
            if (CellId < -1 || CellId > 559)
                throw new Exception("Forbidden value on CellId = " + CellId + ", it doesn't respect the following condition : cellId < -1 || cellId > 559");
        }
        
    }
    
}