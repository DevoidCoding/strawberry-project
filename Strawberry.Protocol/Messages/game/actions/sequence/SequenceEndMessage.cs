

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SequenceEndMessage : NetworkMessage
    {
        public const uint ProtocolId = 956;
        public override uint MessageID => ProtocolId;
        
        public ushort ActionId { get; set; }
        public double AuthorId { get; set; }
        public sbyte SequenceType { get; set; }
        
        public SequenceEndMessage()
        {
        }
        
        public SequenceEndMessage(ushort actionId, double authorId, sbyte sequenceType)
        {
            this.ActionId = actionId;
            this.AuthorId = authorId;
            this.SequenceType = sequenceType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ActionId);
            writer.WriteDouble(AuthorId);
            writer.WriteSByte(SequenceType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ActionId = reader.ReadVarUhShort();
            if (ActionId < 0)
                throw new Exception("Forbidden value on ActionId = " + ActionId + ", it doesn't respect the following condition : actionId < 0");
            AuthorId = reader.ReadDouble();
            if (AuthorId < -9007199254740990 || AuthorId > 9007199254740990)
                throw new Exception("Forbidden value on AuthorId = " + AuthorId + ", it doesn't respect the following condition : authorId < -9007199254740990 || authorId > 9007199254740990");
            SequenceType = reader.ReadSByte();
        }
        
    }
    
}