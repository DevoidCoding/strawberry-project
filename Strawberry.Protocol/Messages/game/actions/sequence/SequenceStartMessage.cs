

// Generated on 02/12/2018 03:56:11
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SequenceStartMessage : NetworkMessage
    {
        public const uint ProtocolId = 955;
        public override uint MessageID => ProtocolId;
        
        public sbyte SequenceType { get; set; }
        public double AuthorId { get; set; }
        
        public SequenceStartMessage()
        {
        }
        
        public SequenceStartMessage(sbyte sequenceType, double authorId)
        {
            this.SequenceType = sequenceType;
            this.AuthorId = authorId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(SequenceType);
            writer.WriteDouble(AuthorId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SequenceType = reader.ReadSByte();
            AuthorId = reader.ReadDouble();
            if (AuthorId < -9007199254740990 || AuthorId > 9007199254740990)
                throw new Exception("Forbidden value on AuthorId = " + AuthorId + ", it doesn't respect the following condition : authorId < -9007199254740990 || authorId > 9007199254740990");
        }
        
    }
    
}