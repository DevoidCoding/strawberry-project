

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareCreatedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6668;
        public override uint MessageID => ProtocolId;
        
        public Types.DareInformations DareInfos { get; set; }
        public bool NeedNotifications { get; set; }
        
        public DareCreatedMessage()
        {
        }
        
        public DareCreatedMessage(Types.DareInformations dareInfos, bool needNotifications)
        {
            this.DareInfos = dareInfos;
            this.NeedNotifications = needNotifications;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            DareInfos.Serialize(writer);
            writer.WriteBoolean(NeedNotifications);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DareInfos = new Types.DareInformations();
            DareInfos.Deserialize(reader);
            NeedNotifications = reader.ReadBoolean();
        }
        
    }
    
}