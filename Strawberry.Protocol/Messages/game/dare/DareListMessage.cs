

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6661;
        public override uint MessageID => ProtocolId;
        
        public Types.DareInformations[] Dares { get; set; }
        
        public DareListMessage()
        {
        }
        
        public DareListMessage(Types.DareInformations[] dares)
        {
            this.Dares = dares;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Dares.Length);
            foreach (var entry in Dares)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Dares = new Types.DareInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Dares[i] = new Types.DareInformations();
                 Dares[i].Deserialize(reader);
            }
        }
        
    }
    
}