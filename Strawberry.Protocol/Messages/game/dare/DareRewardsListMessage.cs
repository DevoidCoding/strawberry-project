

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareRewardsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6677;
        public override uint MessageID => ProtocolId;
        
        public Types.DareReward[] Rewards { get; set; }
        
        public DareRewardsListMessage()
        {
        }
        
        public DareRewardsListMessage(Types.DareReward[] rewards)
        {
            this.Rewards = rewards;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Rewards.Length);
            foreach (var entry in Rewards)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Rewards = new Types.DareReward[limit];
            for (int i = 0; i < limit; i++)
            {
                 Rewards[i] = new Types.DareReward();
                 Rewards[i].Deserialize(reader);
            }
        }
        
    }
    
}