

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareRewardConsumeValidationMessage : NetworkMessage
    {
        public const uint ProtocolId = 6675;
        public override uint MessageID => ProtocolId;
        
        public double DareId { get; set; }
        public sbyte Type { get; set; }
        
        public DareRewardConsumeValidationMessage()
        {
        }
        
        public DareRewardConsumeValidationMessage(double dareId, sbyte type)
        {
            this.DareId = dareId;
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DareId);
            writer.WriteSByte(Type);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}