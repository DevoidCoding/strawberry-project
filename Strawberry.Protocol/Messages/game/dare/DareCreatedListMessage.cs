

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareCreatedListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6663;
        public override uint MessageID => ProtocolId;
        
        public Types.DareInformations[] DaresFixedInfos { get; set; }
        public Types.DareVersatileInformations[] DaresVersatilesInfos { get; set; }
        
        public DareCreatedListMessage()
        {
        }
        
        public DareCreatedListMessage(Types.DareInformations[] daresFixedInfos, Types.DareVersatileInformations[] daresVersatilesInfos)
        {
            this.DaresFixedInfos = daresFixedInfos;
            this.DaresVersatilesInfos = daresVersatilesInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)DaresFixedInfos.Length);
            foreach (var entry in DaresFixedInfos)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)DaresVersatilesInfos.Length);
            foreach (var entry in DaresVersatilesInfos)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            DaresFixedInfos = new Types.DareInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 DaresFixedInfos[i] = new Types.DareInformations();
                 DaresFixedInfos[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            DaresVersatilesInfos = new Types.DareVersatileInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 DaresVersatilesInfos[i] = new Types.DareVersatileInformations();
                 DaresVersatilesInfos[i].Deserialize(reader);
            }
        }
        
    }
    
}