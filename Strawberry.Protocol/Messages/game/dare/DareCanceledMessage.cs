

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareCanceledMessage : NetworkMessage
    {
        public const uint ProtocolId = 6679;
        public override uint MessageID => ProtocolId;
        
        public double DareId { get; set; }
        
        public DareCanceledMessage()
        {
        }
        
        public DareCanceledMessage(double dareId)
        {
            this.DareId = dareId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(DareId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
        }
        
    }
    
}