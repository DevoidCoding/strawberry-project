

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareSubscribedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6660;
        public override uint MessageID => ProtocolId;
        
        public bool Success { get; set; }
        public bool Subscribe { get; set; }
        public double DareId { get; set; }
        public Types.DareVersatileInformations DareVersatilesInfos { get; set; }
        
        public DareSubscribedMessage()
        {
        }
        
        public DareSubscribedMessage(bool success, bool subscribe, double dareId, Types.DareVersatileInformations dareVersatilesInfos)
        {
            this.Success = success;
            this.Subscribe = subscribe;
            this.DareId = dareId;
            this.DareVersatilesInfos = dareVersatilesInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Success);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Subscribe);
            writer.WriteByte(flag1);
            writer.WriteDouble(DareId);
            DareVersatilesInfos.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag1, 0);
            Subscribe = BooleanByteWrapper.GetFlag(flag1, 1);
            DareId = reader.ReadDouble();
            if (DareId < 0 || DareId > 9007199254740990)
                throw new Exception("Forbidden value on DareId = " + DareId + ", it doesn't respect the following condition : dareId < 0 || dareId > 9007199254740990");
            DareVersatilesInfos = new Types.DareVersatileInformations();
            DareVersatilesInfos.Deserialize(reader);
        }
        
    }
    
}