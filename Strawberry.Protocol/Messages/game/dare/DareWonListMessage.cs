

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareWonListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6682;
        public override uint MessageID => ProtocolId;
        
        public double[] DareId { get; set; }
        
        public DareWonListMessage()
        {
        }
        
        public DareWonListMessage(double[] dareId)
        {
            this.DareId = dareId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)DareId.Length);
            foreach (var entry in DareId)
            {
                 writer.WriteDouble(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            DareId = new double[limit];
            for (int i = 0; i < limit; i++)
            {
                 DareId[i] = reader.ReadDouble();
            }
        }
        
    }
    
}