

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareCreationRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6665;
        public override uint MessageID => ProtocolId;
        
        public bool IsPrivate { get; set; }
        public bool IsForGuild { get; set; }
        public bool IsForAlliance { get; set; }
        public bool NeedNotifications { get; set; }
        public ulong SubscriptionFee { get; set; }
        public ulong Jackpot { get; set; }
        public ushort MaxCountWinners { get; set; }
        public uint DelayBeforeStart { get; set; }
        public uint Duration { get; set; }
        public Types.DareCriteria[] Criterions { get; set; }
        
        public DareCreationRequestMessage()
        {
        }
        
        public DareCreationRequestMessage(bool isPrivate, bool isForGuild, bool isForAlliance, bool needNotifications, ulong subscriptionFee, ulong jackpot, ushort maxCountWinners, uint delayBeforeStart, uint duration, Types.DareCriteria[] criterions)
        {
            this.IsPrivate = isPrivate;
            this.IsForGuild = isForGuild;
            this.IsForAlliance = isForAlliance;
            this.NeedNotifications = needNotifications;
            this.SubscriptionFee = subscriptionFee;
            this.Jackpot = jackpot;
            this.MaxCountWinners = maxCountWinners;
            this.DelayBeforeStart = delayBeforeStart;
            this.Duration = duration;
            this.Criterions = criterions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, IsPrivate);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, IsForGuild);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 2, IsForAlliance);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 3, NeedNotifications);
            writer.WriteByte(flag1);
            writer.WriteVarLong(SubscriptionFee);
            writer.WriteVarLong(Jackpot);
            writer.WriteUShort(MaxCountWinners);
            writer.WriteUInt(DelayBeforeStart);
            writer.WriteUInt(Duration);
            writer.WriteUShort((ushort)Criterions.Length);
            foreach (var entry in Criterions)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            IsPrivate = BooleanByteWrapper.GetFlag(flag1, 0);
            IsForGuild = BooleanByteWrapper.GetFlag(flag1, 1);
            IsForAlliance = BooleanByteWrapper.GetFlag(flag1, 2);
            NeedNotifications = BooleanByteWrapper.GetFlag(flag1, 3);
            SubscriptionFee = reader.ReadVarUhLong();
            if (SubscriptionFee < 0 || SubscriptionFee > 9007199254740990)
                throw new Exception("Forbidden value on SubscriptionFee = " + SubscriptionFee + ", it doesn't respect the following condition : subscriptionFee < 0 || subscriptionFee > 9007199254740990");
            Jackpot = reader.ReadVarUhLong();
            if (Jackpot < 0 || Jackpot > 9007199254740990)
                throw new Exception("Forbidden value on Jackpot = " + Jackpot + ", it doesn't respect the following condition : jackpot < 0 || jackpot > 9007199254740990");
            MaxCountWinners = reader.ReadUShort();
            if (MaxCountWinners < 0 || MaxCountWinners > 65535)
                throw new Exception("Forbidden value on MaxCountWinners = " + MaxCountWinners + ", it doesn't respect the following condition : maxCountWinners < 0 || maxCountWinners > 65535");
            DelayBeforeStart = reader.ReadUInt();
            if (DelayBeforeStart < 0 || DelayBeforeStart > 4294967295)
                throw new Exception("Forbidden value on DelayBeforeStart = " + DelayBeforeStart + ", it doesn't respect the following condition : delayBeforeStart < 0 || delayBeforeStart > 4294967295");
            Duration = reader.ReadUInt();
            if (Duration < 0 || Duration > 4294967295)
                throw new Exception("Forbidden value on Duration = " + Duration + ", it doesn't respect the following condition : duration < 0 || duration > 4294967295");
            var limit = reader.ReadUShort();
            Criterions = new Types.DareCriteria[limit];
            for (int i = 0; i < limit; i++)
            {
                 Criterions[i] = new Types.DareCriteria();
                 Criterions[i].Deserialize(reader);
            }
        }
        
    }
    
}