

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareRewardWonMessage : NetworkMessage
    {
        public const uint ProtocolId = 6678;
        public override uint MessageID => ProtocolId;
        
        public Types.DareReward Reward { get; set; }
        
        public DareRewardWonMessage()
        {
        }
        
        public DareRewardWonMessage(Types.DareReward reward)
        {
            this.Reward = reward;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Reward.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Reward = new Types.DareReward();
            Reward.Deserialize(reader);
        }
        
    }
    
}