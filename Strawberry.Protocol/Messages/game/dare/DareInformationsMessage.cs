

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DareInformationsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6656;
        public override uint MessageID => ProtocolId;
        
        public Types.DareInformations DareFixedInfos { get; set; }
        public Types.DareVersatileInformations DareVersatilesInfos { get; set; }
        
        public DareInformationsMessage()
        {
        }
        
        public DareInformationsMessage(Types.DareInformations dareFixedInfos, Types.DareVersatileInformations dareVersatilesInfos)
        {
            this.DareFixedInfos = dareFixedInfos;
            this.DareVersatilesInfos = dareVersatilesInfos;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            DareFixedInfos.Serialize(writer);
            DareVersatilesInfos.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DareFixedInfos = new Types.DareInformations();
            DareFixedInfos.Deserialize(reader);
            DareVersatilesInfos = new Types.DareVersatileInformations();
            DareVersatilesInfos.Deserialize(reader);
        }
        
    }
    
}