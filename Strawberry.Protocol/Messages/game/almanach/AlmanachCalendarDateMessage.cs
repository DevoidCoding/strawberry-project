

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AlmanachCalendarDateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6341;
        public override uint MessageID => ProtocolId;
        
        public int Date { get; set; }
        
        public AlmanachCalendarDateMessage()
        {
        }
        
        public AlmanachCalendarDateMessage(int date)
        {
            this.Date = date;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(Date);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Date = reader.ReadInt();
        }
        
    }
    
}