

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BulletinMessage : SocialNoticeMessage
    {
        public new const uint ProtocolId = 6695;
        public override uint MessageID => ProtocolId;
        
        public int LastNotifiedTimestamp { get; set; }
        
        public BulletinMessage()
        {
        }
        
        public BulletinMessage(string content, int timestamp, ulong memberId, string memberName, int lastNotifiedTimestamp)
         : base(content, timestamp, memberId, memberName)
        {
            this.LastNotifiedTimestamp = lastNotifiedTimestamp;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteInt(LastNotifiedTimestamp);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            LastNotifiedTimestamp = reader.ReadInt();
            if (LastNotifiedTimestamp < 0)
                throw new Exception("Forbidden value on LastNotifiedTimestamp = " + LastNotifiedTimestamp + ", it doesn't respect the following condition : lastNotifiedTimestamp < 0");
        }
        
    }
    
}