

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ContactLookMessage : NetworkMessage
    {
        public const uint ProtocolId = 5934;
        public override uint MessageID => ProtocolId;
        
        public uint RequestId { get; set; }
        public string PlayerName { get; set; }
        public ulong PlayerId { get; set; }
        public Types.EntityLook Look { get; set; }
        
        public ContactLookMessage()
        {
        }
        
        public ContactLookMessage(uint requestId, string playerName, ulong playerId, Types.EntityLook look)
        {
            this.RequestId = requestId;
            this.PlayerName = playerName;
            this.PlayerId = playerId;
            this.Look = look;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(RequestId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarLong(PlayerId);
            Look.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadVarUhInt();
            if (RequestId < 0)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0");
            PlayerName = reader.ReadUTF();
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            Look = new Types.EntityLook();
            Look.Deserialize(reader);
        }
        
    }
    
}