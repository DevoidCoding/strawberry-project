

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SocialNoticeMessage : NetworkMessage
    {
        public const uint ProtocolId = 6688;
        public override uint MessageID => ProtocolId;
        
        public string Content { get; set; }
        public int Timestamp { get; set; }
        public ulong MemberId { get; set; }
        public string MemberName { get; set; }
        
        public SocialNoticeMessage()
        {
        }
        
        public SocialNoticeMessage(string content, int timestamp, ulong memberId, string memberName)
        {
            this.Content = content;
            this.Timestamp = timestamp;
            this.MemberId = memberId;
            this.MemberName = memberName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Content);
            writer.WriteInt(Timestamp);
            writer.WriteVarLong(MemberId);
            writer.WriteUTF(MemberName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Content = reader.ReadUTF();
            Timestamp = reader.ReadInt();
            if (Timestamp < 0)
                throw new Exception("Forbidden value on Timestamp = " + Timestamp + ", it doesn't respect the following condition : timestamp < 0");
            MemberId = reader.ReadVarUhLong();
            if (MemberId < 0 || MemberId > 9007199254740990)
                throw new Exception("Forbidden value on MemberId = " + MemberId + ", it doesn't respect the following condition : memberId < 0 || memberId > 9007199254740990");
            MemberName = reader.ReadUTF();
        }
        
    }
    
}