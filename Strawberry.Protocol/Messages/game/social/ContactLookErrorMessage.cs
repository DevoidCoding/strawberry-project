

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ContactLookErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6045;
        public override uint MessageID => ProtocolId;
        
        public uint RequestId { get; set; }
        
        public ContactLookErrorMessage()
        {
        }
        
        public ContactLookErrorMessage(uint requestId)
        {
            this.RequestId = requestId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(RequestId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadVarUhInt();
            if (RequestId < 0)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0");
        }
        
    }
    
}