

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ContactLookRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5932;
        public override uint MessageID => ProtocolId;
        
        public byte RequestId { get; set; }
        public sbyte ContactType { get; set; }
        
        public ContactLookRequestMessage()
        {
        }
        
        public ContactLookRequestMessage(byte requestId, sbyte contactType)
        {
            this.RequestId = requestId;
            this.ContactType = contactType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteByte(RequestId);
            writer.WriteSByte(ContactType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            RequestId = reader.ReadByte();
            if (RequestId < 0 || RequestId > 255)
                throw new Exception("Forbidden value on RequestId = " + RequestId + ", it doesn't respect the following condition : requestId < 0 || requestId > 255");
            ContactType = reader.ReadSByte();
            if (ContactType < 0)
                throw new Exception("Forbidden value on ContactType = " + ContactType + ", it doesn't respect the following condition : contactType < 0");
        }
        
    }
    
}