

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NumericWhoIsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6297;
        public override uint MessageID => ProtocolId;
        
        public ulong PlayerId { get; set; }
        public int AccountId { get; set; }
        
        public NumericWhoIsMessage()
        {
        }
        
        public NumericWhoIsMessage(ulong playerId, int accountId)
        {
            this.PlayerId = playerId;
            this.AccountId = accountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarLong(PlayerId);
            writer.WriteInt(AccountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
        }
        
    }
    
}