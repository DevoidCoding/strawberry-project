

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CurrentServerStatusUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6525;
        public override uint MessageID => ProtocolId;
        
        public sbyte Status { get; set; }
        
        public CurrentServerStatusUpdateMessage()
        {
        }
        
        public CurrentServerStatusUpdateMessage(sbyte status)
        {
            this.Status = status;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Status);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Status = reader.ReadSByte();
            if (Status < 0)
                throw new Exception("Forbidden value on Status = " + Status + ", it doesn't respect the following condition : status < 0");
        }
        
    }
    
}