

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicDateMessage : NetworkMessage
    {
        public const uint ProtocolId = 177;
        public override uint MessageID => ProtocolId;
        
        public sbyte Day { get; set; }
        public sbyte Month { get; set; }
        public short Year { get; set; }
        
        public BasicDateMessage()
        {
        }
        
        public BasicDateMessage(sbyte day, sbyte month, short year)
        {
            this.Day = day;
            this.Month = month;
            this.Year = year;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Day);
            writer.WriteSByte(Month);
            writer.WriteShort(Year);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Day = reader.ReadSByte();
            if (Day < 0)
                throw new Exception("Forbidden value on Day = " + Day + ", it doesn't respect the following condition : day < 0");
            Month = reader.ReadSByte();
            if (Month < 0)
                throw new Exception("Forbidden value on Month = " + Month + ", it doesn't respect the following condition : month < 0");
            Year = reader.ReadShort();
            if (Year < 0)
                throw new Exception("Forbidden value on Year = " + Year + ", it doesn't respect the following condition : year < 0");
        }
        
    }
    
}