

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class BasicLatencyStatsMessage : NetworkMessage
    {
        public const uint ProtocolId = 5663;
        public override uint MessageID => ProtocolId;
        
        public ushort Latency { get; set; }
        public ushort SampleCount { get; set; }
        public ushort Max { get; set; }
        
        public BasicLatencyStatsMessage()
        {
        }
        
        public BasicLatencyStatsMessage(ushort latency, ushort sampleCount, ushort max)
        {
            this.Latency = latency;
            this.SampleCount = sampleCount;
            this.Max = max;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort(Latency);
            writer.WriteVarShort(SampleCount);
            writer.WriteVarShort(Max);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Latency = reader.ReadUShort();
            if (Latency < 0 || Latency > 65535)
                throw new Exception("Forbidden value on Latency = " + Latency + ", it doesn't respect the following condition : latency < 0 || latency > 65535");
            SampleCount = reader.ReadVarUhShort();
            if (SampleCount < 0)
                throw new Exception("Forbidden value on SampleCount = " + SampleCount + ", it doesn't respect the following condition : sampleCount < 0");
            Max = reader.ReadVarUhShort();
            if (Max < 0)
                throw new Exception("Forbidden value on Max = " + Max + ", it doesn't respect the following condition : max < 0");
        }
        
    }
    
}