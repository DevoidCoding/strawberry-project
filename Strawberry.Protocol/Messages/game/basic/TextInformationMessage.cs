

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TextInformationMessage : NetworkMessage
    {
        public const uint ProtocolId = 780;
        public override uint MessageID => ProtocolId;
        
        public sbyte MsgType { get; set; }
        public ushort MsgId { get; set; }
        public string[] Parameters { get; set; }
        
        public TextInformationMessage()
        {
        }
        
        public TextInformationMessage(sbyte msgType, ushort msgId, string[] parameters)
        {
            this.MsgType = msgType;
            this.MsgId = msgId;
            this.Parameters = parameters;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(MsgType);
            writer.WriteVarShort(MsgId);
            writer.WriteUShort((ushort)Parameters.Length);
            foreach (var entry in Parameters)
            {
                 writer.WriteUTF(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            MsgType = reader.ReadSByte();
            if (MsgType < 0)
                throw new Exception("Forbidden value on MsgType = " + MsgType + ", it doesn't respect the following condition : msgType < 0");
            MsgId = reader.ReadVarUhShort();
            if (MsgId < 0)
                throw new Exception("Forbidden value on MsgId = " + MsgId + ", it doesn't respect the following condition : msgId < 0");
            var limit = reader.ReadUShort();
            Parameters = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Parameters[i] = reader.ReadUTF();
            }
        }
        
    }
    
}