

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicWhoIsNoMatchMessage : NetworkMessage
    {
        public const uint ProtocolId = 179;
        public override uint MessageID => ProtocolId;
        
        public string Search { get; set; }
        
        public BasicWhoIsNoMatchMessage()
        {
        }
        
        public BasicWhoIsNoMatchMessage(string search)
        {
            this.Search = search;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Search);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Search = reader.ReadUTF();
        }
        
    }
    
}