

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicWhoIsMessage : NetworkMessage
    {
        public const uint ProtocolId = 180;
        public override uint MessageID => ProtocolId;
        
        public bool Self { get; set; }
        public bool Verbose { get; set; }
        public sbyte Position { get; set; }
        public string AccountNickname { get; set; }
        public int AccountId { get; set; }
        public string PlayerName { get; set; }
        public ulong PlayerId { get; set; }
        public short AreaId { get; set; }
        public short ServerId { get; set; }
        public short OriginServerId { get; set; }
        public Types.AbstractSocialGroupInfos[] SocialGroups { get; set; }
        public sbyte PlayerState { get; set; }
        
        public BasicWhoIsMessage()
        {
        }
        
        public BasicWhoIsMessage(bool self, bool verbose, sbyte position, string accountNickname, int accountId, string playerName, ulong playerId, short areaId, short serverId, short originServerId, Types.AbstractSocialGroupInfos[] socialGroups, sbyte playerState)
        {
            this.Self = self;
            this.Verbose = verbose;
            this.Position = position;
            this.AccountNickname = accountNickname;
            this.AccountId = accountId;
            this.PlayerName = playerName;
            this.PlayerId = playerId;
            this.AreaId = areaId;
            this.ServerId = serverId;
            this.OriginServerId = originServerId;
            this.SocialGroups = socialGroups;
            this.PlayerState = playerState;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Self);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Verbose);
            writer.WriteByte(flag1);
            writer.WriteSByte(Position);
            writer.WriteUTF(AccountNickname);
            writer.WriteInt(AccountId);
            writer.WriteUTF(PlayerName);
            writer.WriteVarLong(PlayerId);
            writer.WriteShort(AreaId);
            writer.WriteShort(ServerId);
            writer.WriteShort(OriginServerId);
            writer.WriteUShort((ushort)SocialGroups.Length);
            foreach (var entry in SocialGroups)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
            writer.WriteSByte(PlayerState);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Self = BooleanByteWrapper.GetFlag(flag1, 0);
            Verbose = BooleanByteWrapper.GetFlag(flag1, 1);
            Position = reader.ReadSByte();
            AccountNickname = reader.ReadUTF();
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            PlayerName = reader.ReadUTF();
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            AreaId = reader.ReadShort();
            ServerId = reader.ReadShort();
            OriginServerId = reader.ReadShort();
            var limit = reader.ReadUShort();
            SocialGroups = new Types.AbstractSocialGroupInfos[limit];
            for (int i = 0; i < limit; i++)
            {
                 SocialGroups[i] = Types.ProtocolTypeManager.GetInstance<Types.AbstractSocialGroupInfos>(reader.ReadShort());
                 SocialGroups[i].Deserialize(reader);
            }
            PlayerState = reader.ReadSByte();
            if (PlayerState < 0)
                throw new Exception("Forbidden value on PlayerState = " + PlayerState + ", it doesn't respect the following condition : playerState < 0");
        }
        
    }
    
}