

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicTimeMessage : NetworkMessage
    {
        public const uint ProtocolId = 175;
        public override uint MessageID => ProtocolId;
        
        public double Timestamp { get; set; }
        public short TimezoneOffset { get; set; }
        
        public BasicTimeMessage()
        {
        }
        
        public BasicTimeMessage(double timestamp, short timezoneOffset)
        {
            this.Timestamp = timestamp;
            this.TimezoneOffset = timezoneOffset;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Timestamp);
            writer.WriteShort(TimezoneOffset);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Timestamp = reader.ReadDouble();
            if (Timestamp < 0 || Timestamp > 9007199254740990)
                throw new Exception("Forbidden value on Timestamp = " + Timestamp + ", it doesn't respect the following condition : timestamp < 0 || timestamp > 9007199254740990");
            TimezoneOffset = reader.ReadShort();
        }
        
    }
    
}