

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class BasicWhoIsRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 181;
        public override uint MessageID => ProtocolId;
        
        public bool Verbose { get; set; }
        public string Search { get; set; }
        
        public BasicWhoIsRequestMessage()
        {
        }
        
        public BasicWhoIsRequestMessage(bool verbose, string search)
        {
            this.Verbose = verbose;
            this.Search = search;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Verbose);
            writer.WriteUTF(Search);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Verbose = reader.ReadBoolean();
            Search = reader.ReadUTF();
        }
        
    }
    
}