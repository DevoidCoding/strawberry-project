

// Generated on 02/12/2018 03:56:13
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicAckMessage : NetworkMessage
    {
        public const uint ProtocolId = 6362;
        public override uint MessageID => ProtocolId;
        
        public uint Seq { get; set; }
        public ushort LastPacketId { get; set; }
        
        public BasicAckMessage()
        {
        }
        
        public BasicAckMessage(uint seq, ushort lastPacketId)
        {
            this.Seq = seq;
            this.LastPacketId = lastPacketId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Seq);
            writer.WriteVarShort(LastPacketId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Seq = reader.ReadVarUhInt();
            if (Seq < 0)
                throw new Exception("Forbidden value on Seq = " + Seq + ", it doesn't respect the following condition : seq < 0");
            LastPacketId = reader.ReadVarUhShort();
            if (LastPacketId < 0)
                throw new Exception("Forbidden value on LastPacketId = " + LastPacketId + ", it doesn't respect the following condition : lastPacketId < 0");
        }
        
    }
    
}