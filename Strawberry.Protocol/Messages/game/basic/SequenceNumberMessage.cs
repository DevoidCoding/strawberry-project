

// Generated on 02/12/2018 03:56:14
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SequenceNumberMessage : NetworkMessage
    {
        public const uint ProtocolId = 6317;
        public override uint MessageID => ProtocolId;
        
        public ushort Number { get; set; }
        
        public SequenceNumberMessage()
        {
        }
        
        public SequenceNumberMessage(ushort number)
        {
            this.Number = number;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort(Number);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Number = reader.ReadUShort();
            if (Number < 0 || Number > 65535)
                throw new Exception("Forbidden value on Number = " + Number + ", it doesn't respect the following condition : number < 0 || number > 65535");
        }
        
    }
    
}