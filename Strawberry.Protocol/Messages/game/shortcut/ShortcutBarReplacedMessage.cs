

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShortcutBarReplacedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6706;
        public override uint MessageID => ProtocolId;
        
        public sbyte BarType { get; set; }
        public Types.Shortcut Shortcut { get; set; }
        
        public ShortcutBarReplacedMessage()
        {
        }
        
        public ShortcutBarReplacedMessage(sbyte barType, Types.Shortcut shortcut)
        {
            this.BarType = barType;
            this.Shortcut = shortcut;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(BarType);
            writer.WriteShort(Shortcut.TypeID);
            Shortcut.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BarType = reader.ReadSByte();
            if (BarType < 0)
                throw new Exception("Forbidden value on BarType = " + BarType + ", it doesn't respect the following condition : barType < 0");
            Shortcut = Types.ProtocolTypeManager.GetInstance<Types.Shortcut>(reader.ReadShort());
            Shortcut.Deserialize(reader);
        }
        
    }
    
}