

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShortcutBarSwapRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6230;
        public override uint MessageID => ProtocolId;
        
        public sbyte BarType { get; set; }
        public sbyte FirstSlot { get; set; }
        public sbyte SecondSlot { get; set; }
        
        public ShortcutBarSwapRequestMessage()
        {
        }
        
        public ShortcutBarSwapRequestMessage(sbyte barType, sbyte firstSlot, sbyte secondSlot)
        {
            this.BarType = barType;
            this.FirstSlot = firstSlot;
            this.SecondSlot = secondSlot;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(BarType);
            writer.WriteSByte(FirstSlot);
            writer.WriteSByte(SecondSlot);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BarType = reader.ReadSByte();
            if (BarType < 0)
                throw new Exception("Forbidden value on BarType = " + BarType + ", it doesn't respect the following condition : barType < 0");
            FirstSlot = reader.ReadSByte();
            if (FirstSlot < 0 || FirstSlot > 99)
                throw new Exception("Forbidden value on FirstSlot = " + FirstSlot + ", it doesn't respect the following condition : firstSlot < 0 || firstSlot > 99");
            SecondSlot = reader.ReadSByte();
            if (SecondSlot < 0 || SecondSlot > 99)
                throw new Exception("Forbidden value on SecondSlot = " + SecondSlot + ", it doesn't respect the following condition : secondSlot < 0 || secondSlot > 99");
        }
        
    }
    
}