

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShortcutBarContentMessage : NetworkMessage
    {
        public const uint ProtocolId = 6231;
        public override uint MessageID => ProtocolId;
        
        public sbyte BarType { get; set; }
        public Types.Shortcut[] Shortcuts { get; set; }
        
        public ShortcutBarContentMessage()
        {
        }
        
        public ShortcutBarContentMessage(sbyte barType, Types.Shortcut[] shortcuts)
        {
            this.BarType = barType;
            this.Shortcuts = shortcuts;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(BarType);
            writer.WriteUShort((ushort)Shortcuts.Length);
            foreach (var entry in Shortcuts)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BarType = reader.ReadSByte();
            if (BarType < 0)
                throw new Exception("Forbidden value on BarType = " + BarType + ", it doesn't respect the following condition : barType < 0");
            var limit = reader.ReadUShort();
            Shortcuts = new Types.Shortcut[limit];
            for (int i = 0; i < limit; i++)
            {
                 Shortcuts[i] = Types.ProtocolTypeManager.GetInstance<Types.Shortcut>(reader.ReadShort());
                 Shortcuts[i].Deserialize(reader);
            }
        }
        
    }
    
}