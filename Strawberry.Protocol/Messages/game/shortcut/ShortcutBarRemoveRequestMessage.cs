

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShortcutBarRemoveRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6228;
        public override uint MessageID => ProtocolId;
        
        public sbyte BarType { get; set; }
        public sbyte Slot { get; set; }
        
        public ShortcutBarRemoveRequestMessage()
        {
        }
        
        public ShortcutBarRemoveRequestMessage(sbyte barType, sbyte slot)
        {
            this.BarType = barType;
            this.Slot = slot;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(BarType);
            writer.WriteSByte(Slot);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            BarType = reader.ReadSByte();
            if (BarType < 0)
                throw new Exception("Forbidden value on BarType = " + BarType + ", it doesn't respect the following condition : barType < 0");
            Slot = reader.ReadSByte();
            if (Slot < 0 || Slot > 99)
                throw new Exception("Forbidden value on Slot = " + Slot + ", it doesn't respect the following condition : slot < 0 || slot > 99");
        }
        
    }
    
}