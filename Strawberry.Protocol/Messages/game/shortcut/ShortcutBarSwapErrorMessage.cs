

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ShortcutBarSwapErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6226;
        public override uint MessageID => ProtocolId;
        
        public sbyte Error { get; set; }
        
        public ShortcutBarSwapErrorMessage()
        {
        }
        
        public ShortcutBarSwapErrorMessage(sbyte error)
        {
            this.Error = error;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Error);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Error = reader.ReadSByte();
            if (Error < 0)
                throw new Exception("Forbidden value on Error = " + Error + ", it doesn't respect the following condition : error < 0");
        }
        
    }
    
}