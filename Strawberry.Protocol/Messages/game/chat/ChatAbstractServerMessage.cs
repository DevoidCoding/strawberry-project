

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatAbstractServerMessage : NetworkMessage
    {
        public const uint ProtocolId = 880;
        public override uint MessageID => ProtocolId;
        
        public sbyte Channel { get; set; }
        public string Content { get; set; }
        public int Timestamp { get; set; }
        public string Fingerprint { get; set; }
        
        public ChatAbstractServerMessage()
        {
        }
        
        public ChatAbstractServerMessage(sbyte channel, string content, int timestamp, string fingerprint)
        {
            this.Channel = channel;
            this.Content = content;
            this.Timestamp = timestamp;
            this.Fingerprint = fingerprint;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Channel);
            writer.WriteUTF(Content);
            writer.WriteInt(Timestamp);
            writer.WriteUTF(Fingerprint);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Channel = reader.ReadSByte();
            if (Channel < 0)
                throw new Exception("Forbidden value on Channel = " + Channel + ", it doesn't respect the following condition : channel < 0");
            Content = reader.ReadUTF();
            Timestamp = reader.ReadInt();
            if (Timestamp < 0)
                throw new Exception("Forbidden value on Timestamp = " + Timestamp + ", it doesn't respect the following condition : timestamp < 0");
            Fingerprint = reader.ReadUTF();
        }
        
    }
    
}