

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class ChatClientMultiWithObjectMessage : ChatClientMultiMessage
    {
        public new const uint ProtocolId = 862;
        public override uint MessageID => ProtocolId;
        
        public Types.ObjectItem[] Objects { get; set; }
        
        public ChatClientMultiWithObjectMessage()
        {
        }
        
        public ChatClientMultiWithObjectMessage(string content, sbyte channel, Types.ObjectItem[] objects)
         : base(content, channel)
        {
            this.Objects = objects;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Objects.Length);
            foreach (var entry in Objects)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Objects = new Types.ObjectItem[limit];
            for (int i = 0; i < limit; i++)
            {
                 Objects[i] = new Types.ObjectItem();
                 Objects[i].Deserialize(reader);
            }
        }
        
    }
    
}