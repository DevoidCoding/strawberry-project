

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatServerMessage : ChatAbstractServerMessage
    {
        public new const uint ProtocolId = 881;
        public override uint MessageID => ProtocolId;
        
        public double SenderId { get; set; }
        public string SenderName { get; set; }
        public int SenderAccountId { get; set; }
        
        public ChatServerMessage()
        {
        }
        
        public ChatServerMessage(sbyte channel, string content, int timestamp, string fingerprint, double senderId, string senderName, int senderAccountId)
         : base(channel, content, timestamp, fingerprint)
        {
            this.SenderId = senderId;
            this.SenderName = senderName;
            this.SenderAccountId = senderAccountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(SenderId);
            writer.WriteUTF(SenderName);
            writer.WriteInt(SenderAccountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            SenderId = reader.ReadDouble();
            if (SenderId < -9007199254740990 || SenderId > 9007199254740990)
                throw new Exception("Forbidden value on SenderId = " + SenderId + ", it doesn't respect the following condition : senderId < -9007199254740990 || senderId > 9007199254740990");
            SenderName = reader.ReadUTF();
            SenderAccountId = reader.ReadInt();
            if (SenderAccountId < 0)
                throw new Exception("Forbidden value on SenderAccountId = " + SenderAccountId + ", it doesn't respect the following condition : senderAccountId < 0");
        }
        
    }
    
}