

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatMessageReportMessage : NetworkMessage
    {
        public const uint ProtocolId = 821;
        public override uint MessageID => ProtocolId;
        
        public string SenderName { get; set; }
        public string Content { get; set; }
        public int Timestamp { get; set; }
        public sbyte Channel { get; set; }
        public string Fingerprint { get; set; }
        public sbyte Reason { get; set; }
        
        public ChatMessageReportMessage()
        {
        }
        
        public ChatMessageReportMessage(string senderName, string content, int timestamp, sbyte channel, string fingerprint, sbyte reason)
        {
            this.SenderName = senderName;
            this.Content = content;
            this.Timestamp = timestamp;
            this.Channel = channel;
            this.Fingerprint = fingerprint;
            this.Reason = reason;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(SenderName);
            writer.WriteUTF(Content);
            writer.WriteInt(Timestamp);
            writer.WriteSByte(Channel);
            writer.WriteUTF(Fingerprint);
            writer.WriteSByte(Reason);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SenderName = reader.ReadUTF();
            Content = reader.ReadUTF();
            Timestamp = reader.ReadInt();
            if (Timestamp < 0)
                throw new Exception("Forbidden value on Timestamp = " + Timestamp + ", it doesn't respect the following condition : timestamp < 0");
            Channel = reader.ReadSByte();
            if (Channel < 0)
                throw new Exception("Forbidden value on Channel = " + Channel + ", it doesn't respect the following condition : channel < 0");
            Fingerprint = reader.ReadUTF();
            Reason = reader.ReadSByte();
            if (Reason < 0)
                throw new Exception("Forbidden value on Reason = " + Reason + ", it doesn't respect the following condition : reason < 0");
        }
        
    }
    
}