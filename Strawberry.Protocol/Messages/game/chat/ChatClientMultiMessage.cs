

// Generated on 02/12/2018 03:56:15
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class ChatClientMultiMessage : ChatAbstractClientMessage
    {
        public new const uint ProtocolId = 861;
        public override uint MessageID => ProtocolId;
        
        public sbyte Channel { get; set; }
        
        public ChatClientMultiMessage()
        {
        }
        
        public ChatClientMultiMessage(string content, sbyte channel)
         : base(content)
        {
            this.Channel = channel;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteSByte(Channel);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            Channel = reader.ReadSByte();
            if (Channel < 0)
                throw new Exception("Forbidden value on Channel = " + Channel + ", it doesn't respect the following condition : channel < 0");
        }
        
    }
    
}