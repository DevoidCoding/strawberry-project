

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatCommunityChannelCommunityMessage : NetworkMessage
    {
        public const uint ProtocolId = 6730;
        public override uint MessageID => ProtocolId;
        
        public short CommunityId { get; set; }
        
        public ChatCommunityChannelCommunityMessage()
        {
        }
        
        public ChatCommunityChannelCommunityMessage(short communityId)
        {
            this.CommunityId = communityId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(CommunityId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CommunityId = reader.ReadShort();
        }
        
    }
    
}