

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChannelEnablingMessage : NetworkMessage
    {
        public const uint ProtocolId = 890;
        public override uint MessageID => ProtocolId;
        
        public sbyte Channel { get; set; }
        public bool Enable { get; set; }
        
        public ChannelEnablingMessage()
        {
        }
        
        public ChannelEnablingMessage(sbyte channel, bool enable)
        {
            this.Channel = channel;
            this.Enable = enable;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Channel);
            writer.WriteBoolean(Enable);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Channel = reader.ReadSByte();
            if (Channel < 0)
                throw new Exception("Forbidden value on Channel = " + Channel + ", it doesn't respect the following condition : channel < 0");
            Enable = reader.ReadBoolean();
        }
        
    }
    
}