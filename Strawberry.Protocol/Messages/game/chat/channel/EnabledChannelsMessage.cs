

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class EnabledChannelsMessage : NetworkMessage
    {
        public const uint ProtocolId = 892;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] Channels { get; set; }
        public sbyte[] Disallowed { get; set; }
        
        public EnabledChannelsMessage()
        {
        }
        
        public EnabledChannelsMessage(sbyte[] channels, sbyte[] disallowed)
        {
            this.Channels = channels;
            this.Disallowed = disallowed;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Channels.Length);
            foreach (var entry in Channels)
            {
                 writer.WriteSByte(entry);
            }
            writer.WriteUShort((ushort)Disallowed.Length);
            foreach (var entry in Disallowed)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Channels = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Channels[i] = reader.ReadSByte();
            }
            limit = reader.ReadUShort();
            Disallowed = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Disallowed[i] = reader.ReadSByte();
            }
        }
        
    }
    
}