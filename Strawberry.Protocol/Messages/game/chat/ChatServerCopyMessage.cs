

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatServerCopyMessage : ChatAbstractServerMessage
    {
        public new const uint ProtocolId = 882;
        public override uint MessageID => ProtocolId;
        
        public ulong ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        
        public ChatServerCopyMessage()
        {
        }
        
        public ChatServerCopyMessage(sbyte channel, string content, int timestamp, string fingerprint, ulong receiverId, string receiverName)
         : base(channel, content, timestamp, fingerprint)
        {
            this.ReceiverId = receiverId;
            this.ReceiverName = receiverName;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteVarLong(ReceiverId);
            writer.WriteUTF(ReceiverName);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            ReceiverId = reader.ReadVarUhLong();
            if (ReceiverId < 0 || ReceiverId > 9007199254740990)
                throw new Exception("Forbidden value on ReceiverId = " + ReceiverId + ", it doesn't respect the following condition : receiverId < 0 || receiverId > 9007199254740990");
            ReceiverName = reader.ReadUTF();
        }
        
    }
    
}