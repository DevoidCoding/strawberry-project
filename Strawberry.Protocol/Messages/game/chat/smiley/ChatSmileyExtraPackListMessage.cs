

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatSmileyExtraPackListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6596;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] PackIds { get; set; }
        
        public ChatSmileyExtraPackListMessage()
        {
        }
        
        public ChatSmileyExtraPackListMessage(sbyte[] packIds)
        {
            this.PackIds = packIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)PackIds.Length);
            foreach (var entry in PackIds)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            PackIds = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 PackIds[i] = reader.ReadSByte();
            }
        }
        
    }
    
}