

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MoodSmileyResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 6196;
        public override uint MessageID => ProtocolId;
        
        public sbyte ResultCode { get; set; }
        public ushort SmileyId { get; set; }
        
        public MoodSmileyResultMessage()
        {
        }
        
        public MoodSmileyResultMessage(sbyte resultCode, ushort smileyId)
        {
            this.ResultCode = resultCode;
            this.SmileyId = smileyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ResultCode);
            writer.WriteVarShort(SmileyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ResultCode = reader.ReadSByte();
            if (ResultCode < 0)
                throw new Exception("Forbidden value on ResultCode = " + ResultCode + ", it doesn't respect the following condition : resultCode < 0");
            SmileyId = reader.ReadVarUhShort();
            if (SmileyId < 0)
                throw new Exception("Forbidden value on SmileyId = " + SmileyId + ", it doesn't respect the following condition : smileyId < 0");
        }
        
    }
    
}