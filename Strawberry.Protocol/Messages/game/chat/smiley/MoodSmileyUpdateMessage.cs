

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MoodSmileyUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6388;
        public override uint MessageID => ProtocolId;
        
        public int AccountId { get; set; }
        public ulong PlayerId { get; set; }
        public ushort SmileyId { get; set; }
        
        public MoodSmileyUpdateMessage()
        {
        }
        
        public MoodSmileyUpdateMessage(int accountId, ulong playerId, ushort smileyId)
        {
            this.AccountId = accountId;
            this.PlayerId = playerId;
            this.SmileyId = smileyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AccountId);
            writer.WriteVarLong(PlayerId);
            writer.WriteVarShort(SmileyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            PlayerId = reader.ReadVarUhLong();
            if (PlayerId < 0 || PlayerId > 9007199254740990)
                throw new Exception("Forbidden value on PlayerId = " + PlayerId + ", it doesn't respect the following condition : playerId < 0 || playerId > 9007199254740990");
            SmileyId = reader.ReadVarUhShort();
            if (SmileyId < 0)
                throw new Exception("Forbidden value on SmileyId = " + SmileyId + ", it doesn't respect the following condition : smileyId < 0");
        }
        
    }
    
}