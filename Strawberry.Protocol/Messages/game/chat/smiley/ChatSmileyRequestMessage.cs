

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatSmileyRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 800;
        public override uint MessageID => ProtocolId;
        
        public ushort SmileyId { get; set; }
        
        public ChatSmileyRequestMessage()
        {
        }
        
        public ChatSmileyRequestMessage(ushort smileyId)
        {
            this.SmileyId = smileyId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(SmileyId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SmileyId = reader.ReadVarUhShort();
            if (SmileyId < 0)
                throw new Exception("Forbidden value on SmileyId = " + SmileyId + ", it doesn't respect the following condition : smileyId < 0");
        }
        
    }
    
}