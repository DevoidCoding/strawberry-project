

// Generated on 02/12/2018 03:56:16
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ChatSmileyMessage : NetworkMessage
    {
        public const uint ProtocolId = 801;
        public override uint MessageID => ProtocolId;
        
        public double EntityId { get; set; }
        public ushort SmileyId { get; set; }
        public int AccountId { get; set; }
        
        public ChatSmileyMessage()
        {
        }
        
        public ChatSmileyMessage(double entityId, ushort smileyId, int accountId)
        {
            this.EntityId = entityId;
            this.SmileyId = smileyId;
            this.AccountId = accountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(EntityId);
            writer.WriteVarShort(SmileyId);
            writer.WriteInt(AccountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            EntityId = reader.ReadDouble();
            if (EntityId < -9007199254740990 || EntityId > 9007199254740990)
                throw new Exception("Forbidden value on EntityId = " + EntityId + ", it doesn't respect the following condition : entityId < -9007199254740990 || entityId > 9007199254740990");
            SmileyId = reader.ReadVarUhShort();
            if (SmileyId < 0)
                throw new Exception("Forbidden value on SmileyId = " + SmileyId + ", it doesn't respect the following condition : smileyId < 0");
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
        }
        
    }
    
}