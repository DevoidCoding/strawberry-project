

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class URLOpenMessage : NetworkMessage
    {
        public const uint ProtocolId = 6266;
        public override uint MessageID => ProtocolId;
        
        public sbyte UrlId { get; set; }
        
        public URLOpenMessage()
        {
        }
        
        public URLOpenMessage(sbyte urlId)
        {
            this.UrlId = urlId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(UrlId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            UrlId = reader.ReadSByte();
            if (UrlId < 0)
                throw new Exception("Forbidden value on UrlId = " + UrlId + ", it doesn't respect the following condition : urlId < 0");
        }
        
    }
    
}