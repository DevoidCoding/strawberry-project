

// Generated on 02/12/2018 03:56:46
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CinematicMessage : NetworkMessage
    {
        public const uint ProtocolId = 6053;
        public override uint MessageID => ProtocolId;
        
        public ushort CinematicId { get; set; }
        
        public CinematicMessage()
        {
        }
        
        public CinematicMessage(ushort cinematicId)
        {
            this.CinematicId = cinematicId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CinematicId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CinematicId = reader.ReadVarUhShort();
            if (CinematicId < 0)
                throw new Exception("Forbidden value on CinematicId = " + CinematicId + ", it doesn't respect the following condition : cinematicId < 0");
        }
        
    }
    
}