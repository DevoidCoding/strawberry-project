

// Generated on 02/12/2018 03:56:44
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class PopupWarningMessage : NetworkMessage
    {
        public const uint ProtocolId = 6134;
        public override uint MessageID => ProtocolId;
        
        public string Author { get; set; }
        public string Content { get; set; }
        
        public PopupWarningMessage()
        {
        }
        
        public PopupWarningMessage(string author, string content)
        {
            this.Author = author;
            this.Content = content;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Author);
            writer.WriteUTF(Content);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Author = reader.ReadUTF();
            Content = reader.ReadUTF();
        }
        
    }
    
}