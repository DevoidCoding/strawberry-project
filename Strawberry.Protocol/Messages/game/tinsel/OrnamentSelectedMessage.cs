

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class OrnamentSelectedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6369;
        public override uint MessageID => ProtocolId;
        
        public ushort OrnamentId { get; set; }
        
        public OrnamentSelectedMessage()
        {
        }
        
        public OrnamentSelectedMessage(ushort ornamentId)
        {
            this.OrnamentId = ornamentId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(OrnamentId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            OrnamentId = reader.ReadVarUhShort();
            if (OrnamentId < 0)
                throw new Exception("Forbidden value on OrnamentId = " + OrnamentId + ", it doesn't respect the following condition : ornamentId < 0");
        }
        
    }
    
}