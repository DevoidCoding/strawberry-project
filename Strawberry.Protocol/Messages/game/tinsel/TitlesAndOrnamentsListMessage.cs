

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TitlesAndOrnamentsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6367;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Titles { get; set; }
        public ushort[] Ornaments { get; set; }
        public ushort ActiveTitle { get; set; }
        public ushort ActiveOrnament { get; set; }
        
        public TitlesAndOrnamentsListMessage()
        {
        }
        
        public TitlesAndOrnamentsListMessage(ushort[] titles, ushort[] ornaments, ushort activeTitle, ushort activeOrnament)
        {
            this.Titles = titles;
            this.Ornaments = ornaments;
            this.ActiveTitle = activeTitle;
            this.ActiveOrnament = activeOrnament;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Titles.Length);
            foreach (var entry in Titles)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteUShort((ushort)Ornaments.Length);
            foreach (var entry in Ornaments)
            {
                 writer.WriteVarShort(entry);
            }
            writer.WriteVarShort(ActiveTitle);
            writer.WriteVarShort(ActiveOrnament);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Titles = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Titles[i] = reader.ReadVarUhShort();
            }
            limit = reader.ReadUShort();
            Ornaments = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ornaments[i] = reader.ReadVarUhShort();
            }
            ActiveTitle = reader.ReadVarUhShort();
            if (ActiveTitle < 0)
                throw new Exception("Forbidden value on ActiveTitle = " + ActiveTitle + ", it doesn't respect the following condition : activeTitle < 0");
            ActiveOrnament = reader.ReadVarUhShort();
            if (ActiveOrnament < 0)
                throw new Exception("Forbidden value on ActiveOrnament = " + ActiveOrnament + ", it doesn't respect the following condition : activeOrnament < 0");
        }
        
    }
    
}