

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class OrnamentGainedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6368;
        public override uint MessageID => ProtocolId;
        
        public short OrnamentId { get; set; }
        
        public OrnamentGainedMessage()
        {
        }
        
        public OrnamentGainedMessage(short ornamentId)
        {
            this.OrnamentId = ornamentId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(OrnamentId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            OrnamentId = reader.ReadShort();
            if (OrnamentId < 0)
                throw new Exception("Forbidden value on OrnamentId = " + OrnamentId + ", it doesn't respect the following condition : ornamentId < 0");
        }
        
    }
    
}