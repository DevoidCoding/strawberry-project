

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TitleSelectRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6365;
        public override uint MessageID => ProtocolId;
        
        public ushort TitleId { get; set; }
        
        public TitleSelectRequestMessage()
        {
        }
        
        public TitleSelectRequestMessage(ushort titleId)
        {
            this.TitleId = titleId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(TitleId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TitleId = reader.ReadVarUhShort();
            if (TitleId < 0)
                throw new Exception("Forbidden value on TitleId = " + TitleId + ", it doesn't respect the following condition : titleId < 0");
        }
        
    }
    
}