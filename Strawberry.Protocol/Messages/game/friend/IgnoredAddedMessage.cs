

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IgnoredAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5678;
        public override uint MessageID => ProtocolId;
        
        public Types.IgnoredInformations IgnoreAdded { get; set; }
        public bool Session { get; set; }
        
        public IgnoredAddedMessage()
        {
        }
        
        public IgnoredAddedMessage(Types.IgnoredInformations ignoreAdded, bool session)
        {
            this.IgnoreAdded = ignoreAdded;
            this.Session = session;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(IgnoreAdded.TypeID);
            IgnoreAdded.Serialize(writer);
            writer.WriteBoolean(Session);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            IgnoreAdded = Types.ProtocolTypeManager.GetInstance<Types.IgnoredInformations>(reader.ReadShort());
            IgnoreAdded.Deserialize(reader);
            Session = reader.ReadBoolean();
        }
        
    }
    
}