

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IgnoredDeleteResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5677;
        public override uint MessageID => ProtocolId;
        
        public bool Success { get; set; }
        public bool Session { get; set; }
        public string Name { get; set; }
        
        public IgnoredDeleteResultMessage()
        {
        }
        
        public IgnoredDeleteResultMessage(bool success, bool session, string name)
        {
            this.Success = success;
            this.Session = session;
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Success);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Session);
            writer.WriteByte(flag1);
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Success = BooleanByteWrapper.GetFlag(flag1, 0);
            Session = BooleanByteWrapper.GetFlag(flag1, 1);
            Name = reader.ReadUTF();
        }
        
    }
    
}