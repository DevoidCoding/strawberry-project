

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FriendDeleteRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5603;
        public override uint MessageID => ProtocolId;
        
        public int AccountId { get; set; }
        
        public FriendDeleteRequestMessage()
        {
        }
        
        public FriendDeleteRequestMessage(int accountId)
        {
            this.AccountId = accountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteInt(AccountId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
        }
        
    }
    
}