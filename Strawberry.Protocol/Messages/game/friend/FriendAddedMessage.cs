

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FriendAddedMessage : NetworkMessage
    {
        public const uint ProtocolId = 5599;
        public override uint MessageID => ProtocolId;
        
        public Types.FriendInformations FriendAdded { get; set; }
        
        public FriendAddedMessage()
        {
        }
        
        public FriendAddedMessage(Types.FriendInformations friendAdded)
        {
            this.FriendAdded = friendAdded;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(FriendAdded.TypeID);
            FriendAdded.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FriendAdded = Types.ProtocolTypeManager.GetInstance<Types.FriendInformations>(reader.ReadShort());
            FriendAdded.Deserialize(reader);
        }
        
    }
    
}