

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FriendsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 4002;
        public override uint MessageID => ProtocolId;
        
        public Types.FriendInformations[] FriendsList { get; set; }
        
        public FriendsListMessage()
        {
        }
        
        public FriendsListMessage(Types.FriendInformations[] friendsList)
        {
            this.FriendsList = friendsList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)FriendsList.Length);
            foreach (var entry in FriendsList)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            FriendsList = new Types.FriendInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 FriendsList[i] = Types.ProtocolTypeManager.GetInstance<Types.FriendInformations>(reader.ReadShort());
                 FriendsList[i].Deserialize(reader);
            }
        }
        
    }
    
}