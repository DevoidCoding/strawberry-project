

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class FriendAddRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 4004;
        public override uint MessageID => ProtocolId;
        
        public string Name { get; set; }
        
        public FriendAddRequestMessage()
        {
        }
        
        public FriendAddRequestMessage(string name)
        {
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Name = reader.ReadUTF();
        }
        
    }
    
}