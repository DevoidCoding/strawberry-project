

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FriendUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 5924;
        public override uint MessageID => ProtocolId;
        
        public Types.FriendInformations FriendUpdated { get; set; }
        
        public FriendUpdateMessage()
        {
        }
        
        public FriendUpdateMessage(Types.FriendInformations friendUpdated)
        {
            this.FriendUpdated = friendUpdated;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(FriendUpdated.TypeID);
            FriendUpdated.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FriendUpdated = Types.ProtocolTypeManager.GetInstance<Types.FriendInformations>(reader.ReadShort());
            FriendUpdated.Deserialize(reader);
        }
        
    }
    
}