

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SpouseStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6265;
        public override uint MessageID => ProtocolId;
        
        public bool HasSpouse { get; set; }
        
        public SpouseStatusMessage()
        {
        }
        
        public SpouseStatusMessage(bool hasSpouse)
        {
            this.HasSpouse = hasSpouse;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(HasSpouse);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            HasSpouse = reader.ReadBoolean();
        }
        
    }
    
}