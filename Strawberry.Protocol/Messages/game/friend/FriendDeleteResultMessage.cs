

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class FriendDeleteResultMessage : NetworkMessage
    {
        public const uint ProtocolId = 5601;
        public override uint MessageID => ProtocolId;
        
        public bool Success { get; set; }
        public string Name { get; set; }
        
        public FriendDeleteResultMessage()
        {
        }
        
        public FriendDeleteResultMessage(bool success, string name)
        {
            this.Success = success;
            this.Name = name;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Success);
            writer.WriteUTF(Name);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Success = reader.ReadBoolean();
            Name = reader.ReadUTF();
        }
        
    }
    
}