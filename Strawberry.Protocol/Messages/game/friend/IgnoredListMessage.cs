

// Generated on 02/12/2018 03:56:32
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IgnoredListMessage : NetworkMessage
    {
        public const uint ProtocolId = 5674;
        public override uint MessageID => ProtocolId;
        
        public Types.IgnoredInformations[] IgnoredList { get; set; }
        
        public IgnoredListMessage()
        {
        }
        
        public IgnoredListMessage(Types.IgnoredInformations[] ignoredList)
        {
            this.IgnoredList = ignoredList;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)IgnoredList.Length);
            foreach (var entry in IgnoredList)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            IgnoredList = new Types.IgnoredInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 IgnoredList[i] = Types.ProtocolTypeManager.GetInstance<Types.IgnoredInformations>(reader.ReadShort());
                 IgnoredList[i].Deserialize(reader);
            }
        }
        
    }
    
}