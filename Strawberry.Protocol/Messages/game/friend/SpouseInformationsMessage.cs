

// Generated on 02/12/2018 03:56:33
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SpouseInformationsMessage : NetworkMessage
    {
        public const uint ProtocolId = 6356;
        public override uint MessageID => ProtocolId;
        
        public Types.FriendSpouseInformations Spouse { get; set; }
        
        public SpouseInformationsMessage()
        {
        }
        
        public SpouseInformationsMessage(Types.FriendSpouseInformations spouse)
        {
            this.Spouse = spouse;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(Spouse.TypeID);
            Spouse.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Spouse = Types.ProtocolTypeManager.GetInstance<Types.FriendSpouseInformations>(reader.ReadShort());
            Spouse.Deserialize(reader);
        }
        
    }
    
}