

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementDetailedListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6358;
        public override uint MessageID => ProtocolId;
        
        public Types.Achievement[] StartedAchievements { get; set; }
        public Types.Achievement[] FinishedAchievements { get; set; }
        
        public AchievementDetailedListMessage()
        {
        }
        
        public AchievementDetailedListMessage(Types.Achievement[] startedAchievements, Types.Achievement[] finishedAchievements)
        {
            this.StartedAchievements = startedAchievements;
            this.FinishedAchievements = finishedAchievements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)StartedAchievements.Length);
            foreach (var entry in StartedAchievements)
            {
                 entry.Serialize(writer);
            }
            writer.WriteUShort((ushort)FinishedAchievements.Length);
            foreach (var entry in FinishedAchievements)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            StartedAchievements = new Types.Achievement[limit];
            for (int i = 0; i < limit; i++)
            {
                 StartedAchievements[i] = new Types.Achievement();
                 StartedAchievements[i].Deserialize(reader);
            }
            limit = reader.ReadUShort();
            FinishedAchievements = new Types.Achievement[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishedAchievements[i] = new Types.Achievement();
                 FinishedAchievements[i].Deserialize(reader);
            }
        }
        
    }
    
}