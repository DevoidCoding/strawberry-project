

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementRewardErrorMessage : NetworkMessage
    {
        public const uint ProtocolId = 6375;
        public override uint MessageID => ProtocolId;
        
        public short AchievementId { get; set; }
        
        public AchievementRewardErrorMessage()
        {
        }
        
        public AchievementRewardErrorMessage(short achievementId)
        {
            this.AchievementId = achievementId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteShort(AchievementId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AchievementId = reader.ReadShort();
        }
        
    }
    
}