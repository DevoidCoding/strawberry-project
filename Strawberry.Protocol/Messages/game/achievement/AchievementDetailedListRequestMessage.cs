

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementDetailedListRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6357;
        public override uint MessageID => ProtocolId;
        
        public ushort CategoryId { get; set; }
        
        public AchievementDetailedListRequestMessage()
        {
        }
        
        public AchievementDetailedListRequestMessage(ushort categoryId)
        {
            this.CategoryId = categoryId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(CategoryId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            CategoryId = reader.ReadVarUhShort();
            if (CategoryId < 0)
                throw new Exception("Forbidden value on CategoryId = " + CategoryId + ", it doesn't respect the following condition : categoryId < 0");
        }
        
    }
    
}