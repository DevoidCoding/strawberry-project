

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6205;
        public override uint MessageID => ProtocolId;
        
        public Types.AchievementAchieved[] FinishedAchievements { get; set; }
        
        public AchievementListMessage()
        {
        }
        
        public AchievementListMessage(Types.AchievementAchieved[] finishedAchievements)
        {
            this.FinishedAchievements = finishedAchievements;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)FinishedAchievements.Length);
            foreach (var entry in FinishedAchievements)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            FinishedAchievements = new Types.AchievementAchieved[limit];
            for (int i = 0; i < limit; i++)
            {
                 FinishedAchievements[i] = Types.ProtocolTypeManager.GetInstance<Types.AchievementAchieved>(reader.ReadShort());
                 FinishedAchievements[i].Deserialize(reader);
            }
        }
        
    }
    
}