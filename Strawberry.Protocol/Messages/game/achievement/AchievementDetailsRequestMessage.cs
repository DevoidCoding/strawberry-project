

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementDetailsRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6380;
        public override uint MessageID => ProtocolId;
        
        public ushort AchievementId { get; set; }
        
        public AchievementDetailsRequestMessage()
        {
        }
        
        public AchievementDetailsRequestMessage(ushort achievementId)
        {
            this.AchievementId = achievementId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(AchievementId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            AchievementId = reader.ReadVarUhShort();
            if (AchievementId < 0)
                throw new Exception("Forbidden value on AchievementId = " + AchievementId + ", it doesn't respect the following condition : achievementId < 0");
        }
        
    }
    
}