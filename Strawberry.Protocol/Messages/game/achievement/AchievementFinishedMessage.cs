

// Generated on 02/12/2018 03:56:09
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AchievementFinishedMessage : NetworkMessage
    {
        public const uint ProtocolId = 6208;
        public override uint MessageID => ProtocolId;
        
        public Types.AchievementAchievedRewardable Achievement { get; set; }
        
        public AchievementFinishedMessage()
        {
        }
        
        public AchievementFinishedMessage(Types.AchievementAchievedRewardable achievement)
        {
            this.Achievement = achievement;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Achievement.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Achievement = new Types.AchievementAchievedRewardable();
            Achievement.Deserialize(reader);
        }
        
    }
    
}