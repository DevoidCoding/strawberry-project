

// Generated on 02/12/2018 03:56:31
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class LeaveDialogMessage : NetworkMessage
    {
        public const uint ProtocolId = 5502;
        public override uint MessageID => ProtocolId;
        
        public sbyte DialogType { get; set; }
        
        public LeaveDialogMessage()
        {
        }
        
        public LeaveDialogMessage(sbyte dialogType)
        {
            this.DialogType = dialogType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(DialogType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            DialogType = reader.ReadSByte();
            if (DialogType < 0)
                throw new Exception("Forbidden value on DialogType = " + DialogType + ", it doesn't respect the following condition : dialogType < 0");
        }
        
    }
    
}