

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DebugHighlightCellsMessage : NetworkMessage
    {
        public const uint ProtocolId = 2001;
        public override uint MessageID => ProtocolId;
        
        public double Color { get; set; }
        public ushort[] Cells { get; set; }
        
        public DebugHighlightCellsMessage()
        {
        }
        
        public DebugHighlightCellsMessage(double color, ushort[] cells)
        {
            this.Color = color;
            this.Cells = cells;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(Color);
            writer.WriteUShort((ushort)Cells.Length);
            foreach (var entry in Cells)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Color = reader.ReadDouble();
            if (Color < -9007199254740990 || Color > 9007199254740990)
                throw new Exception("Forbidden value on Color = " + Color + ", it doesn't respect the following condition : color < -9007199254740990 || color > 9007199254740990");
            var limit = reader.ReadUShort();
            Cells = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Cells[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}