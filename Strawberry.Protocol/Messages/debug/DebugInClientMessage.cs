

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class DebugInClientMessage : NetworkMessage
    {
        public const uint ProtocolId = 6028;
        public override uint MessageID => ProtocolId;
        
        public sbyte Level { get; set; }
        public string Message { get; set; }
        
        public DebugInClientMessage()
        {
        }
        
        public DebugInClientMessage(sbyte level, string message)
        {
            this.Level = level;
            this.Message = message;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Level);
            writer.WriteUTF(Message);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Level = reader.ReadSByte();
            if (Level < 0)
                throw new Exception("Forbidden value on Level = " + Level + ", it doesn't respect the following condition : level < 0");
            Message = reader.ReadUTF();
        }
        
    }
    
}