

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class TrustStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6267;
        public override uint MessageID => ProtocolId;
        
        public bool Trusted { get; set; }
        public bool Certified { get; set; }
        
        public TrustStatusMessage()
        {
        }
        
        public TrustStatusMessage(bool trusted, bool certified)
        {
            this.Trusted = trusted;
            this.Certified = certified;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, Trusted);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, Certified);
            writer.WriteByte(flag1);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            Trusted = BooleanByteWrapper.GetFlag(flag1, 0);
            Certified = BooleanByteWrapper.GetFlag(flag1, 1);
        }
        
    }
    
}