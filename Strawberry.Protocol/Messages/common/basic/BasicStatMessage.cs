

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicStatMessage : NetworkMessage
    {
        public const uint ProtocolId = 6530;
        public override uint MessageID => ProtocolId;
        
        public double TimeSpent { get; set; }
        public ushort StatId { get; set; }
        
        public BasicStatMessage()
        {
        }
        
        public BasicStatMessage(double timeSpent, ushort statId)
        {
            this.TimeSpent = timeSpent;
            this.StatId = statId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(TimeSpent);
            writer.WriteVarShort(StatId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            TimeSpent = reader.ReadDouble();
            if (TimeSpent < 0 || TimeSpent > 9007199254740990)
                throw new Exception("Forbidden value on TimeSpent = " + TimeSpent + ", it doesn't respect the following condition : timeSpent < 0 || timeSpent > 9007199254740990");
            StatId = reader.ReadVarUhShort();
            if (StatId < 0)
                throw new Exception("Forbidden value on StatId = " + StatId + ", it doesn't respect the following condition : statId < 0");
        }
        
    }
    
}