

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicStatWithDataMessage : BasicStatMessage
    {
        public new const uint ProtocolId = 6573;
        public override uint MessageID => ProtocolId;
        
        public Types.StatisticData[] Datas { get; set; }
        
        public BasicStatWithDataMessage()
        {
        }
        
        public BasicStatWithDataMessage(double timeSpent, ushort statId, Types.StatisticData[] datas)
         : base(timeSpent, statId)
        {
            this.Datas = datas;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Datas.Length);
            foreach (var entry in Datas)
            {
                 writer.WriteShort(entry.TypeID);
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Datas = new Types.StatisticData[limit];
            for (int i = 0; i < limit; i++)
            {
                 Datas[i] = Types.ProtocolTypeManager.GetInstance<Types.StatisticData>(reader.ReadShort());
                 Datas[i].Deserialize(reader);
            }
        }
        
    }
    
}