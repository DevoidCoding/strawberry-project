

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class BasicPingMessage : NetworkMessage
    {
        public const uint ProtocolId = 182;
        public override uint MessageID => ProtocolId;
        
        public bool Quiet { get; set; }
        
        public BasicPingMessage()
        {
        }
        
        public BasicPingMessage(bool quiet)
        {
            this.Quiet = quiet;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteBoolean(Quiet);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Quiet = reader.ReadBoolean();
        }
        
    }
    
}