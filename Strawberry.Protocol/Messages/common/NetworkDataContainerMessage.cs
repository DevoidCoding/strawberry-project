

// Generated on 11/02/2015 20:40:19
using Strawberry.Core.IO;
using Strawberry.Core.Network;


namespace Strawberry.Protocol.Messages
{
    public class NetworkDataContainerMessage : NetworkMessage
    {
        public const uint ProtocolId = 2;
        public byte[] Content;
        public override uint MessageID => ProtocolId;

        public NetworkDataContainerMessage()
        {
        }

        public NetworkDataContainerMessage(byte[] content)
        {
            this.Content = content;
        }

        public override void Serialize(IDataWriter writer)
        {
            Content = ZipHelper.Compress(Content);
            writer.WriteVarInt(Content.Length);
            writer.WriteBytes(Content);
        }

        public override void Deserialize(IDataReader reader)
        {
            Content = reader.ReadBytes(reader.ReadVarInt());
            Content = ZipHelper.Uncompress(Content);
        }
    }
}
