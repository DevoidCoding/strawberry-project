

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SelectedServerDataExtendedMessage : SelectedServerDataMessage
    {
        public new const uint ProtocolId = 6469;
        public override uint MessageID => ProtocolId;
        
        public Types.GameServerInformations[] Servers { get; set; }
        
        public SelectedServerDataExtendedMessage()
        {
        }
        
        public SelectedServerDataExtendedMessage(ushort serverId, string address, ushort port, bool canCreateNewCharacter, sbyte[] ticket, Types.GameServerInformations[] servers)
         : base(serverId, address, port, canCreateNewCharacter, ticket)
        {
            this.Servers = servers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)Servers.Length);
            foreach (var entry in Servers)
            {
                 entry.Serialize(writer);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            Servers = new Types.GameServerInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Servers[i] = new Types.GameServerInformations();
                 Servers[i].Deserialize(reader);
            }
        }
        
    }
    
}