

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MigratedServerListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6731;
        public override uint MessageID => ProtocolId;
        
        public ushort[] MigratedServerIds { get; set; }
        
        public MigratedServerListMessage()
        {
        }
        
        public MigratedServerListMessage(ushort[] migratedServerIds)
        {
            this.MigratedServerIds = migratedServerIds;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)MigratedServerIds.Length);
            foreach (var entry in MigratedServerIds)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            MigratedServerIds = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 MigratedServerIds[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}