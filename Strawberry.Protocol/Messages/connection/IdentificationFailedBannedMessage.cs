

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdentificationFailedBannedMessage : IdentificationFailedMessage
    {
        public new const uint ProtocolId = 6174;
        public override uint MessageID => ProtocolId;
        
        public double BanEndDate { get; set; }
        
        public IdentificationFailedBannedMessage()
        {
        }
        
        public IdentificationFailedBannedMessage(sbyte reason, double banEndDate)
         : base(reason)
        {
            this.BanEndDate = banEndDate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteDouble(BanEndDate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            BanEndDate = reader.ReadDouble();
            if (BanEndDate < 0 || BanEndDate > 9007199254740990)
                throw new Exception("Forbidden value on BanEndDate = " + BanEndDate + ", it doesn't respect the following condition : banEndDate < 0 || banEndDate > 9007199254740990");
        }
        
    }
    
}