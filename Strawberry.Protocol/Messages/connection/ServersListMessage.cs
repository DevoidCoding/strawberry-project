

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServersListMessage : NetworkMessage
    {
        public const uint ProtocolId = 30;
        public override uint MessageID => ProtocolId;
        
        public Types.GameServerInformations[] Servers { get; set; }
        public ushort AlreadyConnectedToServerId { get; set; }
        public bool CanCreateNewCharacter { get; set; }
        
        public ServersListMessage()
        {
        }
        
        public ServersListMessage(Types.GameServerInformations[] servers, ushort alreadyConnectedToServerId, bool canCreateNewCharacter)
        {
            this.Servers = servers;
            this.AlreadyConnectedToServerId = alreadyConnectedToServerId;
            this.CanCreateNewCharacter = canCreateNewCharacter;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Servers.Length);
            foreach (var entry in Servers)
            {
                 entry.Serialize(writer);
            }
            writer.WriteVarShort(AlreadyConnectedToServerId);
            writer.WriteBoolean(CanCreateNewCharacter);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Servers = new Types.GameServerInformations[limit];
            for (int i = 0; i < limit; i++)
            {
                 Servers[i] = new Types.GameServerInformations();
                 Servers[i].Deserialize(reader);
            }
            AlreadyConnectedToServerId = reader.ReadVarUhShort();
            if (AlreadyConnectedToServerId < 0)
                throw new Exception("Forbidden value on AlreadyConnectedToServerId = " + AlreadyConnectedToServerId + ", it doesn't respect the following condition : alreadyConnectedToServerId < 0");
            CanCreateNewCharacter = reader.ReadBoolean();
        }
        
    }
    
}