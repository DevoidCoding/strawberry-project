

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SelectedServerDataMessage : NetworkMessage
    {
        public const uint ProtocolId = 42;
        public override uint MessageID => ProtocolId;
        
        public ushort ServerId { get; set; }
        public string Address { get; set; }
        public ushort Port { get; set; }
        public bool CanCreateNewCharacter { get; set; }
        public sbyte[] Ticket { get; set; }
        
        public SelectedServerDataMessage()
        {
        }
        
        public SelectedServerDataMessage(ushort serverId, string address, ushort port, bool canCreateNewCharacter, sbyte[] ticket)
        {
            this.ServerId = serverId;
            this.Address = address;
            this.Port = port;
            this.CanCreateNewCharacter = canCreateNewCharacter;
            this.Ticket = ticket;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ServerId);
            writer.WriteUTF(Address);
            writer.WriteUShort(Port);
            writer.WriteBoolean(CanCreateNewCharacter);
            writer.WriteVarInt((int)Ticket.Length);
            foreach (var entry in Ticket)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ServerId = reader.ReadVarUhShort();
            if (ServerId < 0)
                throw new Exception("Forbidden value on ServerId = " + ServerId + ", it doesn't respect the following condition : serverId < 0");
            Address = reader.ReadUTF();
            Port = reader.ReadUShort();
            if (Port < 0 || Port > 65535)
                throw new Exception("Forbidden value on Port = " + Port + ", it doesn't respect the following condition : port < 0 || port > 65535");
            CanCreateNewCharacter = reader.ReadBoolean();
            var limit = reader.ReadVarInt();
            Ticket = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Ticket[i] = reader.ReadSByte();
            }
        }
        
    }
    
}