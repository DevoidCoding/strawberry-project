

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class IdentificationSuccessMessage : NetworkMessage
    {
        public const uint ProtocolId = 22;
        public override uint MessageID => ProtocolId;
        
        public bool HasRights { get; set; }
        public bool WasAlreadyConnected { get; set; }
        public string Login { get; set; }
        public string Nickname { get; set; }
        public int AccountId { get; set; }
        public sbyte CommunityId { get; set; }
        public string SecretQuestion { get; set; }
        public double AccountCreation { get; set; }
        public double SubscriptionElapsedDuration { get; set; }
        public double SubscriptionEndDate { get; set; }
        public byte HavenbagAvailableRoom { get; set; }
        
        public IdentificationSuccessMessage()
        {
        }
        
        public IdentificationSuccessMessage(bool hasRights, bool wasAlreadyConnected, string login, string nickname, int accountId, sbyte communityId, string secretQuestion, double accountCreation, double subscriptionElapsedDuration, double subscriptionEndDate, byte havenbagAvailableRoom)
        {
            this.HasRights = hasRights;
            this.WasAlreadyConnected = wasAlreadyConnected;
            this.Login = login;
            this.Nickname = nickname;
            this.AccountId = accountId;
            this.CommunityId = communityId;
            this.SecretQuestion = secretQuestion;
            this.AccountCreation = accountCreation;
            this.SubscriptionElapsedDuration = subscriptionElapsedDuration;
            this.SubscriptionEndDate = subscriptionEndDate;
            this.HavenbagAvailableRoom = havenbagAvailableRoom;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            byte flag1 = 0;
            flag1 = BooleanByteWrapper.SetFlag(flag1, 0, HasRights);
            flag1 = BooleanByteWrapper.SetFlag(flag1, 1, WasAlreadyConnected);
            writer.WriteByte(flag1);
            writer.WriteUTF(Login);
            writer.WriteUTF(Nickname);
            writer.WriteInt(AccountId);
            writer.WriteSByte(CommunityId);
            writer.WriteUTF(SecretQuestion);
            writer.WriteDouble(AccountCreation);
            writer.WriteDouble(SubscriptionElapsedDuration);
            writer.WriteDouble(SubscriptionEndDate);
            writer.WriteByte(HavenbagAvailableRoom);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            byte flag1 = reader.ReadByte();
            HasRights = BooleanByteWrapper.GetFlag(flag1, 0);
            WasAlreadyConnected = BooleanByteWrapper.GetFlag(flag1, 1);
            Login = reader.ReadUTF();
            Nickname = reader.ReadUTF();
            AccountId = reader.ReadInt();
            if (AccountId < 0)
                throw new Exception("Forbidden value on AccountId = " + AccountId + ", it doesn't respect the following condition : accountId < 0");
            CommunityId = reader.ReadSByte();
            if (CommunityId < 0)
                throw new Exception("Forbidden value on CommunityId = " + CommunityId + ", it doesn't respect the following condition : communityId < 0");
            SecretQuestion = reader.ReadUTF();
            AccountCreation = reader.ReadDouble();
            if (AccountCreation < 0 || AccountCreation > 9007199254740990)
                throw new Exception("Forbidden value on AccountCreation = " + AccountCreation + ", it doesn't respect the following condition : accountCreation < 0 || accountCreation > 9007199254740990");
            SubscriptionElapsedDuration = reader.ReadDouble();
            if (SubscriptionElapsedDuration < 0 || SubscriptionElapsedDuration > 9007199254740990)
                throw new Exception("Forbidden value on SubscriptionElapsedDuration = " + SubscriptionElapsedDuration + ", it doesn't respect the following condition : subscriptionElapsedDuration < 0 || subscriptionElapsedDuration > 9007199254740990");
            SubscriptionEndDate = reader.ReadDouble();
            if (SubscriptionEndDate < 0 || SubscriptionEndDate > 9007199254740990)
                throw new Exception("Forbidden value on SubscriptionEndDate = " + SubscriptionEndDate + ", it doesn't respect the following condition : subscriptionEndDate < 0 || subscriptionEndDate > 9007199254740990");
            HavenbagAvailableRoom = reader.ReadByte();
            if (HavenbagAvailableRoom < 0 || HavenbagAvailableRoom > 255)
                throw new Exception("Forbidden value on HavenbagAvailableRoom = " + HavenbagAvailableRoom + ", it doesn't respect the following condition : havenbagAvailableRoom < 0 || havenbagAvailableRoom > 255");
        }
        
    }
    
}