

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerSelectionMessage : NetworkMessage
    {
        public const uint ProtocolId = 40;
        public override uint MessageID => ProtocolId;
        
        public ushort ServerId { get; set; }
        
        public ServerSelectionMessage()
        {
        }
        
        public ServerSelectionMessage(ushort serverId)
        {
            this.ServerId = serverId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ServerId);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ServerId = reader.ReadVarUhShort();
            if (ServerId < 0)
                throw new Exception("Forbidden value on ServerId = " + ServerId + ", it doesn't respect the following condition : serverId < 0");
        }
        
    }
    
}