

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ServerStatusUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 50;
        public override uint MessageID => ProtocolId;
        
        public Types.GameServerInformations Server { get; set; }
        
        public ServerStatusUpdateMessage()
        {
        }
        
        public ServerStatusUpdateMessage(Types.GameServerInformations server)
        {
            this.Server = server;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            Server.Serialize(writer);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Server = new Types.GameServerInformations();
            Server.Deserialize(reader);
        }
        
    }
    
}