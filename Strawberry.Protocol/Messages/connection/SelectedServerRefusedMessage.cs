

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class SelectedServerRefusedMessage : NetworkMessage
    {
        public const uint ProtocolId = 41;
        public override uint MessageID => ProtocolId;
        
        public ushort ServerId { get; set; }
        public sbyte Error { get; set; }
        public sbyte ServerStatus { get; set; }
        
        public SelectedServerRefusedMessage()
        {
        }
        
        public SelectedServerRefusedMessage(ushort serverId, sbyte error, sbyte serverStatus)
        {
            this.ServerId = serverId;
            this.Error = error;
            this.ServerStatus = serverStatus;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(ServerId);
            writer.WriteSByte(Error);
            writer.WriteSByte(ServerStatus);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ServerId = reader.ReadVarUhShort();
            if (ServerId < 0)
                throw new Exception("Forbidden value on ServerId = " + ServerId + ", it doesn't respect the following condition : serverId < 0");
            Error = reader.ReadSByte();
            if (Error < 0)
                throw new Exception("Forbidden value on Error = " + Error + ", it doesn't respect the following condition : error < 0");
            ServerStatus = reader.ReadSByte();
            if (ServerStatus < 0)
                throw new Exception("Forbidden value on ServerStatus = " + ServerStatus + ", it doesn't respect the following condition : serverStatus < 0");
        }
        
    }
    
}