

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AcquaintanceServerListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6142;
        public override uint MessageID => ProtocolId;
        
        public ushort[] Servers { get; set; }
        
        public AcquaintanceServerListMessage()
        {
        }
        
        public AcquaintanceServerListMessage(ushort[] servers)
        {
            this.Servers = servers;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Servers.Length);
            foreach (var entry in Servers)
            {
                 writer.WriteVarShort(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Servers = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 Servers[i] = reader.ReadVarUhShort();
            }
        }
        
    }
    
}