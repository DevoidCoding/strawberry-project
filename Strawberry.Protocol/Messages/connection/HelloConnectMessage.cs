

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HelloConnectMessage : NetworkMessage
    {
        public const uint ProtocolId = 3;
        public override uint MessageID => ProtocolId;
        
        public string Salt { get; set; }
        public sbyte[] Key { get; set; }
        
        public HelloConnectMessage()
        {
        }
        
        public HelloConnectMessage(string salt, sbyte[] key)
        {
            this.Salt = salt;
            this.Key = key;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Salt);
            writer.WriteVarInt((int)Key.Length);
            foreach (var entry in Key)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Salt = reader.ReadUTF();
            var limit = reader.ReadVarInt();
            Key = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Key[i] = reader.ReadSByte();
            }
        }
        
    }
    
}