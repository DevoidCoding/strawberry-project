

// Generated on 02/12/2018 03:56:08
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NicknameChoiceRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 5639;
        public override uint MessageID => ProtocolId;
        
        public string Nickname { get; set; }
        
        public NicknameChoiceRequestMessage()
        {
        }
        
        public NicknameChoiceRequestMessage(string nickname)
        {
            this.Nickname = nickname;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Nickname);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Nickname = reader.ReadUTF();
        }
        
    }
    
}