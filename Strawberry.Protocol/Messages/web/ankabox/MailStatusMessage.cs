

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class MailStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6275;
        public override uint MessageID => ProtocolId;
        
        public ushort Unread { get; set; }
        public ushort Total { get; set; }
        
        public MailStatusMessage()
        {
        }
        
        public MailStatusMessage(ushort unread, ushort total)
        {
            this.Unread = unread;
            this.Total = total;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarShort(Unread);
            writer.WriteVarShort(Total);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Unread = reader.ReadVarUhShort();
            if (Unread < 0)
                throw new Exception("Forbidden value on Unread = " + Unread + ", it doesn't respect the following condition : unread < 0");
            Total = reader.ReadVarUhShort();
            if (Total < 0)
                throw new Exception("Forbidden value on Total = " + Total + ", it doesn't respect the following condition : total < 0");
        }
        
    }
    
}