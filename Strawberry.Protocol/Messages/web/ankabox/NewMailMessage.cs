

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class NewMailMessage : MailStatusMessage
    {
        public new const uint ProtocolId = 6292;
        public override uint MessageID => ProtocolId;
        
        public int[] SendersAccountId { get; set; }
        
        public NewMailMessage()
        {
        }
        
        public NewMailMessage(ushort unread, ushort total, int[] sendersAccountId)
         : base(unread, total)
        {
            this.SendersAccountId = sendersAccountId;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUShort((ushort)SendersAccountId.Length);
            foreach (var entry in SendersAccountId)
            {
                 writer.WriteInt(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            var limit = reader.ReadUShort();
            SendersAccountId = new int[limit];
            for (int i = 0; i < limit; i++)
            {
                 SendersAccountId[i] = reader.ReadInt();
            }
        }
        
    }
    
}