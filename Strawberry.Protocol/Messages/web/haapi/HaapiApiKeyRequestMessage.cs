

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HaapiApiKeyRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6648;
        public override uint MessageID => ProtocolId;
        
        public sbyte KeyType { get; set; }
        
        public HaapiApiKeyRequestMessage()
        {
        }
        
        public HaapiApiKeyRequestMessage(sbyte keyType)
        {
            this.KeyType = keyType;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(KeyType);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            KeyType = reader.ReadSByte();
            if (KeyType < 0)
                throw new Exception("Forbidden value on KeyType = " + KeyType + ", it doesn't respect the following condition : keyType < 0");
        }
        
    }
    
}