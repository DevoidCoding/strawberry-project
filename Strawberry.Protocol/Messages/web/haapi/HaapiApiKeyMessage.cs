

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class HaapiApiKeyMessage : NetworkMessage
    {
        public const uint ProtocolId = 6649;
        public override uint MessageID => ProtocolId;
        
        public sbyte ReturnType { get; set; }
        public sbyte KeyType { get; set; }
        public string Token { get; set; }
        
        public HaapiApiKeyMessage()
        {
        }
        
        public HaapiApiKeyMessage(sbyte returnType, sbyte keyType, string token)
        {
            this.ReturnType = returnType;
            this.KeyType = keyType;
            this.Token = token;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(ReturnType);
            writer.WriteSByte(KeyType);
            writer.WriteUTF(Token);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            ReturnType = reader.ReadSByte();
            if (ReturnType < 0)
                throw new Exception("Forbidden value on ReturnType = " + ReturnType + ", it doesn't respect the following condition : returnType < 0");
            KeyType = reader.ReadSByte();
            if (KeyType < 0)
                throw new Exception("Forbidden value on KeyType = " + KeyType + ", it doesn't respect the following condition : keyType < 0");
            Token = reader.ReadUTF();
        }
        
    }
    
}