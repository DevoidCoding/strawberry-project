

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class KrosmasterAuthTokenMessage : NetworkMessage
    {
        public const uint ProtocolId = 6351;
        public override uint MessageID => ProtocolId;
        
        public string Token { get; set; }
        
        public KrosmasterAuthTokenMessage()
        {
        }
        
        public KrosmasterAuthTokenMessage(string token)
        {
            this.Token = token;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Token);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Token = reader.ReadUTF();
        }
        
    }
    
}