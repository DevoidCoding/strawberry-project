

// Generated on 11/02/2015 20:40:43
using Strawberry.Core.IO;
using Strawberry.Core.Network;


namespace Strawberry.Protocol.Messages
{
    public class RawDataMessage : NetworkMessage
    {
        public const uint ProtocolId = 6253;
        public byte[] Content;
        public override uint MessageID => ProtocolId;
        
        public RawDataMessage()
        {
        }

        public RawDataMessage(byte[] content)
        {
            this.Content = content;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt(Content.Length);
            writer.WriteBytes(Content);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Content = reader.ReadBytes(reader.ReadVarInt());
        }
    }
}