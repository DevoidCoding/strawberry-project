

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    [HashMessage]
public class ClientKeyMessage : NetworkMessage
    {
        public const uint ProtocolId = 5607;
        public override uint MessageID => ProtocolId;
        
        public string Key { get; set; }
        
        public ClientKeyMessage()
        {
        }
        
        public ClientKeyMessage(string key)
        {
            this.Key = key;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Key);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Key = reader.ReadUTF();
        }
        
    }
    
}