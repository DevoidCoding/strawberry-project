

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CheckIntegrityMessage : NetworkMessage
    {
        public const uint ProtocolId = 6372;
        public override uint MessageID => ProtocolId;
        
        public sbyte[] Data { get; set; }
        
        public CheckIntegrityMessage()
        {
        }
        
        public CheckIntegrityMessage(sbyte[] data)
        {
            this.Data = data;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteVarInt((int)Data.Length);
            foreach (var entry in Data)
            {
                 writer.WriteSByte(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadVarInt();
            Data = new sbyte[limit];
            for (int i = 0; i < limit; i++)
            {
                 Data[i] = reader.ReadSByte();
            }
        }
        
    }
    
}