

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CheckFileMessage : NetworkMessage
    {
        public const uint ProtocolId = 6156;
        public override uint MessageID => ProtocolId;
        
        public string FilenameHash { get; set; }
        public sbyte Type { get; set; }
        public string Value { get; set; }
        
        public CheckFileMessage()
        {
        }
        
        public CheckFileMessage(string filenameHash, sbyte type, string value)
        {
            this.FilenameHash = filenameHash;
            this.Type = type;
            this.Value = value;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(FilenameHash);
            writer.WriteSByte(Type);
            writer.WriteUTF(Value);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            FilenameHash = reader.ReadUTF();
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            Value = reader.ReadUTF();
        }
        
    }
    
}