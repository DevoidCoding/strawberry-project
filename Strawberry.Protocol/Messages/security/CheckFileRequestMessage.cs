

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class CheckFileRequestMessage : NetworkMessage
    {
        public const uint ProtocolId = 6154;
        public override uint MessageID => ProtocolId;
        
        public string Filename { get; set; }
        public sbyte Type { get; set; }
        
        public CheckFileRequestMessage()
        {
        }
        
        public CheckFileRequestMessage(string filename, sbyte type)
        {
            this.Filename = filename;
            this.Type = type;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Filename);
            writer.WriteSByte(Type);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Filename = reader.ReadUTF();
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
        }
        
    }
    
}