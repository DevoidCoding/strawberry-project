

// Generated on 02/12/2018 03:56:47
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class QueueStatusMessage : NetworkMessage
    {
        public const uint ProtocolId = 6100;
        public override uint MessageID => ProtocolId;
        
        public ushort Position { get; set; }
        public ushort Total { get; set; }
        
        public QueueStatusMessage()
        {
        }
        
        public QueueStatusMessage(ushort position, ushort total)
        {
            this.Position = position;
            this.Total = total;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort(Position);
            writer.WriteUShort(Total);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Position = reader.ReadUShort();
            if (Position < 0 || Position > 65535)
                throw new Exception("Forbidden value on Position = " + Position + ", it doesn't respect the following condition : position < 0 || position > 65535");
            Total = reader.ReadUShort();
            if (Total < 0 || Total > 65535)
                throw new Exception("Forbidden value on Total = " + Total + ", it doesn't respect the following condition : total < 0 || total > 65535");
        }
        
    }
    
}