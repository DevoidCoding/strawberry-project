

// Generated on 02/12/2018 03:56:48
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AccountInformationsUpdateMessage : NetworkMessage
    {
        public const uint ProtocolId = 6740;
        public override uint MessageID => ProtocolId;
        
        public double SubscriptionEndDate { get; set; }
        public double UnlimitedRestatEndDate { get; set; }
        
        public AccountInformationsUpdateMessage()
        {
        }
        
        public AccountInformationsUpdateMessage(double subscriptionEndDate, double unlimitedRestatEndDate)
        {
            this.SubscriptionEndDate = subscriptionEndDate;
            this.UnlimitedRestatEndDate = unlimitedRestatEndDate;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteDouble(SubscriptionEndDate);
            writer.WriteDouble(UnlimitedRestatEndDate);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            SubscriptionEndDate = reader.ReadDouble();
            if (SubscriptionEndDate < 0 || SubscriptionEndDate > 9007199254740990)
                throw new Exception("Forbidden value on SubscriptionEndDate = " + SubscriptionEndDate + ", it doesn't respect the following condition : subscriptionEndDate < 0 || subscriptionEndDate > 9007199254740990");
            UnlimitedRestatEndDate = reader.ReadDouble();
            if (UnlimitedRestatEndDate < 0 || UnlimitedRestatEndDate > 9007199254740990)
                throw new Exception("Forbidden value on UnlimitedRestatEndDate = " + UnlimitedRestatEndDate + ", it doesn't respect the following condition : unlimitedRestatEndDate < 0 || unlimitedRestatEndDate > 9007199254740990");
        }
        
    }
    
}