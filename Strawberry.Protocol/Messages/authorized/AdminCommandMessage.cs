

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class AdminCommandMessage : NetworkMessage
    {
        public const uint ProtocolId = 76;
        public override uint MessageID => ProtocolId;
        
        public string Content { get; set; }
        
        public AdminCommandMessage()
        {
        }
        
        public AdminCommandMessage(string content)
        {
            this.Content = content;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUTF(Content);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Content = reader.ReadUTF();
        }
        
    }
    
}