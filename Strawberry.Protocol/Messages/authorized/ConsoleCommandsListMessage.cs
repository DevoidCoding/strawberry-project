

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ConsoleCommandsListMessage : NetworkMessage
    {
        public const uint ProtocolId = 6127;
        public override uint MessageID => ProtocolId;
        
        public string[] Aliases { get; set; }
        public string[] Args { get; set; }
        public string[] Descriptions { get; set; }
        
        public ConsoleCommandsListMessage()
        {
        }
        
        public ConsoleCommandsListMessage(string[] aliases, string[] args, string[] descriptions)
        {
            this.Aliases = aliases;
            this.Args = args;
            this.Descriptions = descriptions;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteUShort((ushort)Aliases.Length);
            foreach (var entry in Aliases)
            {
                 writer.WriteUTF(entry);
            }
            writer.WriteUShort((ushort)Args.Length);
            foreach (var entry in Args)
            {
                 writer.WriteUTF(entry);
            }
            writer.WriteUShort((ushort)Descriptions.Length);
            foreach (var entry in Descriptions)
            {
                 writer.WriteUTF(entry);
            }
        }
        
        public override void Deserialize(IDataReader reader)
        {
            var limit = reader.ReadUShort();
            Aliases = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Aliases[i] = reader.ReadUTF();
            }
            limit = reader.ReadUShort();
            Args = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Args[i] = reader.ReadUTF();
            }
            limit = reader.ReadUShort();
            Descriptions = new string[limit];
            for (int i = 0; i < limit; i++)
            {
                 Descriptions[i] = reader.ReadUTF();
            }
        }
        
    }
    
}