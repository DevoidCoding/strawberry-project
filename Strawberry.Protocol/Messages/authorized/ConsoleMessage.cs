

// Generated on 02/12/2018 03:56:07
using System;
using System.Collections.Generic;
using System.Linq;
using Strawberry.Protocol.Types;
using Strawberry.Core.IO;
using Strawberry.Core.Network;
using Strawberry.Protocol;

namespace Strawberry.Protocol.Messages
{
    public class ConsoleMessage : NetworkMessage
    {
        public const uint ProtocolId = 75;
        public override uint MessageID => ProtocolId;
        
        public sbyte Type { get; set; }
        public string Content { get; set; }
        
        public ConsoleMessage()
        {
        }
        
        public ConsoleMessage(sbyte type, string content)
        {
            this.Type = type;
            this.Content = content;
        }
        
        public override void Serialize(IDataWriter writer)
        {
            writer.WriteSByte(Type);
            writer.WriteUTF(Content);
        }
        
        public override void Deserialize(IDataReader reader)
        {
            Type = reader.ReadSByte();
            if (Type < 0)
                throw new Exception("Forbidden value on Type = " + Type + ", it doesn't respect the following condition : type < 0");
            Content = reader.ReadUTF();
        }
        
    }
    
}