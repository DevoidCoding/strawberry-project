﻿// <copyright file="SwlFile.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.Collections.Generic;
using System.IO;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Swl
{
    public class SwlFile : IDisposable
    {
        private readonly BigEndianReader _reader;
        private readonly Stream _stream;

        public SwlFile(Stream stream)
        {
            _stream = stream;
            _reader = new BigEndianReader(_stream);

            Classes = new List<string>();

            ReadFile();
        }

        public SwlFile(string file)
            : this(File.Open(file, FileMode.Open))
        {
        }

        public List<string> Classes { get; set; }

        public string FilePath { get; private set; }

        public uint FrameRate { get; set; }

        public byte[] SwfData { get; set; }

        public byte Version { get; set; }

        public void Dispose()
        {
            _reader.Dispose();
        }

        private void ReadFile()
        {
            if (_reader.ReadByte() != 76)
            {
                throw new Exception("Malformated swf file");
            }

            Version = _reader.ReadByte();
            FrameRate = _reader.ReadUInt();

            var count = _reader.ReadInt();
            for (var i = 0; i < count; i++)
            {
                Classes.Add(_reader.ReadUTF());
            }

            SwfData = _reader.ReadBytes((int) _reader.BytesAvailable);
        }
    }
}