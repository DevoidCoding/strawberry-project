﻿// <copyright file="D2PIndexTable.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using System.IO;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.D2P
{
    public class D2PIndexTable : INotifyPropertyChanged
    {
        public const int TableOffset = -24;
        public const SeekOrigin TableSeekOrigin = SeekOrigin.End;

        public D2PIndexTable(D2PFile container)
        {
            Container = container;
        }

        public D2PFile Container { get; private set; }

        public int EntriesCount { get; set; }

        public int EntriesDefinitionOffset { get; set; }

        public int OffsetBase { get; set; }

        public int PropertiesCount { get; set; }

        public int PropertiesOffset { get; set; }

        public int Size { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ReadTable(IDataReader reader)
        {
            OffsetBase = reader.ReadInt();
            Size = reader.ReadInt();
            EntriesDefinitionOffset = reader.ReadInt();
            EntriesCount = reader.ReadInt();
            PropertiesOffset = reader.ReadInt();
            PropertiesCount = reader.ReadInt();
        }

        public void WriteTable(IDataWriter writer)
        {
            writer.WriteInt(OffsetBase);
            writer.WriteInt(Size);
            writer.WriteInt(EntriesDefinitionOffset);
            writer.WriteInt(EntriesCount);
            writer.WriteInt(PropertiesOffset);
            writer.WriteInt(PropertiesCount);
        }
    }
}