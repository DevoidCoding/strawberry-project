﻿// <copyright file="D2PFile.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using NLog;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.D2P
{
    public class D2PFile : INotifyPropertyChanged, IDisposable
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<string, D2PEntry> m_entries = new Dictionary<string, D2PEntry>();
        private readonly List<D2PFile> m_links = new List<D2PFile>();

        private readonly Queue<D2PFile> m_linksToSave = new Queue<D2PFile>();
        private readonly List<D2PProperty> m_properties = new List<D2PProperty>();

        private readonly Dictionary<string, D2PDirectory> m_rootDirectories = new Dictionary<string, D2PDirectory>();
        private string m_filePath;

        private bool m_isDisposed;
        private readonly IDataReader m_reader;

        public D2PFile()
        {
            IndexTable = new D2PIndexTable(this);
            m_reader = new BigEndianReader(new byte[0]);
        }

        public D2PFile(string file, bool preload = false)
        {
            FilePath = file;
            if (preload)
                m_reader = new FastBigEndianReader(File.ReadAllBytes(file));
            else
                m_reader = new BigEndianReader(File.OpenRead(file));

            InternalOpen();
        }

        public IEnumerable<D2PEntry> Entries
        {
            get { return m_entries.Values; }
        }

        public string FileName { get; private set; }

        public string FilePath
        {
            get { return m_filePath; }
            private set
            {
                m_filePath = value;
                FileName = Path.GetFileName(value);
            }
        }

        public D2PIndexTable IndexTable { get; private set; }

        public ReadOnlyCollection<D2PFile> Links
        {
            get { return m_links.AsReadOnly(); }
        }

        public ReadOnlyCollection<D2PProperty> Properties
        {
            get { return m_properties.AsReadOnly(); }
        }

        public IEnumerable<D2PDirectory> RootDirectories
        {
            get { return m_rootDirectories.Values; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (m_isDisposed)
                return;

            m_isDisposed = true;

            if (m_reader != null)
                m_reader.Dispose();

            if (m_links != null)
                foreach (var link in m_links)
                {
                    link.Dispose();
                }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action<D2PFile, int> ExtractPercentProgress;

        public bool HasFilePath()
        {
            return !string.IsNullOrEmpty(FilePath);
        }

        private void OnExtractPercentProgress(int percent)
        {
            var handler = ExtractPercentProgress;
            if (handler != null) handler(this, percent);
        }

        private void OnPropertyChanged(string propertyName)
        {
            var propd = PropertyChanged;
            if (propd != null)
                propd(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Initialize

        public D2PEntry this[string fileName]
        {
            get { return m_entries[fileName]; }
        }

        private void InternalOpen()
        {
            if (m_reader.ReadByte() != 2 || m_reader.ReadByte() != 1)
                throw new FileLoadException("Corrupted d2p header");

            ReadTable();
            ReadProperties();
            ReadEntriesDefinitions();
        }

        private void ReadTable()
        {
            m_reader.Seek(D2PIndexTable.TableOffset, D2PIndexTable.TableSeekOrigin);
            IndexTable = new D2PIndexTable(this);
            IndexTable.ReadTable(m_reader);
        }

        private void ReadProperties()
        {
            m_reader.Seek(IndexTable.PropertiesOffset, SeekOrigin.Begin);
            for (var i = 0; i < IndexTable.PropertiesCount; i++)
            {
                var property = new D2PProperty();
                property.ReadProperty(m_reader);

                if (property.Key == "link")
                {
                    InternalAddLink(property.Value);
                }

                m_properties.Add(property);
            }
        }

        private void ReadEntriesDefinitions()
        {
            m_reader.Seek(IndexTable.EntriesDefinitionOffset, SeekOrigin.Begin);
            for (var i = 0; i < IndexTable.EntriesCount; i++)
            {
                var entry = D2PEntry.CreateEntryDefinition(this, m_reader);

                InternalAddEntry(entry);
            }
        }

        public void AddProperty(D2PProperty property)
        {
            if (property.Key == "link")
            {
                InternalAddLink(property.Value);
            }

            InternalAddProperty(property);
        }

        public bool RemoveProperty(D2PProperty property)
        {
            if (property.Key == "link")
            {
                var link = m_links.FirstOrDefault(entry =>
                    Path.GetFullPath(GetLinkFileAbsolutePath(property.Value)) ==
                    Path.GetFullPath(entry.FilePath));

                if (link == null || !InternalRemoveLink(link))
                    throw new Exception($"Cannot remove the associated link {property.Value} to this property");
            }

            if (m_properties.Remove(property))
            {
                OnPropertyChanged("Properties");
                IndexTable.PropertiesCount--;
                return true;
            }

            return false;
        }

        private void InternalAddProperty(D2PProperty property)
        {
            m_properties.Add(property);
            OnPropertyChanged("Properties");
            IndexTable.PropertiesCount++;
        }

        public void AddLink(string linkFile)
        {
            InternalAddLink(linkFile);
            InternalAddProperty(new D2PProperty
            {
                Key = "link",
                Value = linkFile
            });
        }

        private void InternalAddLink(string linkFile)
        {
            var path = GetLinkFileAbsolutePath(linkFile);

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(linkFile);
            }

            var link = new D2PFile(path);
            foreach (var entry in link.Entries)
            {
                InternalAddEntry(entry);
            }

            m_links.Add(link);
            OnPropertyChanged("Links");
        }

        private string GetLinkFileAbsolutePath(string linkFile)
        {
            if (File.Exists(linkFile))
            {
                return linkFile;
            }

            if (HasFilePath())
            {
                var absolutePath = Path.Combine(Path.GetDirectoryName(FilePath), linkFile);

                if (File.Exists(absolutePath))
                {
                    return absolutePath;
                }
            }

            return linkFile;
        }

        public bool RemoveLink(D2PFile file)
        {
            var property =
                m_properties.FirstOrDefault(entry =>
                    Path.GetFullPath(GetLinkFileAbsolutePath(entry.Value)) ==
                    Path.GetFullPath(file.FilePath));

            if (property == null)
                return false;

            var result = InternalRemoveLink(file) && m_properties.Remove(property);

            if (result)
                OnPropertyChanged("Properties");

            return result;
        }

        private bool InternalRemoveLink(D2PFile link)
        {
            if (m_links.Remove(link))
            {
                OnPropertyChanged("Links");

                return true;
            }

            return false;
        }

        /// <summary>
        ///     Ignore entries of linked files
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public D2PEntry[] GetEntriesOfInstanceOnly()
        {
            return m_entries.Values.Where(entry => entry.Container == this).ToArray();
        }

        public D2PEntry GetEntry(string fileName)
        {
            return m_entries[fileName];
        }

        public D2PEntry TryGetEntry(string fileName)
        {
            D2PEntry entry;
            if (m_entries.TryGetValue(fileName, out entry))
                return entry;

            return null;
        }

        public string[] GetFilesName()
        {
            return m_entries.Keys.ToArray();
        }

        public void AddEntry(D2PEntry entry)
        {
            entry.State = D2PEntryState.Added;
            InternalAddEntry(entry);
            IndexTable.EntriesCount++;
            OnPropertyChanged("Entries");
        }

        private void InternalAddEntry(D2PEntry entry)
        {
            var registerdEntry = TryGetEntry(entry.FullFileName);

            // shouldn't be possible but dofus don't care about that
            if (registerdEntry != null)
            {
                logger.Warn("Entry '{0}'({1}) already added and will be override ({2})", registerdEntry.FullFileName,
                    registerdEntry.Container.FileName, FileName);
                m_entries[registerdEntry.FullFileName] = entry;
            }
            else
                m_entries.Add(entry.FullFileName, entry);

            InternalAddDirectories(entry);
        }

        private void InternalAddDirectories(D2PEntry entry)
        {
            var directories = entry.GetDirectoriesName();

            if (directories.Length == 0)
                return;

            D2PDirectory current = null;
            if (!m_rootDirectories.ContainsKey(directories[0]))
            {
                m_rootDirectories.Add(directories[0], current = new D2PDirectory(directories[0]));
            }
            else
            {
                current = m_rootDirectories[directories[0]];
            }

            current.Entries.Add(entry);

            foreach (var directory in directories.Skip(1))
            {
                var next = current.TryGetDirectory(directory);
                if (next == null)
                {
                    var dir = new D2PDirectory(directory)
                    {
                        Parent = current
                    };
                    current.Directories.Add(directory, dir);

                    current = dir;
                }
                else
                {
                    current = current.TryGetDirectory(directory);
                }

                current.Entries.Add(entry);
            }

            entry.Directory = current;
        }

        public bool RemoveEntry(D2PEntry entry)
        {
            if (entry.Container != this)
            {
                if (!entry.Container.RemoveEntry(entry))
                    return false;

                if (!m_linksToSave.Contains(entry.Container))
                    m_linksToSave.Enqueue(entry.Container);
            }

            if (m_entries.Remove(entry.FullFileName))
            {
                entry.State = D2PEntryState.Removed;
                InternalRemoveDirectories(entry);
                OnPropertyChanged("Entries");

                if (entry.Container == this)
                    IndexTable.EntriesCount--;

                return true;
            }

            return false;
        }

        private void InternalRemoveDirectories(D2PEntry entry)
        {
            var current = entry.Directory;
            while (current != null)
            {
                current.Entries.Remove(entry);

                if (current.Parent != null && current.Entries.Count == 0)
                    current.Parent.Directories.Remove(current.Name);
                else if (current.IsRoot && current.Entries.Count == 0)
                    m_rootDirectories.Remove(current.Name);

                current = current.Parent;
            }
        }

        #endregion

        #region Read

        public bool Exists(string fileName)
        {
            return m_entries.ContainsKey(fileName);
        }

        public Dictionary<D2PEntry, byte[]> ReadAllFiles()
        {
            var result = new Dictionary<D2PEntry, byte[]>();

            foreach (var entry in m_entries)
            {
                result.Add(entry.Value, ReadFile(entry.Value));
            }

            return result;
        }

        public byte[] ReadFile(D2PEntry entry)
        {
            if (entry.Container != this)
                return entry.Container.ReadFile(entry);

            lock (m_reader)
            {
                if (entry.Index >= 0 && IndexTable.OffsetBase + entry.Index >= 0)
                    m_reader.Seek(IndexTable.OffsetBase + entry.Index, SeekOrigin.Begin);

                var data = entry.ReadEntry(m_reader);

                return data;
            }
        }

        public byte[] ReadFile(string fileName)
        {
            if (!Exists(fileName))
                throw new FileNotFoundException(fileName);

            var entry = GetEntry(fileName);

            return ReadFile(entry);
        }

        public void ExtractFile(string fileName, bool overwrite = false)
        {
            if (!Exists(fileName))
                throw new FileNotFoundException(fileName);

            var entry = GetEntry(fileName);

            var dest = Path.Combine("./", entry.FullFileName);

            if (!Directory.Exists(Path.GetDirectoryName(dest)))
                Directory.CreateDirectory(dest);

            ExtractFile(fileName, dest, overwrite);
        }

        public void ExtractFile(string fileName, string destination, bool overwrite = false)
        {
            var bytes = ReadFile(fileName);
            var attr = File.GetAttributes(destination);

            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                destination = Path.Combine(destination, Path.GetFileName(fileName));
            }

            if (File.Exists(destination) && !overwrite)
                throw new InvalidOperationException($"Cannot overwrite {destination}");

            if (!Directory.Exists(Path.GetDirectoryName(destination)))
                Directory.CreateDirectory(Path.GetDirectoryName(destination));

            File.WriteAllBytes(destination, bytes);
        }

        public void ExtractDirectory(string directoryName, string destination)
        {
            if (!HasDirectory(directoryName))
                throw new InvalidOperationException($"Directory {directoryName} does not exist");

            var directory = TryGetDirectory(directoryName);

            if (!Directory.Exists(Path.GetDirectoryName(Path.Combine(destination, directory.FullName))))
                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(destination, directory.FullName)));

            foreach (var entry in directory.Entries)
            {
                ExtractFile(entry.FullFileName, Path.Combine(destination, entry.FullFileName));
            }

            foreach (var pDirectory in directory.Directories)
            {
                ExtractDirectory(pDirectory.Value.FullName, destination);
            }
        }

        public void ExtractAllFiles(string destination, bool overwrite = false, bool progress = false)
        {
            if (!Directory.Exists(Path.GetDirectoryName(destination)))
                Directory.CreateDirectory(destination);

            //create dirs
            foreach (var dir in m_entries.Select(entry => entry.Value.GetDirectoriesName()).Distinct())
            {
                var dest = Path.Combine(Path.GetFullPath(destination), Path.Combine(dir));

                if (!Directory.Exists(dest))
                    Directory.CreateDirectory(dest);
            }

            double i = 0;
            var progressPercent = 0;
            foreach (var entry in m_entries)
            {
                if (File.Exists(Path.GetFullPath(destination)) && !overwrite)
                    throw new InvalidOperationException($"Cannot overwrite {destination}");

                var dest = Path.Combine(Path.GetFullPath(destination), entry.Value.FullFileName);

                File.WriteAllBytes(dest, ReadFile(entry.Value));
                i++;

                if (progress)
                {
                    if ((int) (i/m_entries.Count*100) != progressPercent)
                        OnExtractPercentProgress(progressPercent = (int) (i/m_entries.Count*100));
                }
            }
        }

        #endregion

        #region Write

        public D2PEntry AddFile(string file)
        {
            var bytes = File.ReadAllBytes(file);

            var dest = file;

            if (HasFilePath())
                dest = GetRelativePath(file, Path.GetDirectoryName(FilePath));

            return AddFile(dest, bytes);
        }

        public D2PEntry AddFile(string fileName, byte[] data)
        {
            var entry = new D2PEntry(this, fileName, data);

            AddEntry(entry);

            return entry;
        }

        public bool RemoveFile(string file)
        {
            var entry = TryGetEntry(file);

            return entry != null && RemoveEntry(entry);
        }

        public bool ModifyFile(string file, byte[] data)
        {
            var entry = TryGetEntry(file);

            if (entry == null)
                return false;

            entry.ModifyEntry(data);

            lock (m_linksToSave)
            {
                if (entry.Container != this &&
                    !m_linksToSave.Contains(entry.Container))
                {
                    m_linksToSave.Enqueue(entry.Container);
                }
            }

            return true;
        }

        private string GetRelativePath(string file, string directory)
        {
            var uri = new Uri(Path.GetFullPath(file));
            var currentUri = new Uri(Path.GetFullPath(directory));

            return currentUri.MakeRelativeUri(uri).ToString();
        }

        public void Save()
        {
            if (HasFilePath())
                SaveAs(FilePath);
            else
            {
                throw new InvalidOperationException("Cannot perform Save : No path defined, use SaveAs instead");
            }
        }

        public void SaveAs(string destination, bool overwrite = true)
        {
            lock (m_linksToSave)
            {
                // save the links before
                while (m_linksToSave.Count > 0)
                {
                    var link = m_linksToSave.Dequeue();

                    // theorically the path is defined
                    link.Save();
                }
            }

            Stream stream;
            if (!File.Exists(destination))
                stream = File.Create(destination);
            else if (!overwrite)
                throw new InvalidOperationException(
                    "Cannot perform SaveAs : file already exist, notify overwrite parameter to true");
            else
                stream = File.OpenWrite(destination);

            using (var writer = new BigEndianWriter(stream))
            {
                // header
                writer.WriteByte(2);
                writer.WriteByte(1);

                var entries = GetEntriesOfInstanceOnly();
                // avoid the header
                var offset = 2;

                foreach (var entry in entries)
                {
                    var data = ReadFile(entry);

                    entry.Index = (int) writer.Position - offset;
                    writer.WriteBytes(data);
                }

                var entriesDefOffset = (int) writer.Position;

                foreach (var entry in entries)
                {
                    entry.WriteEntryDefinition(writer);
                }

                var propertiesOffset = (int) writer.Position;

                foreach (var property in m_properties)
                {
                    property.WriteProperty(writer);
                }

                IndexTable.OffsetBase = offset;
                IndexTable.EntriesCount = entries.Length;
                IndexTable.EntriesDefinitionOffset = entriesDefOffset;
                IndexTable.PropertiesCount = m_properties.Count;
                IndexTable.PropertiesOffset = propertiesOffset;
                IndexTable.Size = IndexTable.EntriesDefinitionOffset - IndexTable.OffsetBase;

                IndexTable.WriteTable(writer);
            }
        }

        #endregion

        #region Explore

        public bool HasDirectory(string directory)
        {
            var directoriesName = directory.Split(new[] {'/', '\\'}, StringSplitOptions.RemoveEmptyEntries);

            if (directoriesName.Length == 0)
                return false;

            D2PDirectory current = null;
            m_rootDirectories.TryGetValue(directoriesName[0], out current);

            if (current == null)
                return false;

            foreach (var dir in directoriesName.Skip(1))
            {
                if (!current.HasDirectory(dir))
                    return false;

                current = current.TryGetDirectory(dir);
            }

            return true;
        }

        public D2PDirectory TryGetDirectory(string directory)
        {
            var directoriesName = directory.Split(new[] {'/', '\\'}, StringSplitOptions.RemoveEmptyEntries);

            if (directoriesName.Length == 0)
                return null;

            D2PDirectory current = null;
            m_rootDirectories.TryGetValue(directoriesName[0], out current);

            if (current == null)
                return null;

            foreach (var dir in directoriesName.Skip(1))
            {
                if (!current.HasDirectory(dir))
                    return null;

                current = current.TryGetDirectory(dir);
            }

            return current;
        }

        public D2PDirectory[] GetDirectoriesTree(string directory)
        {
            var result = new List<D2PDirectory>();
            var directoriesName = directory.Split(new[] {'/', '\\'}, StringSplitOptions.RemoveEmptyEntries);

            if (directoriesName.Length == 0)
                return new D2PDirectory[0];

            D2PDirectory current = null;
            m_rootDirectories.TryGetValue(directoriesName[0], out current);

            if (current == null)
                return new D2PDirectory[0];

            result.Add(current);

            foreach (var dir in directoriesName.Skip(1))
            {
                if (!current.HasDirectory(dir))
                    return result.ToArray();

                current = current.TryGetDirectory(dir);
                result.Add(current);
            }

            return result.ToArray();
        }

        #endregion
    }
}