﻿// <copyright file="D2PProperty.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.D2P
{
    public class D2PProperty : INotifyPropertyChanged
    {
        public D2PProperty()
        {
        }

        public D2PProperty(string key, string property)
        {
            Key = key;
            Value = property;
        }

        public string Key { get; set; }

        public string Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ReadProperty(IDataReader reader)
        {
            Key = reader.ReadUTF();
            Value = reader.ReadUTF();
        }

        public void WriteProperty(IDataWriter writer)
        {
            writer.WriteUTF(Key);
            writer.WriteUTF(Value);
        }
    }
}