﻿// <copyright file="D2PEntry.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.ComponentModel;
using System.IO;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.D2P
{
    public class D2PEntry : INotifyPropertyChanged
    {
        private D2PDirectory[] m_directories;
        private byte[] m_newData;

        private D2PEntry(D2PFile container)
        {
            Container = container;
            Index = -1;
        }

        public D2PEntry(D2PFile container, string fileName)
        {
            Container = container;
            FullFileName = fileName;
            Index = -1;
        }

        public D2PEntry(D2PFile container, string fileName, byte[] data)
        {
            Container = container;
            FullFileName = fileName;
            m_newData = data;
            State = D2PEntryState.Added;
            Size = data.Length;
            Index = -1;
        }

        public D2PFile Container { get; set; }

        public D2PDirectory Directory { get; set; }

        public string FileName
        {
            get { return Path.GetFileName(FullFileName); }
        }

        public string FullFileName { get; private set; }

        public int Index { get; set; }

        public int Size { get; set; }

        public D2PEntryState State { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public static D2PEntry CreateEntryDefinition(D2PFile container, IDataReader reader)
        {
            var entry = new D2PEntry(container);
            entry.ReadEntryDefinition(reader);

            return entry;
        }

        public string[] GetDirectoriesName()
        {
            return Path.GetDirectoryName(FullFileName).Split(new[] {'/', '\\'}, StringSplitOptions.RemoveEmptyEntries);
        }

        public void ModifyEntry(byte[] data)
        {
            m_newData = data;
            Size = data.Length;
            State = D2PEntryState.Dirty;
        }

        public byte[] ReadEntry(IDataReader reader)
        {
            if (State == D2PEntryState.Removed)
                throw new InvalidOperationException("Cannot read a deleted entry");

            if (State == D2PEntryState.Dirty || State == D2PEntryState.Added)
                return m_newData;

            return reader.ReadBytes(Size);
        }

        public void ReadEntryDefinition(IDataReader reader)
        {
            FullFileName = reader.ReadUTF();
            Index = reader.ReadInt();
            Size = reader.ReadInt();
        }

        public void WriteEntryDefinition(IDataWriter writer)
        {
            if (Index == -1)
                throw new InvalidOperationException("Invalid entry, index = -1");

            writer.WriteUTF(FullFileName);
            writer.WriteInt(Index);
            writer.WriteInt(Size);
        }
    }
}