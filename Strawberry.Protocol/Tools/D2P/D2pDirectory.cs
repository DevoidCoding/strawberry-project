﻿// <copyright file="D2PDirectory.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.Collections.Generic;
using System.Linq;

namespace Strawberry.Protocol.Tools.D2P
{
    public class D2PDirectory
    {
        private D2PDirectory m_parent;

        public D2PDirectory(string name)
        {
            Name = name;
            FullName = name;
        }

        public Dictionary<string, D2PDirectory> Directories { get; set; } = new Dictionary<string, D2PDirectory>();

        public List<D2PEntry> Entries { get; set; } = new List<D2PEntry>();

        public string FullName { get; set; }

        public bool IsRoot
        {
            get { return Parent == null; }
        }

        public string Name { get; set; }

        public D2PDirectory Parent
        {
            get { return m_parent; }
            set
            {
                m_parent = value;
                UpdateFullName();
            }
        }

        public bool HasDirectory(string directory)
        {
            return Directories.ContainsKey(directory);
        }

        public bool HasEntry(string entryName)
        {
            return Entries.Any(entry => entry.FullFileName == entryName);
        }

        public D2PDirectory TryGetDirectory(string name)
        {
            D2PDirectory directory;
            if (Directories.TryGetValue(name, out directory))
                return directory;

            return null;
        }

        public D2PEntry TryGetEntry(string entryName)
        {
            return Entries.SingleOrDefault(entry => entry.FullFileName == entryName);
        }

        private void UpdateFullName()
        {
            var current = this;
            FullName = current.Name;
            while (current.Parent != null)
            {
                FullName = FullName.Insert(0, current.Parent.Name + "\\");
                current = current.Parent;
            }
        }
    }
}