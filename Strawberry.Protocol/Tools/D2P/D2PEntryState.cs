﻿// <copyright file="D2PEntryState.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

namespace Strawberry.Protocol.Tools.D2P
{
    public enum D2PEntryState
    {
        None,
        Dirty,
        Added,
        Removed
    }
}