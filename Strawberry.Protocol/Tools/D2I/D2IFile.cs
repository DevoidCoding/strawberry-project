﻿// <copyright file="D2IFile.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.Collections.Generic;
using System.IO;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.D2I
{
    public class D2IFile
    {
        private readonly Dictionary<int, string> m_indexes = new Dictionary<int, string>();
        private readonly Dictionary<string, string> m_textIndexes = new Dictionary<string, string>();

        public D2IFile(string uri)
        {
            FilePath = uri;
            Initialize();
        }

        public string FilePath { get; }

        public Dictionary<int, string> GetAllText()
        {
            return m_indexes;
        }

        public Dictionary<string, string> GetAllUiText()
        {
            return m_textIndexes;
        }

        public string GetText(int id)
        {
            if (m_indexes.ContainsKey(id))
            {
                return m_indexes[id];
            }
            return "{null}";
        }

        public string GetText(string id)
        {
            if (m_textIndexes.ContainsKey(id))
            {
                return m_textIndexes[id];
            }
            return "{null}";
        }

        public void Save()
        {
            Save(FilePath);
        }

        public void Save(string uri)
        {
            using (var writer = new BigEndianWriter(new StreamWriter(uri).BaseStream))
            {
                var indexTable = new BigEndianWriter();
                writer.Seek(4, SeekOrigin.Begin);

                foreach (var index in m_indexes)
                {
                    indexTable.WriteInt(index.Key);
                    indexTable.WriteInt((int) writer.Position);
                    writer.WriteUTF(index.Value);
                }

                var indexLen = (int) indexTable.Position;

                foreach (var index in m_textIndexes)
                {
                    indexTable.WriteUTF(index.Key);
                    indexTable.WriteInt((int) writer.Position);
                    writer.WriteUTF(index.Value);
                }

                var indexPos = (int) writer.Position;

                /* write index at end */
                var indexData = indexTable.Data;
                writer.WriteInt(indexLen);
                writer.WriteBytes(indexData);

                /* write index pos at begin */
                writer.Seek(0, SeekOrigin.Begin);
                writer.WriteInt(indexPos);
            }
        }

        public void SetText(int id, string value)
        {
            if (m_indexes.ContainsKey(id))
                m_indexes[id] = value;
            else
                m_indexes.Add(id, value);
        }

        public void SetText(string id, string value)
        {
            if (m_textIndexes.ContainsKey(id))
                m_textIndexes[id] = value;
            else
                m_textIndexes.Add(id, value);
        }

        private void Initialize()
        {
            using (var reader = new FastBigEndianReader(File.ReadAllBytes(FilePath)))
            {
                var indexPos = reader.ReadInt();
                reader.Seek(indexPos, SeekOrigin.Begin);
                var indexLen = reader.ReadInt();
                var addOffset = 0;
                for (var i = 0; i < indexLen; i += 9)
                {
                    var key = reader.ReadInt();
                    var nbAdditionnalStrings = reader.ReadByte();
                    var dataPos = reader.ReadInt();
                    var pos = (int) reader.Position;
                    reader.Seek(dataPos + addOffset, SeekOrigin.Begin);
                    m_indexes.Add(key, reader.ReadUTF());
                    reader.Seek(pos, SeekOrigin.Begin);
                    while (nbAdditionnalStrings-- > 0)
                    {
                        dataPos = reader.ReadInt();
                        pos = (int) reader.Position;
                        reader.Seek(dataPos + addOffset, SeekOrigin.Begin);
                        var unusedString = reader.ReadUTF(); // Well, no real use to read that, as we don't use 'em
                        reader.Seek(pos, SeekOrigin.Begin);
                        i += 4;
                    }
                }
                var lastOffset = reader.ReadInt() + (int) reader.Position;

                var locpos = (int) reader.Position;

                while (locpos < lastOffset)
                {
                    var key = reader.ReadUTF();
                    var dataPos = reader.ReadInt();
                    locpos = (int) reader.Position;
                    reader.Seek(dataPos, SeekOrigin.Begin);
                    m_textIndexes.Add(key, reader.ReadUTF());
                    reader.Seek(locpos, SeekOrigin.Begin);
                }
            }
        }
    }
}