﻿// <copyright file="D2OReader.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using Strawberry.Core.Extensions;
using Strawberry.Core.IO;
using Strawberry.Protocol.Data;

namespace Strawberry.Protocol.Tools.D2O
{
    public class D2OReader
    {
        private const int NullIdentifier = unchecked((int) 0xAAAAAAAA);

        /// <summary>
        ///     Contains all assembly where the reader search d2o class
        /// </summary>
        public static List<Assembly> ClassesContainers = new List<Assembly>
        {
            typeof(Breed).Assembly
        };

        private static readonly Dictionary<Type, Func<object[], object>> objectCreators =
            new Dictionary<Type, Func<object[], object>>();


        private int m_classcount;
        private int m_headeroffset;
        private int m_indextablelen;
        private readonly IDataReader m_reader;

        /// <summary>
        ///     Create and initialise a new D2O file
        /// </summary>
        /// <param name="filePath">Path of the file</param>
        public D2OReader(string filePath)
            : this(new FastBigEndianReader(File.ReadAllBytes(filePath)))
        {
            FilePath = filePath;
        }

        public D2OReader(Stream stream)
        {
            m_reader = new BigEndianReader(stream);
            Initialize();
        }

        public D2OReader(IDataReader reader)
        {
            m_reader = reader;
            Initialize();
        }

        public Dictionary<int, D2OClassDefinition> Classes { get; private set; }

        public string FileName
        {
            get { return Path.GetFileNameWithoutExtension(FilePath); }
        }

        public string FilePath { get; }

        public int IndexCount
        {
            get { return Indexes.Count; }
        }

        public Dictionary<int, int> Indexes { get; private set; } = new Dictionary<int, int>();

        public void Close()
        {
            lock (m_reader)
            {
                m_reader.Dispose();
            }
        }


        /// <summary>
        ///     Get the class corresponding to the object at the given index
        /// </summary>
        public D2OClassDefinition GetObjectClass(int index)
        {
            lock (m_reader)
            {
                var offset = Indexes[index];
                m_reader.Seek(offset, SeekOrigin.Begin);

                var classid = m_reader.ReadInt();

                return Classes[classid];
            }
        }

        public Dictionary<int, D2OClassDefinition> GetObjectsClasses()
        {
            return Indexes.ToDictionary(index => index.Key, index => GetObjectClass(index.Key));
        }

        /// <summary>
        ///     Get an object from his index
        /// </summary>
        /// <param name="cloneReader">When sets to true it copies the reader to have a thread safe method</param>
        /// <returns></returns>
        public object ReadObject(int index, bool cloneReader = false)
        {
            if (cloneReader)
            {
                using (var reader = CloneReader())
                {
                    return ReadObject(index, reader);
                }
            }

            lock (m_reader)
            {
                return ReadObject(index, m_reader);
            }
        }

        public T ReadObject<T>(int index, bool cloneReader = false, bool noExceptionThrown = false)
            where T : class
        {
            if (cloneReader)
            {
                using (var reader = CloneReader())
                {
                    return ReadObject<T>(index, reader, noExceptionThrown);
                }
            }

            return ReadObject<T>(index, m_reader, noExceptionThrown);
        }

        /// <summary>
        ///     Get all objects that corresponding to T associated to his index
        /// </summary>
        /// <typeparam name="T">Corresponding class</typeparam>
        /// <param name="allownulled">True to adding null instead of throwing an exception</param>
        /// <returns></returns>
        public Dictionary<int, T> ReadObjects<T>(bool allownulled = false)
            where T : class
        {
            if (!IsTypeDefined(typeof(T)))
                throw new Exception("The file doesn't contain this class");

            var result = new Dictionary<int, T>(Indexes.Count);

            using (var reader = CloneReader())
            {
                foreach (var index in Indexes)
                {
                    reader.Seek(index.Value, SeekOrigin.Begin);

                    var classid = reader.ReadInt();

                    if (Classes[classid].ClassType == typeof(T) ||
                        Classes[classid].ClassType.IsSubclassOf(typeof(T)))
                    {
                        try
                        {
                            result.Add(index.Key, BuildObject(Classes[classid], reader) as T);
                        }
                        catch
                        {
                            if (allownulled)
                                result.Add(index.Key, default(T));
                            else
                                throw;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        ///     Get all objects associated to his index
        /// </summary>
        /// <param name="allownulled">True to adding null instead of throwing an exception</param>
        /// <returns></returns>
        public Dictionary<int, object> ReadObjects(bool allownulled = false, bool cloneReader = false)
        {
            var result = new Dictionary<int, object>(Indexes.Count);

            var reader = cloneReader ? CloneReader() : m_reader;

            foreach (var index in Indexes)
            {
                reader.Seek(index.Value, SeekOrigin.Begin);

                try
                {
                    result.Add(index.Key, ReadObject(index.Key, reader));
                }
                catch
                {
                    if (allownulled)
                        result.Add(index.Key, null);
                    else
                        throw;
                }
            }

            if (cloneReader)
                reader.Dispose();

            return result;
        }

        internal IDataReader CloneReader()
        {
            lock (m_reader)
            {
                if (m_reader.Position > 0)
                    m_reader.Seek(0, SeekOrigin.Begin);

                Stream streamClone;

                if (m_reader is FastBigEndianReader)
                    return new FastBigEndianReader((m_reader as FastBigEndianReader).Buffer);
                streamClone = new MemoryStream();
                ((BigEndianReader) m_reader).BaseStream.CopyTo(streamClone);

                return new BigEndianReader(streamClone);
            }
        }

        private object BuildObject(D2OClassDefinition classDefinition, IDataReader reader)
        {
            if (!objectCreators.ContainsKey(classDefinition.ClassType))
            {
                var creator = CreateObjectBuilder(classDefinition.ClassType,
                    classDefinition.Fields.Select(
                        entry => entry.Value.FieldInfo).ToArray());

                objectCreators.Add(classDefinition.ClassType, creator);
            }

            var values = new List<object>();
            foreach (var field in classDefinition.Fields.Values)
            {
                var fieldValue = ReadField(reader, field, field.TypeId);

                if (field.FieldType.IsInstanceOfType(fieldValue))
                    values.Add(fieldValue);
                else if (fieldValue is IConvertible &&
                         field.FieldType.GetInterface("IConvertible") != null)
                {
                    try
                    {
                        if (fieldValue is int i && i < 0 && field.FieldType == typeof(uint))
                            values.Add(unchecked ((uint) i));
                        else
                            values.Add(Convert.ChangeType(fieldValue, field.FieldType));
                    }
                    catch
                    {
                        throw new Exception($"Field '{classDefinition.Name}.{field.Name}' with value {fieldValue} is not of type '{fieldValue.GetType()}'");
                    }
                }
                else
                {
                    throw new Exception($"Field '{classDefinition.Name}.{field.Name}' with value {fieldValue} is not of type '{fieldValue.GetType()}'");
                }
            }

            return objectCreators[classDefinition.ClassType](values.ToArray());
        }

        private static Func<object[], object> CreateObjectBuilder(Type classType, params FieldInfo[] fields)
        {
            var fieldsType = from entry in fields
                select entry.FieldType;

            var method = new DynamicMethod(Guid.NewGuid().ToString("N"), typeof(object),
                new[] {typeof(object[])}.ToArray());

            var ilGenerator = method.GetILGenerator();

            ilGenerator.DeclareLocal(classType);
            ilGenerator.DeclareLocal(classType);

            ilGenerator.Emit(OpCodes.Newobj, classType.GetConstructor(Type.EmptyTypes));
            ilGenerator.Emit(OpCodes.Stloc_0);
            for (var i = 0; i < fields.Length; i++)
            {
                ilGenerator.Emit(OpCodes.Ldloc_0);
                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldc_I4, i);
                ilGenerator.Emit(OpCodes.Ldelem_Ref);

                if (fields[i].FieldType.IsClass)
                    ilGenerator.Emit(OpCodes.Castclass, fields[i].FieldType);
                else
                {
                    ilGenerator.Emit(OpCodes.Unbox_Any, fields[i].FieldType);
                }

                ilGenerator.Emit(OpCodes.Stfld, fields[i]);
            }

            ilGenerator.Emit(OpCodes.Ldloc_0);
            ilGenerator.Emit(OpCodes.Stloc_1);
            ilGenerator.Emit(OpCodes.Ldloc_1);
            ilGenerator.Emit(OpCodes.Ret);

            return
                (Func<object[], object>)
                    method.CreateDelegate(Expression.GetFuncType(new[] {typeof(object[]), typeof(object)}.ToArray()));
        }

        private static Type FindType(string className)
        {
            var correspondantTypes = from asm in ClassesContainers
                let types = asm.GetTypes()
                from type in types
                where
                    type.Name.Equals(className, StringComparison.InvariantCulture) &&
                    type.HasInterface(typeof(IDataObject))
                select type;

            return correspondantTypes.Single();
        }

        private void Initialize()
        {
            lock (m_reader)
            {
                var header = m_reader.ReadUTFBytes(3);

                if (header != "D2O")
                {
                    throw new Exception("Header doesn't equal the string \'D2O\' : Corrupted file");
                }

                ReadIndexTable();
                ReadClassesTable();
            }
        }

        private bool IsTypeDefined(Type type)
        {
            return Classes.Count(entry => entry.Value.ClassType == type) > 0;
        }

        private void ReadClassesTable()
        {
            m_classcount = m_reader.ReadInt();
            Classes = new Dictionary<int, D2OClassDefinition>(m_classcount);
            for (var i = 0; i < m_classcount; i++)
            {
                var classId = m_reader.ReadInt();

                var classMembername = m_reader.ReadUTF();
                var classPackagename = m_reader.ReadUTF();

                var classType = FindType(classMembername);

                var fieldscount = m_reader.ReadInt();
                var fields = new List<D2OFieldDefinition>(fieldscount);
                for (var l = 0; l < fieldscount; l++)
                {
                    var fieldname = m_reader.ReadUTF();
                    var fieldtype = (D2OFieldType) m_reader.ReadInt();

                    var vectorTypes = new List<Tuple<D2OFieldType, string>>();
                    if (fieldtype == D2OFieldType.List)
                    {
                        addVectorType:

                        var name = m_reader.ReadUTF();
                        var id = (D2OFieldType) m_reader.ReadInt();
                        vectorTypes.Add(Tuple.Create(id, name));

                        if (id == D2OFieldType.List)
                            goto addVectorType;
                    }
                    var field = classType.GetField(GetCamelCase(fieldname));

                    if (field == null)
                        throw new Exception($"Missing field '{GetCamelCase(fieldname)}' ({fieldtype}) in class '{classType.Name}'");

                    fields.Add(new D2OFieldDefinition(fieldname, fieldtype, field, m_reader.Position,
                        vectorTypes.ToArray()));
                }

                Classes.Add(classId,
                    new D2OClassDefinition(classId, classMembername, classPackagename, classType, fields,
                        m_reader.Position));
            }
        }

        string GetCamelCase(string name)
        {
            if (name.StartsWith("@"))
                name = name.Substring(1, name.Length - 1);

            // Hack : URL in Url class data
            if (name == "url")
                return "URL";
            return name.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries).Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1)).Aggregate(string.Empty, (s1, s2) => s1 + s2);
        }

        private object ReadField(IDataReader reader, D2OFieldDefinition field, D2OFieldType typeId,
            int vectorDimension = 0)
        {
            switch (typeId)
            {
                case D2OFieldType.Int:
                    return ReadFieldInt(reader);
                case D2OFieldType.Bool:
                    return ReadFieldBool(reader);
                case D2OFieldType.String:
                    return ReadFieldUTF(reader);
                case D2OFieldType.Double:
                    return ReadFieldDouble(reader);
                case D2OFieldType.I18N:
                    return ReadFieldI18N(reader);
                case D2OFieldType.UInt:
                    return ReadFieldUInt(reader);
                case D2OFieldType.List:
                    return ReadFieldVector(reader, field, vectorDimension);
                default:
                    return ReadFieldObject(reader);
            }
        }

        private static bool ReadFieldBool(IDataReader reader)
        {
            return reader.ReadByte() != 0;
        }

        private static double ReadFieldDouble(IDataReader reader)
        {
            return reader.ReadDouble();
        }

        private static int ReadFieldI18N(IDataReader reader)
        {
            return reader.ReadInt();
        }

        private static int ReadFieldInt(IDataReader reader)
        {
            return reader.ReadInt();
        }

        private object ReadFieldObject(IDataReader reader)
        {
            var classid = reader.ReadInt();

            if (classid == NullIdentifier)
                return null;

            if (Classes.Keys.Contains(classid))
                return BuildObject(Classes[classid], reader);

            return null;
        }

        private static uint ReadFieldUInt(IDataReader reader)
        {
            return reader.ReadUInt();
        }

        private static string ReadFieldUTF(IDataReader reader)
        {
            return reader.ReadUTF();
        }


        private object ReadFieldVector(IDataReader reader, D2OFieldDefinition field, int vectorDimension)
        {
            var count = reader.ReadInt();

            var vectorType = field.FieldType;
            for (var i = 0; i < vectorDimension; i++)
            {
                vectorType = vectorType.GetGenericArguments()[0];
            }

            lock (objectCreators)
                // We sometimes have error on objectCreators.Add(vectorType, creator) : mainLock allready in the dictionary
                if (!objectCreators.ContainsKey(vectorType))
                {
                    var creator = CreateObjectBuilder(vectorType);

                    objectCreators.Add(vectorType, creator);
                }

            var result = objectCreators[vectorType](new object[0]) as IList;

            for (var i = 0; i < count; i++)
            {
                vectorDimension++;
                if (vectorDimension <= 0)
                    continue;
                // i didn't found a way to have thez correct dimension so i just add "- 1"
                var f = ReadField(reader, field, field.VectorTypes[vectorDimension - 1].Item1, vectorDimension);
                result?.Add(f);
                vectorDimension--;
            }

            return result;
        }

        private void ReadIndexTable()
        {
            m_headeroffset = m_reader.ReadInt();
            m_reader.Seek(m_headeroffset, SeekOrigin.Begin); // place the reader at the beginning of the indextable
            m_indextablelen = m_reader.ReadInt();

            // init table index
            Indexes = new Dictionary<int, int>(m_indextablelen/8);
            for (var i = 0; i < m_indextablelen; i += 8)
            {
                Indexes.Add(m_reader.ReadInt(), m_reader.ReadInt());
            }
        }

        private object ReadObject(int index, IDataReader reader)
        {
            var offset = Indexes[index];
            reader.Seek(offset, SeekOrigin.Begin);

            var classid = reader.ReadInt();

            var result = BuildObject(Classes[classid], reader);

            return result;
        }

        private T ReadObject<T>(int index, IDataReader reader, bool noExceptionThrown = false)
            where T : class
        {
            if (!IsTypeDefined(typeof(T)))
                throw new Exception("The file doesn't contain this class"); // Serious error, exception always thrown

            var offset = 0;
            if (!Indexes.TryGetValue(index, out offset))
            {
                if (noExceptionThrown) return null;
                throw new Exception($"Can't find Index {index} in {FileName}");
            }

            reader.Seek(offset, SeekOrigin.Begin);

            var classid = reader.ReadInt();

            if (Classes[classid].ClassType != typeof(T) && !Classes[classid].ClassType.IsSubclassOf(typeof(T)))
                throw new Exception(string.Format("Wrong type, try to read object with {1} instead of {0}",
                    typeof(T).Name, Classes[classid].ClassType.Name));

            return BuildObject(Classes[classid], reader) as T;
        }
    }
}