// <copyright file="D2OFieldType.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

namespace Strawberry.Protocol.Tools.D2O
{
    public enum D2OFieldType
    {
        Int = -1,
        Bool = -2,
        String = -3,
        Double = -4,
        I18N = -5,
        UInt = -6,
        List = -99
    }
}