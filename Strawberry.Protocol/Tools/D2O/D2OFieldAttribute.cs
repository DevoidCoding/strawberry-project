// <copyright file="D2OFieldAttribute.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;

namespace Strawberry.Protocol.Tools.D2O
{
    public class D2OFieldAttribute : Attribute
    {
        public D2OFieldAttribute()
        {
        }

        public D2OFieldAttribute(string fieldName)
        {
            FieldName = fieldName;
        }

        public string FieldName { get; set; }
    }
}