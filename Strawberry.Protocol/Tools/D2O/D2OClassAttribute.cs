﻿// <copyright file="D2OClassAttribute.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;

namespace Strawberry.Protocol.Tools.D2O
{
    public class D2OClassAttribute : Attribute
    {
        public D2OClassAttribute(string name, bool autoBuild = true)
        {
            Name = name;
            AutoBuild = autoBuild;
        }

        public D2OClassAttribute(string name, string packageName, bool autoBuild = true)
        {
            Name = name;
            PackageName = packageName;
            AutoBuild = autoBuild;
        }

        public bool AutoBuild { get; set; }

        public string Name { get; set; }

        public string PackageName { get; set; }
    }
}