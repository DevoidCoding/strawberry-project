﻿// <copyright file="DlmCell.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmCell
    {
        public DlmCell(DlmLayer layer)
        {
            Layer = layer;
        }

        public DlmBasicElement[] Elements { get; set; }

        public short Id { get; set; }

        public DlmLayer Layer { get; set; }

        public static DlmCell ReadFromStream(DlmLayer layer, IDataReader reader)
        {
            var cell = new DlmCell(layer);

            cell.Id = reader.ReadShort();
            cell.Elements = new DlmBasicElement[reader.ReadShort()];

            for (var i = 0; i < cell.Elements.Length; i++)
            {
                var element =
                    DlmBasicElement.ReadFromStream(cell, reader);
                cell.Elements[i] = element;
            }

            return cell;
        }
    }
}