﻿// <copyright file="DlmSoundElement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmSoundElement : DlmBasicElement
    {
        public DlmSoundElement(DlmCell cell)
            : base(cell)
        {
        }

        public int BaseVolume { get; set; }

        public int FullVolumedistance { get; set; }

        public int MaxDelayBetweenloops { get; set; }

        public int MinDelayBetweenloops { get; set; }

        public int NullVolumedistance { get; set; }

        public int SoundId { get; set; }


        public new static DlmSoundElement ReadFromStream(DlmCell cell, IDataReader reader)
        {
            var element = new DlmSoundElement(cell);

            element.SoundId = reader.ReadInt();
            element.BaseVolume = reader.ReadShort();
            element.FullVolumedistance = reader.ReadInt();
            element.NullVolumedistance = reader.ReadInt();
            element.MinDelayBetweenloops = reader.ReadShort();
            element.MaxDelayBetweenloops = reader.ReadShort();

            return element;
        }
    }
}