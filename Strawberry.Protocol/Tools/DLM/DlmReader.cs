﻿// <copyright file="DlmReader.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.IO;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmReader : IDisposable
    {
        /// <summary>
        ///     Returns the decryption mainLock
        /// </summary>
        /// <param name="mapId">The map to decrypt</param>
        /// <returns>The decryption mainLock</returns>
        public delegate string KeyProvider(uint mapId);

        private IDataReader m_reader;

        public DlmReader(string filePath)
        {
            m_reader = new FastBigEndianReader(File.ReadAllBytes(filePath));
        }

        public DlmReader(string filePath, string decryptionKey)
        {
            m_reader = new FastBigEndianReader(File.ReadAllBytes(filePath));
            DecryptionKey = decryptionKey;
        }

        public DlmReader(IDataReader reader)
        {
            m_reader = reader;
        }

        public DlmReader(Stream stream)
        {
            m_reader = new BigEndianReader(stream);
        }

        public DlmReader(byte[] buffer)
        {
            m_reader = new FastBigEndianReader(buffer);
        }

        public string DecryptionKey { get; set; }

        public KeyProvider DecryptionKeyProvider { get; set; }

        public void Dispose()
        {
            m_reader.Dispose();
        }

        public DlmMap ReadMap()
        {
            m_reader.Seek(0, SeekOrigin.Begin);
            int header = m_reader.ReadByte();

            if (header != 77)
            {
                try
                {
                    m_reader.Seek(0, SeekOrigin.Begin);
                    var output = new MemoryStream();
                    ZipHelper.Deflate(new MemoryStream(m_reader.ReadBytes((int) m_reader.BytesAvailable)), output);

                    var uncompress = output.ToArray();

                    m_reader.Dispose();
                    m_reader = new FastBigEndianReader(uncompress);

                    header = m_reader.ReadByte();

                    if (header != 77)
                        throw new FileLoadException("Wrong header file");
                }
                catch (Exception ex)
                {
                    throw new FileLoadException("Wrong header file");
                }
            }

            var map = DlmMap.ReadFromStream(m_reader, this);

            return map;
        }
    }
}