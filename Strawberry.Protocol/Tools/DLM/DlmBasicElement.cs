﻿// <copyright file="DlmBasicElement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public abstract class DlmBasicElement
    {
        public enum ElementTypesEnum
        {
            Graphical = 2,
            Sound = 33
        }

        protected DlmBasicElement(DlmCell cell)
        {
            Cell = cell;
        }

        public DlmCell Cell { get; private set; }

        public static DlmBasicElement ReadFromStream(DlmCell cell, IDataReader reader)
        {
            var type = reader.ReadByte();

            switch ((ElementTypesEnum) type)
            {
                case ElementTypesEnum.Graphical:
                    return DlmGraphicalElement.ReadFromStream(cell, reader);

                case ElementTypesEnum.Sound:
                    return DlmSoundElement.ReadFromStream(cell, reader);

                default:
                    System.Diagnostics.Debug.Write($"Unknown element ID : {type.ToString().PadLeft(3)}; MapID : {cell.Layer.Map.Id.ToString().PadLeft(int.MaxValue.ToString().Length)} CellID : {cell.Id}{Environment.NewLine}");
                    return null;
            }
        }
    }
}