﻿// <copyright file="DlmFixture.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmFixture
    {
        public DlmFixture(DlmMap map)
        {
            Map = map;
        }

        public byte Alpha { get; set; }

        public int FixtureId { get; private set; }

        public int Hue { get; set; }

        public DlmMap Map { get; set; }

        public Point Offset { get; set; }

        public short Rotation { get; set; }

        public short ScaleX { get; set; }

        public short ScaleY { get; set; }

        public static DlmFixture ReadFromStream(DlmMap map, IDataReader reader)
        {
            var fixture = new DlmFixture(map);

            fixture.FixtureId = reader.ReadInt();
            fixture.Offset = new Point(reader.ReadShort(), reader.ReadShort());
            fixture.Rotation = reader.ReadShort();
            fixture.ScaleX = reader.ReadShort();
            fixture.ScaleY = reader.ReadShort();
            fixture.Hue = reader.ReadByte() << 16 | reader.ReadByte() << 8 | reader.ReadByte();
            fixture.Alpha = reader.ReadByte();

            return fixture;
        }
    }
}