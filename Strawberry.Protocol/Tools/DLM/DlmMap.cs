﻿// <copyright file="DlmMap.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Strawberry.Core.IO;
using Strawberry.Protocol.Data;
using Point = System.Drawing.Point;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmMap : IDataObject
    {
        public const int CellCount = 560;
        public const uint Height = 20;

        public const uint Width = 14;
        private static readonly Point[] s_orthogonalGridReference = new Point[CellCount];
        private static bool m_initialized;


        public DlmFixture[] BackgroudFixtures { get; set; }

        public Color BackgroundColor { get; set; }

        public int BottomNeighbourId { get; set; }

        public DlmCellData[] Cells { get; set; }

        public bool Encrypted { get; set; }

        public byte EncryptionVersion { get; set; }

        public DlmFixture[] ForegroundFixtures { get; set; }

        public int GroundCRC { get; set; }

        public uint Id { get; set; }

        public DlmLayer[] Layers { get; set; }

        public int LeftNeighbourId { get; set; }

        public byte MapType { get; set; }

        public int PresetId { get; set; }

        public uint RelativeId { get; set; }

        public int RightNeighbourId { get; set; }

        public uint ShadowBonusOnEntities { get; set; }

        public int SubAreaId { get; set; }

        public int TopNeighbourId { get; set; }

        public bool UseLowPassFilter { get; set; }

        public bool UseReverb { get; set; }

        public bool UsingNewMovementSystem { get; set; }

        public byte Version { get; set; }

        public short ZoomOffsetX { get; set; }

        public short ZoomOffsetY { get; set; }

        public ushort ZoomScale { get; set; }

        public List<int> TopArrowCellIds { get; set; } = new List<int>();
        public List<int> BottomArrowCellIds { get; set; } = new List<int>();
        public List<int> LeftArrowCellIds { get; set; } = new List<int>();
        public List<int> RightArrowCellIds { get; set; } = new List<int>();

        public static DlmMap ReadFromStream(IDataReader givenReader, DlmReader dlmReader)
        {
            var reader = givenReader;
            var map = new DlmMap();

            map.Version = reader.ReadByte();
            map.Id = reader.ReadUInt();

            if (map.Version >= 7)
            {
                map.Encrypted = reader.ReadBoolean();
                map.EncryptionVersion = reader.ReadByte();

                var len = reader.ReadInt();

                if (map.Encrypted)
                {
                    var key = dlmReader.DecryptionKey;

                    if (key == null && dlmReader.DecryptionKeyProvider != null)
                        key = dlmReader.DecryptionKeyProvider(map.Id);

                    if (key == null)
                    {
                        throw new InvalidOperationException(
                            $"Cannot decrypt the map {map.Id} without decryption key");
                    }

                    var data = reader.ReadBytes(len);
                    var encodedKey = Encoding.Default.GetBytes(key);

                    if (key.Length > 0)
                    {
                        for (var i = 0; i < data.Length; i++)
                        {
                            data[i] = (byte) (data[i] ^ encodedKey[i%key.Length]);
                        }

                        reader = new FastBigEndianReader(data);
                    }
                }
            }

            map.RelativeId = reader.ReadUInt();
            map.MapType = reader.ReadByte();

            // temp, just to know if the result is coherent
            if (map.MapType > 1)
                throw new Exception("Invalid decryption key");

            map.SubAreaId = reader.ReadInt();
            map.TopNeighbourId = reader.ReadInt();
            map.BottomNeighbourId = reader.ReadInt();
            map.LeftNeighbourId = reader.ReadInt();
            map.RightNeighbourId = reader.ReadInt();
            map.ShadowBonusOnEntities = reader.ReadUInt();

            if (map.Version >= 9)
            {
                var readColor = reader.ReadInt();
                var backgroundAlpha = (int)((readColor & 4278190080) >> 24);
                var backgroundRed = (readColor & 16711680) >> 16;
                var backgroundGreen = (readColor & 65280) >> 8;
                var backgroundBlue = readColor & 255;
                var backgroundColor = (backgroundAlpha & 255) << 24 | (backgroundRed & 255) << 16 | (backgroundGreen & 255) << 8 | backgroundBlue & 255;
                map.BackgroundColor = Color.FromArgb(backgroundColor);
                var uReadColor = (int)reader.ReadUInt();
                var gridAlpha = (int)((uReadColor & 4278190080) >> 24);
                var gridRed = (uReadColor & 16711680) >> 16;
                var gridGreen = (uReadColor & 65280) >> 8;
                var gridBlue = uReadColor & 255;
                var gridColor = (gridAlpha & 255) << 24 | (gridRed & 255) << 16 | (gridGreen & 255) << 8 | gridBlue & 255;
                map.GridColor = Color.FromArgb(gridColor);
            }
            else if (map.Version >= 3)
            {
                map.BackgroundColor = Color.FromArgb(reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
            }

            if (map.Version >= 4)
            {
                map.ZoomScale = reader.ReadUShort();
                map.ZoomOffsetX = reader.ReadShort();
                map.ZoomOffsetY = reader.ReadShort();
            }

            if (map.Version > 10)
            {
                map.TactictalModeTemplateId = reader.ReadInt();
            }

            map.UseLowPassFilter = reader.ReadByte() == 1;
            map.UseReverb = reader.ReadByte() == 1;

            if (map.UseReverb)
            {
                map.PresetId = reader.ReadInt();
            }
            {
                map.PresetId = -1;
            }

            map.BackgroudFixtures = new DlmFixture[reader.ReadByte()];
            for (var i = 0; i < map.BackgroudFixtures.Length; i++)
            {
                map.BackgroudFixtures[i] = DlmFixture.ReadFromStream(map, reader);
            }

            map.ForegroundFixtures = new DlmFixture[reader.ReadByte()];
            for (var i = 0; i < map.ForegroundFixtures.Length; i++)
            {
                map.ForegroundFixtures[i] = DlmFixture.ReadFromStream(map, reader);
            }

            reader.ReadInt();
            map.GroundCRC = reader.ReadInt();

            map.Layers = new DlmLayer[reader.ReadByte()];
            for (var i = 0; i < map.Layers.Length; i++)
            {
                map.Layers[i] = DlmLayer.ReadFromStream(map, reader);
            }

            map.Cells = new DlmCellData[CellCount];
            int? lastMoveZone = null;
            for (short i = 0; i < map.Cells.Length; i++)
            {
                map.Cells[i] = DlmCellData.ReadFromStream(i, map, reader);
                if (!lastMoveZone.HasValue)
                    lastMoveZone = map.Cells[i].MoveZone;
                else if (lastMoveZone != map.Cells[i].MoveZone) // if a cell is different the new system is used
                    map.UsingNewMovementSystem = true;
            }

            return map;
        }

        public int TactictalModeTemplateId { get; set; }

        public Color GridColor { get; set; }
    }
}