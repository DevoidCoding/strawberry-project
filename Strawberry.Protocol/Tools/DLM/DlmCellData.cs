﻿// <copyright file="DlmCellData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.Linq;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public struct DlmCellData
    {
        public DlmCellData(short id)
        {
            Id = id;
            _losmov = 3;
            _arrow = 0;
            _linkedZone = 0;
            MoveZone = 0;
            MapChangeData = 0;
            Speed = 0;
            NonWalkableDuringRP = false;
            HavenbagCell = false;
            FarmCell = false;
            Visible = false;
            Red = false;
            Blue = false;
            Los = false;
            NonWalkableDuringFight = false;
            Mov = false;
            Floor = 0;
        }

        public short Id;
        private int _losmov;
        private int _arrow;
        private int _linkedZone;

        public bool UseTopArrow => (_arrow & 1) != 0;

        public bool UseBottomArrow => (_arrow & 2) != 0;

        public bool UseRightArrow => (_arrow & 4) != 0;

        public bool UseLeftArrow => (_arrow & 8) != 0;

        // ReSharper disable once InconsistentNaming
        public bool HasLinkedZoneRP => Mov && !FarmCell;

        // ReSharper disable once InconsistentNaming
        public int LinkedZoneRP => (_linkedZone & 240) >> 4;
        public bool HasLinkedZoneFight => Mov && !NonWalkableDuringFight && !FarmCell && !HavenbagCell;
        public int LinkedZoneFight => _linkedZone & 15;

        public static DlmCellData ReadFromStream(short id, DlmMap map, IDataReader reader)
        {
            var cell = new DlmCellData(id);

            cell.Floor = reader.ReadSByte() * 10;

            if (cell.Floor == -1280)
                return cell;

            if (map.Version >= 9)
            {
                int tmpbytesv9 = reader.ReadShort();
                cell.Mov = (tmpbytesv9 & 1) == 0;
                cell.NonWalkableDuringFight = (tmpbytesv9 & 2) != 0;
                cell.NonWalkableDuringRP = (tmpbytesv9 & 4) != 0;
                cell.Los = (tmpbytesv9 & 8) == 0;
                cell.Blue = (tmpbytesv9 & 16) != 0;
                cell.Red = (tmpbytesv9 & 32) != 0;
                cell.Visible = (tmpbytesv9 & 64) != 0;
                cell.FarmCell = (tmpbytesv9 & 128) != 0;
                var topArrow = false;
                var bottomArrow = false;
                var rightArrow = false;
                var leftArrow = false;
                if (map.Version >= 10)
                {
                    cell.HavenbagCell = (tmpbytesv9 & 256) != 0;
                    topArrow = (tmpbytesv9 & 512) != 0;
                    bottomArrow = (tmpbytesv9 & 1024) != 0;
                    rightArrow = (tmpbytesv9 & 2048) != 0;
                    leftArrow = (tmpbytesv9 & 4096) != 0;
                }
                else
                {
                    topArrow = (tmpbytesv9 & 256) != 0;
                    bottomArrow = (tmpbytesv9 & 512) != 0;
                    rightArrow = (tmpbytesv9 & 1024) != 0;
                    leftArrow = (tmpbytesv9 & 2048) != 0;
                }
                if (topArrow)
                {
                    map.TopArrowCellIds.Add(id);
                }
                if (bottomArrow)
                {
                    map.BottomArrowCellIds.Add(id);
                }
                if (rightArrow)
                {
                    map.RightArrowCellIds.Add(id);
                }
                if (leftArrow)
                {
                    map.LeftArrowCellIds.Add(id);
                }
            }
            else
            {
                cell._losmov = reader.ReadByte();
                cell.Los = (cell._losmov & 2) >> 1 == 1;
                cell.Mov = (cell._losmov & 1) == 1;
                cell.Visible = (cell._losmov & 64) >> 6 == 1;
                cell.FarmCell = (cell._losmov & 32) >> 5 == 1;
                cell.Blue = (cell._losmov & 16) >> 4 == 1;
                cell.Red = (cell._losmov & 8) >> 3 == 1;
                cell.NonWalkableDuringRP = (cell._losmov & 128) >> 7 == 1;
                cell.NonWalkableDuringFight = (cell._losmov & 4) >> 2 == 1;
            }

            cell.Speed = reader.ReadSByte();
            cell.MapChangeData = reader.ReadSByte();
            if (map.Version > 5)
            {
                cell.MoveZone = reader.ReadByte();
            }

            if (map.Version >= 10 && (cell.HasLinkedZoneRP && cell.HasLinkedZoneFight))
            {
                cell._linkedZone = reader.ReadByte();
            }
            if (map.Version > 7 && map.Version < 9)
            {
                int tmpBits = reader.ReadSByte();
                cell._arrow = 15 & tmpBits;
                if (cell.UseTopArrow)
                {
                    map.TopArrowCellIds.Add(id);
                }
                if (cell.UseBottomArrow)
                {
                    map.BottomArrowCellIds.Add(id);
                }
                if (cell.UseLeftArrow)
                {
                    map.RightArrowCellIds.Add(id);
                }
                if (cell.UseRightArrow)
                {
                    map.LeftArrowCellIds.Add(id);
                }
            }

            return cell;
        }

        public byte MoveZone;

        public sbyte MapChangeData;

        public sbyte Speed;

        // ReSharper disable once InconsistentNaming
        public bool NonWalkableDuringRP;

        public bool HavenbagCell;

        public bool FarmCell;

        public bool Visible;

        public bool Red;

        public bool Blue;

        public bool Los;

        public bool NonWalkableDuringFight;

        public bool Mov;

        public int Floor;
    }
}