﻿// <copyright file="DlmLayer.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmLayer
    {
        public DlmLayer(DlmMap map)
        {
            Map = map;
        }

        public DlmCell[] Cells { get; set; }

        public int LayerId { get; set; }

        public DlmMap Map { get; set; }

        public static DlmLayer ReadFromStream(DlmMap map, IDataReader reader)
        {
            var layer = new DlmLayer(map);

            layer.LayerId = map.Version >= 9 ? reader.ReadByte() : reader.ReadInt();
            layer.Cells = new DlmCell[reader.ReadShort()];
            for (var i = 0; i < layer.Cells.Length; i++)
            {
                layer.Cells[i] = DlmCell.ReadFromStream(layer, reader);
            }

            return layer;
        }
    }
}