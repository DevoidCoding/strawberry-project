﻿// <copyright file="DlmGraphicalElement.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Dlm
{
    public class DlmGraphicalElement : DlmBasicElement
    {
        public const float CELL_HALF_HEIGHT = 21.5f;
        public const float CELL_HALF_WIDTH = 43;

        private Point m_offset;
        private Point m_pixelOffset;

        public DlmGraphicalElement(DlmCell cell)
            : base(cell)
        {
        }

        public int Altitude { get; set; }

        public ColorMultiplicator ColorMultiplicator { get; private set; }

        public uint ElementId { get; set; }

        public ElementTypesEnum ElementType
        {
            get { return ElementTypesEnum.Graphical; }
        }

        public ColorMultiplicator FinalTeint
        {
            get { return ColorMultiplicator; }
            set { ColorMultiplicator = value; }
        }

        public ColorMultiplicator Hue { get; set; }

        public uint Identifier { get; set; }

        public Point Offset
        {
            get { return m_offset; }
            set { m_offset = value; }
        }

        public Point PixelOffset
        {
            get { return m_pixelOffset; }
            set { m_pixelOffset = value; }
        }

        public ColorMultiplicator Shadow { get; set; }

        public void CalculateFinalTeint()
        {
            var loc1 = Hue.Red + Shadow.Red;
            var loc2 = Hue.Green + Shadow.Green;
            var loc3 = Hue.Blue + Shadow.Blue;

            loc1 = ColorMultiplicator.Clamp((loc1 + 128)*2, 0, 512);
            loc2 = ColorMultiplicator.Clamp((loc2 + 128)*2, 0, 512);
            loc3 = ColorMultiplicator.Clamp((loc3 + 128)*2, 0, 512);

            ColorMultiplicator = new ColorMultiplicator(loc1, loc2, loc3, true);
        }

        public new static DlmGraphicalElement ReadFromStream(DlmCell cell, IDataReader reader)
        {
            var element = new DlmGraphicalElement(cell);

            element.ElementId = reader.ReadUInt();
            element.Hue = new ColorMultiplicator(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), false);
            element.Shadow = new ColorMultiplicator(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), false);

            if (cell.Layer.Map.Version <= 4)
            {
                element.m_offset.X = reader.ReadByte();
                element.m_offset.Y = reader.ReadByte();
                element.m_pixelOffset.X = (int) (element.m_offset.X*CELL_HALF_WIDTH);
                element.m_pixelOffset.Y = (int) (element.m_offset.Y*CELL_HALF_HEIGHT);
            }
            else
            {
                element.m_pixelOffset.X = reader.ReadShort();
                element.m_pixelOffset.Y = reader.ReadShort();
                element.m_offset.X = (int) (element.m_pixelOffset.X/CELL_HALF_WIDTH);
                element.m_offset.Y = (int) (element.m_pixelOffset.Y/CELL_HALF_HEIGHT);
            }

            element.Altitude = reader.ReadByte();
            element.Identifier = reader.ReadUInt();

            // we don't care
            //element.CalculateFinalTeint();

            return element;
        }
    }
}