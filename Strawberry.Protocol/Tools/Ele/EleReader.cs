﻿// <copyright file="EleReader.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.IO;
using Strawberry.Core.IO;
using Strawberry.Core.Reflection;

namespace Strawberry.Protocol.Tools.Ele
{
    public class EleReader : Singleton<EleReader>, IDisposable
    {
        private BigEndianReader _reader;
        private Stream _stream;
        private EleInstance _eleInstance;

        public EleInstance EleInstance => _eleInstance ?? throw new Exception("Not initialized");
        public BigEndianReader Reader => _reader;
        private bool _initialized;

        public void Dispose()
        {
            _stream.Dispose();
            _reader.Dispose();
        }

        public void Initialize(string path)
        {
            if (_initialized)
                return;

            _stream = File.OpenRead(path);
            _reader = new BigEndianReader(_stream);

            _reader.Seek(0, SeekOrigin.Begin);

            int header = _reader.ReadByte();

            if (header != 69)
            {
                try
                {
                    var uncompress = ZipHelper.Uncompress(_reader.Data);

                    if (uncompress.Length <= 0 || uncompress[0] != 69)
                        throw new FileLoadException("Wrong header file");

                    ChangeStream(new MemoryStream(uncompress));
                }
                catch (Exception)
                {
                    throw new FileLoadException("Wrong header file");
                }
            }
            
            header = _reader.ReadByte();
            if (header != 69)
                throw new FileLoadException("Wrong header file");
            _eleInstance = EleInstance.ReadFromStream(_reader);
            _initialized = true;
        }

        private void ChangeStream(Stream stream)
        {
            _stream.Dispose();
            _reader.Dispose();

            _stream = stream;
            _reader = new BigEndianReader(_stream);
        }
    }
}