﻿// <copyright file="EntityGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class EntityGraphicalElementData : EleGraphicalData, INotifyPropertyChanged
    {
        public EntityGraphicalElementData(EleInstance instance, int id)
            : base(instance, id)
        {
        }

        public string EntityLook { get; set; }

        public bool HorizontalSymmetry { get; set; }

        public uint MaxDelay { get; set; }

        public uint MinDelay { get; set; }

        public bool PlayAnimation { get; set; }

        public bool PlayAnimStatic { get; set; }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.ENTITY; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static EntityGraphicalElementData ReadFromStream(EleInstance instance, int id, BigEndianReader reader)
        {
            var data = new EntityGraphicalElementData(instance, id);

            data.EntityLook = reader.ReadUTF7BitLength();
            data.HorizontalSymmetry = reader.ReadBoolean();

            if (instance.Version >= 7)
            {
                data.PlayAnimation = reader.ReadBoolean();
            }

            if (instance.Version >= 6)
            {
                data.PlayAnimStatic = reader.ReadBoolean();
            }

            if (instance.Version >= 5)
            {
                data.MinDelay = (uint)reader.ReadInt();
                data.MaxDelay = (uint)reader.ReadInt();
            }

            return data;
        }
    }
}