﻿// <copyright file="NormalGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class NormalGraphicalElementData : EleGraphicalData, INotifyPropertyChanged
    {
        public NormalGraphicalElementData(EleInstance instance, int id) : base(instance, id)
        {
        }

        public int Gfx { get; set; }

        public uint Height { get; set; }

        public bool HorizontalSymmetry { get; set; }

        public Point Origin { get; set; }

        public Point Size { get; set; }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.NORMAL; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static NormalGraphicalElementData ReadFromStream(EleInstance instance, int id, BigEndianReader reader)
        {
            var data = new NormalGraphicalElementData(instance, id);

            data.Gfx = reader.ReadInt();
            data.Height = reader.ReadByte();
            data.HorizontalSymmetry = reader.ReadBoolean();
            data.Origin = new Point(reader.ReadShort(), reader.ReadShort());
            data.Size = new Point(reader.ReadShort(), reader.ReadShort());

            return data;
        }
    }
}