﻿// <copyright file="ParticlesGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class ParticlesGraphicalElementData : EleGraphicalData, INotifyPropertyChanged
    {
        public ParticlesGraphicalElementData(EleInstance instance, int id) : base(instance, id)
        {
        }

        public int ScriptId { get; set; }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.ANIMATED; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static ParticlesGraphicalElementData ReadFromStream(EleInstance instance, int id, BigEndianReader reader)
        {
            var data = EleGraphicalData.ReadFromStream(instance, id, reader) as ParticlesGraphicalElementData;

            data.ScriptId = reader.ReadShort();

            return data;
        }
    }
}