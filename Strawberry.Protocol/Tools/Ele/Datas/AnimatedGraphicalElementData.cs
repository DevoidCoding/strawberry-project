﻿// <copyright file="AnimatedGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class AnimatedGraphicalElementData : NormalGraphicalElementData, INotifyPropertyChanged
    {
        public AnimatedGraphicalElementData(EleInstance instance, int id) : base(instance, id)
        {
        }

        public uint MaxDelay { get; set; }

        public uint MinDelay { get; set; }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.ANIMATED; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public new static AnimatedGraphicalElementData ReadFromStream(EleInstance instance, int id,
            BigEndianReader reader)
        {
            var data = NormalGraphicalElementData.ReadFromStream(instance, id, reader) as AnimatedGraphicalElementData;

            if (instance.Version != 4)
                return data;

            data.MinDelay = (uint)reader.ReadInt();
            data.MaxDelay = (uint)reader.ReadInt();

            return data;
        }
    }
}