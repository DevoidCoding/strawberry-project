﻿// <copyright file="BoundingBoxGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class BoundingBoxGraphicalElementData : NormalGraphicalElementData, INotifyPropertyChanged
    {
        public BoundingBoxGraphicalElementData(EleInstance instance, int id) : base(instance, id)
        {
        }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.BOUNDING_BOX; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public new static BoundingBoxGraphicalElementData ReadFromStream(EleInstance instance, int id,
            BigEndianReader reader)
        {
            var data = NormalGraphicalElementData.ReadFromStream(instance, id, reader) as BoundingBoxGraphicalElementData;

            return data;
        }
    }
}