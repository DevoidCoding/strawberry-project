﻿// <copyright file="BlendedGraphicalElementData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System.ComponentModel;
using System.Drawing;
using Strawberry.Core.IO;

namespace Strawberry.Protocol.Tools.Ele.Datas
{
    public class BlendedGraphicalElementData : NormalGraphicalElementData, INotifyPropertyChanged
    {
        public BlendedGraphicalElementData(EleInstance instance, int id)
            : base(instance, id)
        {
        }

        public BlendedGraphicalElementData(EleInstance instance, int id, NormalGraphicalElementData elementData) : base(instance, id)
        {
            Gfx = elementData.Gfx;
            Height = elementData.Height;
            HorizontalSymmetry = elementData.HorizontalSymmetry;
            Origin = elementData.Origin;
            Size = elementData.Size;
        }

        public string BlendMode { get; set; }

        public override EleGraphicalElementTypes Type
        {
            get { return EleGraphicalElementTypes.BLENDED; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public new static BlendedGraphicalElementData ReadFromStream(EleInstance instance, int id,
            BigEndianReader reader)
        {
            var data = new BlendedGraphicalElementData(instance, id, NormalGraphicalElementData.ReadFromStream(instance, id, reader));

            data.BlendMode = reader.ReadUTF7BitLength();

            return data;
        }
    }
}