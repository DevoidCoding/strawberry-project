﻿// <copyright file="EleInstance.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Strawberry.Core.IO;
using Strawberry.Core.Reflection;

namespace Strawberry.Protocol.Tools.Ele
{
    public class EleInstance : INotifyPropertyChanged
    {
        public EleInstance()
        {
            ElementsIndex = new Dictionary<int, long>();
            GraphicalDatas = new Dictionary<int, EleGraphicalData>();
            GfxJpgMap = new Dictionary<int, bool>();
        }

        public Dictionary<int, bool> GfxJpgMap { get; set; }

        public Dictionary<int, EleGraphicalData> GraphicalDatas { get; set; }

        public Dictionary<int, long> ElementsIndex { get; set; }

        public byte Version { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public EleGraphicalData GetElementData(int elementId)
        {
            if (!GraphicalDatas.ContainsKey(elementId))
                GraphicalDatas[elementId] = ReadElement(elementId);
            return GraphicalDatas[elementId];
        }

        public static EleInstance ReadFromStream(BigEndianReader reader)
        {
            var instance = new EleInstance {Version = reader.ReadByte()};


            var count = reader.ReadUInt();
            var skipLen = 0;
            for (var i = 0; i < count; i++)
            {
                if (instance.Version >= 9)
                    skipLen = reader.ReadUShort();
                var edId = reader.ReadInt();
                if (instance.Version <= 8)
                {
                    instance.ElementsIndex.Add(edId, reader.Position);
                }
                else
                {
                    instance.ElementsIndex.Add(edId, reader.Position);
                    reader.SetPosition(skipLen - 4, SeekOrigin.Current);
                }
            }

            if (instance.Version >= 8)
            {
                var gfxCount = reader.ReadInt();
                for (var i = 0; i < gfxCount; i++)
                {
                    instance.GfxJpgMap.Add(reader.ReadInt(), true);
                }
            }

            return instance;
        }

        public EleGraphicalData ReadElement(int edId)
        {
            var reader = EleReader.Instance.Reader;
            var elementIndex = ElementsIndex[edId];
            if (elementIndex > int.MaxValue)
                throw new OverflowException("Position > int.MaxValue");

            reader.SetPosition((int)elementIndex);
            return EleGraphicalData.ReadFromStream(EleReader.Instance.EleInstance, edId, reader);
        }
    }
}