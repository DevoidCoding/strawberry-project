﻿// <copyright file="EleGraphicalData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

using System;
using System.ComponentModel;
using Strawberry.Core.IO;
using Strawberry.Protocol.Tools.Ele.Datas;

namespace Strawberry.Protocol.Tools.Ele
{
    public abstract class EleGraphicalData : INotifyPropertyChanged
    {
        public EleGraphicalData(EleInstance instance, int id)
        {
            Instance = instance;
            Id = id;
        }

        public int Id { get; set; }

        public EleInstance Instance { get; set; }

        public abstract EleGraphicalElementTypes Type { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public static EleGraphicalData ReadFromStream(EleInstance instance, int id,  BigEndianReader reader)
        {
            var type = (EleGraphicalElementTypes) reader.ReadByte();

            switch (type)
            {
                case EleGraphicalElementTypes.ANIMATED:
                    return AnimatedGraphicalElementData.ReadFromStream(instance, id, reader);
                case EleGraphicalElementTypes.BLENDED:
                    return BlendedGraphicalElementData.ReadFromStream(instance, id, reader);
                case EleGraphicalElementTypes.BOUNDING_BOX:
                    return BoundingBoxGraphicalElementData.ReadFromStream(instance, id, reader);
                case EleGraphicalElementTypes.ENTITY:
                    return EntityGraphicalElementData.ReadFromStream(instance, id, reader);
                case EleGraphicalElementTypes.NORMAL:
                    return NormalGraphicalElementData.ReadFromStream(instance, id, reader);
                case EleGraphicalElementTypes.PARTICLES:
                    return ParticlesGraphicalElementData.ReadFromStream(instance, id, reader);
                default:
                    throw new Exception("Unknown graphical data of type " + type);
            }
        }
    }
}