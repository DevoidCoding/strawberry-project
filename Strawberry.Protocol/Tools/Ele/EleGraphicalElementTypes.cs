﻿// <copyright file="EleGraphicalElementTypes.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:46</date>

namespace Strawberry.Protocol.Tools.Ele
{
    public enum EleGraphicalElementTypes
    {
        NORMAL = 0,
        BOUNDING_BOX = 1,
        ANIMATED = 2,
        ENTITY = 3,
        PARTICLES = 4,
        BLENDED = 5
    }
}