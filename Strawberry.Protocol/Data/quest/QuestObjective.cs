

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("QuestObjectives")]
    public class QuestObjective : IDataObject
    {
        public const String MODULE = "QuestObjectives";
        public uint Id;
        public uint StepId;
        public uint TypeId;
        public int DialogId;
        public QuestObjectiveParameters Parameters;
        public Point Coords;
        public double MapId;
        public QuestObjectiveType Type;
    }
}