

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("QuestObjectiveParameters")]
    public class QuestObjectiveParameters : IDataObject
    {
        public uint NumParams;
        public int Parameter0;
        public int Parameter1;
        public int Parameter2;
        public int Parameter3;
        public int Parameter4;
        public Boolean DungeonOnly;
    }
}