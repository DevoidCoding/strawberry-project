

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("QuestStepRewards")]
    public class QuestStepRewards : IDataObject
    {
        public const String MODULE = "QuestStepRewards";
        public uint Id;
        public uint StepId;
        public int LevelMin;
        public int LevelMax;
        public double KamasRatio;
        public double ExperienceRatio;
        public Boolean KamasScaleWithPlayerLevel;
        public List<List<uint>> ItemsReward;
        public List<uint> EmotesReward;
        public List<uint> JobsReward;
        public List<uint> SpellsReward;
    }
}