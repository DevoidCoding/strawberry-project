

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Achievements")]
    public class Achievement : IDataObject
    {
        public const String MODULE = "Achievements";
        public uint Id;
        public uint NameId;
        public uint CategoryId;
        public uint DescriptionId;
        public int IconId;
        public uint Points;
        public uint Level;
        public uint Order;
        public Boolean AccountLinked;
        public List<int> ObjectiveIds;
        public List<int> RewardIds;
    }
}