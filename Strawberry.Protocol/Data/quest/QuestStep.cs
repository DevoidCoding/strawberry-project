

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("QuestSteps")]
    public class QuestStep : IDataObject
    {
        public const String MODULE = "QuestSteps";
        public uint Id;
        public uint QuestId;
        public uint NameId;
        public uint DescriptionId;
        public int DialogId;
        public uint OptimalLevel;
        public double Duration;
        public List<uint> ObjectiveIds;
        public List<uint> RewardsIds;
    }
}