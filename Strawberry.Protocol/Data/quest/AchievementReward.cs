

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("AchievementRewards")]
    public class AchievementReward : IDataObject
    {
        public const String MODULE = "AchievementRewards";
        public uint Id;
        public uint AchievementId;
        public String Criteria;
        public double KamasRatio;
        public double ExperienceRatio;
        public Boolean KamasScaleWithPlayerLevel;
        public List<uint> ItemsReward;
        public List<uint> ItemsQuantityReward;
        public List<uint> EmotesReward;
        public List<uint> SpellsReward;
        public List<uint> TitlesReward;
        public List<uint> OrnamentsReward;
    }
}