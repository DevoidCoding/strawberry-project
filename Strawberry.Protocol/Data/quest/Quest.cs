

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Quests")]
    public class Quest : IDataObject
    {
        public const String MODULE = "Quests";
        public uint Id;
        public uint NameId;
        public List<uint> StepIds;
        public uint CategoryId;
        public uint RepeatType;
        public uint RepeatLimit;
        public Boolean IsDungeonQuest;
        public uint LevelMin;
        public uint LevelMax;
        public Boolean IsPartyQuest;
        public String StartCriterion;
    }
}