

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SpellStates")]
    public class SpellState : IDataObject
    {
        public const String MODULE = "SpellStates";
        public int Id;
        public uint NameId;
        public Boolean PreventsSpellCast;
        public Boolean PreventsFight;
        public Boolean IsSilent;
        public Boolean CantDealDamage;
        public Boolean Invulnerable;
        public Boolean Incurable;
        public Boolean CantBeMoved;
        public Boolean CantBePushed;
        public Boolean CantSwitchPosition;
        public List<int> EffectsIds;
        public String Icon = "";
        public int IconVisibilityMask;
        public Boolean InvulnerableMelee;
        public Boolean InvulnerableRange;
        public Boolean CantTackle;
        public Boolean CantBeTackled;
    }
}