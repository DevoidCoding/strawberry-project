

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Spells")]
    public class Spell : IDataObject
    {
        public const String MODULE = "Spells";
        public int Id;
        public uint NameId;
        public uint DescriptionId;
        public uint TypeId;
        public uint Order;
        public String ScriptParams;
        public String ScriptParamsCritical;
        public int ScriptId;
        public int ScriptIdCritical;
        public int IconId;
        public List<uint> SpellLevels;
        public Boolean UseParamCache = true;
        public Boolean VerboseCast;
        public Boolean UseSpellLevelScaling;
        public String DefaultZone;
    }
}