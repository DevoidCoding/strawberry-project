

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SpellPairs")]
    public class SpellPair : IDataObject
    {
        public const String MODULE = "SpellPairs";
        public int Id;
        public uint NameId;
        public uint DescriptionId;
        public int IconId;
    }
}