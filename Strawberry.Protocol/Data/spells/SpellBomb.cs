

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SpellBombs")]
    public class SpellBomb : IDataObject
    {
        public const String MODULE = "SpellBombs";
        public int Id;
        public int ChainReactionSpellId;
        public int ExplodSpellId;
        public int WallId;
        public int InstantSpellId;
        public int ComboCoeff;
    }
}