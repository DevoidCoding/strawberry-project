

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("CensoredWords")]
    public class CensoredWord : IDataObject
    {
        public const String MODULE = "CensoredWords";
        public uint Id;
        public uint ListId;
        public String Language;
        public String Word;
        public Boolean DeepLooking;
    }
}