

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ChatChannels")]
    public class ChatChannel : IDataObject
    {
        public const String MODULE = "ChatChannels";
        public uint Id;
        public uint NameId;
        public uint DescriptionId;
        public String Shortcut;
        public String ShortcutKey;
        public Boolean IsPrivate;
        public Boolean AllowObjects;
    }
}