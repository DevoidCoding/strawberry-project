

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Smileys")]
    public class Smiley : IDataObject
    {
        public const String MODULE = "Smileys";
        public uint Id;
        public uint Order;
        public String GfxId;
        public Boolean ForPlayers;
        public List<String> Triggers;
        public uint ReferenceId;
        public uint CategoryId;
    }
}