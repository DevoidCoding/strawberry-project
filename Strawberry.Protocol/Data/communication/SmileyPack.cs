

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SmileyPacks")]
    public class SmileyPack : IDataObject
    {
        public const String MODULE = "SmileyPacks";
        public uint Id;
        public uint NameId;
        public uint Order;
        public List<uint> Smileys;
    }
}