

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Emoticons")]
    public class Emoticon : IDataObject
    {
        public const String MODULE = "Emoticons";
        public uint Id;
        public uint NameId;
        public uint ShortcutId;
        public uint Order;
        public String DefaultAnim;
        public Boolean Persistancy;
        public Boolean EightDirections;
        public Boolean Aura;
        public List<String> Anims;
        public uint Cooldown = 1000;
        public uint Duration = 0;
        public uint Weight;
        public uint SpellLevelId = 0;
    }
}