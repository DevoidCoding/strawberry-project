

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Houses")]
    public class House : IDataObject
    {
        public const String MODULE = "Houses";
        public int TypeId;
        public uint DefaultPrice;
        public int NameId;
        public int DescriptionId;
        public int GfxId;
    }
}