

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("HavenbagFurnitures")]
    public class HavenbagFurniture : IDataObject
    {
        public const String MODULE = "HavenbagFurnitures";
        public int TypeId;
        public int ThemeId;
        public int ElementId;
        public int Color;
        public int SkillId;
        public int LayerId;
        public Boolean BlocksMovement;
        public Boolean IsStackable;
        public uint CellsWidth;
        public uint CellsHeight;
        public uint Order;
    }
}