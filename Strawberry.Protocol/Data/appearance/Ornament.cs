

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Ornaments")]
    public class Ornament : IDataObject
    {
        public const String MODULE = "Ornaments";
        public int Id;
        public uint NameId;
        public Boolean Visible;
        public int AssetId;
        public int IconId;
        public int Rarity;
        public int Order;
    }
}