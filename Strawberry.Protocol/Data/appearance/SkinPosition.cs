

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SkinPositions")]
    public class SkinPosition : IDataObject
    {
        private const String MODULE = "SkinPositions";
        public uint Id;
        public List<TransformData> Transformation;
        public List<String> Clip;
        public List<uint> Skin;
    }
}