

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("CreatureBonesTypes")]
    public class CreatureBoneType : IDataObject
    {
        public const String MODULE = "CreatureBonesTypes";
        public int Id;
        public int CreatureBoneId;
    }
}