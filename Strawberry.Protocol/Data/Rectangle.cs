// <copyright file="Rectangle.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:44</date>

using System;

namespace Strawberry.Protocol.Data
{
    [Serializable]
    public class Rectangle : IDataObject
    {
        public int Bottom;
        public Point BottomRight;
        public int Height;
        public int Left;
        public int Right;
        public Point Size;
        public int Top;
        public Point TopLeft;
        public int Width;
        public int X;
        public int Y;
    }
}