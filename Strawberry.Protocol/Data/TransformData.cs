﻿// <copyright file="TransformData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:44</date>

namespace Strawberry.Protocol.Data
{
    public class TransformData : IDataObject
    {
        public string OriginalClip;
        public string OverrideClip;
        public double Rotation = 0.0;
        public double ScaleX = 1.0;
        public double ScaleY = 1.0;
        public double X = 0.0;
        public double Y = 0.0;
    }
}