

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Pets")]
    public class Pet : IDataObject
    {
        public const String MODULE = "Pets";
        public int Id;
        public List<int> FoodItems;
        public List<int> FoodTypes;
        public int MinDurationBeforeMeal;
        public int MaxDurationBeforeMeal;
        public List<EffectInstance> PossibleEffects;
    }
}