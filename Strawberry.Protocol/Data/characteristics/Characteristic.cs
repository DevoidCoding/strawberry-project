

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Characteristics")]
    public class Characteristic : IDataObject
    {
        public const String MODULE = "Characteristics";
        public int Id;
        public String Keyword;
        public uint NameId;
        public String Asset;
        public int CategoryId;
        public Boolean Visible;
        public int Order;
        public Boolean Upgradable;
    }
}