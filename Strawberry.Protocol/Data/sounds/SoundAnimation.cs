

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SoundAnimations")]
    public class SoundAnimation : IDataObject
    {
        public String MODULE = "SoundAnimations";
        public uint Id;
        public String Name;
        public String Label;
        public String Filename;
        public int Volume;
        public int Rolloff;
        public int AutomationDuration;
        public int AutomationVolume;
        public int AutomationFadeIn;
        public int AutomationFadeOut;
        public Boolean NoCutSilence;
        public uint StartFrame;
    }
}