

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SoundUi")]
    public class SoundUi : IDataObject
    {
        public String MODULE = "SoundUi";
        public uint Id;
        public String UiName;
        public String OpenFile;
        public String CloseFile;
        public List<SoundUiElement> SubElements;
    }
}