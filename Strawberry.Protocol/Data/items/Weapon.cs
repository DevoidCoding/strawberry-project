

// Generated on 02/12/2018 03:57:01
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Weapon")]
    public class Weapon : Item
    {
        public int ApCost;
        public int MinRange;
        public int Range;
        public uint MaxCastPerTurn;
        public Boolean CastInLine;
        public Boolean CastInDiagonal;
        public Boolean CastTestLos;
        public int CriticalHitProbability;
        public int CriticalHitBonus;
        public int CriticalFailureProbability;
    }
}