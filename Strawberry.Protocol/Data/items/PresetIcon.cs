

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("PresetIcons")]
    public class PresetIcon : IDataObject
    {
        public const String MODULE = "PresetIcons";
        public int Id;
        public int Order;
    }
}