

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Items")]
    public class Item : IDataObject
    {
        public const String MODULE = "Items";
        public const uint EQUIPEMENTCATEGORY = 0;
        public const uint CONSUMABLESCATEGORY = 1;
        public const uint RESSOURCESCATEGORY = 2;
        public const uint QUESTCATEGORY = 3;
        public const uint OTHERCATEGORY = 4;
        public const int MAXJOBLEVELGAP = 100;
        public int Id;
        public uint NameId;
        public uint TypeId;
        public uint DescriptionId;
        public int IconId;
        public uint Level;
        public uint RealWeight;
        public Boolean Cursed;
        public int UseAnimationId;
        public Boolean Usable;
        public Boolean Targetable;
        public Boolean Exchangeable;
        public double Price;
        public Boolean TwoHanded;
        public Boolean Etheral;
        public int ItemSetId;
        public String Criteria;
        public String CriteriaTarget;
        public Boolean HideEffects;
        public Boolean Enhanceable;
        public Boolean NonUsableOnAnother;
        public uint AppearanceId;
        public Boolean SecretRecipe;
        public List<uint> DropMonsterIds;
        public uint RecipeSlots;
        public List<uint> RecipeIds;
        public Boolean BonusIsSecret;
        public List<EffectInstance> PossibleEffects;
        public List<uint> FavoriteSubAreas;
        public uint FavoriteSubAreasBonus;
        public int CraftXpRatio;
        public Boolean NeedUseConfirm;
        public Boolean IsDestructible;
        public List<List<double>> NuggetsBySubarea;
        public List<uint> ContainerIds;
        public List<List<int>> ResourcesBySubarea;
        public ItemType Type;
        public uint Weight;
    }
}