

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ItemTypes")]
    public class ItemType : IDataObject
    {
        public const String MODULE = "ItemTypes";
        public int Id;
        public uint NameId;
        public uint SuperTypeId;
        public Boolean Plural;
        public uint Gender;
        public String RawZone;
        public Boolean Mimickable;
        public int CraftXpRatio;
    }
}