

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ExternalNotifications")]
    public class ExternalNotification : IDataObject
    {
        public const String MODULE = "ExternalNotifications";
        public int Id;
        public int CategoryId;
        public int IconId;
        public int ColorId;
        public uint DescriptionId;
        public Boolean DefaultEnable;
        public Boolean DefaultSound;
        public Boolean DefaultNotify;
        public Boolean DefaultMultiAccount;
        public String Name;
        public uint MessageId;
    }
}