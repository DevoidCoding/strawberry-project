

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Tips")]
    public class Tips : IDataObject
    {
        public const String MODULE = "Tips";
        public int Id;
        public uint DescId;
    }
}