

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Subhints")]
    public class Subhints : IDataObject
    {
        public const String MODULE = "Subhints";
        public int HintId;
        public String HintParentUid;
        public String HintAnchoredElement;
        public int HintAnchor;
        public int HintPositionX;
        public int HintPositionY;
        public int HintWidth;
        public int HintHeight;
        public String HintHighlightedElement;
        public String HintTexture;
        public int HintOrder;
        public String HintTooltipText;
        public int HintTooltipPositionEnum;
        public String HintTooltipUrl;
        public int HintTooltipOffsetX;
        public int HintTooltipOffsetY;
    }
}