

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("OptionalFeatures")]
    public class OptionalFeature : IDataObject
    {
        public const String MODULE = "OptionalFeatures";
        public int Id;
        public String Keyword;
    }
}