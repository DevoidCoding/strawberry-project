

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("CensoredContents")]
    public class CensoredContent : IDataObject
    {
        public const String MODULE = "CensoredContents";
        public int Type;
        public int OldValue;
        public int NewValue;
        public String Lang;
    }
}