

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ActionDescriptions")]
    public class ActionDescription : IDataObject
    {
        public const String MODULE = "ActionDescriptions";
        public uint Id;
        public uint TypeId;
        public String Name;
        public uint DescriptionId;
        public Boolean Trusted;
        public Boolean NeedInteraction;
        public uint MaxUsePerFrame;
        public uint MinimalUseInterval;
        public Boolean NeedConfirmation;
    }
}