

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("AlignmentRank")]
    public class AlignmentRank : IDataObject
    {
        public const String MODULE = "AlignmentRank";
        public int Id;
        public uint OrderId;
        public uint NameId;
        public uint DescriptionId;
        public int MinimumAlignment;
        public int ObjectsStolen;
        public List<int> Gifts;
    }
}