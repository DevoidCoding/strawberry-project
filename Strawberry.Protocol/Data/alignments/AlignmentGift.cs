

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("AlignmentGift")]
    public class AlignmentGift : IDataObject
    {
        public const String MODULE = "AlignmentGift";
        public int Id;
        public uint NameId;
        public int EffectId;
        public uint GfxId;
    }
}