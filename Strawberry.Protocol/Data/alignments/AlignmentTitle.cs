

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("AlignmentTitles")]
    public class AlignmentTitle : IDataObject
    {
        public const String MODULE = "AlignmentTitles";
        public int SideId;
        public List<int> NamesId;
        public List<int> ShortsId;
    }
}