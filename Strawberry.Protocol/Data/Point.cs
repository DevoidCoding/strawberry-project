// <copyright file="Point.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:44</date>

using System;

namespace Strawberry.Protocol.Data
{
    [Serializable]
    public class Point : IDataObject
    {
        public double Length;

        public int X;
        public int Y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}