

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Idols")]
    public class Idol : IDataObject
    {
        public const String MODULE = "Idols";
        public int Id;
        public String Description;
        public int CategoryId;
        public int ItemId;
        public Boolean GroupOnly;
        public int SpellPairId;
        public int Score;
        public int ExperienceBonus;
        public int DropBonus;
        public List<int> SynergyIdolsIds;
        public List<double> SynergyIdolsCoeff;
        public List<int> IncompatibleMonsters;
    }
}