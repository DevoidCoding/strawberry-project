

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("EmblemSymbols")]
    public class EmblemSymbol : IDataObject
    {
        public const String MODULE = "EmblemSymbols";
        public int Id;
        public int IconId;
        public int SkinId;
        public int Order;
        public int CategoryId;
        public Boolean Colorizable;
    }
}