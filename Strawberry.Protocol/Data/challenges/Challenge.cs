

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Challenge")]
    public class Challenge : IDataObject
    {
        public const String MODULE = "Challenge";
        public int Id;
        public uint NameId;
        public uint DescriptionId;
        public Boolean DareAvailable;
        public List<uint> IncompatibleChallenges;
    }
}