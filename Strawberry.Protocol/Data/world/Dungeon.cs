

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Dungeons")]
    public class Dungeon : IDataObject
    {
        public const String MODULE = "Dungeons";
        public int Id;
        public uint NameId;
        public int OptimalPlayerLevel;
        public List<double> MapIds;
        public double EntranceMapId;
        public double ExitMapId;
    }
}