

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("WorldMaps")]
    public class WorldMap : IDataObject
    {
        public const String MODULE = "WorldMaps";
        public int Id;
        public uint NameId;
        public int OrigineX;
        public int OrigineY;
        public double MapWidth;
        public double MapHeight;
        public uint HorizontalChunck;
        public uint VerticalChunck;
        public Boolean ViewableEverywhere;
        public double MinScale;
        public double MaxScale;
        public double StartScale;
        public int CenterX;
        public int CenterY;
        public int TotalWidth;
        public int TotalHeight;
        public List<String> Zoom;
    }
}