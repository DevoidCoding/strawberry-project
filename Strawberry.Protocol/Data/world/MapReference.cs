

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MapReferences")]
    public class MapReference : IDataObject
    {
        public const String MODULE = "MapReferences";
        public int Id;
        public double MapId;
        public int CellId;
    }
}