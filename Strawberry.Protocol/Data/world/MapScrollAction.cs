

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MapScrollActions")]
    public class MapScrollAction : IDataObject
    {
        public const String MODULE = "MapScrollActions";
        public double Id;
        public Boolean RightExists;
        public Boolean BottomExists;
        public Boolean LeftExists;
        public Boolean TopExists;
        public double RightMapId;
        public double BottomMapId;
        public double LeftMapId;
        public double TopMapId;
    }
}