

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SubAreas")]
    public class SubArea : IDataObject
    {
        public const String MODULE = "SubAreas";
        public int Id;
        public uint NameId;
        public int AreaId;
        public List<AmbientSound> AmbientSounds;
        public List<List<int>> Playlists;
        public List<double> MapIds;
        public Rectangle Bounds;
        public List<int> Shape;
        public List<uint> CustomWorldMap;
        public int PackId;
        public uint Level;
        public Boolean IsConquestVillage;
        public Boolean BasicAccountAllowed;
        public Boolean DisplayOnWorldMap;
        public Boolean MountAutoTripAllowed;
        public List<uint> Monsters;
        public List<double> EntranceMapIds;
        public List<double> ExitMapIds;
        public Boolean Capturable;
        public List<uint> Achievements;
        public List<List<double>> Quests;
        public List<List<double>> Npcs;
        public int ExploreAchievementId;
        public Boolean IsDiscovered;
        public List<int> Harvestables;
    }
}