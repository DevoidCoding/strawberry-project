

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Hints")]
    public class Hint : IDataObject
    {
        public const String MODULE = "Hints";
        public int Id;
        public uint Gfx;
        public uint NameId;
        public double MapId;
        public double RealMapId;
        public int X;
        public int Y;
        public Boolean Outdoor;
        public int SubareaId;
        public int WorldMapId;
        public uint Level;
        public int CategoryId;
    }
}