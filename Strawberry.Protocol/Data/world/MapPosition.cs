

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MapPositions")]
    public class MapPosition : IDataObject
    {
        public const String MODULE = "MapPositions";
        public double Id;
        public int PosX;
        public int PosY;
        public Boolean Outdoor;
        public int Capabilities;
        public int NameId;
        public Boolean ShowNameOnFingerpost;
        public List<AmbientSound> Sounds;
        public List<List<int>> Playlists;
        public int SubAreaId;
        public int WorldMap;
        public Boolean HasPriorityOnWorldmap;
        public Boolean IsUnderWater;
        public Boolean AllowPrism;
        public Boolean IsTransition;
        public uint TacticalModeTemplateId;
    }
}