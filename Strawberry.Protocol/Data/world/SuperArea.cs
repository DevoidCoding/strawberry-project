

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("SuperAreas")]
    public class SuperArea : IDataObject
    {
        public const String MODULE = "SuperAreas";
        public int Id;
        public uint NameId;
        public uint WorldmapId;
        public Boolean HasWorldMap;
    }
}