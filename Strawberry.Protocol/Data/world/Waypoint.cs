

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Waypoints")]
    public class Waypoint : IDataObject
    {
        public const String MODULE = "Waypoints";
        public uint Id;
        public double MapId;
        public uint SubAreaId;
    }
}