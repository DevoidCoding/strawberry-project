

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Phoenixes")]
    public class Phoenix : IDataObject
    {
        public const String MODULE = "Phoenixes";
        public double MapId;
    }
}