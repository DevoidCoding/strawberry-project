

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Areas")]
    public class Area : IDataObject
    {
        public const String MODULE = "Areas";
        public int Id;
        public uint NameId;
        public int SuperAreaId;
        public Boolean ContainHouses;
        public Boolean ContainPaddocks;
        public Rectangle Bounds;
        public uint WorldmapId;
        public Boolean HasWorldMap;
    }
}