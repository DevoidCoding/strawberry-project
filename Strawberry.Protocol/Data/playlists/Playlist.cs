

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Playlists")]
    public class Playlist : IDataObject
    {
        public const int AMBIENTTYPEROLEPLAY = 1;
        public const int AMBIENTTYPEAMBIENT = 2;
        public const int AMBIENTTYPEFIGHT = 3;
        public const int AMBIENTTYPEBOSS = 4;
        public const String MODULE = "Playlists";
        public int Id;
        public int SilenceDuration;
        public int Iteration;
        public int Type;
        public List<PlaylistSound> Sounds;
    }
}