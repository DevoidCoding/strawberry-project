

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("AmbientSounds")]
    public class AmbientSound : PlaylistSound
    {
        public const int AMBIENTTYPEROLEPLAY = 1;
        public const int AMBIENTTYPEAMBIENT = 2;
        public const int AMBIENTTYPEFIGHT = 3;
        public const int AMBIENTTYPEBOSS = 4;
        public const String MODULE = "AmbientSounds";
        public int CriterionId;
        public uint SilenceMin;
        public uint SilenceMax;
        public int TypeId;
    }
}