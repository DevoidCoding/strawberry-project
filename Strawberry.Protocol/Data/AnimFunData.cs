﻿// <copyright file="AnimFunData.cs" company="Strawberry Project">
//      Copyright (c) Strawberry Project. All rights reserved.
// </copyright>
// <author>Strawberry</author>
// <date>06/23/2016 06:44</date>

namespace Strawberry.Protocol.Data
{
    public class AnimFunData : IDataObject
    {
        public string AnimName;
        public int AnimWeight;
    }
}