

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Interactives")]
    public class Interactive : IDataObject
    {
        public const String MODULE = "Interactives";
        public int Id;
        public uint NameId;
        public int ActionId;
        public Boolean DisplayTooltip;
    }
}