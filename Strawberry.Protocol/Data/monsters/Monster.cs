

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Monsters")]
    public class Monster : IDataObject
    {
        public const String MODULE = "Monsters";
        public int Id;
        public uint NameId;
        public uint GfxId;
        public int Race;
        public List<MonsterGrade> Grades;
        public String Look;
        public Boolean UseSummonSlot;
        public Boolean UseBombSlot;
        public Boolean CanPlay;
        public Boolean CanTackle;
        public List<AnimFunMonsterData> AnimFunList;
        public Boolean IsBoss;
        public List<MonsterDrop> Drops;
        public List<uint> Subareas;
        public List<uint> Spells;
        public int FavoriteSubareaId;
        public Boolean IsMiniBoss;
        public Boolean IsQuestMonster;
        public uint CorrespondingMiniBossId;
        public double SpeedAdjust = 0.0;
        public int CreatureBoneId;
        public Boolean CanBePushed;
        public Boolean FastAnimsFun;
        public Boolean CanSwitchPos;
        public List<uint> IncompatibleIdols;
        public Boolean AllIdolsDisabled;
        public Boolean DareAvailable;
        public List<uint> IncompatibleChallenges;
        public Boolean UseRaceValues;
        public int AggressiveZoneSize;
        public int AggressiveLevelDiff;
        public String AggressiveImmunityCriterion;
        public int AggressiveAttackDelay;
    }
}