

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("CompanionCharacteristics")]
    public class CompanionCharacteristic : IDataObject
    {
        public const String MODULE = "CompanionCharacteristics";
        public int Id;
        public int CaracId;
        public int CompanionId;
        public int Order;
        public List<List<double>> StatPerLevelRange;
    }
}