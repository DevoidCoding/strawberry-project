

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MonsterSuperRaces")]
    public class MonsterSuperRace : IDataObject
    {
        public const String MODULE = "MonsterSuperRaces";
        public int Id;
        public uint NameId;
    }
}