

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Companions")]
    public class Companion : IDataObject
    {
        public const String MODULE = "Companions";
        public int Id;
        public uint NameId;
        public String Look;
        public Boolean WebDisplay;
        public uint DescriptionId;
        public uint StartingSpellLevelId;
        public uint AssetId;
        public List<uint> Characteristics;
        public List<uint> Spells;
        public int CreatureBoneId;
    }
}