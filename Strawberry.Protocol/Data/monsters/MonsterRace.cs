

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MonsterRaces")]
    public class MonsterRace : IDataObject
    {
        public const String MODULE = "MonsterRaces";
        public int Id;
        public int SuperRaceId;
        public uint NameId;
        public List<uint> Monsters;
        public int AggressiveZoneSize;
        public int AggressiveLevelDiff;
        public String AggressiveImmunityCriterion;
        public int AggressiveAttackDelay;
    }
}