

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MonsterGrade")]
    public class MonsterGrade : IDataObject
    {
        public uint Grade;
        public int MonsterId;
        public uint Level;
        public int Vitality;
        public int PaDodge;
        public int PmDodge;
        public int Wisdom;
        public int EarthResistance;
        public int AirResistance;
        public int FireResistance;
        public int WaterResistance;
        public int NeutralResistance;
        public int GradeXp;
        public int LifePoints;
        public int ActionPoints;
        public int MovementPoints;
        public int DamageReflect;
        public uint HiddenLevel;
    }
}