

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MonsterDrop")]
    public class MonsterDrop : IDataObject
    {
        public uint DropId;
        public int MonsterId;
        public int ObjectId;
        public double PercentDropForGrade1;
        public double PercentDropForGrade2;
        public double PercentDropForGrade3;
        public double PercentDropForGrade4;
        public double PercentDropForGrade5;
        public int Count;
        public Boolean HasCriteria;
    }
}