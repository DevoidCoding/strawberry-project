

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Notifications")]
    public class Notification : IDataObject
    {
        public const String MODULE = "Notifications";
        public int Id;
        public uint TitleId;
        public uint MessageId;
        public int IconId;
        public int TypeId;
        public String Trigger;
    }
}