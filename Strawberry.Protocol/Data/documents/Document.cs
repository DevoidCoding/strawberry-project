

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Documents")]
    public class Document : IDataObject
    {
        private const String MODULE = "Documents";
        public int Id;
        public uint TypeId;
        public Boolean ShowTitle;
        public Boolean ShowBackgroundImage;
        public uint TitleId;
        public uint AuthorId;
        public uint SubTitleId;
        public uint ContentId;
        public String ContentCSS;
        public String ClientProperties;
    }
}