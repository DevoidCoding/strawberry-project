

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Heads")]
    public class Head : IDataObject
    {
        public const String MODULE = "Heads";
        public int Id;
        public String Skins;
        public String AssetId;
        public uint Breed;
        public uint Gender;
        public String Label;
        public uint Order;
    }
}