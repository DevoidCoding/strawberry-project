

// Generated on 02/12/2018 03:56:59
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("BreedRoles")]
    public class BreedRole : IDataObject
    {
        public const String MODULE = "BreedRoles";
        public int Id;
        public uint NameId;
        public uint DescriptionId;
        public int AssetId;
        public int Color;
    }
}