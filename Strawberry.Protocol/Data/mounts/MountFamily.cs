

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("MountFamily")]
    public class MountFamily : IDataObject
    {
        private String MODULE = "MountFamily";
        public uint Id;
        public uint NameId;
        public String HeadUri;
    }
}