

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("DareCriterias")]
    public class DareCriteria : IDataObject
    {
        public const String MODULE = "DareCriterias";
        public uint Id;
        public uint NameId;
        public uint MaxOccurence;
        public uint MaxParameters;
        public int MinParameterBound;
        public int MaxParameterBound;
    }
}