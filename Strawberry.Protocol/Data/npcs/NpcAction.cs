

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("NpcActions")]
    public class NpcAction : IDataObject
    {
        public const String MODULE = "NpcActions";
        public int Id;
        public int RealId;
        public uint NameId;
    }
}