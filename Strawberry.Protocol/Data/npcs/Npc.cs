

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Npcs")]
    public class Npc : IDataObject
    {
        public const String MODULE = "Npcs";
        public int Id;
        public uint NameId;
        public List<List<int>> DialogMessages;
        public List<List<int>> DialogReplies;
        public List<uint> Actions;
        public uint Gender;
        public String Look;
        public int TokenShop;
        public List<AnimFunNpcData> AnimFunList;
        public Boolean FastAnimsFun;
    }
}