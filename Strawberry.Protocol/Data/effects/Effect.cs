

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Effects")]
    public class Effect : IDataObject
    {
        public const String MODULE = "Effects";
        public int Id;
        public uint DescriptionId;
        public int IconId;
        public int Characteristic;
        public uint Category;
        public String Operator;
        public Boolean ShowInTooltip;
        public Boolean UseDice;
        public Boolean ForceMinMax;
        public Boolean Boost;
        public Boolean Active;
        public int OppositeId;
        public uint TheoreticalDescriptionId;
        public uint TheoreticalPattern;
        public Boolean ShowInSet;
        public int BonusType;
        public Boolean UseInFight;
        public uint EffectPriority;
        public int ElementId;
    }
}