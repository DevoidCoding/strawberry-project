

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("EffectInstance")]
    public class EffectInstance : IDataObject
    {
        public uint EffectUid;
        public uint EffectId;
        public int TargetId;
        public String TargetMask;
        public int Duration;
        public int Delay;
        public int Random;
        public int Group;
        public int Modificator;
        public Boolean Trigger;
        public String Triggers;
        public Boolean VisibleInTooltip = true;
        public Boolean VisibleInBuffUi = true;
        public Boolean VisibleInFightLog = true;
        public Object ZoneSize;
        public uint ZoneShape;
        public Object ZoneMinSize;
        public Object ZoneEfficiencyPercent;
        public Object ZoneMaxEfficiency;
        public Object ZoneStopAtTarget;
        public int EffectElement;
        public String RawZone;
    }
}