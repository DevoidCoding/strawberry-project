

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("EffectInstanceDuration")]
    public class EffectInstanceDuration : EffectInstance
    {
        public uint Days;
        public uint Hours;
        public uint Minutes;
    }
}