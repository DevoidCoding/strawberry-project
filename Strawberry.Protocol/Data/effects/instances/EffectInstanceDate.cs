

// Generated on 02/12/2018 03:57:00
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("EffectInstanceDate")]
    public class EffectInstanceDate : EffectInstance
    {
        public uint Year;
        public uint Month;
        public uint Day;
        public uint Hour;
        public uint Minute;
    }
}