

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Skills")]
    public class Skill : IDataObject
    {
        public const String MODULE = "Skills";
        public int Id;
        public uint NameId;
        public int ParentJobId;
        public Boolean IsForgemagus;
        public List<int> ModifiableItemTypeIds;
        public int GatheredRessourceItem;
        public List<int> CraftableItemIds;
        public int InteractiveId;
        public String UseAnimation;
        public int Cursor;
        public int ElementActionId;
        public Boolean AvailableInHouse;
        public uint LevelMin;
        public Boolean ClientDisplay;
    }
}