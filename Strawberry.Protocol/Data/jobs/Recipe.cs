

// Generated on 02/12/2018 03:57:02
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Recipes")]
    public class Recipe : IDataObject
    {
        public const String MODULE = "Recipes";
        public int ResultId;
        public uint ResultNameId;
        public uint ResultTypeId;
        public uint ResultLevel;
        public List<int> IngredientIds;
        public List<uint> Quantities;
        public int JobId;
        public int SkillId;
    }
}