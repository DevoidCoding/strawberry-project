

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("Servers")]
    public class Server : IDataObject
    {
        public const String MODULE = "Servers";
        public int Id;
        public uint NameId;
        public uint CommentId;
        public double OpeningDate;
        public String Language;
        public int PopulationId;
        public uint GameTypeId;
        public int CommunityId;
        public List<String> RestrictedToLanguages;
    }
}