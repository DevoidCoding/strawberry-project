

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ServerGameTypes")]
    public class ServerGameType : IDataObject
    {
        public const String MODULE = "ServerGameTypes";
        public int Id;
        public Boolean SelectableByPlayer;
        public uint NameId;
        public uint RulesId;
        public uint DescriptionId;
    }
}