

// Generated on 02/12/2018 03:57:03
using System;
using System.Collections.Generic;
using Strawberry.Protocol.Tools;
using Strawberry.Protocol.Tools.D2O;

namespace Strawberry.Protocol.Data
{
    [D2OClass("ServerCommunities")]
    public class ServerCommunity : IDataObject
    {
        public const String MODULE = "ServerCommunities";
        public int Id;
        public uint NameId;
        public String ShortId;
        public List<String> DefaultCountries;
        public List<int> SupportedLangIds;
        public int NamingRulePlayerNameId;
        public int NamingRuleGuildNameId;
        public int NamingRuleAllianceNameId;
        public int NamingRuleAllianceTagId;
        public int NamingRulePartyNameId;
        public int NamingRuleMountNameId;
        public int NamingRuleNameGeneratorId;
        public int NamingRuleAdminId;
        public int NamingRuleModoId;
    }
}