﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strawberry.Protocol.Enums;

namespace Strawberry.Protocol
{
    public static class BuildInfo
    {
        public static Version Version = new Version(2, 41, 0);
        public static BuildTypeEnum Type = BuildTypeEnum.Release;
        public static int Revision = 118597;
        public static int Patch = 0;
        public static DateTime Date = new DateTime(2017, 2, 21, 18, 16, 49);

        public new static string ToString()
        {
            return $"{Version}.{Revision}";
        }
    }
}
