

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TextInformationTypeEnum
    {
        TextInformationMessage = 0,
        TextInformationError = 1,
        TextInformationPvp = 2,
        TextInformationFightLog = 3,
        TextInformationPopup = 4,
        TextLivingObject = 5,
        TextEntityTalk = 6,
        TextInformationFight = 7,
    }
}