

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum AccessoryPreviewErrorEnum
    {
        PreviewError = 0,
        PreviewCooldown = 1,
        PreviewBadItem = 2,
    }
}