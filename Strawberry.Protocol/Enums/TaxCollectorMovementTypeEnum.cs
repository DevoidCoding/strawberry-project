

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TaxCollectorMovementTypeEnum
    {
        TaxCollectorUnknownAction = 0,
        TaxCollectorHired = 1,
        TaxCollectorHarvested = 2,
        TaxCollectorDefeated = 3,
        TaxCollectorDestroyed = 4,
    }
}