

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ExchangeTypeEnum
    {
        NpcShop = 0,
        PlayerTrade = 1,
        NpcTrade = 2,
        Craft = 3,
        DisconnectedVendor = 4,
        Storage = 5,
        ShopStock = 6,
        Taxcollector = 8,
        NpcModifyTrade = 9,
        BidhouseSell = 10,
        BidhouseBuy = 11,
        MulticraftCrafter = 12,
        MulticraftCustomer = 13,
        JobIndex = 14,
        Mount = 15,
        MountStable = 16,
        NpcResurectPet = 17,
        NpcTradeDragoturkey = 18,
        RealestateHouse = 19,
        RealestateFarm = 20,
        RunesTrade = 21,
        RecycleTrade = 22,
        Bank = 23,
        Trashbin = 24,
        AlliancePrism = 25,
        Havenbag = 26,
        NpcTradeSeemyool = 27,
    }
}