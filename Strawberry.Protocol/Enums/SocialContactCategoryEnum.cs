

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum SocialContactCategoryEnum
    {
        SocialContactFriend = 0,
        SocialContactSpouse = 1,
        SocialContactParty = 2,
        SocialContactGuild = 3,
        SocialContactAlliance = 4,
        SocialContactCrafter = 5,
        SocialContactInterlocutor = 6,
        SocialContactFight = 7,
    }
}