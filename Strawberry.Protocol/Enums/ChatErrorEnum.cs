

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ChatErrorEnum
    {
        ChatErrorUnknown = 0,
        ChatErrorReceiverNotFound = 1,
        ChatErrorInteriorMonologue = 2,
        ChatErrorNoGuild = 3,
        ChatErrorNoParty = 4,
        ChatErrorAlliance = 5,
        ChatErrorInvalidMap = 6,
        ChatErrorNoPartyArena = 7,
        ChatErrorNoTeam = 8,
        ChatErrorMalformedContent = 9,
        ChatErrorNoChannelCommunity = 10,
    }
}