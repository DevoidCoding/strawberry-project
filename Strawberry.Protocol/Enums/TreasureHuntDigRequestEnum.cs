

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TreasureHuntDigRequestEnum
    {
        TreasureHuntDigErrorUndefined = 0,
        TreasureHuntDigNewHint = 1,
        TreasureHuntDigFinished = 2,
        TreasureHuntDigWrong = 3,
        TreasureHuntDigLost = 4,
        TreasureHuntDigErrorImpossible = 5,
        TreasureHuntDigWrongAndYouKnowIt = 6,
    }
}