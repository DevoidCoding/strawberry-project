

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GameServerTypeEnum
    {
        ServerTypeUndefined = -1,
        ServerTypeClassical = 0,
        ServerTypeHardcore = 1,
        ServerTypeKolizeum = 2,
        ServerTypeTournament = 3,
        ServerTypeEpic = 4,
    }
}