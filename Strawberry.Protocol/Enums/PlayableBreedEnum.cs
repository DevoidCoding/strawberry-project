

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PlayableBreedEnum
    {
        Undefined = 0,
        Feca = 1,
        Osamodas = 2,
        Enutrof = 3,
        Sram = 4,
        Xelor = 5,
        Ecaflip = 6,
        Eniripsa = 7,
        Iop = 8,
        Cra = 9,
        Sadida = 10,
        Sacrieur = 11,
        Pandawa = 12,
        Roublard = 13,
        Zobal = 14,
        Steamer = 15,
        Eliotrope = 16,
        Huppermage = 17,
        Ouginak = 18,
    }
}