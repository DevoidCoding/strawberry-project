

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TreasureHuntTypeEnum
    {
        TreasureHuntClassic = 0,
        TreasureHuntPortal = 1,
        TreasureHuntLegendary = 2,
    }
}