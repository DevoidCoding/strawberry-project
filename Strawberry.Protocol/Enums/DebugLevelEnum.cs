

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum DebugLevelEnum
    {
        LevelTrace = 0,
        LevelDebug = 1,
        LevelInfo = 2,
        LevelWarn = 3,
        LevelError = 4,
        LevelFatal = 5,
    }
}