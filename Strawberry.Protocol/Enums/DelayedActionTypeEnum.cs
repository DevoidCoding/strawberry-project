

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum DelayedActionTypeEnum
    {
        DelayedActionDisconnect = 0,
        DelayedActionObjectUse = 1,
        DelayedActionJoinCharacter = 2,
        DelayedActionAggressionImmune = 3,
    }
}