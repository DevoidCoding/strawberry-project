

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum AllianceRightsBitEnum
    {
        AllianceRightNone = 0,
        AllianceRightBoss = 1,
        AllianceRightManagePrisms = 2,
        AllianceRightTalkInChan = 4,
        AllianceRightRecruitGuilds = 8,
        AllianceRightKickGuilds = 16,
        AllianceRightManageRights = 32,
    }
}