

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ServerStatusEnum
    {
        StatusUnknown = 0,
        Offline = 1,
        Starting = 2,
        Online = 3,
        Nojoin = 4,
        Saving = 5,
        Stoping = 6,
        Full = 7,
    }
}