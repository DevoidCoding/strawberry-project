

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum CompassTypeEnum
    {
        CompassTypeSimple = 0,
        CompassTypeSpouse = 1,
        CompassTypeParty = 2,
        CompassTypePvpSeek = 3,
        CompassTypeQuest = 4,
    }
}