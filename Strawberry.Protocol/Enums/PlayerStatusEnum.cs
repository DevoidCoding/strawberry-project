

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PlayerStatusEnum
    {
        PlayerStatusOffline = 0,
        PlayerStatusUnknown = 1,
        PlayerStatusAvailable = 10,
        PlayerStatusIdle = 20,
        PlayerStatusAfk = 21,
        PlayerStatusPrivate = 30,
        PlayerStatusSolo = 40,
    }
}