

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum CharacterRemodelingEnum
    {
        CharacterRemodelingNotApplicable = 0,
        CharacterRemodelingName = 1,
        CharacterRemodelingColors = 2,
        CharacterRemodelingCosmetic = 4,
        CharacterRemodelingBreed = 8,
        CharacterRemodelingGender = 16,
        CharacterOptRemodelingName = 32,
    }
}