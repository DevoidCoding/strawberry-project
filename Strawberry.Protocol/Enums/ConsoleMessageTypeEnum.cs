

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ConsoleMessageTypeEnum
    {
        ConsoleTextMessage = 0,
        ConsoleInfoMessage = 1,
        ConsoleErrMessage = 2,
    }
}