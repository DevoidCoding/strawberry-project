

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TreasureHuntFlagRequestEnum
    {
        TreasureHuntFlagErrorUndefined = 0,
        TreasureHuntFlagOk = 1,
        TreasureHuntFlagWrong = 2,
        TreasureHuntFlagTooMany = 3,
        TreasureHuntFlagErrorImpossible = 4,
        TreasureHuntFlagWrongIndex = 5,
        TreasureHuntFlagSameMap = 6,
    }
}