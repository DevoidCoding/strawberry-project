

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ListAddFailureEnum
    {
        ListAddFailureUnknown = 0,
        ListAddFailureOverQuota = 1,
        ListAddFailureNotFound = 2,
        ListAddFailureEgocentric = 3,
        ListAddFailureIsDouble = 4,
    }
}