

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PresetUseResultEnum
    {
        PresetUseOk = 1,
        PresetUseOkPartial = 2,
        PresetUseErrUnknown = 3,
        PresetUseErrCriterion = 4,
        PresetUseErrBadPresetId = 5,
        PresetUseErrCooldown = 6,
    }
}