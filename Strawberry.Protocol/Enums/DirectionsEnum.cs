

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum DirectionsEnum
    {
        DirectionEast = 0,
        DirectionSouthEast = 1,
        DirectionSouth = 2,
        DirectionSouthWest = 3,
        DirectionWest = 4,
        DirectionNorthWest = 5,
        DirectionNorth = 6,
        DirectionNorthEast = 7,
    }
}