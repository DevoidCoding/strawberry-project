

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PartyNameErrorEnum
    {
        PartyNameUndefinedError = 0,
        PartyNameInvalid = 1,
        PartyNameAlreadyUsed = 2,
        PartyNameUnallowedRights = 3,
        PartyNameUnallowedNow = 4,
    }
}