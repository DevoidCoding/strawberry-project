

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum FightDispellableEnum
    {
        Dispellable = 1,
        DispellableByDeath = 2,
        DispellableByStrongDispel = 3,
        ReallyNotDispellable = 4,
    }
}