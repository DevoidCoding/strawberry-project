

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum SubEntityBindingPointCategoryEnum
    {
        HookPointCategoryUnused = 0,
        HookPointCategoryPet = 1,
        HookPointCategoryMountDriver = 2,
        HookPointCategoryLiftedEntity = 3,
        HookPointCategoryBaseBackground = 4,
        HookPointCategoryMerchantBag = 5,
        HookPointCategoryBaseForeground = 6,
        HookPointCategoryPetFollower = 7,
        HookPointCategoryUnderwaterBubbles = 8,
    }
}