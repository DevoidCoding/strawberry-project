

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ServerCompletionEnum
    {
        CompletionRecomandated = 0,
        CompletionAverage = 1,
        CompletionHigh = 2,
        CompletionComingSoon = 3,
        CompletionFull = 4,
    }
}