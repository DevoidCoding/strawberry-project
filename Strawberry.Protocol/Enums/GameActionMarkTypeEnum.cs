

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GameActionMarkTypeEnum
    {
        Glyph = 1,
        Trap = 2,
        Wall = 3,
        Portal = 4,
        Rune = 5,
    }
}