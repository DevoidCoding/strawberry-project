

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PrismStateEnum
    {
        PrismStateInvulnerable = 0,
        PrismStateNormal = 1,
        PrismStateAttacked = 2,
        PrismStateFighting = 3,
        PrismStateWeakened = 4,
        PrismStateVulnerable = 5,
        PrismStateDefeated = 6,
        PrismStateSabotaged = 7,
    }
}