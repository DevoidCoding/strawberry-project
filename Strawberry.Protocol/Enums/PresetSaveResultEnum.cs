

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PresetSaveResultEnum
    {
        PresetSaveOk = 1,
        PresetSaveErrUnknown = 2,
        PresetSaveErrTooMany = 3,
    }
}