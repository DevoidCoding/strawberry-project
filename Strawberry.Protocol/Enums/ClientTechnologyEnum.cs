

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ClientTechnologyEnum
    {
        ClientTechnologyUnknown = 0,
        ClientAir = 1,
        ClientFlash = 2,
    }
}