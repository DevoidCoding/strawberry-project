

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GameHierarchyEnum
    {
        Unavailable = -1,
        Player = 0,
        Moderator = 10,
        GamemasterPadawan = 20,
        Gamemaster = 30,
        Admin = 40,
    }
}