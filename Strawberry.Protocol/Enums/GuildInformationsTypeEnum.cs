

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GuildInformationsTypeEnum
    {
        InfoGeneral = 1,
        InfoMembers = 2,
        InfoBoosts = 3,
        InfoPaddocks = 4,
        InfoHouses = 5,
        InfoTaxCollectorGuildOnly = 6,
        InfoTaxCollectorAlliance = 7,
        InfoTaxCollectorLeave = 8,
    }
}