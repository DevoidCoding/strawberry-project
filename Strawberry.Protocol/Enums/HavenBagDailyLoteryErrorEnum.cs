

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum HavenBagDailyLoteryErrorEnum
    {
        HavenbagDailyLoteryOk = 0,
        HavenbagDailyLoteryAlreadyused = 1,
        HavenbagDailyLoteryError = 2,
    }
}