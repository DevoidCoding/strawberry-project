

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PlayerStateEnum
    {
        NotConnected = 0,
        GameTypeRoleplay = 1,
        GameTypeFight = 2,
        UnknownState = 99,
    }
}