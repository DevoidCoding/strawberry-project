

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PartyTypeEnum
    {
        PartyTypeUndefined = 0,
        PartyTypeClassical = 1,
        PartyTypeDungeon = 2,
        PartyTypeArena = 3,
    }
}