

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum UpdatableMountBoostEnum
    {
        Stamina = 3,
        Maturity = 5,
        Energy = 1,
        Serenity = 2,
        Love = 4,
        Tiredness = 6,
        Rideable = 7,
    }
}