

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum NicknameErrorEnum
    {
        AlreadyUsed = 1,
        SameAsLogin = 2,
        TooSimilarToLogin = 3,
        InvalidNick = 4,
        UnknownNickError = 99,
    }
}