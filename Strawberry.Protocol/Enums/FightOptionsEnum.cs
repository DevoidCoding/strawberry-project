

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum FightOptionsEnum
    {
        FightOptionSetSecret = 0,
        FightOptionSetToPartyOnly = 1,
        FightOptionSetClosed = 2,
        FightOptionAskForHelp = 3,
    }
}