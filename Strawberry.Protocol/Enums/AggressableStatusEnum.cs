

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum AggressableStatusEnum
    {
        NonAggressable = 0,
        PvpEnabledAggressable = 10,
        PvpEnabledNonAggressable = 11,
        AvaEnabledAggressable = 20,
        AvaEnabledNonAggressable = 21,
        AvaDisqualified = 22,
        AvaPrequalifiedAggressable = 23,
    }
}