

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TaxCollectorStateEnum
    {
        StateCollecting = 0,
        StateWaitingForHelp = 1,
        StateFighting = 2,
    }
}