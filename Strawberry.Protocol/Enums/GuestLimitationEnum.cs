

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GuestLimitationEnum
    {
        LimitedToRegistered = 0,
        GuestLimitOnJobXp = 1,
        GuestLimitOnJobUse = 2,
        GuestLimitOnMap = 3,
        GuestLimitOnItem = 4,
        GuestLimitOnVendor = 5,
        GuestLimitOnChat = 6,
        GuestLimitOnGuild = 7,
    }
}