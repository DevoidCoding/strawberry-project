

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ExchangeReplayStopReasonEnum
    {
        StoppedReasonOk = 1,
        StoppedReasonUser = 2,
        StoppedReasonMissingRessource = 3,
        StoppedReasonImpossibleModification = 4,
    }
}