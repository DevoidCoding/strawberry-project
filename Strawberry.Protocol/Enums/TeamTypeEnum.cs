

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TeamTypeEnum
    {
        TeamTypePlayer = 0,
        TeamTypeMonster = 1,
        TeamTypeMutant = 2,
        TeamTypeTaxcollector = 3,
        TeamTypeBadPlayer = 4,
        TeamTypePrism = 5,
    }
}