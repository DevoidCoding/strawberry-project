

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum MountEquipedErrorEnum
    {
        Unset = 0,
        Set = 1,
        Riding = 2,
    }
}