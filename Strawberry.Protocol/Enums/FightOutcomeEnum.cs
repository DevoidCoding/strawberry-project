

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum FightOutcomeEnum
    {
        ResultLost = 0,
        ResultDraw = 1,
        ResultVictory = 2,
        ResultTax = 5,
        ResultDefenderGroup = 6,
    }
}