

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum BuildTypeEnum
    {
        Release = 0,
        Beta = 1,
        Alpha = 2,
        Testing = 3,
        Internal = 4,
        Debug = 5,
        Experimental = 6,
    }
}