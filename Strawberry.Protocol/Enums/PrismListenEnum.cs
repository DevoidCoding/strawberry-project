

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PrismListenEnum
    {
        PrismListenNone = 0,
        PrismListenMine = 1,
        PrismListenAll = 2,
    }
}