

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum FightTypeEnum
    {
        FightTypeChallenge = 0,
        FightTypeAgression = 1,
        FightTypePvma = 2,
        FightTypeMxvm = 3,
        FightTypePvm = 4,
        FightTypePvt = 5,
        FightTypePvmu = 6,
        FightTypePvpArena = 7,
        FightTypeKoh = 8,
        FightTypePvpr = 9,
    }
}