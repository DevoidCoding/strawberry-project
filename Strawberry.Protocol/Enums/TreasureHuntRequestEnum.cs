

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TreasureHuntRequestEnum
    {
        TreasureHuntErrorUndefined = 0,
        TreasureHuntErrorNoQuestFound = 2,
        TreasureHuntErrorAlreadyHaveQuest = 3,
        TreasureHuntErrorNotAvailable = 4,
        TreasureHuntErrorDailyLimitExceeded = 5,
        TreasureHuntOk = 1,
    }
}