

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum GameActionMarkCellsTypeEnum
    {
        CellsCircle = 0,
        CellsCross = 1,
        CellsSquare = 2,
    }
}