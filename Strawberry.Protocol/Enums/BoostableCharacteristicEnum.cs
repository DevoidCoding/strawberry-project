

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum BoostableCharacteristicEnum
    {
        BoostableCharacStrength = 10,
        BoostableCharacVitality = 11,
        BoostableCharacWisdom = 12,
        BoostableCharacChance = 13,
        BoostableCharacAgility = 14,
        BoostableCharacIntelligence = 15,
    }
}