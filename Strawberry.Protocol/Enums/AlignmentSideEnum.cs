

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum AlignmentSideEnum
    {
        AlignmentUnknown = -2,
        AlignmentWithout = -1,
        AlignmentNeutral = 0,
        AlignmentAngel = 1,
        AlignmentEvil = 2,
        AlignmentMercenary = 3,
    }
}