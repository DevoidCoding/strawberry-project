

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PrismSetSabotagedRefusedReasonEnum
    {
        SabotageRefused = -1,
        SabotageInsufficientRights = 0,
        SabotageMemberAccountNeeded = 1,
        SabotageRestrictedAccount = 2,
        SabotageWrongAlliance = 3,
        SabotageNoPrism = 4,
        SabotageWrongState = 5,
    }
}