

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum StatsUpgradeResultEnum
    {
        None = -1,
        Success = 0,
        Restricted = 1,
        Guest = 2,
        InFight = 3,
        NotEnoughPoint = 4,
    }
}