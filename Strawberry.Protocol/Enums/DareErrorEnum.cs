

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum DareErrorEnum
    {
        UknownError = 0,
        DareCreationFailed = 1,
        DareUnknown = 2,
        DareCantCancel = 3,
        DareRewardUnknown = 4,
    }
}