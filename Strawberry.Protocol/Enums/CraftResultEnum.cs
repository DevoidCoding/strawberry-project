

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum CraftResultEnum
    {
        CraftImpossible = 0,
        CraftFailed = 1,
        CraftSuccess = 2,
        CraftNeutral = 3,
    }
}