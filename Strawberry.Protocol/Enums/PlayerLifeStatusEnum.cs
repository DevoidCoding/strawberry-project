

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PlayerLifeStatusEnum
    {
        StatusAliveAndKicking = 0,
        StatusTombstone = 1,
        StatusPhantom = 2,
    }
}