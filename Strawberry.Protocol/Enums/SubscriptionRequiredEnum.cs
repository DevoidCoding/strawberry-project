

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum SubscriptionRequiredEnum
    {
        LimitedToSubscriber = 0,
        LimitOnJobXp = 1,
        LimitOnJobUse = 2,
        LimitOnMap = 3,
        LimitOnItem = 4,
        LimitOnVendor = 5,
    }
}