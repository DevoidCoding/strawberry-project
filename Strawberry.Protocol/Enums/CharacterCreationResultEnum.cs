

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum CharacterCreationResultEnum
    {
        Ok = 0,
        ErrNoReason = 1,
        ErrInvalidName = 2,
        ErrNameAlreadyExists = 3,
        ErrTooManyCharacters = 4,
        ErrNotAllowed = 5,
        ErrNewPlayerNotAllowed = 6,
        ErrRestricedZone = 7,
        ErrInconsistentCommunity = 8,
    }
}