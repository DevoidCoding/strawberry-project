

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum ServerConnectionErrorEnum
    {
        ServerConnectionErrorDueToStatus = 0,
        ServerConnectionErrorNoReason = 1,
        ServerConnectionErrorAccountRestricted = 2,
        ServerConnectionErrorCommunityRestricted = 3,
        ServerConnectionErrorLocationRestricted = 4,
        ServerConnectionErrorSubscribersOnly = 5,
        ServerConnectionErrorRegularPlayersOnly = 6,
        ServerConnectionErrorMonoaccountCannotVerify = 7,
        ServerConnectionErrorMonoaccountOnly = 8,
        ServerConnectionErrorServerOverload = 9,
    }
}