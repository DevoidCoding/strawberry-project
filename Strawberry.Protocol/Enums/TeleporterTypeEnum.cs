

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TeleporterTypeEnum
    {
        TeleporterZaap = 0,
        TeleporterSubway = 1,
        TeleporterPrism = 2,
        TeleporterHavenbag = 3,
    }
}