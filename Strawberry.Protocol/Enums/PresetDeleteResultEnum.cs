

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PresetDeleteResultEnum
    {
        PresetDelOk = 1,
        PresetDelErrUnknown = 2,
        PresetDelErrBadPresetId = 3,
    }
}