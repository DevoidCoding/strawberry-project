

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PartyJoinErrorEnum
    {
        PartyJoinErrorUnknown = 0,
        PartyJoinErrorPlayerNotFound = 1,
        PartyJoinErrorPartyNotFound = 2,
        PartyJoinErrorPartyFull = 3,
        PartyJoinErrorPlayerBusy = 4,
        PartyJoinErrorPlayerAlreadyInvited = 6,
        PartyJoinErrorPlayerTooSollicited = 7,
        PartyJoinErrorPlayerLoyal = 8,
        PartyJoinErrorUnmodifiable = 9,
        PartyJoinErrorUnmetCriterion = 10,
        PartyJoinErrorNotEnoughRoom = 11,
        PartyJoinErrorCompositionChanged = 12,
        PartyJoinErrorPlayerInTutorial = 13,
    }
}