

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum PresetSaveUpdateErrorEnum
    {
        PresetUpdateErrUnknown = 1,
        PresetUpdateErrBadPresetId = 2,
        PresetUpdateErrBadPosition = 3,
        PresetUpdateErrBadObjectId = 4,
    }
}