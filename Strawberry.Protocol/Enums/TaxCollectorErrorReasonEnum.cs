

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TaxCollectorErrorReasonEnum
    {
        TaxCollectorErrorUnknown = 0,
        TaxCollectorNotFound = 1,
        TaxCollectorNotOwned = 2,
        TaxCollectorNoRights = 3,
        TaxCollectorMaxReached = 4,
        TaxCollectorAlreadyOne = 5,
        TaxCollectorCantHireYet = 6,
        TaxCollectorCantHireHere = 7,
        TaxCollectorNotEnoughKamas = 8,
    }
}