

// Generated on 02/12/2018 03:57:04
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum DareCriteriaTypeEnum
    {
        MonsterId = 0,
        ChallengeId = 1,
        Idols = 2,
        IdolsScore = 3,
        MaxCharLvl = 4,
        MaxFightTurns = 5,
        MaxCountChar = 6,
        MinCountChar = 7,
        ForbiddenBreeds = 8,
        MandatoryBreeds = 9,
        MinCountMonsters = 10,
    }
}