

// Generated on 02/12/2018 03:57:05
using System;
using System.Collections.Generic;

namespace Strawberry.Protocol.Enums
{
    public enum TreasureHuntFlagStateEnum
    {
        TreasureHuntFlagStateUnknown = 0,
        TreasureHuntFlagStateOk = 1,
        TreasureHuntFlagStateWrong = 2,
    }
}