# Protocol Change Log

## 2.41.0
### Datas
#### Added
- NamingRule
- ArenaDuelRankCriterion
- ArenaMaxDuelRankCriterion
- ServerLangs
- ServerCommunity
	- List<int> SupportedLangIds
    - int NamingRulePlayerNameId
    - int NamingRuleGuildNameId
    - int NamingRuleAllianceNameId
    - int NamingRuleAllianceTagId
    - int NamingRulePartyNameId
    - int NamingRuleNameGeneratorId
    - int NamingRuleAdminId
    - int NamingRuleModoId

#### Renamed
- Smiley#8044 to Smiley#8070
- ItemSet#904 to ItemSet#914

### Enums
#### Added
- ChatActivableChannelsEnum
	- ChannelCommunity
- ChatChannelsMultiEnum
	- ChannelCommunity
- PvpArenaTypeEnum
	- ArenaType1vs1

#### Removed
- PvpArenaTypeEnum
	- ArenaType5vs5

### Messages
#### Added
- MigratedServerListMessage
- ChatCommunityChannelCommunityMessage
- ChatCommunityChannelSetCommunityRequestMessage
- HouseGuildRightsViewMessage
	- uint HouseId
	- int InstanceId
- FollowQuestObjectiveRequestMessage
- RefreshFollowedQuestsOrderRequestMessage
- UnfollowQuestObjectiveRequestMessage

#### Renamed
- GameRolePlayArenaUpdatePlayerInfosWithTeamMessage to GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage
- HouseSoldMessage to HouseSellingUpdateMessage 
- GuildHouseTeleportRequestMessage to HouseTeleportRequestMessage

### Types
#### Added
- CharacterCharacteristicsInformations
    - CharacterBaseCharacteristic MeleeDamageDonePercent
    - CharacterBaseCharacteristic MeleeDamageReceivedPercent
    - CharacterBaseCharacteristic RangedDamageDonePercent
    - CharacterBaseCharacteristic RangedDamageReceivedPercent
    - CharacterBaseCharacteristic WeaponDamageDonePercent
    - CharacterBaseCharacteristic WeaponDamageReceivedPercent
    - CharacterBaseCharacteristic SpellDamageDonePercent
    - CharacterBaseCharacteristic SpellDamageReceivedPercent
- GameFightMinimalStats
    - ushort MeleeDamageReceivedPercent
    - ushort RangedDamageReceivedPercent
    - ushort WeaponDamageReceivedPercent
    - ushort SpellDamageReceivedPercent
- AccountHouseInformations
    - HouseInstanceInformations HouseInfos
    - ulong RealPrice
    - bool IsLocked

#### Removed
- AccountHouseInformations
    - uint InstanceId
	- bool SecondHand